PROJECT := $(PWD)
BUILD := $(PROJECT)/Build
SOURCE := $(PROJECT)/MPPInterfaces
GPCPINTERFACES := $(BUILD)/GPCPInterfaces
FPCPINTERFACES := $(BUILD)/FPCPInterfaces
MWPINTERFACES := $(BUILD)/MWPInterfaces
DEPENDENCIES := $(BUILD)/Dependencies.make
UNITLIST := $(BUILD)/UnitList.txt
UNITDEPENDENCYLIST := $(BUILD)/UnitDependencyList.txt

CS := NONE
FORCE := NO
USEINCLUDES := NO

mkdirs = $(shell mkdir -p "$(BUILD)"); \
				$(shell mkdir -p "$(GPCPINTERFACES)"); \
				$(shell mkdir -p "$(FPCPINTERFACES)"); \
				$(shell mkdir -p "$(MWPINTERFACES)");

now_mkdirs := $(mkdirs)

update_unit_list := $(shell /usr/bin/perl Scripts/UpdateUnitList.pl "$(SOURCE)" "$(UNITLIST)")
update_dependencies := $(shell /usr/bin/perl Scripts/UpdateDependencies.pl "$(UNITLIST)" "$(DEPENDENCIES)")

.PHONY: all
all: gpcpinterfaces fpcpinterfaces mwpinterfaces

.PHONY: compile
compile: compilegpcpinterfaces compilefpcpinterfaces compilemwpinterfaces

.PHONY: test
test: testgpcpinterfaces testfpcpinterfaces testmwpinterfaces

.PHONY: link
link: linkgpcpinterfaces linkfpcpinterfaces linkmwpinterfaces

.PHONY: clean
clean:
	rm -rf "$(BUILD)"

.PHONY: convert
convert:
	/usr/bin/perl Scripts/Convert.pl $(CS) $(FORCE) $(SOURCE)

include $(DEPENDENCIES)

GPCEXTRAS := 
FPCEXTRAS := 
MWEXTRAS := 


# MacOS
GPCEXTRAS += $(GPCPINTERFACES)/MacOS.pas
$(GPCPINTERFACES)/MacOS.pas: $(UNITLIST) Scripts/GenerateMacOS.pl
	/usr/bin/perl Scripts/GenerateMacOS.pl GPC "$<" "$@"

FPCEXTRAS += $(FPCPINTERFACES)/MacOS.pas
$(FPCPINTERFACES)/MacOS.pas: $(UNITLIST) Scripts/GenerateMacOS.pl
	/usr/bin/perl Scripts/GenerateMacOS.pl FPC "$<" "$@"

MWEXTRAS += $(MWPINTERFACES)/MacOS.p
$(MWPINTERFACES)/MacOS.p: $(UNITLIST) Scripts/GenerateMacOS.pl
	/usr/bin/perl Scripts/GenerateMacOS.pl MW "$<" "$@"


# MacOSAll
GPCEXTRAS += $(GPCPINTERFACES)/MacOSAll.pas
$(GPCPINTERFACES)/MacOSAll.pas: $(GPCPINTERFACESFILES) $(UNITDEPENDENCYLIST) Scripts/GenerateMacOSAll.pl
	/usr/bin/perl Scripts/GenerateMacOSAll.pl GPC "$(GPCPINTERFACES)" "$(UNITDEPENDENCYLIST)" "$(USEINCLUDES)" "$@"

FPCEXTRAS += $(FPCPINTERFACES)/MacOSAll.pas
$(FPCPINTERFACES)/MacOSAll.pas: $(FPCPINTERFACESFILES) $(UNITDEPENDENCYLIST) Scripts/GenerateMacOSAll.pl
	/usr/bin/perl Scripts/GenerateMacOSAll.pl FPC "$(FPCPINTERFACES)" "$(UNITDEPENDENCYLIST)" "$(USEINCLUDES)" "$@"

MWEXTRAS += $(MWPINTERFACES)/MacOSAll.p
$(MWPINTERFACES)/MacOSAll.p: $(MWPINTERFACESFILES) $(UNITDEPENDENCYLIST) Scripts/GenerateMacOSAll.pl
	/usr/bin/perl Scripts/GenerateMacOSAll.pl MW "$(MWPINTERFACES)" "$(UNITDEPENDENCYLIST)" "$(USEINCLUDES)" "$@"

# FPCStrings
FPCEXTRAS += $(FPCPINTERFACES)/GPCStrings.pas
$(FPCPINTERFACES)/GPCStrings.pas: $(SOURCE)/FPCStrings.pas Scripts/GenerateFile.pl
	/usr/bin/perl Scripts/GenerateFile.pl FPC "$<" "$@"

# GPCStrings
GPCEXTRAS += $(GPCPINTERFACES)/GPCStrings.pas
$(GPCPINTERFACES)/GPCStrings.pas: $(SOURCE)/GPCStrings.pas Scripts/GenerateFile.pl
	/usr/bin/perl Scripts/GenerateFile.pl GPC "$<" "$@"

GPCEXTRAS += $(GPCPINTERFACES)/GPCStringsAll.pas
$(GPCPINTERFACES)/GPCStringsAll.pas: $(SOURCE)/GPCStrings.pas Scripts/GenerateFile.pl
	/usr/bin/perl Scripts/GenerateFile.pl GPC "$<" "$@"


# GPCMacros.inc
GPCEXTRAS += $(GPCPINTERFACES)/GPCMacros.inc
$(GPCPINTERFACES)/GPCMacros.inc: $(SOURCEFILES) $(UNITLIST) Scripts/GenerateFile.pl
	/usr/bin/perl Scripts/GenerateFile.pl GPC "$(SOURCE)" "$@"


# UnitDependencyList.txt
$(UNITDEPENDENCYLIST): $(SOURCEFILES) $(UNITLIST) Scripts/UpdateUnitDpendencies.pl
	/usr/bin/perl Scripts/UpdateUnitDpendencies.pl "$(SOURCE)" "$(UNITLIST)" "$@"


# Makefile
GPCEXTRAS += $(GPCPINTERFACES)/Makefile
$(GPCPINTERFACES)/Makefile: $(UNITDEPENDENCYLIST) Scripts/GenerateMakefile.pl $(GPCPINTERFACES)/MacOS.pas $(GPCPINTERFACES)/MacOSAll.pas  $(GPCPINTERFACES)/GPCStrings.pas $(GPCPINTERFACES)/GPCStringsAll.pas
	/usr/bin/perl Scripts/GenerateMakefile.pl GPC "$(UNITDEPENDENCYLIST)" "$@"

FPCEXTRAS += $(FPCPINTERFACES)/Makefile 
$(FPCPINTERFACES)/Makefile: $(UNITDEPENDENCYLIST) Scripts/GenerateMakefile.pl $(FPCPINTERFACES)/MacOS.pas $(FPCPINTERFACES)/MacOSAll.pas $(FPCPINTERFACES)/GPCStrings.pas
	/usr/bin/perl Scripts/GenerateMakefile.pl FPC "$(UNITDEPENDENCYLIST)" "$@"

MWEXTRAS += $(MWPINTERFACES)/Makefile $(MWPINTERFACES)/MacOS.p $(MWPINTERFACES)/MacOSAll.p
$(MWPINTERFACES)/Makefile: $(UNITDEPENDENCYLIST) Scripts/GenerateMakefile.pl
	/usr/bin/perl Scripts/GenerateMakefile.pl MW "$(UNITDEPENDENCYLIST)" "$@"


# Make everything
.PHONY: gpcpinterfaces
gpcpinterfaces: $(GPCPINTERFACESFILES) $(GPCEXTRAS)

.PHONY: fpcpinterfaces
fpcpinterfaces: $(FPCPINTERFACESFILES) $(FPCEXTRAS)

.PHONY: mwpinterfaces
mwpinterfaces: $(MWPINTERFACESFILES) $(MWEXTRAS)


# Compile everything
.PHONY: compilegpcpinterfaces
compilegpcpinterfaces: gpcpinterfaces
	cd $(GPCPINTERFACES); make

.PHONY: compilefpcpinterfaces
compilefpcpinterfaces: fpcpinterfaces
	cd $(FPCPINTERFACES); make

.PHONY: compilemwpinterfaces
compilemwpinterfaces: mwpinterfaces
	cd $(MWPINTERFACES); make


# Test everything
.PHONY: testgpcpinterfaces
testgpcpinterfaces: compilegpcpinterfaces

.PHONY: testfpcpinterfaces
testfpcpinterfaces: compilefpcpinterfaces

.PHONY: testmwpinterfaces
testmwpinterfaces: compilemwpinterfaces


# link everything
.PHONY: linkgpcpinterfaces
linkgpcpinterfaces: $(GPCPINTERFACES)/Makefile
	cd $(GPCPINTERFACES); make link

.PHONY: linkfpcpinterfaces
linkfpcpinterfaces: $(FPCPINTERFACES)/Makefile
	cd $(FPCPINTERFACES); make link

.PHONY: linkmwpinterfaces
linkmwpinterfaces: $(MWPINTERFACES)/Makefile
	cd $(MWPINTERFACES); make link


