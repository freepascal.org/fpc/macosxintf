#!/usr/bin/perl

use strict;
use warnings;

# TODO
# Add support for patch adding CFSTR declarations

# REMOVE if ( 0 ) {
use diagnostics;
use Carp ();
$SIG{__WARN__} = \&Carp::cluck;
$SIG{__DIE__} = \&Carp::confess;use strict;
# REMOVE }

# HACK HACK HACK START
chdir( '/Users/peter/unix/gpc/common-pinterfaces' ) or die;
# HACK HACK HACK FINISH

my $gpcutil = `which gpc`;
$gpcutil ||= '/usr/local/bin/gpc' if -e '/usr/local/bin/gpc';
$gpcutil ||= '/usr/bin/gpc' if -e '/usr/bin/gpc';
chomp $gpcutil if $gpcutil;

my $fpcutil = `which fpc`;
$fpcutil ||= '/usr/local/bin/fpc' if -e '/usr/local/bin/fpc';
$fpcutil ||= '/usr/bin/fpc' if -e '/usr/bin/fpc';
chomp $fpcutil if $fpcutil;

my $makeutil = `which make`;
$makeutil ||= '/usr/local/bin/make' if -e '/usr/local/bin/make';
$makeutil ||= '/usr/bin/make' if -e '/usr/bin/make';
$makeutil ||= '/bin/make' if -e '/bin/make';
chomp $makeutil if $makeutil;

our $url = 'ftp://ftp.apple.com/developer/Development_Kits/UniversalInterfaces3.4.2.img.bin';
our $binname = 'UniversalInterfaces3.4.2.img.bin';
our $imgname = 'UniversalInterfaces3.4.2.img';
our $volume = '/Volumes/UniversalInterfaces3.4.2';

our $imgpinterfaces = "$volume/Universal/Interfaces/PInterfaces";
our $orgpinterfaces = 'OriginalPInterfaces';
our $extrapinterfaces = 'ExtraPInterfaces';
our $gpcpinterfaces = 'GPCPInterfaces';
our $patches = 'Patches';
our $patchesremade = 'PatchesRemade';

our $mwpinterfaces = 'MWPInterfaces';
our $mwpatches = 'MWPatches';
our $mwpatchesremade = 'MWPatchesRemade';

our $fpcpinterfaces = 'FPCPInterfaces';
our $fpcpatches = 'FPCPatches';
our $fpcpatchesremade = 'FPCPatchesRemade';

our $mppinterfaces = 'MPPInterfaces';
our $mppatches = 'MPPatches';
our $mppatchesremade = 'MPPatchesRemade';

our $gpcmacrosfile = 'GPCMacros.inc';
our $gpcmacosallunit = 'MacOSAll';
our $gpcmacosallfile = "$gpcmacosallunit.pas";
our $gpcstringsunit = 'GPCStrings';
our $gpcstringsfile = "$gpcstringsunit.pas";
our $gpcstringsallunit = 'GPCStringsAll';
our $gpcstringsallfile = "$gpcstringsallunit.pas";

our $cpisetcftypes = 'cpisetcftypes';

our $remakepatches = 1;
our $makemacosall = 1;
our $gpcautomake = 1;
our $removegpcobjects = 1;
our $makemwpinterfaces = 1;
our $makefpcpinterfaces = 1;
our $makemppinterfaces = 1;
our $fpcautomake = 1;
our $removefpcobjects = 1;

our $outputtestrecord = 1;
our $outputextraunits = 0;
our $outputstringtypes = 0;
our $justmw = 0;

our $peterhack = 1;

our $stringtypes;
our $stringinterface;
our $stringimplementation;

our $maketemplate = <<'EOM';
# Makefile for %COMPILERNAME% Interfaces.
#
# Author: Adriaan van Os <gpc@microbizz.nl> Date: January 11, 2005.
# Updated: Peter N Lewis <peter@stairways.com.au> Date: June 30, 2005.

# This file is freeware.
# It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# -----------------------------------------------------------------------------------------------

PATH := $(PATH):/usr/local/bin:/usr/bin:/bin
INTERFACEDIR := %INTERFACEDIR%

.PHONY: all
all: precompile link

.PHONY: precompile
precompile: cleaner
%COMPILECODE%

.PHONY: link
link:
	ln -f -s -h "$(shell pwd)" "$(INTERFACEDIR)"

.PHONY: clean
clean:
	rm -f *.o *.gpi *.gpd *.ppu

.PHONY: cleaner
cleaner:
	rm -f *.o *.gpi *.gpd *.ppu *.a

# That's all folks.
EOM

die "Current directory must be in common-pinterfaces directory" 
  unless -r 'common-pinterfaces.txt' && -d $patches;

if ( $gpcautomake && !$gpcutil ) {
	print "GPC not found, not compiling GPC interfaces\n";
	$gpcautomake = 0;
}
if ( $fpcautomake && !$fpcutil ) {
	print "FPC not found, not compiling FPC interfaces\n";
	$fpcautomake = 0;
}

our $version = '1.1.0';
$version =~ m!^(\d)\.(\d)\.(\d)$! or die;
our $versioncode = "\$0$1$2$3";
our $interfacecomment = <<"EOM";
    Modified for use with GNU Pascal by make-common-interfaces.pl version $version
    Please report any bugs to <gpc\@microbizz.nl>
EOM
our $mwinterfacecomment = <<"EOM";
    Modified for use with MW Pascal by make-common-interfaces.pl version $version
    Please report any bugs to <gpc\@microbizz.nl>
EOM
our $fpcinterfacecomment = <<"EOM";
    Modified for use with Free Pascal by make-common-interfaces.pl version $version
    Please report any bugs to <gpc\@microbizz.nl>
EOM
our $applecopyright;
our $applerelease;

our %defines = (
  TARGET_CPU_PPC => 1,
  TARGET_CPU_68K => 0,
  TARGET_CPU_X86 => 0,
  TARGET_CPU_MIPS => 0,
  TARGET_CPU_SPARC => 0,
  TARGET_RT_MAC_CFM => 0,
  TARGET_RT_MAC_MACHO => 1,
  TARGET_RT_MAC_68881 => 0,
  TARGET_OS_MAC => 1,
  TARGET_OS_WIN32 => 0,
  TARGET_OS_UNIX => 0,
  TARGET_RT_LITTLE_ENDIAN => 0,
  TARGET_RT_BIG_ENDIAN => 1,
  TYPE_EXTENDED => 0,
  TYPE_LONGLONG => 1,
  TYPE_BOOL => 0,
  TYPED_FUNCTION_POINTERS => 1,
  TARGET_API_MAC_OS8 => 0,
  TARGET_API_MAC_CARBON => 1,
  TARGET_API_MAC_OSX => 1,
  TARGET_CARBON => 1,
  OLDROUTINENAMES => 0,
  OPAQUE_TOOLBOX_STRUCTS => 1,
  OPAQUE_UPP_TYPES => 1,
  ACCESSOR_CALLS_ARE_FUNCTIONS => 1,
  CALL_NOT_IN_CARBON => 0,
  OTKERNEL => 0,
  OTCARBONAPPLICATION => 1,
  PM_USE_SESSION_APIS => 1,
);

our $gpcsetupconditionals = <<'EOM';
{$gnu-pascal}
{$w no-underscore}
{$w no-identifier-case-local}
{$maximum-field-alignment=16}
{$disable-keyword=pow}

EOM

our $fpcsetupconditionals = <<'EOM';
{$mode macpas}
{$packenum 2}
{$macro on}
{$CALLING MWPASCAL}

EOM

our $defineusecfstr = <<'EOM';
{$ifc not defined USE_CFSTR_CONSTANT_MACROS}
    {$definec USE_CFSTR_CONSTANT_MACROS TRUE}
{$endc}
EOM

our $conditionals = '';
$conditionals .= <<"EOM";
{\$definec UNIVERSAL_INTERFACES_VERSION \$0342}
{\$definec GAP_INTERFACES_VERSION $versioncode}
{\$definec FOUR_CHAR_CODE(s) ((Ord (substr((s),1,1)[1]) shl 24) or (Ord(substr((s),2,1)[1]) shl 16) or (Ord (substr((s),3,1)[1]) shl 8) or Ord(substr((s),4,1)[1]))}

EOM
foreach my $define (sort keys %defines) {
  $conditionals .= sprintf( "{\$definec %-30s %s}\n", $define, $defines{$define} ? 'TRUE':'FALSE' );
}

if ( ! -d $orgpinterfaces ) {
  # Download the interfaces

  # Unmount the disk image
  if ( -d $volume ) {
    print "Unmount the disk image\n";
    unmount( $volume );
    wait_on_existence( 0, $volume );
  }
  
  # Download the Universal Interfaces from Apple
  if ( !-r $binname && !-r $imgname ) {
    print "Download the Universal Interfaces from Apple\n";
    system( '/usr/bin/curl', '-s', '-o', $binname, $url );
  }
    
  # Mount the disk image
  print "Mount the disk image\n";
  if ( -r $imgname ) {
    system( '/usr/bin/open', $imgname );
  } elsif ( -r $binname ) {
    system( '/usr/bin/open', $binname );
  } else {
    die "Neither $imgname nor $binname found";
  }
  wait_on_existence( 1, $volume );
  
  # Mount the PInterfaces
  die "Cannot find PInterfaces on disk image" unless -d $imgpinterfaces;
  system( '/bin/rm', '-rf', $orgpinterfaces );
  die "Delete failed" if -d $orgpinterfaces;
  system( '/bin/cp', '-R', $imgpinterfaces, $orgpinterfaces );
  die "Copy failed" unless -d $imgpinterfaces;
  
  # Unmount the disk image
  print "Unmount the disk image\n";
  unmount( $volume );
  wait_on_existence( 0, $volume );
}

our %fixparam = ();
our %addconstants = ();
our %cftypes = ();
our %fixdirid = ();
our %extraunits = ();
our %nomactypes = ();
our %ignorerecords = ();
our %units = ();
our %killunits = ();
our %gpcbadconst = ();
our %mwbadconst = ();
our %fpcbadconst = ();
our %fixunitnames = ();
our %fixmwerks = ();

our @allunits = ();
our %records = ();

our %cfstrings = ();

if ( $justmw ) {
  chdir( "$gpcpinterfaces" ) or die "Cant chdir ../$gpcpinterfaces $!";
} else {
  
  
print "Convert PInterfaces for use by GPC\n";
# Remove the old GPCInterfaces
system( '/bin/rm', '-rf', $gpcpinterfaces );
die "Delete failed" if -d $gpcpinterfaces;
mkdir( $gpcpinterfaces, 0755 ) or die "Cant mkdir $gpcpinterfaces $!";

# Read in the automated correction data (see DATA section at end)

load_data();
setup_string_extras();

print "Process each file\n";
# Process each file
chdir( $orgpinterfaces ) or die "Cant chdir $gpcpinterfaces $!";
our @allfiles = read_dir( '.' );
push @allfiles, read_dir( "../$extrapinterfaces" );
my $previousfile = undef;
foreach my $orgfile ( sort( @allfiles ) ) {
  next unless $orgfile =~ /\.p/;
  next if $previousfile && $previousfile eq $orgfile;
  $previousfile = $orgfile;
  my $file = $fixunitnames{$orgfile} || $orgfile;
  next if $killunits{$orgfile};
  
  my $isextraunit = 1;
  my $data = read_file( "../$extrapinterfaces/$orgfile" );
  if ( !$data ) {
  	$data = read_file( $orgfile );
  	$isextraunit = 0;
  }
  die "Cannot read file $orgfile" unless $data;

  # Fix end of line characters (so diff can work if nothing else)
  $data =~ tr!\015!\n!;
  
  $data =~ s!\}!}\n{\n$interfacecomment}!;

  # GPC supports const parameters, so use them
  $data =~ s!\{CONST\}VAR\b!protected var!g;
  
  $data =~ s!\bUNIT\b!unit!g;
  $data =~ s!\bINTERFACE\b!interface!g;
  $data =~ s!\bIMPLEMENTATION\b!implementation!g;
  $data =~ s!\bFUNCTION\b!function!g;
  $data =~ s!\bPROCEDURE\b!procedure!g;
  $data =~ s!\bVAR\b!var!g;
  $data =~ s!\bCONST\b!const!g;
  $data =~ s!\bRECORD\b!record!g;
  $data =~ s!\bCASE\b!case!g;
  $data =~ s!\bEND\b!end!g;
  $data =~ s!\bARRAY\b!array!g;
  $data =~ s!\bPACKED\b!packed!g;
  $data =~ s!\bSET\b!set!g;
  $data =~ s!\bOF\b!of!g;
  $data =~ s!\bTYPE\b!type!g;
  $data =~ s!\bPROTECTED\b!protected!g;
  $data =~ s!\bCHAR\b!char!g;
  $data =~ s!\bBOOLEAN\b!boolean!g;
  
  $data =~ s#(\b(?i:function|procedure)\s+\w+\s*\([ -~]*)(?<!var )\b(\w+\s*:\s*Str\d{1,3})\b#${1}protected var $2#g;
  $data =~ s#(\b(?i:function|procedure)\s+\w+\s*\([ -~]*)(?<!var )\b(\w+\s*:\s*Str\d{1,3})\b#${1}protected var $2#g;
  $data =~ s#(\b(?i:function|procedure)\s+\w+\s*\([ -~]*)(?<!var )\b(\w+\s*:\s*Str\d{1,3})\b#${1}protected var $2#g;
  $data =~ s#(\b(?i:function|procedure)\s+\w+\s*\([ -~]*)(?<!var )\b(\w+\s*:\s*Str\d{1,3})\b#${1}protected var $2#g;

# write_file( "../$gpcpinterfaces/$file", $data ); # .'as'
  
  # Fix up buggy parameters
  if ( $fixparam{$file} ) {
    foreach my $fix ( @{$fixparam{$file}} ) {
      my ( $func, $old, $new ) = split( /#/, $fix );
      die unless $func && $old && $new;
      $data =~ s!(\b(?i:function|procedure)\s+$func\s*\([ -~]*)$old\b!$1$new! || die "Cant fix param for $file:$fix";
    }
  }

  # Add missing external constants
  if ( $addconstants{$file} ) {
    foreach my $fix ( @{$addconstants{$file}} ) {
      my ( $type, $constant ) = split( /#/, $fix );
      die unless $type && $constant;
      $data =~ s!(\n[ \t]*\{\s*\*\s*$constant\n)!\nvar $constant: $type; external name '$constant'; attribute (const);$1! or die "Cant add constant for $file:$fix";
    }
  }

  # Fix up abstract CF Types
#  if ( $cftypes{$file} ) {
#    foreach my $type ( @{$cftypes{$file}} ) {
#      my $obj = $type;
#      $obj =~ s!Ref$!AbstractObject!;
#      $data =~ s!\n([ \t]*)($type\s*=\s*)\^LONGINT;!\n$1$obj = abstract object(CFTypeAbstractObject) end;\n$1$2^$obj;! or die "Cant fix abstract type for $file:$type";
#    }
#  }

  # Fix up UInt32 dir IDs
  if ( $fixdirid{$file} ) {
    $data =~ s!(DirID:\s+)LONGINT!$1UInt32!g;
    $data =~ s!(parID:\s+)LONGINT!$1UInt32!g;
    $data =~ s!(dirID:\s+)LONGINT!$1UInt32!g;
  }
  
#  write_file( "../$gpcpinterfaces/$file", $data ); # .'as'
	# Convert from Includes to uses
	$data =~ s!\{\$IFC UNDEFINED UsingIncludes\}\n\{\$SETC UsingIncludes := 0\}\n\{\$ENDC\}\n!!i || $isextraunit || die "Cant remove UsingIncludes $file 1";
	$data =~ s!\{\$IFC NOT UsingIncludes\}\n\s*unit\s+(\w+);\n\s*interface\n\{\$ENDC\}\n!unit $1;\ninterface\n!i || die "Cant find unit name $file 2";
	my $unitname = $1;
	my $orgunitname = $unitname;
	if ( $fixunitnames{$orgfile} ) {
		die unless $fixunitnames{$unitname};
		$data =~ s!\nunit $unitname;\ninterface\n!\nunit $fixunitnames{$unitname};\ninterface\n! || die "Cant find unit name $file 3";
		$unitname = $fixunitnames{$unitname};
	}
	push @allunits, $unitname;
	my $unitincludes = "${orgunitname}Includes";
	my $unitunderscores = "__${orgunitname}__";
	$unitunderscores =~ tr/a-z/A-Z/;
	$data =~ s!\{\$IFC UNDEFINED $unitunderscores\}\n\{\$SETC $unitunderscores := 1\}\n!!i || $isextraunit || die "Cant remove UsingIncludes $file 3";
	$data =~ s!\{\$I\+\}\n!! || $isextraunit || die "Cant remove I+ $file 4";
	$data =~ s!\{\$SETC $unitincludes := UsingIncludes\}\n!!i || $isextraunit || die "Cant remove UsingIncludes $file 5";
	$data =~ s!\{\$SETC UsingIncludes := 1\}\n!!i || $isextraunit || die "Cant remove UsingIncludes $file 6";
	$data =~ s!\{\$SETC UsingIncludes := $unitincludes\}\n!!i || $isextraunit || die "Cant remove UsingIncludes $file 7";
	$data =~ s!\{\$ENDC\} \{$unitunderscores\}\n!!i || $isextraunit || die "Cant remove UsingIncludes $file 8";
	$data =~ s!\{\$IFC NOT UsingIncludes\}\n\s*end\.\n\{\$ENDC\}\n! end.\n!i || $isextraunit || die "Cant remove UsingIncludes $file 9";
	
	$data =~ s!\{\$LibExport\+\}!{#LibExport+}!i;

	$data =~ s!\{\$PUSH\}!{#PUSH}!ig;
	$data =~ s!\{\$ALIGN MAC68K\}!{\$maximum-field-alignment=16}!ig;
	$data =~ s!\{\$ALIGN POWER\}!{\$maximum-field-alignment=32}!ig;
	$data =~ s!\{\$ALIGN RESET\}!{\$maximum-field-alignment=16}!ig;
	$data =~ s!\{\$POP\}!{#POP}!ig;

	$data =~ s|\n[ \t]*\{?\s*(\w+)\s*=\s*CFSTR\s*\(\s*"([ !#\$%&\(\)\*-~]*)"\s*\)\s*;?\s*\}?|\n{ $1 = CFSTR("$2"); defined in GPCMacros.inc }|gm;
	while ( $data =~ m|\{ (\w+) = CFSTR\("([^"']*)"\); defined in GPCMacros.inc \}|g ) {
		print "Duplicate CFSTR defintion for $1 in $file\n" if $cfstrings{$1};
		print "Empty CFSTR definition for $1 in $file\n" unless $2;
		$cfstrings{$1} = $2;
	}

	$data =~ s!\{\$I\+\}!{#I+}!g;
	$data =~ s!\;\s*C;!;!g;  #  (* C; *)

	if ( $unitname eq 'MacTypes' ) {
		$data =~ m!\n[ \t]*(Copyright:.*)!m or die;
		$applecopyright = $1;
		$data =~ m!\n[ \t]*(Release:.*)!m or die;
		$applerelease = $1;
	}

	my @units;
	@units = ( $data =~ m!\{\$IFC UNDEFINED __\w+__\}\n\{\$I (\w+)\.p\}\n\{\$ENDC\}\n!gi );
	if ( $extraunits{$file} ) {
		unshift( @units, split(',',$extraunits{$file}) );
	}
	if ( !$nomactypes{$file} ) {
		unshift( @units, 'MacTypes' );
	}
	
	# Remove kill units
	@units = map $fixunitnames{$_} || $_, grep( !$killunits{"$_.p"}, @units );
	if ( $unitname =~ m!^(Appearance|CarbonEvents)$! ) {
		@units = grep( !/MacWindows/, @units );
	}
	
	# Remove duplicate units
	my %checkdupunits = ();
	my @tmpunits = @units;
	@units = ();
	foreach ( @tmpunits ) {
		push @units, $_ unless $checkdupunits{$_};
		$checkdupunits{$_} = 1;
	}
	$units{$unitname} = join( ',', @units );
	if ( @units ) {
		$data =~ s!(\n\s*interface\s*\n)!$1.'uses '.join( ',', @units ).";\n"!e  || die "Cant add units $file 1";
	}
	$data =~ s!\{\$IFC UNDEFINED __\w+__\}\n\{\$I (\w+)\.p\}\n\{\$ENDC\}\n!!ig;

	$data =~ s!\bINTEGER\b!SInt16!gi;
	$data =~ s!\bLONGINT\b!SInt32!gi;
	$data =~ s!\bLONGINTPTR\b!SInt32Ptr!gi;
	$data =~ s!\bINTEGERPTR\b!SInt16Ptr!gi;
	
	$data =~ s!\bUNIV Ptr\b!UnivPtr!gi;
	$data =~ s!(\bprocedure[\s\n]+(\w+)[\s\n]*(\([^)]*\))?;)!$1 external name '$2';!gi;
	$data =~ s!(\bfunction[\s\n]+(\w+)[\s\n]*(\([^)]*\))?:[\s\n]*\w+[\s\n]*;)!$1 external name '$2';!gi;
	
	$data =~ s!\bproperty(\s*):!proprty${1}:!g;
	$data =~ s!\bobject(\s*):!objct${1}:!g;
	$data =~ s!\bforward(\s*):!forwrd${1}:!g;
	
	$data =~ s!\n([ \t]*\w+\s*=\s*)('[^']{4}');!\n${1}FOUR_CHAR_CODE($2);!mg;
#  $data =~ s!^((\s*\w+)\s*=\s*('[^']{4}');.*)!$1\n${2}UInt32 = REPLACEUINT32-$3;!mg;
#  $data =~ s!REPLACEUINT32-'(....)'!sprintf( "\$%08X", unpack( "N", $1 ) )!ge;

  $data =~ s!\bprotected var(\s+\w+\s*(?:,\s*\w+\s*)*:\s*(\w+))!($gpcbadconst{$2} ? '(*const*) var' : 'const (*var*)').$1!ge;

	$data =~ s!\n[ \t]*\{\$IFC TARGET_OS_MAC AND TARGET_CPU_68K AND NOT TARGET_RT_MAC_CFM\}.*?\{\$ENDC\}\s*\n?!\n!msgi;

	$data =~ s! *unit (\w+);\s*interface\b!$gpcsetupconditionals\nunit $1;\ninterface\n$conditionals\n! or die "Cant insert conditionals $unitname";

#  $data =~ s!(.*)\end\.!$1 end.!si;

	$data =~ s!\{\$ifc not undefined MWERKS and TARGET_CPU_68K\}!{\$ifc TARGET_CPU_68K}!g;
  
	$data =~ s!\{\$IFC\s+NOT\s+UNDEFINED\s+(\w+)\s*\}!{\$ifc defined $1}!gi;
	$data =~ s!\{\$IFC\s+UNDEFINED\s+(\w+)\s*\}!{\$ifc not defined $1}!gi;
	$data =~ s!\{\$SETC\s+(\w+)\s*:=\s*(.*?)\s*\}!{\$definec $1 $2}!gi;

	$data =~ s!\{\$IFDEF\s+(\w+)\s*\}!{\$ifc defined $1}!gi;
	$data =~ s!\{\$IFNDEF\s+(\w+)\s*\}!{\$ifc not defined $1}!gi;
	$data =~ s!\{\$IFC\s+([^}]*?)\s*\}!{\$ifc $1}!g;
	$data =~ s!\{\$ELSEC\}!{\$elsec}!g;
	$data =~ s!\{\$ENDC\}!{\$endc}!g;

  if ( $fixmwerks{$file} ) {
	  $data =~ s!{\$ifc defined MWERKS}\s*\n(.*)\s*\n{\$endc}!$1!g;
	}
	
  write_file( "../$gpcpinterfaces/${file}as", $data );
}

# die;
chdir( "../$gpcpinterfaces" ) or die "Cant chdir ../$gpcpinterfaces $!";

print "Parse the CFSTRs in the patches\n";
foreach my $file ( sort( grep( m!\.patch$!, read_dir( "../$patches" ) ) ) ) {
	my $patch = read_file( "../$patches/$file" );
	while ( $patch =~ m|^\+[ \t]*\{ (\w+) = CFSTR\("([^"']*)"\); defined in GPCMacros.inc \}|gm ) {
		print "Duplicate CFSTR defintion for $1 in Patch file $file\n" if $cfstrings{$1};
		print "Empty CFSTR definition for $1 in Patch file $file\n" unless $2;
		$cfstrings{$1} = $2;
	}
}

print "Apply all the patches\n";
# Apply all the patches
if ( $remakepatches ) {
  my $newpatches = "../$patchesremade";
  system( '/bin/rm', '-rf', $newpatches );
  die "Delete failed" if -d $newpatches;
  mkdir( $newpatches, 0755 ) or die "Cant mkdir $newpatches $!";
  foreach my $patch ( sort( grep( m!\.patch$!, read_dir( "../$patches" ) ) ) ) {
    my $file = $patch;
    $file =~ s/\.patch/\.pas/;
    system( '/usr/bin/patch', '--backup', '--suffix', '.orig', "--input=../$patches/$patch" );
    system( "/usr/bin/diff -au $file.orig $file >$newpatches/$patch" );
  }
} else {
  foreach my $patch ( sort( grep( m!\.patch$!, read_dir( "../$patches" ) ) ) ) {
    system( '/usr/bin/patch', '--silent', '--no-backup-if-mismatch', "--input=../$patches/$patch" ); #
  }
}

print "Create include file $gpcmacrosfile\n";
open( INCLUDEFILE, ">$gpcmacrosfile" ) or die "cant open out include file $!";
print INCLUDEFILE <<EOM;
{\$ifdef __GPC__}
$conditionals
$defineusecfstr
{\$ifc USE_CFSTR_CONSTANT_MACROS}
EOM
foreach my $cfstr ( sort keys %cfstrings ) {
	print INCLUDEFILE "\t{\$definec $cfstr CFSTRP('$cfstrings{$cfstr}')}\n";
}
print INCLUDEFILE <<EOM;
{\$endc}

{\$endc}
EOM
close( INCLUDEFILE );

print "Create file $gpcstringsfile\n";
push @allunits, $gpcstringsunit;

open( GPCSTRINGS, ">$gpcstringsfile" ) or die "cant open out $gpcstringsfile $!";
print GPCSTRINGS <<EOM;
{\$gnu-pascal}
unit $gpcstringsunit;
interface

uses 
  MacTypes;

$stringinterface

implementation

{\$R-}
$stringimplementation

end.
EOM
close( GPCSTRINGS );

if ( $makemacosall ) {

	open( GPCSTRINGS, ">$gpcstringsallfile" ) or die "cant open out $gpcstringsallfile $!";
	print GPCSTRINGS <<EOM;
{\$gnu-pascal}
unit $gpcstringsallunit;
interface

uses 
  $gpcmacosallunit;

$stringinterface

implementation

{\$R-}
$stringimplementation

end.
EOM
	close( GPCSTRINGS );

}

if ( $outputstringtypes ) {
  print $stringtypes;
}

print "Create file MacOS.pas\n";
open( MACOS, ">MacOS.pas" ) or die "cant open out MacOS.pas $!";
print MACOS <<'EOM';
unit MacOS;
interface

uses 
EOM
print MACOS '  ';
print MACOS join( ",\n  ", @allunits );
print MACOS <<'EOM';
;

end.
EOM
close( MACOS );

if ( $makemacosall ) {
  print "Create file $gpcmacosallfile\n";

  closure( \%units );
  my @units = sort keys %units;
  my %done = ();
  my @sortedunits = ();
  
  my $finished = 0;
  do {
    $finished = 1;
    foreach my $unit ( @units ) {
      next if $done{$unit};
      my $good = 1;
      foreach my $uses (split( /,/, $units{$unit})) {
        $good = 0, last if !$done{$uses};
      }
      if ( $good ) {
        push @sortedunits, $unit;
        $done{$unit} = 1;
        $finished = 0;
      }
    }
  } until $finished;
  
  foreach my $unit (@units) {
    die $unit unless $done{$unit};
  }
#  foreach my $unit ( @sortedunits ) {
#    print STDERR "makemacosall result: $unit\n";
#  }

  open( MACOSALL, ">$gpcmacosallfile.temp" ) or die "cant open out $gpcmacosallfile.temp $!";
  print MACOSALL <<EOM;
{
    $applecopyright
    $applerelease

$interfacecomment

    This file is assembled from all the Universal Interface files.
}

$gpcsetupconditionals

unit $gpcmacosallunit;
interface

$conditionals

EOM
# {\$include <$gpcmacrosfile>}

  foreach my $unit (@sortedunits) {
  	next if $unit eq 'CFStringGlue';
  	
    my $data = read_file( "$unit.pas" );
    die unless $data;
    
    $data =~ s!.*?\n\s*interface *\n!!sm or die "Missing interface in $unit";
# \bimplementation\b.*    
    $data =~ s!\n\s*end\.\s*$!!s or die "Missing end in $unit";
    $data =~ s!^uses .*;$!\n!m;

		RemoveString( $data, $gpcsetupconditionals );
		RemoveString( $data, $conditionals );

    print MACOSALL "{unit $unit}\n";
    print MACOSALL "{\$maximum-field-alignment=16}\n";
    print MACOSALL $data;
    print MACOSALL "\n";
  }

  print MACOSALL <<'EOM';

end.
EOM
  close( MACOSALL );

  open( MACOSALL, "$gpcmacosallfile.temp" ) or die "cant open in $gpcmacosallfile.temp $!";
  open( PARSED, ">$gpcmacosallfile" ) or die "cant open out $gpcmacosallfile $!";
  my $trues = 0;
  my $falses = 0;
  while( <MACOSALL> ) {
    if ( /\{\$IF/i ) {
#      print STDERR $falses ? 0 : 1, " $trues $falses $_";
      if ( $falses > 0 ) {
        DoIf( 0 ); # does not matter, will be skipped anyway
      } elsif ( /\{\$IFC\s+(.+)\}/i ) {
        DoIf( GetVal( $1 ) );
      } elsif ( /\{\$IFNDEF\s+(\w+)\s*\}/i ) {
        DoIf( GetVal( "not defined $1" ) );
      } elsif ( /\{\$IFDEF\s+(\w+)\s*\}/i ) {
        DoIf( GetVal( "defined $1" ) );
      } else {
        die "Unknow \$IF conditional $_";
      }
#      print STDERR $falses ? 0 : 1, " $trues $falses\n";
    } elsif ( /\{\$ELSE/i ) {
#      print STDERR $falses ? 0 : 1, " $trues $falses $_";
      DoElse();
#      print STDERR $falses ? 0 : 1, " $trues $falses\n";
    } elsif ( /\{\$ENDC/i ) {
#      print STDERR $falses ? 0 : 1, " $trues $falses $_";
      if ( $falses ) {
        $falses--;
      } elsif ( $trues ) {
        $trues--;
      } else {
        die "Mismatched \$endc conditional";
      }
#      print STDERR $falses ? 0 : 1, " $trues $falses\n";
    } elsif ( $falses == 0 ) {
      if ( /\{\$definec\s+(\w+)\s+(.*)\}/i ) {
        my ( $newdefine, $val ) = ( $1, GetVal( $2 ) );
        die "$newdefine already defined" if $defines{$newdefine} && $defines{$newdefine} != $val;
        $defines{$newdefine} = $val;
#        print STDERR "$newdefine -> $val\n";
      }

			if ( m!(\w+)\s*=\s*record\b!gi ) {
				$records{$1} = 1 unless $ignorerecords{$1};
			}

      print PARSED $_;
    }
  }
  close( PARSED );
  close( MACOSALL );
  if ( $trues || $falses ) {
    die "Unclosed \$IF conditionals $trues $falses";
  }
  unlink( "$gpcmacosallfile.temp" );
  
  sub GetVal {
    my $in = shift;
    
    if ( $in =~ /^\s*not\s*\(([^)]+)\)\s*$/i ) {
      return GetVal( $1 ) ? 0 : 1;
    } elsif ( $in =~ /(.*) or (.*)/i ) {
      my ( $left, $right ) = ( $1, $2 );
      return 1 if GetVal( $left );
      return 1 if GetVal( $right );
      return 0;
    } elsif ( $in =~ /(.*) and (.*)/i ) {
      my ( $left, $right ) = ( $1, $2 );
      return 0 unless GetVal( $left );
      return 0 unless GetVal( $right );
      return 1;
    } elsif ( $in =~ /^\s*not\s+(.*)$/i ) {
      return GetVal( $1 ) ? 0 : 1;
    } elsif ( $in =~ /^\s*DEFINED\s+(\w+)\s*$/i ) {
      return defined( $defines{$1} ) ? 1 : 0;
    } elsif ( $in =~ /^\s*(TRUE|1)\s*$/i ) {
      return 1;
    } elsif ( $in =~ /^\s*(FALSE|0)\s*$/i ) {
      return 0;
    } elsif ( $in =~ /^\s*(\w+)\s*$/ ) {
      die $in unless defined( $defines{$1} );
      return $defines{$1};
    } elsif ( $in =~ /^\s*\$([A-Fa-f0-9]+)\s*$/ ) {
      return eval( $in );
    } else {
      die $in;
    }
  }
  
  sub DoIf {
    my $result = shift;
    
    if ( $falses ) { # already ignoring
      $falses++;
    } elsif ( $result ) {
      $trues++;
    } else {
      $falses++;
    }
  }
  
  sub DoElse {
    if ( $falses == 1 ) {
      $falses--;
      $trues++;
    } elsif ( $falses > 1 ) {
      # do nothing
    } else {
      die unless $trues;
      $trues--;
      $falses++;
    }
  }
}

if ( $outputtestrecord ) {

  print "Create TestRecords.pas\n";
  open( TEST, ">../TestRecords.pas" ) or die "cant open out TestRecords.pas $!";
  print TEST <<'EOM';
program TestRecords;

uses 
EOM
  print TEST '  ';
  print TEST join( ",\n  ", @allunits );
  print TEST <<'EOM';
;

begin
EOM
  foreach ( sort keys %records ) {
    print TEST "  writeln( '$_: ', sizeof($_) );\n";
  }
  print TEST <<'EOM';
  writeln( 'SInt8: ', sizeof(SInt8):1 );
  writeln( 'SInt16: ', sizeof(SInt16):1 );
  writeln( 'SInt32: ', sizeof(SInt32):1 );
  writeln( 'SInt64: ', sizeof(SInt64):1 );
  writeln( 'UInt8: ', sizeof(UInt8):1 );
  writeln( 'UInt16: ', sizeof(UInt16):1 );
  writeln( 'UInt32: ', sizeof(UInt32):1 );
  writeln( 'UInt64: ', sizeof(UInt64):1 );
  writeln( 'Float32: ', sizeof(Float32):1 );
  writeln( 'Float64: ', sizeof(Float64):1 );
  writeln( 'Float80: ', sizeof(Float80):1 );
  writeln( 'Float96: ', sizeof(Float96):1 );
  writeln( 'Str15: ', sizeof(Str15):1 );
  writeln( 'Str27: ', sizeof(Str27):1 );
  writeln( 'Str31: ', sizeof(Str31):1 );
  writeln( 'Str32: ', sizeof(Str32):1 );
  writeln( 'Str36: ', sizeof(Str36):1 );
  writeln( 'Str255: ', sizeof(Str255):1 );
  writeln( 'Str32Field: ', sizeof(Str32Field):1 );
  writeln( 'ByteParameter: ', sizeof(ByteParameter):1 );
end.
EOM

  close( TEST );

}

my $makegpccode = <<EOM;
gpc -c -gstabs -I. --automake MacOS.pas
libtool -static -o GPCMacOS.a *.o
if [ -e  $gpcmacosallfile ]; then gpc -c -gstabs -I. $gpcmacosallfile ; fi
if [ -e  $gpcstringsallfile ]; then gpc -c -gstabs -I. $gpcstringsallfile ; fi
EOM
$makegpccode =~ s!^!\t!mg;
chomp $makegpccode;
our $makegpc = $maketemplate;
$makegpc =~ s!%COMPILERNAME%!GNU Pascal! or die;
$makegpc =~ s!%COMPILECODE%!$makegpccode! or die;
$makegpc =~ s!%INTERFACEDIR%!/Developer/Pascal/GPCPInterfaces! or die;
write_file( 'Makefile', $makegpc );

if ( $gpcautomake ) {
  print "Precompile all the GPCPInterfaces\n";
  system( $makeutil, 'all' );
}

}

chdir( ".." ) or die "Cant chdir .. $!";

if ( $gpcautomake && $outputtestrecord ) {
  print "Compile TestRecords\n";
  system( $gpcutil, '-o', 'TestRecords', '-gstabs', '-IGPCPInterfaces', '--unit-path=GPCPInterfaces', '--object-path=GPCPInterfaces', 'TestRecords.pas' );
}

if ( $makemwpinterfaces ) {
  print "Create $mwpinterfaces\n";
  system( '/bin/rm', '-rf', $mwpinterfaces );
  mkdir( $mwpinterfaces, 0755 ) or die "Cant mkdir $mwpinterfaces $!";
  foreach my $file ( sort( grep( m!\.pas$!, read_dir( $gpcpinterfaces ) ) ) ) {
    next if $file =~ /^GPCString/;
    my $data = read_file( "$gpcpinterfaces/$file" );
    die "cant read $file" unless $data;
    if ( $file =~ s!^GPC!MW! ) {
      $data =~ s!^unit GPC!unit MW!m or die;
    }
    if ( $file eq 'MacOS.pas' ) {
      $data =~ s!,\s*GPCStrings!!s or die;
    }
    $file =~ s!\.pas!.p!;
    ConvertGPCtoMW( \$data ) unless $file eq 'MacOS.p';
    write_file( "$mwpinterfaces/$file", $data );
  }
  chdir( $mwpinterfaces ) or die "Cant chdir $mwpinterfaces $!";

  if ( $remakepatches ) {
    my $newpatches = "../$mwpatchesremade";
    system( '/bin/rm', '-rf', $newpatches );
    die "Delete failed" if -d $newpatches;
    mkdir( $newpatches, 0755 ) or die "Cant mkdir $newpatches $!";
    foreach my $patch ( sort( grep( m!\.patch$!, read_dir( "../$mwpatches" ) ) ) ) {
      my $file = $patch;
      $file =~ s/\.patch/\.p/;
      system( '/usr/bin/patch', '--backup', '--suffix', '.orig', "--input=../$mwpatches/$patch" );
      system( "/usr/bin/diff -au $file.orig $file >$newpatches/$patch" );
    }
  } else {
    foreach my $patch ( sort( grep( m!\.patch$!, read_dir( "../$mwpatches" ) ) ) ) {
      system( '/usr/bin/patch', '--silent', '--no-backup-if-mismatch', "--input=../$mwpatches/$patch" ); #
    }
  }
  my $pwd = `pwd`;
  chomp $pwd;
  system( 'ln', '-f', '-s', '-h', $pwd, '/Developer/Pascal/MWPInterfaces' );
	chdir( ".." ) or die "Cant chdir .. $!";
}

if ( $makefpcpinterfaces ) {
  print "Create $fpcpinterfaces\n";
  system( '/bin/rm', '-rf', $fpcpinterfaces );
  mkdir( $fpcpinterfaces, 0755 ) or die "Cant mkdir $fpcpinterfaces $!";
  foreach my $file ( sort( grep( m!\.pas$!, read_dir( $gpcpinterfaces ) ) ) ) {
    next if $file =~ /^GPCString/;
    my $data = read_file( "$gpcpinterfaces/$file" );
    die "cant read $file" unless $data;

    if ( $file =~ s!^GPC!FPC! ) {
      $data =~ s!^unit GPC!unit FPC!m or die;
    }
    if ( $file eq 'MacOS.pas' ) {
      $data =~ s!,\s*GPCStrings!!s or die;
      $data = $fpcsetupconditionals.$data;
    }
    $file =~ s!\.pas!.p!;
    ConvertGPCtoFPC( \$data ) unless $file eq 'MacOS.p';
    write_file( "$fpcpinterfaces/$file", $data );
  }
  chdir( $fpcpinterfaces ) or die "Cant chdir $fpcpinterfaces $!";

  if ( $remakepatches ) {
    my $newpatches = "../$fpcpatchesremade";
    system( '/bin/rm', '-rf', $newpatches );
    die "Delete failed" if -d $newpatches;
    mkdir( $newpatches, 0755 ) or die "Cant mkdir $newpatches $!";
    foreach my $patch ( sort( grep( m!\.patch$!, read_dir( "../$fpcpatches" ) ) ) ) {
      my $file = $patch;
      $file =~ s/\.patch/\.p/;
      system( '/usr/bin/patch', '--backup', '--suffix', '.orig', "--input=../$fpcpatches/$patch" );
      system( "/usr/bin/diff -au $file.orig $file >$newpatches/$patch" );
    }
  } else {
    foreach my $patch ( sort( grep( m!\.patch$!, read_dir( "../$fpcpatches" ) ) ) ) {
      system( '/usr/bin/patch', '--silent', '--no-backup-if-mismatch', "--input=../$fpcpatches/$patch" ); #
    }
  }

	my $fpcmacosallfile = $gpcmacosallfile;
	$fpcmacosallfile =~ s!^GPC(.*)\.pas!FPC$1.p! or die;
	my $makefpccode = <<EOM;
fpc -vw0 -Si MacOS.p
libtool -static -o FPCMacOS.a *.o
if [ -e  $fpcmacosallfile ]; then fpc -vw0 -Si $fpcmacosallfile ; fi
EOM
	$makefpccode =~ s!^!\t!mg;
	chomp $makefpccode;
	our $makefpc = $maketemplate;
	$makefpc =~ s!%COMPILERNAME%!Free Pascal! or die;
	$makefpc =~ s!%COMPILECODE%!$makefpccode! or die;
	$makefpc =~ s!%INTERFACEDIR%!/Developer/Pascal/FPCPInterfaces! or die;
	write_file( 'Makefile', $makefpc );

	if ( $fpcautomake ) {
		print "Precompile all the FPCPInterfaces\n";
  	system( $makeutil, 'all' );
	}

	chdir( ".." ) or die "Cant chdir .. $!";

}

if ( $makemppinterfaces ) {
  print "Create $mppinterfaces\n";
  system( '/bin/rm', '-rf', $mppinterfaces );
  mkdir( $mppinterfaces, 0755 ) or die "Cant mkdir $mppinterfaces $!";
  foreach my $file ( sort( grep( m!\.pas$!, read_dir( $gpcpinterfaces ) ) ) ) {
    next if $file =~ /^GPCString/;
    my $data = read_file( "$gpcpinterfaces/$file" );
    die "cant read $file" unless $data;

    if ( $file =~ s!^GPC!MP! ) {
    	next;
    }
    if ( $file eq 'MacOS.pas' ) {
    	next;
    }
    ConvertGPCtoMP( \$data ) unless $file eq 'MacOS.p';
    write_file( "$mppinterfaces/$file", $data );
  }
  chdir( $mppinterfaces ) or die "Cant chdir $mppinterfaces $!";

  if ( $remakepatches ) {
    my $newpatches = "../$mppatchesremade";
    system( '/bin/rm', '-rf', $newpatches );
    die "Delete failed" if -d $newpatches;
    mkdir( $newpatches, 0755 ) or die "Cant mkdir $newpatches $!";
    foreach my $patch ( sort( grep( m!\.patch$!, read_dir( "../$mppatches" ) ) ) ) {
      my $file = $patch;
      $file =~ s/\.patch/\.p/;
      system( '/usr/bin/patch', '--backup', '--suffix', '.orig', "--input=../$mppatches/$patch" );
      system( "/usr/bin/diff -au $file.orig $file >$newpatches/$patch" );
    }
  } else {
    foreach my $patch ( sort( grep( m!\.patch$!, read_dir( "../$mppatches" ) ) ) ) {
      system( '/usr/bin/patch', '--silent', '--no-backup-if-mismatch', "--input=../$mppatches/$patch" ); #
    }
  }

	chdir( ".." ) or die "Cant chdir .. $!";

}

SetTextCFTypes( 'R*ch', $gpcpinterfaces );
SetTextCFTypes( 'R*ch', $fpcpinterfaces );
SetTextCFTypes( 'CWIE', $mwpinterfaces );
SetTextCFTypes( 'R*ch', $mppinterfaces );
SetTextCFTypes( 'R*ch', $patchesremade );
SetTextCFTypes( 'R*ch', $mwpatchesremade );
SetTextCFTypes( 'R*ch', $fpcpatchesremade );
SetTextCFTypes( 'R*ch', $mppatchesremade );

if ( $gpcautomake && $removegpcobjects ) {
  print "Remove GPCPInterfaces Objects\n";
	chdir( $gpcpinterfaces ) or die "Cant chdir $gpcpinterfaces $!";
  system( $makeutil, 'clean' );
	chdir( ".." ) or die "Cant chdir .. $!";
}

if ( $fpcautomake && $removefpcobjects ) {
  print "Remove FPCPInterfaces Objects\n";
	chdir( $fpcpinterfaces ) or die "Cant chdir $fpcpinterfaces $!";
  system( $makeutil, 'clean' );
	chdir( ".." ) or die "Cant chdir .. $!";
}

print "All Done\n";

sub SetTextCFTypes {
	my $creator = shift;
	my $directrory = shift;
	
	return unless -d $directrory;

	if ( ! -x $cpisetcftypes && ($gpcautomake || $fpcautomake) ) {
	
		open( OUT, '>', "$cpisetcftypes.pas" );
		print OUT <<EOM;
program $cpisetcftypes;

{
	Author: Peter N Lewis <peter\@stairways.com.au>
	License: Public Domain (free as in really free)
	Usage: $cpisetcftypes creator type file1 file2 ...' );

	Compile with: gpc -s -o $cpisetcftypes $cpisetcftypes.pas --uses=MacOSAll,GPCStringsAll --include=GPCPInterfaces/GPCMacros.inc  -Wl,-framework,Carbon --unit-path=GPCPInterfaces
	Compile with: fpc -vw0 -Si $cpisetcftypes.pas -FaMacOSAll -k-framework -kCarbon -FuFPCPInterfaces
}

	function StringToFourCharCode( const s: String ): FourCharCode;
	begin
		// Assert( Length(s) = 4 );
		StringToFourCharCode := (Ord(s[1]) shl 24) or (Ord(s[2]) shl 16) or (Ord (s[3]) shl 8) or Ord(s[4]);
	end;
	
	var
		err: OSStatus;
		ref: FSRef;
		i: Integer;
		ci: FSCatalogInfo;
		s: String[255];
begin
	if ParamCount >= 3 then begin
		for i := 3 to ParamCount do begin
			s := ParamStr( i ) + Chr(0);
			err := FSPathMakeRef( CStringPtr( \@s[1] ), ref, nil );
			if err = noErr then begin
				err := FSGetCatalogInfo( ref, kFSCatInfoFinderInfo, \@ci, nil, nil, nil );
			end;
			if err = noErr then begin
				FInfo(ci.finderInfo).fdCreator := StringToFourCharCode( ParamStr( 1 ) );
				FInfo(ci.finderInfo).fdType := StringToFourCharCode( ParamStr( 2 ) );
				err := FSSetCatalogInfo( ref, kFSCatInfoFinderInfo, ci );
			end;
			if err <> noErr then begin
				WriteLn( 'Failed on ', ParamStr( i ), ' with error ', err );
			end;
		end;
	end;
end.
EOM
  	print "Compile cpisetcftypes\n";
  	if ( $gpcautomake ) {
			system( $gpcutil, '-s', '-o', $cpisetcftypes, "$cpisetcftypes.pas", '--uses=MacOSAll,GPCStringsAll', '--include=GPCPInterfaces/GPCMacros.inc', '-Wl,-framework,Carbon', '--unit-path=GPCPInterfaces' );
		} else {
			system( $fpcutil, '-vw0', '-Si', "$cpisetcftypes.pas", '-FaMacOSAll', '-k-framework', '-kCarbon', '-FuFPCPInterfaces' );
		}
	}
	
	return unless -x $cpisetcftypes;

	chdir( $directrory ) or die "Cant chdir $directrory $!";
	system( "../$cpisetcftypes", $creator, 'TEXT', grep( m!\.(?:p|pas|patch|inc)?(?:.orig)?$! || m!^Makefile$!, read_dir( '.' ) ) );
	chdir( '..' ) or die "Cant chdir .. $!";
}

sub ConvertGPCtoMP {
  my ( $data ) = @_;

	$$data =~ s!$interfacecomment!!;
	$$data =~ s!$conditionals!!;
	$$data =~ s!\ninterface\s*uses !interface\nuses !;
	$$data =~ s!\Q$gpcsetupconditionals\E!! or die 'missing gpcsetupconditionals'.$$data;

  $$data =~ s!\{\$maximum-field-alignment=16\}!{\$ALIGN MAC68K}!g;
  $$data =~ s!\{\$maximum-field-alignment=32\}!{\$ALIGN POWER}!g;

  $$data =~ s!;\s*attribute\s*\(\s*const\s*\);!; (* attribute const *)!g;

  $$data =~ s!\{\n\}\n!!g;

  $$data =~ s!\Q(*const*) var\E\b!const var!g;
  $$data =~ s!\b\Qconst (*var*)\E!const var!g;

  $$data =~ s!\bUnivPtr\b!univ Ptr!g;
  $$data =~ s!\bUnivHandle\b!univ Handle!g;

  $$data =~ s!;\s*external\s+name\s*'[^']*';!;!g;

  $$data =~ s|\{ (\w+) = CFSTR\("([^"']*)"\); defined in GPCMacros.inc \}|const $1 = CFSTR( '$2' );|g;
}

sub ConvertGPCtoMW {
  my ( $data ) = @_;
 
	$$data =~ s!$interfacecomment!$mwinterfacecomment!;
	$$data =~ s!\Q$gpcsetupconditionals\E!! or die 'missing gpcsetupconditionals'.$$data;
	
	ConvertGPCtoMWFPC( $data );

  $$data =~ s!\bUnivPtr\b!univ Ptr!g;
  $$data =~ s!\bUnivHandle\b!univ Handle!g;
  
  $$data =~ s!\n(\s*var \w+: \w+;)\s*external\s+name\s*'[^']*';(.*)$!\n{\$J+}\n$1$2\n{\$J-}!gm;
  $$data =~ s!{\$J-}\n{\$J\+}\n!!g;
  
  $$data =~ s!;\s*external\s+name\s*'[^']*';!;!g;
  
  $$data =~ s!\bprotected var(\s+\w+\s*(?:,\s*\w+\s*)*:\s*(\w+))!($mwbadconst{$2} ? '(*const*) var' : 'const (*var*)').$1!ge;

  $$data =~ s!(definec TARGET_RT_MAC_MACHO\s+)TRUE!${1}FALSE!i;
  $$data =~ s!(definec TARGET_RT_MAC_CFM\s+)FALSE!${1}TRUE!i;
  
  $$data =~ s!(definec FOUR_CHAR_CODE\(s\))[^}]*!$1 (s)!i;
  
  $$data =~ s!(?<=\(| )(\$[A-Fa-f0-9]+|\d+) shl (\d+)(?=\)|;)!$2 <= 15 ? "$1 shl $2" : "UInt32($1) shl $2"!ge;
  
  $$data =~ s|\{ (\w+) = CFSTR\("([^"']*)"\); defined in GPCMacros.inc \}|{\$ifc USE_CFSTR_CONSTANT_MACROS}\n{\$definec $1 CFSTRP('$2')}\n{\$endc}|g;

	$$data =~ s!\{MW-ONLY-START\n!!;
	$$data =~ s!mwimplementation!implementation!;
	$$data =~ s!MW-ONLY-FINISH\}\n!!;
}


sub ConvertGPCtoFPC {
  my ( $data ) = @_;
 
	$$data =~ s!\Q$interfacecomment\E!$fpcinterfacecomment! or die 'missing interfacecomment'.$$data;
	$$data =~ s!\Q$gpcsetupconditionals\E!$fpcsetupconditionals! or die 'missing gpcsetupconditionals'.$$data;
	
	ConvertGPCtoMWFPC( $data );
	
	$$data =~ s!\{\$definec (\w+)\s+(.*)\}!{\$setc $1 := $2}!g;

	$$data =~ s!\{\$definec FOUR_CHAR_CODE.*\}\n!!mg;
  $$data =~ s!FOUR_CHAR_CODE\('([^']{4})'\)!sprintf( q#$%08X (* '%s' *)#, unpack( "N", $1 ), $1 )!mge;

  $$data =~ s!(;\s*external\s+name\s*')([^']*';)!${1}_${2}!g;

  $$data =~ s|\{ (\w+) = CFSTR\("([^"']*)"\); defined in GPCMacros.inc \}|{\$ifc USE_CFSTR_CONSTANT_MACROS}\n{\$definec $1 CFSTRP('$2')}\n{\$endc}|g;

  $$data =~ s!^\s*const\s+(\w+)\s*=\s*"([^"']*)";!const $1 = '$2';!gm;
  $$data =~ s!\bprotected var(\s+\w+\s*(?:,\s*\w+\s*)*:\s*(\w+))!($fpcbadconst{$2} ? '(*const*) var' : 'const (*var*)').$1!ge;
  
  $$data =~ s!\{\$ifc defined !{\$ifc not undefined !gi;
  $$data =~ s!\{\$ifc not defined !{\$ifc undefined !gi;

  $$data =~ s!(\n[ \t]*\w+[ \t]*=[ \t]*)"([^"\n]*)";!$1'$2';!gm;

}

sub ConvertGPCtoMWFPC {
  my ( $data ) = @_;

  $$data =~ s!\{\$maximum-field-alignment=16\}!{\$ALIGN MAC68K}!g;
  $$data =~ s!\{\$maximum-field-alignment=32\}!{\$ALIGN POWER}!g;

  $$data =~ s!;\s*attribute\s*\(\s*ignorable\s*\);!; (* attribute ignoreable *)!g;
  $$data =~ s!;\s*attribute\s*\(\s*const\s*\);!; (* attribute const *)!g;

  $$data =~ s!\babstract object end;!SInt32;!g;
  $$data =~ s!\babstract object\(\w+\) end;!SInt32;!g;

	$$data =~ s!(\bunit \w+;\ninterface\n)!$1$defineusecfstr\n! or die "Cant insert conditionals";

  $$data =~ s!\{\$ifc not defined !{\$ifc undefined !gi;

  $$data =~ s!\{\n\}\n!!g;

  $$data =~ s!\Q(*const*) var\E\b!protected var!g;
  $$data =~ s!\b\Qconst (*var*)\E!protected var!g;

  return $data;
}

sub RemoveString {
  my $pos = index( $_[0], $_[1] );
  if ( $pos >= 0 ) {
    substr( $_[0], $pos, length($_[1]) ) = '';
  }
}

sub closure {
  my( $map ) = @_;
  
  my @elements = sort keys %{$map};
  
  my $finished;
  do {
    $finished = 1;
    foreach my $elem (@elements) {
      my $newval = ','.$map->{$elem}.',';
      foreach my $uses (split( /,/, $map->{$elem})) {
        foreach my $ituses (split( /,/, $map->{$uses})) {
          if ( $newval !~ /,$ituses,/ ) {
            $newval .= $ituses.',';
            $finished = 0;
          }
        }
      }
      $newval =~ s/,,/,/g;
      $newval =~ s/^,+//;
      $newval =~ s/,+$//;
      $map->{$elem} = $newval;
    }
  } until $finished;
}

sub setup_string_extras {

  my( $a, $b );

  my @allsizes = (15, 27, 31, 32, 36, 63, 255);
  my @sizes = (15, 27, 31, 32, 63, 255);
  my @sizessizes;
  
  foreach $a (@sizes) {
    push @sizessizes, map( "$a $_", @sizes );
  }

# 6   type StrN
# 6   type StringN
# 6   StrNLength( StrN ): Integer;
# 6   SetStrNLength( StrN, len ): Boolean;
# 36  StrN + StrM -> Str255
# 6   StrN + String -> Str255
# 6   String + StrN -> Str255
# 36  StrN = StrM
# 6   StrN = String
# 6   String = StrN
# 36  StrN < StrM
# 36  StrN <= StrM
# 36  StrN > StrM
# 36  StrN >= StrM
# 36  StrNToStrM( StrN ): StrM;
# 6   StringToStrN( String ): StrN;
# 36  StrNIntoStrM( StrN, StrM );
# 6   StringIntoStrN( String, StrN );
# 6   StrNToString( StrN ): StringN;
  
  $stringtypes = '';
  $stringinterface = '';
  $stringimplementation = '';
  
# 6   type StrN
# 6   type StringN

  $stringtypes .= <<'EOM';
  TYPE
EOM
  for (@allsizes) {
    $stringtypes .= <<EOM;
	Str$_				= record
					    sLength: UInt8;
					    sChars: packed array[1..$_] of char;
EOM
    $stringtypes .= <<EOM if $_ % 2 == 1;
					    sPad: Byte;
EOM
    $stringtypes .= <<EOM;
					  end;
EOM
  }
  $stringtypes .= "\n";
  
  $stringinterface .= <<'EOM';
  TYPE
EOM
  $stringinterface .= <<EOM for (@allsizes);
	String$_			= String($_);
EOM
  $stringinterface .= "\n";

# 6   StrNLength( StrN ): Integer;

  $stringinterface .= <<EOM for (@sizes);
  function Str${_}Length( protected var s: Str$_ ) = Result : Integer;
EOM
  $stringinterface .= "\n";
  
  $stringimplementation .= <<EOM for (@sizes);
  function Str${_}Length( protected var s: Str$_ ) = Result : Integer;
  begin
    Result := s.SLength;
    Assert( (0 <= Result) and (Result <= $_) );
  end;
  
EOM
  $stringimplementation .= "\n";

# 6   SetStrNLength( StrN, len ): Boolean;

  $stringinterface .= <<EOM for (@sizes);
  procedure SetStr${_}Length( var s: Str$_; len: Integer );
EOM
  $stringinterface .= "\n";

  $stringimplementation .= <<EOM foreach (@sizes);
  procedure SetStr${_}Length( var s: Str$_; len: Integer );
  begin
    Assert( (0 <= len) and (len <= $_) );
    s.sLength := Min( Max( len, 0 ), $_ );
  end;
  
EOM
  $stringimplementation .= "\n";

# 36  StrN + StrM -> Str255
  ($a,$b) = split, $stringinterface .= <<EOM for (@sizessizes);
  operator + ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Str255;
EOM
  $stringinterface .= "\n";

  $stringimplementation .= <<'EOM';
  procedure PrivAddStr255( protected var s1in; protected var s2in; var Result: Str255 );
    var
      s1, s2: StringPtr;
      l1, l2: Integer;
  begin
    {$local W-} s1 := @s1in; {$endlocal}
    {$local W-} s2 := @s2in; {$endlocal}
    l1 := s1^.sLength;
    l2 := Min( 255 - l1, s2^.sLength );
    Result.sLength := l1 + l2;
    if l1 > 0 then begin
      Result.sChars[1..l1] := s1^.sChars[1..l1];
    end;
    if l2 > 0 then begin
      Result.sChars[l1+1..l1+l2] := s2^.sChars[1..l2];
    end;
  end;
  
EOM
  ($a,$b) = split, $stringimplementation .= <<EOM for (@sizessizes);
  operator + ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Str255;
  begin
    PrivAddStr255( s1, s2, Result );
  end;

  
EOM
  $stringimplementation .= "\n";

# Sadly String cannot be used as an operator input
# Probably because it never matches exactly to String(n) for any given n
if ( 0 ) {
# 6   StrN + String -> Str255
# 6   String + StrN -> Str255
  $stringinterface .= <<EOM foreach (@sizes);
  operator + ( protected var s1: Str$_; protected var s2: String ) = Result : Str255;
  operator + ( protected var s1: String; protected var s2: Str$_ ) = Result : Str255;
EOM
  $stringinterface .= "\n";

  $stringimplementation .= <<'EOM';
  procedure PrivAddStr255String( protected var s1in; protected var s2: String; var Result: Str255 );
    var
    	count: Integer;
      s1: StringPtr;
  begin
    {$local W-} s1 := @s1in; {$endlocal}
    SetStr255Length( Result, Min( 255, Str255Length( s1^) + Length( s2 ) ) );
    count := Str255Length( s1^ );
    if count > 0 then begin
    	Result.sChars[1..count] := s1^.sChars[1..count];
    end;
    count := Str255Length( Result ) - Str255Length( s1^ );
    if count > 0 then begin
    	Result.sChars[Str255Length( s1^ ) + 1 .. Str255Length( s1^ ) + count] := s2[1..count];
    end;
  end;

  procedure PrivAddStringStr255( protected var s1: String; protected var s2in; var Result: Str255 );
    var
    	count: Integer;
      s2: StringPtr;
  begin
    {$local W-} s2 := @s2in; {$endlocal}
    SetStr255Length( Result, Min( 255, Length( s1 ) + Str255Length( s2^ ) ) );
		count := Min( 255, Length( s1 ) );
    if count > 0 then begin
    	Result.sChars[1..count] := s1[1..count];
    end;
    count := Str255Length( Result ) - Length( s1 );
    if count > 0 then begin
    	Result.sChars[Length( s1 ) + 1 .. Length( s1 ) + count] := s2^.sChars[1..count];
    end;
  end;

EOM
  $stringimplementation .= <<EOM foreach (@sizes);
  operator + ( protected var s1: Str$_; protected var s2: String ) = Result : Str255;
  begin
    PrivAddStr255String( s1, s2, Result );
  end;

  operator + ( protected var s1: String; protected var s2: Str$_ ) = Result : Str255;
  begin
    PrivAddStringStr255( s1, s2, Result );
  end;

EOM
  $stringimplementation .= "\n";
}

# 36  StrN = StrM
  ($a,$b) = split, $stringinterface .= <<EOM for (@sizessizes);
  operator = ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
EOM
  $stringinterface .= "\n";

  ($a,$b) = split, $stringimplementation .= <<EOM for (@sizessizes);
  operator = ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
  begin
    Result := s1.sLength = s2.sLength;
    if Result and (s1.sLength > 0) then begin
      Result := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

EOM
  $stringimplementation .= "\n";

# 36  StrN < StrM
# 36  StrN <= StrM
# 36  StrN > StrM
# 36  StrN >= StrM

  ($a,$b) = split, $stringinterface .= <<EOM for (@sizessizes);
  operator <  ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
  operator <= ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
  operator >  ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
  operator >= ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
EOM
  $stringinterface .= "\n";

  $stringimplementation .= <<'EOM';
  function PrivLessThanStrN( protected var s1in; protected var s2in ) = Result : Boolean;
    var
      s1, s2: StringPtr;
  begin
    {$local W-} s1 := @s1in; {$endlocal}
    {$local W-} s2 := @s2in; {$endlocal}
    if s2^.sLength = 0 then begin
      Result := false;
    end else if s1^.sLength = 0 then begin
      Result := true;
    end else if LT( s1^.sChars[1..Min( s1^.sLength, s2^.sLength )], s2^.sChars[1..Min( s1^.sLength, s2^.sLength )] ) then begin
      Result := true;
    end else if (s1^.sLength < s2^.sLength) and EQ( s1^.sChars[1..Min( s1^.sLength, s2^.sLength )], s2^.sChars[1..Min( s1^.sLength, s2^.sLength )] ) then begin
      Result := true;
    end else begin
      Result := false;
    end;
  end;
  
  function PrivLessThanOrEqualStrN( protected var s1in; protected var s2in ) = Result : Boolean;
    var
      s1, s2: StringPtr;
  begin
    {$local W-} s1 := @s1in; {$endlocal}
    {$local W-} s2 := @s2in; {$endlocal}
    if s1^.sLength = 0 then begin
      Result := true;
    end else if s2^.sLength = 0 then begin
      Result := false;
    end else if LT( s1^.sChars[1..Min( s1^.sLength, s2^.sLength )], s2^.sChars[1..Min( s1^.sLength, s2^.sLength )] ) then begin
      Result := true;
    end else if (s1^.sLength <= s2^.sLength) and EQ( s1^.sChars[1..Min( s1^.sLength, s2^.sLength )], s2^.sChars[1..Min( s1^.sLength, s2^.sLength )] ) then begin
      Result := true;
    end else begin
      Result := false;
    end;
  end;
  
EOM

  ($a,$b) = split, $stringimplementation .= <<EOM for (@sizessizes);
  operator <  ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
  begin
    Result := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
  begin
    Result := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
  begin
    Result := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( protected var s1: Str$a; protected var s2: Str$b ) = Result : Boolean;
  begin
    Result := PrivLessThanOrEqualStrN( s2, s1 );
  end;

EOM
  $stringimplementation .= "\n";

# Sadly String cannot be used as an operator input
# Probably because it never matches exactly to String(n) for any given n
if ( 0 ) {
# 6   StrN = String
# 6   String = StrN
  $stringinterface .= <<EOM foreach (@sizes);
  operator = ( protected var s1: Str$_; protected var s2: String ) = Result : Boolean;
  operator = ( protected var s1: String; protected var s2: Str$_ ) = Result : Boolean;
EOM
  $stringinterface .= "\n";

  $stringimplementation .= <<'EOM';
  function PrivEquStrNString( protected var s1in; protected var s2: String ) = Result : Boolean;
    var
      i, len: Integer;
      s1: StringPtr;
  begin
    {$local W-} s1 := @s1in; {$endlocal}
    len := Str255Length( s1^ );
    Result := len = Length( s2 );
    i := 1;
    while Result and (i <= len) do begin
      Result := s1^.sChars[i] = s2[i];
      Inc(i);
    end;
  end;

EOM
  $stringimplementation .= <<EOM foreach (@sizes);
  operator = ( protected var s1: Str$_; protected var s2: String ) = Result : Boolean;
  begin
    Result := PrivEquStrNString( s1, s2 );
  end;

  operator = ( protected var s1: String; protected var s2: Str$_ ) = Result : Boolean;
  begin
    Result := PrivEquStrNString( s2, s1 );
  end;

EOM
  $stringimplementation .= "\n";
}

# 36  StrNToStrM( StrN ): StrM;
# 36  StrNIntoStrM( StrN, StrM );
  ($a,$b) = split, $stringinterface .= <<EOM for (@sizessizes);
  function Str${a}ToStr${b} ( protected var s: Str$a ) = Result : Str$b;
EOM
  $stringinterface .= "\n";

  ($a,$b) = split, $stringinterface .= <<EOM for (@sizessizes);
  procedure Str${a}IntoStr${b} ( protected var s: Str$a; var Result: Str$b );
EOM
  $stringinterface .= "\n";

  ($a,$b) = split, $stringimplementation .= <<EOM for (@sizessizes);
  function Str${a}ToStr${b} ( const s: Str$a ) = Result : Str$b;
  begin
    Result.sLength := Min( s.sLength, $b );
    if Result.sLength > 0 then begin
      Result.sChars[1..Result.sLength] := s.sChars[1..Result.sLength];
    end;
  end;

EOM
  $stringimplementation .= "\n";
  ($a,$b) = split, $stringimplementation .= <<EOM for (@sizessizes);
  procedure Str${a}IntoStr${b} ( const s: Str$a; var Result: Str$b );
  begin
    Result.sLength := Min( s.sLength, $b );
    if Result.sLength > 0 then begin
      Result.sChars[1..Result.sLength] := s.sChars[1..Result.sLength];
    end;
  end;

EOM
  $stringimplementation .= "\n";

# 6   StringToStrN( String ): StrN;
# 6   StringIntoStrN( String, StrN );
  $stringinterface .= <<EOM for (@sizes);
  function StringToStr$_( const s: String ) = Result : Str$_;
  procedure StringIntoStr$_( const s: String; var Result: Str$_ );
EOM
  $stringinterface .= "\n";

  $stringimplementation .= <<EOM for (@sizes);
  function StringToStr$_( const s: String ) = Result : Str$_;
  begin
    Result.sLength := Min( Length( s ), $_ );
    if Result.sLength > 0 then begin
      Result.sChars[1..Result.sLength] := s[1..Result.sLength];
    end;
  end;

  procedure StringIntoStr$_( const s: String; var Result: Str$_ );
  begin
    Result.sLength := Min( Length( s ), $_ );
    if Result.sLength > 0 then begin
      Result.sChars[1..Result.sLength] := s[1..Result.sLength];
    end;
  end;

EOM
  $stringimplementation .= "\n";

# 6   StrNToString( StrN ): StringN;
  $stringinterface .= <<EOM for (@sizes);
  function Str${_}ToString( protected var s: Str$_ ) = Result : String$_;
EOM

  $stringimplementation .= <<EOM for (@sizes);
  function Str${_}ToString( protected var s: Str$_ ) = Result : String$_;
  begin
    SetLength( Result, Min( s.sLength, Result.Capacity ) );
    if Length( Result ) > 0 then begin
      Result[1..Length( Result )] := s.sChars[1..Length( Result )];
    end;
  end;

EOM
  $stringimplementation .= "\n";

	$stringinterface =~ s!\s*\Z!!s;
	$stringimplementation =~ s!\s*\Z!!s;
}

sub load_data {
  my $infixparam = 0;
  my $inaddconstants = 0;
  my $incftypes = 0;
  my $infixdirid = 0;
  my $inextraunits = 0;
  my $innomactypes = 0;
  my $inignorerecords = 0;
  my $inkillunits = 0;
  my $inbadconst = 0;
  my $infixunitnames = 0;
  my $infixmwerks = 0;
  while ( <DATA> ) {
    chomp;
    next if /^#/ | /^$/;
    if ( /^[A-Z]+$/ ) {
      $infixparam = $inaddconstants = $incftypes = $infixdirid = $inextraunits = $innomactypes = $inignorerecords = $inkillunits = $inbadconst = $infixunitnames = $infixmwerks = 0;
      $infixparam = 1, next if $_ eq 'FIXPARAM';
      $inaddconstants = 1, next if $_ eq 'ADDCONSTANTS';
      $incftypes = 1, next if $_ eq 'CFTYPES';
      $infixdirid = 1, next if $_ eq 'FIXDIRID';
      $inextraunits = 1, next if $_ eq 'EXTRAUNITS';
      $innomactypes = 1, next if $_ eq 'NOMACTYPES';
      $inignorerecords = 1, next if $_ eq 'IGNORERECORDS';
      $inkillunits = 1, next if $_ eq 'KILLUNITS';
      $inbadconst = 1, next if $_ eq 'BADCONST';
      $infixunitnames = 1, next if $_ eq 'FIXUNITNAMES';
      $infixmwerks = 1, next if $_ eq 'FIXMWERKS';
      die $_;
    } elsif ( $infixparam ) {
      die $_ unless /^([^#]+\.p)#([^#]+#[^#]+#[^#]+)$/;
      $fixparam{$1} = [] unless $fixparam{$1};
      push @{$fixparam{$1}}, $2;
    } elsif ( $inaddconstants ) {
      die $_ unless /^([^#]+\.p)#([^#]+#[^#]+)$/;
      $addconstants{$1} = [] unless $addconstants{$1};
      push @{$addconstants{$1}}, $2;
    } elsif ( $incftypes ) {
      die $_ unless /^([^#]+\.p)#([^#]+)$/;
      $cftypes{$1} = [] unless $cftypes{$1};
      push @{$cftypes{$1}}, $2;
    } elsif ( $infixdirid ) {
      die $_ unless /^(\w+\.p)$/;
      $fixdirid{$1} = 1;
    } elsif ( $innomactypes ) {
      die $_ unless /^(\w+\.p)$/;
      $nomactypes{$1} = 1;
    } elsif ( $inignorerecords ) {
      die $_ unless /^(\w+)$/;
      $ignorerecords{$1} = 1;
    } elsif ( $inextraunits ) {
      die $_ unless /^([^#]+\.p)#([\w,]+)$/;
#      die $_ if $extraunits{$1};
      if ( $extraunits{$1} ) { # Support this for now, and join them together later
        $extraunits{$1} .= ",$2";
      } else {
        $extraunits{$1} = $2;
      }
    } elsif ( $inkillunits ) {
      die $_ unless /^(\w+\.p)$/;
      $killunits{$1} = 1;
    } elsif ( $inbadconst ) {
      die $_ unless /^(\w+): (GPC)?,?(MW)?,?(FPC)?$/;
      $gpcbadconst{$1} = 1 if $2;
      $mwbadconst{$1} = 1 if $3;
      $fpcbadconst{$1} = 1 if $4;
    } elsif ( $infixunitnames ) {
      die $_ unless /^(\w+)$/;
      $fixunitnames{$1} = "${1}s";
      $fixunitnames{"$1.p"} = "${1}s.p";
      $fixunitnames{"$1.pas"} = "${1}s.pas";
    } elsif ( $infixmwerks ) {
      $fixmwerks{$_} = 1;
    } else {
      die $_;
    }
  }

  if ( $outputextraunits ) {
    foreach (sort keys(%extraunits)) {
      print "$_#$extraunits{$_}\n";
    }
  }
}

sub write_file {
  my ($file, $data) = @_;
  my $result = undef;
  if ( open( my $fh, ">$file" ) ) {
    print $fh $data;
    close( $fh );
    $result = 1;
  }
  return $result;
}

sub read_file {
  my ($file) = @_;
  my $result = undef;
  local( $/ ) = undef;
  if ( open( my $fh, $file ) ) {
    $result = <$fh>;
    close( $fh );
  }
  return $result;
}

sub read_dir {
  my( $dir ) = @_;
  
  opendir( DIR, $dir ) or die "cant open dir $dir $!";
  my @dirs = readdir( DIR );
  closedir( DIR );
  grep ( s!.*/!!, @dirs );
  @dirs = grep ( !m!^\.!, @dirs );
  @dirs = sort( @dirs );

  return @dirs;
}

sub unmount {
  my ($vol) = @_;
  
  my $disk = `df "$vol"`;
  $disk =~ s!^.*\n(/\S+).*$volume\n.*!$1!s or die "weird df result $disk";

  system( '/usr/bin/hdiutil', 'eject', '-quiet', $disk );
}

sub wait_on_existence {
  my ( $exists, $file ) = @_;
  
  foreach ( 1..20 ) {
    return if ($exists ? -e $file : !-e $file);
    sleep( 1 );
  }
  die $exists ? "$file wont appear" : "$file wont go away";
}

__DATA__
FIXUNITNAMES
CGAffineTransform
CGError
CGWindowLevel
FIXPARAM
AEDataModel.p#AEGetNthPtr#var theAEKeyword: AEKeyword#theAEKeywordPtr: AEKeywordPtr
AEDataModel.p#AEGetNthPtr#var typeCode: DescType#typeCode: DescTypePtr
AEDataModel.p#AEGetNthPtr#var actualSize: Size#actualSize: SizePtr
AEDataModel.p#AEGetNthDesc#var theAEKeyword: AEKeyword#theAEKeywordPtr: AEKeywordPtr
AEDataModel.p#AESizeOfNthItem#var typeCode: DescType#typeCode: DescTypePtr
AEDataModel.p#AESizeOfNthItem#var dataSize: Size#dataSize: SizePtr
# This seems entirely pointless to be nil AEDataModel.p#AECreateAppleEvent#protected var target: AEAddressDesc#target: AEAddressDescPtr
AEDataModel.p#AEGetParamPtr#var typeCode: DescType#actualType: DescTypePtr
AEDataModel.p#AEGetParamPtr#var actualSize: Size#actualSize: SizePtr
AEDataModel.p#AESizeOfParam#var typeCode: DescType#typeCode: DescTypePtr
AEDataModel.p#AESizeOfParam#var dataSize: Size#dataSize: SizePtr
AEDataModel.p#AEGetAttributePtr#var typeCode: DescType#typeCode: DescTypePtr
AEDataModel.p#AEGetAttributePtr#var actualSize: Size#actualSize: SizePtr
AEDataModel.p#AESizeOfAttribute#var typeCode: DescType#typeCode: DescTypePtr
AEDataModel.p#AESizeOfAttribute#var dataSize: Size#dataSize: SizePtr
Appearance.p#GetTextAndEncodingFromCFString#var outEncoding: TextEncoding#outEncoding: TextEncodingPtr
Appearance.p#TruncateThemeText#var outTruncated: boolean#outTruncated: BooleanPtr
ATSFont.p#ATSFontGetFontFamilyResource#var oSize: ByteCount#oSize: ByteCountPtr
ATSFont.p#ATSFontGetTable#var oSize: ByteCount#oSize: ByteCountPtr
ATSFont.p#ATSFontGetTableDirectory#var oSize: ByteCount#oSize: ByteCountPtr
CarbonEvents.p#AddEventTypesToHandler#protected var inList: EventTypeSpec#inList: EventTypeSpecPtr
CarbonEvents.p#FlushEventsMatchingListFromQueue#protected var inList: EventTypeSpec#inList: EventTypeSpecPtr
CarbonEvents.p#GetEventParameter#var outActualSize: UInt32#outActualSize: UInt32Ptr
CarbonEvents.p#GetEventParameter#var outActualType: EventParamType#outActualType: EventParamTypePtr
CarbonEvents.p#InstallEventHandler#protected var inList: EventTypeSpec#inList: EventTypeSpecPtr
CarbonEvents.p#ReceiveNextEvent#protected var inList: EventTypeSpec#inList: EventTypeSpecPtr
CarbonEvents.p#RegisterToolboxObjectClass#protected var inEventList: EventTypeSpec#inEventList: EventTypeSpecPtr
CarbonEvents.p#RemoveEventTypesFromHandler#protected var inList: EventTypeSpec#inList: EventTypeSpecPtr
CarbonEvents.p#SetMouseCoalescingEnabled#var outOldState: boolean#outOldState: BooleanPtr
CarbonEvents.p#TrackMouseLocationWithOptions#var outModifiers: UInt32#outModifiers: UInt32Ptr
CFArray.p#CFArrayCreate#var values: UNIV Ptr#values: Ptr
CFBundle.p#CFBundleGetPackageInfo#var packageCreator: UInt32#var packageCreator: OSType
CFBundle.p#CFBundleGetPackageInfo#var packageType: UInt32#var packageType: OSType
CFData.p#CFDataAppendBytes#protected var bytes: UInt8#bytes: UnivPtr
CFData.p#CFDataCreate#protected var bytes: UInt8#bytes: UnivPtr
CFData.p#CFDataCreateWithBytesNoCopy#protected var bytes: UInt8#bytes: UnivPtr
CFData.p#CFDataGetBytes#var buffer: UInt8#buffer: UnivPtr
CFData.p#CFDataReplaceBytes#protected var newBytes: UInt8#newBytes: UnivPtr
CFPropertyList.p#CFPropertyListCreateFromXMLData#var errorString: CFStringRef#errorString: CFStringRefPtr
CFString.p#CFStringAppendCharacters#protected var chars: UniChar#chars: UniCharPtr
CFString.p#CFStringCreateMutableWithExternalCharactersNoCopy#var chars: UniChar#chars: UniCharPtr
CFString.p#CFStringCreateWithCharacters#protected var chars: UniChar#chars: UniCharPtr
CFString.p#CFStringCreateWithCharactersNoCopy#protected var chars: UniChar#chars: UniCharPtr
CFString.p#CFStringGetBytes#var buffer: UInt8#buffer: UInt8Ptr
CFString.p#CFStringGetCharacters#var buffer: UniChar#buffer: UniCharPtr
CFString.p#CFStringSetExternalCharactersNoCopy#var chars: UniChar#chars: UniCharPtr
CFURL.p#CFURLCreateFromFileSystemRepresentation#protected var buffer: UInt8#buffer: UInt8Ptr
CFURL.p#CFURLCreateFromFileSystemRepresentationRelativeToBase#protected var buffer: UInt8#buffer: UInt8Ptr
CFURL.p#CFURLCreateWithBytes#protected var URLBytes: UInt8#URLBytes: UInt8Ptr
CFURL.p#CFURLGetFileSystemRepresentation#var buffer: UInt8#buffer: CStringPtr
CFURLAccess.p#CFURLCreateDataAndPropertiesFromResource#var properties: CFDictionaryRef#properties: CFDictionaryRefPtr
CFURLAccess.p#CFURLCreateDataAndPropertiesFromResource#var resourceData: CFDataRef#resourceData: CFDataRefPtr
CGColorSpace.p#CGColorSpaceCreateCalibratedGray#protected var blackPoint: Single#protected var blackPoint: TristimulusValue
CGColorSpace.p#CGColorSpaceCreateCalibratedGray#protected var whitePoint: Single#protected var whitePoint: TristimulusValue
CGColorSpace.p#CGColorSpaceCreateCalibratedRGB#protected var blackPoint: Single#protected var blackPoint: TristimulusValue
CGColorSpace.p#CGColorSpaceCreateCalibratedRGB#protected var gamma: Single#protected var gamma: RedGreenBlueValue
CGColorSpace.p#CGColorSpaceCreateCalibratedRGB#protected var matrix: Single#protected var matrix: Single9
CGColorSpace.p#CGColorSpaceCreateCalibratedRGB#protected var whitePoint: Single#protected var whitePoint: TristimulusValue
CGColorSpace.p#CGColorSpaceCreateIndexed#protected var colorTable: UInt8#colorTable: UInt8Ptr
CGColorSpace.p#CGColorSpaceCreateLab#protected var blackPoint: Single#protected var blackPoint: TristimulusValue
CGColorSpace.p#CGColorSpaceCreateLab#protected var whitePoint: Single#protected var whitePoint: TristimulusValue
CGColorSpace.p#CGColorSpaceCreateLab#protected var range: Single#protected var range: Single4
CGContext.p#CGContextSetStrokeColor#protected var components: Single#components: SinglePtr
CGContext.p#CGContextSetStrokePattern#protected var components: Single#components: SinglePtr
CGContext.p#CGContextSetLineDash#protected var lengths: Single#lengths: SinglePtr
CGContext.p#CGContextSetFillColor#protected var components: Single#components: SinglePtr
CGContext.p#CGContextSetFillPattern#protected var components: Single#components: SinglePtr
CGContext.p#CGContextShowGlyphs#protected var g: CGGlyph#g: CGGlyphPtr
CGContext.p#CGContextShowGlyphsAtPoint#protected var g: CGGlyph#g: CGGlyphPtr
CGDirectDisplay.p#CGGetActiveDisplayList#var activeDspys: CGDirectDisplayID#dspys: CGDirectDisplayIDPtr
CGDirectDisplay.p#CGGetDisplaysWithOpenGLDisplayMask#var dspys: CGDirectDisplayID#dspys: CGDirectDisplayIDPtr
CGDirectDisplay.p#CGGetDisplaysWithPoint#var dspys: CGDirectDisplayID#dspys: CGDirectDisplayIDPtr
CGDirectDisplay.p#CGGetDisplaysWithRect#var dspys: CGDirectDisplayID#dspys: CGDirectDisplayIDPtr
CGDirectDisplay.p#CGSetDisplayTransferByByteTable#protected var blueTable: CGByteValue#blueTable: CGByteValuePtr
CGDirectDisplay.p#CGSetDisplayTransferByByteTable#protected var greenTable: CGByteValue#greenTable: CGByteValuePtr
CGDirectDisplay.p#CGSetDisplayTransferByByteTable#protected var redTable: CGByteValue#redTable: CGByteValuePtr
CGDirectDisplay.p#CGSetDisplayTransferByTable#protected var blueTable: CGGammaValue#blueTable: CGGammaValuePtr
CGDirectDisplay.p#CGSetDisplayTransferByTable#protected var greenTable: CGGammaValue#greenTable: CGGammaValuePtr
CGDirectDisplay.p#CGSetDisplayTransferByTable#protected var redTable: CGGammaValue#redTable: CGGammaValuePtr
CGDirectDisplay.p#CGGetDisplayTransferByTable#var blueTable: CGGammaValue#blueTable: CGGammaValuePtr
CGDirectDisplay.p#CGGetDisplayTransferByTable#var greenTable: CGGammaValue#greenTable: CGGammaValuePtr
CGDirectDisplay.p#CGGetDisplayTransferByTable#var redTable: CGGammaValue#redTable: CGGammaValuePtr
CGImage.p#CGImageCreate#protected var decode: Single#decode: SinglePtr
CGImage.p#CGImageCreateWithJPEGDataProvider#protected var decode: Single#decode: SinglePtr
CGImage.p#CGImageMaskCreate#protected var decode: Single#decode: SinglePtr
CMApplication.p#CMSetProfileDescriptions#protected var uName: UniChar#uName: ConstUniCharPtr
CodeFragments.p#FindSymbol#var symAddr: Ptr#symAddr: symAddrPtr
CodeFragments.p#FindSymbol#var symClass: CFragSymbolClass#symClass: CFragSymbolClassPtr
CodeFragments.p#GetDiskFragment#var mainAddr: Ptr#mainAddr: mainAddrPtr
CodeFragments.p#GetIndSymbol#var symAddr: Ptr#symAddr: symAddrPtr
CodeFragments.p#GetIndSymbol#var symClass: CFragSymbolClass#symClass: CFragSymbolClassPtr
CodeFragments.p#GetMemFragment#var mainAddr: Ptr#mainAddr: mainAddrPtr
ControlDefinitions.p#AddDataBrowserItems#protected var items: DataBrowserItemID#items: DataBrowserItemIDPtr
ControlDefinitions.p#RemoveDataBrowserItems#protected var items: DataBrowserItemID#items: DataBrowserItemIDPtr
ControlDefinitions.p#SetDataBrowserColumnViewPath#protected var path: DataBrowserItemID#path: DataBrowserItemIDPtr
ControlDefinitions.p#SetDataBrowserSelectedItems#protected var items: DataBrowserItemID#items: DataBrowserItemIDPtr
ControlDefinitions.p#UpdateDataBrowserItems#protected var items: DataBrowserItemID#items: DataBrowserItemIDPtr
Controls.p#CreateRootControl#var outControl: ControlRef#outControl: ControlRefPtr
Controls.p#FindControlUnderMouse#var outPart: ControlPartCode#outPart: ControlPartCodePtr
Controls.p#GetControlData#var outActualSize: Size#outActualSize: SizePtr
Controls.p#GetControlProperty#var actualSize: UInt32#actualSize: UInt32Ptr
DateTimeUtils.p#DateString#dateTime: LONGINT#dateTime: UInt32
DateTimeUtils.p#TimeString#dateTime: LONGINT#dateTime: UInt32
Dialogs.p#GetDialogTimeout#var outButtonToPress: SInt16#outButtonToPress: SInt16Ptr
Dialogs.p#GetDialogTimeout#var outSecondsRemaining: UInt32#outSecondsRemaining: UInt32Ptr
Dialogs.p#GetDialogTimeout#var outSecondsToWait: UInt32#outSecondsToWait: UInt32Ptr
Dictionary.p#DCMGetFieldData#protected var dataTag: DCMFieldTag#dataTag: DCMFieldTagPtr
Dictionary.p#DCMGetDictionaryPropertyList#var propertyTag: DCMFieldTag#propertyTag: DCMFieldTagPtr
Dictionary.p#DCMFindRecords#var preFetchedData: DCMFieldTag#preFetchedData: DCMFieldTagPtr
DriverSynchronization.p#TestAndClear#var startAddress: UInt8#startAddress: UnivPtr
DriverSynchronization.p#TestAndSet#var startAddress: UInt8#startAddress: UnivPtr
Events.p#GetKeys#theKeys: KeyMap#var theKeys: KeyMap
Files.p#FNNotifyByPath#protected var path: UInt8#path: CStringPtr
Files.p#FNSubscribeByPath#protected var directoryPath: UInt8#directoryPath: CStringPtr
Files.p#FSCatalogSearch#var containerChanged: boolean#containerChanged: BooleanPtr
Files.p#FSCreateDirectoryUnicode#protected var name: UniChar#name: UniCharPtr
Files.p#FSCreateDirectoryUnicode#var newDirID: UInt32#newDirID: UInt32Ptr
Files.p#FSCreateFileUnicode#protected var name: UniChar#name: UniCharPtr
Files.p#FSCreateFork#protected var forkName: UniChar#forkName: UniCharPtr
Files.p#FSDeleteFork#protected var forkName: UniChar#forkName: UniCharPtr
Files.p#FSGetCatalogInfoBulk#var containerChanged: boolean#containerChanged: BooleanPtr
Files.p#FSGetForkCBInfo#var actualRefNum: SInt16#actualRefNum: SInt16Ptr
Files.p#FSGetForkCBInfo#var iterator: SInt16#iterator: SInt16Ptr
Files.p#FSGetVolumeInfo#var actualVolume: FSVolumeRefNum#actualVolume: FSVolumeRefNumPtr
Files.p#FSMakeFSRefUnicode#protected var name: UniChar#name: UniCharPtr
Files.p#FSOpenFork#protected var forkName: UniChar#forkName: UniCharPtr
Files.p#FSPathMakeRef#protected var path: UInt8#path: CStringPtr
Files.p#FSPathMakeRef#var isDirectory: boolean#isDirectory: BooleanPtr
Files.p#FSReadFork#var actualCount: ByteCount#actualCount: ByteCountPtr
Files.p#FSRefMakePath#var path: UInt8#path: CStringPtr
Files.p#FSRenameUnicode#protected var name: UniChar#name: UniCharPtr
Files.p#FSWriteFork#var actualCount: ByteCount#actualCount: ByteCountPtr
FindByContent.p#FBCDoExampleSearch#protected var exampleHitNums: UInt32#exampleHitNums: UInt32Ptr
FindByContent.p#FBCSetSessionVolumes#protected var vRefNums: SInt16#vRefNums: SInt16Ptr
Folders.p#AddFolderDescriptor#name: StrFileName#protected var name: StrFileName
Fonts.p#FMGetFontFamilyResource#var oSize: ByteCount#oSize: ByteCountPtr
Fonts.p#FMGetFontFromFontFamilyInstance#var oIntrinsicStyle: FMFontStyle#oIntrinsicStyle: FMFontStylePtr
Fonts.p#FMGetFontTable#var oActualLength: ByteCount#oActualLength: ByteCountPtr
Fonts.p#FMGetFontTableDirectory#var oActualLength: ByteCount#oActualLength: ByteCountPtr
Fonts.p#FMGetNextFontFamilyInstance#var oSize: FMFontSize#oSize: FMFontSizePtr
Fonts.p#FMGetNextFontFamilyInstance#var oStyle: FMFontStyle#oStyle: FMFontStylePtr
FontSync.p#FNSProfileCreateWithFSRef#protected var iName: UniChar#iName: ConstUniCharPtr
FontSync.p#FNSProfileMatchReference#oIndices: LongIntPtr#oIndices: UInt32Ptr
FontSync.p#FNSProfileMatchReference#var oNumMatches: ItemCount#oNumMatches: ItemCountPtr
FontSync.p#FNSReferenceCreateFromFamily#var oActualStyle: FMFontStyle#oActualStyle: FMFontStylePtr
FontSync.p#FNSReferenceFindName#var oActualNameLength: ByteCount#oActualNameLength: ByteCountPtr
FontSync.p#FNSReferenceFindName#var oFontNameIndex: ItemCount#oFontNameIndex: ItemCountPtr
FontSync.p#FNSReferenceFlatten#var oFlattenedSize: ByteCount#oFlattenedSize: ByteCountPtr
FontSync.p#FNSReferenceGetFamilyInfo#var oActualStyle: FMFontStyle#oActualStyle: FMFontStylePtr
FontSync.p#FNSReferenceGetFamilyInfo#var oFamilyNameScript: ScriptCode#oFamilyNameScript: ScriptCodePtr
FontSync.p#FNSReferenceGetIndName#var oActualNameLength: ByteCount#oActualNameLength: ByteCountPtr
FontSync.p#FNSReferenceGetIndName#var oFontNameCode: FontNameCode#oFontNameCode: FontNameCodePtr
FontSync.p#FNSReferenceGetIndName#var oFontNameLanguage: FontLanguageCode#oFontNameLanguage: FontLanguageCodePtr
FontSync.p#FNSReferenceGetIndName#var oFontNamePlatform: FontPlatformCode#oFontNamePlatform: FontPlatformCodePtr
FontSync.p#FNSReferenceGetIndName#var oFontNameScript: FontScriptCode#oFontNameScript: FontScriptCodePtr
FontSync.p#FNSReferenceMatch#var oFailedMatchOptions: FNSMatchOptions#oFailedMatchOptions: FNSMatchOptionsPtr
FontSync.p#FNSReferenceMatchFamilies#var oNumMatches: ItemCount#oNumMatches: ItemCountPtr
FontSync.p#FNSReferenceMatchFonts#oFonts: LongIntPtr#oFonts: FMFontPtr
FontSync.p#FNSReferenceMatchFonts#var oNumMatches: ItemCount#oNumMatches: ItemCountPtr
Icons.p#GetIconRefFromFileInfo#protected var inFileName: UniChar#inFileName: UniCharPtr
Icons.p#PlotIconRefInContext#protected var inLabelColor: RGBColor#inLabelColor: RGBColorPtr
ImageCompression.p#QTGetFileNameExtension#fileName: StrFileName#protected var fileName: StrFileName
KeychainCore.p#KCFindAppleSharePassword#var serverSignature: AFPServerSignature#serverSignature: AFPServerSignaturePtr
LaunchServices.p#LSGetExtensionInfo#protected var inNameBuffer: UniChar#inNameBuffer: ConstUniCharPtr
MacHelp.p#HMGetHelpMenu#var outFirstCustomItemIndex: MenuItemIndex#outFirstCustomItemIndex: MenuItemIndexPtr
MacLocales.p#LocaleGetIndName#var displayName: UniChar#displayName: UniCharPtr
MacLocales.p#LocaleGetName#var displayName: UniChar#displayName: UniCharPtr
MacLocales.p#LocaleOperationGetIndName#var displayName: UniChar#displayName: UniCharPtr
MacLocales.p#LocaleOperationGetName#var displayName: UniChar#displayName: UniCharPtr
MacLocales.p#LocaleRefFromLocaleString#protected var localeString: char#localeString: ConstCStringPtr
MacLocales.p#LocaleStringToLangAndRegionCodes#protected var localeString: char#localeString: ConstCStringPtr
MacTextEditor.p#TXNCanRedo#var oTXNActionKey: TXNActionKey#oTXNActionKey: TXNActionKeyPtr
MacTextEditor.p#TXNCanUndo#var oTXNActionKey: TXNActionKey#oTXNActionKey: TXNActionKeyPtr
MacTextEditor.p#TXNDrawUnicodeTextBox#protected var iText: UniChar#iText: ConstUniCharPtr
MacTextEditor.p#TXNGetIndexedRunInfoFromRange#var oRunDataType: TXNDataType#oRunDataType: TXNDataTypePtr
MacTextEditor.p#TXNGetIndexedRunInfoFromRange#var oRunEndOffset: TXNOffset#oRunEndOffset: TXNOffsetPtr
MacTextEditor.p#TXNGetIndexedRunInfoFromRange#var oRunStartOffset: TXNOffset#oRunStartOffset: TXNOffsetPtr
MacTextEditor.p#TXNGetTXNObjectControls#protected var iControlTags: TXNControlTag#iControlTags: TXNControlTagPtr
MacTextEditor.p#TXNSetFontDefaults#var iFontDefaults: TXNMacOSPreferredFontDescription#iFontDefaults: TXNMacOSPreferredFontDescriptionPtr
MacTextEditor.p#TXNSetTXNObjectControls#iControlTags: LongIntPtr#iControlTags: TXNControlTagPtr
MacTextEditor.p#TXNSetTypeAttributes#protected var iAttributes: TXNTypeAttributes#iAttributes: TXNTypeAttributesPtr
MacWindows.p#FindWindowOfClass#var outWindowPart: WindowPartCode#outWindowPart: WindowPartCodePtr
MacWindows.p#GetWindowGreatestAreaDevice#var outGreatestDevice: GDHandle#outGreatestDevice: GDHandlePtr
MacWindows.p#GetWindowGroupContents#var outNumItems: ItemCount#outNumItems: ItemCountPtr
MacWindows.p#GetWindowModality#var outUnavailableWindow: WindowRef#outUnavailableWindow: WindowRefPtr
MacWindows.p#GetWindowProperty#var actualSize: UInt32#actualSize: UInt32Ptr
MacWindows.p#IsWindowLatentVisible#var outLatentVisible: WindowLatentVisibility#outLatentVisible: WindowLatentVisibilityPtr
Menus.p#AppendMenuItemTextWithCFString#var outNewItem: MenuItemIndex#outNewItem: MenuItemIndexPtr
Menus.p#GetIndMenuItemWithCommandID#var outIndex: MenuItemIndex#outIndex: MenuItemIndexPtr
Menus.p#GetIndMenuItemWithCommandID#var outMenu: MenuRef#outMenu: MenuRefPtr
Menus.p#GetMenuCommandProperty#var outActualSize: ByteCount#outActualSize: ByteCountPtr
Menus.p#GetMenuTitleIcon#var outType: UInt32#outType: UInt32Ptr
Menus.p#IsMenuKeyEvent#var outMenu: MenuRef#outMenu: MenuRefPtr
Menus.p#IsMenuKeyEvent#var outMenuItem: MenuItemIndex#outMenuItem: MenuItemIndexPtr
Movies.p#GetMovieNextInterestingTime#protected var whichMediaTypes: OSType#whichMediaTypes: OSTypePtr
Movies.p#NewMovieFromFile#resId: INTEGERPtr#resId: SInt16Ptr
Movies.p#NewMovieFromFile#var dataRefWasChanged: boolean#dataRefWasChanged: BooleanPtr
Movies.p#SpriteMediaSetActionVariable#protected var value: Single#value: SinglePtr
Movies.p#TextMediaAddTextSample#textFace: ByteParameter#txtFace: ByteParameter
Multiprocessing.p#_MPLibraryVersion#var major: UInt32#major: UInt32Ptr
Multiprocessing.p#_MPLibraryVersion#var minor: UInt32#minor: UInt32Ptr
Multiprocessing.p#_MPLibraryVersion#var release: UInt32#release: UInt32Ptr
Multiprocessing.p#_MPLibraryVersion#var revision: UInt32#revision: UInt32Ptr
Multiprocessing.p#_MPLibraryVersion#var versionCString: ConstCStringPtr#versionCString: ConstCStringPtrPtr
Multiprocessing.p#MPWaitForEvent#var flags: MPEventFlags#flags: MPEventFlagsPtr
OpenTransport.p#OTNextOption#var buffer: UInt8#buffer: UInt8Ptr
OpenTransport.p#OTFindOption#var buffer: UInt8#buffer: UInt8Ptr
OpenTransport.p#OTAtomicSetBit#var bytePtr: UInt8#bytePtr: UInt8Ptr
OpenTransport.p#OTAtomicClearBit#var bytePtr: UInt8#bytePtr: UInt8Ptr
OpenTransport.p#OTAtomicTestBit#var bytePtr: UInt8#bytePtr: UInt8Ptr
OpenTransportProviders.p#OTSetAddressFromNBPEntity#var nameBuf: UInt8#nameBuf: UInt8Ptr
OpenTransportProviders.p#OTSetAddressFromNBPString#var addrBuf: UInt8#addrBuf: UInt8Ptr
OpenTransportProtocol.p#OTSetFirstClearBit#var bitMap: UInt8#bitMap: UInt8Ptr
OpenTransportProtocol.p#OTClearBit#var bitMap: UInt8#bitMap: UInt8Ptr
OpenTransportProtocol.p#OTSetBit#var bitMap: UInt8#bitMap: UInt8Ptr
OpenTransportProtocol.p#OTTestBit#var bitMap: UInt8#bitMap: UInt8Ptr
ImageCodec.p#QTPhotoDefineHuffmanTable#var lengthCounts: UInt8#lengthCounts: UInt8Ptr
ImageCodec.p#QTPhotoDefineHuffmanTable#var values: UInt8#values: UInt8Ptr
ImageCodec.p#QTPhotoDefineQuantizationTable#var table: UInt8#table: UInt8Ptr
OpenTransportProviders.p#OTInetMailExchange#var mx: InetMailExchange#mx: InetMailExchangePtr
OpenTransportProviders.p#OTSetNBPEntityFromAddress#protected var addrBuf: UInt8#addrBuf: UInt8Ptr
PMApplication.p#PMSessionBeginPage#protected var pageFrame: PMRect#pageFrame: PMRectPtr
PMCore.p#PMSessionBeginPageNoDialog#protected var pageFrame: PMRect#pageFrame: PMRectPtr
PMCore.p#PMSessionValidatePageFormat#var result: boolean#result: BooleanPtr
PMCore.p#PMSessionValidatePrintSettings#var result: boolean#result: BooleanPtr
PMCore.p#PMSetJobName#name: StringPtr#protected var name: Str255
Quickdraw.p#QDSwapPort#var outOldPort: CGrafPtr#outOldPort: CGrafPtrPtr
QuickTimeComponents.p#InvokePreprocessInstructionHandlerUPP#protected var atts: ConstCStringPtr#atts: ConstCStringPtrPtr
Resources.p#FSCreateResFile#protected var name: UniChar#name: UniCharPtr
Resources.p#FSCreateResFile#var newRef: FSRef#newRef: FSRefPtr
Resources.p#FSCreateResFile#var newSpec: FSSpec#newSpec: FSSpecPtr
Resources.p#FSCreateResourceFile#protected var forkName: UniChar#forkName: UniCharPtr
Resources.p#FSCreateResourceFile#protected var name: UniChar#name: UniCharPtr
Resources.p#FSOpenResourceFile#protected var forkName: UniChar#forkName: UniCharPtr
TextCommon.p#GetTextEncodingName#var oActualEncoding: TextEncoding#oActualEncoding: TextEncodingPtr
TextCommon.p#GetTextEncodingName#var oActualRegion: RegionCode#oActualRegion: RegionCodePtr
TextCommon.p#RevertTextEncodingToScriptInfo#var oTextLanguageID: LangCode#oTextLanguageID: LangCodePtr
TextCommon.p#UCGetCharProperty#protected var charPtr: UniChar#charPtr: ConstUniCharPtr
TextCommon.p#UpgradeScriptInfoToTextEncoding#protected var iTextFontname: Str255#iTextFontname: StringPtr
TextEncodingConverter.p#TECCreateConverterFromPath#protected var inPath: TextEncoding#inPath: TextEncodingPtr
TextEncodingConverter.p#TECCreateOneToManyConverter#protected var outputEncodings: TextEncoding#outputEncodings: TextEncodingPtr
TextEncodingConverter.p#TECGetAvailableTextEncodings#var availableEncodings: TextEncoding#availableEncodings: TextEncodingPtr
TextEncodingConverter.p#TECGetDestinationTextEncodings#var destinationEncodings: TextEncoding#destinationEncodings: TextEncodingPtr
TextEncodingConverter.p#TECGetSubTextEncodings#var subEncodings: TextEncoding#subEncodings: TextEncodingPtr
TextEncodingConverter.p#TECGetWebTextEncodings#var availableEncodings: TextEncoding#availableEncodings: TextEncodingPtr
TextEncodingConverter.p#TECGetMailTextEncodings#var availableEncodings: TextEncoding#availableEncodings: TextEncodingPtr
TextEncodingConverter.p#TECGetAvailableSniffers#var availableSniffers: TextEncoding#availableSniffers: TextEncodingPtr
TextEncodingConverter.p#TECCreateSniffer#var testEncodings: TextEncoding#testEncodings: TextEncodingPtr
TextEncodingConverter.p#TECSniffTextEncoding#var testEncodings: TextEncoding#testEncodings: TextEncodingPtr
TextEncodingConverter.p#TECGetDirectTextEncodingConversions#var availableConversions: TECConversionInfo#availableConversions: TECConversionInfoPtr
TextServices.p#UCTextServiceEvent#var unicodeString: UniChar#unicodeString: UniCharPtr
Translation.p#CanDocBeOpened#protected var nativeTypes: FileType#nativeTypes: FileTypePtr
Translation.p#ExtendFileTypeList#protected var originalTypeList: FileType#originalTypeList: FileTypePtr
Translation.p#ExtendFileTypeList#var extendedTypeList: FileType#extendedTypeList: FileTypePtr
Translation.p#GetFileTypesThatAppCanNativelyOpen#var nativeTypes: FileType#var nativeTypes: TypesBlock
UnicodeConverter.p#ConvertFromPStringToUnicode#var oUnicodeStr: UniChar#oUnicodeStr: UniCharPtr
UnicodeConverter.p#ConvertFromTextToUnicode#iOffsetArray: LongIntPtr#iOffsetArray: ByteOffsetPtr
UnicodeConverter.p#ConvertFromTextToUnicode#oOffsetArray: LongIntPtr#oOffsetArray: ByteOffsetPtr
UnicodeConverter.p#ConvertFromTextToUnicode#var oOffsetCount: ItemCount#oOffsetCount: ItemCountPtr
UnicodeConverter.p#ConvertFromTextToUnicode#var oUnicodeStr: UniChar#oUnicodeStr: UniCharPtr
UnicodeConverter.p#ConvertFromUnicodeToPString#protected var iUnicodeStr: UniChar#iUnicodeStr: UniCharPtr
UnicodeConverter.p#ConvertFromUnicodeToScriptCodeRun#iOffsetArray: LongIntPtr#iOffsetArray: ByteOffsetPtr
UnicodeConverter.p#ConvertFromUnicodeToScriptCodeRun#oOffsetArray: LongIntPtr#oOffsetArray: ByteOffsetPtr
UnicodeConverter.p#ConvertFromUnicodeToScriptCodeRun#protected var iUnicodeStr: UniChar#iUnicodeStr: UniCharPtr
UnicodeConverter.p#ConvertFromUnicodeToScriptCodeRun#var oOffsetCount: ItemCount#oOffsetCount: ItemCountPtr
UnicodeConverter.p#ConvertFromUnicodeToText#iOffsetArray: LongIntPtr#iOffsetArray: ByteOffsetPtr
UnicodeConverter.p#ConvertFromUnicodeToText#oOffsetArray: LongIntPtr#oOffsetArray: ByteOffsetPtr
UnicodeConverter.p#ConvertFromUnicodeToText#protected var iUnicodeStr: UniChar#iUnicodeStr: UniCharPtr
UnicodeConverter.p#ConvertFromUnicodeToText#var oOffsetCount: ItemCount#oOffsetCount: ItemCountPtr
UnicodeConverter.p#ConvertFromUnicodeToTextRun#iOffsetArray: LongIntPtr#iOffsetArray: ByteOffsetPtr
UnicodeConverter.p#ConvertFromUnicodeToTextRun#oOffsetArray: LongIntPtr#oOffsetArray: ByteOffsetPtr
UnicodeConverter.p#ConvertFromUnicodeToTextRun#protected var iUnicodeStr: UniChar#iUnicodeStr: UniCharPtr
UnicodeConverter.p#ConvertFromUnicodeToTextRun#var oOffsetCount: ItemCount#oOffsetCount: ItemCountPtr
UnicodeConverter.p#CreateUnicodeToTextRunInfoByEncoding#protected var iEncodings: TextEncoding#iEncodings: TextEncodingPtr
UnicodeConverter.p#CreateUnicodeToTextRunInfoByScriptCode#protected var iScripts: ScriptCode#iScripts: ScriptCodePtr
UnicodeConverter.p#InvokeUnicodeToTextFallbackUPP#var iSrcUniStr: UniChar#iSrcUniStr: UniCharPtr
UnicodeConverter.p#TruncateForUnicodeToText#protected var iSourceStr: UniChar#iSourceStr: ConstUniCharPtr
UnicodeUtilities.p#UCCompareCollationKeys#protected var key1Ptr: UCCollationValue#key1Ptr: UCCollationValuePtr
UnicodeUtilities.p#UCCompareCollationKeys#protected var key2Ptr: UCCollationValue#key2Ptr: UCCollationValuePtr
UnicodeUtilities.p#UCCompareText#protected var text1Ptr: UniChar#text1Ptr: ConstUniCharPtr
UnicodeUtilities.p#UCCompareText#protected var text2Ptr: UniChar#text2Ptr: ConstUniCharPtr
UnicodeUtilities.p#UCCompareTextDefault#protected var text1Ptr: UniChar#text1Ptr: ConstUniCharPtr
UnicodeUtilities.p#UCCompareTextDefault#protected var text2Ptr: UniChar#text2Ptr: ConstUniCharPtr
UnicodeUtilities.p#UCCompareTextNoLocale#protected var text1Ptr: UniChar#text1Ptr: ConstUniCharPtr
UnicodeUtilities.p#UCCompareTextNoLocale#protected var text2Ptr: UniChar#text2Ptr: ConstUniCharPtr
UnicodeUtilities.p#UCFindTextBreak#protected var textPtr: UniChar#textPtr: ConstUniCharPtr
UnicodeUtilities.p#UCGetCollationKey#protected var textPtr: UniChar#textPtr: ConstUniCharPtr
UnicodeUtilities.p#UCKeyTranslate#var unicodeString: UniChar#unicodeString: UniCharPtr
UnicodeUtilities.p#UCGetCollationKey#var collationKey: UCCollationValue#collationKey: UCCollationValuePtr
ADDCONSTANTS
CFArray.p#CFArrayCallBacks#kCFTypeArrayCallBacks
CFBag.p#CFBagCallBacks#kCFTypeBagCallBacks
CFBag.p#CFBagCallBacks#kCFCopyStringBagCallBacks
CFBase.p#CFAllocatorRef#kCFAllocatorDefault
CFBase.p#CFAllocatorRef#kCFAllocatorSystemDefault
CFBase.p#CFAllocatorRef#kCFAllocatorMalloc
CFBase.p#CFAllocatorRef#kCFAllocatorNull
CFBase.p#CFAllocatorRef#kCFAllocatorUseContext
CFBundle.p#CFStringRef#kCFBundleInfoDictionaryVersionKey
CFBundle.p#CFStringRef#kCFBundleExecutableKey
CFBundle.p#CFStringRef#kCFBundleIdentifierKey
CFBundle.p#CFStringRef#kCFBundleVersionKey
CFBundle.p#CFStringRef#kCFBundleDevelopmentRegionKey
CFBundle.p#CFStringRef#kCFBundleNameKey
CFDate.p#CFTimeInterval#kCFAbsoluteTimeIntervalSince1970
CFDate.p#CFTimeInterval#kCFAbsoluteTimeIntervalSince1904
CFDictionary.p#CFDictionaryKeyCallBacks#kCFTypeDictionaryKeyCallBacks
CFDictionary.p#CFDictionaryKeyCallBacks#kCFCopyStringDictionaryKeyCallBacks
CFDictionary.p#CFDictionaryValueCallBacks#kCFTypeDictionaryValueCallBacks
CFNumber.p#CFBooleanRef#kCFBooleanTrue
CFNumber.p#CFBooleanRef#kCFBooleanFalse
CFNumber.p#CFNumberRef#kCFNumberPositiveInfinity
CFNumber.p#CFNumberRef#kCFNumberNegativeInfinity
CFNumber.p#CFNumberRef#kCFNumberNaN
CFPlugIn.p#CFStringRef#kCFPlugInDynamicRegistrationKey
CFPlugIn.p#CFStringRef#kCFPlugInDynamicRegisterFunctionKey
CFPlugIn.p#CFStringRef#kCFPlugInUnloadFunctionKey
CFPlugIn.p#CFStringRef#kCFPlugInFactoriesKey
CFPlugIn.p#CFStringRef#kCFPlugInTypesKey
CFPreferences.p#CFStringRef#kCFPreferencesAnyApplication
CFPreferences.p#CFStringRef#kCFPreferencesCurrentApplication
CFPreferences.p#CFStringRef#kCFPreferencesAnyHost
CFPreferences.p#CFStringRef#kCFPreferencesCurrentHost
CFPreferences.p#CFStringRef#kCFPreferencesAnyUser
CFPreferences.p#CFStringRef#kCFPreferencesCurrentUser
CFSet.p#CFSetCallBacks#kCFTypeSetCallBacks
CFSet.p#CFSetCallBacks#kCFCopyStringSetCallBacks
CFURLAccess.p#CFStringRef#kCFURLFileExists
CFURLAccess.p#CFStringRef#kCFURLFileDirectoryContents
CFURLAccess.p#CFStringRef#kCFURLFileLength
CFURLAccess.p#CFStringRef#kCFURLFileLastModificationTime
CFURLAccess.p#CFStringRef#kCFURLFilePOSIXMode
CFURLAccess.p#CFStringRef#kCFURLFileOwnerID
CFURLAccess.p#CFStringRef#kCFURLHTTPStatusCode
CFURLAccess.p#CFStringRef#kCFURLHTTPStatusLine
CGAffineTransforms.p#CGAffineTransform#CGAffineTransformIdentity
CGGeometry.p#CGPoint#CGPointZero
CGGeometry.p#CGSize#CGSizeZero
CGGeometry.p#CGRect#CGRectZero
CGGeometry.p#CGRect#CGRectNull
CFTYPES
CFArray.p#CFArrayRef
CFBag.p#CFBagRef
CFBase.p#CFStringRef
CFBase.p#CFAllocatorRef
CFBundle.p#CFBundleRef
CFCharacterSet.p#CFCharacterSetRef
CFData.p#CFDataRef
CFDate.p#CFDateRef
CFDate.p#CFTimeZoneRef
CFDictionary.p#CFDictionaryRef
CFNumber.p#CFBooleanRef
CFNumber.p#CFNumberRef
CFPlugIn.p#CFPlugInInstanceRef
CFSet.p#CFSetRef
CFTree.p#CFTreeRef
CFURL.p#CFURLRef
CFUUID.p#CFUUIDRef
CFXMLNode.p#CFXMLNodeRef
CFXMLParser.p#CFXMLParserRef
FIXDIRID
Files.p
Folders.p
IGNORERECORDS
NOMACTYPES
ConditionalMacros.p
MacTypes.p
EXTRAUNITS
ADSP.p#OSUtils
AEInteraction.p#Quickdraw
AEObjects.p#AEDataModel
AEPackObject.p#AEDataModel
AERegistry.p#ATSTypes
ASDebugging.p#AEDataModel,OSA
AuthorizationDB.p#Authorization,CFBase,CFDictionary,CFString,CFBundle
AuthSession.p#Authorization
AXValue.p#CFBase
ATSFont.p#CFBase,CFRunLoop,CFPropertyList
AVComponents.p#Quickdraw,CMTypes,Events
Appearance.p#CFBase,CGContext,Collections,Processes,QuickdrawText,TextCommon
AppleGuide.p#AEDataModel
AppleHelp.p#CFBase
AppleScript.p#AEDataModel,Components
CarbonEvents.p#CFBase,CGContext,Quickdraw,AXUIElement,Drag,CFArray,HIObjectCore
CFNetServices.p#CFBase,CFStream,CFArray,CFRunLoop
CFNotificationCenter.p#CFBase,CFDictionary
CFSocket.p#CFBase,CFData,CFString,CFRunLoop
CFStream.p#CFString,CFDictionary,CFURL,CFRunLoop,CFSocket
CFPreferences.p#CFDictionary
CFRunLoop.p#CFBase,CFArray,CFDate,CFString
CFSocket.p#CFBase,CFDate,CFPropertyList
CFStream.p#CFString,CFDictionary,CFURL,CFRunLoop,CFSocket,CFBase
CFXMLNode.p#CFBase
CGBitmapContext.p#CGColorSpace,CGImage
CGColorSpace.p#CMTypes
CGContext.p#CGGeometry
CGGeometry.p#CFBase
CGImage.p#CFBase
CGPDFContext.p#CGGeometry,CFURL
CGPattern.p#CGGeometry,CGAffineTransforms
CMAcceleration.p#CMTypes
CMApplication.p#CFBase
CMCalibrator.p#CMTypes
CMDeviceIntegration.p#CFBase,CFDictionary,CMTypes
CMMComponent.p#Files,CMTypes,CMICCProfile
CMPRComponent.p#CMTypes,CMICCProfile
CMScriptingPlugin.p#CMTypes,AEDataModel
ColorPicker.p#CMTypes,CMICCProfile
ColorPickerComponents.p#CMTypes,Events,CMICCProfile
Connections.p#Events
ControlDefinitions.p#AEDataModel,CFBase,Events,Quickdraw,Icons,CFData,DateTimeUtils,Drag,TextCommon
Controls.p#CFBase,Files,Events,CGImage
Dialogs.p#CFBase,Quickdraw
Dictionary.p#AEDataModel
Displays.p#Quickdraw,CMTypes,AEDataModel
Drag.p#AEDataModel,CGImage,CGGeometry
DrawSprocket.p#Video
DriverServices.p#OSUtils
Editions.p#AEDataModel,Events
FileSigning.p#CFArray
FileTransfers.p#Events,Quickdraw
FindByContent.p#CFBase
FontSync.p#ATSTypes
Fonts.p#QuickdrawText
GXEnvironment.p#CMTypes
HTMLRendering.p#CFBase
HyperXCmd.p#Quickdraw
IBCarbonRuntime.p#CFBase,Quickdraw,Menus
ICAApplication.p#AEDataModel
Icons.p#CFBase,CGGeometry
ImageCodec.p#Events,QDOffscreen,OSUtils,Dialogs
ImageCompression.p#Files,OSUtils
InternetConfig.p#Files
IsochronousDataHandler.p#Components,Movies
JManager.p#AEDataModel,Events
KeychainHI.p#CFBase
LanguageAnalysis.p#AEDataModel
LaunchServices.p#CFBase,CFArray
Lists.p#Events,Quickdraw
LocationManager.p#AEDataModel
MacApplication.p#CGImage,Menus
MacHelp.p#CFBase,Quickdraw,TextEdit
MacTCP.p#OSUtils
MacTextEditor.p#CFBase,AEDataModel,TextCommon,Quickdraw,QDOffscreen,Menus,ATSUnicodeTypes
MacWindows.p#CFBase,Files,Appearance,CarbonEvents,HIToolbar
MediaHandlers.p#Quickdraw,Events,ImageCompression
Menus.p#AEDataModel,CFBase,CGContext,ATSTypes
Movies.p#Files,QDOffscreen,TextEdit,Controls,Dialogs
MoviesFormat.p#ImageCompression,Components
Navigation.p#AEDataModel,CFBase,Quickdraw,Finder,Events
OSA.p#AEDataModel,
OSAGeneric.p#AEDataModel,Components
OSUtils.p#CFBase
PCCardAdapterPlugin.p#DriverServices
PCCardEnablerPlugin.p#DriverServices
PMApplication.p#Quickdraw,PMDefinitions
PMCore.p#CFBase,CFArray
PictUtils.p#Quickdraw
Processes.p#CFBase,Quickdraw,AEDataModel
QD3DRenderer.p#Quickdraw
QTML.p#Quickdraw,Events
QTStreamingComponents.p#Dialogs
Quickdraw.p#CMTypes,CGDirectDisplay
QuickTimeComponents.p#Files,Events,QDOffscreen,Menus,Dialogs,Aliases
QuickTimeMusic.p#Dialogs,Files
QuickTimeStreaming.p#Files,Events,ImageCompression
QuickTimeVR.p#Quickdraw
QuickTimeVRFormat.p#Files
Scrap.p#CFBase
SpeechRecognition.p#Files,AEDataModel
Start.p#Finder
Terminals.p#Events,Quickdraw
TextCommon.p#AEDataModel
TextServices.p#Quickdraw
TextUtils.p#IntlResources
USB.p#Files
HIObject.p#CFBase,CFBundle,Events
HIShape.p#CFBase,CGContext,Drag,Quickdraw
HIToolbar.p#CFArray,CFBase,CGImage,Icons
HIView.p#CFArray,CFBase,CGContext,CGImage,CFBase,Drag,Events,Quickdraw,Menus,Appearance
KILLUNITS
ADSP.p
AppleGuide.p
AppleTalk.p
ATA.p
ATSUnicode.p
AVComponents.p
Balloons.p
Carbon.p
CardServices.p
CMAcceleration.p
CMComponent.p
CMConversions.p
ColorPickerComponents.p
CommResources.p
Connections.p
ConnectionTools.p
ControlStrip.p
CoreServices.p
CRMSerialDevices.p
CryptoMessageSyntax.p
CTBUtilities.p
CursorDevices.p
DatabaseAccess.p
DeskBus.p
DesktopPrinting.p
DeviceControl.p
DiskInit.p
Disks.p
DriverSupport.p
Editions.p
ENET.p
EPPC.p
Errors.p
FileMapping.p
FileSigning.p
FileTransfers.p
FileTransferTools.p
FSM.p
GXEnvironment.p
GXErrors.p
GXFonts.p
GXGraphics.p
GXLayout.p
GXMath.p
HID.p
HyperXCmd.p
IAExtractor.p
InputSprocket.p
Interrupts.p
IsochronousDataHandler.p
JManager.p
Kernel.p
Keychain.p
LocationManager.p
MacOS.p
MacTCP.p
Memory.p
MIDI.p
MultiprocessingInfo.p
NetSprocket.p
NetworkSetup.p
OpenTptAppleTalk.p
OpenTptClient.p
OpenTptCommon.p
OpenTptConfig.p
OpenTptDevLinks.p
OpenTptGlobalNew.p
OpenTptInternet.p
OpenTptISDN.p
OpenTptLinks.p
OpenTptModule.p
OpenTptPCISupport.p
OpenTptSerial.p
OpenTptXTI.p
OpenTransportKernel.p
OpenTransportUNIX.p
Packages.p
Patches.p
PCCard.p
PCCardAdapterPlugin.p
PCCardEnablerPlugin.p
PCCardTuples.p
PCI.p
PPCToolbox.p
QD3D.p
QD3DAcceleration.p
QD3DCamera.p
QD3DController.p
QD3DCustomElements.p
QD3DDrawContext.p
QD3DErrors.p
QD3DExtension.p
QD3DGeometry.p
QD3DGroup.p
QD3DIO.p
QD3DLight.p
QD3DMath.p
QD3DPick.p
QD3DRenderer.p
QD3DSet.p
QD3DShader.p
QD3DStorage.p
QD3DString.p
QD3DStyle.p
QD3DTransform.p
QD3DView.p
QD3DViewer.p
QD3DWinViewer.p
QuickTime.p
RAVE.p
RAVESystem.p
Retrace.p
ROMDefs.p
ScalerStreamTypes.p
ScalerTypes.p
SegLoad.p
Serial.p
ShutDown.p
Slots.p
SocketServices.p
SoundComponents.p
SoundInput.p
SoundSprocket.p
Speech.p
StandardFile.p
Start.p
Strings.p
TargetConditionals.p
Telephones.p
Terminals.p
TerminalTools.p
Traps.p
Types.p
Unicode.p
vBasicOps.p
vBigNum.p
vectorOps.p
vfp.p
VideoServices.p
Windows.p
ZoomedVideo.p
DriverServices.p
ApplicationServices.p
CoreFoundation.p
BADCONST
Fixed: GPC,MW,FPC
Double: GPC,MW,FPC
LongDateTime: GPC
LongDouble: GPC,MW,FPC
Point: GPC
SInt32: GPC,MW,FPC
SInt64: GPC
Single: GPC,MW,FPC
TimeValue64: GPC
UInt64: GPC
decform: GPC,FPC
fenv_t: GPC,MW,FPC
fexcept_t: GPC,MW,FPC
ConstCStringPtr: GPC,MW,FPC
FIXMWERKS
URLAccess.p
PMDefinitions.p
MacWindows.p
CGDirectDisplay.p
