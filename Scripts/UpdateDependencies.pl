#!/usr/bin/perl

use strict;
use warnings;
use lib 'Scripts';

use MetaPascalUtils qw( replace_file_with_data );

my $unitlist = shift || die "UnitList?";
my $output = shift || die "Output?";
die "UnitList is not UnitList.txt" unless -f $unitlist && -r $unitlist && $unitlist =~ m!/UnitList\.txt$!;
die "Output is not in the Build folder" unless $output =~ m!/Build/!;
die "Output is not .make file" unless $output =~ m!\.make$!;

my $result = <<EOM;
SOURCEFILES := 
GPCPINTERFACESFILES := 
FPCPINTERFACESFILES := 
MWPINTERFACESFILES := 

EOM

open( UNITLIST, $unitlist ) or die "Cant open $unitlist $!";
while ( <UNITLIST> ) {
	chomp;
	my $filepas = "$_.pas";
	my $filep = "$_.p";
	$result .= <<EOM;
SOURCEFILES += \$(SOURCE)/$
filepas

GPCPINTERFACESFILES += \$(GPCPINTERFACES)/$filepas
\$(GPCPINTERFACES)/$filepas: \$(SOURCE)/$filepas Scripts/GenerateFile.pl
	/usr/bin/perl Scripts/GenerateFile.pl GPC "\$<" "\$@"

FPCPINTERFACESFILES += \$(FPCPINTERFACES)/$filepas
\$(FPCPINTERFACES)/$filepas: \$(SOURCE)/$filepas Scripts/GenerateFile.pl
	/usr/bin/perl Scripts/GenerateFile.pl FPC "\$<" "\$@"

MWPINTERFACESFILES += \$(MWPINTERFACES)/$filep
\$(MWPINTERFACES)/$filep: \$(SOURCE)/$filepas Scripts/GenerateFile.pl
	/usr/bin/perl Scripts/GenerateFile.pl MW "\$<" "\$@"

EOM
}
close( UNITLIST );

replace_file_with_data( $output, $result, {creator=>'R*ch', type=>'TEXT'} );
