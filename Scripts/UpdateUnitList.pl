#!/usr/bin/perl

use strict;
use warnings;
use lib 'Scripts';

use MetaPascalUtils qw( read_dir replace_file_with_data );

my $source = shift || die "Source?";
my $output = shift || die "Output?";
die "Source is not directory" unless -d $source;
die "Output is not in the Build folder" unless $output =~ m!/Build/!;
die "Output is not .txt file" unless $output =~ m!\.txt$!;

our @names = sort( grep( s!\.pas$!!, read_dir( $source ) ) );

my $data = '';

foreach my $name ( sort @names ) {
	next if $name =~ m!^GPCStrings!;
	next if $name =~ m!^FPCStrings!;
	
	$data .= "$name\n";
}

replace_file_with_data( $output, $data, {creator=>'R*ch', type=>'TEXT'} );
