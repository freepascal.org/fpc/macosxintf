#!/usr/bin/perl

use lib "$ENV{HOME}/perl";
use strict;
use warnings;
use lib 'Scripts';

use utf8;

use MetaPascalUtils qw( read_dir read_file );

$ENV{PATH} = '/bin:/usr/bin:/usr/local/bin';

my $oldrevision = shift || 'HEAD';
$oldrevision =~ s!^-r!!;

my $newrevision = shift || 'CURRENT';
$newrevision =~ s!^-r!!;

$| = 1;

die "Must be in metaPascal directory" unless -d 'MPPInterfaces' and -d 'Scripts';
my $url = `svn info | grep ^URL`;
chomp $url;
$url =~ s!^URL:\s*!!;

my $oldname = 'TempMPPInterfacesOldRevision';
my $newname = $newrevision eq 'CURRENT' ? 'MPPInterfaces' : 'TempMPPInterfacesNewRevision';

if ( !-d $oldname ) {
	system( 'svn', 'checkout', "-r$oldrevision", '--quiet', "$url/MPPInterfaces", $oldname );
}
if ( !-d $newname ) {
	system( 'svn', 'checkout', "-r$newrevision", '--quiet', "$url/MPPInterfaces", $newname );
}

our %functionnames = ();
our %functions = ();
print "==== Scan New $newname\n";
scan( $newname, 'New' );
print "==== Scan Old $oldname\n";
scan( $oldname, 'Old' );
print "==== Post Scan for Duplicates\n";
postscan_duplicates();
print "==== Removed Functions\n";
scan_for( 'Old' );
print "==== Added Functions\n";
scan_for( 'New' );
print "==== Unchanged Functions\n";
scan_both( \&scan_same, 1 );
print "==== Minor Changed Functions\n";
scan_both( \&scan_minor );
print "==== Parameter Name Changed Functions\n";
scan_both( \&scan_minor_or_name );
print "==== Changed Functions\n";
scan_both( \&scan_always );

sub scan {
  my ( $dir, $code ) = @_;
  
  my @files = sort( grep( m!\.pas$!, read_dir( $dir ) ) );
  my $count = 0;
  foreach my $filename ( @files ) {
  	$count++;
  	print "$code: $count/".scalar(@files),"\n" if $count % 25 == 0;
  	next if $filename eq 'GPCStrings.pas';
  	scan_file( "$dir/$filename", $code );
  }
}

sub scan_file {
  my ( $file, $code ) = @_;
  
  my $data = read_file( $file );
  
	$data =~ s!^implementation\n.*^end\.$!end.!sm;

  $data =~ s!^[ \t]*(procedure\s+(\w+)\s*(\([^)]*\))?)\s*;!add_entry( $code, $2, $1 )!gme;
  $data =~ s!^[ \t]*(function\s+(\w+)\s*(\([^)]*\))?:\s*\w+)\s*;!add_entry( $code, $2, $1 )!gme;
  $data =~ s!^[ \t]*((\w+)\s*=\s*procedure\s+\w+\s*(\([^)]*\))?)\s*;!add_entry( $code, $2, $1 )!gme;
  $data =~ s!^[ \t]*((\w+)\s*=\s*function\s+\w+\s*(\([^)]*\))?:\s*\w+)\s*;!add_entry( $code, $2, $1 )!gme;
}

sub add_entry {
  my ( $code, $name, $entry ) = @_;
  
  $entry =~ s!\s+! !g;
  $entry =~ s!\A\s+!!;
  $entry =~ s!\s+\Z!!;
  $functionnames{$name} ||= $code;
  if ( $functionnames{$name} ne $code ) {
  	$functionnames{$name} = 'Both';
  }
  if ( $functions{"$name.$code"} ) {
  	print "Duplicate definition $name.$code\n";
  	if ( !ref $functions{"$name.$code"} ) {
  		$functions{"$name.$code"} = [$functions{"$name.$code"}];
  	}
  	push @{$functions{"$name.$code"}}, $entry;
  } else {
	$functions{"$name.$code"} = $entry;
  }
}

sub postscan_duplicates {
	foreach my $key ( keys %functions ) {
		if ( ref $functions{$key} ) {
			print "Duplicate definition $key\n";
			$functions{$key} = join( '|', sort @{$functions{$key}} );
			print "Gives: $functions{$key}\n";
		}
	}
}

sub scan_for {
	my ( $code ) = @_;
	foreach my $key ( sort keys %functionnames ) {
		if ( $functionnames{$key} eq $code ) {
			print $functions{"$key.$code"},"\n";
		}
	}
}

sub scan_both {
	my ( $scan_func, $skip ) = @_;
	foreach my $key ( sort keys %functionnames ) {
		if ( $functionnames{$key} eq 'Both' ) {
			my $old = &$scan_func( $functions{"$key.Old"} );
			my $new = &$scan_func( $functions{"$key.New"} );
			if ( $old eq $new ) {
				print $functions{"$key.Old"},"\n" unless $skip;
				print $functions{"$key.New"},"\n" unless $skip;
#				print scan_minor( $functions{"$key.Old"} ),"\n" unless $skip;
#				print scan_minor( $functions{"$key.New"} ),"\n" unless $skip;
				$functionnames{$key} = 'Done';
			}
		}
	}
	print "Skipped\n" if $skip;
}

sub scan_same {
	my ( $func ) = @_;
	
	$func =~ s!\{.*?\}! !g;
	$func =~ s!  +! !g;
	$func =~ s!\A +!!;
	$func =~ s! +\Z!!;
	$func =~ s!(\W) !$1!g;
	$func =~ s! (\W)!$1!g;
	
	return $func;
}

sub scan_minor {
	my ( $func ) = @_;
	
	$func = scan_same( $func );

	$func =~ s!\bboolean\b!Boolean!g;
	$func =~ s!\bproperty\b!proprty!g;
	$func =~ s!\bMenuItemIndex\b!SInt16!g;
	$func =~ s!\bMenuID\b!SInt16!g;
	$func =~ s!\bThemeTrackEnableState\b!UInt8!g;
	$func =~ s!\bAEInteractAllowed\b!UInt8!g;
	$func =~ s!\bByteParameter\b!UInt8!g;
	$func =~ s!\bStyle\b!UInt8!g;
	$func =~ s!\bThemeTrackPressState\b!UInt8!g;
	$func =~ s!\bTextServicePropertyTag\b!OSType!g;
	$func =~ s!\bSingle\b!Float32!g;
	$func =~ s!\bDouble\b!Float64!g;
	$func =~ s!\bUnivPtr\b!Ptr!g;
	$func =~ s!\bHIViewPartCode\b!ControlPartCode!g;
	$func =~ s!\bAppearancePartCode\b!ControlPartCode!g;
	$func =~ s!\buserUPP\b!userRoutine!g;
	$func =~ s#\bconst (?!var\b)#const var #g;

	return $func;
}

sub scan_minor_or_name {
	my ( $func ) = @_;

	$func = scan_minor( $func );
	$func =~ s!\w+:!param:!g;
	
	return $func;
}

sub scan_always {
	my ( $func ) = @_;
	
	return '1';
}
