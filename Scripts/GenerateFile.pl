#!/usr/bin/perl

use strict;
use warnings;
use lib 'Scripts';

use utf8;

use MetaPascalUtils qw( read_dir read_file replace_file_with_data );

my $type = shift || die "Type?";
my $source = shift || die "Source?";
my $output = shift || die "Output?";
die "Type is not GPC/FPC/MW" unless $type =~ m!\A(GPC|FPC|MW)\Z!;
die "Output is not in the Build folder" unless $output =~ m!/Build/!;
if ( $output =~ m!\.inc$! ) {
	die "INC is for GPC only" unless $type eq 'GPC';
	die "Source is not a readable folder" unless -r $source && -d $source;
} else {
	die "Source is not a readable file" unless -r $source;
	if ( -d $source ) {
		die "Output is not a folder (and Source is)" unless -d $output;
	} else {
		die "Output is not pascal target file" unless $output =~ m!\.p(as)?$!;
	}
}

our ( $data, $add_before, $add_after, $creator );

our $GAP = 308;

# Set up for finding availability macros
# NOTE: Has an embedded capturing group needed to deal with, if it exists, macro's parenthesized parameter str - capturing needed balanced parentheses matchng
our $avail_re = qr!(?:(?:DEPRECATED_|AVAILABLE_)[A-Z0-9_]+|[A-Z_]*(?:_AVAILABLE(?:_STARTING|_BUT_DEPRECATED(?:_MSG|_WITH_ADVICE)?|_MAC)?|_DEPRECATED(?:_WITH_REPLACEMENT|_MAC)?|_UNAVAILABLE|_PROHIBITED)(?:(\((?:[^()]++|(?-1))*+\)))?\s?)+!;

our $machoconditionals = '';
if ( $type eq 'MW' ) {
	$machoconditionals .= <<'EOM';
{$ifc defined __POWERPC__ and __POWERPC__}
	{$definec __ppc__ 1}
{$elsec}
	{$errorc These interfaces can only be used with the PowerPC version of the MetroWerks CodeWarrior Pascal compiler}
{$endc}

{$definec TARGET_CPU_PPC           TRUE}
{$definec TARGET_CPU_PPC64         FALSE}
{$definec TARGET_CPU_X86           FALSE}
{$definec TARGET_CPU_X86_64        FALSE}
{$definec TARGET_CPU_ARM           FALSE}
{$definec TARGET_CPU_64            FALSE}
{$definec TARGET_OS_MAC            TRUE}
{$definec TARGET_OS_IPHONE         FALSE}
{$definec TARGET_IPHONE_SIMULATOR  FALSE}
{$definec TARGET_OS_EMBEDDED       FALSE}
{$definec TARGET_RT_BIG_ENDIAN     TRUE}
{$definec TARGET_RT_LITTLE_ENDIAN  FALSE}
EOM
} elsif ( $type eq 'GPC' ) {
	$machoconditionals .= <<'EOM';
{$ifc defined __ppc__ and __ppc__ and defined __i386__ and __i386__}
	{$error Conflicting initial definitions for __ppc__ and __i386__, define only one to true}
{$endc}
{$ifc defined __BYTES_BIG_ENDIAN__ and (__BYTES_BIG_ENDIAN__=1) and defined __BYTES_LITTLE_ENDIAN__ and (__BYTES_LITTLE_ENDIAN__=1)}
	{$error Conflicting initial definitions for __BYTES_BIG_ENDIAN__ and __BYTES_LITTLE_ENDIAN__}
{$endc}

{$ifc not defined __ppc__ and not defined __i386__}
	{$ifc defined __BYTES_BIG_ENDIAN__ and (__BYTES_BIG_ENDIAN__=1)}
		{$definec __ppc__ 1}
	{$elifc defined __BYTES_LITTLE_ENDIAN__ and (__BYTES_LITTLE_ENDIAN__=1)}
		{$definec __i386__ 1}
	{$elsec}
		{$error None of __ppc__, __i386__, (__BYTES_BIG_ENDIAN__=1), or (__BYTES_LITTLE_ENDIAN__=1) are defined}
	{$endc}
{$endc}

{$ifc defined __ppc__ and __ppc__}
	{$definec TARGET_CPU_PPC TRUE}
	{$definec TARGET_CPU_X86 FALSE}
{$elifc defined __i386__ and __i386__}
	{$definec TARGET_CPU_PPC FALSE}
	{$definec TARGET_CPU_X86 TRUE}
{$elsec}
	{$error Neither __ppc__ nor __i386__ is defined. Please compile with gp or define __ppc__ or __i386__}
{$endc}
{$definec TARGET_CPU_ARM             FALSE}
{$definec TARGET_CPU_PPC64           FALSE}
{$definec TARGET_CPU_X86_64          FALSE}
{$definec TARGET_CPU_64              FALSE}
{$definec TARGET_OS_MAC              TRUE}
{$definec TARGET_OS_IPHONE           FALSE}
{$definec TARGET_IPHONE_SIMULATOR    FALSE}
{$definec TARGET_OS_EMBEDDED         FALSE}

{$ifc defined __BYTES_BIG_ENDIAN__ and (__BYTES_BIG_ENDIAN__=1)}
	{$definec TARGET_RT_BIG_ENDIAN     TRUE}
	{$definec TARGET_RT_LITTLE_ENDIAN  FALSE}
{$elifc defined __BYTES_LITTLE_ENDIAN__ and (__BYTES_LITTLE_ENDIAN__=1)}
	{$definec TARGET_RT_BIG_ENDIAN     FALSE}
	{$definec TARGET_RT_LITTLE_ENDIAN  TRUE}
{$elsec}
	{$error Neither (__BYTES_BIG_ENDIAN__=1) nor (__BYTES_LITTLE_ENDIAN__=1) is defined.}
{$endc}
EOM
} elsif ( $type eq 'FPC' ) {
	$machoconditionals .= <<'EOM';
{$ifc defined CPUPOWERPC and defined CPUI386}
	{$error Conflicting initial definitions for CPUPOWERPC and CPUI386}
{$endc}
{$ifc defined FPC_BIG_ENDIAN and defined FPC_LITTLE_ENDIAN}
	{$error Conflicting initial definitions for FPC_BIG_ENDIAN and FPC_LITTLE_ENDIAN}
{$endc}

{$ifc not defined __ppc__ and defined CPUPOWERPC32}
	{$setc __ppc__ := 1}
{$elsec}
	{$setc __ppc__ := 0}
{$endc}
{$ifc not defined __ppc64__ and defined CPUPOWERPC64}
	{$setc __ppc64__ := 1}
{$elsec}
	{$setc __ppc64__ := 0}
{$endc}
{$ifc not defined __i386__ and defined CPUI386}
	{$setc __i386__ := 1}
{$elsec}
	{$setc __i386__ := 0}
{$endc}
{$ifc not defined __x86_64__ and defined CPUX86_64}
	{$setc __x86_64__ := 1}
{$elsec}
	{$setc __x86_64__ := 0}
{$endc}
{$ifc not defined __arm__ and defined CPUARM}
	{$setc __arm__ := 1}
{$elsec}
	{$setc __arm__ := 0}
{$endc}
{$ifc not defined __arm64__ and defined CPUAARCH64}
  {$setc __arm64__ := 1}
{$elsec}
  {$setc __arm64__ := 0}
{$endc}

{$ifc defined cpu64}
  {$setc __LP64__ := 1}
{$elsec}
  {$setc __LP64__ := 0}
{$endc}


{$ifc defined __ppc__ and __ppc__ and defined __i386__ and __i386__}
	{$error Conflicting definitions for __ppc__ and __i386__}
{$endc}

{$ifc defined __ppc__ and __ppc__}
	{$setc TARGET_CPU_PPC := TRUE}
	{$setc TARGET_CPU_PPC64 := FALSE}
	{$setc TARGET_CPU_X86 := FALSE}
	{$setc TARGET_CPU_X86_64 := FALSE}
	{$setc TARGET_CPU_ARM := FALSE}
	{$setc TARGET_CPU_ARM64 := FALSE}
	{$setc TARGET_OS_MAC := TRUE}
	{$setc TARGET_OS_IPHONE := FALSE}
	{$setc TARGET_IPHONE_SIMULATOR := FALSE}
	{$setc TARGET_OS_EMBEDDED := FALSE}
{$elifc defined __ppc64__ and __ppc64__}
	{$setc TARGET_CPU_PPC := FALSE}
	{$setc TARGET_CPU_PPC64 := TRUE}
	{$setc TARGET_CPU_X86 := FALSE}
	{$setc TARGET_CPU_X86_64 := FALSE}
	{$setc TARGET_CPU_ARM := FALSE}
	{$setc TARGET_CPU_ARM64 := FALSE}
	{$setc TARGET_OS_MAC := TRUE}
	{$setc TARGET_OS_IPHONE := FALSE}
	{$setc TARGET_IPHONE_SIMULATOR := FALSE}
	{$setc TARGET_OS_EMBEDDED := FALSE}
{$elifc defined __i386__ and __i386__}
	{$setc TARGET_CPU_PPC := FALSE}
	{$setc TARGET_CPU_PPC64 := FALSE}
	{$setc TARGET_CPU_X86 := TRUE}
	{$setc TARGET_CPU_X86_64 := FALSE}
	{$setc TARGET_CPU_ARM := FALSE}
	{$setc TARGET_CPU_ARM64 := FALSE}
{$ifc defined iphonesim}
 	{$setc TARGET_OS_MAC := FALSE}
	{$setc TARGET_OS_IPHONE := TRUE}
	{$setc TARGET_IPHONE_SIMULATOR := TRUE}
{$elsec}
	{$setc TARGET_OS_MAC := TRUE}
	{$setc TARGET_OS_IPHONE := FALSE}
	{$setc TARGET_IPHONE_SIMULATOR := FALSE}
{$endc}
	{$setc TARGET_OS_EMBEDDED := FALSE}
{$elifc defined __x86_64__ and __x86_64__}
	{$setc TARGET_CPU_PPC := FALSE}
	{$setc TARGET_CPU_PPC64 := FALSE}
	{$setc TARGET_CPU_X86 := FALSE}
	{$setc TARGET_CPU_X86_64 := TRUE}
	{$setc TARGET_CPU_ARM := FALSE}
	{$setc TARGET_CPU_ARM64 := FALSE}
{$ifc defined iphonesim}
 	{$setc TARGET_OS_MAC := FALSE}
	{$setc TARGET_OS_IPHONE := TRUE}
	{$setc TARGET_IPHONE_SIMULATOR := TRUE}
{$elsec}
	{$setc TARGET_OS_MAC := TRUE}
	{$setc TARGET_OS_IPHONE := FALSE}
	{$setc TARGET_IPHONE_SIMULATOR := FALSE}
{$endc}
	{$setc TARGET_OS_EMBEDDED := FALSE}
{$elifc defined __arm__ and __arm__}
	{$setc TARGET_CPU_PPC := FALSE}
	{$setc TARGET_CPU_PPC64 := FALSE}
	{$setc TARGET_CPU_X86 := FALSE}
	{$setc TARGET_CPU_X86_64 := FALSE}
	{$setc TARGET_CPU_ARM := TRUE}
	{$setc TARGET_CPU_ARM64 := FALSE}
	{$setc TARGET_OS_MAC := FALSE}
	{$setc TARGET_OS_IPHONE := TRUE}
	{$setc TARGET_IPHONE_SIMULATOR := FALSE}
	{$setc TARGET_OS_EMBEDDED := TRUE}
{$elifc defined __arm64__ and __arm64__}
	{$setc TARGET_CPU_PPC := FALSE}
	{$setc TARGET_CPU_PPC64 := FALSE}
	{$setc TARGET_CPU_X86 := FALSE}
	{$setc TARGET_CPU_X86_64 := FALSE}
	{$setc TARGET_CPU_ARM := FALSE}
	{$setc TARGET_CPU_ARM64 := TRUE}
{$ifc defined ios}
	{$setc TARGET_OS_MAC := FALSE}
	{$setc TARGET_OS_IPHONE := TRUE}
	{$setc TARGET_OS_EMBEDDED := TRUE}
{$elsec}
	{$setc TARGET_OS_MAC := TRUE}
	{$setc TARGET_OS_IPHONE := FALSE}
	{$setc TARGET_OS_EMBEDDED := FALSE}
{$endc}
	{$setc TARGET_IPHONE_SIMULATOR := FALSE}
{$elsec}
	{$error __ppc__ nor __ppc64__ nor __i386__ nor __x86_64__ nor __arm__ nor __arm64__ is defined.}
{$endc}

{$ifc defined __LP64__ and __LP64__ }
  {$setc TARGET_CPU_64 := TRUE}
{$elsec}
  {$setc TARGET_CPU_64 := FALSE}
{$endc}

{$ifc defined FPC_BIG_ENDIAN}
	{$setc TARGET_RT_BIG_ENDIAN := TRUE}
	{$setc TARGET_RT_LITTLE_ENDIAN := FALSE}
{$elifc defined FPC_LITTLE_ENDIAN}
	{$setc TARGET_RT_BIG_ENDIAN := FALSE}
	{$setc TARGET_RT_LITTLE_ENDIAN := TRUE}
{$elsec}
	{$error Neither FPC_BIG_ENDIAN nor FPC_LITTLE_ENDIAN are defined.}
{$endc}
EOM
} else {
	die;
}
$machoconditionals .= <<'EOM';
{$definec ACCESSOR_CALLS_ARE_FUNCTIONS   TRUE}
{$definec CALL_NOT_IN_CARBON             FALSE}
{$definec OLDROUTINENAMES                FALSE}
{$definec OPAQUE_TOOLBOX_STRUCTS         TRUE}
{$definec OPAQUE_UPP_TYPES               TRUE}
{$definec OTCARBONAPPLICATION            TRUE}
{$definec OTKERNEL                       FALSE}
{$definec PM_USE_SESSION_APIS            TRUE}
{$definec TARGET_API_MAC_CARBON          TRUE}
{$definec TARGET_API_MAC_OS8             FALSE}
{$definec TARGET_API_MAC_OSX             TRUE}
{$definec TARGET_CARBON                  TRUE}
{$definec TARGET_CPU_68K                 FALSE}
{$definec TARGET_CPU_MIPS                FALSE}
{$definec TARGET_CPU_SPARC               FALSE}
{$definec TARGET_OS_UNIX                 FALSE}
{$definec TARGET_OS_WIN32                FALSE}
{$definec TARGET_RT_MAC_68881            FALSE}
{$definec TARGET_RT_MAC_CFM              FALSE}
{$definec TARGET_RT_MAC_MACHO            TRUE}
{$definec TYPED_FUNCTION_POINTERS        TRUE}
{$definec TYPE_BOOL                      FALSE}
{$definec TYPE_EXTENDED                  FALSE}
{$definec TYPE_LONGLONG                  TRUE}
EOM
chomp $machoconditionals;
our $cfmconditionals = $machoconditionals;
$cfmconditionals =~ s!(definec\s+TARGET_RT_MAC_CFM\s+)TRUE!${1}FALSE!;
$cfmconditionals =~ s!(definec\s+TARGET_RT_MAC_MACHO\s+)FALSE!${1}TRUE!;
our $gpcconditionals = <<EOM;
{\$definec UNIVERSAL_INTERFACES_VERSION \$0400}
{\$definec GAP_INTERFACES_VERSION \$0$GAP}
{\$definec FOUR_CHAR_CODE(s) ((Ord (substr((s),1,1)[1]) shl 24) or (Ord(substr((s),2,1)[1]) shl 16) or (Ord (substr((s),3,1)[1]) shl 8) or Ord(substr((s),4,1)[1]))}

$machoconditionals
EOM
our $defineUSE_CFSTR_CONSTANT_MACROS = <<'EOM';
{$ifc not defined USE_CFSTR_CONSTANT_MACROS}
    {$definec USE_CFSTR_CONSTANT_MACROS TRUE}
{$endc}
EOM

our %gpcbadconst = ();
our %mwbadconst = ();
our %fpcbadconst = ();
while ( <DATA> ) {
	chomp;
	last if m!^END$!;
	die $_ unless /^(\w+): (GPC)?,?(MW)?,?(FPC)?$/;
	$gpcbadconst{$1} = 1 if $2;
	$mwbadconst{$1} = 1 if $3;
	$fpcbadconst{$1} = 1 if $4;
}

our ( $unitname, $sourcefile, $outputfile );
if ( $output =~ m!\.inc$! ) {
	make_gpc_inc( $source, $output );
} elsif ( -d $source ) {
	foreach my $filename ( sort( grep( m!\.pas$!, read_dir( $source ) ) ) ) {
		next if $filename =~ m!^GPCString!;
		next if $filename =~ m!^FPCString!;
		print STDERR "$filename\n";
		process_file( "$source/$filename", "$output/$filename" );
	}
	if ( $type eq 'GPC' ) {
		process_file( "$source/GPCStrings.pas", "$output/GPCStrings.pas" );
		process_file( "$source/GPCStrings.pas", "$output/GPCStringsAll.pas" );
	}
	if ( $type eq 'FPC' ) {
		process_file( "$source/FPCStrings.pas", "$output/GPCStrings.pas" );
	}
		
} else {
	process_file( $source, $output );
}

sub process_file {
	( $sourcefile, $outputfile ) = @_;
	($unitname = $outputfile) =~ s!.*/(.*)\..*!$1!;
	$data = read_file( $sourcefile );
	my $implementation;
	if ( $data =~ s!^(implementation\n.*(^end.\s*))\Z!$2!sm ) {
		$implementation = $1;
		process_ONLY_tokens( \$implementation );
		$implementation =~ s!(;\s*)(inline)\s*;!$1(* $2 *)!g unless $type eq 'FPC';
	}
	$add_before = '';
	$add_after = '';
	$creator = 'R*ch';
	
	if ( $type eq 'GPC' ) {
		convert_to_gpc();
	} elsif ( $type eq 'FPC' ) {
		convert_to_fpc();
	} elsif ( $type eq 'MW' ) {
		convert_to_mw();
	} else {
		die;
	}
	
	$data =~ s!(\nunit \w+;\ninterface\n)!\n$add_before$1$add_after! or die "Cannot insert data in $outputfile";
	$data =~ s!\{\n\}\n!!g;
	if ( $implementation ) {
		$data =~ s!(^end.\s*)\Z!$implementation!sm;
	}
	
	replace_file_with_data( $outputfile, $data, {creator=>$creator, type=>'TEXT', touch=>1} );
}

sub make_gpc_inc {
	( $source, $outputfile ) = @_;

	my $grep = `grep CFSTR "$source"/*.pas`;
	my %cfstrs;
	foreach my $line ( split( /\n/, $grep ) ) {
		$line =~ s!^[^:]+:!!;
		if ( $line =~ m!^[ \t]*const\s+(\w+)\s*=\s*CFSTR\s*\(\s*'([^'"]*)'\s*\)\s*;! ) {
			$cfstrs{$1} = $2;
		}
	}
	my $cfstrs = '';
	foreach my $key ( sort keys %cfstrs ) {
		$cfstrs .= "  	{\$definec $key CFSTRP('$cfstrs{$key}')}\n";
	}
	chomp $cfstrs;

	$grep = `grep gpcdefinec "$source"/*.pas`;
	my $gpcextramacros = '';
	foreach my $line ( split( /\n/, $grep ) ) {
		$line =~ s!^[^:]+:!!;
		if ( $line =~ m#^[ \t]*({\$)(?:mw)?gpc(definec\s+)(?:([!A-Za-z0-9_]+):)?(.*)# ) {
			my ( $lead, $definec, $ifc, $rest ) = ($1, $2, $3, $4);
			$gpcextramacros .= "{\$ifc $ifc}\n" if $ifc;
			$gpcextramacros .= "$lead$definec$rest\n";
			$gpcextramacros .= "{\$endc}\n" if $ifc;
		}
	}
	chomp $gpcextramacros;

	$data = <<EOM;
{\$ifdef __GPC__}
$gpcconditionals
$defineUSE_CFSTR_CONSTANT_MACROS
{\$ifc USE_CFSTR_CONSTANT_MACROS}
$cfstrs
{\$endc}

$gpcextramacros

{\$endc}
EOM
	replace_file_with_data( $outputfile, $data, {creator=>$creator, type=>'TEXT', touch=>1} );
}

sub convert_to_gpc {
	$creator = 'R*ch';
	$add_before = <<EOM;
{
    Modified for use with GNU Pascal
    Version $GAP
    Please report any bugs to <gpc\@microbizz.nl>
}

{\$gnu-pascal}
{\$w no-underscore}
{\$w no-identifier-case-local}
{\$maximum-field-alignment=16}
{\$disable-keyword=pow}
EOM
	$add_after = <<EOM;
{\$definec UNIVERSAL_INTERFACES_VERSION \$0400}
{\$definec GAP_INTERFACES_VERSION \$0$GAP}
{\$definec FOUR_CHAR_CODE(s) ((Ord (substr((s),1,1)[1]) shl 24) or (Ord(substr((s),2,1)[1]) shl 16) or (Ord (substr((s),3,1)[1]) shl 8) or Ord(substr((s),4,1)[1]))}

$machoconditionals
EOM

	print STDERR "Warning: No alignment directive\nbbedit $sourcefile\n" unless $data =~ m!{\$ALIGN!;

	process_ONLY_tokens( \$data );

	$data =~ s!{\$ALIGN MAC68K}!{\$maximum-field-alignment=16}!g;
	$data =~ s!{\$ALIGN POWER}!{\$maximum-field-alignment=32}!g;
	
	$data =~ s!^\{\$(?:mw)?gpc(definec.*)}$!{#$1}!gm;

	if ( $unitname eq 'GPCStringsAll' ) {
		$data =~ s!^(unit GPCStrings);!${1}All;!m or die "cant fix GPCStrings unit name";
		$data =~ s!^uses MacTypes;!uses MacOSAll;!m or die "cant fix GPCStrings uses clause";
	}
	return if $unitname =~ m!^GPCString!;
	
	$data =~ s!(\n[ \t]*procedure\s+(\w+)\s*(\([^)]*\))?;)!$1 external name '$2';!g;
	$data =~ s!(\n[ \t]*function\s+(\w+)\s*(\([^)]*\))?:[\s\n]*\w+[\s\n]*(?:{[^}]*}[\s\n]*)?;)!$1 external name '$2';!g;
	$data =~ s!(\n[ \t]*)const(\s+(\w+)\s*({[^}]*}\s*)?:\s*[^;:()]*);!${1}var${2}; external name '$3'; attribute (const);!g;
	$data =~ s!(\n[ \t]*)implemented\s+(procedure\s+(\w+)\s*(\([^)]*\))?;)!$1$2!g;
	$data =~ s!(\n[ \t]*)implemented\s+(function\s+(\w+)\s*(\([^)]*\))?:[\s\n]*\w+[\s\n]*;)!$1$2!g;
	$data =~ s!(\n[ \t]*)implemented\s+const(\s+(\w+)\s*:\s*[^;:()]*);!${1}var${2}; attribute (const);!g;
#	$data =~ s!\bconst var(\s+\w+\s*(?:,\s*\w+\s*)*:\s*(\w+))!($gpcbadconst{$2} ? '(*const*) var' : 'const (*var*)').$1!ge;
	$data =~ s!\buniv\s+Ptr\b!UnivPtr!g;
	$data =~ s!\buniv\s+Handle\b!UnivHandle!g;
	$data =~ s!\n[ \t]*const\s+(\w+)\s*=\s*CFSTR\s*\(\s*'([^'"]*)'\s*\)\s*;!\n{ $1 = CFSTR("$2"); defined in GPCMacros.inc }!g;
	$data =~ s!(;\s*)(inline)\s*;!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:{[^}]*})?\s*)(AVAILABLE_MAC_OS_X_VERSION_10_[0-9]_AND_LATER(?:_BUT_DEPRECATED(?:_IN_MAC_OS_X_VERSION_10_[0-9])?)?);!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:{[^}]*})?\s*)(DEPRECATED_IN_MAC_OS_X_VERSION_10_[0-9]_AND_LATER);!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:{[^}]*})?\s*)([A-Z_]*_AVAILABLE_(?:BUT_DEPRECATED|STARTING_?)+\([^)]*\));!$1(* $2 *)!g;
	$data =~ s!(;\s*(?:{[^}]*})?\s*)($avail_re);!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:\(\* attribute (?:const|ignoreable) \*\)\s*(?:{[^}]*})?\s*)?)([A-Z_]*_(?:DEPRECATED|AVAILABLE)\([^)]*\));!$1(* $2 *)!g;
	$data =~ s!(;\s*(?:\(\* attribute (?:const|ignoreable) \*\)\s*(?:{[^}]*})?\s*)?)($avail_re);!$1(* $2 *)!g;
	$data =~ s!'(\w+)__NAMED__(\w+)'!'$2'!g;
	$data =~ s!(\w+)__NAMED__(\w+)!$1!g;
}

sub convert_to_fpc {
	$creator = 'R*ch';
	$add_before = <<EOM;
{
    Modified for use with Free Pascal
    Version $GAP
    Please report any bugs to <gpc\@microbizz.nl>
}

{\$mode macpas}
{\$modeswitch cblocks}
{\$packenum 1}
{\$macro on}
{\$inline on}
{\$calling mwpascal}
EOM
	$add_after = <<EOM;
{\$definec UNIVERSAL_INTERFACES_VERSION \$0400}
{\$definec GAP_INTERFACES_VERSION \$0$GAP}

$defineUSE_CFSTR_CONSTANT_MACROS
$machoconditionals
EOM

	process_ONLY_tokens( \$data );

	$data =~ s!{\$propagate-units}\n!!g;

	return if $unitname =~ m!^GPCString!;
#	$data =~ s!(\n[ \t]*procedure[\s\n]+)(\w+)__NAMED__(\w+)([\s\n]*(\([^)]*\))?;)!$1$2$4 external name '$3';!gi;
#	$data =~ s!(\n[ \t]*function[\s\n]+)(\w+)__NAMED__(\w+)([\s\n]*(\([^)]*\))?:[\s\n]*\w+[\s\n]*;)!$1$2$4 external name '$3';!gi;
#	$data =~ s!(\n[ \t]*)const(\s+)(\w+)__NAMED__(\w+)(\s*:\s*[^;:()]*);!${1}var$2$3$5; external name '$4'; attribute (const);!g;
	$data =~ s!(\n[ \t]*procedure\s+(\w+)\s*(\([^)]*\))?;)!$1 external name '_$2';!g;
	$data =~ s!(\n[ \t]*function\s+(\w+)\s*(\([^)]*\))?:[\s\n]*\w+[\s\n]*(?:{[^}]*}[\s\n]*)?;)!$1 external name '_$2';!g;
	$data =~ s!(\n[ \t]*)const(\s+(\w+)\s*({[^}]*}\s*)?:\s*[^;:()]*);!${1}var${2}; external name '_$3'; (* attribute const *)!g;
	$data =~ s!(\n[ \t]*)implemented\s+(procedure|function|const)\b!$1$2!g;

	$data =~ s!^\{\$(?:mw)?gpc(definec.*)}$!{#$1}!gm;

	$data =~ s!;\s*attribute\s*\(\s*ignorable\s*\);!; (* attribute ignoreable *)!g;
	$data =~ s!\{\$ifc not defined !{\$ifc undefined !gi;
	$data =~ s!\{\$ifc defined !{\$ifc not undefined !gi;

	$data =~ s!\bconst var(\s+\w+\s*(?:,\s*\w+\s*)*:\s*(\w+))!($fpcbadconst{$2} ? '(*const*) var' : 'const (*var*)').$1!ge;
	$data =~ s!\buniv\s+Ptr\b!UnivPtr!g;

	$data =~ s!\{\$definec (\w+)\s+(.*)\}!{\$setc $1 := $2}!g;
	$add_before =~ s!\{\$definec (\w+)\s+(.*)\}!{\$setc $1 := $2}!g;
	$add_after =~ s!\{\$definec (\w+)\s+(.*)\}!{\$setc $1 := $2}!g;

#	$data =~ s!\bFOUR_CHAR_CODE\('([^']{4})'\)!sprintf( q#$%08X (* '%s' *)#, unpack( "N", $1 ), $1 )!mge;
	$data =~ s!\bFOUR_CHAR_CODE\('([^']{4})'\)!FourCharCode\('$1'\)!g;
	
	$data =~ s!^\s*const\s+(\w+)\s*=\s*"([^"']*)";!const $1 = '$2';!gm;
	$data =~ s!(\n[ \t]*\w+[ \t]*=[ \t]*)"([^"\n]*)";!$1'$2';!gm;

	$data =~ s!\n[ \t]*const\s+(\w+)\s*=\s*CFSTR\s*\(\s*'([^'"]*)'\s*\)\s*;!\n{\$ifc USE_CFSTR_CONSTANT_MACROS}\n{\$definec $1 CFSTRP('$2')}\n{\$endc}!g;

# 	$data =~ s!(;\s*(?:\(\* attribute (?:const|ignoreable) \*\)\s*(?:{[^}]*})?\s*)?)(AVAILABLE_MAC_OS_X_VERSION_10_[0-8]_AND_LATER(?:_BUT_DEPRECATED(?:_IN_MAC_OS_X_VERSION_10_[0-8])?)?);!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:\(\* attribute (?:const|ignoreable) \*\)\s*(?:{[^}]*})?\s*)?)(DEPRECATED_IN_MAC_OS_X_VERSION_10_[0-8]_AND_LATER);!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:\(\* attribute (?:const|ignoreable) \*\)\s*(?:{[^}]*})?\s*)?)([A-Z_]*_AVAILABLE_(?:BUT_DEPRECATED|STARTING_?)+\([^)]*\));!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:\(\* attribute (?:const|ignoreable) \*\)\s*(?:{[^}]*})?\s*)?)([A-Z_]*_(?:DEPRECATED|AVAILABLE)\([^)]*\));!$1(* $2 *)!g;
	$data =~ s!(;\s*(?:\(\* attribute (?:const|ignoreable) \*\)\s*(?:{[^}]*})?\s*)?)($avail_re);!$1(* $2 *)!g;
	$data =~ s!'(\w+)__NAMED__(\w+)'!'_$2'!g;
	$data =~ s!(\w+)__NAMED__(\w+)!$1!g;
}

sub convert_to_mw {
	$creator = 'CWIE';

	$add_before = <<EOM;
{
    Modified for use with MW Pascal
    Version $GAP
    Please report any bugs to <gpc\@microbizz.nl>
}
EOM
	$add_after = <<EOM;
{\$definec UNIVERSAL_INTERFACES_VERSION \$0400}
{\$definec GAP_INTERFACES_VERSION \$0$GAP}
{\$definec FOUR_CHAR_CODE(s) (s)}

{\$ifc undefined USE_CFSTR_CONSTANT_MACROS}
    {\$definec USE_CFSTR_CONSTANT_MACROS TRUE}
{\$endc}

$cfmconditionals
EOM

	process_ONLY_tokens( \$data );

	$data =~ s!{\$propagate-units}\n!!g;

	$data =~ s!(\n[ \t]*)implemented\s+(procedure|function|const)\b!$1$2!g;

	$data =~ s!\b(\w+)__NAMED__(\w+)\b!$2!g;  # MW cannot handle external names

	$data =~ s#^[ \t]*({\$)(?:mw)?gpc(definec\s+)(?:([!A-Za-z0-9_]+):)?(.*)#$1$2$4#gm;

	$data =~ s!;\s*attribute\s*\(\s*ignorable\s*\);!; (* attribute ignoreable *)!g;
	$data =~ s!\{\$ifc not defined !{\$ifc undefined !gi;
	$data =~ s!\{\$ifc defined !{\$ifc not undefined !gi;

# 	$data =~ s!^([ \t]*)const(\s+\w+\s*({[^}]*}\s*)?:\s*[^;:()]*);(.*)$!{\$J+}\n${1}var${2}; (* attribute const *)$3\n{\$J-}!gm;
	$data =~ s!^([ \t]*)const(\s+\w+\s*({[^}]*}\s*)?:\s*[^;:()]*);(.*)$!{\$J+}\n${1}var${2}; (* attribute const *)$4\n{\$J-}!gm;
	$data =~ s!\{\$J-}\n\{\$J\+}\n!!g;
	
	$data =~ s!\bconst var(\s+\w+\s*(?:,\s*\w+\s*)*:\s*(\w+))!($mwbadconst{$2} ? '(*const*) var' : 'const (*var*)').$1!ge;

	$data =~ s!(?<=\(| )(\$[A-Fa-f0-9]+|\d+) shl (\d+)(?=\)|;)!$2 <= 15 ? "$1 shl $2" : "UInt32($1) shl $2"!ge;
	$data =~ s!\n[ \t]*const\s+(\w+)\s*=\s*CFSTR\s*\(\s*'([^'"]*)'\s*\)\s*;!\n{\$ifc USE_CFSTR_CONSTANT_MACROS}\n{\$definec $1 CFSTRP('$2')}\n{\$endc}!g;

	$data =~ s!(;\s*)(inline);!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:\(\* attribute const \*\)\s*)?)(?:\s*\{\$J-\}\s*)?(AVAILABLE_MAC_OS_X_VERSION_10_[0-8]_AND_LATER(?:_BUT_DEPRECATED(?:_IN_MAC_OS_X_VERSION_10_[0-8])?)?);!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:\(\* attribute const \*\)\s*)?)(?:\s*\{\$J-\}\s*)?(DEPRECATED_IN_MAC_OS_X_VERSION_10_[0-8]_AND_LATER);!$1(* $2 *)!g;
# 	$data =~ s!(;\s*(?:\(\* attribute const \*\)\s*)?)(?:\s*\{\$J-\}\s*)?([A-Z_]*_AVAILABLE_(?:BUT_DEPRECATED|STARTING_?)+\([^)]*\));!$1(* $2 *)!g;
	$data =~ s!(;\s*(?:\(\* attribute const \*\)\s*)?)(?:\s*\{\$J-\}\s*)?($avail_re);!$1(* $2 *)!g;
}

sub process_ONLY_tokens {
	my( $data ) = @_;
	
	$$data =~ s!^\{([A-Z_\-]+)-ONLY-START}\n((?:.|\n)*?\n)\{\1-ONLY-FINISH}\n!
		my ( $which, $texton ) = ($1, $2);
		my $textoff = '';
		if ( $texton =~ m#((?:.|\n)*?\n)\{$which-ONLY-ELSE\}\n((?:.|\n)*)$# ) {
			$texton = $1;
			$textoff = $2;
		}
		$which =~ m|\b$type\b| ? $texton : $textoff;
	!gme;
}

__DATA__
Fixed: GPC,MW,FPC
Double: GPC,MW,FPC
LongDateTime: GPC,FPC
LongDouble: GPC,MW,FPC
Point: GPC
SInt32: GPC,MW,FPC
SInt64: GPC,FPC
Single: GPC,MW,FPC
TimeValue64: GPC,FPC
UInt64: GPC,FPC
decform: GPC
fenv_t: GPC,MW,FPC
fexcept_t: GPC,MW,FPC
ConstCStringPtr: GPC,MW,FPC
NumVersion: GPC
END
