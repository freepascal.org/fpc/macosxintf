#!/usr/bin/perl

use strict;
use warnings;
use lib 'Scripts';

use MetaPascalUtils qw( read_file replace_file_with_data );

my $source = shift || die "Source?";
my $unitlist = shift || die "Source?";
my $output = shift || die "Output?";
die "Source is not a folder" unless -d $source;
die "UnitList is not UnitList.txt" unless -f $unitlist && -r $unitlist && $unitlist =~ m!/UnitList\.txt$!;
die "Output is not in the Build folder" unless $output =~ m!/Build/!;
die "Output is not text target file" unless $output =~ m!\.txt$!;

my $result = '';

our %units = ();

open( UNITLIST, $unitlist ) or die "Cant open $unitlist $!";
our @units = ('GPCStrings');
while ( <UNITLIST> ) {
	chomp;
	push @units, $_;
}
close( UNITLIST );
@units = sort( @units );
foreach my $unitname ( @units ) {
	my $file = "$unitname.pas";
	my $data = read_file( "$source/$file" ) || die;
	if ( $data =~ m!^unit $unitname;\ninterface\nuses\s+([^;]+);!m ) {
		$units{$unitname} = $1;
		$units{$unitname} =~ tr/\r\n\t //d;
	} else {
		$units{$unitname} = '';
	}
}
close( UNITLIST );

closure( \%units );
my %done = ();
my @sortedunits = ();

my $finished = 0;
do {
	$finished = 1;
	foreach my $unit ( @units ) {
		next if $done{$unit};
		my $good = 1;
		foreach my $uses (split( /,/, $units{$unit})) {
			$good = 0, last if !$done{$uses};
		}
		if ( $good ) {
			push @sortedunits, $unit;
			$done{$unit} = 1;
			$finished = 0;
		}
	}
} until $finished;
  
my $die = 0;
foreach my $unit (@units) {
	$die = 1, print STDERR $unit.':'.$units{$unit}."\n" unless $done{$unit};
}
die "Cant generate closure" if $die;

foreach my $unit (@sortedunits) {
	$result .= "$unit:$units{$unit}\n";
}

replace_file_with_data( $output, $result, {creator=>'R*ch', type=>'TEXT', touch=>1} );

sub closure {
  my( $map ) = @_;
  
  my @elements = sort keys %{$map};
  
  my $finished;
  do {
    $finished = 1;
    foreach my $elem (@elements) {
      my $newval = ','.$map->{$elem}.',';
      foreach my $uses (split( /,/, $map->{$elem})) {
      	if ( !defined($map->{$uses}) ) {
      		die "$elem uses $uses which does not exist";
      	}
        foreach my $ituses (split( /,/, $map->{$uses})) {
          if ( $newval !~ /,$ituses,/ ) {
            $newval .= $ituses.',';
            $finished = 0;
          }
        }
      }
      $newval =~ s/,,/,/g;
      $newval =~ s/^,+//;
      $newval =~ s/,+$//;
      $map->{$elem} = $newval;
    }
  } until $finished;
}

