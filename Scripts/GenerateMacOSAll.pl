#!/usr/bin/perl

use strict;
use warnings;
use lib 'Scripts';

use utf8;

use MetaPascalUtils qw( read_file replace_file_with_data );

my $type = shift || die "Type?";
my $source = shift || die "Source?";
my $unitdependencylist = shift || die "UnitDependencyList?";
my $useincludes = shift || die "UseIncludes?";
my $output = shift || die "Output?";
die "Type is not GPC/FPC/MW" unless $type =~ m!\A(GPC|FPC|MW)\Z!;
die "Source is not a folder" unless -d $source;
die "UnitDependencyList is not UnitDependencyList.txt" unless -f $unitdependencylist && -r $unitdependencylist && $unitdependencylist =~ m!/UnitDependencyList\.txt$!;
die "Output is not in the Build folder" unless $output =~ m!/Build/!;
die "Output is not pascal target file" unless $output =~ m!\.p(as)?$!;
die "UseIncludes is not YES or NO" unless $useincludes =~ m!\A(YES|NO)\Z!;

our $extension = $type eq 'MW' ? 'p' : 'pas';

our $data = '';
our %filedata = ();
our %fileimplementation = ();
our %units = ();
our @units = ();
our $add_before;
our $add_after;
my  $includestart = "{\$ifc not defined MACOSALLINCLUDE or not MACOSALLINCLUDE}\n";
my  $includeend = "{\$endc} {not MACOSALLINCLUDE}\n";

(our $macosall = $output) =~ s!.*/(.*)\..*!$1!;
our $creator = $type eq 'MW' ? 'CWIE' : 'R*ch';

my $mactypes = read_file( "$source/MacTypes.$extension" ) or die "Cant read MacTypes.$extension $!";
if ( $mactypes =~ m!^({\$(?:.|\n)+?\n)unit \w+;\ninterface\n!m ) {
	$add_before = $1;
} else {
	$add_before = '';
}
if ( $mactypes =~ m!\nunit \w+;\ninterface\n((?:.|\n)+?\n)uses\s+! ) {
	$add_after = $1;
} else {
	$add_after = '';
}

open( LIST, $unitdependencylist ) or die "Cant open $unitdependencylist $!";
while ( <LIST> ) {
	chomp;
	m!(.*):(.*)! or die;
	my $unitname = $1;
	$units{$unitname} = $2;
	my $file = "$unitname.$extension";
	next if $unitname eq 'GPCStrings';
	next if $unitname eq 'FPCStrings';
	push @units, $unitname;
	if ( $useincludes eq 'NO') {
		$filedata{$unitname} = read_file( "$source/$file" );
		$filedata{$unitname} =~ 	s!{\$propagate-units}\n!!g;
		$filedata{$unitname} =~ s!\n[ \t]*end\.\s*\Z!! or die "Missing end in $unitname";
		if ( $filedata{$unitname} =~ s!^implementation\n(.*)!!sm ) {
			$fileimplementation{$unitname} = $1;
		}
		if ( $add_before ) {
			$filedata{$unitname} =~ s!\Q$add_before\E!!;
		}
		if ( $add_after ) {
			$filedata{$unitname} =~ s!\Q$add_after\E!!;
		}
		$filedata{$unitname} =~ s!^unit $unitname;\ninterface\nuses\s+[^;]+;!!m 
					|| $filedata{$unitname} =~ s!^unit $unitname;\ninterface\n!!m 
					|| die "Cant remove unit/interface for $unitname";
	} else {
		my $tempfile = read_file( "$source/$file" );
		$filedata{$unitname} = "{\$i $file}";
		if ( $tempfile =~ s!^(implementation\n)(.*)(\nend\.\s*)\Z!$includestart$1$2$3\n$includeend!sm ) {
			$fileimplementation{$unitname} = $2;
		} else {
			$tempfile =~ s!(\n[ \t]*end\.\s*)\Z!$includestart$1$includeend! or die "Missing end in $unitname";
		}
		$tempfile =~ 	s!({\$propagate-units})\n!$includestart$1$includeend!g;
		$tempfile =~ s!^((?:\Q$add_before\E)unit $unitname;\ninterface\n(?:\Q$add_after\E)uses\s+[^;]+;)!$includestart$1\n$includeend!m 
					|| $tempfile =~ s!^((?:\Q$add_before\E)unit $unitname;\ninterface\n(?:\Q$add_after\E))!$includestart$1$includeend!m 
					|| die "Cant remove unit/interface for $unitname";
		replace_file_with_data( "$source/$file", $tempfile, {creator=>$creator, type=>'TEXT', touch=>1} );
	}
}
close( LIST );

if ( $useincludes eq 'YES') {
  if ( $type eq 'FPC' ) {
		$add_before .= "{\$setc MACOSALLINCLUDE := TRUE}\n";
	} else {
		$add_before .= "{\$definec MACOSALLINCLUDE TRUE}\n";
	}
}

$data .= <<EOM;
{
    This file is assembled from all the Interface files.
}
${add_before}unit $macosall;
interface
$add_after
EOM

foreach my $unit (@units) {
	$data .= <<EOM;
{unit $unit}
$filedata{$unit}
EOM
}

$data .= <<EOM;

implementation

EOM

foreach my $unit (@units) {
	$data .= <<EOM if $fileimplementation{$unit};
{implementation of unit $unit}
$fileimplementation{$unit}
EOM
}

$data .= <<EOM;

end.
EOM

replace_file_with_data( $output, $data, {creator=>$creator, type=>'TEXT', touch=>1} );


sub closure {
  my( $map ) = @_;
  
  my @elements = sort keys %{$map};
  
  my $finished;
  do {
    $finished = 1;
    foreach my $elem (@elements) {
      my $newval = ','.$map->{$elem}.',';
      foreach my $uses (split( /,/, $map->{$elem})) {
      	if ( !defined($map->{$uses}) ) {
      		die "$elem uses $uses which does not exist";
      	}
        foreach my $ituses (split( /,/, $map->{$uses})) {
          if ( $newval !~ /,$ituses,/ ) {
            $newval .= $ituses.',';
            $finished = 0;
          }
        }
      }
      $newval =~ s/,,/,/g;
      $newval =~ s/^,+//;
      $newval =~ s/,+$//;
      $map->{$elem} = $newval;
    }
  } until $finished;
}

