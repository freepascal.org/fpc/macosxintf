#!/usr/bin/perl

use lib "$ENV{HOME}/perl";
use strict;
use warnings;

use utf8;

use MetaPascalUtils qw( read_dir read_file );

our $cfile = shift;
our $pfile = shift;

#$cfile = '/Developer/SDKs/MacOSX10.4u.sdk/Developer/Headers/CFMCarbon/CGContext.h';
#$pfile = '/Users/peter/unix/gpc/common-pinterfaces/MWPInterfaces/CGContext.p';

die "C file first" unless $cfile =~ m!\.h$!;
die "Pascal file second" unless $pfile =~ m!\.p(as)?$!;

my $pdata = read_file( $pfile );
$pdata =~ s!^implementation\n.*^end\.$!end.!sm;

$pdata =~ s!\{.*?\}!!gs;
$pdata =~ s!//.*!!g;
$pdata =~ s!\n\n\n+!\n\n!g;

print $pdata;

our %pthings;
while ( $pdata =~ m!(\w+)\s+=!g ) {
	$pthings{$1} = 1;
}
while ( $pdata =~ m!\n\s*(?:function|procedure)\s+(\w+)!gs ) {
	$pthings{$1} = 1;
}

foreach my $known ( sort keys %pthings ) {
	print "P:$known\n";
}

my $cdata = read_file( $cfile );
print $cdata;

$cdata =~ s!/\*.*?\*/!!gs;
$cdata =~ s!//.*!!g;
$cdata =~ s!#.*!!g;
$cdata =~ s!\n\n\n+!\n\n!g;

our @cthings = ();
our %cthings = ();
our @corder = ();
while ( $cdata =~ m!(\w+)\s+=\s*(.*?)\s*[,}]!g ) {
	my( $const, $value ) = ($1, $2);
	$value =~ tr!\n! !;
	push @cthings, "CONST:$const:$value";
	push @corder, $const;
	die "Duplicate $const" if $cthings{$const};
	$cthings{$const} = $cthings[$#cthings];
}
while ( $cdata =~ m!\n\s*(?:extern|CG_EXTERN|CF_EXPORT)\s+(\w+)\s+(\w+)\s*(\(.*?;)!gs ) {
	my( $type, $name, $params ) = ($1, $2, $3);
	$params =~ tr!\n! !;
	push @cthings, "FUNC:$name:$params:$type";
	push @corder, $name;
	warn "Duplicate $name" if $cthings{$name};
	$cthings{$name} = $cthings[$#cthings];
}

foreach my $known ( sort @cthings ) {
	print "C:$known\n";
}

foreach my $known ( @corder ) {
	print "?:$cthings{$known}\n" unless $pthings{$known};
}
