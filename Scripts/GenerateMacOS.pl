#!/usr/bin/perl

use strict;
use warnings;
use lib 'Scripts';

use utf8;

use MetaPascalUtils qw( replace_file_with_data );

my $type = shift || die "Type?";
my $unitlist = shift || die "UnitList?";
my $output = shift || die "Output?";
die "Type is not GPC/FPC/MW" unless $type =~ m!\A(GPC|FPC|MW)\Z!;
die "UnitList is not UnitList.txt" unless -f $unitlist && -r $unitlist && $unitlist =~ m!/UnitList\.txt$!;
die "Output is not in the Build folder" unless $output =~ m!/Build/!;
die "Output is not pascal target file" unless $output =~ m!\.p(as)?$!;

our $data = '';

$data .= <<'EOM' if $type eq 'FPC';
{$mode macpas}
{$packenum 1}
{$macro on}
{$inline on}
{$CALLING MWPASCAL}

EOM
$data .= <<'EOM' if $type eq 'GPC';
{$gnu-pascal}
{$w no-underscore}
{$w no-identifier-case-local}
{$maximum-field-alignment=16}
{$disable-keyword=pow}
{$propagate-units}
EOM
$data .= <<'EOM';
unit MacOS;
interface

uses 
EOM
open( UNITLIST, $unitlist ) or die "Cant open $unitlist $!";
while ( <UNITLIST> ) {
	chomp;
	$data .= "  $_,\n";
}
close( UNITLIST );
$data .= "  GPCStrings,\n" if ( $type eq 'GPC') or ( $type eq 'FPC');
$data =~ s!,\n\Z!;\n!;
$data .= <<'EOM';

end.
EOM

our $creator = $type eq 'MW' ? 'CWIE' : 'R*ch';
replace_file_with_data( $output, $data, {creator=>$creator, type=>'TEXT', touch=>1} );
