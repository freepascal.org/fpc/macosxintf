package MetaPascalUtils;

use 5.006;
use strict;
use warnings;

require Exporter;

our @ISA = qw(Exporter);
our %EXPORT_TAGS = ( 'all' => [ qw(
) ] );
our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );
our @EXPORT = qw(
	read_dir
	read_file
	replace_file_with_data
);
our $VERSION = '1.00';

use utf8;

sub read_dir {
  my( $dir ) = @_;
  
  my @dirs;
  opendir( DIR, $dir ) or die "cant open dir $dir $!";
  foreach ( readdir( DIR ) ) {
  	s!.*/!!;
  	next if m!^\.!;
  	next if $_ eq "Icon\015";
  	push @dirs, $_;
  }
  return @dirs;
}

sub read_file {
  my ($file) = @_;
  my $result = undef;
  local( $/ ) = undef;
  if ( open( my $fh, "<:encoding(MacRoman)", $file ) ) {
    $result = <$fh>;
    close( $fh );
  }
  return $result;
}

sub write_file {
  my ( $file, $data ) = @_;
  my $result = undef;
  if ( open( my $fh, ">:encoding(MacRoman)", $file ) ) {
    print $fh $data;
    close( $fh );
    $result = 1;
  }
  return $result;
}

sub replace_file_with_data {
  my( $file, $data, $params ) = @_;
  
	my $permissions = $params->{permissions};
	my $creator = $params->{creator};
	my $type = $params->{type};
	my $touch = $params->{touch};
  
  my $ret;
  
	chmod( 0666, $file ) if $permissions;
  my $oldfile = eval { read_file( $file ) };
  if (-e $file && $oldfile && $oldfile eq $data) {
  	$ret = 0;
  	if ( $touch ) {
			my $time = time;
			utime $time, $time, $file;
  	}
  } else {
  	write_file( $file, $data );
  	if ( $creator && $type ) {
			system( '/usr/bin/SetFile', '-c', $creator, '-t', $type, $file );
  	} elsif ( $creator ) {
			system( '/usr/bin/SetFile', '-c', $creator, $file );
  	} elsif ( $type ) {
			system( '/usr/bin/SetFile', '-t', $type, $file );
  	}
  	$ret = 1;
  }
	chmod( $permissions, $file ) if $permissions;
	
  return $ret;
}

