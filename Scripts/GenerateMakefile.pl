#!/usr/bin/perl

use strict;
use warnings;
use lib 'Scripts';

use utf8;

use MetaPascalUtils qw( read_dir replace_file_with_data );

my $type = shift || die "Type?";
my $unitdependencylist = shift || die "UnitDependencyList?";
my $output = shift || die "Output?";
die "Type is not GPC/FPC/MW" unless $type =~ m!\A(GPC|FPC|MW)\Z!;
die "UnitDependencyList is not UnitDependencyList.txt" unless -f $unitdependencylist && -r $unitdependencylist && $unitdependencylist =~ m!/UnitDependencyList\.txt$!;
die "Output is not in the Build folder" unless $output =~ m!/Build/!;
die "Output is not Makefile target file" unless $output =~ m!/Makefile$!;

our $extension = 'pas';

our $data;

if ( $type eq 'MW' ) {
	$data = <<'EOM';
# Makefile for Metrowerks Pascal Interfaces.
#
# Author: Adriaan van Os <gpc@microbizz.nl> Date: January 11, 2005.
# Updated: Peter N Lewis <peter@stairways.com.au> Date: August, 2005.

# This file is freeware.
# It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# -----------------------------------------------------------------------------------------------

PATH := $(PATH):/usr/local/bin:/usr/bin:/bin
INTERFACEDIR := /Developer/Pascal/MWPInterfaces

.PHONY: all
all:
	@echo MWPInterfaces not compiled

.PHONY: link
link:
	ln -f -s -h "$(shell pwd)" "$(INTERFACEDIR)"

.PHONY: clean
clean:
	rm -f *.o *.gpi *.gpd *.ppu

.PHONY: cleaner
cleaner:
	rm -f *.o *.gpi *.gpd *.ppu *.a

EOM

} else {
	our $maketemplate = <<'EOM';
# Makefile for %COMPILERNAME% Interfaces.
#
# Author: Adriaan van Os <gpc@microbizz.nl> Date: January 11, 2005.
# Updated: Peter N Lewis <peter@stairways.com.au> Date: August, 2005.

# This file is freeware.
# It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# -----------------------------------------------------------------------------------------------

BUILD := Build
PATH := $(PATH):/usr/local/bin:/usr/bin:/bin
INTERFACEDIR := %INTERFACEDIR%
%FORMALCOMPILER% := %ACTUALCOMPILER%

mkdirs := $(shell mkdir -p "$(BUILD)")

# cancel any default rules
.SUFFIXES:

%OBJECTS%

.PHONY: all
all: precompile

.PHONY: precompile
precompile: $(OBJECTS) %LIBNAME%

%LIBNAME%: $(OBJECTS)
	libtool -static -o $@ $(OBJECTS)
	
.PHONY: link
link:
	ln -f -s -h "$(shell pwd)" "$(INTERFACEDIR)"

.PHONY: clean
clean:
	cd "$(BUILD)"; rm -f *.o *.gpi *.gpd *.ppu

.PHONY: cleaner
cleaner:
	cd "$(BUILD)"; rm -f *.o *.gpi *.gpd *.ppu *.a
	rmdir "$(BUILD)"

.PHONY: objects
objects: $(OBJECTS)

.PHONY: allobjects
allobjects: $(OBJECTS)

%DEPENDENCIES%
EOM

	our ($objects, $allunit, $dependencies, $libname, $actualcompiler, $formalcompiler, $compilecommand, $compilername, $interfacedir);
	
	our @units = ();
	our %units = ();
	open( LIST, $unitdependencylist ) or die "Cant open $unitdependencylist $!";
	while ( <LIST> ) {
		chomp;
		m!(.*):(.*)! or die;
		next if $1 eq 'GPCStrings' && $type ne 'GPC';
		next if $1 eq 'FPCStrings' && $type ne 'FPC';
		push @units, $1;
		$units{$1} = $2;
	}
	close( LIST );
	
	$units{MacOS} = join(',',@units);
	unshift @units, 'MacOS';
	
	if ( $type eq 'GPC' ) {
		$formalcompiler = 'GPC_COMPILER';
		$actualcompiler = 'gpc';
		$compilecommand = 'gp PC=$(GPC_COMPILER) -W -Wall -gstabs -I. --object-path=$(BUILD) --unit-destination-path=$(BUILD) --include=GPCMacros.inc -c %PAS%';
		$libname = 'GPCMacOS.a';
		$allunit = 'MacOSAll';
		$compilername = 'GNU Pascal';
		$interfacedir = '/Developer/Pascal/GPCPInterfaces';
	} elsif ( $type eq 'FPC' ) {
		$formalcompiler = 'FPC_COMPILER';
		$actualcompiler = 'fpc';
		$compilecommand = '$(FPC_COMPILER) -vw0 -Mmacpas -Si -FE$(BUILD) -Fo$(BUILD) %PAS%';
		$libname = 'FPCMacOS.a';
		$allunit = 'MacOSAll';
		$compilername = 'Free Pascal';
		$interfacedir = '/Developer/Pascal/FPCPInterfaces';
	} else {
		die;
	}

	$units{$allunit} = '';
	unshift @units, $allunit;
	if ( $type eq 'GPC' ) {
		$units{GPCStringsAll} = $allunit;
		push @units, 'GPCStringsAll';
	}

	$objects = 'OBJECTS :=';
	foreach my $unit ( @units ) {
		$objects =~ m!(.*)\Z!;
		if ( length( $1 ) > 65 ) {
			$objects .= " \\\n  ";
		}
		my $obj = "\$(BUILD)/$unit.o";
		my $pas = "$unit.$extension";
		$objects .= " $obj";
		my $command = $compilecommand;
		my $dep = "\n".join( ' ', map( "$_.$extension", split( /,/, $units{$unit} ) ) );
		while ( $dep =~ s!(\n.{65,}?) (.*)\Z!$1 \\\n$2!g ) { };
		$dep =~ s!\A\n!!;
		$command =~ s!%OBJ%!$obj!;
		$command =~ s!%PAS%!$pas! or die;
		$dependencies .= "$obj: $dep $unit.pas\n\t$command\n\n";
	}
	
	$data = $maketemplate;
	$data =~ s!%EXTENSION%!$extension!g;
	$data =~ s!%OBJECTS%!$objects!g or die;
	$data =~ s!%DEPENDENCIES%!$dependencies!g or die;
	$data =~ s!%COMPILERNAME%!$compilername!g or die;
	$data =~ s!%LIBNAME%!$libname!g or die;
	$data =~ s!%INTERFACEDIR%!$interfacedir!g or die;
	$data =~ s!%ACTUALCOMPILER%!$actualcompiler!g or die;
	$data =~ s!%FORMALCOMPILER%!$formalcompiler!g or die;
}

replace_file_with_data( $output, $data, {creator=>'R*ch', type=>'TEXT', touch=>1} );
