#!/usr/bin/perl -w

use warnings;
use strict;
use diagnostics;

use Carp ();
local $SIG{__WARN__} = \&Carp::cluck;

use utf8;

binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

our $source = shift || 'NONE';
if ( $source eq 'NONE' ) {
	die "Usage: Convert.pl source-header-file [FORCE/NO/DISPLAY [dest-pascal-file]";
}
our $force = shift || 'DISPLAY';
die "Source is not C header file" unless $source =~ m!.*/(.*)\.h$!;
our $unitname = $1;
die "Force $force is not FORCE or NO or DISPLAY" unless $force =~ m!^FORCE|NO|DISPLAY$!;
our $dest = undef;
if ( $force ne 'DISPLAY' ) {
	$dest = shift;
	die "Dest is not folder" unless -d $dest;
	$dest =~ s!/$!!;
	$dest .= "/$unitname.pas";
	die "Dest exists, use FORCE to overwrite" if $force ne 'FORCE' && -e $dest;
}

#define EXTERN_API(_type)                       extern _type
#define EXTERN_API_C(_type)                     extern _type
#define EXTERN_API_STDCALL(_type)               extern _type
#define EXTERN_API_C_STDCALL(_type)             extern _type

#define DEFINE_API(_type)                       _type
#define DEFINE_API_C(_type)                     _type
#define DEFINE_API_STDCALL(_type)               _type
#define DEFINE_API_C_STDCALL(_type)             _type

#define CALLBACK_API(_type, _name)              _type ( * _name)
#define CALLBACK_API_C(_type, _name)            _type ( * _name)
#define CALLBACK_API_STDCALL(_type, _name)      _type ( * _name)
#define CALLBACK_API_C_STDCALL(_type, _name)    _type ( * _name)


#our $file = "/Developer/SDKs/MacOSX10.4u.sdk/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/OSServices.framework/Versions/A/Headers/SystemSound.h";

our $data = '';
our @uses = ('MacTypes');
readin( $source );
our %saved_data = ();
our $saved_token = "CPTOKENAAAAA";
$saved_data{$saved_token} = <<EOM;
unit $unitname;
interface
uses @{[join( ',', @uses )]};
{\$ALIGN POWER}

EOM

# Set up for finding availability macros
# NOTE: Has an embedded capturing group needed to deal with, if it exists, macro's parenthesized parameter str - capturing needed balanced parentheses matching
our $avail_re = qr!(?:(?:DEPRECATED_|AVAILABLE_)[A-Z0-9_]+|[A-Z_]*(?:_AVAILABLE(?:_STARTING|_BUT_DEPRECATED(?:_MSG|_WITH_ADVICE)?|_MAC)?|_DEPRECATED(?:_WITH_REPLACEMENT|_MAC)?|_UNAVAILABLE|_PROHIBITED)(?:(\((?:[^()]++|(?-1))*+\)))?\s?)+!;

# Apply simple macros
$data =~ s!CALLBACK_API(?:_C|_STDCALL|_C_STDCALL|)\s*\(([^)]*),([^)]*)\)!$1 ( * $2)!g;
$data =~ s!EXTERN_API(?:_C|_STDCALL|_C_STDCALL|)\s*\(([^)]*)\)!extern $1!g;
$data =~ s!DEFINE_API(?:_C|_STDCALL|_C_STDCALL|)\s*\(([^)]*)\)!$1!g;
$data =~ s!\b(?:CF_EXPORT|CG_EXTERN|CM_EXPORT|CV_EXPORT|CFN_EXPORT|CSEXTERN|QL_EXPORT|MD_EXPORT|MT_EXPORT|IMAGEIO_EXTERN|VT_EXPORT)\b!extern!g;

$data =~ s!^\s*CG_INLINE!// CG_INLINE!gm;
$data =~ s!^([ \t]*(?:CF|CM|DISPATCH|DVD|OPENGL)_ASSUME_NONNULL_(?:BEGIN|END)[ \t]*\n)!//$1!gm;

$data =~s!MD_AVAIL_LEOPARD!AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER!g;
$data =~s!MD_AVAIL!AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER!g;
$data =~s!CF_AVAILABLE!CF_AVAILABLE_STARTING!g;
$data =~s!CT_AVAILABLE!CT_AVAILABLE_STARTING!g;

# Remove unwanted stuff
$data =~ s!^[ \t]*(?:CA|CG|CF|MA|QL)_EXTERN_C_(?:BEGIN|END)\n!!gm;
$data =~ s!^#define\s+(?:__[A-Z_]+__|[A-Z_]+_H_)(?:\s+1)?\n!!gm;
$data =~ s!CSSMACI|CSSMAPI|CSSMCLI|CSSMCSPI|CSSMDLI|CSSMKRI|CSSMSPI|CSSMTPI!!gm;
$data =~ s!\bCF_BRIDGED(?:_MUTABLE)?_TYPE\(\s*\w+\s*\)!!gm;
$data =~ s!\bCF_RELATED_TYPE\([^\)]*\)!!gm;

# Handle some pragma conversion
$data =~ s!^#pragma\s+options\s+align=mac68k!{\$ALIGN MAC68K}!gm;
$data =~ s!^#pragma\s+options\s+align=power!{\$ALIGN POWER}!gm;

# Special cleanups
$data =~ s!\s*([),])[ \t]*(\/\* can be NULL \*\/)! $2 $1!g;

# Process comments
$data =~ s!/\*((?:.|\n)*?)\*/!my $result = slash_to_curly_comment( $1 ); $saved_token++; $saved_data{$saved_token} = $result; "«$saved_token»";!ge;
$data =~ s!(//.*)!$saved_token++; $saved_data{$saved_token} = $1; "«$saved_token»";!ge;

# insert unit header and end
$data =~ s!\A(«CPTOKENAAAAB»\n|)!$1«CPTOKENAAAAA»!;
$saved_token++; $saved_data{$saved_token} = "end."; $data .= "\n«$saved_token»\n";

# enum constants
$data =~ s!\n[ \t]*enum\s*_{1,2}(\w+)?\s*(«\w+»)?\s*(?:(\{(?:[^\{\}]++|(?-1))*+\}))\s*($avail_re)?\s*;\s*typedef\s*(\b\w+)?\s+\1;!my $result = enum_to_const( $1, $2, $3, $6, $5 ); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;
$data =~ s!\n[ \t]*enum\s*(\w+)?\s*(«\w+»)?\s*(?:(\{(?:[^\{\}]++|(?-1))*+\}))\s*($avail_re)?\s*;(?:\s*typedef\s+enum\s+\1\s+\1;)?!my $result = enum_to_const( $1, $2, $3, undef, $4 ); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;
# typedef enum optional_id {enumerator-list} new_type_id; pattern
$data =~ s!\n[ \t]*typedef\s+enum\s*(?:\w+)?(«\w+»)?\s*(?:(\{(?:[^{}}]++|(?-1))*+\}))\s*(\w+)\s*($avail_re)\s*;!my $result = enum_to_const( $3, $1, $2, undef, $4 ); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;

# enum constants through macros in SDK 10.9 or later
# CF_ENUM(id) {enumerator-list}; pattern
$data =~ s!\n[ \t]*\s*CF_ENUM\s*\(\s*\w+\s*\)\s*(«\w+»)?\s*(?:(\{(?:[^\{\}]++|(?-1))*+\}))\s*($avail_re)?\s*;!my $result = enum_to_const( undef, $1, $2, undef, $3 ); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;
# typedef CF_ENUM(type_id,new_type_id) {enumerator-list}; pattern
$data =~ s!\n[ \t]*typedef\s*CF_ENUM\s*\(\s*(\w+)\s*,\s*(\w+)\s*\)\s*(«\w+»)?\s*(?:(\{(?:[^\{\}]++|(?-1))*+\}))\s*($avail_re)?\s*;!my $result = enum_to_const( $2, $3, $4, $1, $5 ); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;
# typedef CF_OPTIONS(type_id,new_type_id) {enumerator-list}; pattern
$data =~ s!\n[ \t]*typedef\s*CF_OPTIONS\s*\(\s*(\w+)\s*,\s*(\w+)\s*\)\s*(«\w+»)?\s*(?:(\{(?:[^\{\}]++|(?-1))*+\}))\s*($avail_re)?\s*;!my $result = enum_to_const( $2, $3, $4, $1, $5 ); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;
# typedef VIMAGE_OPTIONS_ENUM(new_type_id,type_id) {enumerator-list}; pattern
$data =~ s!\n[ \t]*typedef\s*VIMAGE_OPTIONS_ENUM\s*\(\s*(\w+)\s*,\s*(\w+)\s*\)\s*(«\w+»)?\s*(?:(\{(?:[^\{\}]++|(?-1))*+\}))\s*($avail_re)?\s*;!my $result = enum_to_const( $1, $3, $4, $2, $5 ); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;

# struct records
$data =~ s!\n[ \t]*struct\s+(\w+)?\s*\{((?:.|\n)*?)\};(?:\s*typedef\s+struct\s+\1\s+(\w+)\s*;)?!my $result = struct_to_record( $1, $2, $3 ); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;
$data =~ s!\n[ \t]*typedef\s+struct\s+(\w+)?\s*\{((?:.|\n)*?)\}\s*(\w+)?\s*(?:,\s*\*\s*(\w+))?;(?:\s*typedef\s+struct\s+\1\s+(\w+)\s*;)?!my $result = struct_to_record( $1, $2, $3, $5, $4 ); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;

# simple type defines
$data =~ s!\n[ \t]*typedef\s+STACK_UPP_TYPE\s*\(\s*(\w+)\s*\)\s*(\w+)\s*;!my $result = simple_type($2,$1,$3); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;
$data =~ s!\n[ \t]*typedef\b\s*([A-Za-z0-9_ *]+?)\s*\b(\w+)\s*(?:,\s*\*\s*(\w+)\s*)?;!my $result = simple_type($2,$1,$3); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;

# procedural types
$data =~ s!\n[ \t]*typedef\s+((?:const\s+)?\w+(?:\s*\*)?)\s*\(\s*(?:\w+\s*)?\*\s*(\w+)\s*\)\s*\(([^)]*?|.*?)\)\s*($avail_re)?\s*;!my $result = dotypedef($1,$2,$3,$4); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;

# external constants
$data =~ s!\n[ \t]*extern\s+(const\s+)?((?:unsigned\s+)?\w+(?:\s*\*\s*const\s+|\s*const\s*|\s*\*\s*|\s+))(\w+(?:(?:\s+)(?:«\w+»))?)\s*($avail_re)?\s*;!my $typefixup = trim_const_r($2); my $result = externconst($1,$typefixup,$3,$4); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;

# functions
$data =~ s!\n[ \t]*extern\s+((?:const\s+)?(?:unsigned\s+)?\w+(?:\s*\*\s*|\s+)(?:«\w+»)?(?:\s+)?)(\w+)\s*\(([^)]*)\)\s*($avail_re)?\s*;!my $result = dofunc($1,$2,$3,$4); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;
$data =~ s!\n[ \t]*((?:const\s+)?(?:unsigned\s+)?\w+(?:\s*\*\s*|\s+)(?:«\w+»)?(?:\s+)?)(\w+)\s*\(([^)]*)\)\s*($avail_re)?\s*;!my $result = dofunc($1,$2,$3,$4); $saved_token++; $saved_data{$saved_token} = $result; "\n«$saved_token»";!ge;

# #define CFSTRs
$data =~ s!^#define\s+(\w+)\s+CFSTR\(\s*"([^"']+)"\s*\)([ \t]*(?:«\w+»)?)$!my $result = "const $1 = CFSTR( '$2' );$3"; $saved_token++; $saved_data{$saved_token} = $result; "«$saved_token»";!gme;

# #define constants
$data =~ s!^#define\s+(\w+)\s+"([^"']+)"[ \t]*$!my $result = "const\n\t$1 = '$2';"; $saved_token++; $saved_data{$saved_token} = $result; "«$saved_token»";!gme;
$data =~ s!^#define\s+(\w+)\s+\(([0-9.]+)\)[ \t]*$!my $result = "const\n\t$1 = $2;"; $saved_token++; $saved_data{$saved_token} = $result; "«$saved_token»";!gme;
$data =~ s!^#define\s+(\w+)\s+([0-9.]+)[ \t]*$!my $result = "const\n\t$1 = $2;"; $saved_token++; $saved_data{$saved_token} = $result; "«$saved_token»";!gme;
$data =~ s!^#define\s+(\w+)\s+\(0x([0-9a-fA-F]+)\)[ \t]*$!my $result = "const\n\t$1 = \$$2;"; $saved_token++; $saved_data{$saved_token} = $result; "«$saved_token»";!gme;
$data =~ s!^#define\s+(\w+)\s+0x([0-9a-fA-F]+)[ \t]*$!my $result = "const\n\t$1 = \$$2;"; $saved_token++; $saved_data{$saved_token} = $result; "«$saved_token»";!gme;

# array type defines
$data =~ s!\n[ \t]*typedef\s+([A-Za-z0-9_\* \t]+?)\s*\b(\w+)\s*\[([^\]]*)\]\s*;(.*)! my $result = array_to_array( $1, $2, $3, $4, " =" ); $saved_token++; $saved_data{$saved_token} = $result; "\ntype\n\t«$saved_token»";!ge;

# print now to see what has not been converted
#print $data;

while ( $data =~ s!«(\w+)»!$saved_data{$1}!g ) {
	# do nothing
}

while ( $data =~ s!^(type\n(?:\s*\w+\s*=.*\n)+)type\n!$1!gm ) {
	# do nothing
}

$data =~ s!^\{\s*\}\n?!!gm;
$data =~ s!\n{4,}!\n\n\n!g;

if ( $force eq 'DISPLAY' ) {
	print $data;
} else {
	die if $force ne 'FORCE' && -e $dest;
	open ( OUT, ">:encoding(MacRoman)", $dest );
	print OUT $data;
	close( OUT );
}

sub externconst {
	my( $const, $type, $name, $avail ) = @_;
	my $ret = '';
	trim( \$type );
	clean_type( \$type );
	$ret .= "const $name: $type;";
	$ret .= "\n$avail;" if $avail;
	return $ret;
}

sub dofunc {
	my( $type, $name, $params, $avail ) = @_;
	
	my $ret = '';
	trim( \$type );
	clean_type( \$type );
	$type =~ s!\A\s*const\s+!!;
	convert_params( \$params );
	if ( $type eq 'void' ) {
		$ret .= "procedure $name$params;";
	} else {
		$ret .= "function $name$params: $type;";
	}
	$ret .= "\n$avail;" if $avail;
	return $ret;
}

sub dotypedef {
	my( $type, $name, $params, $avail ) = @_;
	
# print STDERR "dotypedef $name, $type\n";
	my $ret = '';
	clean_type( \$type );
	$type =~ s!\A\s*const\s+!!;
	convert_params( \$params );
	if ( $type eq 'void' ) {
		$ret .= "type\n\t$name = procedure$params;";
	} else {
		$ret .= "type\n\t$name = function$params: $type;";
	}
	$ret .= "\n$avail;" if $avail;
#	$ret .= " external name '$name';";
	return $ret;
}

sub convert_params {
	my( $params ) = @_;

	$$params =~ s!\{ can be NULL \}!!g;
	my @params;
	foreach my $param ( split( /\,/, $$params ) ) {
		trim( \$param );
		$param =~ s!\s+! !g;
		my $prename = '';
		my $name = undef;
		my $pretype = '';
		my $type = undef;
		my $canbenull = 0;
		my $comment = '';
		if ( $param =~ s!\s*(«(\w+)»)\s*$!! ) {
			$comment = " $1";
			my $token = $2;
			$canbenull = 1 if $saved_data{$2} eq '{ can be NULL }';
		}
		if ( $param =~ m!^const char ?\* ?(\w+)$! ) {
			$name = $1;
			$type = 'ConstCStringPtr';
		} elsif ( $param =~ m!^const (\w+) (\w+)\[\]$! ) {
			$prename = '{const} ';
			$name = $2;
			$pretype = '{variable-size-array} ';
			$type = "${1} *";
		} elsif ( $param =~ m!^(\w+) (\w+)\[\]$! ) {
			$name = $2;
			$pretype = '{variable-size-array} ';
			$type = "${1} *";
#print STDERR "Type is $type\n";
		} elsif ( $param =~ m!^const\s+void\s*\*\s*(\w+)$! ) {
			$name = $1;
			$type = '{const} univ Ptr';
		} elsif ( !$canbenull && $param =~ m!^const ((?:unsigned\s+)?\w+)\s*\*\s*(\w+)$! ) {
			$prename = 'const var ';
			$name = $2;
			$type = $1;
		} elsif ( !$canbenull && $param =~ m!^(\w+) ?\* ?(\w+)$! && $1 ne 'void' ) {
			$prename = 'var ';
			$name = $2;
			$type = $1;
		} elsif ( $param =~ m!^(\S.*?)\s*(\b\w+)$! ) {
			$name = $2;
			$type = $1;
		} else {
			$name = undef;
		}
		if ( $name ) {
			clean_name( \$name );
			clean_type( \$type );
#print STDERR "Type is now $type\n" if $pretype;
			$type =~ s!^\s*UnivPtr\s*$!univ Ptr!;
			if ( $prename eq '' && $type =~ s!^\s*const\s+!! ) {
				$prename = '{const} ';
			}
			if ( $prename eq '' && $type eq 'ConstStr255Param' ) {
				$prename = 'const var ';
				$type = 'Str255';
			}
			if ( $type eq 'Str255' ) {
				$prename ||= 'var ';
			}
			push @params, "$prename$name: $pretype$type$comment";
		} else {
			push @params, "$param$comment";
		}
	}
	$$params = join( '; ', @params );
	if ( $$params eq 'void' || $$params eq '' ) {
		$$params = '';
	} else {
		$$params = "( $$params )";
	}
}

sub simple_type {
	my( $name, $type, $ptrtype ) = @_;
	
# print STDERR "simple_type $name, $type\n";
	clean_type( \$type );
	my $result = '';
	(my $nameref = $name) =~ s!Ref$!!;
	if ( $type =~ m!^\s*(?:const\s+)?struct\s+((|_|__|Opaque)(?:$nameref|$name))\s*\*\s*$! ) {
		if ( $2 ) {
			$result = "type\n\t$name = ^$1; { an opaque type }\n\t$1 = record end;";
		}
		else {
			$result = "type\n\t$name = ^SInt32; { an opaque type }";
		}
	} elsif ( $name eq $type && $type =~ s!Ptr$!! ) {
		$result = "\t$name = ^$type;"
	} else {
		$result = "type\n\t$name = $type;"
	}
	if ( $ptrtype )
	{
		$result .= "\n\t$ptrtype = ^$type;"
	}
	return $result;
}

sub trim {
	my( $s ) = @_;
	$$s =~ s!\A\s+!!;
	$$s =~ s!\s+\Z!!;
}

sub trim_const_r {
	my( $t_const  ) = @_;
	$t_const =~ s!\bconst\b!!;
	return $t_const;
}

sub trim_brackets {
	my( $s ) =@_;
	$$s =~ s!\A\{\s*!!;
	$$s =~ s!\s*\}\Z!!;
}

sub clean_value {
	my( $value ) = @_;

# print STDERR "clean_value $$value\n";	
	trim( $value );
	$$value =~ s!^\(([^()]*)\)$!$1!;

	$$value =~ s!^(\d+)L?$!$1!;
	$$value =~ s!^(\d+)L?\s*<<\s*(\w+)$!$1 shl $2!;
	$$value =~ s!^(\d+)L?\s*<<\s*(\([A-Za-z0-9_+\-]*\))$!$1 shl $2!;
	$$value =~ s!^(?:\(unsigned long\)|\(long\)|)\((\d+)L?\s*<<\s*(\w+)\)!($1 shl $2)!;
	$$value =~ s!^(?:\(unsigned long\)|\(long\)|)\(\((\d+)L?\s*<<\s*(\w+)\)\)!($1 shl $2)!;
	$$value =~ s!^(\w+)\s*\|\s*(\w+)$!$1 or $2!;
	$$value =~ s!^(\w+)\s*\&\s*(\w+)$!$1 and $2!;
	$$value =~ s!^(?:\(unsigned long\)|\(long\)|)0x([0-9A-Fa-f]+)$!\$$1!;
	$$value =~ s!^('[^']{4}')$!FOUR_CHAR_CODE($1)!;
	$$value =~ s!\bsizeof\b!SizeOf!;
	
	if ( $$value =~ m!\w+(\s*\|\s*\w+)+! ) {
		$$value =~ s!\s*\|\s*! or !g;
	}

	$$value =~ s!^\(([^()]*)\)$!$1!;
}

sub clean_type {
	my( $type ) = @_;
	$$type =~ s!\bNSString\s*\*\s*const\b!CFStringRef!g;
	$$type =~ s!\bfloat\b!Float32!g;
	$$type =~ s!\bdouble\b!Float64!g;
	$$type =~ s!\bunsigned\s+int\b!UInt32!g;
	$$type =~ s!\bsigned\s+int\b!SInt32!g;
	$$type =~ s!\blong\s+int\b!SIGNEDLONG!g;
	$$type =~ s!\bunsigned\s+long\s+int\b!UNSIGNEDLONG!g;
	$$type =~ s!\bsigned\s+long\s+int\b!SIGNEDLONG!g;
	$$type =~ s!\bunsigned\s+long\b!UNSIGNEDLONG!g;
	$$type =~ s!\bsigned\s+long\b!SIGNEDLONG!g;
	$$type =~ s!\bunsigned\s+short\s+int\b!UInt16!g;
	$$type =~ s!\bsigned\s+short\s+int\b!SInt16!g;
	$$type =~ s!\bunsigned\s+short\b!UInt16!g;
	$$type =~ s!\bsigned\s+short\b!SInt16!g;
	$$type =~ s!\bshort\s+int\b!SInt16!g;
	$$type =~ s!\bshort\b!SInt16!g;
	$$type =~ s!\bint\b!SInt32!g;
	$$type =~ s!\blong\b!SIGNEDLONG!g;
	$$type =~ s!\bunsigned\s+char\b!UInt8!g;
	$$type =~ s!\bsigned\s+char\b!SInt8!g;
	$$type =~ s!\bbool\b!CBool!g;
	$$type =~ s!\bu_int16_t\b!UInt16!g;
	$$type =~ s!\buint16_t\b!UInt16!g;
	$$type =~ s!\bint16_t\b!SInt16!g;
	$$type =~ s!\bu_int32_t\b!UInt32!g;
	$$type =~ s!\buint32_t\b!UInt32!g;
	$$type =~ s!\bint32_t\b!SInt32!g;
	$$type =~ s!\bu_int64_t\b!UInt64!g;
	$$type =~ s!\buint64_t\b!UInt64!g;
	$$type =~ s!\bint64_t\b!SInt64!g;
	$$type =~ s!\buint8\b!UInt8!g;
	$$type =~ s!\bsint8\b!SInt8!g;
	$$type =~ s!\buint16\b!UInt16!g;
	$$type =~ s!\bsint16\b!SInt16!g;
	$$type =~ s!\buint32\b!UInt32!g;
	$$type =~ s!\bsint32\b!SInt32!g;
	$$type =~ s!\buint64\b!UInt64!g;
	$$type =~ s!\bsint64\b!SInt64!g;
	$$type =~ s!\bvoid\s*\*!UnivPtr!g;
	$$type =~ s!^\s*(\w+)\s*\*\s*$!${1}Ptr!g;
	$$type =~ s!^\s*const\s+(\w+)\s*\*\s*$!const ${1}Ptr!g;
}

sub clean_name {
	my( $name ) = @_;
	$$name =~ s!^string$!strng!;
	$$name =~ s!^object$!objct!;
	$$name =~ s!^type$!typ!;
	$$name =~ s!^function$!func!;
	$$name =~ s!^procedure$!proc!;
	$$name =~ s!^end$!finish!;
	$$name =~ s!^in$!inp!;
	$$name =~ s!^array$!arry!;
	$$name =~ s!^pointer$!pointr!;
	$$name =~ s!^unit$!unt!;
}

sub clean_names {
	my( $names) = @_;
	my $result = '';
	my @names = split( /,/, $$names );
	foreach my $name ( @names ) {
		trim( \$name );
		clean_name( \$name );
		$result .= "$name, ";
	}
	$result =~ s!, \Z!!;
	$$names = $result;
}

sub slash_to_curly_comment {
	my ( $comment ) = @_;
	
	$comment =~ tr!{}!()!;
	return "{$comment}";
}

sub enum_to_const {
	my( $type, $comment, $input, $actualtype, $avail_tail ) = @_;

	my $start = '';
	trim_brackets( \$input );

	if ( $type ) {
		$actualtype ||= 'SInt32';
		clean_type( \$actualtype );
		$start .= "type\n\t$type = $actualtype;\n";
	}
	if ( defined $avail_tail ) {
		$avail_tail = "\n(* $avail_tail *)\n";
	} else {
		$avail_tail = '';
	}
	$start .= "const";
	$start .= " $comment" if $comment;
	$start .= "\n";
	my $constants = '';
	my $base = 0;
	my $index = 0;
	while ( $input ) {
 #  print STDERR "input: '",$input,"'\n";
		if ( $input=~ s!\A(\s*#if\s.*\n)!! ) {
			$constants .= $1;
		} elsif ( $input=~ s!\A(\s*#else.*\n)!! ) {
			$constants .= $1;
		} elsif ( $input=~ s!\A(\s*#endif[^\n]*\n?)!! ) {
			$constants .= $1;
		} elsif ( $input=~ s!\A(\s*,\s*\n)!! ) {
			# do nothing
		} elsif ( $input =~ s!\A[ \t]*(\w+)\s*\b($avail_re)?\s*=\s*([^,«\n]*)!! ) {
			my ( $var, $avail_item, $value ) = ($1, $2, $4 );
			my $item_comment = '';
			if ( defined $avail_item ) {
				$avail_item = "(* $avail_item *)";
			} else {
				$avail_item = '';
			}
			if ( $input =~ s!\A,!! ) {
				# do nothing
			} elsif ( $input =~ s!\A(«\w+»),!! ) {
				$item_comment = $1;
			} elsif ( $value =~ s!(\s+)\Z!! ) {
				$input = $1.$input;
			}
			clean_value( \$value );
			$constants .= "\t$var = $value$item_comment; $avail_item";
			$base = $value;
			$index = 1;
		} elsif ( $input =~ s!\A[ \t]*(\w+)(?:\s*,|(?=\s*[«\n])|\s*\Z)!! ) {
			my ( $var ) = ($1);
			my $value;
			if ( $base =~ m!\A\d+\Z! ) {
				$value = $base + $index;
			} else {
				$value = "$base + $index";
			}
			$constants .= "\t$var = $value;";
			$index++;
		} elsif ( $input =~ s!\A(\s*«\w+»)!! ) {
			$constants .= $1;
		} elsif ( $input =~ s!\A([ \t]*\n)!! ) {
			$constants .= $1;
		} elsif ( $input =~ s!\A(\s*)\Z!! ) {
			$constants .= $1;
		} else {
			die "enum_to_const $input";
		}
	}
	$constants =~ s!;\s*(?=\w)!;\n\t!g;
	return $start.$constants.$avail_tail;
}

sub struct_to_record {
	my( $type, $input, $type2, $type3, $ptrtype ) = @_;

#print STDERR "type ",$type || 'undef',"\n";
#print STDERR "type2 ",$type2 || 'undef',"\n";
#print STDERR "ptrtype ",$ptrtype || 'undef',"\n";
#print STDERR "type3 ",$type3 || 'undef',"\n";
	if ( !$type || ($type2 && $type eq "_$type2") ) {
		$type = $type2;
	}
	if ( $type && $type2 && $type ne $type2 && $type ne "_$type2" && $type2 ne "_$type" ) {
		warn "Struct with different names $type and $type2";
	}
	if ( !$type || ($type3 && $type eq "_$type3") ) {
		$type = $type3;
	}
	if ( $type && $type3 && $type ne $type3 && $type ne "_$type3" && $type3 ne "_$type" ) {
		warn "Struct with different names $type and $type3";
	}
	if ( !$type ) {
		$type = 'RECORD';
		warn "Struct with no name";
	}

	my $result = "type\n";
	if ( $ptrtype ) {
		$result .= "\t$ptrtype = ^$type;\n";
	}
	$result .= "\t$type = record\n";
	trim( \$input );
	foreach my $line ( split( /\n/, $input ) ) {
		if ( $line =~ m!^\s*([A-Za-z0-9_\* \t]+?)\s*\b(\w+\s*(?:,\s*\w+)*)\s*;(.*)! ) {
			my ( $type, $name, $comment ) = ($1, $2, $3);
			trim( \$name );
			clean_type( \$type );
			clean_names( \$name );
			$result .= "\t\t$name: $type;$comment\n";
		} elsif ( $line =~ m!^\s*([A-Za-z0-9_\* \t]+?)\s*\b(\w+)\s*\[([^\]]*)\]\s*;(.*)! ) {
			$result .= "\t\t";
			$result .= array_to_array( $1, $2, $3, $4, ":");
		} else {
			$result .= "$line\n";
		}
	}
	$result .= "\tend;";
	return $result;
}

sub array_to_array {
	my ( $type, $name, $arrsize, $comment, $separ ) = @_;
	trim( \$name );
	clean_type( \$type );
	clean_names( \$name );
	my $result = "$name$separ array [0..$arrsize-1] of $type;$comment\n";
}

sub readin {
	my( $file ) = @_;
	
	open( IN, "<:encoding(MacRoman)", $file ) or die "cant read $file $!";

	our @conditionals = ();
	our $skipping = 0;
	while( <IN> ) {
		if ( /^\s*#if/ ) {
#print STDERR join('',@conditionals),$_;
			if ( $skipping ) {
				DoIf( 'U' ); # does not matter, will be skipped anyway
			} elsif ( /^\s*#if\s+(.+)/i ) {
				DoIf( GetVal( $1 ) );
			} elsif ( m!^\s*#ifndef\s+(\w+)\s*(?:/\*.*\*/)?\s*$!i ) {
				DoIf( GetVal( "not defined $1" ) );
			} elsif ( m!^\s*#ifdef\s+(\w+)\s*(?:/\*.*\*/)?\s*$!i ) {
				DoIf( GetVal( "defined $1" ) );
			} else {
				die "Unknow #if conditional $_";
			}
		} elsif ( /^\s*#else\b/i ) {
#print STDERR join('',@conditionals),$_;
			die "Mismatched #else conditional" unless @conditionals;
			DoElse();
		} elsif ( /^\s*#elif\b(.*)/i ) {
#print STDERR join('',@conditionals),$_;
			die "Mismatched #elif conditional" unless @conditionals;
			DoElif( GetVal( $1 ) );
		} elsif ( /^\s*#endif\b/i ) {
#print STDERR join('',@conditionals),$_;
			die "Mismatched #endif conditional" unless @conditionals;
			DoEndif();
		} elsif ( !$skipping ) {
			if ( /^\s*#include\s+[<"](.*)[>"]/ ) {
				my $unit = $1;
				$unit =~ s!.*/!!;
				$unit =~ s!\.h$!!;
				if ( $unit ne 'AvailabilityMacros' ) {
					push @uses, $unit;
				}
			} elsif ( /^\s*#define\s+__[A-Z]+__/ ) {
				# do nothing
			} else {
				$data .= $_;
			}
		}
	}
	close( IN );
	die "Unclosed #if conditional @conditionals" if @conditionals;
	
	sub GetVal {
		my $in = shift;
#print STDERR "$in\n";		
		if ( $in =~ /^\s*(?:not|\!)\s*\(([^)]+)\)\s*$/i ) {
			my $ret = GetVal( $1 );
			$ret =~ tr/UFT/UTF/;
			return $ret;
		} elsif ( $in =~ /(.*) or (.*)/i ) {
			my ( $left, $right ) = ( $1, $2 );
			$left = GetVal( $left );
			$right = GetVal( $right );
			return 'T' if $left eq 'T';
			return 'T' if $right eq 'T';
			return 'U' if $left eq 'U';
			return 'U' if $right eq 'U';
			return 'F';
		} elsif ( $in =~ /(.*) and (.*)/i ) {
			my ( $left, $right ) = ( $1, $2 );
			$left = GetVal( $left );
			$right = GetVal( $right );
			return 'F' if $left eq 'F';
			return 'F' if $right eq 'F';
			return 'U' if $left eq 'U';
			return 'U' if $right eq 'U';
			return 'T';
		} elsif ( $in =~ /^\s*(?:not\s+|\!\s*)(.*)$/i ) {
			my $ret = GetVal( $1 );
			$ret =~ tr/UFT/UTF/;
			return $ret;
		} elsif ( $in =~ /^\s*DEFINED\b\s*(?:(\w+)|\(\s*(\w+)\s*\))\s*$/i ) {
			my $var = $1 || $2;
#print STDERR "DeFineD $var\n";		
			return 'F' if $var eq '__MWERKS__';
			return 'U' if $var eq '__GNUC__';
			return 'U' if $var eq '__i386__';
			return 'U' if $var eq '__LITTLE_ENDIAN__';
			return 'U' if $var eq '__BIG_ENDIAN__';
			return 'T' if $var eq '__MACH__';
			return 'F' if $var eq '__WIN32__';
			return 'F' if $var eq '__cplusplus';
			return 'F' if $var =~ m!^__[A-Z_]+__$!;
			return 'F' if $var =~ m!^[A-Z_]+_H_$!;
			return 'U';
		} elsif ( $in =~ /^\s*(TRUE|1)\s*$/i ) {
			return 'T';
		} elsif ( $in =~ /^\s*(FALSE|0)\s*$/i ) {
			return 'F';
		} elsif ( $in =~ /^\s*(\w+)\s*$/ ) {
			my $var = $1;
			return 'F' if $var eq 'PRAGMA_ONCE';
			return 'U';
		} elsif ( $in =~ /^\s*\$([A-Fa-f0-9]+)\s*$/ ) {
			return 'U';
		} else {
			return 'U';
#			die $in;
		}
	}
	
	sub DoIf {
		my $result = shift;
		
		push @conditionals, $result;
		$skipping = grep( /F/i, @conditionals );
		$data .= $_ if !$skipping && $result eq 'U';
	}
	
	sub DoElif {
		my $nextresult = shift;
		
		my $lastresult = pop @conditionals;
		
		my $result;
		if ( $lastresult =~ m!U! || $nextresult =~ m!U! ) {
			$result = 'U';
		} elsif ( $lastresult =~ m!F! ) {
			$result = $nextresult;
		} else {
			$result = 'f';
		}
		
		push @conditionals, $result;
		$skipping = grep( /F/i, @conditionals );
		$data .= $_ if !$skipping && $result eq 'U';
	}
	
	sub DoElse {
		my $result = pop @conditionals;
		$result =~ tr/UFTf/UTFf/;
		push @conditionals, $result;
		$skipping = grep( /F/i, @conditionals );
		$data .= $_ if !$skipping && $result eq 'U';
	}

	sub DoEndif {
		my $result = pop @conditionals;
		$skipping = grep( /F/i, @conditionals );
		$data .= $_ if !$skipping && $result eq 'U';
	}
}
