{
     File:       CarbonCore/MachineExceptions.h
 
     Contains:   Processor Exception Handling Interfaces.
                 The contents of this header file are deprecated.
 
     Copyright:  � 1993-2011 by Apple Inc. All rights reserved.
}
{     Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, June 2018 }

unit MachineExceptions;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

{ Some basic declarations used throughout the kernel }
type
	AreaID = ^OpaqueAreaID; { an opaque type }
	OpaqueAreaID = record end;
	AreaIDPtr = ^AreaID;
{ Machine Dependent types for PowerPC: }

{ Because a number of sources do a #define CR 13 and this file contains a struct member named CR,
 * an obscure compilation error gets spit out.  Rename the field to CRRegister.   }

type
	MachineInformationPowerPCPtr = ^MachineInformationPowerPC;
	MachineInformationPowerPC = record
		CTR: UnsignedWide;
		LR: UnsignedWide;
		PC: UnsignedWide;
		CRRegister: UNSIGNEDLONG;             {    changed from CR since some folks had a #define CR  13 in their source code}
		XER: UNSIGNEDLONG;
		MSR: UNSIGNEDLONG;
		MQ: UNSIGNEDLONG;
		ExceptKind: UNSIGNEDLONG;
		DSISR: UNSIGNEDLONG;
		DAR: UnsignedWide;
		Reserved: UnsignedWide;
	end;
type
	RegisterInformationPowerPCPtr = ^RegisterInformationPowerPC;
	RegisterInformationPowerPC = record
		R0: UnsignedWide;
		R1: UnsignedWide;
		R2: UnsignedWide;
		R3: UnsignedWide;
		R4: UnsignedWide;
		R5: UnsignedWide;
		R6: UnsignedWide;
		R7: UnsignedWide;
		R8: UnsignedWide;
		R9: UnsignedWide;
		R10: UnsignedWide;
		R11: UnsignedWide;
		R12: UnsignedWide;
		R13: UnsignedWide;
		R14: UnsignedWide;
		R15: UnsignedWide;
		R16: UnsignedWide;
		R17: UnsignedWide;
		R18: UnsignedWide;
		R19: UnsignedWide;
		R20: UnsignedWide;
		R21: UnsignedWide;
		R22: UnsignedWide;
		R23: UnsignedWide;
		R24: UnsignedWide;
		R25: UnsignedWide;
		R26: UnsignedWide;
		R27: UnsignedWide;
		R28: UnsignedWide;
		R29: UnsignedWide;
		R30: UnsignedWide;
		R31: UnsignedWide;
	end;
type
	FPUInformationPowerPCPtr = ^FPUInformationPowerPC;
	FPUInformationPowerPC = record
		Registers: array [0..31] of UnsignedWide;
		FPSCR: UNSIGNEDLONG;
		Reserved: UNSIGNEDLONG;
	end;
type
	Vector128Ptr = ^Vector128;
	Vector128 = record
		case SInt16 of
		0: (
			l: array [0..3] of UInt32;
			);
		1: (
			s: array [0..7] of UInt16;
			);
		2: (
			c: packed array [0..15] of UInt8;
			);
	end;
type
	VectorInformationPowerPCPtr = ^VectorInformationPowerPC;
	VectorInformationPowerPC = record
		Registers: array [0..31] of Vector128;
		VSCR: Vector128;
		VRsave: UInt32;
	end;
{ Exception related declarations }
const
	kWriteReference = 0;
	kReadReference = 1;
	kFetchReference = 2;
	writeReference = kWriteReference; { Obsolete name}
	readReference = kReadReference; { Obsolete name}
	fetchReference = kFetchReference; { Obsolete name}


type
	MemoryReferenceKind = UNSIGNEDLONG;
	MemoryExceptionInformationPtr = ^MemoryExceptionInformation;
	MemoryExceptionInformation = record
		theArea: AreaID;                { The area related to the execption, same as MPAreaID.}
		theAddress: LogicalAddress;             { The 32-bit address of the exception.}
		theError: OSStatus;               { See enum below.}
		theReference: MemoryReferenceKind;          { read, write, instruction fetch.}
	end;
const
	kUnknownException = 0;
	kIllegalInstructionException = 1;
	kTrapException = 2;
	kAccessException = 3;
	kUnmappedMemoryException = 4;
	kExcludedMemoryException = 5;
	kReadOnlyMemoryException = 6;
	kUnresolvablePageFaultException = 7;
	kPrivilegeViolationException = 8;
	kTraceException = 9;
	kInstructionBreakpointException = 10; { Optional}
	kDataBreakpointException = 11;   { Optional}
	kIntegerException = 12;
	kFloatingPointException = 13;
	kStackOverflowException = 14;   { Optional, may be implemented as kAccessException on some systems.}
	kTaskTerminationException = 15;   { Obsolete}
	kTaskCreationException = 16;   { Obsolete}
	kDataAlignmentException = 17;    { May occur when a task is in little endian mode or created with kMPTaskTakesAllExceptions.}

{$ifc OLDROUTINENAMES}
const
	unknownException = kUnknownException; { Obsolete name}
	illegalInstructionException = kIllegalInstructionException; { Obsolete name}
	trapException = kTrapException; { Obsolete name}
	accessException = kAccessException; { Obsolete name}
	unmappedMemoryException = kUnmappedMemoryException; { Obsolete name}
	excludedMemoryException = kExcludedMemoryException; { Obsolete name}
	readOnlyMemoryException = kReadOnlyMemoryException; { Obsolete name}
	unresolvablePageFaultException = kUnresolvablePageFaultException; { Obsolete name}
	privilegeViolationException = kPrivilegeViolationException; { Obsolete name}
	traceException = kTraceException; { Obsolete name}
	instructionBreakpointException = kInstructionBreakpointException; { Obsolete name}
	dataBreakpointException = kDataBreakpointException; { Obsolete name}
	integerException = kIntegerException; { Obsolete name}
	floatingPointException = kFloatingPointException; { Obsolete name}
	stackOverflowException = kStackOverflowException; { Obsolete name}
	terminationException = kTaskTerminationException; { Obsolete name}
	kTerminationException = kTaskTerminationException; { Obsolete name}

{$endc}  {OLDROUTINENAMES}


type
	ExceptionKind = UNSIGNEDLONG;
	ExceptionInfoPtr = ^ExceptionInfo;
	ExceptionInfo = record
		case SInt16 of
		0: (
			memoryInfo: MemoryExceptionInformationPtr;
			);
	end;
type
	ExceptionInformationPowerPCPtr = ^ExceptionInformationPowerPC;
	ExceptionInformationPowerPC = record
		theKind: ExceptionKind;
		machineState: MachineInformationPowerPCPtr;
		registerImage: RegisterInformationPowerPCPtr;
		FPUImage: FPUInformationPowerPCPtr;
		info: ExceptionInfo;
		vectorImage: VectorInformationPowerPCPtr;
	end;
{$ifc TARGET_CPU_PPC or TARGET_CPU_PPC64}
type
	ExceptionInformation = ExceptionInformationPowerPC;
	MachineInformation = MachineInformationPowerPC;
	RegisterInformation = RegisterInformationPowerPC;
	FPUInformation = FPUInformationPowerPC;
	VectorInformation = VectorInformationPowerPC;
	ExceptionInformationPtr = ^ExceptionInformation;
	MachineInformationPtr = ^MachineInformation;
	RegisterInformationPtr = ^RegisterInformation;
	FPUInformationPtr = ^FPUInformation;
	VectorInformationPtr = ^VectorInformation;
{$endc}

{$ifc TARGET_CPU_X86 or TARGET_CPU_X86_64}
type
  Vector128intel = record
		case SInt16 of
		0: (
			s: array [0..3] of Float32;
			);
		1: (
			si: array [0..1] of SInt64;
			);
		2: (
			sd: array [0..1] of Float64;
			);
		3: (
			c: packed array [0..15] of UInt8;
			);
	end;
{$endc}  { TARGET_CPU_X86 or TARGET_CPU_X86_64 }

{$ifc TARGET_CPU_X86}
type
	MachineInformationIntelPtr = ^MachineInformationIntel;
	MachineInformationIntel = record
		CS: UNSIGNEDLONG;
		DS: UNSIGNEDLONG;
		SS: UNSIGNEDLONG;
		ES: UNSIGNEDLONG;
		FS: UNSIGNEDLONG;
		GS: UNSIGNEDLONG;
		EFLAGS: UNSIGNEDLONG;
		EIP: UNSIGNEDLONG;
		ExceptTrap: UNSIGNEDLONG;
		ExceptErr: UNSIGNEDLONG;
		ExceptAddr: UNSIGNEDLONG;
	end;
type
	RegisterInformationIntelPtr = ^RegisterInformationIntel;
	RegisterInformationIntel = record
		EAX: UNSIGNEDLONG;
		EBX: UNSIGNEDLONG;
		ECX: UNSIGNEDLONG;
		EDX: UNSIGNEDLONG;
		ESI: UNSIGNEDLONG;
		EDI: UNSIGNEDLONG;
		EBP: UNSIGNEDLONG;
		ESP: UNSIGNEDLONG;
	end;

type
	FPRegIntel = packed array[0..9] of UInt8;
type
	FPUInformationIntel = record
		Registers: array[0..7] of FPRegIntel;
		Control: UInt16;
		Status: UInt16;
		Tag: UInt16;
		Opcode: UInt16;
		EIP: UInt32;
		DP: UInt32;
		DS: UInt32;
	end;

type
	VectorInformationIntel = record
		Registers: array[0..7] of Vector128Intel;
	end;

type
	MachineInformationPtr = ^MachineInformation;
	RegisterInformationPtr = ^RegisterInformation;
	FPUInformationPtr = ^FPUInformation;
	VectorInformationPtr = ^VectorInformation;
	MachineInformation = MachineInformationIntel;
	RegisterInformation = RegisterInformationIntel;
	FPUInformation = FPUInformationIntel;
	VectorInformation = VectorInformationIntel;
{$endc}  { TARGET_CPU_X86 }

{$ifc TARGET_CPU_X86_64}
type
	MachineInformationIntel64 = record
		CS: UNSIGNEDLONG;
		FS: UNSIGNEDLONG;
		GS: UNSIGNEDLONG;
		RFLAGS: UNSIGNEDLONG;
		RIP: UNSIGNEDLONG;
		ExceptTrap: UNSIGNEDLONG;
		ExceptErr: UNSIGNEDLONG;
		ExceptAddr: UNSIGNEDLONG;
	end;
type
	RegisterInformationIntel64 = record
		RAX: UNSIGNEDLONG;
		RBX: UNSIGNEDLONG;
		RCX: UNSIGNEDLONG;
		RDX: UNSIGNEDLONG;
		RDI: UNSIGNEDLONG;
		RSI: UNSIGNEDLONG;
		RBP: UNSIGNEDLONG;
		RSP: UNSIGNEDLONG;
		R8: UNSIGNEDLONG;
		R9: UNSIGNEDLONG;
		R10: UNSIGNEDLONG;
		R11: UNSIGNEDLONG;
		R12: UNSIGNEDLONG;
		R13: UNSIGNEDLONG;
		R14: UNSIGNEDLONG;
		R15: UNSIGNEDLONG;
	end;
type
	FPRegIntel = packed array[0..9] of UInt8;
type
	FPUInformationIntel64 = record
		Registers: array[0..7] of FPRegIntel;
		Control: UInt16;
		Status: UInt16;
		Tag: UInt16;
		Opcode: UInt16;
		IP: UInt32;
		DP: UInt32;
		DS: UInt32;
	end;
type
	VectorInformationIntel64 = record
		Registers: array[0..15] of Vector128Intel;
	end;

type
	MachineInformationPtr = ^MachineInformation;
	RegisterInformationPtr = ^RegisterInformation;
	FPUInformationPtr = ^FPUInformation;
	VectorInformationPtr = ^VectorInformation;
	MachineInformation = MachineInformationIntel64;
	RegisterInformation = RegisterInformationIntel64;
	FPUInformation = FPUInformationIntel64;
	VectorInformation = VectorInformationIntel64;
{$endc}  { TARGET_CPU_X86_64 }

{$ifc TARGET_CPU_X86 or TARGET_CPU_X86_64}
type
	ExceptionInformationPtr = ^ExceptionInformation;
	ExceptionInformation = record
		theKind: ExceptionKind;
		machineState: MachineInformationPtr;
		registerImage: RegisterInformationPtr;
		FPUImage: FPUInformationPtr;
		info: ExceptionInfo;
		vectorImage: VectorInformationPtr;
	end;
{$endc} { TARGET_CPU_X86 || TARGET_CPU_X86_64 }


{$ifc TARGET_CPU_ARM64}
type
	MachineInformationPtr = ^MachineInformation;
	MachineInformation = record
		__unusedMachineInformationField: {const} UnivPtr;
	end;
type
	RegisterInformationPtr = ^RegisterInformation;
	RegisterInformation = record
		__unusedRegisterInformationField: {const} UnivPtr;
	end;
type
	FPUInformationPtr = ^FPUInformation;
	FPUInformation = record
		__unusedFPUInformationField: {const} UnivPtr;
	end;
type
	VectorInformationPtr = ^VectorInformation;
	VectorInformation = record
		__unusedVectorInformationField: {const} UnivPtr;
	end;

type
	ExceptionInformationPtr = ^ExceptionInformation;
	ExceptionInformation = record
		theKind: ExceptionKind;
  { XXX: Not implemented }
		machineState: MachineInformationPtr;
		registerImage: RegisterInformationPtr;
		FPUImage: FPUInformationPtr;
		info: ExceptionInfo;
		vectorImage: VectorInformationPtr;
	end;
{$endc}  { TARGET_CPU_ARM64 }

{ 
    Note:   An ExceptionHandler is NOT a UniversalProcPtr, except in Carbon.
            It must be a PowerPC function pointer with NO routine descriptor, 
            except on Carbon, where it must be a UniversalProcPtr (TPP actually)
            to allow the interface to work from both CFM and Mach-O.
}
type
	ExceptionHandlerProcPtr = function( var theException: ExceptionInformation ): OSStatus;
{GPC-ONLY-START}
	ExceptionHandlerUPP = UniversalProcPtr; // should be ExceptionHandlerProcPtr
{GPC-ONLY-ELSE}
	ExceptionHandlerUPP = ExceptionHandlerProcPtr;
{GPC-ONLY-FINISH}

{
 *  NewExceptionHandlerUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewExceptionHandlerUPP( userRoutine: ExceptionHandlerProcPtr ): ExceptionHandlerUPP;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{
 *  DisposeExceptionHandlerUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeExceptionHandlerUPP( userUPP: ExceptionHandlerUPP );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{
 *  InvokeExceptionHandlerUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeExceptionHandlerUPP( var theException: ExceptionInformation; userUPP: ExceptionHandlerUPP ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{
   ExceptionHandler function pointers (TPP):
   on classic PowerPC, use raw function pointers
   on classic PowerPC with OPAQUE_UPP_TYPES=1, use UPP's
   on Carbon, use UPP's
}
{ use UPP's}

type
	ExceptionHandlerTPP = ExceptionHandlerUPP;
	ExceptionHandler = ExceptionHandlerTPP;
{ Routine for installing per-process exception handlers }
{
 *  InstallExceptionHandler()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function InstallExceptionHandler( theHandler: ExceptionHandlerTPP ): ExceptionHandlerTPP;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);



{$endc} {TARGET_OS_MAC}

end.
