{
     File:       OSServices/CSIdentityQuery.h
 
     Contains:   Identity Query APIs
 
     Copyright:  (c) 2006-2011 Apple Inc. All rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit CSIdentityQuery;
interface
uses MacTypes,MacOSXPosix,CSIdentity,CSIdentityAuthority,CFBase,CFArray,CFData,CFError,CFRunLoop,CFUUID;


{$ALIGN MAC68K}


{
 *  CSIdentityQueryGetTypeID()
 *  
 *  Summary:
 *    Retrieve the CFTypeID of the CSIdentityQuery class
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryGetTypeID: CFTypeID;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_5_0);


{
 *  CSIdentityQueryFlags
 *  
 *  Summary:
 *    Execution options for an identity query
 *  
 *  Discussion:
 *    A bit mask for setting execution options on a query
 }
const
{
   * After the intial query phase is complete, monitor the result set
   * for live updates
   }
	kCSIdentityQueryGenerateUpdateEvents = $0001;

  {
   * Include all matching identities in the result set, including
   * hidden "system" users and groups (root, www, etc.)
   }
	kCSIdentityQueryIncludeHiddenIdentities = $0002;

type
	CSIdentityQueryFlags = CFOptionFlags;

{
 *  CSIdentityQueryStringComparisonMethod
 *  
 *  Summary:
 *    Options for querying the database by name
 *  
 *  Discussion:
 *    When searching for identities by name, this value specifies the
 *    string comparison function
 }
const
{
   * The identity name must equal the search string
   }
	kCSIdentityQueryStringEquals = 1;

  {
   * The identity name must begin with the search string
   }
	kCSIdentityQueryStringBeginsWith = 2;

type
	CSIdentityQueryStringComparisonMethod = CFIndex;
{$ifc not TARGET_OS_IPHONE and not TARGET_IPHONE_SIMULATOR}
{
 *  CSIdentityQueryCreate()
 *  
 *  Summary:
 *    Creates an identity query object for all identities in the
 *    specified authority
 *  
 *  Discussion:
 *    The results of this query include all of the identities in the
 *    specified authority's database.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    allocator:
 *      The allocator to use for this instance
 *    
 *    identityClass:
 *      The class of identity to find
 *    
 *    authority:
 *      The identity authority to query
 *  
 *  Result:
 *    A new CSIdentityQuery object
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryCreate( allocator: CFAllocatorRef; identityClass: CSIdentityClass; authority: CSIdentityAuthorityRef ): CSIdentityQueryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);
{$endc} {!TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR}

{
 *  CSIdentityQueryCreateForName()
 *  
 *  Summary:
 *    Creates an identity query object based on a name
 *  
 *  Discussion:
 *    The query finds identities by name. It searches the full names,
 *    posix names and aliases for matches.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    allocator:
 *      The allocator to use for this instance
 *    
 *    name:
 *      The name criteria for the query.
 *    
 *    comparisonMethod:
 *      The comparision function (equal or begins with)
 *    
 *    identityClass:
 *      The class of identity to find
 *    
 *    authority:
 *      The identity authority to query
 *  
 *  Result:
 *    A new CSIdentityQuery object
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryCreateForName( allocator: CFAllocatorRef; name: CFStringRef; comparisonMethod: CSIdentityQueryStringComparisonMethod; identityClass: CSIdentityClass; authority: CSIdentityAuthorityRef ): CSIdentityQueryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_5_0);


{$ifc not TARGET_OS_IPHONE and not TARGET_IPHONE_SIMULATOR}
{
 *  CSIdentityQueryCreateForUUID()
 *  
 *  Summary:
 *    Creates an identity query object based on a UUID
 *  
 *  Discussion:
 *    Finds an identity by its UUID
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    allocator:
 *      The allocator to use for this instance
 *    
 *    uuid:
 *      The UUID of the identity to find
 *    
 *    authority:
 *      The identity authority to query
 *  
 *  Result:
 *    A new CSIdentityQuery object
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryCreateForUUID( allocator: CFAllocatorRef; uuid: CFUUIDRef; authority: CSIdentityAuthorityRef ): CSIdentityQueryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);


{
 *  CSIdentityQueryCreateForPosixID()
 *  
 *  Summary:
 *    Creates an identity query object based on a POSIX ID
 *  
 *  Discussion:
 *    Finds an identity by its UID or GID
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    allocator:
 *      The allocator to use for this instance
 *    
 *    posixID:
 *      The UID or GID of the identity to find
 *    
 *    identityClass:
 *      The class of identity to find
 *    
 *    authority:
 *      The identity authority to query
 *  
 *  Result:
 *    A new CSIdentityQuery object
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryCreateForPosixID( allocator: CFAllocatorRef; posixID: id_t; identityClass: CSIdentityClass; authority: CSIdentityAuthorityRef ): CSIdentityQueryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);
{$endc} {!TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR}

{
 *  CSIdentityQueryCreateForPersistentReference()
 *  
 *  Summary:
 *    Creates an identity query object based on an identity reference
 *    data object
 *  
 *  Discussion:
 *    Finds an identity by reference data obtained from
 *    CSIdentityCreateReferenceData
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    allocator:
 *      The allocator to use for this instance
 *    
 *    referenceData:
 *      The reference data that fully describes an identity
 *  
 *  Result:
 *    A new CSIdentityQuery object
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryCreateForPersistentReference( allocator: CFAllocatorRef; referenceData: CFDataRef ): CSIdentityQueryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_5_0);


{$ifc not TARGET_OS_IPHONE and not TARGET_IPHONE_SIMULATOR}
{
 *  CSIdentityQueryCreateForCurrentUser()
 *  
 *  Summary:
 *    Creates a query for the current session user's identity
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    allocator:
 *      The allocator to use for this instance
 *  
 *  Result:
 *    A new CSIdentityQuery object
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryCreateForCurrentUser( allocator: CFAllocatorRef ): CSIdentityQueryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);
{$endc} {!TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR}

{
 *  CSIdentityQueryCopyResults()
 *  
 *  Summary:
 *    Retrieve the results of executing an identity query
 *  
 *  Discussion:
 *    Returns an immutable array of CSIdentityRefs, reflecting the
 *    current results of the query's execution.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    query:
 *      The query object to access
 *  
 *  Result:
 *    An array of zero or more CSIdentityRefs
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryCopyResults( query: CSIdentityQueryRef ): CFArrayRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_5_0);


{
 *  CSIdentityQueryExecute()
 *  
 *  Summary:
 *    Execute an identity query synchronously
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    query:
 *      The query object to execute
 *    
 *    flags:
 *      Execution options
 *    
 *    error:
 *      Optional pointer to a CFError object which must be released by
 *      the caller if CSIdentityQueryExecute returns false
 *  
 *  Result:
 *    Returns true if the query executed successfully, false if an
 *    error occurred.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryExecute( query: CSIdentityQueryRef; flags: CSIdentityQueryFlags; error: CFErrorRefPtr { can be NULL } ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_5_0);


{
 *  CSIdentityQueryEvent
 *  
 *  Summary:
 *    Results from executing an asynchronous query
 *  
 *  Discussion:
 *    Events generated during asynchronous query execution
 }
const
{
   * Event generated when the initial lookup of identities has
   * finished. Live update events will follow if caller requests the
   * kCSIdentityQueryGenerateUpdateEvents option.
   }
	kCSIdentityQueryEventSearchPhaseFinished = 1;

  {
   * Event generated when identities are added to the query results
   }
	kCSIdentityQueryEventResultsAdded = 2;

  {
   * Event generated when identities already in the query results have
   * been modified
   }
	kCSIdentityQueryEventResultsChanged = 3;

  {
   * Event generated when identities are removed from the query results
   }
	kCSIdentityQueryEventResultsRemoved = 4;

  {
   * Used to report an error. Query execution stops (permanently) if
   * this event is sent.
   }
	kCSIdentityQueryEventErrorOccurred = 5;

type
	CSIdentityQueryEvent = CFIndex;

{
 *  CSIdentityQueryReceiveEventCallback
 *  
 *  Summary:
 *    The client event callback function for receiving asynchronous
 *    query events
 *  
 *  Parameters:
 *    
 *    query:
 *      The identity query object that has completed an event
 *    
 *    event:
 *      The event the identity query object has completed
 *    
 *    identities:
 *      a CFArray containing identities resulting from the query
 *    
 *    error:
 *      A CFError object if there was an error from the query
 *    
 *    info:
 *      Any other information you want passed to the callback function
 }
type
	CSIdentityQueryReceiveEventCallback = procedure( query: CSIdentityQueryRef; event: CSIdentityQueryEvent; identities: CFArrayRef; error: CFErrorRef; info: univ Ptr );

{
 *  CSIdentityQueryClientContext
 *  
 *  Summary:
 *    Client structure specifying callbacks and private context data
 }
type
	CSIdentityQueryClientContext = record
		version: CFIndex;
		info: UnivPtr;
		retainInfo: CFAllocatorRetainCallBack;
		releaseInfo: CFAllocatorReleaseCallBack;
		copyInfoDescription: CFAllocatorCopyDescriptionCallBack;
		receiveEvent: CSIdentityQueryReceiveEventCallback;
	end;
{
 *  CSIdentityQueryExecuteAsynchronously()
 *  
 *  Summary:
 *    Execute an identity query asynchronously
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    query:
 *      The query object to execute
 *    
 *    flags:
 *      Execution options
 *    
 *    clientContext:
 *      The client context and callbacks to be used during execution
 *    
 *    runLoop:
 *      The run loop on which to schedule callbacks
 *    
 *    runLoopMode:
 *      The run loop mode in which callbacks may be scheduled
 *  
 *  Result:
 *    Returns true if query execution started, false if the query has
 *    already been executed.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityQueryExecuteAsynchronously( query: CSIdentityQueryRef; flags: CSIdentityQueryFlags; const var clientContext: CSIdentityQueryClientContext; runLoop: CFRunLoopRef; runLoopMode: CFStringRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_5_0);


{
 *  CSIdentityQueryStop()
 *  
 *  Summary:
 *    Invalidate an identity query client
 *  
 *  Discussion:
 *    Invalidate a query client so that its callback will never be
 *    called in the future. Clients should call CSIdentityQueryStop
 *    when an query will no longer be used, prior to releasing the
 *    final query reference.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    query:
 *      The query to access
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure CSIdentityQueryStop( query: CSIdentityQueryRef );
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_5_0);



end.
