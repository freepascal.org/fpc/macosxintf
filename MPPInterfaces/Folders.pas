{
     File:       CarbonCore/Folders.h
 
     Contains:   Folder Manager Interfaces.
                 The contents of this header file are deprecated.
                 Use NSFileManager instead.
 
     Copyright:  � 1995-2011 by Apple Inc. All rights reserved.
}
{     Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, June 2018 }

unit Folders;
interface
uses MacTypes,Files;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}


{
    Common folder locations:
    ========================
    kSystemDomain is generally /System and things inside it, so
     - kSystemDomain, kDomainLibraryFolderType is /System/Library/
     - kSystemDomain, kTrashFolderType is a trash folder on the same volume as /System ( the boot disk )
     - kSystemDomain, kDomainTopLevelFolderType is the root of the system domain, so it's the same as /System
    kLocalDomain is generally the admin-writeable, system-wide location for things, so
     - kLocalDomain, kDomainLibraryFolderType is /Library/
    kUserDomain maps to the current user's home folder, so that
     - kUserDomain, kCurrentUserFolderType is the user's home folder itself ( "$HOME", ~ )
     - kUserDomain, kDomainLibraryFolderType is the Library folder in the user home ( "$HOME/Library", "~/Library/" )
     - kUserDomain, kPreferencesFolderType is the Preferences folder in the user home ( "$HOME/Library/Preferences/" )
     - kUserDomain, kTrashFolderType is a trash folder on the same volume as the user home
    kNetworkDomain, if configured, is a network file system which a network administrator may have installed items into
     - kNetworkDomain, kApplicationsFolderType is /Network/Applications/
    kClassicDomain, if configured, is where the Mac OS X Classic environment information is stored
     - kClassicDomain, kSystemFolderType is the currently active Macintosh Classic system folder ( or fnfErr if a Classic isn't installed )
}

const
	kOnSystemDisk = -32768; { previously was 0x8000 but that is an unsigned value whereas vRefNum is signed}
	kOnAppropriateDisk = -32767; { Generally, the same as kOnSystemDisk, but it's clearer that this isn't always the 'boot' disk.}
                                        { Folder Domains - Carbon only.  The constants above can continue to be used, but the folder/volume returned will}
                                        { be from one of the domains below.}
	kSystemDomain = -32766; { Read-only system hierarchy.}
	kLocalDomain = -32765; { All users of a single machine have access to these resources.}
	kNetworkDomain = -32764; { All users configured to use a common network server has access to these resources.}
	kUserDomain = -32763; { Read/write. Resources that are private to the user.}
	kClassicDomain = -32762; { Domain referring to the currently configured Classic System Folder.  Not supported in Mac OS X Leopard and later.}
	kFolderManagerLastDomain = -32760;

{
   The ID of the last domain in the above list, used by the Folder Manager to determine if a given 
   parameter should be treated as a domain or a volume...
}
const
	kLastDomainConstant = -32760;

const
	kCreateFolder = true;
	kDontCreateFolder = false;

{
 *  FindFolder()
 *  
 *  Summary:
 *    Obtains location information for system-related directories.
 *  
 *  Discussion:
 *    For the folder type on the particular volume (specified,
 *    respectively, in the folderType and vRefNum parameters), the
 *    FindFolder function returns the directory's volume reference
 *    number in the foundVRefNum parameter and its directory ID in the
 *    foundDirID parameter.
 *    
 *    The specified folder used for a given volume might be located on
 *    a different volume in future versions of system software;
 *    therefore, do not assume the volume that you specify in vRefNum
 *    and the volume returned in foundVRefNum will be the same.
 *     
 *    Specify a volume reference number (or the constant kOnSystemDisk
 *    for the startup disk) or one of the domain constants ( on Mac OS
 *    X ) in the vRefNum parameter.
 *    
 *    Specify a four-character folder type--or the constant that
 *    represents it--in the folderType parameter.
 *    
 *    Use the constant kCreateFolder in the createFolder parameter to
 *    tell FindFolder to create a directory if it does not already
 *    exist; otherwise, use the constant kDontCreateFolder. Directories
 *    inside the System Folder are created only if the System Folder
 *    directory exists. The FindFolder function will not create a
 *    System Folder directory even if you specify the kCreateFolder
 *    constant in the createFolder parameter.
 *    
 *    The FindFolder function returns a nonzero result code if the
 *    folder isn't found, and it can also return other file system
 *    errors reported by the File Manager or Memory Manager.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    vRefNum:
 *      Pass the volume reference number of the volume on which you
 *      want to locate a directory, or a constant specifying a disk or
 *      domain.   The constants which you can use in this parameter are
 *      described in "Disk and Domain Constants".
 *      Note that, on Mac OS X, passing a volume reference number in
 *      this parameter does not make sense for most of the folder type
 *      selectors which you can specify in the folderType parameter. On
 *      Mac OS X, folders are "domain-oriented"; because there may be
 *      more than one domain on any given physical volume, asking for
 *      these folders on a per-volume basis yields undefined results.
 *      For example, if you were to request the Fonts folder
 *      (represented by the selector kFontsFolderType ) on volume -100,
 *      are you requesting the folder /System/Library/Fonts,
 *      /Library/Fonts, or ~/Fonts? On Mac OS X you should pass a disk
 *      or domain constant in this parameter.
 *    
 *    folderType:
 *      A four-character folder type, or a constant that represents the
 *      type, for the directory you want to find.
 *    
 *    createFolder:
 *      Pass the constant kCreateFolder in this parameter to create a
 *      directory if it does not already exist; otherwise, pass the
 *      constant kDontCreateFolder.
 *    
 *    foundVRefNum:
 *      The volume reference number, returned by FindFolder , for the
 *      volume containing the directory you specify in the folderType
 *      parameter.
 *    
 *    foundDirID:
 *      The directory ID number, returned by FindFolder , for the
 *      directory you specify in the folderType parameter.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FindFolder( vRefNum: FSVolumeRefNum; folderType: OSType; createFolder: Boolean; var foundVRefNum: FSVolumeRefNum; var foundDirID: SInt32 ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  ReleaseFolder()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    This call is not needed on Mac OS X and later.
 *  
 *  Summary:
 *    On Mac OS 9.x and earlier, release any hold the system may have
 *    on a given folder on a volume so that the volume may be unmounted.
 *  
 *  Discussion:
 *    On Mac OS 9.x, the system sometimes has files open on volumes
 *    which need to be closed in order for the volume to be
 *    successfully unmounted.  This call releases any hold the Folder
 *    Manager may have for the given volume.
 *    <br> This call is unnecessary on Mac OS X and later.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    vRefNum:
 *      The vRefNum to release.
 *    
 *    folderType:
 *      The folder type to release.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function ReleaseFolder( vRefNum: FSVolumeRefNum; folderType: OSType ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);


{
 *  FSFindFolder()
 *  
 *  Summary:
 *    FSFindFolder returns an FSRef for certain system-related
 *    directories.
 *  
 *  Discussion:
 *    For the folder type on the particular volume (specified,
 *    respectively, in the folderType and vRefNum parameters), the
 *    FindFolder function returns the FSRef of that directory. 
 *     
 *    The specified folder used for a given volume might be located on
 *    a different volume in future versions of system software;
 *    therefore, do not assume the volume that you specify in vRefNum
 *    and the volume returned in the FSRef will be the same.
 *    
 *    Specify a volume reference number (or the constant kOnSystemDisk
 *    for the startup disk) or one of the domain constants ( on Mac OS
 *    X ) in the vRefNum parameter.
 *    
 *    Specify a four-character folder type--or the constant that
 *    represents it--in the folderType parameter.
 *    
 *    Use the constant kCreateFolder in the createFolder parameter to
 *    tell FindFolder to create a directory if it does not already
 *    exist; otherwise, use the constant kDontCreateFolder. Directories
 *    inside the System Folder are created only if the System Folder
 *    directory exists. The FindFolder function will not create a
 *    System Folder directory even if you specify the kCreateFolder
 *    constant in the createFolder parameter.
 *    
 *    The FindFolder function returns a nonzero result code if the
 *    folder isn't found, and it can also return other file system
 *    errors reported by the File Manager or Memory Manager.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    vRefNum:
 *      The volume reference number (or the constant kOnSystemDisk for
 *      the startup disk) or one of the domain constants ( like
 *      kUserDomain ) of the volume or domain in which you want to
 *      locate a directory.
 *    
 *    folderType:
 *      A four-character folder type, or a constant that represents the
 *      type, for the directory you want to find.
 *    
 *    createFolder:
 *      Pass the constant kCreateFolder in this parameter to create a
 *      directory if it does not already exist; otherwise, pass the
 *      constant kDontCreateFolder.
 *    
 *    foundRef:
 *      The FSRef for the directory you specify on the volume or domain
 *      and folderType given.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSFindFolder( vRefNum: FSVolumeRefNum; folderType: OSType; createFolder: Boolean; var foundRef: FSRef ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{//}

{
 *  Folder types
 *  
 }
const
	kDesktopFolderType = FOUR_CHAR_CODE('desk'); { the desktop folder; objects in this folder show on the desktop. }
	kTrashFolderType = FOUR_CHAR_CODE('trsh'); { the trash folder; objects in this folder show up in the trash }
	kWhereToEmptyTrashFolderType = FOUR_CHAR_CODE('empt'); { the "empty trash" folder; Finder starts empty from here down }
	kFontsFolderType = FOUR_CHAR_CODE('font'); { Fonts go here }
	kPreferencesFolderType = FOUR_CHAR_CODE('pref'); { preferences for applications go here }
	kSystemPreferencesFolderType = FOUR_CHAR_CODE('sprf'); { the PreferencePanes folder, where Mac OS X Preference Panes go }
	kTemporaryFolderType = FOUR_CHAR_CODE('temp'); {    On Mac OS X, each user has their own temporary items folder, and the Folder Manager attempts to set permissions of these}
                                        {    folders such that other users can not access the data inside.  On Mac OS X 10.4 and later the data inside the temporary}
                                        {    items folder is deleted at logout and at boot, but not otherwise.  Earlier version of Mac OS X would delete items inside}
                                        {    the temporary items folder after a period of inaccess.  You can ask for a temporary item in a specific domain or on a }
                                        {    particular volume by FSVolumeRefNum.  If you want a location for temporary items for a short time, then use either}
                                        {    ( kUserDomain, kkTemporaryFolderType ) or ( kSystemDomain, kTemporaryFolderType ).  The kUserDomain varient will always be}
                                        {    on the same volume as the user's home folder, while the kSystemDomain version will be on the same volume as /var/tmp/ ( and}
                                        {    will probably be on the local hard drive in case the user's home is a network volume ).  If you want a location for a temporary}
                                        {    file or folder to use for saving a document, especially if you want to use FSpExchangeFile() to implement a safe-save, then}
                                        {    ask for the temporary items folder on the same volume as the file you are safe saving.}
                                        {    However, be prepared for a failure to find a temporary folder in any domain or on any volume.  Some volumes may not have}
                                        {    a location for a temporary folder, or the permissions of the volume may be such that the Folder Manager can not return}
                                        {    a temporary folder for the volume.}
                                        {    If your application creates an item in a temporary items older you should delete that item as soon as it is not needed,}
                                        {    and certainly before your application exits, since otherwise the item is consuming disk space until the user logs out or}
                                        {    restarts.  Any items left inside a temporary items folder should be moved into a folder inside the Trash folder on the disk}
                                        {    when the user logs in, inside a folder named "Recovered items", in case there is anything useful to the end user.}
	kChewableItemsFolderType = FOUR_CHAR_CODE('flnt'); { similar to kTemporaryItemsFolderType, except items in this folder are deleted at boot or when the disk is unmounted }
	kTemporaryItemsInCacheDataFolderType = FOUR_CHAR_CODE('vtmp'); { A folder inside the kCachedDataFolderType for the given domain which can be used for transient data}
	kApplicationsFolderType = FOUR_CHAR_CODE('apps'); {    Applications on Mac OS X are typically put in this folder ( or a subfolder ).}
	kVolumeRootFolderType = FOUR_CHAR_CODE('root'); { root folder of a volume or domain }
	kDomainTopLevelFolderType = FOUR_CHAR_CODE('dtop'); { The top-level of a Folder domain, e.g. "/System"}
	kDomainLibraryFolderType = FOUR_CHAR_CODE('dlib'); { the Library subfolder of a particular domain}
	kUsersFolderType = FOUR_CHAR_CODE('usrs'); { "Users" folder, usually contains one folder for each user. }
	kCurrentUserFolderType = FOUR_CHAR_CODE('cusr'); { The folder for the currently logged on user; domain passed in is ignored. }
	kSharedUserDataFolderType = FOUR_CHAR_CODE('sdat'); { A Shared folder, readable & writeable by all users }

{
    The following selectors refer specifically to subfolders inside the user's home folder, and should
    be used only with  kUserDomain as the domain in the various FindFolder calls.
}
const
	kDocumentsFolderType = FOUR_CHAR_CODE('docs'); {    User documents are typically put in this folder ( or a subfolder ).}
	kPictureDocumentsFolderType = FOUR_CHAR_CODE('pdoc'); { Refers to the "Pictures" folder in a users home directory}
	kMovieDocumentsFolderType = FOUR_CHAR_CODE('mdoc'); { Refers to the "Movies" folder in a users home directory}
	kMusicDocumentsFolderType = FOUR_CHAR_CODE('�doc'); { Refers to the "Music" folder in a users home directory}
	kInternetSitesFolderType = FOUR_CHAR_CODE('site'); { Refers to the "Sites" folder in a users home directory}
	kPublicFolderType = FOUR_CHAR_CODE('pubb'); { Refers to the "Public" folder in a users home directory}

{  The following selectors are available on Mac OS X 10.7 and later.}
const
	kDropBoxFolderType = FOUR_CHAR_CODE('drop'); { Refers to the "Drop Box" folder inside the user's home directory}

const
	kSharedLibrariesFolderType = FOUR_CHAR_CODE('�lib'); { for general shared libs. }
	kVoicesFolderType = FOUR_CHAR_CODE('fvoc'); { macintalk can live here }
	kUtilitiesFolderType = FOUR_CHAR_CODE('uti�'); { for Utilities folder }
	kThemesFolderType = FOUR_CHAR_CODE('thme'); { for Theme data files }
	kFavoritesFolderType = FOUR_CHAR_CODE('favs'); { Favorties folder for Navigation Services }
	kInternetSearchSitesFolderType = FOUR_CHAR_CODE('issf'); { Internet Search Sites folder }
	kInstallerLogsFolderType = FOUR_CHAR_CODE('ilgf'); { Installer Logs folder }
	kScriptsFolderType = FOUR_CHAR_CODE('scr�'); { Scripts folder }
	kFolderActionsFolderType = FOUR_CHAR_CODE('fasf'); { Folder Actions Scripts folder }
	kSpeakableItemsFolderType = FOUR_CHAR_CODE('spki'); { Speakable Items folder }
	kKeychainFolderType = FOUR_CHAR_CODE('kchn'); { Keychain folder }

{ New Folder Types to accommodate the Mac OS X Folder Manager }
{ These folder types are not applicable on Mac OS 9.          }
const
	kColorSyncFolderType = FOUR_CHAR_CODE('sync'); { Contains ColorSync-related folders}
	kColorSyncCMMFolderType = FOUR_CHAR_CODE('ccmm'); { ColorSync CMMs}
	kColorSyncScriptingFolderType = FOUR_CHAR_CODE('cscr'); { ColorSync Scripting support}
	kPrintersFolderType = FOUR_CHAR_CODE('impr'); { Contains Printing-related folders}
	kSpeechFolderType = FOUR_CHAR_CODE('spch'); { Contains Speech-related folders}
	kCarbonLibraryFolderType = FOUR_CHAR_CODE('carb'); { Contains Carbon-specific file}
	kDocumentationFolderType = FOUR_CHAR_CODE('info'); { Contains Documentation files (not user documents)}
	kISSDownloadsFolderType = FOUR_CHAR_CODE('issd'); { Contains Internet Search Sites downloaded from the Internet}
	kUserSpecificTmpFolderType = FOUR_CHAR_CODE('utmp'); { Contains temporary items created on behalf of the current user}
	kCachedDataFolderType = FOUR_CHAR_CODE('cach'); { Contains various cache files for different clients}
	kFrameworksFolderType = FOUR_CHAR_CODE('fram'); { Contains MacOS X Framework folders}
	kPrivateFrameworksFolderType = FOUR_CHAR_CODE('pfrm'); { Contains MacOS X Private Framework folders     }
	kClassicDesktopFolderType = FOUR_CHAR_CODE('sdsk'); { MacOS 9 compatible desktop folder - same as kSystemDesktopFolderType but with a more appropriate name for Mac OS X code.}
	kSystemSoundsFolderType = FOUR_CHAR_CODE('ssnd'); { Contains Mac OS X System Sound Files ( valid in kSystemDomain, kLocalDomain, and kUserDomain )}
	kComponentsFolderType = FOUR_CHAR_CODE('cmpd'); { Contains Mac OS X components   ( valid in kSystemDomain, kLocalDomain, and kUserDomain )}
	kQuickTimeComponentsFolderType = FOUR_CHAR_CODE('wcmp'); { Contains QuickTime components for Mac OS X ( valid in kSystemDomain, kLocalDomain, and kUserDomain )}
	kCoreServicesFolderType = FOUR_CHAR_CODE('csrv'); { Refers to the "/System/Library/CoreServices" folder on Mac OS X}
	kAudioSupportFolderType = FOUR_CHAR_CODE('adio'); { Refers to the Audio support folder for Mac OS X}
	kAudioPresetsFolderType = FOUR_CHAR_CODE('apst'); { "Presets" folder of "Audio" folder, Mac OS X 10.4 and later}
	kAudioSoundsFolderType = FOUR_CHAR_CODE('asnd'); { Refers to the Sounds subfolder of Audio Support}
	kAudioSoundBanksFolderType = FOUR_CHAR_CODE('bank'); { Refers to the Banks subfolder of the Sounds Folder}
	kAudioAlertSoundsFolderType = FOUR_CHAR_CODE('alrt'); { Refers to the Alert Sounds subfolder of the Sound Folder}
	kAudioPlugInsFolderType = FOUR_CHAR_CODE('aplg'); { Refers to the Plug-ins subfolder of the Audio Folder   }
	kAudioComponentsFolderType = FOUR_CHAR_CODE('acmp'); { Refers to the Components subfolder of the Audio Plug-ins Folder    }
	kKernelExtensionsFolderType = FOUR_CHAR_CODE('kext'); { Refers to the Kernel Extensions Folder on Mac OS X}
	kDirectoryServicesFolderType = FOUR_CHAR_CODE('dsrv'); { Refers to the Directory Services folder on Mac OS X}
	kDirectoryServicesPlugInsFolderType = FOUR_CHAR_CODE('dplg'); { Refers to the Directory Services Plug-Ins folder on Mac OS X }
	kInstallerReceiptsFolderType = FOUR_CHAR_CODE('rcpt'); { Refers to the "Receipts" folder in Mac OS X}
	kFileSystemSupportFolderType = FOUR_CHAR_CODE('fsys'); { Refers to the [domain]/Library/Filesystems folder in Mac OS X}
	kAppleShareSupportFolderType = FOUR_CHAR_CODE('shar'); { Refers to the [domain]/Library/Filesystems/AppleShare folder in Mac OS X}
	kAppleShareAuthenticationFolderType = FOUR_CHAR_CODE('auth'); { Refers to the [domain]/Library/Filesystems/AppleShare/Authentication folder in Mac OS X}
	kMIDIDriversFolderType = FOUR_CHAR_CODE('midi'); { Refers to the MIDI Drivers folder on Mac OS X}
	kKeyboardLayoutsFolderType = FOUR_CHAR_CODE('klay'); { Refers to the [domain]/Library/KeyboardLayouts folder in Mac OS X}
	kIndexFilesFolderType = FOUR_CHAR_CODE('indx'); { Refers to the [domain]/Library/Indexes folder in Mac OS X}
	kFindByContentIndexesFolderType = FOUR_CHAR_CODE('fbcx'); { Refers to the [domain]/Library/Indexes/FindByContent folder in Mac OS X}
	kManagedItemsFolderType = FOUR_CHAR_CODE('mang'); { Refers to the Managed Items folder for Mac OS X }
	kBootTimeStartupItemsFolderType = FOUR_CHAR_CODE('empz'); { Refers to the "StartupItems" folder of Mac OS X }
	kAutomatorWorkflowsFolderType = FOUR_CHAR_CODE('flow'); { Automator Workflows folder }
	kAutosaveInformationFolderType = FOUR_CHAR_CODE('asav'); { ~/Library/Autosaved Information/ folder, can be used to store autosave information for user's applications.  Available in Mac OS X 10.4 and later.  }
	kSpotlightSavedSearchesFolderType = FOUR_CHAR_CODE('spot'); { Usually ~/Library/Saved Searches/; used by Finder and Nav/Cocoa panels to find saved Spotlight searches }
                                        { The following folder types are available in Mac OS X 10.5 and later }
	kSpotlightImportersFolderType = FOUR_CHAR_CODE('simp'); { Folder for Spotlight importers, usually /Library/Spotlight/ or ~/Library/Spotlight, etc. }
	kSpotlightMetadataCacheFolderType = FOUR_CHAR_CODE('scch'); { Folder for Spotlight metadata caches, for example: ~/Library/Caches/Metadata/ }
	kInputManagersFolderType = FOUR_CHAR_CODE('inpt'); { InputManagers }
	kInputMethodsFolderType = FOUR_CHAR_CODE('inpf'); { ../Library/Input Methods/ }
	kLibraryAssistantsFolderType = FOUR_CHAR_CODE('astl'); { Refers to the [domain]/Library/Assistants folder}
	kAudioDigidesignFolderType = FOUR_CHAR_CODE('adig'); { Refers to the Digidesign subfolder of the Audio Plug-ins folder}
	kAudioVSTFolderType = FOUR_CHAR_CODE('avst'); { Refers to the VST subfolder of the Audio Plug-ins folder}
	kColorPickersFolderType = FOUR_CHAR_CODE('cpkr'); { Refers to the ColorPickers folder}
	kCompositionsFolderType = FOUR_CHAR_CODE('cmps'); { Refers to the Compositions folder}
	kFontCollectionsFolderType = FOUR_CHAR_CODE('fncl'); { Refers to the FontCollections folder}
	kiMovieFolderType = FOUR_CHAR_CODE('imov'); { Refers to the iMovie folder}
	kiMoviePlugInsFolderType = FOUR_CHAR_CODE('impi'); { Refers to the Plug-ins subfolder of the iMovie Folder}
	kiMovieSoundEffectsFolderType = FOUR_CHAR_CODE('imse'); { Refers to the Sound Effects subfolder of the iMovie Folder}
	kDownloadsFolderType = FOUR_CHAR_CODE('down'); { Refers to the ~/Downloads folder}

const
	kColorSyncProfilesFolderType = FOUR_CHAR_CODE('prof'); { for ColorSync� Profiles }
	kApplicationSupportFolderType = FOUR_CHAR_CODE('asup'); { third-party items and folders }
	kTextEncodingsFolderType = FOUR_CHAR_CODE('�tex'); { encoding tables }
	kPrinterDescriptionFolderType = FOUR_CHAR_CODE('ppdf'); { new folder at root of System folder for printer descs. }
	kPrinterDriverFolderType = FOUR_CHAR_CODE('�prd'); { new folder at root of System folder for printer drivers }
	kScriptingAdditionsFolderType = FOUR_CHAR_CODE('�scr'); { at root of system folder }

const
	kClassicPreferencesFolderType = FOUR_CHAR_CODE('cprf'); { "Classic" folder in ~/Library/ for redirected preference files. }

const
	kQuickLookFolderType = FOUR_CHAR_CODE('qlck'); { The QuickLook folder, supported in Mac OS X 10.6 and later. }

const
	kServicesFolderType = FOUR_CHAR_CODE('svcs'); { The services folder, supported in Mac OS X 10.7 and later. }

const
{    The following selectors really only make sense when used within the Classic environment on Mac OS X.}
	kSystemFolderType = FOUR_CHAR_CODE('macs'); { the system folder }
	kSystemDesktopFolderType = FOUR_CHAR_CODE('sdsk'); { the desktop folder at the root of the hard drive, never the redirected user desktop folder }
	kSystemTrashFolderType = FOUR_CHAR_CODE('strs'); { the trash folder at the root of the drive, never the redirected user trash folder }
	kPrintMonitorDocsFolderType = FOUR_CHAR_CODE('prnt'); { Print Monitor documents }
	kALMModulesFolderType = FOUR_CHAR_CODE('walk'); { for Location Manager Module files except type 'thng' (within kExtensionFolderType) }
	kALMPreferencesFolderType = FOUR_CHAR_CODE('trip'); { for Location Manager Preferences (within kPreferencesFolderType; contains kALMLocationsFolderType) }
	kALMLocationsFolderType = FOUR_CHAR_CODE('fall'); { for Location Manager Locations (within kALMPreferencesFolderType) }
	kAppleExtrasFolderType = FOUR_CHAR_CODE('aex�'); { for Apple Extras folder }
	kContextualMenuItemsFolderType = FOUR_CHAR_CODE('cmnu'); { for Contextual Menu items }
	kMacOSReadMesFolderType = FOUR_CHAR_CODE('mor�'); { for MacOS ReadMes folder }
	kStartupFolderType = FOUR_CHAR_CODE('strt'); { Finder objects (applications, documents, DAs, aliases, to...) to open at startup go here }
	kShutdownFolderType = FOUR_CHAR_CODE('shdf'); { Finder objects (applications, documents, DAs, aliases, to...) to open at shutdown go here }
	kAppleMenuFolderType = FOUR_CHAR_CODE('amnu'); { Finder objects to put into the Apple menu go here }
	kControlPanelFolderType = FOUR_CHAR_CODE('ctrl'); { Control Panels go here (may contain INITs) }
	kSystemControlPanelFolderType = FOUR_CHAR_CODE('sctl'); { System control panels folder - never the redirected one, always "Control Panels" inside the System Folder }
	kExtensionFolderType = FOUR_CHAR_CODE('extn'); { System extensions go here }
	kExtensionDisabledFolderType = FOUR_CHAR_CODE('extD');
	kControlPanelDisabledFolderType = FOUR_CHAR_CODE('ctrD');
	kSystemExtensionDisabledFolderType = FOUR_CHAR_CODE('macD');
	kStartupItemsDisabledFolderType = FOUR_CHAR_CODE('strD');
	kShutdownItemsDisabledFolderType = FOUR_CHAR_CODE('shdD');
	kAssistantsFolderType = FOUR_CHAR_CODE('ast�'); { for Assistants (MacOS Setup Assistant, etc) }
	kStationeryFolderType = FOUR_CHAR_CODE('odst'); { stationery }
	kOpenDocFolderType = FOUR_CHAR_CODE('odod'); { OpenDoc root }
	kOpenDocShellPlugInsFolderType = FOUR_CHAR_CODE('odsp'); { OpenDoc Shell Plug-Ins in OpenDoc folder }
	kEditorsFolderType = FOUR_CHAR_CODE('oded'); { OpenDoc editors in MacOS Folder }
	kOpenDocEditorsFolderType = FOUR_CHAR_CODE('�odf'); { OpenDoc subfolder of Editors folder }
	kOpenDocLibrariesFolderType = FOUR_CHAR_CODE('odlb'); { OpenDoc libraries folder }
	kGenEditorsFolderType = FOUR_CHAR_CODE('�edi'); { CKH general editors folder at root level of Sys folder }
	kHelpFolderType = FOUR_CHAR_CODE('�hlp'); { CKH help folder currently at root of system folder }
	kInternetPlugInFolderType = FOUR_CHAR_CODE('�net'); { CKH internet plug ins for browsers and stuff }
	kModemScriptsFolderType = FOUR_CHAR_CODE('�mod'); { CKH modem scripts, get 'em OUT of the Extensions folder }
	kControlStripModulesFolderType = FOUR_CHAR_CODE('sdev'); { CKH for control strip modules }
	kInternetFolderType = FOUR_CHAR_CODE('int�'); { Internet folder (root level of startup volume) }
	kAppearanceFolderType = FOUR_CHAR_CODE('appr'); { Appearance folder (root of system folder) }
	kSoundSetsFolderType = FOUR_CHAR_CODE('snds'); { Sound Sets folder (in Appearance folder) }
	kDesktopPicturesFolderType = FOUR_CHAR_CODE('dtp�'); { Desktop Pictures folder (in Appearance folder) }
	kFindSupportFolderType = FOUR_CHAR_CODE('fnds'); { Find support folder }
	kRecentApplicationsFolderType = FOUR_CHAR_CODE('rapp'); { Recent Applications folder }
	kRecentDocumentsFolderType = FOUR_CHAR_CODE('rdoc'); { Recent Documents folder }
	kRecentServersFolderType = FOUR_CHAR_CODE('rsvr'); { Recent Servers folder }
	kLauncherItemsFolderType = FOUR_CHAR_CODE('laun'); { Launcher Items folder }
	kQuickTimeExtensionsFolderType = FOUR_CHAR_CODE('qtex'); { QuickTime Extensions Folder (in Extensions folder) }
	kDisplayExtensionsFolderType = FOUR_CHAR_CODE('dspl'); { Display Extensions Folder (in Extensions folder) }
	kMultiprocessingFolderType = FOUR_CHAR_CODE('mpxf'); { Multiprocessing Folder (in Extensions folder) }
	kPrintingPlugInsFolderType = FOUR_CHAR_CODE('pplg'); { Printing Plug-Ins Folder (in Extensions folder) }
	kAppleshareAutomountServerAliasesFolderType = FOUR_CHAR_CODE('srv�'); { Appleshare puts volumes to automount inside this folder. }
	kVolumeSettingsFolderType = FOUR_CHAR_CODE('vsfd'); { Volume specific user information goes here }
	kPreMacOS91ApplicationsFolderType = FOUR_CHAR_CODE('�pps'); { The "Applications" folder, pre Mac OS 9.1 }
	kPreMacOS91InstallerLogsFolderType = FOUR_CHAR_CODE('�lgf'); { The "Installer Logs" folder, pre Mac OS 9.1 }
	kPreMacOS91AssistantsFolderType = FOUR_CHAR_CODE('�st�'); { The "Assistants" folder, pre Mac OS 9.1 }
	kPreMacOS91UtilitiesFolderType = FOUR_CHAR_CODE('�ti�'); { The "Utilities" folder, pre Mac OS 9.1 }
	kPreMacOS91AppleExtrasFolderType = FOUR_CHAR_CODE('�ex�'); { The "Apple Extras" folder, pre Mac OS 9.1 }
	kPreMacOS91MacOSReadMesFolderType = FOUR_CHAR_CODE('�or�'); { The "Mac OS ReadMes" folder, pre Mac OS 9.1 }
	kPreMacOS91InternetFolderType = FOUR_CHAR_CODE('�nt�'); { The "Internet" folder, pre Mac OS 9.1 }
	kPreMacOS91AutomountedServersFolderType = FOUR_CHAR_CODE('�rv�'); { The "Servers" folder, pre Mac OS 9.1 }
	kPreMacOS91StationeryFolderType = FOUR_CHAR_CODE('�dst'); { The "Stationery" folder, pre Mac OS 9.1 }
	kLocalesFolderType = FOUR_CHAR_CODE('�loc'); { PKE for Locales folder }
	kFindByContentPluginsFolderType = FOUR_CHAR_CODE('fbcp'); { Find By Content Plug-ins }
	kFindByContentFolderType = FOUR_CHAR_CODE('fbcf'); { Find by content folder }

{  These folder types are not supported on Mac OS X at all and should be removed from your source code.}
const
	kMagicTemporaryItemsFolderType = FOUR_CHAR_CODE('mtmp');
	kTemporaryItemsInUserDomainFolderType = FOUR_CHAR_CODE('temq');
	kCurrentUserRemoteFolderLocation = FOUR_CHAR_CODE('rusf'); { The remote folder for the currently logged on user }
	kCurrentUserRemoteFolderType = FOUR_CHAR_CODE('rusr'); { The remote folder location for the currently logged on user }

{
   These folder types are deprecated in 10.5. The location of developer tools is no longer hard coded to "/Developer/" and 
   so these folder types work only when developer tools are installed at the default location.
}
const
	kDeveloperDocsFolderType = FOUR_CHAR_CODE('ddoc'); { Deprecated in 10.5. Contains Developer Documentation files and folders}
	kDeveloperHelpFolderType = FOUR_CHAR_CODE('devh'); { Deprecated in 10.5. Contains Developer Help related files}
	kDeveloperFolderType = FOUR_CHAR_CODE('devf'); { Deprecated in 10.5. Contains MacOS X Developer Resources}
	kDeveloperApplicationsFolderType = FOUR_CHAR_CODE('dapp'); { Deprecated in 10.5. Contains Developer Applications}

{ FolderDescFlags values }
const
	kCreateFolderAtBoot = $00000002;
	kCreateFolderAtBootBit = 1;
	kFolderCreatedInvisible = $00000004;
	kFolderCreatedInvisibleBit = 2;
	kFolderCreatedNameLocked = $00000008;
	kFolderCreatedNameLockedBit = 3;
	kFolderCreatedAdminPrivs = $00000010;
	kFolderCreatedAdminPrivsBit = 4;

const
	kFolderInUserFolder = $00000020;
	kFolderInUserFolderBit = 5;
	kFolderTrackedByAlias = $00000040;
	kFolderTrackedByAliasBit = 6;
	kFolderInRemoteUserFolderIfAvailable = $00000080;
	kFolderInRemoteUserFolderIfAvailableBit = 7;
	kFolderNeverMatchedInIdentifyFolder = $00000100;
	kFolderNeverMatchedInIdentifyFolderBit = 8;
	kFolderMustStayOnSameVolume = $00000200;
	kFolderMustStayOnSameVolumeBit = 9;
	kFolderManagerFolderInMacOS9FolderIfMacOSXIsInstalledMask = $00000400;
	kFolderManagerFolderInMacOS9FolderIfMacOSXIsInstalledBit = 10;
	kFolderInLocalOrRemoteUserFolder = kFolderInUserFolder or kFolderInRemoteUserFolderIfAvailable;
	kFolderManagerNotCreatedOnRemoteVolumesBit = 11;
	kFolderManagerNotCreatedOnRemoteVolumesMask = 1 shl kFolderManagerNotCreatedOnRemoteVolumesBit;
	kFolderManagerNewlyCreatedFolderIsLocalizedBit = 12;
	kFolderManagerNewlyCreatedFolderShouldHaveDotLocalizedCreatedWithinMask = 1 shl kFolderManagerNewlyCreatedFolderIsLocalizedBit;

type
	FolderDescFlags = UInt32;
{ FolderClass values }
const
	kRelativeFolder = FOUR_CHAR_CODE('relf');
	kRedirectedRelativeFolder = FOUR_CHAR_CODE('rrel');
	kSpecialFolder = FOUR_CHAR_CODE('spcf');

type
	FolderClass = OSType;
{ special folder locations }
const
	kBlessedFolder = FOUR_CHAR_CODE('blsf');
	kRootFolder = FOUR_CHAR_CODE('rotf');

const
	kCurrentUserFolderLocation = FOUR_CHAR_CODE('cusf'); {    the magic 'Current User' folder location}


const
	kDictionariesFolderType = FOUR_CHAR_CODE('dict'); { Dictionaries folder }
	kLogsFolderType = FOUR_CHAR_CODE('logs'); { Logs folder }
	kPreferencePanesFolderType = FOUR_CHAR_CODE('ppan'); { PreferencePanes folder, in .../Library/ }


const
	kWidgetsFolderType = FOUR_CHAR_CODE('wdgt'); { Dashboard Widgets folder, in system, local, and user domains  }
	kScreenSaversFolderType = FOUR_CHAR_CODE('scrn'); { Screen Savers folder, in system, local, and user domains }

type
	FolderType = OSType;
	FolderLocation = OSType;

type
	FolderDesc = record
		descSize: Size;
		foldType: FolderType;
		flags: FolderDescFlags;
		foldClass: FolderClass;
		foldLocation: FolderType;
		badgeSignature: OSType;
		badgeType: OSType;
		reserved: UInt32;
		name: StrFileName;                   { Str63 on MacOS}
	end;
	FolderDescPtr = ^FolderDesc;

type
	RoutingFlags = UInt32;
	FolderRouting = record
		descSize: Size;
		fileType: OSType;
		routeFromFolder: FolderType;
		routeToFolder: FolderType;
		flags: RoutingFlags;
	end;
	FolderRoutingPtr = ^FolderRouting;


{
 *  AddFolderDescriptor()
 *  
 *  Summary:
 *    Copies the supplied information into a new folder descriptor
 *    entry in the system folder list. @discussion The
 *    AddFolderDescriptor function copies the supplied information into
 *    a new descriptor entry in the system folder list. You need to
 *    provide folder descriptors for each folder you wish the Folder
 *    Manager to be able to find via the function FindFolder. For
 *    example, a child folder located in a parent folder needs to have
 *    a descriptor created both for it and its parent folder, so that
 *    the child can be found. This function is supported under Mac OS 8
 *    and later. 
 *    On Mac OS X, folder descriptors added in one process are not
 *    visible in other processes.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    foldType:
 *      Pass a constant identifying the type of the folder you wish the
 *      Folder Manager to be able to find. See �Folder Type Constants�.
 *    
 *    flags:
 *      Set these flags to indicate whether a folder is created during
 *      startup, if the folder name is locked, and if the folder is
 *      created invisible; see �Folder Descriptor Flags�.
 *    
 *    foldClass:
 *      Pass the class of the folder which you wish the Folder Manager
 *      to be able to find. The folder class determines how the
 *      foldLocation parameter is interpreted. See "Folder Descriptor
 *      Classes" for a discussion of relative and special folder
 *      classes.
 *    
 *    foldLocation:
 *      For a relative folder, specify the folder type of the parent
 *      folder of the target. For a special folder, specify the
 *      location of the folder; see �Folder Descriptor Locations�.
 *    
 *    badgeSignature:
 *      Reserved. Pass 0.
 *    
 *    badgeType:
 *      Reserved. Pass 0.
 *    
 *    name:
 *      A string specifying the name of the desired folder. For
 *      relative folders, this is the exact name of the desired folder.
 *      For special folders, the actual target folder may have a
 *      different name than the name specified in the folder
 *      descriptor. For example, the System Folder is often given a
 *      different name, but it can still be located with FindFolder.
 *    
 *    replaceFlag:
 *      Pass a Boolean value indicating whether you wish to replace a
 *      folder descriptor that already exists for the specified folder
 *      type. If true , it replaces the folder descriptor for the
 *      specified folder type. If false , it does not replace the
 *      folder descriptor for the specified folder type.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function AddFolderDescriptor( foldType: FolderType; flags: FolderDescFlags; foldClass: FolderClass; foldLocation: FolderLocation; badgeSignature: OSType; badgeType: OSType; const var name: StrFileName; replaceFlag: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  GetFolderTypes()
 *  
 *  Summary:
 *    Obtains the folder types contained in the global descriptor list.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    requestedTypeCount:
 *      Pass the number of FolderType values that can fit in the buffer
 *      pointed to by the theTypes parameter; see �Folder Type
 *      Constants�.
 *    
 *    totalTypeCount:
 *      Pass a pointer to an unsigned 32-bit integer value. On return,
 *      the value is set to the total number of FolderType values in
 *      the list. The totalTypeCount parameter may produce a value that
 *      is larger or smaller than that of the requestedTypeCount
 *      parameter. If totalTypeCount is equal to or smaller than the
 *      value passed in for requestedTypeCount and the value produced
 *      by the theTypes parameter is non-null, then all folder types
 *      were returned to the caller.
 *    
 *    theTypes:
 *      Pass a pointer to an array of FolderType values; see "Folder
 *      Type Constants". On return, the array contains the folder types
 *      for the installed descriptors. You can step through the array
 *      and call GetFolderDescriptor for each folder type. Pass null if
 *      you only want to know the number of descriptors installed in
 *      the system�s global list, rather than the actual folder types
 *      of those descriptors.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function GetFolderTypes( requestedTypeCount: UInt32; var totalTypeCount: UInt32; var theTypes: FolderType ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  RemoveFolderDescriptor()
 *  
 *  Summary:
 *    Deletes the specified folder descriptor entry from the system
 *    folder list.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    foldType:
 *      Pass a constant identifying the type of the folder for which
 *      you wish to remove a descriptor.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function RemoveFolderDescriptor( foldType: FolderType ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  GetFolderNameUnicode()
 *  
 *  Summary:
 *    Obtains the name of the specified folder.
 *  
 *  Discussion:
 *    The GetFolderName function obtains the name of the folder in the
 *    folder descriptor, not the name of the folder on the disk. The
 *    names may differ for a few special folders such as the System
 *    Folder. For relative folders, however, the actual name is always
 *    returned. You typically do not need to call this function.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    vRefNum:
 *      Pass the volume reference number (or the constant kOnSystemDisk
 *      for the startup disk) of the volume containing the folder for
 *      which you wish the name to be identified.
 *    
 *    foldType:
 *      Pass a constant identifying the type of the folder for which
 *      you wish the name to be identified. See "Folder Type Constants".
 *    
 *    foundVRefNum:
 *      On return, a pointer to the volume reference number for the
 *      volume containing the folder specified in the foldType
 *      parameter.
 *    
 *    name:
 *      A pointer to an HFSUniStr255 which will contain the unicode
 *      name on return.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function GetFolderNameUnicode( vRefNum: FSVolumeRefNum; foldType: OSType; var foundVRefNum: FSVolumeRefNum; var name: HFSUniStr255 ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_5, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  InvalidateFolderDescriptorCache()
 *  
 *  Summary:
 *    Invalidates any prior FindFolder results for the specified folder.
 *  
 *  Discussion:
 *    The InvalidateFolderDescriptorCache function searches to see if
 *    there is currently a cache of results from FindFolder calls on
 *    the specified folder. If so, it invalidates the cache from the
 *    previous calls to the FindFolder function in order to force the
 *    Folder Manager to reexamine the disk when FindFolder is called
 *    again on the specified directory ID or volume reference number.
 *    
 *    
 *    You should not normally need to call
 *    InvalidateFolderDescriptorCache.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    vRefNum:
 *      Pass the volume reference number (or the constant kOnSystemDisk
 *      for the startup disk) of the volume containing the folder for
 *      which you wish the descriptor cache to be invalidated. Pass 0
 *      to completely invalidate all folder cache information.
 *    
 *    dirID:
 *      Pass the directory ID number for the folder for which you wish
 *      the descriptor cache to be invalidated. Pass 0 to invalidate
 *      the cache for all folders on the specified disk.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function InvalidateFolderDescriptorCache( vRefNum: FSVolumeRefNum; dirID: SInt32 ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  IdentifyFolder()
 *  
 *  Summary:
 *    Obtains the folder type for the specified folder.
 *  
 *  Discussion:
 *    The folder type is identified for the folder specified by the
 *    vRefNum and dirID parameters, if such a folder exists. Note that
 *    IdentifyFolder may take several seconds to complete. Note also
 *    that if there are multiple folder descriptors that map to an
 *    individual folder, IdentifyFolder returns the folder type of only
 *    the first matching descriptor that it finds.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    vRefNum:
 *      Pass the volume reference number (or the constant kOnSystemDisk
 *      for the startup disk) of the volume containing the folder whose
 *      type you wish to identify.
 *    
 *    dirID:
 *      Pass the directory ID number for the folder whose type you wish
 *      to identify.
 *    
 *    foldType:
 *      Pass a pointer to a value of type FolderType. On return, the
 *      value is set to the folder type of the folder with the
 *      specified vRefNum and dirID parameters; see "Folder Type
 *      Constants" for descriptions of possible values.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function IdentifyFolder( vRefNum: FSVolumeRefNum; dirID: SInt32; var foldType: FolderType ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSDetermineIfRefIsEnclosedByFolder()
 *  
 *  Summary:
 *    Determine whether the given FSRef is enclosed inside the given
 *    special folder type for the given domain.
 *  
 *  Discussion:
 *    This is a fairly fast call which can determine whether a given
 *    FSRef on disk is 'inside' the given special folder type for the
 *    given domain.  This call will be more efficient than the
 *    equivalent client code which walked up the file list, checking
 *    each parent with IdentifyFolder() to see if it matches. One use
 *    for this call is to determine if a given file or folder is inside
 *    the trash on a volume, with something like
 *    
 *    err = FSDetermineIfRefIsEnclosedByFolder( kOnAppropriateDisk,
 *    kTrashFolderType, & ref, & result );
 *    if ( err == noErr && result ) (
 *    //  FSRef is inside trash on the volume.<br> )
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    domainOrVRefNum:
 *      The domain or vRefNum to check.  You can also pass
 *      kOnAppropriateDisk to check whatever vRefNum is appropriate for
 *      the given FSRef, or the value 0 to check all vRefNums and
 *      domains.
 *    
 *    folderType:
 *      The folder type to check
 *    
 *    inRef:
 *      The FSRef to look for.
 *    
 *    outResult:
 *      If non-NULL, this will be filled in with true if the given
 *      FSRef is enclosed inside the special folder type for the given
 *      domain, or false otherwise.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function FSDetermineIfRefIsEnclosedByFolder( domainOrVRefNum: FSVolumeRefNum; folderType: OSType; const var inRef: FSRef; var outResult: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  DetermineIfPathIsEnclosedByFolder()
 *  
 *  Summary:
 *    Determine whether a file path is enclosed inside the given
 *    special folder type for the given domain.
 *  
 *  Discussion:
 *    This is a fairly fast call which can determine whether a given
 *    path on disk is 'inside' the given special folder type for the
 *    given domain.  This call will be more efficient than the
 *    equivalent client code which walked up the file list, checking
 *    each parent with IdentifyFolder() to see if it matches. One use
 *    for this call is to determine if a given file or folder is inside
 *    the trash on a volume, with something like
 *    
 *    err = DetermineIfPathIsEnclosedByFolder( kOnAppropriateDisk,
 *    kTrashFolderType, path, false, & result );
 *    if ( err == noErr && result ) (
 *    //  path is inside trash on the volume.<br> )
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    domainOrVRefNum:
 *      The domain or vRefNum to check.  You can also pass
 *      kOnAppropriateDisk to check whatever vRefNum is appropriate for
 *      the given path, or the value 0 to check all vRefNums and
 *      domains.
 *    
 *    folderType:
 *      The folder type to check
 *    
 *    utf8Path:
 *      A UTF-8 encoded path name for the file.
 *    
 *    pathIsRealPath:
 *      Pass true if utf8Path is guaranteed to be a real pathname, with
 *      no symlinks or relative pathname items. Pass false if the
 *      utf8Path may contain relative pathnames, or symlinks, or
 *      aliases, etc.
 *    
 *    outResult:
 *      If non-NULL, this will be filled in with true if the given path
 *      is enclosed inside the special folder type for the given
 *      domain, or false otherwise.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function DetermineIfPathIsEnclosedByFolder( domainOrVRefNum: FSVolumeRefNum; folderType: OSType; utf8Path: ConstCStringPtr; pathIsRealPath: Boolean; var outResult: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{$ifc not TARGET_CPU_64}
{
 *  FindFolderExtended()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FindFolder instead wherever possible.
 *  
 *  Summary:
 *    Obtains location information for system-related directories.
 *  
 *  Discussion:
 *    For the folder type on the particular volume (specified,
 *    respectively, in the folderType and vRefNum parameters), the
 *    FindFolder function returns the directory's volume reference
 *    number in the foundVRefNum parameter and its directory ID in the
 *    foundDirID parameter.
 *    
 *    The specified folder used for a given volume might be located on
 *    a different volume in future versions of system software;
 *    therefore, do not assume the volume that you specify in vRefNum
 *    and the volume returned in foundVRefNum will be the same.
 *     
 *    Specify a volume reference number (or the constant kOnSystemDisk
 *    for the startup disk) or one of the domain constants ( on Mac OS
 *    X ) in the vRefNum parameter.
 *    
 *    Specify a four-character folder type--or the constant that
 *    represents it--in the folderType parameter.
 *    
 *    Use the constant kCreateFolder in the createFolder parameter to
 *    tell FindFolder to create a directory if it does not already
 *    exist; otherwise, use the constant kDontCreateFolder. Directories
 *    inside the System Folder are created only if the System Folder
 *    directory exists. The FindFolder function will not create a
 *    System Folder directory even if you specify the kCreateFolder
 *    constant in the createFolder parameter.
 *    
 *    The FindFolder function returns a nonzero result code if the
 *    folder isn't found, and it can also return other file system
 *    errors reported by the File Manager or Memory Manager.
 *     FindFolderExtended() is equivalent to FindFolder() on Mac OS X.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    vRefNum:
 *      The volume reference number (or the constant kOnSystemDisk for
 *      the startup disk) or one of the domain constants ( like
 *      kUserDomain ) of the volume or domain in which you want to
 *      locate a directory.
 *    
 *    folderType:
 *      A four-character folder type, or a constant that represents the
 *      type, for the directory you want to find.
 *    
 *    createFolder:
 *      Pass the constant kCreateFolder in this parameter to create a
 *      directory if it does not already exist; otherwise, pass the
 *      constant kDontCreateFolder.
 *    
 *    foundVRefNum:
 *      The volume reference number, returned by FindFolder , for the
 *      volume containing the directory you specify in the folderType
 *      parameter.
 *    
 *    flags:
 *      The flags passed in which control extended behaviour
 *    
 *    data:
 *      Unique data which is interpreted differently depending on the
 *      passed in flags.
 *    
 *    foundDirID:
 *      The directory ID number, returned by FindFolder , for the
 *      directory you specify in the folderType parameter.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.0 and later
 }
function FindFolderExtended( vRefNum: FSVolumeRefNum; folderType: OSType; createFolder: Boolean; flags: UInt32; data: univ Ptr; var foundVRefNum: FSVolumeRefNum; var foundDirID: SInt32 ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);


{
 *  FSFindFolderExtended()   *** DEPRECATED ***
 *  
 *  Summary:
 *    FSFindFolderExtended returns an FSRef for certain system-related
 *    directories.
 *  
 *  Discussion:
 *    For the folder type on the particular volume (specified,
 *    respectively, in the folderType and vRefNum parameters), the
 *    FindFolder function returns the FSRef of that directory. 
 *     
 *    The specified folder used for a given volume might be located on
 *    a different volume in future versions of system software;
 *    therefore, do not assume the volume that you specify in vRefNum
 *    and the volume returned in the FSRef will be the same.
 *    
 *    Specify a volume reference number (or the constant kOnSystemDisk
 *    for the startup disk) or one of the domain constants ( on Mac OS
 *    X ) in the vRefNum parameter.
 *    
 *    Specify a four-character folder type--or the constant that
 *    represents it--in the folderType parameter.
 *    
 *    Use the constant kCreateFolder in the createFolder parameter to
 *    tell FindFolder to create a directory if it does not already
 *    exist; otherwise, use the constant kDontCreateFolder. Directories
 *    inside the System Folder are created only if the System Folder
 *    directory exists. The FindFolder function will not create a
 *    System Folder directory even if you specify the kCreateFolder
 *    constant in the createFolder parameter.
 *    
 *    The FindFolder function returns a nonzero result code if the
 *    folder isn't found, and it can also return other file system
 *    errors reported by the File Manager or Memory Manager.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    vRefNum:
 *      The volume reference number (or the constant kOnSystemDisk for
 *      the startup disk) or one of the domain constants ( like
 *      kUserDomain ) of the volume or domain in which you want to
 *      locate a directory.
 *    
 *    folderType:
 *      A four-character folder type, or a constant that represents the
 *      type, for the directory you want to find.
 *    
 *    createFolder:
 *      Pass the constant kCreateFolder in this parameter to create a
 *      directory if it does not already exist; otherwise, pass the
 *      constant kDontCreateFolder.
 *    
 *    flags:
 *      The flags passed in which control extended behaviour
 *    
 *    data:
 *      Unique data which is interpreted differently depending on the
 *      passed in flags.
 *    
 *    foundRef:
 *      The FSRef for the directory you specify on the volume or domain
 *      and folderType given.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSFindFolderExtended( vRefNum: FSVolumeRefNum; folderType: OSType; createFolder: Boolean; flags: UInt32; data: univ Ptr; var foundRef: FSRef ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);


{
 *  GetFolderDescriptor()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    GetFolderDescriptor is deprecated on Mac OS X.
 *  
 *  Summary:
 *    Obtains the folder descriptor information for the specified
 *    folder type from the global descriptor list.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    foldType:
 *      Pass a constant identifying the type of the folder for which
 *      you wish to get descriptor information. See "Folder Type
 *      Constants".
 *    
 *    descSize:
 *      Pass the size (in bytes) of the folder descriptor structure for
 *      which a pointer is passed in the foldDesc parameter. This value
 *      is needed in order to determine the version of the structure
 *      being used.
 *    
 *    foldDesc:
 *      Pass a pointer to a folder descriptor structure. On return, the
 *      folder descriptor structure contains information from the
 *      global descriptor list for the specified folder type.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function GetFolderDescriptor( foldType: FolderType; descSize: Size; var foldDesc: FolderDesc ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);


{
 *  GetFolderName()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use GetFolderNameUnicode.
 *  
 *  Summary:
 *    Obtains the name of the specified folder.
 *  
 *  Discussion:
 *    The GetFolderName function obtains the name of the folder in the
 *    folder descriptor, not the name of the folder on the disk. The
 *    names may differ for a few special folders such as the System
 *    Folder. For relative folders, however, the actual name is always
 *    returned. You typically do not need to call this function.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    vRefNum:
 *      Pass the volume reference number (or the constant kOnSystemDisk
 *      for the startup disk) of the volume containing the folder for
 *      which you wish the name to be identified.
 *    
 *    foldType:
 *      Pass a constant identifying the type of the folder for which
 *      you wish the name to be identified. See "Folder Type Constants".
 *    
 *    foundVRefNum:
 *      On return, a pointer to the volume reference number for the
 *      volume containing the folder specified in the foldType
 *      parameter.
 *    
 *    name:
 *      On return, a string containing the title of the folder
 *      specified in the foldType and vRefNum parameters.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function GetFolderName( vRefNum: FSVolumeRefNum; foldType: OSType; var foundVRefNum: FSVolumeRefNum; var name: StrFileName ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);


{
 *  AddFolderRouting()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Folder Manager routing is deprecated on Mac OS X.  Do not rely on
 *    this feature in your application, because support for it will be
 *    removed in a future version of the OS.
 *  
 *  Summary:
 *    Adds a folder routing structure to the global routing list.
 *  
 *  Discussion:
 *    Your application can use the AddFolderRouting function to specify
 *    how the Finder routes a given file type. 
 *    Folder Routing is deprecated on Mac OS X at this time.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    fileType:
 *      Pass the OSType of the file to be routed.
 *    
 *    routeFromFolder:
 *      Pass the folder type of the "from" folder see "Folder Type
 *      Constants" for descriptions of possible values. An item dropped
 *      on the folder specified in this parameter will be routed to the
 *      folder specified in the routeToFolder parameter.
 *    
 *    routeToFolder:
 *      The folder type of the "to" folder see "Folder Type Constants"
 *      for descriptions of possible values.
 *    
 *    flags:
 *      Reserved for future use; pass 0.
 *    
 *    replaceFlag:
 *      Pass a Boolean value indicating whether you wish to replace a
 *      folder routing that already exists. If true , it replaces the
 *      folder to which the item is being routed. If false , it leaves
 *      the folder to which the item is being routed.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function AddFolderRouting( fileType: OSType; routeFromFolder: FolderType; routeToFolder: FolderType; flags: RoutingFlags; replaceFlag: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  RemoveFolderRouting()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Folder Manager routing is deprecated on Mac OS X.  Do not rely on
 *    this feature in your application, because support for it will be
 *    removed in a future version of the OS.
 *  
 *  Summary:
 *    Deletes a folder routing structure from the global routing list.
 *  
 *  Discussion:
 *    Both the file type and the folder type specified must match those
 *    of an existing folder routing structure in the global routing
 *    list for the RemoveFolderRouting function to succeed.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    fileType:
 *      Pass the file type value contained in the folder routing
 *      structure to be removed.
 *    
 *    routeFromFolder:
 *      Pass the folder type of the "from" folder see "Folder Type
 *      Constants" for descriptions of possible values.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function RemoveFolderRouting( fileType: OSType; routeFromFolder: FolderType ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  FindFolderRouting()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Folder Manager routing is deprecated on Mac OS X.  Do not rely on
 *    this feature in your application, because support for it will be
 *    removed in a future version of the OS.
 *  
 *  Summary:
 *    Finds the destination folder from a matching folder routing
 *    structure for the specified file.
 *  
 *  Discussion:
 *    Both the file type and the folder type specified must match those
 *    of a folder routing structure in the global routing list for the
 *    FindFolderRouting function to succeed.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    fileType:
 *      Pass the file type specified in the appropriate folder routing
 *      structure for the file for which you wish to find a destination
 *      folder.
 *    
 *    routeFromFolder:
 *      Pass the folder type of the "from" folder for which you wish to
 *      find a "to" folder see "Folder Type Constants" for descriptions
 *      of possible values. An item dropped on the folder specified in
 *      this parameter will be routed to the folder specified in the
 *      routeToFolder parameter.
 *    
 *    routeToFolder:
 *      A pointer to a value of type FolderType. On return, the value
 *      is set to the folder type of the destination folder.
 *    
 *    flags:
 *      Reserved; pass 0.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function FindFolderRouting( fileType: OSType; routeFromFolder: FolderType; var routeToFolder: FolderType; var flags: RoutingFlags ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  GetFolderRoutings()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Folder Manager routing is deprecated on Mac OS X.  Do not rely on
 *    this feature in your application, because support for it will be
 *    removed in a future version of the OS.
 *  
 *  Summary:
 *    Obtains folder routing information from the global routing list.
 *  
 *  Discussion:
 *    The folder routing information in the global routing list
 *    determines how the Finder routes files.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    requestedRoutingCount:
 *      An unsigned 32-bit value. Pass the number of folder routing
 *      structures that can fit in the buffer pointed to by the
 *      theRoutings parameter.
 *    
 *    totalRoutingCount:
 *      A pointer to an unsigned 32-bit value. On return, the value is
 *      set to the number of folder routing structures in the global
 *      list. If this value is less than or equal to
 *      requestedRoutingCount , all folder routing structures were
 *      returned to the caller.
 *    
 *    routingSize:
 *      Pass the size (in bytes) of the FolderRouting structure.
 *    
 *    theRoutings:
 *      Pass a pointer to an array of FolderRouting structures. On
 *      return the structure(s) contain the requested routing
 *      information. You may pass null if you do not wish this
 *      information.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in FoldersLib 1.0 and later
 }
function GetFolderRoutings( requestedRoutingCount: UInt32; var totalRoutingCount: UInt32; routingSize: Size; var theRoutings: FolderRouting ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  FSpDetermineIfSpecIsEnclosedByFolder()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSDetemineIfRefIsEnclosedByFolder
 *  
 *  Summary:
 *    Determine whether the given FSSpec is enclosed inside the given
 *    special folder type for the given domain.
 *  
 *  Discussion:
 *    This is a fairly fast call which can determine whether a given
 *    FSSpec on disk is 'inside' the given special folder type for the
 *    given domain.  This call will be more efficient than the
 *    equivalent client code which walked up the file list, checking
 *    each parent with IdentifyFolder() to see if it matches. One use
 *    for this call is to determine if a given file or folder is inside
 *    the trash on a volume, with something like
 *    
 *    err = FSpDetermineIfRefIsEnclosedByFolder( kOnAppropriateDisk,
 *    kTrashFolderType, & spec, & result );
 *    if ( err == noErr && result ) (
 *    //  FSSpec is inside trash on the volume.<br> )
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    domainOrVRefNum:
 *      The domain or vRefNum to check.  You can also pass
 *      kOnAppropriateDisk to check whatever vRefNum is appropriate for
 *      the given FSSpec, or the value 0 to check all vRefNums and
 *      domains.
 *    
 *    folderType:
 *      The folder type to check
 *    
 *    inSpec:
 *      The FSSpec to look for.
 *    
 *    outResult:
 *      If non-NULL, this will be filled in with true if the given
 *      FSSpec is enclosed inside the special folder type for the given
 *      domain, or false otherwise.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function FSpDetermineIfSpecIsEnclosedByFolder( domainOrVRefNum: FSVolumeRefNum; folderType: OSType; const var inSpec: FSSpec; var outResult: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);


{$endc} {not TARGET_CPU_64}

type
	FolderManagerNotificationProcPtr = function( message: OSType; arg: univ Ptr; userRefCon: univ Ptr ): OSStatus;
{GPC-ONLY-START}
	FolderManagerNotificationUPP = UniversalProcPtr; // should be FolderManagerNotificationProcPtr
{GPC-ONLY-ELSE}
	FolderManagerNotificationUPP = FolderManagerNotificationProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewFolderManagerNotificationUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewFolderManagerNotificationUPP( userRoutine: FolderManagerNotificationProcPtr ): FolderManagerNotificationUPP;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{
 *  DisposeFolderManagerNotificationUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeFolderManagerNotificationUPP( userUPP: FolderManagerNotificationUPP );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{
 *  InvokeFolderManagerNotificationUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeFolderManagerNotificationUPP( message: OSType; arg: univ Ptr; userRefCon: univ Ptr; userUPP: FolderManagerNotificationUPP ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{$ifc not TARGET_CPU_64}
{
 *  FolderManagerRegisterNotificationProc()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    This function is deprecated on Mac OS X.
 *  
 *  Summary:
 *    Register a function to be called at certain times
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    notificationProc:
 *    
 *    refCon:
 *    
 *    options:
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.0 and later
 }
function FolderManagerRegisterNotificationProc( notificationProc: FolderManagerNotificationUPP; refCon: univ Ptr; options: UInt32 ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);


{
 *  FolderManagerUnregisterNotificationProc()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    This function is deprecated on Mac OS X.
 *  
 *  Summary:
 *    Unregister a function to be called at certain times
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    notificationProc:
 *    
 *    refCon:
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.0 and later
 }
function FolderManagerUnregisterNotificationProc( notificationProc: FolderManagerNotificationUPP; refCon: univ Ptr ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);


{
 *  FolderManagerRegisterCallNotificationProcs()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    This function is deprecated on Mac OS X.
 *  
 *  Summary:
 *    Call the registered Folder Manager notification procs.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    message:
 *    
 *    arg:
 *    
 *    options:
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.0 and later
 }
function FolderManagerRegisterCallNotificationProcs( message: OSType; arg: univ Ptr; options: UInt32 ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}


end.
