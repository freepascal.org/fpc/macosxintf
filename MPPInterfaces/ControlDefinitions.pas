{
     File:       HIToolbox/ControlDefinitions.h
 
     Contains:   Definitions of controls provided by the Control Manager
 
     Version:    HIToolbox-624~3
 
     Copyright:  © 1999-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ControlDefinitions;
interface
uses MacTypes,TextEdit,AXUIElement,AEDataModel,CFBase,Events,QuickdrawTypes,IconsCore,CFData,CFDictionary,CFString,DateTimeUtils,Drag,TextCommon,Appearance,CarbonEvents,Controls,Lists,MacHelp,Menus,HIObject;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}


{
 *  ControlDefinitions.h
 *  
 *  Discussion:
 *    System software supplies a variety of controls for your
 *    applications to use. They are described herein.
 }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ Control Definition IDΥs                                                                           }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{ Standard System 7 procIDs}

const
	pushButProc = 0;
	checkBoxProc = 1;
	radioButProc = 2;
	scrollBarProc = 16;
	popupMenuProc = 1008;

{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ Control Part Codes                                                                }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
const
	kControlLabelPart = 1;
	kControlMenuPart = 2;
	kControlTrianglePart = 4;
	kControlEditTextPart = 5;    { Appearance 1.0 and later}
	kControlPicturePart = 6;    { Appearance 1.0 and later}
	kControlIconPart = 7;    { Appearance 1.0 and later}
	kControlClockPart = 8;    { Appearance 1.0 and later}
	kControlListBoxPart = 24;   { Appearance 1.0 and later}
	kControlListBoxDoubleClickPart = 25;  { Appearance 1.0 and later}
	kControlImageWellPart = 26;   { Appearance 1.0 and later}
	kControlRadioGroupPart = 27;   { Appearance 1.0.2 and later}
	kControlButtonPart = 10;
	kControlCheckBoxPart = 11;
	kControlRadioButtonPart = 11;
	kControlUpButtonPart = kAppearancePartUpButton;
	kControlDownButtonPart = kAppearancePartDownButton;
	kControlPageUpPart = kAppearancePartPageUpArea;
	kControlPageDownPart = kAppearancePartPageDownArea;
	kControlClockHourDayPart = 9;    { Appearance 1.1 and later}
	kControlClockMinuteMonthPart = 10;   { Appearance 1.1 and later}
	kControlClockSecondYearPart = 11;   { Appearance 1.1 and later}
	kControlClockAMPMPart = 12;   { Appearance 1.1 and later}
	kControlDataBrowserPart = 24;   { CarbonLib 1.0 and later}
	kControlDataBrowserDraggedPart = 25;   { CarbonLib 1.0 and later}

{--------------------------------------------------------------------------------------}
{  ₯ DEPRECATED                                                                        }
{  All functions below this point are either deprecated (they continue to function     }
{  but are not the most modern nor most efficient solution to a problem), or they are  }
{  completely unavailable on Mac OS X.                                                 }
{--------------------------------------------------------------------------------------}
{
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
    ₯ EDIT TEXT (CDEF 17)
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
    Use the EditUnicodeText control to replace the Edit Text control. It uses the same
    data tags as the Edit Text control.
}

{ Edit Text proc IDs }
const
	kControlEditTextProc = 272;
	kControlEditTextPasswordProc = 274;

{ proc IDs available with Appearance 1.1 or later }
const
	kControlEditTextInlineInputProc = 276; { Can't combine with the other variants}

{ Control Kind Tag }
const
	kControlKindEditText = FOUR_CHAR_CODE('etxt');

{$ifc not TARGET_CPU_64}
{
 *  CreateEditTextControl()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use CreateEditUnicodeTextControl API instead.
 *  
 *  Summary:
 *    Creates a new edit text control.
 *  
 *  Discussion:
 *    This control is a legacy control. It is deprecated in favor of
 *    the EditUnicodeText control, which handles Unicode and draws its
 *    text using antialiasing.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    window:
 *      The window in which the control should be placed. May be NULL
 *      in 10.3 and later.
 *    
 *    boundsRect:
 *      The bounds of the control, in local coordinates of the window.
 *    
 *    text:
 *      The text of the control. May be NULL.
 *    
 *    isPassword:
 *      A Boolean indicating whether the field is to be used as a
 *      password field. Passing false indicates that the field is to
 *      display entered text normally. True means that the field will
 *      be used as a password field and any text typed into the field
 *      will be displayed only as bullets.
 *    
 *    useInlineInput:
 *      A Boolean indicating whether or not the control is to accept
 *      inline input. Pass true to to accept inline input, otherwise
 *      pass false.
 *    
 *    style:
 *      The control's font style, size, color, and so on. May be NULL.
 *    
 *    outControl:
 *      On exit, contains the new control (if noErr is returned as the
 *      result code).
 *  
 *  Result:
 *    An operating system result code.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateEditTextControl( window: WindowRef; const var boundsRect: Rect; text: CFStringRef { can be NULL }; isPassword: Boolean; useInlineInput: Boolean; {const} style: ControlFontStyleRecPtr { can be NULL }; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ ControlData tags supported only by the classic EditText control}
{$endc} {not TARGET_CPU_64}

const
	kControlEditTextTEHandleTag = FOUR_CHAR_CODE('than'); { The TEHandle of the text edit record}
	kControlEditTextInlinePreUpdateProcTag = FOUR_CHAR_CODE('prup'); { TSMTEPreUpdateUPP and TSMTEPostUpdateUpp. For use with inline input variant...}
	kControlEditTextInlinePostUpdateProcTag = FOUR_CHAR_CODE('poup'); { ...The refCon parameter will contain the ControlRef.}

{
    The classic EditText control also supports these tags defined for the EditUnicodeText control:
    
        kControlEditTextLockedTag
        kControlEditTextStyleTag
        kControlEditTextFixedTextTag
        kControlEditTextTextTag
        kControlEditTextKeyFilterTag
        kControlEditTextValidationProcTag
        kControlEditTextSelectionTag
        kControlEditTextKeyScriptBehaviorTag
        kControlEditTextCFStringTag
        kControlEditTextPasswordTag
        kControlEditTextPasswordCFStringTag
}

{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ PICTURE CONTROL (CDEF 19)                                                         }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  Value parameter should contain the ID of the picture you wish to display when       }
{  creating controls of this type. If you don't want the control tracked at all, use   }
{  the 'no track' variant.                                                             }
{ Picture control proc IDs }
const
	kControlPictureProc = 304;
	kControlPictureNoTrackProc = 305;   { immediately returns kControlPicturePart}

{ Control Kind Tag }
const
	kControlKindPicture = FOUR_CHAR_CODE('pict');

{ The HIObject class ID for the HIPictureView class. }
const kHIPictureViewClassID = CFSTR( 'com.apple.HIPictureView' );
{$ifc not TARGET_CPU_64}
{
 *  CreatePictureControl()
 *  
 *  Summary:
 *    Creates a picture control.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    window:
 *      The window that should contain the control. May be NULL on 10.3
 *      and later.
 *    
 *    boundsRect:
 *      The bounding box of the control.
 *    
 *    content:
 *      The descriptor for the picture you want the control to display.
 *    
 *    dontTrack:
 *      A Boolean value indicating whether the control should hilite
 *      when it is clicked on. False means hilite and track the mouse.
 *    
 *    outControl:
 *      On exit, contains the new control.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreatePictureControl( window: WindowRef { can be NULL }; const var boundsRect: Rect; const var content: ControlButtonContentInfo; dontTrack: Boolean; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Tagged data supported by picture controls }
{$endc} {not TARGET_CPU_64}

const
	kControlPictureHandleTag = FOUR_CHAR_CODE('pich'); { PicHandle}

{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ LIST BOX (CDEF 22)                                                                }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  Lists use an auxiliary resource to define their format. The resource type used is   }
{  'ldes' and a definition for it can be found in Appearance.r. The resource ID for    }
{  the ldes is passed in the 'value' parameter when creating the control. You may pass }
{  zero in value. This tells the List Box control to not use a resource. The list will }
{  be created with default values, and will use the standard LDEF (0). You can change  }
{  the list by getting the list handle. You can set the LDEF to use by using the tag   }
{  below (kControlListBoxLDEFTag)                                                      }
{ List Box proc IDs }
const
	kControlListBoxProc = 352;
	kControlListBoxAutoSizeProc = 353;

{ Control Kind Tag }
const
	kControlKindListBox = FOUR_CHAR_CODE('lbox');

{$ifc not TARGET_CPU_64}
{
 *  CreateListBoxControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateListBoxControl( window: WindowRef; const var boundsRect: Rect; autoSize: Boolean; numRows: SInt16; numColumns: SInt16; horizScroll: Boolean; vertScroll: Boolean; cellHeight: SInt16; cellWidth: SInt16; hasGrowSpace: Boolean; const var listDef: ListDefSpec; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Tagged data supported by list box }
{$endc} {not TARGET_CPU_64}

const
	kControlListBoxListHandleTag = FOUR_CHAR_CODE('lhan'); { ListHandle}
	kControlListBoxKeyFilterTag = kControlKeyFilterTag; { ControlKeyFilterUPP}
	kControlListBoxFontStyleTag = kControlFontStyleTag; { ControlFontStyleRec}

{ New tags in 1.0.1 or later }
const
	kControlListBoxDoubleClickTag = FOUR_CHAR_CODE('dblc'); { Boolean. Was last click a double-click?}
	kControlListBoxLDEFTag = FOUR_CHAR_CODE('ldef'); { SInt16. ID of LDEF to use.}

{ Resource Types }
const
	kControlListDescResType = FOUR_CHAR_CODE('ldes'); { used for list box control (Appearance 1.0 and later)}

{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ SCROLL TEXT BOX (CDEF 27)                                                         }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  This control implements a scrolling box of (non-editable) text. This is useful for  }
{  credits in about boxes, etc.                                                        }
{  The standard version of this control has a scroll bar, but the autoscrolling        }
{  variant does not. The autoscrolling variant needs two pieces of information to      }
{  work: delay (in ticks) before the scrolling starts, and time (in ticks) between     }
{  scrolls. It will scroll one pixel at a time, unless changed via SetControlData.     }
{  Parameter                   What Goes Here                                          }
{  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ         ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ    }
{  Value                       Resource ID of 'TEXT'/'styl' content.                   }
{  Min                         Scroll start delay (in ticks)                       .   }
{  Max                         Delay (in ticks) between scrolls.                       }
{  NOTE: This control is only available with Appearance 1.1.                           }
{ Scroll Text Box Proc IDs }
const
	kControlScrollTextBoxProc = 432;
	kControlScrollTextBoxAutoScrollProc = 433;

{ Control Kind Tag }
const
	kControlKindScrollingTextBox = FOUR_CHAR_CODE('stbx');

{$ifc not TARGET_CPU_64}
{
 *  CreateScrollingTextBoxControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateScrollingTextBoxControl( window: WindowRef; const var boundsRect: Rect; contentResID: SInt16; autoScroll: Boolean; delayBeforeAutoScroll: UInt32; delayBetweenAutoScroll: UInt32; autoScrollAmount: UInt16; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Tagged data supported by Scroll Text Box }
{$endc} {not TARGET_CPU_64}

const
	kControlScrollTextBoxDelayBeforeAutoScrollTag = FOUR_CHAR_CODE('stdl'); { UInt32 (ticks until autoscrolling starts)}
	kControlScrollTextBoxDelayBetweenAutoScrollTag = FOUR_CHAR_CODE('scdl'); { UInt32 (ticks between scrolls)}
	kControlScrollTextBoxAutoScrollAmountTag = FOUR_CHAR_CODE('samt'); { UInt16 (pixels per scroll) -- defaults to 1}
	kControlScrollTextBoxContentsTag = FOUR_CHAR_CODE('tres'); { SInt16 (resource ID of 'TEXT'/'styl') -- write only!}
	kControlScrollTextBoxAnimatingTag = FOUR_CHAR_CODE('anim'); { Boolean (whether the text box should auto-scroll)}

{$ifc OLDROUTINENAMES}
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ OLDROUTINENAMES                                                                   }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
const
	inLabel = kControlLabelPart;
	inMenu = kControlMenuPart;
	inTriangle = kControlTrianglePart;
	inButton = kControlButtonPart;
	inCheckBox = kControlCheckBoxPart;
	inUpButton = kControlUpButtonPart;
	inDownButton = kControlDownButtonPart;
	inPageUp = kControlPageUpPart;
	inPageDown = kControlPageDownPart;

const
	kInLabelControlPart = kControlLabelPart;
	kInMenuControlPart = kControlMenuPart;
	kInTriangleControlPart = kControlTrianglePart;
	kInButtonControlPart = kControlButtonPart;
	kInCheckBoxControlPart = kControlCheckBoxPart;
	kInUpButtonControlPart = kControlUpButtonPart;
	kInDownButtonControlPart = kControlDownButtonPart;
	kInPageUpControlPart = kControlPageUpPart;
	kInPageDownControlPart = kControlPageDownPart;


{$endc}  {OLDROUTINENAMES}


{$endc} {TARGET_OS_MAC}

end.
