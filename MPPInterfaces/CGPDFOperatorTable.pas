{ CoreGraphics - CGPDFOperatorTable.h
 * Copyright (c) 2004-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFOperatorTable;
interface
uses MacTypes,CGBase;
{$ALIGN POWER}


// CGPDFOperatorTableRef defined in CGBase


type
	CGPDFOperatorCallback = procedure( scanner: CGPDFScannerRef; info: univ Ptr );

{ Return an empty operator table. }

function CGPDFOperatorTableCreate: CGPDFOperatorTableRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Increment the retain count of `table'. }

function CGPDFOperatorTableRetain( table: CGPDFOperatorTableRef ): CGPDFOperatorTableRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Decrement the retain count of `table'. }

procedure CGPDFOperatorTableRelease( table: CGPDFOperatorTableRef );
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Set the callback for the operator named `name' to `callback' }

procedure CGPDFOperatorTableSetCallback( table: CGPDFOperatorTableRef; name: ConstCStringPtr; callback: CGPDFOperatorCallback );
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);


end.
