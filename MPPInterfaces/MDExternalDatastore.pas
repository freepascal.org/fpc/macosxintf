{
 *  MDExternalDatastore.h
 *  MDDatastoreHarness
 *
 *  Created by Jonah Petri on 1/6/06.
 *  Copyright 2006 Apple. All rights reserved.
 *
 }
 
 { Pascal Translation: Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }
 
unit MDExternalDatastore;
interface
uses MacTypes,CFBase,CFArray,CFUUID,CFPlugIn,CFPlugInCOM;
{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

{FPC-ONLY-START}
implemented function kMDExternalDatastoreTypeID : CFUUIDRef; inline;
implemented function kMDExternalDatastoreStoreInterfaceID : CFUUIDRef; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec kMDExternalDatastoreTypeID      CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$49,$68,$94,$B1,$00,$30,$47,$E0,$96,$11,$F2,$48,$FB,$E0,$B8,$CA)}
{$mwgpcdefinec kMDExternalDatastoreStoreInterfaceID CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$DA,$62,$12,$99,$ED,$BE,$4A,$63,$92,$39,$CB,$24,$13,$73,$E2,$07)}
{FPC-ONLY-FINISH}

{ some opaque types }
type
	MDExternalDatastoreQueryRef = UnivPtr;
	MDExternalDatastoreRef = UnivPtr;
	MDResponseChannelRef = UnivPtr;

{ an enumeration of oids, for space efficiency }
type
	MDOIDEnumerationRef = UnivPtr;
function MDOIDEnumerationNextOID( oidEnum: MDOIDEnumerationRef ): UInt64;
function MDOIDEnumerationHasMoreOIDs( oidEnum: MDOIDEnumerationRef ): CBool;

{ send an object in response to a query.  Returns true if the query should keep going, false if it has been cancelled, and should stop. }
function MDResponseChannelSendObject( channel: MDResponseChannelRef; var obj: CFTypeRef ): CBool;

{ send an OID in response to a URL=>OID conversion.  Use MDResponseChannelSendOID, it will return true if the store should keep processing the URLs, false otherwise  }
function MDResponseChannelSendOID( channel: MDResponseChannelRef; oid: UInt64 ): CBool;

type
	MDExternalDatastoreStoreInterfaceStruct = record
		IUNKNOWN_C_GUTS	: IUnknownVTbl;
		{ Initialize a store structure for a token, and return it.  In the static store case, token will be NULL.}
		MDExternalDatastoreCreate : function(token: CFTypeRef): MDExternalDatastoreRef;
		
		{ Dealloc a given store }
		MDExternalDatastoreDealloc : procedure(store: MDExternalDatastoreRef);
		
    { execute the query, and send the attribute dicitonaries of each URL through the channel 
        - query will be explained more later, but will include the query tree, and maybe grouping and sorting params, maybe original text? 
        - channel expects CFDictionaries, with at a minimum a kMDItemURL set.  
        - If the store wants to talk in terms of OIDs, it should send along an OID in the dictionary too.
        }
        MDExternalDatastoreExecuteQuery: function(store: MDExternalDatastoreRef; channel: MDResponseChannelRef; query: MDExternalDatastoreQueryRef): Boolean;

	{ send the attribute dictionaries of each URL through the channel
        - urls is a CFArrayRef of CFURLRefs
        - attributes is a CFArrayRef of CFStringRefs 
       }
       MDExternalDatastoreFetchAttributesForURLs: function(store: MDExternalDatastoreRef; channel: MDResponseChannelRef; attributes: CFArrayRef; URLs: CFArrayRef): Boolean;
       
    { OID<=>URL converters.  If the external datastore has reasonably persistant object IDs, it should provide them as part of the query answers.  It will then be expected to implement the following functions concerning OIDs.  
        
        Implementing these functions can result in substantial gains in performance for your plugin. }
       
       {  send an OID in response to a URL=>OID conversion.  Use MDResponseChannelSendOID, it will return true if the store should keep processing the URLs, false otherwise  }
       MDExternalDatastoreFetchOIDsForURLs: function(store: MDExternalDatastoreRef; channel: MDResponseChannelRef; URLs: CFArrayRef): Boolean;
       
       { send attribute dictionaries for the given OIDs.  Use MDResponseChannelSendObject on each dictionary, it will return true if the store should keep processing the OIDs, false otherwise  }
       MDExternalDatastoreFetchAttributesForOIDs: function(store: MDExternalDatastoreRef; channel: MDResponseChannelRef; attributes: CFArrayRef; OIDs: MDOIDEnumerationRef): Boolean;
       
       { send URLs in response to OID=>URL conversions.  Use MDResponseChannelSendObject, it will return true if the store should keep processing the OIDs, false otherwise  }
       MDExternalDatastoreFetchURLsForOIDs: function(store: MDExternalDatastoreRef; channel: MDResponseChannelRef; OIDs: MDOIDEnumerationRef): Boolean;
	end;

	MDExternalDatastoreStoreInterfaceStructPtr = ^MDExternalDatastoreStoreInterfaceStruct;
{$endc} {TARGET_OS_MAC}
	
implementation

{$ifc TARGET_OS_MAC}

{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

{FPC-ONLY-START}
function kMDExternalDatastoreTypeID : CFUUIDRef; inline;
begin
	kMDExternalDatastoreTypeID := CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$49,$68,$94,$B1,$00,$30,$47,$E0,$96,$11,$F2,$48,$FB,$E0,$B8,$CA)
end;

function kMDExternalDatastoreStoreInterfaceID : CFUUIDRef; inline;
begin
	kMDExternalDatastoreStoreInterfaceID := CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$DA,$62,$12,$99,$ED,$BE,$4A,$63,$92,$39,$CB,$24,$13,$73,$E2,$07)
end;
{FPC-ONLY-FINISH}

{$endc} {TARGET_OS_MAC}

end.
