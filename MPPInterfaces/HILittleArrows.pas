{
     File:       HIToolbox/HILittleArrows.h
 
     Contains:   Definition of the little arrows view provided by HIToolbox.
 
     Version:    HIToolbox-624~3
 
     Copyright:  © 2006-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HILittleArrows;
interface
uses MacTypes,Appearance,CarbonEvents,Controls,QuickdrawTypes,HIObject;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
 *  HILittleArrows.h
 *  
 *  Discussion:
 *    API definitions for the little arrows view.
 }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ LITTLE ARROWS (CDEF 6)                                                            }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  This control implements the little up and down arrows that are used in clock views. }
{ Little Arrows proc IDs }
const
	kControlLittleArrowsProc = 96;

{ Control Kind Tag }
const
	kControlKindLittleArrows = FOUR_CHAR_CODE('larr');


{
 *  Summary:
 *    Tags available with Mac OS X 10.3 or later
 }
const
{
   * Passed data is an SInt32.  Gets or sets the increment value of the
   * control. Note that the little arrows control does not actually
   * ever change its own value in response to clicks, and so it does
   * not ever use the increment value; you must use an action proc to
   * handle clicks on the control's arrows and change the control value
   * yourself. However, you can store the increment value in the
   * control's data and read it later with GetControlData.
   }
	kControlLittleArrowsIncrementValueTag = FOUR_CHAR_CODE('incr');

{ The HIObject class ID for the HILittleArrows class. }
const kHILittleArrowsClassID = CFSTR( 'com.apple.HILittleArrows' );
{ Creation API: Carbon only }
{$ifc not TARGET_CPU_64}
{
 *  CreateLittleArrowsControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateLittleArrowsControl( window: WindowRef; const var boundsRect: Rect; value: SInt32; minimum: SInt32; maximum: SInt32; increment: SInt32; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
