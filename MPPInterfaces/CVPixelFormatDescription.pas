{
 *  CVPixelFormatDescription.h
 *  CoreVideo
 *
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2008 }
{  Pascal Translation Update:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }
 
unit CVPixelFormatDescription;
interface
uses MacTypes, CFArray, CFBase, CFDictionary, CVPixelBuffer;

{$ALIGN POWER}


{ This document is influenced by Ice Floe #19: http://developer.apple.com/quicktime/icefloe/dispatch019.html }

{ The canonical name for the format.  This should bethe same as the codec name you'd use in QT }
const kCVPixelFormatName: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ QuickTime/QuickDraw Pixel Format Type constant (OSType) }
const kCVPixelFormatConstant: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ This is the codec type constant, i.e. '2vuy' or k422YpCbCr8CodecType }
const kCVPixelFormatCodecType: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ This is the equivalent Microsoft FourCC code for this pixel format }
const kCVPixelFormatFourCC: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ kCFBooleanTrue indicates that the format contains alpha and some images may be considered transparent;
   kCFBooleanFalse indicates that there is no alpha and images are always opaque. }
const kCVPixelFormatContainsAlpha: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_3);

{ All buffers have one or more image planes.  Each plane may contain a single or an interleaved set of components }   
{ For simplicity sake, pixel formats that are not planar may place the required format keys at the top
   level dictionary. }
const kCVPixelFormatPlanes: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ The following keys describe the requirements/layout of a a single image plane. }

{ Used to assist with allocating memory for pixel formats that don't have an integer value for
   bytes per pixel }
{ Block width is essentially the width in pixels of the smallest "byte addressable" group of pixels }
{ This works in close conjunction with BitsPerBlock }
{ Examples:
   8-bit luminance only, BlockWidth would be 1, BitsPerBlock would be 8
   16-bit 1555 RGB, BlockWidth would be 1, BitsPerBlock would be 16
   32-bit 8888 ARGB, BlockWidth would be 1, BitsPerBlock would be 32
   2vuy (CbYCrY), BlockWidth would be 2, BitsPerBlock would be 32
   1-bit bitmap, BlockWidth would be 8, BitsPerBlock would be 8
   v210, BlockWidth would be 6, BitsPerBlock would be 128 }
{ Values assumed to 1 be one if not present }
const kCVPixelFormatBlockWidth: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVPixelFormatBlockHeight: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ This value must be present.  For simple pixel formats this will be equivalent to the traditional 
   bitsPerPixel value. }
const kCVPixelFormatBitsPerBlock: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ Used to state requirements on block multiples.  v210 would be '8' here for the horizontal case, 
   to match the standard v210 row alignment value of 48.
   These may be assumed as 1 if not present. }
const kCVPixelFormatBlockHorizontalAlignment: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVPixelFormatBlockVerticalAlignment: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ CFData containing the bit pattern for a block of black pixels.  If absent, black is assumed to be all zeros.
   If present, this should be bitsPerPixel bits long -- if bitsPerPixel is less than a byte, repeat the bit pattern 
   for the full byte.  }
const kCVPixelFormatBlackBlock: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0);

{ Subsampling information for this plane.  Assumed to be '1' if not present. }
const kCVPixelFormatHorizontalSubsampling: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVPixelFormatVerticalSubsampling: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ If present, these two keys describe the OpenGL format and type enums you would use to describe this
   image plane to OpenGL }
const kCVPixelFormatOpenGLFormat: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVPixelFormatOpenGLType: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVPixelFormatOpenGLInternalFormat: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ CGBitmapInfo value, if required }
const kCVPixelFormatCGBitmapInfo: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ Pixel format compatibility flags }
const kCVPixelFormatQDCompatibility: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVPixelFormatCGBitmapContextCompatibility: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVPixelFormatCGImageCompatibility: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVPixelFormatOpenGLCompatibility: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
{$ifc TARGET_OS_IPHONE}
const kCVPixelFormatOpenGLESCompatibility: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_5_0);
{$endc}

{ This callback routine implements code to handle the functionality of CVPixelBufferFillExtendedPixels.  
   For custom pixel formats where you will never need to use that call, this is not required. }
type
	CVFillExtendedPixelsCallBack = function( pixelBuffer: CVPixelBufferRef; refCon: univ Ptr ): Boolean;
	CVFillExtendedPixelsCallBackData = record
		version: CFIndex;
		fillCallBack: CVFillExtendedPixelsCallBack;
		refCon: UnivPtr;
	end;

{ The value for this key is a CFData containing a CVFillExtendedPixelsCallBackData struct }
const kCVPixelFormatFillExtendedPixelsCallback: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ Create a description of a pixel format from a provided OSType }
function CVPixelFormatDescriptionCreateWithPixelFormatType( allocator: CFAllocatorRef; pixelFormat: OSType ): CFDictionaryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ Get a CFArray of CFNumbers containing all known pixel formats as OSTypes }
function CVPixelFormatDescriptionArrayCreateWithAllPixelFormatTypes( allocator: CFAllocatorRef ): CFArrayRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{ Register a new pixel format with CoreVideo }
procedure CVPixelFormatDescriptionRegisterDescriptionWithPixelFormatType( description: CFDictionaryRef; pixelFormat: OSType );
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);



end.
