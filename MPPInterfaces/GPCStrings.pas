{
	GPC support for handling UCSD-Pascal strings.
}

unit GPCStrings;
interface
uses MacTypes;
{$ALIGN MAC68K}

const
  kEmptyStr15 : Str15 = ( sLength: 0, sChars: '');
  kEmptyStr27 : Str27 = ( sLength: 0, sChars: '');
  kEmptyStr31 : Str31 = ( sLength: 0, sChars: '');
  kEmptyStr32 : Str32 = ( sLength: 0, sChars: '');
  kEmptyStr63 : Str63 = ( sLength: 0, sChars: '');
  kEmptyStr255 : Str255 = ( sLength: 0, sChars: '');

type
	String15			= String(15);
	String27			= String(27);
	String31			= String(31);
	String32			= String(32);
	String36			= String(36);
	String63			= String(63);
	String255			= String(255);

  function Str15Length( const s: Str15 ) = theResult : Integer;
  function Str27Length( const s: Str27 ) = theResult : Integer;
  function Str31Length( const s: Str31 ) = theResult : Integer;
  function Str32Length( const s: Str32 ) = theResult : Integer;
  function Str63Length( const s: Str63 ) = theResult : Integer;
  function Str255Length( const s: Str255 ) = theResult : Integer;

  procedure SetStr15Length( var s: Str15; len: Integer );
  procedure SetStr27Length( var s: Str27; len: Integer );
  procedure SetStr31Length( var s: Str31; len: Integer );
  procedure SetStr32Length( var s: Str32; len: Integer );
  procedure SetStr63Length( var s: Str63; len: Integer );
  procedure SetStr255Length( var s: Str255; len: Integer );

  operator + ( ch: char; const s2: Str15 ) = theResult : Str255;
  operator + ( ch: char; const s2: Str27 ) = theResult : Str255;
  operator + ( ch: char; const s2: Str31 ) = theResult : Str255;
  operator + ( ch: char; const s2: Str32 ) = theResult : Str255;
  operator + ( ch: char; const s2: Str63 ) = theResult : Str255;
  operator + ( ch: char; const s2: Str255 ) = theResult : Str255;
  operator + ( const s1: Str15; ch: char ) = theResult : Str255;
  operator + ( const s1: Str27; ch: char ) = theResult : Str255;
  operator + ( const s1: Str31; ch: char ) = theResult : Str255;
  operator + ( const s1: Str32; ch: char ) = theResult : Str255;
  operator + ( const s1: Str63; ch: char ) = theResult : Str255;
  operator + ( const s1: Str255; ch: char ) = theResult : Str255;
	operator + ( const t1: String; const s1: Str15) = theResult : Str255;
	operator + ( const t1: String; const s1: Str27) = theResult : Str255;
	operator + ( const t1: String; const s1: Str31) = theResult : Str255;
	operator + ( const t1: String; const s1: Str32) = theResult : Str255;
	operator + ( const t1: String; const s1: Str63) = theResult : Str255;
	operator + ( const t1: String; const s1: Str255) = theResult : Str255;
	operator + ( const s1: Str15; const t1: String) = theResult : Str255;
	operator + ( const s1: Str27; const t1: String) = theResult : Str255;
	operator + ( const s1: Str31; const t1: String) = theResult : Str255;
	operator + ( const s1: Str32; const t1: String) = theResult : Str255;
	operator + ( const s1: Str63; const t1: String) = theResult : Str255;
	operator + ( const s1: Str255; const t1: String) = theResult : Str255;
  operator + ( const s1: Str15; const s2: Str15 ) = theResult : Str255;
  operator + ( const s1: Str15; const s2: Str27 ) = theResult : Str255;
  operator + ( const s1: Str15; const s2: Str31 ) = theResult : Str255;
  operator + ( const s1: Str15; const s2: Str32 ) = theResult : Str255;
  operator + ( const s1: Str15; const s2: Str63 ) = theResult : Str255;
  operator + ( const s1: Str15; const s2: Str255 ) = theResult : Str255;
  operator + ( const s1: Str27; const s2: Str15 ) = theResult : Str255;
  operator + ( const s1: Str27; const s2: Str27 ) = theResult : Str255;
  operator + ( const s1: Str27; const s2: Str31 ) = theResult : Str255;
  operator + ( const s1: Str27; const s2: Str32 ) = theResult : Str255;
  operator + ( const s1: Str27; const s2: Str63 ) = theResult : Str255;
  operator + ( const s1: Str27; const s2: Str255 ) = theResult : Str255;
  operator + ( const s1: Str31; const s2: Str15 ) = theResult : Str255;
  operator + ( const s1: Str31; const s2: Str27 ) = theResult : Str255;
  operator + ( const s1: Str31; const s2: Str31 ) = theResult : Str255;
  operator + ( const s1: Str31; const s2: Str32 ) = theResult : Str255;
  operator + ( const s1: Str31; const s2: Str63 ) = theResult : Str255;
  operator + ( const s1: Str31; const s2: Str255 ) = theResult : Str255;
  operator + ( const s1: Str32; const s2: Str15 ) = theResult : Str255;
  operator + ( const s1: Str32; const s2: Str27 ) = theResult : Str255;
  operator + ( const s1: Str32; const s2: Str31 ) = theResult : Str255;
  operator + ( const s1: Str32; const s2: Str32 ) = theResult : Str255;
  operator + ( const s1: Str32; const s2: Str63 ) = theResult : Str255;
  operator + ( const s1: Str32; const s2: Str255 ) = theResult : Str255;
  operator + ( const s1: Str63; const s2: Str15 ) = theResult : Str255;
  operator + ( const s1: Str63; const s2: Str27 ) = theResult : Str255;
  operator + ( const s1: Str63; const s2: Str31 ) = theResult : Str255;
  operator + ( const s1: Str63; const s2: Str32 ) = theResult : Str255;
  operator + ( const s1: Str63; const s2: Str63 ) = theResult : Str255;
  operator + ( const s1: Str63; const s2: Str255 ) = theResult : Str255;
  operator + ( const s1: Str255; const s2: Str15 ) = theResult : Str255;
  operator + ( const s1: Str255; const s2: Str27 ) = theResult : Str255;
  operator + ( const s1: Str255; const s2: Str31 ) = theResult : Str255;
  operator + ( const s1: Str255; const s2: Str32 ) = theResult : Str255;
  operator + ( const s1: Str255; const s2: Str63 ) = theResult : Str255;
  operator + ( const s1: Str255; const s2: Str255 ) = theResult : Str255;

  operator = ( ch: char; const s2: Str15 ) = theResult : Boolean;
  operator = ( ch: char; const s2: Str27 ) = theResult : Boolean;
  operator = ( ch: char; const s2: Str31 ) = theResult : Boolean;
  operator = ( ch: char; const s2: Str32 ) = theResult : Boolean;
  operator = ( ch: char; const s2: Str63 ) = theResult : Boolean;
  operator = ( ch: char; const s2: Str255 ) = theResult : Boolean;
  operator = ( const s1: Str15; ch: char) = theResult : Boolean;
  operator = ( const s1: Str27; ch: char) = theResult : Boolean;
  operator = ( const s1: Str31; ch: char) = theResult : Boolean;
  operator = ( const s1: Str32; ch: char) = theResult : Boolean;
  operator = ( const s1: Str63; ch: char) = theResult : Boolean;
  operator = ( const s1: Str255; ch: char) = theResult : Boolean;
  operator = ( const t1: String; const s1: Str15 ) = theResult : Boolean;
  operator = ( const t1: String; const s1: Str27 ) = theResult : Boolean;
  operator = ( const t1: String; const s1: Str31 ) = theResult : Boolean;
  operator = ( const t1: String; const s1: Str32 ) = theResult : Boolean;
  operator = ( const t1: String; const s1: Str63 ) = theResult : Boolean;
  operator = ( const t1: String; const s1: Str255 ) = theResult : Boolean;
  operator = ( const s1: Str15; const t1: String ) = theResult : Boolean;
  operator = ( const s1: Str27; const t1: String ) = theResult : Boolean;
  operator = ( const s1: Str31; const t1: String ) = theResult : Boolean;
  operator = ( const s1: Str32; const t1: String ) = theResult : Boolean;
  operator = ( const s1: Str63; const t1: String ) = theResult : Boolean;
  operator = ( const s1: Str255; const t1: String ) = theResult : Boolean;
  operator = ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  operator = ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  operator = ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  operator = ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  operator = ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  operator = ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  operator = ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  operator = ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  operator = ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  operator = ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  operator = ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  operator = ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  operator = ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  operator = ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  operator = ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  operator = ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  operator = ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  operator = ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  operator = ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  operator = ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  operator = ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  operator = ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  operator = ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  operator = ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  operator = ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  operator = ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  operator = ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  operator = ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  operator = ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  operator = ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  operator = ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  operator = ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  operator = ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  operator = ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  operator = ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  operator = ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;

  operator <> ( ch: char; const s2: Str15 ) = theResult : Boolean;
  operator <> ( ch: char; const s2: Str27 ) = theResult : Boolean;
  operator <> ( ch: char; const s2: Str31 ) = theResult : Boolean;
  operator <> ( ch: char; const s2: Str32 ) = theResult : Boolean;
  operator <> ( ch: char; const s2: Str63 ) = theResult : Boolean;
  operator <> ( ch: char; const s2: Str255 ) = theResult : Boolean;
  operator <> ( const s1: Str15; ch: char) = theResult : Boolean;
  operator <> ( const s1: Str27; ch: char) = theResult : Boolean;
  operator <> ( const s1: Str31; ch: char) = theResult : Boolean;
  operator <> ( const s1: Str32; ch: char) = theResult : Boolean;
  operator <> ( const s1: Str63; ch: char) = theResult : Boolean;
  operator <> ( const s1: Str255; ch: char) = theResult : Boolean;
  operator <> ( const t1: String; const s1: Str15 ) = theResult : Boolean;
  operator <> ( const t1: String; const s1: Str27 ) = theResult : Boolean;
  operator <> ( const t1: String; const s1: Str31 ) = theResult : Boolean;
  operator <> ( const t1: String; const s1: Str32 ) = theResult : Boolean;
  operator <> ( const t1: String; const s1: Str63 ) = theResult : Boolean;
  operator <> ( const t1: String; const s1: Str255 ) = theResult : Boolean;
  operator <> ( const s1: Str15; const t1: String ) = theResult : Boolean;
  operator <> ( const s1: Str27; const t1: String ) = theResult : Boolean;
  operator <> ( const s1: Str31; const t1: String ) = theResult : Boolean;
  operator <> ( const s1: Str32; const t1: String ) = theResult : Boolean;
  operator <> ( const s1: Str63; const t1: String ) = theResult : Boolean;
  operator <> ( const s1: Str255; const t1: String ) = theResult : Boolean;
  operator <> ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  operator <> ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  operator <> ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  operator <> ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  operator <> ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  operator <> ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  operator <> ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  operator <> ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  operator <> ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  operator <> ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  operator <> ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  operator <> ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  operator <> ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  operator <> ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  operator <> ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  operator <> ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  operator <> ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  operator <> ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  operator <> ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  operator <> ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  operator <> ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  operator <> ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  operator <> ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  operator <> ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  operator <> ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  operator <> ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  operator <> ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  operator <> ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  operator <> ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  operator <> ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  operator <> ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  operator <> ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  operator <> ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  operator <> ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  operator <> ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  operator <> ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;

  operator <  ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  operator <= ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  operator >  ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  operator >= ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  operator <  ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  operator <= ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  operator >  ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  operator >= ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  operator <  ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  operator <= ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  operator >  ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  operator >= ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  operator <  ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  operator <= ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  operator >  ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  operator >= ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  operator <  ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  operator <= ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  operator >  ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  operator >= ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  operator <  ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  operator <= ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  operator >  ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  operator >= ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  operator <  ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  operator <= ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  operator >  ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  operator >= ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  operator <  ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  operator <= ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  operator >  ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  operator >= ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  operator <  ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  operator <= ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  operator >  ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  operator >= ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  operator <  ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  operator <= ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  operator >  ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  operator >= ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  operator <  ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  operator <= ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  operator >  ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  operator >= ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  operator <  ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  operator <= ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  operator >  ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  operator >= ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  operator <  ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  operator <= ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  operator >  ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  operator >= ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  operator <  ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  operator <= ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  operator >  ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  operator >= ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  operator <  ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  operator <= ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  operator >  ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  operator >= ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  operator <  ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  operator <= ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  operator >  ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  operator >= ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  operator <  ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  operator <= ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  operator >  ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  operator >= ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  operator <  ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  operator <= ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  operator >  ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  operator >= ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  operator <  ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  operator <= ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  operator >  ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  operator >= ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  operator <  ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  operator <= ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  operator >  ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  operator >= ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  operator <  ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  operator <= ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  operator >  ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  operator >= ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  operator <  ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  operator <= ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  operator >  ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  operator >= ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  operator <  ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  operator <= ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  operator >  ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  operator >= ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  operator <  ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  operator <= ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  operator >  ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  operator >= ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  operator <  ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  operator <= ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  operator >  ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  operator >= ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  operator <  ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  operator <= ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  operator >  ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  operator >= ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  operator <  ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  operator <= ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  operator >  ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  operator >= ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  operator <  ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  operator <= ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  operator >  ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  operator >= ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  operator <  ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  operator <= ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  operator >  ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  operator >= ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  operator <  ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  operator <= ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  operator >  ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  operator >= ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  operator <  ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  operator <= ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  operator >  ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  operator >= ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  operator <  ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  operator <= ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  operator >  ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  operator >= ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  operator <  ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  operator <= ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  operator >  ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  operator >= ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  operator <  ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  operator <= ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  operator >  ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  operator >= ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  operator <  ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  operator <= ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  operator >  ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  operator >= ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  operator <  ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;
  operator <= ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;
  operator >  ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;
  operator >= ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;

	function CharToStr15( ch: char) = theResult : Str15;
	function CharToStr27( ch: char) = theResult : Str27;
	function CharToStr31( ch: char) = theResult : Str31;
	function CharToStr32( ch: char) = theResult : Str32;
	function CharToStr63( ch: char) = theResult : Str63;
	function CharToStr255( ch: char) = theResult : Str255;
  function Str15ToStr15 ( const s: Str15 ) = theResult : Str15;
  function Str15ToStr27 ( const s: Str15 ) = theResult : Str27;
  function Str15ToStr31 ( const s: Str15 ) = theResult : Str31;
  function Str15ToStr32 ( const s: Str15 ) = theResult : Str32;
  function Str15ToStr63 ( const s: Str15 ) = theResult : Str63;
  function Str15ToStr255 ( const s: Str15 ) = theResult : Str255;
  function Str27ToStr15 ( const s: Str27 ) = theResult : Str15;
  function Str27ToStr27 ( const s: Str27 ) = theResult : Str27;
  function Str27ToStr31 ( const s: Str27 ) = theResult : Str31;
  function Str27ToStr32 ( const s: Str27 ) = theResult : Str32;
  function Str27ToStr63 ( const s: Str27 ) = theResult : Str63;
  function Str27ToStr255 ( const s: Str27 ) = theResult : Str255;
  function Str31ToStr15 ( const s: Str31 ) = theResult : Str15;
  function Str31ToStr27 ( const s: Str31 ) = theResult : Str27;
  function Str31ToStr31 ( const s: Str31 ) = theResult : Str31;
  function Str31ToStr32 ( const s: Str31 ) = theResult : Str32;
  function Str31ToStr63 ( const s: Str31 ) = theResult : Str63;
  function Str31ToStr255 ( const s: Str31 ) = theResult : Str255;
  function Str32ToStr15 ( const s: Str32 ) = theResult : Str15;
  function Str32ToStr27 ( const s: Str32 ) = theResult : Str27;
  function Str32ToStr31 ( const s: Str32 ) = theResult : Str31;
  function Str32ToStr32 ( const s: Str32 ) = theResult : Str32;
  function Str32ToStr63 ( const s: Str32 ) = theResult : Str63;
  function Str32ToStr255 ( const s: Str32 ) = theResult : Str255;
  function Str63ToStr15 ( const s: Str63 ) = theResult : Str15;
  function Str63ToStr27 ( const s: Str63 ) = theResult : Str27;
  function Str63ToStr31 ( const s: Str63 ) = theResult : Str31;
  function Str63ToStr32 ( const s: Str63 ) = theResult : Str32;
  function Str63ToStr63 ( const s: Str63 ) = theResult : Str63;
  function Str63ToStr255 ( const s: Str63 ) = theResult : Str255;
  function Str255ToStr15 ( const s: Str255 ) = theResult : Str15;
  function Str255ToStr27 ( const s: Str255 ) = theResult : Str27;
  function Str255ToStr31 ( const s: Str255 ) = theResult : Str31;
  function Str255ToStr32 ( const s: Str255 ) = theResult : Str32;
  function Str255ToStr63 ( const s: Str255 ) = theResult : Str63;
  function Str255ToStr255 ( const s: Str255 ) = theResult : Str255;

  procedure Str15IntoStr15 ( const s: Str15; var theResult: Str15 );
  procedure Str15IntoStr27 ( const s: Str15; var theResult: Str27 );
  procedure Str15IntoStr31 ( const s: Str15; var theResult: Str31 );
  procedure Str15IntoStr32 ( const s: Str15; var theResult: Str32 );
  procedure Str15IntoStr63 ( const s: Str15; var theResult: Str63 );
  procedure Str15IntoStr255 ( const s: Str15; var theResult: Str255 );
  procedure Str27IntoStr15 ( const s: Str27; var theResult: Str15 );
  procedure Str27IntoStr27 ( const s: Str27; var theResult: Str27 );
  procedure Str27IntoStr31 ( const s: Str27; var theResult: Str31 );
  procedure Str27IntoStr32 ( const s: Str27; var theResult: Str32 );
  procedure Str27IntoStr63 ( const s: Str27; var theResult: Str63 );
  procedure Str27IntoStr255 ( const s: Str27; var theResult: Str255 );
  procedure Str31IntoStr15 ( const s: Str31; var theResult: Str15 );
  procedure Str31IntoStr27 ( const s: Str31; var theResult: Str27 );
  procedure Str31IntoStr31 ( const s: Str31; var theResult: Str31 );
  procedure Str31IntoStr32 ( const s: Str31; var theResult: Str32 );
  procedure Str31IntoStr63 ( const s: Str31; var theResult: Str63 );
  procedure Str31IntoStr255 ( const s: Str31; var theResult: Str255 );
  procedure Str32IntoStr15 ( const s: Str32; var theResult: Str15 );
  procedure Str32IntoStr27 ( const s: Str32; var theResult: Str27 );
  procedure Str32IntoStr31 ( const s: Str32; var theResult: Str31 );
  procedure Str32IntoStr32 ( const s: Str32; var theResult: Str32 );
  procedure Str32IntoStr63 ( const s: Str32; var theResult: Str63 );
  procedure Str32IntoStr255 ( const s: Str32; var theResult: Str255 );
  procedure Str63IntoStr15 ( const s: Str63; var theResult: Str15 );
  procedure Str63IntoStr27 ( const s: Str63; var theResult: Str27 );
  procedure Str63IntoStr31 ( const s: Str63; var theResult: Str31 );
  procedure Str63IntoStr32 ( const s: Str63; var theResult: Str32 );
  procedure Str63IntoStr63 ( const s: Str63; var theResult: Str63 );
  procedure Str63IntoStr255 ( const s: Str63; var theResult: Str255 );
  procedure Str255IntoStr15 ( const s: Str255; var theResult: Str15 );
  procedure Str255IntoStr27 ( const s: Str255; var theResult: Str27 );
  procedure Str255IntoStr31 ( const s: Str255; var theResult: Str31 );
  procedure Str255IntoStr32 ( const s: Str255; var theResult: Str32 );
  procedure Str255IntoStr63 ( const s: Str255; var theResult: Str63 );
  procedure Str255IntoStr255 ( const s: Str255; var theResult: Str255 );

  function StringToStr15( const s: String ) = theResult : Str15;
  procedure StringIntoStr15( const s: String; var theResult: Str15 );
  function StringToStr27( const s: String ) = theResult : Str27;
  procedure StringIntoStr27( const s: String; var theResult: Str27 );
  function StringToStr31( const s: String ) = theResult : Str31;
  procedure StringIntoStr31( const s: String; var theResult: Str31 );
  function StringToStr32( const s: String ) = theResult : Str32;
  procedure StringIntoStr32( const s: String; var theResult: Str32 );
  function StringToStr63( const s: String ) = theResult : Str63;
  procedure StringIntoStr63( const s: String; var theResult: Str63 );
  function StringToStr255( const s: String ) = theResult : Str255;
  procedure StringIntoStr255( const s: String; var theResult: Str255 );

  function Str15ToString( const s: Str15 ) = theResult : String15;
  function Str27ToString( const s: Str27 ) = theResult : String27;
  function Str31ToString( const s: Str31 ) = theResult : String31;
  function Str32ToString( const s: Str32 ) = theResult : String32;
  function Str63ToString( const s: Str63 ) = theResult : String63;
  function Str255ToString( const s: Str255 ) = theResult : String255;

  procedure InitStr15( var s: Str15);
  procedure InitStr27( var s: Str27);
  procedure InitStr31( var s: Str31);
  procedure InitStr32( var s: Str32);
  procedure InitStr63( var s: Str63);
  procedure InitStr255( var s: Str255);

  function CopyStr15( const theSource: Str15; theStart, theCount: Integer) = theResult : Str255;
  function CopyStr27( const theSource: Str27; theStart, theCount: Integer) = theResult : Str255;
  function CopyStr31( const theSource: Str31; theStart, theCount: Integer) = theResult : Str255;
  function CopyStr32( const theSource: Str32; theStart, theCount: Integer) = theResult : Str255;
  function CopyStr63( const theSource: Str63; theStart, theCount: Integer) = theResult : Str255;
  function CopyStr255( const theSource: Str255; theStart, theCount: Integer) = theResult : Str255;

  procedure DeleteStr15( var theDest: Str15; theStart, theCount: Integer);
  procedure DeleteStr27( var theDest: Str27; theStart, theCount: Integer);
  procedure DeleteStr31( var theDest: Str31; theStart, theCount: Integer);
  procedure DeleteStr32( var theDest: Str32; theStart, theCount: Integer);
  procedure DeleteStr63( var theDest: Str63; theStart, theCount: Integer);
  procedure DeleteStr255( var theDest: Str255; theStart, theCount: Integer);

  procedure InsertStr255( const theSource: Str255; var theDest: Str255; theStart: Integer);

	function PosCharInStr15( theCh: Char; const theStr: Str15) = theIndex : Integer;
	function PosCharInStr27( theCh: Char; const theStr: Str27) = theIndex : Integer;
	function PosCharInStr31( theCh: Char; const theStr: Str31) = theIndex : Integer;
	function PosCharInStr32( theCh: Char; const theStr: Str32) = theIndex : Integer;
	function PosCharInStr63( theCh: Char; const theStr: Str63) = theIndex : Integer;
	function PosCharInStr255( theCh: Char; const theStr: Str255) = theIndex : Integer;

	function PosInStr255( const theSubStr, theSourceStr: Str255) = theIndex : Integer;


implementation

{$R-}
  function Str15Length( const s: Str15 ) = theResult : Integer;
  begin
    theResult := s.sLength;
    Assert( (0 <= theResult) and (theResult <= 15) );
  end;
  
  function Str27Length( const s: Str27 ) = theResult : Integer;
  begin
    theResult := s.sLength;
    Assert( (0 <= theResult) and (theResult <= 27) );
  end;
  
  function Str31Length( const s: Str31 ) = theResult : Integer;
  begin
    theResult := s.sLength;
    Assert( (0 <= theResult) and (theResult <= 31) );
  end;
  
  function Str32Length( const s: Str32 ) = theResult : Integer;
  begin
    theResult := s.sLength;
    Assert( (0 <= theResult) and (theResult <= 32) );
  end;
  
  function Str63Length( const s: Str63 ) = theResult : Integer;
  begin
    theResult := s.sLength;
    Assert( (0 <= theResult) and (theResult <= 63) );
  end;
  
  function Str255Length( const s: Str255 ) = theResult : Integer;
  begin
    theResult := s.sLength;
    Assert( (0 <= theResult) and (theResult <= 255) );
  end;
  

  procedure SetStr15Length( var s: Str15; len: Integer );
  begin
    Assert( (0 <= len) and (len <= 15) );
    s.sLength := Min( Max( len, 0 ), 15 );
  end;
  
  procedure SetStr27Length( var s: Str27; len: Integer );
  begin
    Assert( (0 <= len) and (len <= 27) );
    s.sLength := Min( Max( len, 0 ), 27 );
  end;
  
  procedure SetStr31Length( var s: Str31; len: Integer );
  begin
    Assert( (0 <= len) and (len <= 31) );
    s.sLength := Min( Max( len, 0 ), 31 );
  end;
  
  procedure SetStr32Length( var s: Str32; len: Integer );
  begin
    Assert( (0 <= len) and (len <= 32) );
    s.sLength := Min( Max( len, 0 ), 32 );
  end;
  
  procedure SetStr63Length( var s: Str63; len: Integer );
  begin
    Assert( (0 <= len) and (len <= 63) );
    s.sLength := Min( Max( len, 0 ), 63 );
  end;
  
  procedure SetStr255Length( var s: Str255; len: Integer );
  begin
    Assert( (0 <= len) and (len <= 255) );
    s.sLength := Min( Max( len, 0 ), 255 );
  end;



  procedure PrivAddPreCharStr255( ch: char; const s1; var theResult: Str255 );
		const
			len1 = 1;
    var
      s: StringPtr;
      len2: Integer;
  begin
    {$local W-} s := @s1; {$endlocal}
		len2 := Min( 255 - len1, s^.sLength );
    theResult.sLength := len1 + len2;
    theResult.sChars[1] := ch;
    if len2 > 0 then begin
      theResult.sChars[2..1+len2] := s^.sChars[1..len2];
    end;
  end;  

  procedure PrivAddPostCharStr255( const s1; ch: char; var theResult: Str255 );
    var
      s: StringPtr;
      len1,len2: Integer;
  begin
    {$local W-} s := @s1; {$endlocal}
		len1 := s^.sLength;
    len2 := Min( 255 - len1, 1 );
    theResult.sLength := len1 + len2;
    if len1 > 0 then
      theResult.sChars[1..len1] := s^.sChars[1..len1];
    if len2 > 0 then
      theResult.sChars[len1+1] := ch;
  end;


  operator + ( ch: char; const s2: Str15 ) = theResult : Str255;
  begin
    PrivAddPreCharStr255( ch, s2, theResult );
  end;
  
  operator + ( ch: char; const s2: Str27 ) = theResult : Str255;
  begin
    PrivAddPreCharStr255( ch, s2, theResult );
  end;
  
  operator + ( ch: char; const s2: Str31 ) = theResult : Str255;
  begin
    PrivAddPreCharStr255( ch, s2, theResult );
  end;
  
  operator + ( ch: char; const s2: Str32 ) = theResult : Str255;
  begin
    PrivAddPreCharStr255( ch, s2, theResult );
  end;
  
  operator + ( ch: char; const s2: Str63 ) = theResult : Str255;
  begin
    PrivAddPreCharStr255( ch, s2, theResult );
  end;

  operator + ( ch: char; const s2: Str255 ) = theResult : Str255;
  begin
    PrivAddPreCharStr255( ch, s2, theResult );
  end;

  operator + ( const s1: Str15; ch: char ) = theResult : Str255;
  begin
    PrivAddPostCharStr255( s1, ch, theResult );
  end;

  operator + ( const s1: Str27; ch: char ) = theResult : Str255;
  begin
    PrivAddPostCharStr255( s1, ch, theResult );
  end;

  operator + ( const s1: Str31; ch: char ) = theResult : Str255;
  begin
    PrivAddPostCharStr255( s1, ch, theResult );
  end;

  operator + ( const s1: Str32; ch: char ) = theResult : Str255;
  begin
    PrivAddPostCharStr255( s1, ch, theResult );
  end;

  operator + ( const s1: Str63; ch: char ) = theResult : Str255;
  begin
    PrivAddPostCharStr255( s1, ch, theResult );
  end;

  operator + ( const s1: Str255; ch: char ) = theResult : Str255;
  begin
    PrivAddPostCharStr255( s1, ch, theResult );
  end;


  procedure PrivAddPreStringStr255( const t1: String; const s1; var theResult: Str255 );
    var
      s: StringPtr;
      len1, len2: Integer;
  begin
    {$local W-} s := @s1; {$endlocal}
    len1 := Min( 255, Length(t1));
    len2 := Min( 255 - len1, s^.sLength);
    theResult.sLength := len1 + len2;
    if len1 > 0 then begin
      theResult.sChars[1..len1] := t1[1..len1];
    end;
    if len2 > 0 then begin
      theResult.sChars[len1+1..len1+len2] := s^.sChars[1..len2];
    end;
  end;  

  procedure PrivAddPostStringStr255( const s1; const t1: String; var theResult: Str255 );
    var
      s: StringPtr;
      len1, len2: Integer;
  begin
    {$local W-} s := @s1; {$endlocal}
    len1 := s^.sLength;
    len2 := Min( 255 - len1, Length(t1));
    theResult.sLength := len1 + len2;
    if len1 > 0 then begin
      theResult.sChars[1..len1] := s^.sChars[1..len1];
    end;
    if len2 > 0 then begin
      theResult.sChars[len1+1..len1+len2] := t1[1..len2];
    end;
  end;  

	operator + ( const t1: String; const s1: Str15) = theResult : Str255;
	begin
		PrivAddPreStringStr255( t1, s1, theResult)
	end;

	operator + ( const t1: String; const s1: Str27) = theResult : Str255;
	begin
		PrivAddPreStringStr255( t1, s1, theResult)
	end;

	operator + ( const t1: String; const s1: Str31) = theResult : Str255;
	begin
		PrivAddPreStringStr255( t1, s1, theResult)
	end;

	operator + ( const t1: String; const s1: Str32) = theResult : Str255;
	begin
		PrivAddPreStringStr255( t1, s1, theResult)
	end;

	operator + ( const t1: String; const s1: Str63) = theResult : Str255;
	begin
		PrivAddPreStringStr255( t1, s1, theResult)
	end;

	operator + ( const t1: String; const s1: Str255) = theResult : Str255;
	begin
		PrivAddPreStringStr255( t1, s1, theResult)
	end;

	operator + ( const s1: Str15; const t1: String) = theResult : Str255;
	begin
		PrivAddPostStringStr255( s1, t1, theResult)
	end;

	operator + ( const s1: Str27; const t1: String) = theResult : Str255;
	begin
		PrivAddPostStringStr255( s1, t1, theResult)
	end;

	operator + ( const s1: Str31; const t1: String) = theResult : Str255;
	begin
		PrivAddPostStringStr255( s1, t1, theResult)
	end;

	operator + ( const s1: Str32; const t1: String) = theResult : Str255;
	begin
		PrivAddPostStringStr255( s1, t1, theResult)
	end;

	operator + ( const s1: Str63; const t1: String) = theResult : Str255;
	begin
		PrivAddPostStringStr255( s1, t1, theResult)
	end;

	operator + ( const s1: Str255; const t1: String) = theResult : Str255;
	begin
		PrivAddPostStringStr255( s1, t1, theResult)
	end;


  procedure PrivAddStr255( const s1in; const s2in; var theResult: Str255 );
    var
      s1, s2: StringPtr;
      l1, l2: Integer;
  begin
    {$local W-} s1 := @s1in; {$endlocal}
    {$local W-} s2 := @s2in; {$endlocal}
    l1 := s1^.sLength;
    l2 := Min( 255 - l1, s2^.sLength );
    theResult.sLength := l1 + l2;
    if l1 > 0 then begin
      theResult.sChars[1..l1] := s1^.sChars[1..l1];
    end;
    if l2 > 0 then begin
      theResult.sChars[l1+1..l1+l2] := s2^.sChars[1..l2];
    end;
  end;


  operator + ( const s1: Str15; const s2: Str15 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str15; const s2: Str27 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str15; const s2: Str31 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str15; const s2: Str32 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str15; const s2: Str63 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str15; const s2: Str255 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str27; const s2: Str15 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str27; const s2: Str27 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str27; const s2: Str31 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str27; const s2: Str32 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str27; const s2: Str63 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str27; const s2: Str255 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str31; const s2: Str15 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;

  operator + ( const s1: Str31; const s2: Str27 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str31; const s2: Str31 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str31; const s2: Str32 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str31; const s2: Str63 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str31; const s2: Str255 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str32; const s2: Str15 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str32; const s2: Str27 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str32; const s2: Str31 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str32; const s2: Str32 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;

  operator + ( const s1: Str32; const s2: Str63 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str32; const s2: Str255 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str63; const s2: Str15 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str63; const s2: Str27 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str63; const s2: Str31 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str63; const s2: Str32 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;

  operator + ( const s1: Str63; const s2: Str63 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str63; const s2: Str255 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str255; const s2: Str15 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str255; const s2: Str27 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str255; const s2: Str31 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str255; const s2: Str32 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str255; const s2: Str63 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;
  
  operator + ( const s1: Str255; const s2: Str255 ) = theResult : Str255;
  begin
    PrivAddStr255( s1, s2, theResult );
  end;

  

  operator = ( ch: char; const s2: Str15 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength = 1) & ( s2.sChars[ 1] = ch)
	end;

  operator = ( ch: char; const s2: Str27 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength = 1) & ( s2.sChars[ 1] = ch)
	end;

  operator = ( ch: char; const s2: Str31 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength = 1) & ( s2.sChars[ 1] = ch)
	end;

  operator = ( ch: char; const s2: Str32 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength = 1) & ( s2.sChars[ 1] = ch)
	end;

  operator = ( ch: char; const s2: Str63 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength = 1) & ( s2.sChars[ 1] = ch)
	end;

  operator = ( ch: char; const s2: Str255 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength = 1) & ( s2.sChars[ 1] = ch)
	end;

  operator = ( const s1: Str15; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength = 1) & ( s1.sChars[ 1] = ch)
	end;

  operator = ( const s1: Str27; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength = 1) & ( s1.sChars[ 1] = ch)
	end;

  operator = ( const s1: Str31; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength = 1) & ( s1.sChars[ 1] = ch)
	end;

  operator = ( const s1: Str32; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength = 1) & ( s1.sChars[ 1] = ch)
	end;

  operator = ( const s1: Str63; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength = 1) & ( s1.sChars[ 1] = ch)
	end;

  operator = ( const s1: Str255; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength = 1) & ( s1.sChars[ 1] = ch)
	end;


  operator = ( const t1: String; const s1: Str15 ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const t1: String; const s1: Str27 ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const t1: String; const s1: Str31 ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const t1: String; const s1: Str32 ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const t1: String; const s1: Str63 ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const t1: String; const s1: Str255 ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const s1: Str15; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const s1: Str27; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const s1: Str31; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const s1: Str32; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const s1: Str63; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator = ( const s1: Str255; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) = s1.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;


  operator = ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator = ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength = s2.sLength;
    if theResult & (s1.sLength > 0) then begin
      theResult := EQ( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;



  operator <> ( ch: char; const s2: Str15 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength <> 1) | ( s2.sChars[ 1] <> ch)
	end;

  operator <> ( ch: char; const s2: Str27 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength <> 1) | ( s2.sChars[ 1] <> ch)
	end;

  operator <> ( ch: char; const s2: Str31 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength <> 1) | ( s2.sChars[ 1] <> ch)
	end;

  operator <> ( ch: char; const s2: Str32 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength <> 1) | ( s2.sChars[ 1] <> ch)
	end;

  operator <> ( ch: char; const s2: Str63 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength <> 1) | ( s2.sChars[ 1] <> ch)
	end;

  operator <> ( ch: char; const s2: Str255 ) = theResult : Boolean;
	begin
		theResult := ( s2.sLength <> 1) | ( s2.sChars[ 1] <> ch)
	end;

  operator <> ( const s1: Str15; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength <> 1) | ( s1.sChars[ 1] <> ch)
	end;

  operator <> ( const s1: Str27; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength <> 1) | ( s1.sChars[ 1] <> ch)
	end;

  operator <> ( const s1: Str31; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength <> 1) | ( s1.sChars[ 1] <> ch)
	end;

  operator <> ( const s1: Str32; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength <> 1) | ( s1.sChars[ 1] <> ch)
	end;

  operator <> ( const s1: Str63; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength <> 1) | ( s1.sChars[ 1] <> ch)
	end;

  operator <> ( const s1: Str255; ch: char) = theResult : Boolean;
	begin
		theResult := ( s1.sLength <> 1) | ( s1.sChars[ 1] <> ch)
	end;


  operator <> ( const t1: String; const s1: Str15 ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const t1: String; const s1: Str27 ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const t1: String; const s1: Str31 ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const t1: String; const s1: Str32 ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const t1: String; const s1: Str63 ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const t1: String; const s1: Str255 ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const s1: Str15; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const s1: Str27; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const s1: Str31; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const s1: Str32; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const s1: Str63; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;

  operator <> ( const s1: Str255; const t1: String ) = theResult : Boolean;
  begin
    theResult := Length(t1) <> s1.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( t1[1..s1.sLength], s1.sChars[1..s1.sLength] );
    end;
  end;


  operator <> ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;

  operator <> ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := s1.sLength <> s2.sLength;
    if not theResult & (s1.sLength > 0) then begin
      theResult := NE( s1.sChars[1..s1.sLength], s2.sChars[1..s2.sLength] );
    end;
  end;



  function PrivLessThanStrN( const s1in; const s2in ) = theResult : Boolean;
    var
      s1, s2: StringPtr;
  begin
    {$local W-} s1 := @s1in; {$endlocal}
    {$local W-} s2 := @s2in; {$endlocal}
    if s2^.sLength = 0 then begin
      theResult := false;
    end else if s1^.sLength = 0 then begin
      theResult := true;
    end else if LT( s1^.sChars[1..Min( s1^.sLength, s2^.sLength )], s2^.sChars[1..Min( s1^.sLength, s2^.sLength )] ) then begin
      theResult := true;
    end else if (s1^.sLength < s2^.sLength) & EQ( s1^.sChars[1..Min( s1^.sLength, s2^.sLength )], s2^.sChars[1..Min( s1^.sLength, s2^.sLength )] ) then begin
      theResult := true;
    end else begin
      theResult := false;
    end;
  end;
  
  function PrivLessThanOrEqualStrN( const s1in; const s2in ) = theResult : Boolean;
    var
      s1, s2: StringPtr;
  begin
    {$local W-} s1 := @s1in; {$endlocal}
    {$local W-} s2 := @s2in; {$endlocal}
    if s1^.sLength = 0 then begin
      theResult := true;
    end else if s2^.sLength = 0 then begin
      theResult := false;
    end else if LT( s1^.sChars[1..Min( s1^.sLength, s2^.sLength )], s2^.sChars[1..Min( s1^.sLength, s2^.sLength )] ) then begin
      theResult := true;
    end else if (s1^.sLength <= s2^.sLength) & EQ( s1^.sChars[1..Min( s1^.sLength, s2^.sLength )], s2^.sChars[1..Min( s1^.sLength, s2^.sLength )] ) then begin
      theResult := true;
    end else begin
      theResult := false;
    end;
  end;
  
  operator <  ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str15; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str15; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str15; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str15; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str15; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str15; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str27; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str27; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str27; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str27; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str27; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str27; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str31; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str31; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str31; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str31; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str31; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str31; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str32; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str32; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str32; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str32; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str32; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str32; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str63; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str63; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str63; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str63; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str63; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str63; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str255; const s2: Str15 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str255; const s2: Str27 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str255; const s2: Str31 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str255; const s2: Str32 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str255; const s2: Str63 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;

  operator <  ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s1, s2 );
  end;

  operator <= ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s1, s2 );
  end;

  operator >  ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanStrN( s2, s1 );
  end;

  operator >= ( const s1: Str255; const s2: Str255 ) = theResult : Boolean;
  begin
    theResult := PrivLessThanOrEqualStrN( s2, s1 );
  end;


	function CharToStr15( ch: char) = theResult : Str15;
  begin
    theResult.sLength := 1;
    theResult.sChars[1] := ch;
  end;

	function CharToStr27( ch: char) = theResult : Str27;
  begin
    theResult.sLength := 1;
    theResult.sChars[1] := ch;
  end;

	function CharToStr31( ch: char) = theResult : Str31;
  begin
    theResult.sLength := 1;
    theResult.sChars[1] := ch;
  end;

	function CharToStr32( ch: char) = theResult : Str32;
  begin
    theResult.sLength := 1;
    theResult.sChars[1] := ch;
  end;

	function CharToStr63( ch: char) = theResult : Str63;
  begin
    theResult.sLength := 1;
    theResult.sChars[1] := ch;
  end;

	function CharToStr255( ch: char) = theResult : Str255;
  begin
    theResult.sLength := 1;
    theResult.sChars[1] := ch;
  end;


  function Str15ToStr15 ( const s: Str15 ) = theResult : Str15;
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str15ToStr27 ( const s: Str15 ) = theResult : Str27;
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str15ToStr31 ( const s: Str15 ) = theResult : Str31;
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str15ToStr32 ( const s: Str15 ) = theResult : Str32;
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str15ToStr63 ( const s: Str15 ) = theResult : Str63;
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str15ToStr255 ( const s: Str15 ) = theResult : Str255;
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str27ToStr15 ( const s: Str27 ) = theResult : Str15;
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str27ToStr27 ( const s: Str27 ) = theResult : Str27;
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str27ToStr31 ( const s: Str27 ) = theResult : Str31;
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str27ToStr32 ( const s: Str27 ) = theResult : Str32;
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str27ToStr63 ( const s: Str27 ) = theResult : Str63;
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str27ToStr255 ( const s: Str27 ) = theResult : Str255;
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str31ToStr15 ( const s: Str31 ) = theResult : Str15;
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str31ToStr27 ( const s: Str31 ) = theResult : Str27;
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str31ToStr31 ( const s: Str31 ) = theResult : Str31;
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str31ToStr32 ( const s: Str31 ) = theResult : Str32;
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str31ToStr63 ( const s: Str31 ) = theResult : Str63;
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str31ToStr255 ( const s: Str31 ) = theResult : Str255;
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str32ToStr15 ( const s: Str32 ) = theResult : Str15;
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str32ToStr27 ( const s: Str32 ) = theResult : Str27;
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str32ToStr31 ( const s: Str32 ) = theResult : Str31;
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str32ToStr32 ( const s: Str32 ) = theResult : Str32;
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str32ToStr63 ( const s: Str32 ) = theResult : Str63;
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str32ToStr255 ( const s: Str32 ) = theResult : Str255;
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str63ToStr15 ( const s: Str63 ) = theResult : Str15;
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str63ToStr27 ( const s: Str63 ) = theResult : Str27;
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str63ToStr31 ( const s: Str63 ) = theResult : Str31;
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str63ToStr32 ( const s: Str63 ) = theResult : Str32;
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str63ToStr63 ( const s: Str63 ) = theResult : Str63;
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str63ToStr255 ( const s: Str63 ) = theResult : Str255;
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str255ToStr15 ( const s: Str255 ) = theResult : Str15;
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str255ToStr27 ( const s: Str255 ) = theResult : Str27;
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str255ToStr31 ( const s: Str255 ) = theResult : Str31;
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str255ToStr32 ( const s: Str255 ) = theResult : Str32;
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str255ToStr63 ( const s: Str255 ) = theResult : Str63;
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  function Str255ToStr255 ( const s: Str255 ) = theResult : Str255;
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;


  procedure Str15IntoStr15 ( const s: Str15; var theResult: Str15 );
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str15IntoStr27 ( const s: Str15; var theResult: Str27 );
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str15IntoStr31 ( const s: Str15; var theResult: Str31 );
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str15IntoStr32 ( const s: Str15; var theResult: Str32 );
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str15IntoStr63 ( const s: Str15; var theResult: Str63 );
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str15IntoStr255 ( const s: Str15; var theResult: Str255 );
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str27IntoStr15 ( const s: Str27; var theResult: Str15 );
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str27IntoStr27 ( const s: Str27; var theResult: Str27 );
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str27IntoStr31 ( const s: Str27; var theResult: Str31 );
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str27IntoStr32 ( const s: Str27; var theResult: Str32 );
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str27IntoStr63 ( const s: Str27; var theResult: Str63 );
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str27IntoStr255 ( const s: Str27; var theResult: Str255 );
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str31IntoStr15 ( const s: Str31; var theResult: Str15 );
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str31IntoStr27 ( const s: Str31; var theResult: Str27 );
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str31IntoStr31 ( const s: Str31; var theResult: Str31 );
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str31IntoStr32 ( const s: Str31; var theResult: Str32 );
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str31IntoStr63 ( const s: Str31; var theResult: Str63 );
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str31IntoStr255 ( const s: Str31; var theResult: Str255 );
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str32IntoStr15 ( const s: Str32; var theResult: Str15 );
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str32IntoStr27 ( const s: Str32; var theResult: Str27 );
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str32IntoStr31 ( const s: Str32; var theResult: Str31 );
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str32IntoStr32 ( const s: Str32; var theResult: Str32 );
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str32IntoStr63 ( const s: Str32; var theResult: Str63 );
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str32IntoStr255 ( const s: Str32; var theResult: Str255 );
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str63IntoStr15 ( const s: Str63; var theResult: Str15 );
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str63IntoStr27 ( const s: Str63; var theResult: Str27 );
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str63IntoStr31 ( const s: Str63; var theResult: Str31 );
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str63IntoStr32 ( const s: Str63; var theResult: Str32 );
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str63IntoStr63 ( const s: Str63; var theResult: Str63 );
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str63IntoStr255 ( const s: Str63; var theResult: Str255 );
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str255IntoStr15 ( const s: Str255; var theResult: Str15 );
  begin
    theResult.sLength := Min( s.sLength, 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str255IntoStr27 ( const s: Str255; var theResult: Str27 );
  begin
    theResult.sLength := Min( s.sLength, 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str255IntoStr31 ( const s: Str255; var theResult: Str31 );
  begin
    theResult.sLength := Min( s.sLength, 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str255IntoStr32 ( const s: Str255; var theResult: Str32 );
  begin
    theResult.sLength := Min( s.sLength, 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str255IntoStr63 ( const s: Str255; var theResult: Str63 );
  begin
    theResult.sLength := Min( s.sLength, 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;

  procedure Str255IntoStr255 ( const s: Str255; var theResult: Str255 );
  begin
    theResult.sLength := Min( s.sLength, 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s.sChars[1..theResult.sLength];
    end;
  end;


  function StringToStr15( const s: String ) = theResult : Str15;
  begin
    theResult.sLength := Min( Length( s ), 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  procedure StringIntoStr15( const s: String; var theResult: Str15 );
  begin
    theResult.sLength := Min( Length( s ), 15 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  function StringToStr27( const s: String ) = theResult : Str27;
  begin
    theResult.sLength := Min( Length( s ), 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  procedure StringIntoStr27( const s: String; var theResult: Str27 );
  begin
    theResult.sLength := Min( Length( s ), 27 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  function StringToStr31( const s: String ) = theResult : Str31;
  begin
    theResult.sLength := Min( Length( s ), 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  procedure StringIntoStr31( const s: String; var theResult: Str31 );
  begin
    theResult.sLength := Min( Length( s ), 31 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  function StringToStr32( const s: String ) = theResult : Str32;
  begin
    theResult.sLength := Min( Length( s ), 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  procedure StringIntoStr32( const s: String; var theResult: Str32 );
  begin
    theResult.sLength := Min( Length( s ), 32 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  function StringToStr63( const s: String ) = theResult : Str63;
  begin
    theResult.sLength := Min( Length( s ), 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  procedure StringIntoStr63( const s: String; var theResult: Str63 );
  begin
    theResult.sLength := Min( Length( s ), 63 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  function StringToStr255( const s: String ) = theResult : Str255;
  begin
    theResult.sLength := Min( Length( s ), 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;

  procedure StringIntoStr255( const s: String; var theResult: Str255 );
  begin
    theResult.sLength := Min( Length( s ), 255 );
    if theResult.sLength > 0 then begin
      theResult.sChars[1..theResult.sLength] := s[1..theResult.sLength];
    end;
  end;


  function Str15ToString( const s: Str15 ) = theResult : String15;
  begin
    SetLength( theResult, Min( s.sLength, theResult.Capacity ) );
    if Length( theResult ) > 0 then begin
      theResult[1..Length( theResult )] := s.sChars[1..Length( theResult )];
    end;
  end;

  function Str27ToString( const s: Str27 ) = theResult : String27;
  begin
    SetLength( theResult, Min( s.sLength, theResult.Capacity ) );
    if Length( theResult ) > 0 then begin
      theResult[1..Length( theResult )] := s.sChars[1..Length( theResult )];
    end;
  end;

  function Str31ToString( const s: Str31 ) = theResult : String31;
  begin
    SetLength( theResult, Min( s.sLength, theResult.Capacity ) );
    if Length( theResult ) > 0 then begin
      theResult[1..Length( theResult )] := s.sChars[1..Length( theResult )];
    end;
  end;

  function Str32ToString( const s: Str32 ) = theResult : String32;
  begin
    SetLength( theResult, Min( s.sLength, theResult.Capacity ) );
    if Length( theResult ) > 0 then begin
      theResult[1..Length( theResult )] := s.sChars[1..Length( theResult )];
    end;
  end;

  function Str63ToString( const s: Str63 ) = theResult : String63;
  begin
    SetLength( theResult, Min( s.sLength, theResult.Capacity ) );
    if Length( theResult ) > 0 then begin
      theResult[1..Length( theResult )] := s.sChars[1..Length( theResult )];
    end;
  end;

  function Str255ToString( const s: Str255 ) = theResult : String255;
  begin
    SetLength( theResult, Min( s.sLength, theResult.Capacity ) );
    if Length( theResult ) > 0 then begin
      theResult[1..Length( theResult )] := s.sChars[1..Length( theResult )];
    end;
  end;


  procedure InitStr15( var s: Str15);
  begin
    s.sLength := 0;
    s.sChars[1..15] := char( 0)
  end;

  procedure InitStr27( var s: Str27);
  begin
    s.sLength := 0;
    s.sChars[1..27] := char( 0)
  end;

  procedure InitStr31( var s: Str31);
  begin
    s.sLength := 0;
    s.sChars[1..31] := char( 0)
  end;

  procedure InitStr32( var s: Str32);
  begin
    s.sLength := 0;
    s.sChars[1..32] := char( 0)
  end;

  procedure InitStr63( var s: Str63);
  begin
    s.sLength := 0;
    s.sChars[1..63] := char( 0)
  end;

  procedure InitStr255( var s: Str255);
  begin
    s.sLength := 0;
    s.sChars[1..255] := char( 0)
  end;


  procedure PrivCopyStr( theSourcePtr: StringPtr; theMaxLength, theStart, theCount: Integer; var theResult: Str255);
  begin
    Assert( theSourcePtr^.sLength <= theMaxLength );
    if ( theStart < 1) or ( theStart > theSourcePtr^.sLength) or ( theCount <= 0) or ( theCount > theSourcePtr^.sLength - theStart + 1)
      then begin
        theResult.sLength := 0;
      end
      else begin
        theResult.sLength := theCount;
        theResult.sChars[1..theCount] := theSourcePtr^.sChars[ theStart..theStart + theCount - 1];
      end;
    theResult.sChars[ theResult.sLength +1 .. theMaxLength] := char( 0)
  end;


  function CopyStr15( const theSource: Str15; theStart, theCount: Integer) = theResult : Str255;
  begin
    {$local W-,T-} PrivCopyStr( @theSource, 15, theStart, theCount, theResult) {$endlocal}
  end;

  function CopyStr27( const theSource: Str27; theStart, theCount: Integer) = theResult : Str255;
  begin
    {$local W-,T-} PrivCopyStr( @theSource, 27, theStart, theCount, theResult) {$endlocal}
  end;

  function CopyStr31( const theSource: Str31; theStart, theCount: Integer) = theResult : Str255;
  begin
    {$local W-,T-} PrivCopyStr( @theSource, 31, theStart, theCount, theResult) {$endlocal}
  end;

  function CopyStr32( const theSource: Str32; theStart, theCount: Integer) = theResult : Str255;
  begin
    {$local W-,T-} PrivCopyStr( @theSource, 32, theStart, theCount, theResult) {$endlocal}
  end;

  function CopyStr63( const theSource: Str63; theStart, theCount: Integer) = theResult : Str255;
  begin
    {$local W-,T-} PrivCopyStr( @theSource, 63, theStart, theCount, theResult) {$endlocal}
  end;

  function CopyStr255( const theSource: Str255; theStart, theCount: Integer) = theResult : Str255;
  begin
    {$local W-,T-} PrivCopyStr( @theSource, 255, theStart, theCount, theResult) {$endlocal}
  end;


  procedure PrivDeleteStr( theDestPtr: StringPtr; theMaxLength, theStart, theCount: Integer);
  var
    theMaxCount: Integer;
    theOldLength: Integer;
    theNewLength: Integer;
  begin
  	with theDestPtr^ do
	  	if ( theStart >= 1) and ( theStart <= sLength) and ( theCount >= 1) and ( theCount <= theMaxLength)
	  		then begin
          Assert( sLength <= theMaxLength);
	  			theOldLength := sLength;
	  			theMaxCount := theOldLength - theStart + 1;
	  			if theCount > theMaxCount
	  				then theCount := theMaxCount;
	  			theNewLength := theOldLength - theCount;
          sChars[ theStart .. theOldLength - theCount] := sChars[ theStart + theCount .. theOldLength];
          sChars[ theNewLength + 1 .. theOldLength] := char( 0);
					sLength := theNewLength;
        end
  end;

  procedure DeleteStr15( var theDest: Str15; theStart, theCount: Integer);
  begin
    {$local W-,T-} PrivDeleteStr( @theDest, 15, theStart, theCount) {$endlocal}
  end;

  procedure DeleteStr27( var theDest: Str27; theStart, theCount: Integer);
  begin
    {$local W-,T-} PrivDeleteStr( @theDest, 27, theStart, theCount) {$endlocal}
  end;

  procedure DeleteStr31( var theDest: Str31; theStart, theCount: Integer);
  begin
    {$local W-,T-} PrivDeleteStr( @theDest, 31, theStart, theCount) {$endlocal}
  end;

  procedure DeleteStr32( var theDest: Str32; theStart, theCount: Integer);
  begin
    {$local W-,T-} PrivDeleteStr( @theDest, 32, theStart, theCount) {$endlocal}
  end;

  procedure DeleteStr63( var theDest: Str63; theStart, theCount: Integer);
  begin
    {$local W-,T-} PrivDeleteStr( @theDest, 63, theStart, theCount) {$endlocal}
  end;

  procedure DeleteStr255( var theDest: Str255; theStart, theCount: Integer);
  begin
    {$local W-,T-} PrivDeleteStr( @theDest, 255, theStart, theCount) {$endlocal}
  end;


  procedure InsertStr255( const theSource: Str255; var theDest: Str255; theStart: Integer);
  var
    theMaxIns: Integer;
    theIns: Integer;
    theIndex: Integer;
    theNewLength: Integer;
	begin
		if ( theStart >= 1) and ( theStart <= 255) and ( theStart <= theDest.sLength + 1)
			then begin
  			theIns := theSource.sLength;
	  		theMaxIns := 255 - theStart + 1;
	  		if theIns > theMaxIns
	  			then theIns := theMaxIns;
	  		theNewLength := theDest.sLength + theIns;
  			if theNewLength > 255
  				then theNewLength := 255;
  			for theIndex := theNewLength downto theStart + theIns do
  				theDest.sChars[ theIndex] := theDest.sChars[ theIndex - theIns];
  			for theIndex := 1 to theIns do
					theDest.sChars[ theStart + theIndex - 1] := theSource.sChars[ theIndex];
        theDest.sLength:=theNewLength
			end
	end;


	function PrivPosCharInStr( theCh: Char; theSourcePtr: StringPtr) = theIndex : Integer;
	var
		theFoundFlag: boolean;
	begin
		theFoundFlag := False;
		theIndex := 1;
		with theSourcePtr^ do
			while ( theIndex <= sLength) and not theFoundFlag do
				if sChars[ theIndex] = theCh
					then theFoundFlag := True
					else theIndex := theIndex + 1;
		if not theFoundFlag
			then theIndex := 0
	end;

	function PosCharInStr15( theCh: Char; const theStr: Str15) = theIndex : Integer;
	begin
		{$local W-,T-} theIndex := PrivPosCharInStr( theCh, @theStr) {$endlocal}
	end;

	function PosCharInStr27( theCh: Char; const theStr: Str27) = theIndex : Integer;
	begin
		{$local W-,T-} theIndex := PrivPosCharInStr( theCh, @theStr) {$endlocal}
	end;

	function PosCharInStr31( theCh: Char; const theStr: Str31) = theIndex : Integer;
	begin
		{$local W-,T-} theIndex := PrivPosCharInStr( theCh, @theStr) {$endlocal}
	end;

	function PosCharInStr32( theCh: Char; const theStr: Str32) = theIndex : Integer;
	begin
		{$local W-,T-} theIndex := PrivPosCharInStr( theCh, @theStr) {$endlocal}
	end;

	function PosCharInStr63( theCh: Char; const theStr: Str63) = theIndex : Integer;
	begin
		{$local W-,T-} theIndex := PrivPosCharInStr( theCh, @theStr) {$endlocal}
	end;

	function PosCharInStr255( theCh: Char; const theStr: Str255) = theIndex : Integer;
	begin
		{$local W-,T-} theIndex := PrivPosCharInStr( theCh, @theStr) {$endlocal}
	end;


	function PosInStr255( const theSubStr, theSourceStr: Str255) = theIndex : Integer;
  	var
    	theFoundFlag: boolean;
    	theLastIndex: Integer;
	begin
	{ for highest speed, use a Boyer-Moore string search instead }
		theFoundFlag := False;
		theLastIndex := theSourceStr.sLength - theSubStr.sLength + 1;
		theIndex := 1;
		while ( theIndex <= theLastIndex) and not theFoundFlag do
			if theSourceStr.sChars[ theIndex .. theIndex + theSubStr.sLength - 1] = theSubStr.sChars[ 1..theSubStr.sLength]
				then theFoundFlag := True
				else theIndex := theIndex + 1;
		if not theFoundFlag
			then theIndex := 0
	end;

end.
