{ CoreGraphics - CGFunction.h
   Copyright (c) 1999-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGFunction;
interface
uses MacTypes,CGBase,CFBase;
{$ALIGN POWER}


{ A CGFunction is a general floating-point function evaluator which uses a
   user-specified callback to map an arbitrary number of inputs to an
   arbitrary number of outputs. }

type
	CGFunctionRef = ^OpaqueCGFunctionRef; { an opaque type }
	OpaqueCGFunctionRef = record end;


{ This callback evaluates a function, using `in' as inputs, and places the
   result in `out'. `info' is the info parameter passed to the CGFunction
   creation functions. }

type
	CGFunctionEvaluateCallback = procedure( info: univ Ptr; inp: {const} CGFloatPtr; out: CGFloatPtr );

{ When a function is deallocated, this callback releases `info', the info
   parameter passed to the CGFunction creation functions. }

type
	CGFunctionReleaseInfoCallback = procedure( info: univ Ptr );

{ Callbacks for a CGFunction.
     `version' is the version number of this structure. This structure is
       version 0.
     `evaluate' is the callback used to evaluate the function.
     `releaseInfo', if non-NULL, is the callback used to release the info
       parameter passed to the CGFunction creation functions when the
       function is deallocated. }

type
	CGFunctionCallbacks = record
		version: UInt32;
{$ifc TARGET_CPU_64}
 		__alignment_dummy: UInt32;
{$endif}
		evaluate: CGFunctionEvaluateCallback;
		releaseInfo: CGFunctionReleaseInfoCallback;
	end;

{ Return the CFTypeID for CGFunctionRefs. }

function CGFunctionGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Create a CGFunction using `callbacks' to evaluate the function. `info' is
   passed to each of the callback functions. `domainDimension' is the number
   of input values to the function; `rangeDimension' is the number of output
   values from the function.

   `domain' is an array of 2M values, where M is the number of input values.
   For each k from 0 to M-1, domain[2*k] must be less than or equal to
   domain[2*k+1]. The k'th input value (in[k]) will be clipped to lie in
   this interval, so that domain[2*k] <= in[k] <= domain[2*k+1]. If `domain'
   is NULL, then the input values will not be clipped. However, it's
   strongly recommended that `domain' be specified; each domain interval
   should specify reasonable values for the minimum and maximum in that
   dimension.

   `range' is an array of 2N values, where N is the number of output values.
   For each k from 0 to N-1, range[2*k] must be less than or equal to
   range[2*k+1]. The k'th output value (out[k]) will be clipped to lie in
   this interval, so that range[2*k] <= out[k] <= range[2*k+1]. If `range'
   is NULL, then the output values will not be clipped. However, it's
   strongly recommended that `range' be specified; each range interval
   should specify reasonable values for the minimum and maximum in that
   dimension.

   The contents of the callbacks structure is copied, so, for example, a
   pointer to a structure on the stack can be passed to this function. }

function CGFunctionCreate( info: univ Ptr; domainDimension: size_t; domain: {const} CGFloatPtr; rangeDimension: size_t; range: {const} CGFloatPtr; const var callbacks: CGFunctionCallbacks ): CGFunctionRef;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Equivalent to `CFRetain(function)', except it doesn't crash (as CFRetain
   does) if `function' is NULL. }

function CGFunctionRetain( func: CGFunctionRef ): CGFunctionRef;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Equivalent to `CFRelease(function)', except it doesn't crash (as
   CFRelease does) if `function' is NULL. }

procedure CGFunctionRelease( func: CGFunctionRef );
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);


end.
