{
 * ImageIO - CGImageDestination.h
 * Copyright (c) 2004-2010 Apple Inc. All rights reserved.
 *
 }

{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }

unit CGImageDestination;
interface
uses MacTypes, CFArray, CFBase, CFData, CFDictionary, CFError, CFURL, CGDataConsumer, CGImage, CGImageSource, CGImageMetadata;

{$ALIGN POWER}


type
	CGImageDestinationRef = ^OpaqueCGImageDestinationRef; { an opaque type }
	OpaqueCGImageDestinationRef = record end;


{* Properties which may be passed to "CGImageDestinationAddImage"
 ** or "CGImageDestinationAddImageFromSource" to effect the output.
 ** The values apply to a single image of an image destination. *}


{ The desired compression quality to use when writing to an image 
 * destination. If present, the value of this key is a CFNumberRef 
 * in the range 0.0 to 1.0. A value of 1.0 implies lossless
 * compression is desired if destination format supports it. 
 * A value of 0.0 implies that that maximum compression is 
 * desired. }

const kCGImageDestinationLossyCompressionQuality: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


{ The desired background color to composite against when writing 
 * an image with alpha to a destination format that does not support 
 * alpha. If present, the value of this key is a CGColorRef without
 * any alpha component of its own.  If not present a white color
 * will be used if needed. }

const kCGImageDestinationBackgroundColor: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


{ Return the CFTypeID for CGImageDestinations. }

function CGImageDestinationGetTypeID: CFTypeID;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Return an array of supported type identifiers. }

function CGImageDestinationCopyTypeIdentifiers: CFArrayRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Create an image destination writing to the data consumer `consumer'.
 * The parameter `type' specifies the type identifier of the resulting
 * image file.  The parameter `count' specifies number of images (not
 * including thumbnails) that the image file will contain. The `options'
 * dictionary is reserved for future use; currently, you should pass NULL
 * for this parameter. }

function CGImageDestinationCreateWithDataConsumer( consumer: CGDataConsumerRef; typ: CFStringRef; count: size_t; options: CFDictionaryRef ): CGImageDestinationRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Create an image destination writing to `data'. The parameter `type'
 * specifies the type identifier of the resulting image file.  The
 * parameter `count' specifies number of images (not including thumbnails)
 * that the image file will contain. The `options' dictionary is reserved
 * for future use; currently, you should pass NULL for this parameter. }

function CGImageDestinationCreateWithData( data: CFMutableDataRef; typ: CFStringRef; count: size_t; options: CFDictionaryRef ): CGImageDestinationRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Create an image destination writing to `url'. The parameter `type'
 * specifies the type identifier of the resulting image file.  The
 * parameter `count' specifies number of images (not including thumbnails)
 * that the image file will contain. The `options' dictionary is reserved
 * for future use; currently, you should pass NULL for this parameter.
 * Note that if `url' already exists, it will be overwritten. }

function CGImageDestinationCreateWithURL( url: CFURLRef; typ: CFStringRef; count: size_t; options: CFDictionaryRef ): CGImageDestinationRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Specify the dictionary `properties' of properties which apply to all
 * images in the image destination `idst'. }

procedure CGImageDestinationSetProperties( idst: CGImageDestinationRef; properties: CFDictionaryRef );
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Set the next image in the image destination `idst' to be `image' with
 * optional properties specified in `properties'.  An error is logged if
 * more images are added than specified in the original count of the image
 * destination. }

procedure CGImageDestinationAddImage( idst: CGImageDestinationRef; image: CGImageRef; properties: CFDictionaryRef );
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Set the next image in the image destination `idst' to be the image at
 * `index' in the image source `isrc'.  The index is zero-based. The
 * properties of the source image can be added to or overriden by supplying
 * additional keys/values in `properties'.  If a key in `properties' has
 * the value kCFNull, the corresponding property in the destination will be
 * removed. }

procedure CGImageDestinationAddImageFromSource( idst: CGImageDestinationRef; isrc: CGImageSourceRef; index: size_t; properties: CFDictionaryRef );
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Write everything to the destination data, url or consumer of the image
 * destination `idst'.  You must call this function or the image
 * destination will not be valid.  After this function is called, no
 * additional data will be written to the image destination.  Return true
 * if the image was successfully written; false otherwise. }

function CGImageDestinationFinalize( idst: CGImageDestinationRef ): CBool;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{$ifc TARGET_OS_MAC}
{ Set the next image in the image destination `idst' to be `image' with
 * metadata properties specified in `metadata'. An error is logged if more
 * images are added than specified in the original count of the image
 * destination. }
procedure CGImageDestinationAddImageAndMetadata( idst: CGImageDestinationRef; image: CGImageRef; metadata: CGImageMetadataRef; options: CFDictionaryRef );
IMAGEIO_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{*
 ** Keys which may be used in the 'options' dictionary of
 ** "CGImageDestinationCopyImageSource" to effect the output.
 *}

{ Set the metadata tags for the image destination. If present, the value of
 * this key is a CGImageMetadataRef. By default, all EXIF, IPTC, and XMP tags
 * will be replaced. Use kCGImageDestinationMergeMetadata to merge the tags
 * with the existing tags in the image source.
 }
const kCGImageDestinationMetadata: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{ If true, The metadata will be copied from the source and merged with the tags
 * specified in kCGImageDestinationMetadata. If a tag does not exist in the 
 * source, it will be added. If the tag exists in the source, it will be 
 * updated. A metadata tag can be removed by setting the tag's value to 
 * kCFNull. If present, the value of this key is a CFBoooleanRef. The default
 * is kCFBooleanFalse.
 } 
const kCGImageDestinationMergeMetadata: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{ XMP data will not be written to the destination. If used in conjunction with 
 * kCGImageDestinationMetadata, EXIF and/or IPTC tags will be preserved, but 
 * an XMP packet will not be written to the file. If present, the value for 
 * this key is a CFBooleanRef. The default is kCFBooleanFalse.
 }
const kCGImageMetadataShouldExcludeXMP: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{ Updates the DateTime parameters of the image metadata. Only values
 * present in the original image will updated. If present, the value should
 * be a CFStringRef or a CFDateRef. If CFString, the value must be in 
 * Exif DateTime or ISO 8601 DateTime format. This option is mutually
 * exclusive with kCGImageDestinationMetadata.
 }
const kCGImageDestinationDateTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{ Updates the orientation in the image metadata. The image data itself will
 * not be rotated. If present, the value should be a CFNumberRef from 1 to 8. 
 * This option is mutually exclusive with kCGImageDestinationMetadata.
 }
const kCGImageDestinationOrientation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{ Losslessly copies the contents of the image source, 'isrc', to the 
 * destination, 'idst'. The image data will not be modified. The image's 
 * metadata can be modified by adding the keys and values defined above to 
 * 'options'. No other images should be added to the image destination. 
 * CGImageDestinationFinalize() should not be called afterward -
 * the result is saved to the destination when this function returns. 
 * The image type of the destination must match the image source. Returns true
 * if the operation was successful. If an error occurs, false will be returned 
 * and 'err' will be set to a CFErrorRef. Not all image formats are supported 
 * for this operation. }
function CGImageDestinationCopyImageSource( idst: CGImageDestinationRef; isrc: CGImageSourceRef; options: CFDictionaryRef; var err: CFErrorRef ): CBool;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{$endc} {TARGET_OS_MAC}

end.
