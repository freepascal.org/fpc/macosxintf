{
 *  AXValue.h
 *  Accessibility
 *
 *  Copyright (c) 2002 Apple Computer, Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, 2004 }
{  Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }

unit AXValue;
interface
uses MacTypes,CFBase;

{$ifc TARGET_OS_MAC}

type AXValueType = UInt32;
const
    { Types from CoreGraphics.h }
    kAXValueCGPointType = 1;
    kAXValueCGSizeType = 2;
    kAXValueCGRectType = 3;

    { Types from CFBase.h }
    kAXValueCFRangeType = 4;

    { Types from AXError.h }
    kAXValueAXErrorType = 5;

    { Other }
    kAXValueIllegalType = 0;

type
	AXValueRef = ^__AXValue; { an opaque type }
	__AXValue = record end;

function AXValueGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;


function AXValueCreate( theType: AXValueType; valuePtr: {const} univ Ptr ): AXValueRef;
function AXValueGetType( value: AXValueRef ): AXValueType;

function AXValueGetValue( value: AXValueRef; theType: AXValueType; valuePtr: univ Ptr ): Boolean;

{$endc} {TARGET_OS_MAC}

end.
