{
     File:       QuickTime/QTML.h
 
     Contains:   QuickTime Cross-platform specific interfaces
 
     Version:    QuickTime 7.7.1
 
     Copyright:  � 1997-2012 by Apple Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit QTML;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{ QuickTime is not available to 64-bit clients }

{$ifc not TARGET_CPU_64}
{
 *  QTMLYieldCPU()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 3.0 and later
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }
procedure QTMLYieldCPU;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ QTMLYieldCPUTime flags}
const
	kQTMLHandlePortEvents = 1 shl 0; { ask for event handling during the yield}

{
 *  QTMLYieldCPUTime()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 3.0 and later
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }
procedure QTMLYieldCPUTime( milliSeconds: SIGNEDLONG; flags: UNSIGNEDLONG );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


type
	QTMLMutex = ^OpaqueQTMLMutex; { an opaque type }
	OpaqueQTMLMutex = record end;
	QTMLMutexPtr = ^QTMLMutex; { when a var xx:QTMLMutex parameter can be nil, it is changed to xx: QTMLMutexPtr }
	QTMLSyncVar = ^OpaqueQTMLSyncVar; { an opaque type }
	OpaqueQTMLSyncVar = record end;
	QTMLSyncVarPtr = ^QTMLSyncVar;  { when a var xx:QTMLSyncVar parameter can be nil, it is changed to xx: QTMLSyncVarPtr }
{ InitializeQTML flags}
const
	kInitializeQTMLNoSoundFlag = 1 shl 0; { flag for requesting no sound when calling InitializeQTML}
	kInitializeQTMLUseGDIFlag = 1 shl 1; { flag for requesting GDI when calling InitializeQTML}
	kInitializeQTMLDisableDirectSound = 1 shl 2; { disables QTML's use of DirectSound}
	kInitializeQTMLUseExclusiveFullScreenModeFlag = 1 shl 3; { later than QTML 3.0: qtml starts up in exclusive full screen mode}
	kInitializeQTMLDisableDDClippers = 1 shl 4; { flag for requesting QTML not to use DirectDraw clipper objects; QTML 5.0 and later}
	kInitializeQTMLEnableDoubleBufferedSurface = 1 shl 6; { flag for requesting QuickTime use a double-buffered destination surface; QT6.4 and later}

{
 *  InitializeQTML()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  TerminateQTML()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{ CreatePortAssociation flags}
const
	kQTMLNoIdleEvents = 1 shl 1; { ask for a non-auto-idled port to be created}
	kQTMLNoDoubleBufferPort = 1 shl 2; { ask for QTML not to double-buffer this port}

const
	kQTMLIsDoubleBuffered = 'UsesDoubleBuffer';
{
 *  CreatePortAssociation()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  DestroyPortAssociation()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLGrabMutex()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 3.0 and later
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }
procedure QTMLGrabMutex( mu: QTMLMutex );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTMLTryGrabMutex()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 4.1 and later
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 4.1 and later
 }
function QTMLTryGrabMutex( mu: QTMLMutex ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTMLReturnMutex()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 3.0 and later
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }
procedure QTMLReturnMutex( mu: QTMLMutex );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTMLCreateMutex()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 3.0 and later
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }
function QTMLCreateMutex: QTMLMutex;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTMLDestroyMutex()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 3.0 and later
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }
procedure QTMLDestroyMutex( mu: QTMLMutex );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTMLCreateSyncVar()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLDestroySyncVar()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLTestAndSetSyncVar()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLWaitAndSetSyncVar()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLResetSyncVar()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  InitializeQHdr()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  TerminateQHdr()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLAcquireWindowList()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLReleaseWindowList()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
   These routines are here to support "interrupt level" code
      These are dangerous routines, only use if you know what you are doing.
}

{
 *  QTMLRegisterInterruptSafeThread()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLUnregisterInterruptSafeThread()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  NativeEventToMacEvent()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{$ifc TARGET_OS_WIN32}
{
 *  WinEventToMacEvent()
 *  
 *  Availability:
 *    Non-Carbon CFM:   not available
 *    CarbonLib:        not available
 *    Mac OS X:         not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }
function WinEventToMacEvent(winMsg: univ Ptr; var macEvent: EventRecord): SInt32;

{
 *  IsTaskBarVisible()
 *  
 *  Availability:
 *    Non-Carbon CFM:   not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  ShowHideTaskBar()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


const
	kDDSurfaceLocked = 1 shl 0;
	kDDSurfaceStatic = 1 shl 1;

{
 *  QTGetDDObject()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTSetDDObject()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTSetDDPrimarySurface()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLGetVolumeRootPath()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLSetWindowWndProc()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTMLGetWindowWndProc()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{$endc}  { TARGET_OS_WIN32 }

{
 *  QTMLGetCanonicalPathName()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


const
	kFullNativePath = 0;
	kFileNameOnly = 1 shl 0;
	kDirectoryPathOnly = 1 shl 1;
	kUFSFullPathName = 1 shl 2;
	kTryVDIMask = 1 shl 3; {    Used in NativePathNameToFSSpec to specify to search VDI mountpoints}
	kFullPathSpecifiedMask = 1 shl 4; {    the passed in name is a fully qualified full path}

{
 *  FSSpecToNativePathName()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


const
	kErrorIfFileNotFound = 1 shl 31;

{
 *  NativePathNameToFSSpec()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 3.0 and later
 }


{
 *  QTGetAliasInfo()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 *    Windows:          in qtmlClient.lib 5.0 and later
 }

{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}


end.
