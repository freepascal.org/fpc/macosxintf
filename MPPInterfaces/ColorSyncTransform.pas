{
 * ColorSync - ColorSyncTransform.h
 * Copyright (c)  2008 Apple Inc.
 * All rights reserved.
 }
{  Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ColorSyncTransform;
interface
uses MacTypes,CFBase,CFArray,CFDictionary;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


type
	ColorSyncTransformRef = ^OpaqueColorSyncTransformRef; { an opaque type }
	OpaqueColorSyncTransformRef = record end;

function ColorSyncTransformGetTypeID: CFTypeID;
   {
    * returns the CFTypeID for ColorSyncTransforms.
    }

function ColorSyncTransformCreate( profileSequence: CFArrayRef; options: CFDictionaryRef ): ColorSyncTransformRef;
   {
    *   profileSequence  - array of dictionaries, each one containing a profile object and the
    *                      information on the usage of the profile in the transform.
    *               
    *               Required keys:
    *               ==============
    *                      kColorSyncProfile           : ColorSyncProfileRef
    *                      kColorSyncRenderingIntent   : CFStringRef defining rendering intent
    *                      kColorSyncTransformTag      : CFStringRef defining which tags to use 
    *               Optional key:
    *               =============
    *                      kColorSyncBlackPointCompensation : CFBooleanRef to enable/disable BPC
    *   
    *   options      - dictionary with additional public global options (e.g. preferred CMM, quality,
    *                       etc... It can also contain custom options that are CMM specific.
    *
    *   returns ColorSyncTransformRef or NULL in case of failure
    }


function ColorSyncTransformCopyProperty( transform: ColorSyncTransformRef; key: CFTypeRef; options: CFDictionaryRef ): CFTypeRef;
   {
    *   transform - transform from which to copy the property
    *   key       - CFTypeRef to be used as a key to identify the property
    }


procedure ColorSyncTransformSetProperty( transform: ColorSyncTransformRef; key: CFTypeRef; property: CFTypeRef );
   {
    *   transform - transform in which to set the property
    *   key       - CFTypeRef to be used as a key to identify the property
    *   property  - CFTypeRef to be set as the property
    }

type
	ColorSyncDataDepth = SInt32;
const
	kColorSync1BitGamut = 1;
	kColorSync8BitInteger = 2;
	kColorSync16BitInteger = 3;
	kColorSync16BitFloat = 4;
	kColorSync32BitInteger = 5;
	kColorSync32BitNamedColorIndex = 6;
	kColorSync32BitFloat = 7;

type
	ColorSyncAlphaInfo = SInt32;
const
	kColorSyncAlphaNone = 0;               { For example, RGB. }
	kColorSyncAlphaPremultipliedLast = 1;  { For example, premultiplied RGBA }
	kColorSyncAlphaPremultipliedFirst = 2; { For example, premultiplied ARGB }
	kColorSyncAlphaLast = 3;               { For example, non-premultiplied RGBA }
	kColorSyncAlphaFirst = 4;              { For example, non-premultiplied ARGB }
	kColorSyncAlphaNoneSkipLast = 5;       { For example, RBGX. }
	kColorSyncAlphaNoneSkipFirst = 6;      { For example, XRGB. }

const
	kColorSyncAlphaInfoMask = $1F;
	kColorSyncByteOrderMask = $7000;
	kColorSyncByteOrderDefault = 0 shl 12;
	kColorSyncByteOrder16Little = 1 shl 12;
	kColorSyncByteOrder32Little = 2 shl 12;
	kColorSyncByteOrder16Big = 3 shl 12;
	kColorSyncByteOrder32Big = 4 shl 12;


type
	ColorSyncDataLayout = UInt32;

function ColorSyncTransformConvert( transform: ColorSyncTransformRef; width: size_t; height: size_t; dst: univ Ptr; dstDepth: ColorSyncDataDepth; dstLayout: ColorSyncDataLayout; dstBytesPerRow: size_t; src: {const} univ Ptr; srcDepth: ColorSyncDataDepth; srcLayout: ColorSyncDataLayout; srcBytesPerRow: size_t; options: CFDictionaryRef ): CBool;
   {
    *   transform         - transform to be used for converting color
    *   width             - width of the image in pixels
    *   height            - height of the image in pixels
    *   dst               - a pointer to the destination where the results will be written.
    *   dstDepth          - describes the bit depth and type of the destination color components
    *   dstFormat         - describes the format and byte packing of the destination pixels
    *   dstBytesPerRow    - number of bytes in the row of data
    *   src               - a pointer to the data to be converted.
    *   srcDepth          - describes the bit depth and type of the source color components
    *   srcFormat         - describes the format and byte packing of the source pixels
    *   srcBytesPerRow    - number of bytes in the row of data
    *
    *   returns true if conversion was successful or false otherwise 
    }


{ Keys and values for profile specific info and options }
const kColorSyncProfile: CFStringRef;
const kColorSyncRenderingIntent: CFStringRef;

    { Legal values for kColorSyncRenderingIntent }
const kColorSyncRenderingIntentPerceptual: CFStringRef;
const kColorSyncRenderingIntentRelative: CFStringRef;
const kColorSyncRenderingIntentSaturation: CFStringRef;
const kColorSyncRenderingIntentAbsolute: CFStringRef;
const kColorSyncRenderingIntentUseProfileHeader: CFStringRef;

const kColorSyncTransformTag: CFStringRef;

    { Legal values for kColorSyncTransformTag }
const kColorSyncTransformDeviceToPCS: CFStringRef;
const kColorSyncTransformPCSToPCS: CFStringRef;
const kColorSyncTransformPCSToDevice: CFStringRef;
const kColorSyncTransformDeviceToDevice: CFStringRef;
const kColorSyncTransformGamutCheck: CFStringRef;

const kColorSyncBlackPointCompensation: CFStringRef;

{ Global transform options }
const kColorSyncPreferredCMM: CFStringRef;       { ColorSyncCMMRef of the preferred CMM }
const kColorSyncConvertQuality: CFStringRef;

    { Legal values for kColorSyncConvertQuality }
const kColorSyncBestQuality: CFStringRef;      { do not coalesce profile transforms (default) }
const kColorSyncNormalQuality: CFStringRef;    { coalesce all transforms }
const kColorSyncDraftQuality: CFStringRef;     { coalesce all transforms, do not interpolate }

{ Conversion options }
const kColorSyncConvertThreadCount: CFStringRef; { applies to large amounts of data; 0 for number of CPUs }
const kColorSyncConvertUseVectorUnit: CFStringRef; { applies to large amounts of data; CFBooleanRef value; default true}

{ Public keys for copying transform properties }

const kColorSyncTranformInfo: CFStringRef;         { dictionary with the following keys }
const kColorSyncTransformCreator: CFStringRef; { name of the CMM that created the transform }
const kColorSyncTransformSrcSpace: CFStringRef;
const kColorSyncTransformDstSpace: CFStringRef;

{
 * =============================
 *
 *  Code Fragment Support
 *
 * =============================
 *
 * ColorSync can return parameters for standard components of color conversion,
 * i.e. tone rendering curves (TRCs), 3x4 matrices (3x3 + 3x1), multi-dimensional
 * interpolation tables and Black Point Compensation.
 * The parameters are wrapped in CFArray or CFData objects specific to the
 * component type and placed in a CFDictionary under a key that identifies the 
 * type of the component. The complete code fragment is a CFArray of component
 * dictionaries that are placed in the in the order they have to be executed.
 * 
 * A code fragment is created by calling ColorSyncTransformCopyProperty with the key 
 * specifying the type of the code fragment to be created. NULL pointer will be
 * returnde if the requested code fragment cannot be created. The code fragment
 * will be stored in the ColorSyncTransform list of properties and can be removed by 
 * by calling ColorSyncTransformSetProperty with NULL value.
 }

{
 * Types of Code Fragments:
 *
 * 1. Full conversion: all non-NULL components based on all the tags from the
 *                     sequence of profiles passed to create the ColorWorld with
 *                     an exception of adjacent matrices that can be collapsed to
 *                     one matrix.
 * 2. Parametric:      same as above, except that the returned code fragment consists
 *                     only of parametric curves, matrices and BPC components.
 * 3. Simplified:      Full conversion is collapsed to N input TRCs one
 *                     multi-dimensional table and M output TRCs.
 }
 
const kColorSyncTransformFullConversionData: CFStringRef;         { CFSTR("com.apple.cmm.FullConversion") }
const kColorSyncTransformSimplifiedConversionData: CFStringRef;   { CFSTR("com.apple.cmm.SimplifiedConversion") }
const kColorSyncTransformParametricConversionData: CFStringRef;   { CFSTR("com.apple.cmm.ParametricConversion") }

{
 * Matrix: represented as a CFArray of three CFArrays of four CFNumbers (Float32)
 *         each, performin the following matrix operation
 *         y[3] = 3x3 matrix *x[3] + 3x1 vector (last column)
 }
const kColorSyncConversionMatrix: CFStringRef;        { CFSTR("com.apple.cmm.Matrix") }

{
 * Tone Rendering Curves:
 *
 * 1. Parametric curves: represented as a CFArray of seven CFNumbers (Float32)
 *                       [gamma a b c d e f]
 *  Curve Type 0  Y = X^gamma
 *  Curve Type 1  Y = (aX+b)^gamma     [X >= -b/a],  Y = 0  [X < -b/a]
 *  Curve Type 2  Y = (aX+b)^gamma + c [X >= -b/a],  Y = c  [X < -b/a]
 *  Curve Type 3  Y = (aX+b)^gamma     [X >= d],     Y = cX [X < d]
 *  Curve Type 4  Y = (aX+b)^gamma + e [X >= d],     Y = cX + f [X < d]
 *
 * 2. 1-dimensional lookup with interpolation:
 *                  represented as CFData containing a Float32 table[gridPoints]
 }
 
const kColorSyncConversionParamCurve0: CFStringRef;   { CFSTR("com.apple.cmm.ParamCurve0") }
const kColorSyncConversionParamCurve1: CFStringRef;   { CFSTR("com.apple.cmm.ParamCurve1") }
const kColorSyncConversionParamCurve2: CFStringRef;   { CFSTR("com.apple.cmm.ParamCurve2") }
const kColorSyncConversionParamCurve3: CFStringRef;   { CFSTR("com.apple.cmm.ParamCurve3") }
const kColorSyncConversionParamCurve4: CFStringRef;   { CFSTR("com.apple.cmm.ParamCurve4") }
const kColorSyncConversion1DLut: CFStringRef;         { CFSTR("com.apple.cmm.1D-LUT") }
const kColorSyncConversionGridPoints: CFStringRef;    { CFSTR("com.apple.cmm.GridPointCount") }
const kColorSyncConversionChannelID: CFStringRef;     { CFSTR("com.apple.cmm.ChannelID") }

{
 * Multi-dimensional lookup with interpolation:
 *
 *       represented as CFData containing a Float32 table for N inputs, M outputs 
 *       and P gridPoints in each direction. The dimensioncorresponding to the
 *       first input channel varies least rapidly, and the dimension corresponding
 *       to the last input channel varies most rapidly. Each grid point value contains
 *       M Float32 numbers, one for each of output channels (M is the number of outputs).
 }
 

const kColorSyncConversion3DLut: CFStringRef;      { CFSTR("com.apple.cmm.3D-LUT") }
const kColorSyncConversionInpChan: CFStringRef;    { CFSTR("com.apple.cmm.InputChannels") }
const kColorSyncConversionOutChan: CFStringRef;    { CFSTR("com.apple.cmm.OutputChannels") }

{
 * Black Point Compensation: represented as an CFArray of CFNumbers (Float32)
 *
 * 1. Scaling in Luminance: CFArray containing two numbers.
 * 2. Scaling in XYZ: CFArray containing six numbers.
 }
 
const kColorSyncConversionBPC: CFStringRef;   { CFSTR("com.apple.cmm.BPC") }

{$endc} {TARGET_OS_MAC}

end.
