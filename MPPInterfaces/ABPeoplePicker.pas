{
//
//  ABPeoplePickerC.h
//  AddressBook Framework
//
//  Copyright (c) 2003-2007 Apple Inc.  All rights reserved.
//
}
{	  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, 2004 }
{	  Pascal Translation Updated:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, November 2009 }

unit ABPeoplePicker;
interface
uses MacTypes,ABAddressBook,CFBase,CFArray,CGGeometry,HIGeometry,CarbonEventsCore;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


type
	ABPickerRef = ^OpaqueABPicker; { an opaque type }
	OpaqueABPicker = record end;

{
 * Picker creation and manipulation
 }

// Creates an ABPickerRef. Release with CFRelease(). The window is created hidden. Call
// ABPickerSetVisibility() to show it.
function ABPickerCreate: ABPickerRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

// Change the structural frame of the window.
procedure ABPickerSetFrame( inPicker: ABPickerRef; const var inFrame: HIRect );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
procedure ABPickerGetFrame( inPicker: ABPickerRef; var outFrame: HIRect );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

procedure ABPickerSetVisibility( inPicker: ABPickerRef; visible: CBool );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function ABPickerIsVisible( inPicker: ABPickerRef ): CBool;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

{
 * Look and Feel
 }

const
// Choose the selection behavior for the value column. If multiple behaviors are selected,
    // the most restrictive behavior will be used. Defaults to kABPickerSingleValueSelection set
    // to TRUE.
	kABPickerSingleValueSelection = 1 shl 0; // Allow user to choose a single value for a person.
	kABPickerMultipleValueSelection = 1 shl 1; // Allow user to choose multiple values for a person.

    // Allow the user to select entire groups in the group column. If false, at least one
    // person in the group will be selected. Defaults to FALSE.
	kABPickerAllowGroupSelection = 1 shl 2;

    // Allow the user to select more than one group/record at a time. Defaults to TRUE.
	kABPickerAllowMultipleSelection = 1 shl 3;

type
	ABPickerAttributes = OptionBits;

function ABPickerGetAttributes( inPicker: ABPickerRef ): ABPickerAttributes;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
procedure ABPickerChangeAttributes( inPicker: ABPickerRef; inAttributesToSet: ABPickerAttributes; inAttributesToClear: ABPickerAttributes );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

{
 * Value column
 }

    // These methods control what data (if any) is shown in the values column. The column will only
    // display if an AB property is added. A popup button in the column header will be used if more
    // than one property is added. Titles for built in properties will localized automatically. A
    // list of AB properties can be found in <AddressBook/ABGlobals.h>.
procedure ABPickerAddProperty( inPicker: ABPickerRef; inProperty: CFStringRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
procedure ABPickerRemoveProperty( inPicker: ABPickerRef; inProperty: CFStringRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
    // Returns an array of AB Properties as CFStringRefs.
function ABPickerCopyProperties( inPicker: ABPickerRef ): CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // Localized titles for third party properties should be set with these methods.
procedure ABPickerSetColumnTitle( inPicker: ABPickerRef; inTitle: CFStringRef; inProperty: CFStringRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function ABPickerCopyColumnTitle( inPicker: ABPickerRef; inProperty: CFStringRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // Display one of the properties added above in the values column.
procedure ABPickerSetDisplayedProperty( inPicker: ABPickerRef; inProperty: CFStringRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function ABPickerCopyDisplayedProperty( inPicker: ABPickerRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

{
 * Selection
 }

    // Returns group column selection as an array of ABGroupRef objects.
function ABPickerCopySelectedGroups( inPicker: ABPickerRef ): CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // Returns names column selection as an array of ABGroupRef or ABPersonRef objects.
function ABPickerCopySelectedRecords( inPicker: ABPickerRef ): CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // This method returns an array of selected multi-value identifiers. Returns nil if the displayed
    // property is a single value type.
function ABPickerCopySelectedIdentifiers( inPicker: ABPickerRef; inPerson: ABPersonRef ): CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // Returns an array containing CFStringRefs for each item selected in the values column.
function ABPickerCopySelectedValues( inPicker: ABPickerRef ): CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // Select group/name/value programatically.
procedure ABPickerSelectGroup( inPicker: ABPickerRef; inGroup: ABGroupRef; inExtendSelection: CBool );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
procedure ABPickerSelectRecord( inPicker: ABPickerRef; inRecord: ABRecordRef; inExtendSelection: CBool );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
    // Individual values contained within an multi-value property can be selected with this method.
procedure ABPickerSelectIdentifier( inPicker: ABPickerRef; inPerson: ABPersonRef; inIdentifier: CFStringRef; inExtendSelection: CBool );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // Remove selection
procedure ABPickerDeselectGroup( inPicker: ABPickerRef; inGroup: ABGroupRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
procedure ABPickerDeselectRecord( inPicker: ABPickerRef; inRecord: ABRecordRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
procedure ABPickerDeselectIdentifier( inPicker: ABPickerRef; inPerson: ABPersonRef; inIdentifier: CFStringRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

procedure ABPickerDeselectAll( inPicker: ABPickerRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

{
 * Events and Actions
 *
 * Your delegate will be notified when the user changes the selection or displayed property of the picker.
 * Picker events have an event class of kEventClassABPeoplePicker and one of the kinds listed below. Picker
 * events contain an event parameter which contains the ABPickerRef. To obtain this:
 *
 * GetEventParameter(inEvent, kEventParamABPickerRef,
 *                   typeCFTypeRef, NULL, sizeof(ABPickerRef),
 *                   NULL, &outPickerRef);
 *
 }

const
// Carbon Event class for People Picker
	kEventClassABPeoplePicker = FOUR_CHAR_CODE('abpp');

const
    // Carbon Event kinds for People Picker
    kEventABPeoplePickerGroupSelectionChanged     = 1;
    kEventABPeoplePickerNameSelectionChanged      = 2;
    kEventABPeoplePickerValueSelectionChanged     = 3;
    kEventABPeoplePickerDisplayedPropertyChanged  = 4;

    kEventABPeoplePickerGroupDoubleClicked        = 5;
    kEventABPeoplePickerNameDoubleClicked         = 6;

const
// Carbon Event parameter name
	kEventParamABPickerRef = FOUR_CHAR_CODE('abpp');

    // Set the event handler for People Picker events.
procedure ABPickerSetDelegate( inPicker: ABPickerRef; inDelegate: EventTargetRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function ABPickerGetDelegate( inPicker: ABPickerRef ): EventTargetRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // Clear the search field and reset the list of displayed names.
procedure ABPickerClearSearchField( inPicker: ABPickerRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // Launch AddressBook and edit the current selection
procedure ABPickerEditInAddressBook( inPicker: ABPickerRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
procedure ABPickerSelectInAddressBook( inPicker: ABPickerRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

{$endc} {TARGET_OS_MAC}

end.
