{
     File:       QD/Displays.h
 
     Contains:   Display Manager Interfaces.
 
     Version:    Quickdraw-262~1
 
     Copyright:  � 1993-2008 by Apple Inc. all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{   Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
unit Displays;
interface
uses MacTypes,QuickdrawTypes,ColorSyncDeprecated,AEDataModel,ConditionalMacros,Components,Video,AppleEvents,Events,Processes,Dialogs;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{******************* DEPRECATION NOTICE *********************
 *
 * The DisplayMgr API is being deprecated, and should be replaced
 * by the CGDirectDisplay API in the CoreGraphics framework in 
 * ApplicationServices.framework.
 *
 ************************************************************}

type
	DMProcessInfoPtr = UnivPtr;
{GPC-ONLY-START}
	DMModalFilterUPP = UniversalProcPtr; // should be UnivPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	DMModalFilterUPP = UnivPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	DMModalFilterUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
const
{ AppleEvents Core Suite }
	kAESystemConfigNotice = FOUR_CHAR_CODE('cnfg'); { Core Suite types }
	kAEDisplayNotice = FOUR_CHAR_CODE('dspl');
	kAEDisplaySummary = FOUR_CHAR_CODE('dsum');
	keyDMConfigVersion = FOUR_CHAR_CODE('dmcv');
	keyDMConfigFlags = FOUR_CHAR_CODE('dmcf');
	keyDMConfigReserved = FOUR_CHAR_CODE('dmcr');
	keyDisplayID = FOUR_CHAR_CODE('dmid');
	keyDisplayComponent = FOUR_CHAR_CODE('dmdc');
	keyDisplayDevice = FOUR_CHAR_CODE('dmdd');
	keyDisplayFlags = FOUR_CHAR_CODE('dmdf');
	keyDisplayMode = FOUR_CHAR_CODE('dmdm');
	keyDisplayModeReserved = FOUR_CHAR_CODE('dmmr');
	keyDisplayReserved = FOUR_CHAR_CODE('dmdr');
	keyDisplayMirroredId = FOUR_CHAR_CODE('dmmi');
	keyDeviceFlags = FOUR_CHAR_CODE('dddf');
	keyDeviceDepthMode = FOUR_CHAR_CODE('dddm');
	keyDeviceRect = FOUR_CHAR_CODE('dddr');
	keyPixMapRect = FOUR_CHAR_CODE('dpdr');
	keyPixMapHResolution = FOUR_CHAR_CODE('dphr');
	keyPixMapVResolution = FOUR_CHAR_CODE('dpvr');
	keyPixMapPixelType = FOUR_CHAR_CODE('dppt');
	keyPixMapPixelSize = FOUR_CHAR_CODE('dpps');
	keyPixMapCmpCount = FOUR_CHAR_CODE('dpcc');
	keyPixMapCmpSize = FOUR_CHAR_CODE('dpcs');
	keyPixMapAlignment = FOUR_CHAR_CODE('dppa');
	keyPixMapResReserved = FOUR_CHAR_CODE('dprr');
	keyPixMapReserved = FOUR_CHAR_CODE('dppr');
	keyPixMapColorTableSeed = FOUR_CHAR_CODE('dpct');
	keySummaryMenubar = FOUR_CHAR_CODE('dsmb');
	keySummaryChanges = FOUR_CHAR_CODE('dsch');
	keyDisplayOldConfig = FOUR_CHAR_CODE('dold');
	keyDisplayNewConfig = FOUR_CHAR_CODE('dnew');

const
	dmOnlyActiveDisplays = true;
	dmAllDisplays = false;


const
{ DMSendDependentNotification notifyClass }
	kDependentNotifyClassShowCursor = FOUR_CHAR_CODE('shcr'); { When display mgr shows a hidden cursor during an unmirror }
	kDependentNotifyClassDriverOverride = FOUR_CHAR_CODE('ndrv'); { When a driver is overridden }
	kDependentNotifyClassDisplayMgrOverride = FOUR_CHAR_CODE('dmgr'); { When display manager is upgraded }
	kDependentNotifyClassProfileChanged = FOUR_CHAR_CODE('prof'); { When DMSetProfileByAVID is called }


const
{ Switch Flags }
	kNoSwitchConfirmBit = 0;    { Flag indicating that there is no need to confirm a switch to this mode }
	kDepthNotAvailableBit = 1;    { Current depth not available in new mode }
	kShowModeBit = 3;    { Show this mode even though it requires a confirm. }
	kModeNotResizeBit = 4;    { Do not use this mode to resize display (for cards that mode drives a different connector). }
	kNeverShowModeBit = 5;     { This mode should not be shown in the user interface. }

{    Summary Change Flags (sticky bits indicating an operation was performed)
    For example, moving a display then moving it back will still set the kMovedDisplayBit.
}
const
	kBeginEndConfigureBit = 0;
	kMovedDisplayBit = 1;
	kSetMainDisplayBit = 2;
	kSetDisplayModeBit = 3;
	kAddDisplayBit = 4;
	kRemoveDisplayBit = 5;
	kNewDisplayBit = 6;
	kDisposeDisplayBit = 7;
	kEnabledDisplayBit = 8;
	kDisabledDisplayBit = 9;
	kMirrorDisplayBit = 10;
	kUnMirrorDisplayBit = 11;


const
{ Notification Messages for extended call back routines }
	kDMNotifyRequestConnectionProbe = 0;  { Like kDMNotifyRequestDisplayProbe only not for smart displays (used in wake before all busses are awake) }
	kDMNotifyInstalled = 1;    { At install time }
	kDMNotifyEvent = 2;    { Post change time }
	kDMNotifyRemoved = 3;    { At remove time }
	kDMNotifyPrep = 4;    { Pre change time }
	kDMNotifyExtendEvent = 5;    { Allow registrees to extend apple event before it is sent }
	kDMNotifyDependents = 6;    { Minor notification check without full update }
	kDMNotifySuspendConfigure = 7;    { Temporary end of configuration }
	kDMNotifyResumeConfigure = 8;    { Resume configuration }
	kDMNotifyRequestDisplayProbe = 9;    { Request smart displays re-probe (used in sleep and hot plugging) }
	kDMNotifyDisplayWillSleep = 10;   { Mac OS X only }
	kDMNotifyDisplayDidWake = 11;   { Mac OS X only }
                                        { Notification Flags }
	kExtendedNotificationProc = 1 shl 16;


{ types for notifyType }
const
	kFullNotify = 0;    { This is the appleevent whole nine yards notify }
	kFullDependencyNotify = 1;     { Only sends to those who want to know about interrelated functionality (used for updating UI) }

{ DisplayID/DeviceID constants }
const
	kDummyDeviceID = $00FF; { This is the ID of the dummy display, used when the last �real� display is disabled.}
	kInvalidDisplayID = $0000; { This is the invalid ID}
	kFirstDisplayID = $0100;

const
{ bits for panelListFlags }
	kAllowDuplicatesBit = 0;

const
{ bits for nameFlags }
	kSuppressNumberBit = 0;
	kSuppressNumberMask = 1;
	kForceNumberBit = 1;
	kForceNumberMask = 2;
	kSuppressNameBit = 2;
	kSuppressNameMask = 4;

{ DMGetNameByAVID masks}
const
	kDMSupressNumbersMask = 1 shl 0; { Supress the numbers and return only names}
	kDMForceNumbersMask = 1 shl 1; { Force numbers to always be shown (even on single display configs)}
	kDMSupressNameMask = 1 shl 2; { Supress the names and return only numbers.}


{ Constants for fidelity checks }
const
	kNoFidelity = 0;
	kMinimumFidelity = 1;
	kDefaultFidelity = 500;  { I'm just picking a number for Apple default panels and engines}
	kDefaultManufacturerFidelity = 1000;  { I'm just picking a number for Manufacturer's panels and engines (overrides apple defaults)}

const
	kAnyPanelType = 0;    { Pass to DMNewEngineList for list of all panels (as opposed to specific types)}
	kAnyEngineType = 0;    { Pass to DMNewEngineList for list of all engines}
	kAnyDeviceType = 0;    { Pass to DMNewDeviceList for list of all devices}
	kAnyPortType = 0;     { Pass to DMNewDevicePortList for list of all devices}

{ portListFlags for DM_NewDevicePortList }
const
{ Should offline devices be put into the port list (such as dummy display) }
	kPLIncludeOfflineDevicesBit = 0;


{ confirmFlags for DMConfirmConfiguration }
const
	kForceConfirmBit = 0;    { Force a confirm dialog }
	kForceConfirmMask = 1 shl kForceConfirmBit;


{ Flags for displayModeFlags }
const
	kDisplayModeListNotPreferredBit = 0;
	kDisplayModeListNotPreferredMask = 1 shl kDisplayModeListNotPreferredBit;


{ Flags for itemFlags }
const
	kComponentListNotPreferredBit = 0;
	kComponentListNotPreferredMask = 1 shl kComponentListNotPreferredBit;

const
	kDisplayTimingInfoVersionZero = 1;
	kDisplayTimingInfoReservedCountVersionZero = 16;
	kDisplayModeEntryVersionZero = 0;    { displayModeVersion - original version}
	kDisplayModeEntryVersionOne = 1;     { displayModeVersion - added displayModeOverrideInfo}


const
	kMakeAndModelReservedCount = 4;     { Number of reserved fields}


{ Display Gestalt for DMDisplayGestalt}
const
	kDisplayGestaltDisplayCommunicationAttr = FOUR_CHAR_CODE('comm');
	kDisplayGestaltForbidI2CMask = 1 shl 0; { Some displays have firmware problems if they get I2C communication.  If this bit is set, then I2C communication is forbidden}
	kDisplayGestaltUseI2CPowerMask = 1 shl 1; { Some displays require I2C power settings (most use DPMS).}
	kDisplayGestaltCalibratorAttr = FOUR_CHAR_CODE('cali');
	kDisplayGestaltBrightnessAffectsGammaMask = 1 shl 0; { Used by default calibrator (should we show brightness panel) }
	kDisplayGestaltViewAngleAffectsGammaMask = 1 shl 1; { Currently not used by color sync}


type
	DMFidelityType = UInt32;
{
   AVID is an ID for ports and devices the old DisplayID type
    is carried on for compatibility
}


type
	DMListType = UnivPtr;
	DMListIndexType = UInt32;
	AVPowerStateRec = VDPowerStateRec;
	AVPowerStateRecPtr = ^AVPowerStateRec;
	AVPowerStatePtr = VDPowerStateRecPtr;
	DMDisplayTimingInfoRecPtr = ^DMDisplayTimingInfoRec;
	DMDisplayTimingInfoRec = record
		timingInfoVersion: UInt32;
		timingInfoAttributes: UInt32;   { Flags }
		timingInfoRelativeQuality: SInt32; { quality of the timing }
		timingInfoRelativeDefault: SInt32; { relative default of the timing }

		timingInfoReserved: array [0..15] of UInt32;  
	end;
type
	DMDisplayTimingInfoPtr = ^DMDisplayTimingInfoRec;

type
	DMComponentListEntryRecPtr = ^DMComponentListEntryRec;
	DMComponentListEntryRec = record
		itemID: DisplayIDType;                 { DisplayID Manager}
		itemComponent: Component;          { Component Manager}
		itemDescription: ComponentDescription;      { We can always construct this if we use something beyond the compontent mgr.}

		itemClass: ResType;              { Class of group to put this panel (eg geometry/color/etc for panels, brightness/contrast for engines, video out/sound/etc for devices)}
		itemFidelity: DMFidelityType;           { How good is this item for the specified search?}
		itemSubClass: ResType;           { Subclass of group to put this panel.  Can use to do sub-grouping (eg volume for volume panel and mute panel)}
		itemSort: Point;               { Set to 0 - future to sort the items in a sub group.}

		itemFlags: UInt32;              { Set to 0 (future expansion)}
		itemReserved: ResType;           { What kind of code does the itemReference point to  (right now - kPanelEntryTypeComponentMgr only)}
		itemFuture1: UInt32;            { Set to 0 (future expansion - probably an alternate code style)}
		itemFuture2: UInt32;            { Set to 0 (future expansion - probably an alternate code style)}
		itemFuture3: UInt32;            { Set to 0 (future expansion - probably an alternate code style)}
		itemFuture4: UInt32;            { Set to 0 (future expansion - probably an alternate code style)}
	end;
type
	DMComponentListEntryPtr = DMComponentListEntryRecPtr;
{ ��� Move AVLocationRec to AVComponents.i AFTER AVComponents.i is created}
type
	AVLocationRecPtr = ^AVLocationRec;
	AVLocationRec = record
		locationConstant: UInt32;       { Set to 0 (future expansion - probably an alternate code style)}
	end;
type
	AVLocationPtr = AVLocationRecPtr;
	DMDepthInfoRecPtr = ^DMDepthInfoRec;
	DMDepthInfoRec = record
		depthSwitchInfo: VDSwitchInfoPtr;        { This is the switch mode to choose this timing/depth }
		depthVPBlock: VPBlockPtr;           { VPBlock (including size, depth and format) }
		depthFlags: UInt32;             { VDVideoParametersInfoRec.csDepthFlags  }
		depthReserved1: UInt32;         { Reserved }
		depthReserved2: UInt32;         { Reserved }
	end;
type
	DMDepthInfoPtr = DMDepthInfoRecPtr;
	DMDepthInfoBlockRecPtr = ^DMDepthInfoBlockRec;
	DMDepthInfoBlockRec = record
		depthBlockCount: UInt32;        { How many depths are there? }
		depthVPBlock: DMDepthInfoPtr;           { Array of DMDepthInfoRec }
		depthBlockFlags: UInt32;        { Reserved }
		depthBlockReserved1: UInt32;    { Reserved }
		depthBlockReserved2: UInt32;    { Reserved }
	end;
type
	DMDepthInfoBlockPtr = DMDepthInfoBlockRecPtr;
	DMDisplayModeListEntryRecPtr = ^DMDisplayModeListEntryRec;
	DMDisplayModeListEntryRec = record
		displayModeFlags: UInt32;
		displayModeSwitchInfo: VDSwitchInfoPtr;
		displayModeResolutionInfo: VDResolutionInfoPtr;
		displayModeTimingInfo: VDTimingInfoPtr;
		displayModeDepthBlockInfo: DMDepthInfoBlockPtr; { Information about all the depths}
		displayModeVersion: UInt32;     { What version is this record (now kDisplayModeEntryVersionOne)}
		displayModeName: StringPtr;        { Name of the timing mode}
		displayModeDisplayInfo: DMDisplayTimingInfoPtr; { Information from the display.}
	end;
type
	DMDisplayModeListEntryPtr = DMDisplayModeListEntryRecPtr;

type
	DependentNotifyRecPtr = ^DependentNotifyRec;
	DependentNotifyRec = record
		notifyType: ResType;             { What type was the engine that made the change (may be zero)}
		notifyClass: ResType;            { What class was the change (eg geometry, color etc)}
		notifyPortID: DisplayIDType;           { Which device was touched (kInvalidDisplayID -> all or none)}
		notifyComponent: ComponentInstance;        { What engine did it (may be 0)?}

		notifyVersion: UInt32;          { Set to 0 (future expansion)}
		notifyFlags: UInt32;            { Set to 0 (future expansion)}
		notifyReserved: UInt32;         { Set to 0 (future expansion)}
		notifyFuture: UInt32;           { Set to 0 (future expansion)}
	end;
type
	DependentNotifyPtr = DependentNotifyRecPtr;

type
	DMMakeAndModelRecPtr = ^DMMakeAndModelRec;
	DMMakeAndModelRec = record
		manufacturer: ResType;
		model: UInt32;
		serialNumber: UInt32;
		manufactureDate: UInt32;

		makeReserved: array [0..3] of UInt32;
	end;
type
	DMMakeAndModelPtr = DMMakeAndModelRecPtr;
{ DMNewDisplayList displayListIncludeFlags}
const
	kIncludeOnlineActiveDisplaysMask = 1 shl 0;
	kIncludeOnlineDisabledDisplaysMask = 1 shl 1;
	kIncludeOfflineDisplaysMask = 1 shl 2;
	kIncludeOfflineDummyDisplaysMask = 1 shl 3;
	kIncludeHardwareMirroredDisplaysMask = 1 shl 4;


const
{ modeListFlags for DMNewDisplayModeList }
	kDMModeListIncludeAllModesMask = 1 shl 0; { Include all timing modes not _explicitly_ excluded (see other bits)}
	kDMModeListIncludeOfflineModesMask = 1 shl 1;
	kDMModeListExcludeDriverModesMask = 1 shl 2; { Exclude old-style timing modes (cscGetNextResolution/kDisplayModeIDFindFirstResolution modes)}
	kDMModeListExcludeDisplayModesMask = 1 shl 3; { Exclude timing modes that come from the display (always arbritrary timing modes)}
	kDMModeListExcludeCustomModesMask = 1 shl 4; { Exclude custom modes that came neither from the driver or display (need a better name)}
	kDMModeListPreferStretchedModesMask = 1 shl 5; { Prefer modes that are stretched over modes that are letterboxed when setting kDisplayModeListNotPreferredBit}
	kDMModeListPreferSafeModesMask = 1 shl 6; { Prefer modes that are safe over modes that are not when setting kDisplayModeListNotPreferredBit}


{ DMNewDisplayList displayListFlags}
type
	DisplayListEntryRecPtr = ^DisplayListEntryRec;
	DisplayListEntryRec = record
		displayListEntryGDevice: GDHandle;
		displayListEntryDisplayID: DisplayIDType;
		displayListEntryIncludeFlags: UInt32; { Reason this entry was included}
		displayListEntryReserved1: UInt32;

		displayListEntryReserved2: UInt32; { Zero}
		displayListEntryReserved3: UInt32; { Zero}
		displayListEntryReserved4: UInt32; { Zero}
		displayListEntryReserved5: UInt32; { Zero}
	end;
type
	DisplayListEntryPtr = DisplayListEntryRecPtr;
	DMProfileListEntryRecPtr = ^DMProfileListEntryRec;
	DMProfileListEntryRec = record
		profileRef: UnivPtr;             { was CMProfileRef}
		profileReserved1: Ptr;       { Reserved}
		profileReserved2: Ptr;       { Reserved}
		profileReserved3: Ptr;       { Reserved}
	end;
type
	DMProfileListEntryPtr = DMProfileListEntryRecPtr;
	DMNotificationProcPtr = procedure( var theEvent: AppleEvent );
	DMExtendedNotificationProcPtr = procedure( userData: univ Ptr; theMessage: SInt16; notifyData: univ Ptr );
	DMComponentListIteratorProcPtr = procedure( userData: univ Ptr; itemIndex: DMListIndexType; componentInfo: DMComponentListEntryPtr );
	DMDisplayModeListIteratorProcPtr = procedure( userData: univ Ptr; itemIndex: DMListIndexType; displaymodeInfo: DMDisplayModeListEntryPtr );
	DMProfileListIteratorProcPtr = procedure( userData: univ Ptr; itemIndex: DMListIndexType; profileInfo: DMProfileListEntryPtr );
	DMDisplayListIteratorProcPtr = procedure( userData: univ Ptr; itemIndex: DMListIndexType; displaymodeInfo: DisplayListEntryPtr );
{GPC-ONLY-START}
	DMNotificationUPP = UniversalProcPtr; // should be DMNotificationProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	DMNotificationUPP = DMNotificationProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	DMNotificationUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	DMExtendedNotificationUPP = UniversalProcPtr; // should be DMExtendedNotificationProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	DMExtendedNotificationUPP = DMExtendedNotificationProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	DMExtendedNotificationUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	DMComponentListIteratorUPP = UniversalProcPtr; // should be DMComponentListIteratorProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	DMComponentListIteratorUPP = DMComponentListIteratorProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	DMComponentListIteratorUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	DMDisplayModeListIteratorUPP = UniversalProcPtr; // should be DMDisplayModeListIteratorProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	DMDisplayModeListIteratorUPP = DMDisplayModeListIteratorProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	DMDisplayModeListIteratorUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	DMProfileListIteratorUPP = UniversalProcPtr; // should be DMProfileListIteratorProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	DMProfileListIteratorUPP = DMProfileListIteratorProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	DMProfileListIteratorUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	DMDisplayListIteratorUPP = UniversalProcPtr; // should be DMDisplayListIteratorProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	DMDisplayListIteratorUPP = DMDisplayListIteratorProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	DMDisplayListIteratorUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{
 *  NewDMNotificationUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewDMNotificationUPP( userRoutine: DMNotificationProcPtr ): DMNotificationUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewDMExtendedNotificationUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewDMExtendedNotificationUPP( userRoutine: DMExtendedNotificationProcPtr ): DMExtendedNotificationUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewDMComponentListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewDMComponentListIteratorUPP( userRoutine: DMComponentListIteratorProcPtr ): DMComponentListIteratorUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewDMDisplayModeListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewDMDisplayModeListIteratorUPP( userRoutine: DMDisplayModeListIteratorProcPtr ): DMDisplayModeListIteratorUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewDMProfileListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewDMProfileListIteratorUPP( userRoutine: DMProfileListIteratorProcPtr ): DMProfileListIteratorUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewDMDisplayListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewDMDisplayListIteratorUPP( userRoutine: DMDisplayListIteratorProcPtr ): DMDisplayListIteratorUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeDMNotificationUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeDMNotificationUPP( userUPP: DMNotificationUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeDMExtendedNotificationUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeDMExtendedNotificationUPP( userUPP: DMExtendedNotificationUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeDMComponentListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeDMComponentListIteratorUPP( userUPP: DMComponentListIteratorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeDMDisplayModeListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeDMDisplayModeListIteratorUPP( userUPP: DMDisplayModeListIteratorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeDMProfileListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeDMProfileListIteratorUPP( userUPP: DMProfileListIteratorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeDMDisplayListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeDMDisplayListIteratorUPP( userUPP: DMDisplayListIteratorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeDMNotificationUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeDMNotificationUPP( var theEvent: AppleEvent; userUPP: DMNotificationUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeDMExtendedNotificationUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeDMExtendedNotificationUPP( userData: univ Ptr; theMessage: SInt16; notifyData: univ Ptr; userUPP: DMExtendedNotificationUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeDMComponentListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeDMComponentListIteratorUPP( userData: univ Ptr; itemIndex: DMListIndexType; componentInfo: DMComponentListEntryPtr; userUPP: DMComponentListIteratorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeDMDisplayModeListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeDMDisplayModeListIteratorUPP( userData: univ Ptr; itemIndex: DMListIndexType; displaymodeInfo: DMDisplayModeListEntryPtr; userUPP: DMDisplayModeListIteratorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeDMProfileListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeDMProfileListIteratorUPP( userData: univ Ptr; itemIndex: DMListIndexType; profileInfo: DMProfileListEntryPtr; userUPP: DMProfileListIteratorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeDMDisplayListIteratorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeDMDisplayListIteratorUPP( userData: univ Ptr; itemIndex: DMListIndexType; displaymodeInfo: DisplayListEntryPtr; userUPP: DMDisplayListIteratorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$ifc not TARGET_CPU_64}
{
 *  DMDisplayGestalt()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMUseScreenPrefs()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMSuspendConfigure()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMResumeConfigure()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMSetGammaByAVID()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMGetGammaByAVID()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMGetMakeAndModelByAVID()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMNewDisplayList()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMGetIndexedDisplayFromList()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMNewProfileListByAVID()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMGetIndexedProfileFromList()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }


{
 *  DMGetFirstScreenDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMGetFirstScreenDevice( activeOnly: Boolean ): GDHandle;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetNextScreenDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMGetNextScreenDevice( theDevice: GDHandle; activeOnly: Boolean ): GDHandle;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMDrawDesktopRect()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure DMDrawDesktopRect( var globalRect: Rect );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMDrawDesktopRegion()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure DMDrawDesktopRegion( globalRgn: RgnHandle );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMBeginConfigureDisplays()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMBeginConfigureDisplays( var displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMEndConfigureDisplays()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMEndConfigureDisplays( displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMAddDisplay()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMAddDisplay( newDevice: GDHandle; driver: SInt16; mode: UInt32; reserved: UInt32; displayID: UInt32; displayComponent: Component; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMMoveDisplay()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMMoveDisplay( moveDevice: GDHandle; x: SInt16; y: SInt16; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMDisableDisplay()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMDisableDisplay( disableDevice: GDHandle; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMEnableDisplay()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMEnableDisplay( enableDevice: GDHandle; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMRemoveDisplay()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMRemoveDisplay( removeDevice: GDHandle; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMSetMainDisplay()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMSetMainDisplay( newMainDevice: GDHandle; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMSetDisplayMode()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMSetDisplayMode( theDevice: GDHandle; mode: UInt32; var depthMode: UInt32; reserved: SIGNEDLONG; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMCheckDisplayMode()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMCheckDisplayMode( theDevice: GDHandle; mode: UInt32; depthMode: UInt32; var switchFlags: UInt32; reserved: UInt32; var modeOk: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetDeskRegion()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMGetDeskRegion( var desktopRegion: RgnHandle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMRegisterNotifyProc()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMRegisterNotifyProc( notificationProc: DMNotificationUPP; whichPSN: DMProcessInfoPtr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMRemoveNotifyProc()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMRemoveNotifyProc( notificationProc: DMNotificationUPP; whichPSN: DMProcessInfoPtr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMQDIsMirroringCapable()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMQDIsMirroringCapable( var qdIsMirroringCapable: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMCanMirrorNow()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMCanMirrorNow( var canMirrorNow: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMIsMirroringOn()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMIsMirroringOn( var isMirroringOn: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMMirrorDevices()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMMirrorDevices( gD1: GDHandle; gD2: GDHandle; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMUnmirrorDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMUnmirrorDevice( gDevice: GDHandle; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetNextMirroredDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMGetNextMirroredDevice( gDevice: GDHandle; var mirroredDevice: GDHandle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMBlockMirroring()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMBlockMirroring: OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMUnblockMirroring()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMUnblockMirroring: OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetDisplayMgrA5World()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }


{
 *  DMGetDisplayIDByGDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMGetDisplayIDByGDevice( displayDevice: GDHandle; var displayID: DisplayIDType; failToMain: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetGDeviceByDisplayID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMGetGDeviceByDisplayID( displayID: DisplayIDType; var displayDevice: GDHandle; failToMain: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMSetDisplayComponent()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMSetDisplayComponent( theDevice: GDHandle; displayComponent: Component ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetDisplayComponent()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMGetDisplayComponent( theDevice: GDHandle; var displayComponent: Component ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMNewDisplay()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMNewDisplay( var newDevice: GDHandle; driverRefNum: SInt16; mode: UInt32; reserved: UInt32; displayID: DisplayIDType; displayComponent: Component; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMDisposeDisplay()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.5 and later
 }
function DMDisposeDisplay( disposeDevice: GDHandle; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMResolveDisplayComponents()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DMResolveDisplayComponents: OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMRegisterExtendedNotifyProc()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib 2.0 and later
 }
function DMRegisterExtendedNotifyProc( notifyProc: DMExtendedNotificationUPP; notifyUserData: univ Ptr; nofifyOnFlags: UInt16; whichPSN: DMProcessInfoPtr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMRemoveExtendedNotifyProc()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib 2.0 and later
 }
function DMRemoveExtendedNotifyProc( notifyProc: DMExtendedNotificationUPP; notifyUserData: univ Ptr; whichPSN: DMProcessInfoPtr; removeFlags: UInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMNewAVPanelList()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMNewAVPanelList( displayID: DisplayIDType; panelType: ResType; minimumFidelity: DMFidelityType; panelListFlags: UInt32; reserved: UInt32; var thePanelCount: DMListIndexType; var thePanelList: DMListType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMNewAVEngineList()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMNewAVEngineList( displayID: DisplayIDType; engineType: ResType; minimumFidelity: DMFidelityType; engineListFlags: UInt32; reserved: UInt32; var engineCount: DMListIndexType; var engineList: DMListType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMNewAVDeviceList()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMNewAVDeviceList( deviceType: ResType; deviceListFlags: UInt32; reserved: UInt32; var deviceCount: DMListIndexType; var deviceList: DMListType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMNewAVPortListByPortType()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMNewAVPortListByPortType( subType: ResType; portListFlags: UInt32; reserved: UInt32; var devicePortCount: DMListIndexType; var theDevicePortList: DMListType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetIndexedComponentFromList()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetIndexedComponentFromList( panelList: DMListType; itemIndex: DMListIndexType; reserved: UInt32; listIterator: DMComponentListIteratorUPP; userData: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMDisposeList()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib 2.0 and later
 }
function DMDisposeList( panelList: DMListType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetNameByAVID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetNameByAVID( theID: AVIDType; nameFlags: UInt32; var name: Str255 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMNewAVIDByPortComponent()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMNewAVIDByPortComponent( thePortComponent: Component; portKind: ResType; reserved: UInt32; var newID: AVIDType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetPortComponentByAVID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetPortComponentByAVID( thePortID: DisplayIDType; var thePortComponent: Component; var theDesciption: ComponentDescription; var thePortKind: ResType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMSendDependentNotification()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib 2.0 and later
 }
function DMSendDependentNotification( notifyType: ResType; notifyClass: ResType; displayID: AVIDType; notifyComponent: ComponentInstance ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMDisposeAVComponent()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMDisposeAVComponent( theAVComponent: Component ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMSaveScreenPrefs()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMSaveScreenPrefs( reserved1: UInt32; saveFlags: UInt32; reserved2: UInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMNewAVIDByDeviceComponent()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMNewAVIDByDeviceComponent( theDeviceComponent: Component; portKind: ResType; reserved: UInt32; var newID: DisplayIDType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMNewAVPortListByDeviceAVID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMNewAVPortListByDeviceAVID( theID: AVIDType; minimumFidelity: DMFidelityType; portListFlags: UInt32; reserved: UInt32; var devicePortCount: DMListIndexType; var theDevicePortList: DMListType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetDeviceComponentByAVID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetDeviceComponentByAVID( theDeviceID: AVIDType; var theDeviceComponent: Component; var theDesciption: ComponentDescription; var theDeviceKind: ResType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMNewDisplayModeList()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMNewDisplayModeList( displayID: DisplayIDType; modeListFlags: UInt32; reserved: UInt32; var thePanelCount: DMListIndexType; var thePanelList: DMListType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetIndexedDisplayModeFromList()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetIndexedDisplayModeFromList( panelList: DMListType; itemIndex: DMListIndexType; reserved: UInt32; listIterator: DMDisplayModeListIteratorUPP; userData: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetGraphicInfoByAVID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetGraphicInfoByAVID( theID: AVIDType; var theAVPcit: PicHandle; var theAVIconSuite: Handle; var theAVLocation: AVLocationRec ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetAVPowerState()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetAVPowerState( theID: AVIDType; getPowerState: AVPowerStatePtr; reserved1: UInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMSetAVPowerState()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMSetAVPowerState( theID: AVIDType; setPowerState: AVPowerStatePtr; powerFlags: UInt32; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetDeviceAVIDByPortAVID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetDeviceAVIDByPortAVID( portAVID: AVIDType; var deviceAVID: AVIDType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetEnableByAVID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetEnableByAVID( theAVID: AVIDType; var isAVIDEnabledNow: Boolean; var canChangeEnableNow: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMSetEnableByAVID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMSetEnableByAVID( theAVID: AVIDType; doEnable: Boolean; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMGetDisplayMode()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib68k 2.0 and later
 }
function DMGetDisplayMode( theDevice: GDHandle; switchInfo: VDSwitchInfoPtr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DMConfirmConfiguration()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DisplayLib 2.1 and later
 }
function DMConfirmConfiguration( filterProc: DMModalFilterUPP; confirmFlags: UInt32; reserved: UInt32; displayState: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
