{
     File:       CarbonCore/TextEncodingPlugin.h
 
     Contains:   Required interface for Text Encoding Converter-Plugins
 
     Copyright:  � 1996-2011 by Apple Inc. All rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{     Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, June 2018 }

unit TextEncodingPlugin;
interface
uses MacTypes,TextCommon,TextEncodingConverter;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{
  ####################################################################################
        Constants
  ####################################################################################
}
{
   This constant is needed for MacOS X development only. It is the name in which the
   function to grab the plugin's dispatch table must go by. 
}
const
	kTECMacOSXDispatchTableNameString = 'ConverterPluginGetPluginDispatchTable';
{ These constant are needed for TEC plugins.}
const
	kTECAvailableEncodingsResType = FOUR_CHAR_CODE('cven');
	kTECAvailableSniffersResType = FOUR_CHAR_CODE('cvsf');
	kTECSubTextEncodingsResType = FOUR_CHAR_CODE('cvsb');
	kTECConversionInfoResType = FOUR_CHAR_CODE('cvif');
	kTECMailEncodingsResType = FOUR_CHAR_CODE('cvml');
	kTECWebEncodingsResType = FOUR_CHAR_CODE('cvwb');
	kTECInternetNamesResType = FOUR_CHAR_CODE('cvmm');

const
	kTECPluginType = FOUR_CHAR_CODE('ecpg');
	kTECPluginCreator = FOUR_CHAR_CODE('encv');
	kTECPluginOneToOne = FOUR_CHAR_CODE('otoo');
	kTECPluginOneToMany = FOUR_CHAR_CODE('otom');
	kTECPluginManyToOne = FOUR_CHAR_CODE('mtoo');
	kTECPluginSniffObj = FOUR_CHAR_CODE('snif');

const
	verUnspecified = 32767;
	kTECResourceID = 128;

{
  ####################################################################################
        Structs
  ####################################################################################
}

{ These structs are needed for TEC plugins.}

type
	TextEncodingRecPtr = ^TextEncodingRec;
	TextEncodingRec = record
		base: UInt32;
		variant: UInt32;
		format: UInt32;
	end;
{ supported encodings & sniffers lists, type TECEncodingsListRec }
type
	TECEncodingsListRecPtr = ^TECEncodingsListRec;
	TECEncodingsListRec = record
		count: UInt32;
		encodings: TextEncodingRec;              { first of many}
	end;
type
	TECEncodingsListPtr = TECEncodingsListRecPtr;
	TECEncodingsListHandle = ^TECEncodingsListPtr;
{ sub encodings list - type TECSubTextEncodingsRec }
type
	TECSubTextEncodingRecPtr =^TECSubTextEncodingRec;
	TECSubTextEncodingRec = record
		offset: UInt32;                 { offset to next variable-length record}
		searchEncoding: TextEncodingRec;         { the encoding}
		count: UInt32;
		subEncodings: TextEncodingRec;           { first of many sub encodings for searchEncoding}
	end;
type
	TECSubTextEncodingsRecPtr = ^TECSubTextEncodingsRec;
	TECSubTextEncodingsRec = record
		count: UInt32;
		subTextEncodingRec: TECSubTextEncodingRec;  { first of many}
	end;
type
	TECSubTextEncodingsPtr = TECSubTextEncodingsRecPtr;
	TECSubTextEncodingsHandle = ^TECSubTextEncodingsPtr;
{ conversions pairs list - type TECEncodingPairsRec }
type
	TECEncodingPairRecPtr = ^TECEncodingPairRec;
	TECEncodingPairRec = record
		source: TextEncodingRec;
		dest: TextEncodingRec;
	end;
type
	TECEncodingPairs = record
		encodingPair: TECEncodingPairRec;
		flags: UInt32;                  { 'flags' name is not really used yet (JKC 9/5/97)}
		speed: UInt32;                  { 'speed' name is not really used yet (JKC 9/5/97)}
	end;
type
	TECEncodingPairsRecPtr =^TECEncodingPairsRec;
	TECEncodingPairsRec = record
		count: UInt32;
		encodingPairs: TECEncodingPairs;
	end;
type
	TECEncodingPairsPtr = TECEncodingPairsRecPtr;
	TECEncodingPairsHandle = ^TECEncodingPairsPtr;
{ mail & web encodings lists - type TECLocaleToEncodingsListRec }
type
	TECLocaleListToEncodingListRecPtr = ^TECLocaleListToEncodingListRec;
	TECLocaleListToEncodingListRec = record
		offset: UInt32;                 { offset to next variable-length record}
		count: UInt32;
		locales: RegionCode;                { first in list of locales}
                                              { TECEncodingListRec encodingList;     // after local variable length array}
	end;
type
	TECLocaleListToEncodingListPtr = TECLocaleListToEncodingListRecPtr;
	TECLocaleToEncodingsListRecPtr = ^TECLocaleToEncodingsListRec;
	TECLocaleToEncodingsListRec = record
		count: UInt32;
		localeListToEncodingList: TECLocaleListToEncodingListRec; { language of name}
	end;
type
	TECLocaleToEncodingsListPtr = TECLocaleToEncodingsListRecPtr;
	TECLocaleToEncodingsListHandle = ^TECLocaleToEncodingsListPtr;
{ internet names list - type TECInternetNamesRec }
type
	TECInternetNameRecPtr = ^TECInternetNameRec;
	TECInternetNameRec = record
		offset: UInt32;                 { offset to next variable-length record}
		searchEncoding: TextEncodingRec;         { named encoding}
		encodingNameLength: UInt8;
		encodingName: array [0..0] of UInt8;        { first byte of many }
	end;
type
	TECInternetNamesRecPtr = ^TECInternetNamesRec;
	TECInternetNamesRec = record
		count: UInt32;
		InternetNames: TECInternetNameRec;          { first of many}
	end;
type
	TECInternetNamesPtr = TECInternetNamesRecPtr;
	TECInternetNamesHandle = ^TECInternetNamesPtr;
{ plugin context record }
type
	TECBufferContextRecPtr = ^TECBufferContextRec;
	TECBufferContextRec = record
		textInputBuffer: ConstTextPtr;
		textInputBufferEnd: ConstTextPtr;
		textOutputBuffer: TextPtr;
		textOutputBufferEnd: TextPtr;

		encodingInputBuffer: ConstTextEncodingRunPtr;
		encodingInputBufferEnd: ConstTextEncodingRunPtr;
		encodingOutputBuffer: TextEncodingRunPtr;
		encodingOutputBufferEnd: TextEncodingRunPtr;
	end;
type
	TECPluginStateRecPtr = ^TECPluginStateRec;
	TECPluginStateRec = record
		state1: UInt8;
		state2: UInt8;
		state3: UInt8;
		state4: UInt8;

		longState1: UInt32;
		longState2: UInt32;
		longState3: UInt32;
		longState4: UInt32;
	end;
type
	TECConverterContextRecPtr = ^TECConverterContextRec;
	TECConverterContextRec = record
{ public - manipulated externally and by plugin}
		pluginRec: Ptr;
		sourceEncoding: TextEncoding;
		destEncoding: TextEncoding;
		reserved1: UInt32;
		reserved2: UInt32;
		bufferContext: TECBufferContextRec;
                                              { private - manipulated only within Plugin}
		contextRefCon: URefCon;
		conversionProc: ProcPtr;
		flushProc: ProcPtr;
		clearContextInfoProc: ProcPtr;
		options1: UInt32;
		options2: UInt32;
		pluginState: TECPluginStateRec;
	end;
type
	TECSnifferContextRecPtr = ^TECSnifferContextRec;
	TECSnifferContextRec = record
{ public - manipulated externally}
		pluginRec: Ptr;
		encoding: TextEncoding;
		maxErrors: ItemCount;
		maxFeatures: ItemCount;
		textInputBuffer: ConstTextPtr;
		textInputBufferEnd: ConstTextPtr;
		numFeatures: ItemCount;
		numErrors: ItemCount;
                                              { private - manipulated only within Plugin}
		contextRefCon: URefCon;
		sniffProc: ProcPtr;
		clearContextInfoProc: ProcPtr;
		pluginState: TECPluginStateRec;
	end;
{
  ####################################################################################
        Functional Messages
  ####################################################################################
}

type
	TECPluginNewEncodingConverterPtr = function( var newEncodingConverter: TECObjectRef; var plugContext: TECConverterContextRec; inputEncoding: TextEncoding; outputEncoding: TextEncoding ): OSStatus;
	TECPluginClearContextInfoPtr = function( encodingConverter: TECObjectRef; var plugContext: TECConverterContextRec ): OSStatus;
	TECPluginConvertTextEncodingPtr = function( encodingConverter: TECObjectRef; var plugContext: TECConverterContextRec ): OSStatus;
	TECPluginFlushConversionPtr = function( encodingConverter: TECObjectRef; var plugContext: TECConverterContextRec ): OSStatus;
	TECPluginDisposeEncodingConverterPtr = function( newEncodingConverter: TECObjectRef; var plugContext: TECConverterContextRec ): OSStatus;
	TECPluginNewEncodingSnifferPtr = function( var encodingSniffer: TECSnifferObjectRef; var snifContext: TECSnifferContextRec; inputEncoding: TextEncoding ): OSStatus;
	TECPluginClearSnifferContextInfoPtr = function( encodingSniffer: TECSnifferObjectRef; var snifContext: TECSnifferContextRec ): OSStatus;
	TECPluginSniffTextEncodingPtr = function( encodingSniffer: TECSnifferObjectRef; var snifContext: TECSnifferContextRec ): OSStatus;
	TECPluginDisposeEncodingSnifferPtr = function( encodingSniffer: TECSnifferObjectRef; var snifContext: TECSnifferContextRec ): OSStatus;
	TECPluginGetCountAvailableTextEncodingsPtr = function( availableEncodings: TextEncodingPtr; maxAvailableEncodings: ItemCount; var actualAvailableEncodings: ItemCount ): OSStatus;
	TECPluginGetCountAvailableTextEncodingPairsPtr = function( availableEncodings: TECConversionInfoPtr; maxAvailableEncodings: ItemCount; var actualAvailableEncodings: ItemCount ): OSStatus;
	TECPluginGetCountDestinationTextEncodingsPtr = function( inputEncoding: TextEncoding; destinationEncodings: TextEncodingPtr; maxDestinationEncodings: ItemCount; var actualDestinationEncodings: ItemCount ): OSStatus;
	TECPluginGetCountSubTextEncodingsPtr = function( inputEncoding: TextEncoding; subEncodings: TextEncodingPtr; maxSubEncodings: ItemCount; var actualSubEncodings: ItemCount ): OSStatus;
	TECPluginGetCountAvailableSniffersPtr = function( availableEncodings: TextEncodingPtr; maxAvailableEncodings: ItemCount; var actualAvailableEncodings: ItemCount ): OSStatus;
	TECPluginGetTextEncodingInternetNamePtr = function( textEncoding_: TextEncoding; var encodingName: Str255 ): OSStatus;
	TECPluginGetTextEncodingFromInternetNamePtr = function( var textEncoding_: TextEncoding; encodingName: Str255 ): OSStatus;
	TECPluginGetCountWebEncodingsPtr = function( availableEncodings: TextEncodingPtr; maxAvailableEncodings: ItemCount; var actualAvailableEncodings: ItemCount ): OSStatus;
	TECPluginGetCountMailEncodingsPtr = function( availableEncodings: TextEncodingPtr; maxAvailableEncodings: ItemCount; var actualAvailableEncodings: ItemCount ): OSStatus;
{
  ####################################################################################
        Dispatch Table Definition
  ####################################################################################
}

const
	kTECPluginDispatchTableVersion1 = $00010000; { 1.0 through 1.0.3 releases}
	kTECPluginDispatchTableVersion1_1 = $00010001; { 1.1 releases}
	kTECPluginDispatchTableVersion1_2 = $00010002; { 1.2 releases}
	kTECPluginDispatchTableCurrentVersion = kTECPluginDispatchTableVersion1_2;

type
	TECPluginDispatchTablePtr = ^TECPluginDispatchTable;
	TECPluginDispatchTable = record
		version: TECPluginVersion;
		compatibleVersion: TECPluginVersion;
		PluginID: TECPluginSignature;

		PluginNewEncodingConverter: TECPluginNewEncodingConverterPtr;
		PluginClearContextInfo: TECPluginClearContextInfoPtr;
		PluginConvertTextEncoding: TECPluginConvertTextEncodingPtr;
		PluginFlushConversion: TECPluginFlushConversionPtr;
		PluginDisposeEncodingConverter: TECPluginDisposeEncodingConverterPtr;

		PluginNewEncodingSniffer: TECPluginNewEncodingSnifferPtr;
		PluginClearSnifferContextInfo: TECPluginClearSnifferContextInfoPtr;
		PluginSniffTextEncoding: TECPluginSniffTextEncodingPtr;
		PluginDisposeEncodingSniffer: TECPluginDisposeEncodingSnifferPtr;

		PluginGetCountAvailableTextEncodings: TECPluginGetCountAvailableTextEncodingsPtr;
		PluginGetCountAvailableTextEncodingPairs: TECPluginGetCountAvailableTextEncodingPairsPtr;
		PluginGetCountDestinationTextEncodings: TECPluginGetCountDestinationTextEncodingsPtr;
		PluginGetCountSubTextEncodings: TECPluginGetCountSubTextEncodingsPtr;
		PluginGetCountAvailableSniffers: TECPluginGetCountAvailableSniffersPtr;
		PluginGetCountWebTextEncodings: TECPluginGetCountWebEncodingsPtr;
		PluginGetCountMailTextEncodings: TECPluginGetCountMailEncodingsPtr;

		PluginGetTextEncodingInternetName: TECPluginGetTextEncodingInternetNamePtr;
		PluginGetTextEncodingFromInternetName: TECPluginGetTextEncodingFromInternetNamePtr;
	end;
{
   The last prototype here is for MacOS X plugins only. TEC Plugins in MacOS X need to export a
   a function called ConverterPluginGetPluginDispatchTable with the following prototype:
   extern TECPluginDispatchTable *ConverterPluginGetPluginDispatchTable( void )
   This function will need to return a pointer to the plugin's function dispatch table 
   when called. It is important that the function be called exactly 
   "ConverterPluginGetPluginDispatchTable". TECPluginGetPluginDispatchTablePtr is a 
   function pointer to this function.
}
type
	TECPluginGetPluginDispatchTablePtr = function: TECPluginDispatchTablePtr;

{$endc} {TARGET_OS_MAC}

end.
