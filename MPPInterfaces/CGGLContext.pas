{ CoreGraphics - CGGLContext.h
 * Copyright (c) 2003-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2015 }
unit CGGLContext;
interface
uses MacTypes,CGBase,CGContext,CGGeometry,CGColorSpace;
{$ALIGN POWER}

{$ifc TARGET_OS_MAC}

{ NOTE: The functions declared in this file do not work as expected in Mac
   OS X 10.5 or later. Do not use them. }

{ Create a context from the OpenGL context `glContext'. `size' specifies
   the dimensions of the OpenGL viewport rectangle which the CGContext will
   establish by calling glViewport(3G). If non-NULL, `colorspace' should be
   an RGB profile which specifies the destination space when rendering
   device-independent colors.

   This function does not work as expected in Mac OS X 10.5 or later. Do not
   use it. }

function CGGLContextCreate( glContext: univ Ptr; size: CGSize; colorspace: CGColorSpaceRef ): CGContextRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_3,
	__MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{ Update the OpenGL viewport associated with the OpenGL context `c' to a
   new size. `size' specifies the dimensions of a new OpenGL viewport
   rectangle which the CGContext will establish by calling glViewport(3G).
   This function should be called whenever the size of the OpenGL context
   changes.

   This function does not work expected in Mac OS X 10.5 or later. Do not
   use it. }

procedure CGGLContextUpdateViewportSize( c: CGContextRef; size: CGSize );
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_3, __MAC_10_6, __IPHONE_NA,
	__IPHONE_NA);

{$endc}

end.
