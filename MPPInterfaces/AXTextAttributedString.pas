{
 *  AXTextAttributedString.h
 *
 *  Copyright (c) 2002 Apple Computer, Inc. All rights reserved.
 *
 }

{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }

unit AXTextAttributedString;
interface
uses MacTypes, CFBase;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

const kAXFontTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFDictionaryRef - see kAXFontTextAttribute keys below
const kAXForegroundColorTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CGColorRef
const kAXBackgroundColorTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CGColorRef
const kAXUnderlineColorTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CGColorRef
const kAXStrikethroughColorTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CGColorRef
const kAXUnderlineTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFNumberRef - AXUnderlineStyle
const kAXSuperscriptTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFNumberRef = + number for superscript - for subscript
const kAXStrikethroughTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFBooleanRef
const kAXShadowTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFBooleanRef

const kAXAttachmentTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// AXUIElementRef
const kAXLinkTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// AXUIElementRef

const kAXNaturalLanguageTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFStringRef - the spoken language of the text
const kAXReplacementStringTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFStringRef

const kAXMisspelledTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// AXUIElementRef

const kAXAutocorrectedTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;	// CFBooleanRef

// kAXFontTextAttribute keys
const kAXFontNameKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFStringRef - required
const kAXFontFamilyKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFStringRef - not required
const kAXVisibleNameKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFStringRef - not required
const kAXFontSizeKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFNumberRef - required

const
	kAXUnderlineStyleNone = $0;
	kAXUnderlineStyleSingle = $1;
	kAXUnderlineStyleThick = $2;
	kAXUnderlineStyleDouble = $9;
type
	AXUnderlineStyle = UInt32;


// DO NOT USE. This is an old, misspelled version of one of the above constants.
const kAXForegoundColorTextAttribute: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CGColorRef

{$endc} {TARGET_OS_MAC}

end.
