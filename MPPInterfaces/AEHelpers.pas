{
     File:       AE/AEHelpers.h
 
     Contains:   AEPrint, AEBuild and AEStream for Carbon
 
    
 
     Copyright:  � 1999-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit AEHelpers;
interface
uses MacTypes,AppleEvents,AEDataModel;

{$ifc TARGET_OS_MAC}

{
 * Originally from AEGIzmos by Jens Alfke, circa 1992.
 }


{$ALIGN MAC68K}

{
 * AEBuild:
 *
 * AEBuild provides a very high level abstraction for building
 * complete AppleEvents and complex ObjectSpeciers.  Using AEBuild it
 * is easy to produce a textual representation of an AEDesc.  The
 * format is similar to the stdio printf call, where meta data is
 * extracted from a format string and used to build the final
 * representation.
 * 
 * For more information on AEBuild and other APIs in AEHelpers, see:
 *     <http://developer.apple.com/technotes/tn/tn2045.html>
 }
{ Syntax Error Codes: }
type
	AEBuildErrorCode = UInt32;
const
	aeBuildSyntaxNoErr = 0;    { (No error) }
	aeBuildSyntaxBadToken = 1;    { Illegal character }
	aeBuildSyntaxBadEOF = 2;    { Unexpected end of format string }
	aeBuildSyntaxNoEOF = 3;    { Unexpected extra stuff past end }
	aeBuildSyntaxBadNegative = 4;    { "-" not followed by digits }
	aeBuildSyntaxMissingQuote = 5;    { Missing close "'" }
	aeBuildSyntaxBadHex = 6;    { Non-digit in hex string }
	aeBuildSyntaxOddHex = 7;    { Odd # of hex digits }
	aeBuildSyntaxNoCloseHex = 8;    { Missing hex quote close "�" }
	aeBuildSyntaxUncoercedHex = 9;    { Hex string must be coerced to a type }
	aeBuildSyntaxNoCloseString = 10;   { Missing close quote }
	aeBuildSyntaxBadDesc = 11;   { Illegal descriptor }
	aeBuildSyntaxBadData = 12;   { Bad data value inside (� �) }
	aeBuildSyntaxNoCloseParen = 13;   { Missing ")" after data value }
	aeBuildSyntaxNoCloseBracket = 14;   { Expected "," or "]" }
	aeBuildSyntaxNoCloseBrace = 15;   { Expected "," or ")" }
	aeBuildSyntaxNoKey = 16;   { Missing keyword in record }
	aeBuildSyntaxNoColon = 17;   { Missing ":" after keyword in record }
	aeBuildSyntaxCoercedList = 18;   { Cannot coerce a list }
	aeBuildSyntaxUncoercedDoubleAt = 19;   { "@@" substitution must be coerced }

{ A structure containing error state.}

type
	AEBuildErrorPtr = ^AEBuildError;
	AEBuildError = record
		fError: AEBuildErrorCode;
		fErrorPos: UInt32;
	end;
{
   Create an AEDesc from the format string.  AEBuildError can be NULL, in which case
   no explicit error information will be returned.
}
{
 *  AEBuildDesc()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEBuildDesc( var dst: AEDesc; error: AEBuildErrorPtr { can be NULL }; src: ConstCStringPtr; ... ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ varargs version of AEBuildDesc}
{
 *  vAEBuildDesc()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
// function vAEBuildDesc( var dst: AEDesc; error: AEBuildErrorPtr { can be NULL }; src: ConstCStringPtr; args: va_list ): OSStatus;
// __OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Append parameters to an existing AppleEvent}
{
 *  AEBuildParameters()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEBuildParameters( var event: AppleEvent; error: AEBuildErrorPtr { can be NULL }; format: ConstCStringPtr; ... ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ varargs version of AEBuildParameters}
{
 *  vAEBuildParameters()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
// function vAEBuildParameters( var event: AppleEvent; error: AEBuildErrorPtr { can be NULL }; format: ConstCStringPtr; args: va_list ): OSStatus;
// __OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Building an entire Apple event:}
{
 *  AEBuildAppleEvent()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEBuildAppleEvent( theClass: AEEventClass; theID: AEEventID; addressType: DescType; addressData: {const} univ Ptr; addressLength: Size; returnID: SInt16; transactionID: SInt32; var result: AppleEvent; error: AEBuildErrorPtr { can be NULL }; paramsFmt: ConstCStringPtr; ... ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ varargs version of AEBuildAppleEvent}
{
 *  vAEBuildAppleEvent()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
// function vAEBuildAppleEvent( theClass: AEEventClass; theID: AEEventID; addressType: DescType; addressData: {const} univ Ptr; addressLength: Size; returnID: SInt16; transactionID: SInt32; var resultEvt: AppleEvent; error: AEBuildErrorPtr { can be NULL }; paramsFmt: ConstCStringPtr; args: va_list ): OSStatus;
// __OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 * AEPrintDescToHandle
 *
 * AEPrintDescToHandle provides a way to turn an AEDesc into a textual
 * representation.  This is most useful for debugging calls to
 * AEBuildDesc and friends.  The Handle returned should be disposed by
 * the caller.  The size of the handle is the actual number of
 * characters in the string.
 }
{
 *  AEPrintDescToHandle()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEPrintDescToHandle( const var desc: AEDesc; var result: Handle ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 * AEStream:
 *
 * The AEStream interface allows you to build AppleEvents by appending
 * to an opaque structure (an AEStreamRef) and then turning this
 * structure into an AppleEvent.  The basic idea is to open the
 * stream, write data, and then close it - closing it produces an
 * AEDesc, which may be partially complete, or may be a complete
 * AppleEvent.
 }
type
	AEStreamRef = ^OpaqueAEStreamRef; { an opaque type }
	OpaqueAEStreamRef = record end;
{
   Create and return an AEStreamRef
   Returns NULL on memory allocation failure
}
{
 *  AEStreamOpen()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamOpen: AEStreamRef;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
   Closes and disposes of an AEStreamRef, producing
   results in the desc.  You must dispose of the desc yourself.
   If you just want to dispose of the AEStreamRef, you can pass NULL for desc.
}
{
 *  AEStreamClose()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamClose( ref: AEStreamRef; var desc: AEDesc ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
   Prepares an AEStreamRef for appending data to a newly created desc.
   You append data with AEStreamWriteData
}
{
 *  AEStreamOpenDesc()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamOpenDesc( ref: AEStreamRef; newType: DescType ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Append data to the previously opened desc.}
{
 *  AEStreamWriteData()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamWriteData( ref: AEStreamRef; data: {const} univ Ptr; length: Size ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
   Finish a desc.  After this, you can close the stream, or adding new
   descs, if you're assembling a list.
}
{
 *  AEStreamCloseDesc()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamCloseDesc( ref: AEStreamRef ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Write data as a desc to the stream}
{
 *  AEStreamWriteDesc()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamWriteDesc( ref: AEStreamRef; newType: DescType; data: {const} univ Ptr; length: Size ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Write an entire desc to the stream}
{
 *  AEStreamWriteAEDesc()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamWriteAEDesc( ref: AEStreamRef; const var desc: AEDesc ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
   Begin a list.  You can then append to the list by doing
   AEStreamOpenDesc, or AEStreamWriteDesc.
}
{
 *  AEStreamOpenList()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamOpenList( ref: AEStreamRef ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Finish a list.}
{
 *  AEStreamCloseList()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamCloseList( ref: AEStreamRef ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
   Begin a record.  A record usually has type 'reco', however, this is
   rather generic, and frequently a different type is used.
}
{
 *  AEStreamOpenRecord()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamOpenRecord( ref: AEStreamRef; newType: DescType ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Change the type of a record.}
{
 *  AEStreamSetRecordType()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamSetRecordType( ref: AEStreamRef; newType: DescType ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Finish a record}
{
 *  AEStreamCloseRecord()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamCloseRecord( ref: AEStreamRef ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
   Add a keyed descriptor to a record.  This is analogous to AEPutParamDesc.
   it can only be used when writing to a record.
}
{
 *  AEStreamWriteKeyDesc()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamWriteKeyDesc( ref: AEStreamRef; key: AEKeyword; newType: DescType; data: {const} univ Ptr; length: Size ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
   OpenDesc for a keyed record entry.  You can use AEStreamWriteData
   after opening a keyed desc.
}
{
 *  AEStreamOpenKeyDesc()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamOpenKeyDesc( ref: AEStreamRef; key: AEKeyword; newType: DescType ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Write a key to the stream - you can follow this with an AEWriteDesc.}
{
 *  AEStreamWriteKey()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamWriteKey( ref: AEStreamRef; key: AEKeyword ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
   Create a complete AppleEvent.  This creates and returns a new stream.
   Use this call to populate the meta fields in an AppleEvent record.
   After this, you can add your records, lists and other parameters.
}
{
 *  AEStreamCreateEvent()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamCreateEvent( clazz: AEEventClass; id: AEEventID; targetType: DescType; targetData: {const} univ Ptr; targetLength: Size; returnID: SInt16; transactionID: SInt32 ): AEStreamRef;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
   This call lets you augment an existing AppleEvent using the stream
   APIs.  This would be useful, for example, in constructing the reply
   record in an AppleEvent handler.  Note that AEStreamOpenEvent will
   consume the AppleEvent passed in - you can't access it again until the
   stream is closed.  When you're done building the event, AEStreamCloseStream
    will reconstitute it.
}
{
 *  AEStreamOpenEvent()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamOpenEvent( var event: AppleEvent ): AEStreamRef;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Mark a keyword as being an optional parameter.}
{
 *  AEStreamOptionalParam()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function AEStreamOptionalParam( ref: AEStreamRef; key: AEKeyword ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );



{$endc} {TARGET_OS_MAC}

end.
