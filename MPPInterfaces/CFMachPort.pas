{	CFMachPort.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFMachPort;
interface
uses MacTypes,CFBase,CFRunLoop,MacOSXPosix;
{$ALIGN POWER}


type
	CFMachPortRef = ^__CFMachPort; { an opaque type }
	__CFMachPort = record end;

type
	CFMachPortContext = record
		version: CFIndex;
		info: UnivPtr;
		retain: function( info: {const} univ Ptr ): UnivPtr;
		release: procedure( info: {const} univ Ptr );
		copyDescription: function( info: {const} univ Ptr ): CFStringRef;
	end;
	CFMachPortContextPtr = ^CFMachPortContext;

type
	CFMachPortCallBack = procedure( port: CFMachPortRef; msg: univ Ptr; size: CFIndex; info: univ Ptr );
	CFMachPortInvalidationCallBack = procedure( port: CFMachPortRef; info: univ Ptr );

function CFMachPortGetTypeID: CFTypeID;

function CFMachPortCreate( allocator: CFAllocatorRef; callout: CFMachPortCallBack; var context: CFMachPortContext; var shouldFreeInfo: Boolean ): CFMachPortRef;
function CFMachPortCreateWithPort( allocator: CFAllocatorRef; portNum: mach_port_t; callout: CFMachPortCallBack; var context: CFMachPortContext; var shouldFreeInfo: Boolean ): CFMachPortRef;

function CFMachPortGetPort( port: CFMachPortRef ): mach_port_t;
procedure CFMachPortGetContext( port: CFMachPortRef; var context: CFMachPortContext );
procedure CFMachPortInvalidate( port: CFMachPortRef );
function CFMachPortIsValid( port: CFMachPortRef ): Boolean;
{GPC-ONLY-START}
// GPC error: function result must not be a procedural type
function CFMachPortGetInvalidationCallBack( port: CFMachPortRef ): UniversalProcPtr;
{GPC-ONLY-ELSE}
function CFMachPortGetInvalidationCallBack( port: CFMachPortRef ): CFMachPortInvalidationCallBack;
{GPC-ONLY-FINISH}
procedure CFMachPortSetInvalidationCallBack( port: CFMachPortRef; callout: CFMachPortInvalidationCallBack );

function CFMachPortCreateRunLoopSource( allocator: CFAllocatorRef; port: CFMachPortRef; order: CFIndex ): CFRunLoopSourceRef;


end.
