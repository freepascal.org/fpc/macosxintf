{
 *  QLThumbnailImage.h
 *  Quick Look
 *
 *  Copyright 2007-2010 Apple Inc.
 *  All rights reserved.
 *
 }
{ Pascal Translation: Gorazd Krosl <gorazd_1957@yahoo.ca>, November 2009 }
{ Pascal Translation updated: Jonas Maebe <jonas@freepascal.org>, October 2012 }
 
unit QLThumbnailImage;
interface
uses MacTypes,CFBase,CFURL,CFDictionary,CGGeometry,CGImage,QLBase;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}



{!
 *      @function QLThumbnailImageCreate
 *      @abstract Creates a thumbnail for the designated file. Returns NULL if Quick Look does not support this file type.
 *      @param allocator The allocator to use to create the image.
 *      @param url The URL to the file.
 *      @param maxThumbnailSize The maximum desired size.
 *      @param options See options below.
 *      @result The thumbnail image. NULL if not available.
 *      @discussion QLThumbnailImageCreate() does not replace IconServices.
 *                  Also QLThumbnailImageCreate() will block until the thumbnail is generated so you should consider calling it in a thread.
 }
function QLThumbnailImageCreate( allocator: CFAllocatorRef; url: CFURLRef; maxThumbnailSize: CGSize; options: CFDictionaryRef ): CGImageRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;

{
 * Options for the thumbnail.
 }

{!
 *      @constant kQLThumbnailOptionIconModeKey
 *      @abstract If kCFBooleanTrue, QL will produce an icon (ie a thumbnail and all the icon decor, like shadows, curled corner, etc.).
 }
const kQLThumbnailOptionIconModeKey: CFStringRef;

{!
 *      @constant kQLThumbnailOptionScaleFactorKey
 *      @abstract This is the user scale factor (as a CFNumber). If absent, default value is 1.0
 }
const kQLThumbnailOptionScaleFactorKey: CFStringRef;

{$endc} {TARGET_OS_MAC}

end.
