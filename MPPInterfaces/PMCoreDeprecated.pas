{
     File:       PrintCore/PMCoreDeprecated.h
 
     Contains:   Deprecated Carbon Printing Manager Interfaces.
 
     Copyright (c) 1998-2006,2008 by Apple Inc. All Rights Reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit PMCoreDeprecated;
interface
uses MacTypes,Files,QuickdrawTypes,PMDefinitions,PMDefinitionsDeprecated,ColorSyncDeprecated,CFBase,CFArray,CFData;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
 *  PMFlattenPageFormatToCFData() *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPageFormatCreateDataRepresentation instead.
 *
 *  Summary:
 *    Returns a flattened representation of the page format object in a CFDataRef.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in ApplicationServices.framework but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function PMFlattenPageFormatToCFData( pageFormat: PMPageFormat; var flatFormat: CFDataRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMFlattenPageFormatToURL() *** DEPRECATED ***
 *  
 *  Discussion:
 *    Instead use PMPageFormatCreateDataRepresentation and write the resulting data to your destination.
 *
 *  Summary:
 *    Writes a flattened representation of the print settings to the
 *    URL specified by flattenFileURL.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in ApplicationServices.framework but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function PMFlattenPageFormatToURL( pageFormat: PMPageFormat; flattenFileURL: CFURLRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMUnflattenPageFormatWithCFData()	*** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPageFormatCreateWithDataRepresentation instead.
 *
 *  Summary:
 *    Returns a page format object given a CFDataRef to a previously
 *    flattened page format.
 *  
 *  Parameters:
 *    
 *    flattenCFData:
 *      A CFDataRef containing a flattened print settings.
 *    
 *    pageFormat:
 *      On return, a newly created page format object create with the
 *      data contained in 'flattenCFData'. The printing framework will
 *      create the object. The app is responsible for disposing of it.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in ApplicationServices.framework but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function PMUnflattenPageFormatWithCFData( flattenCFData: CFDataRef; var pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMUnflattenPageFormatWithURL()	*** DEPRECATED ***
 *  
 *  Discussion:
 *    Instead read the data into a CFData object and use PMPageFormatCreateWithDataRepresentation.
 *
 *  Summary:
 *    Returns a page format object given a CFURLRef referencing a file
 *    containing a previously flattened page format.
 *  
 *  Parameters:
 *    
 *    flattenFileURL:
 *      A CFURLRef referencing a file that contains a flattened page
 *      format.
 *    
 *    pageFormat:
 *      On return, a newly created page format object created from the
 *      flattened page format data specified by 'flattenFileURL'. The
 *      caller is responsible for releasing the page format created.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in ApplicationServices.framework but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function PMUnflattenPageFormatWithURL( flattenFileURL: CFURLRef; var pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMFlattenPrintSettingsToCFData()	*** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrintSettingsCreateDataRepresentation instead.
 *
 *  Summary:
 *    Returns a flattened print settings object as a CFDataRef.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in ApplicationServices.framework but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function PMFlattenPrintSettingsToCFData( printSettings: PMPrintSettings; var flatSetting: CFDataRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMFlattenPrintSettingsToURL()	*** DEPRECATED ***
 *  
 *  Discussion:
 *    Instead use PMPrintSettingsCreateDataRepresentation and write the resulting data to your destination.
 *
 *  Summary:
 *    Writes a flattened representation of the print settings to the
 *    URL specified by flattenFileURL.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in ApplicationServices.framework but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function PMFlattenPrintSettingsToURL( printSettings: PMPrintSettings; flattenFileURL: CFURLRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMUnflattenPrintSettingsWithCFData()	*** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrintSettingsCreateWithDataRepresentation instead.
 *
 *  Summary:
 *    Returns a print settings object given a CFDataRef to a previously
 *    flatten print settings.
 *  
 *  Parameters:
 *    
 *    flattenCFData:
 *      A CFDataRef containing a flatten print settings.
 *    
 *    printSettings:
 *      On return, a newly created print settings object create with
 *      the data contained in 'flattenCFData'. The printing framework
 *      will create the object. The app is responsible for disposing of
 *      it.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in ApplicationServices.framework but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function PMUnflattenPrintSettingsWithCFData( flattenCFData: CFDataRef; var printSettings: PMPrintSettings ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMUnflattenPrintSettingsWithURL()	*** DEPRECATED ***
 *  
 *  Discussion:
 *    Instead read the data into a CFData object and use PMPrintSettingsCreateWithDataRepresentation.
 *
 *  Summary:
 *    Returns a print settings object given a CFURLRef referencing a
 *    file containing a previously flattened print settings.
 *  
 *  Parameters:
 *    
 *    flattenFileURL:
 *      A CFURLRef referencing a file that contains a flattened print
 *      settings.
 *    
 *    printSettings:
 *      On return, a newly created print settings object created with
 *      the data specified by 'flattenFileURL'. The caller is
 *      responsible for releasing the print settings created.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in ApplicationServices.framework but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function PMUnflattenPrintSettingsWithURL( flattenFileURL: CFURLRef; var printSettings: PMPrintSettings ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;
  
{
 *  PMGetPrintSettingsExtendedData()	*** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrintSettingsGetValue instead.
 *
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetPrintSettingsExtendedData( printSettings: PMPrintSettings; dataID: OSType; var size: UInt32; extendedData: univ Ptr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;

{
 *  PMSetPrintSettingsExtendedData()	*** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrintSettingsSetValue instead.
 *
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetPrintSettingsExtendedData( printSettings: PMPrintSettings; dataID: OSType; size: UInt32; extendedData: univ Ptr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;

{
 *  PMPaperGetName()
 *  
 *  Summary:
 *    Returns the name for a given paper.
 *  
 *  Discussion:
 *		Use PMPaperCreateLocalizedName instead
 *
 *  Parameters:
 *    
 *    paper:
 *      Obtain the name for this paper.
 *    
 *    paperName:
 *      If successful noErr is returned and *paperName is set to the
 *      name of the paper. The returned paper name is that appropriate
 *      to display to a user.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework but deprecated in 10.7
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.3 and later
 *    Non-Carbon CFM:   not available
 *  
 *  Compatibility:
 *  	Not appropriate for CUPS filters, drivers, and backends.
 *  
 }
function PMPaperGetName( paper: PMPaper; var paperName: CFStringRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_7;

{
 *  PMPrinterCreatePaperInfoListForMenu()
 *  
 *  Summary:
 *    Hand back a list of paper info items that can be used to create a
 *    paper size menu. Each item is a dictionary describing the menu item.
 *  
 *  Parameters:
 *    
 *    printer:
 *      The printer whose page size list should be enumerated. To get
 *      the session's current printer, see PMSessionGetCurrentPrinter().
 *    
 *    pageFormat:
 *      The page format object that contains the current paper size. 
 *    
 *    paperInfoList:
 *      If this function is successful then noErr will be returned and
 *      *'paperInfoList' will be set to a newly created CFArray. Each
 *      element in the array will be a CFDictionary describing the
 *      menu item that needs to be added for the paper size menu. If this
 *      function fails then a non-zero error code will be returned and
 *      *'paperInfoList' will be set to NULL.
 *
 }
function PMPrinterCreatePaperInfoListForMenu( printer: PMPrinter; pageFormat: PMPageFormat; paperInfoList: CFArrayRefPtr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_7;

{$ifc not TARGET_CPU_64}

{
 * SPECIAL AVAILABILITY note: This routine is available in ApplicationsServices.framework in
 * Mac OS X version 10.0 and later. On Mac OS X it is available to CFM applications through CarbonLib
 * starting with Mac OS X version 10.2 and later.
 *
 * On Mac OS 8/9 using CarbonLib, this routine returns kPMNotImplemented
 }

{
 *  PMSetProfile()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Application must be rendering with QuickDraw for this to have
 *    effect. Use Quartz drawing instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetProfile( printSettings: PMPrintSettings; tag: PMTag; const var profile: CMProfileLocation ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{ Callbacks }
{***************************************}
{  All Idle UPP routines are deprecated }
{***************************************}
type
	PMIdleProcPtr = procedure;
{GPC-ONLY-START}
	PMIdleUPP = UniversalProcPtr; // should be PMIdleProcPtr
{GPC-ONLY-ELSE}
	PMIdleUPP = PMIdleProcPtr;
{GPC-ONLY-FINISH}

{
 *  NewPMIdleUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function NewPMIdleUPP( userRoutine: PMIdleProcPtr ): PMIdleUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposePMIdleUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure DisposePMIdleUPP( userUPP: PMIdleUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokePMIdleUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure InvokePMIdleUPP( userUPP: PMIdleUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$ifc PM_USE_SESSION_APIS}

{
 *      @function PMSessionBeginDocumentNoDialog
 *      @discussion Use PMSessionBeginCGDocumentNoDialog instead.
 }
{
 *  PMSessionBeginDocumentNoDialog()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.6 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionBeginDocumentNoDialog( printSession: PMPrintSession; printSettings: PMPrintSettings; pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMSessionGetGraphicsContext()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionGetCGGraphicsContext instead.
 *  
 *  Parameters:
 *    
 *    printSession:
 *      the session
 *    
 *    graphicsContextType:
 *      either kPMGraphicsContextQuickdraw or
 *      kPMGraphicsContextCoreGraphics
 *    
 *    graphicsContext:
 *      returns a GrafPtr or a CGContextRef
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionGetGraphicsContext( printSession: PMPrintSession; graphicsContextType: CFStringRef; var graphicsContext: UnivPtr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMSessionEnableColorSync()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Application must be rendering with QuickDraw for this to have
 *    effect. Use Quartz drawing instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionEnableColorSync( printSession: PMPrintSession ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMSessionDisableColorSync()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Application must be rendering with QuickDraw for this to have
 *    effect. Use Quartz drawing instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionDisableColorSync( printSession: PMPrintSession ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMSessionSetIdleProc()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionSetIdleProc( printSession: PMPrintSession; idleProc: PMIdleUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionGeneral()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterGetCommInfo instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionGeneral( printSession: PMPrintSession; pData: Ptr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionConvertOldPrintRecord()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionConvertOldPrintRecord( printSession: PMPrintSession; printRecordHandle: Handle; var printSettings: PMPrintSettings; var pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionMakeOldPrintRecord()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionMakeOldPrintRecord( printSession: PMPrintSession; printSettings: PMPrintSettings; pageFormat: PMPageFormat; var printRecordHandle: Handle ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionSetCurrentPrinter()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionSetCurrentPMPrinter after creating a PMPrinter with
 *    PMPrinterCreateFromPrinterID.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.1 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.4 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionSetCurrentPrinter( session: PMPrintSession; printerName: CFStringRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_1_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionPostScriptBegin()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionPostScriptBegin( printSession: PMPrintSession ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionPostScriptEnd()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionPostScriptEnd( printSession: PMPrintSession ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionPostScriptHandle()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionPostScriptHandle( printSession: PMPrintSession; psHandle: Handle ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionPostScriptData()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionPostScriptData( printSession: PMPrintSession; psPtr: Ptr; len: Size ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionPostScriptFile()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionPostScriptFile( printSession: PMPrintSession; var psFile: FSSpec ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionSetPSInjectionData()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionSetPSInjectionData( printSession: PMPrintSession; printSettings: PMPrintSettings; injectionDictArray: CFArrayRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionGetDocumentFormatSupported()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionGetDocumentFormatSupported( printSession: PMPrintSession; var docFormats: CFArrayRef; limit: UInt32 ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionGetDocumentFormatGeneration()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionGetDocumentFormatGeneration( printSession: PMPrintSession; var docFormats: CFArrayRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionSetDocumentFormatGeneration()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Carbon applications using CoreGraphics instead of QuickDraw for
 *    all their drawing should use PMSessionBeginCGDocument or
 *    PMSessionBeginCGDocumentNoDialog. For generating PostScript data
 *    you should be using: PMPrinterPrintWithFile or
 *    PMPrinterPrintWithProvider. For mixing EPS data with Quartz drawing
 *    use PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionSetDocumentFormatGeneration( printSession: PMPrintSession; docFormat: CFStringRef; graphicsContextTypes: CFArrayRef; options: CFTypeRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionIsDocumentFormatSupported()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionIsDocumentFormatSupported( printSession: PMPrintSession; docFormat: CFStringRef; var supported: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$elsec} {PM_USE_SESSION_APIS}

{
 *  PMSetIdleProc()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetIdleProc( idleProc: PMIdleUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ Print loop }
{
 *  PMBegin()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMCreateSession instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMBegin: OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMEnd()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMEnd: OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetGrafPtr()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionGetCGGraphicsContext instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetGrafPtr( printContext: PMPrintContext; var grafPort: GrafPtr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ PMPageFormat }
{
 *  PMNewPageFormat()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMCreatePageFormat instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMNewPageFormat( var pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMDisposePageFormat()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMRelease instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMDisposePageFormat( pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMDefaultPageFormat()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionDefaultPageFormat instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMDefaultPageFormat( pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMValidatePageFormat()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionValidatePageFormat instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMValidatePageFormat( pageFormat: PMPageFormat; var result: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ PMPrintSettings }
{
 *  PMNewPrintSettings()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMCreatePrintSettings instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMNewPrintSettings( var printSettings: PMPrintSettings ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMDisposePrintSettings()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMRelease instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMDisposePrintSettings( printSettings: PMPrintSettings ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMDefaultPrintSettings()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionDefaultPrintSettings instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMDefaultPrintSettings( printSettings: PMPrintSettings ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMValidatePrintSettings()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionValidatePrintSettings instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMValidatePrintSettings( printSettings: PMPrintSettings; result: BooleanPtr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ Mac OS 9 Support }
{
 *  PMGeneral()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGeneral( pData: Ptr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMConvertOldPrintRecord()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMConvertOldPrintRecord( printRecordHandle: Handle; var printSettings: PMPrintSettings; var pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMMakeOldPrintRecord()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMMakeOldPrintRecord( printSettings: PMPrintSettings; pageFormat: PMPageFormat; var printRecordHandle: Handle ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ Driver Information }
{
 *  PMIsPostScriptDriver()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterIsPostScriptCapable or PMPrinterIsPostScriptPrinter instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMIsPostScriptDriver( var isPostScript: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetLanguageInfo()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterGetLanguageInfo instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetLanguageInfo( var info: PMLanguageInfo ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetDriverCreator()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterGetDriverCreator instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetDriverCreator( var creator: OSType ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetDriverReleaseInfo()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterGetDriverReleaseInfo instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetDriverReleaseInfo( var release: VersRec ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetPrinterResolutionCount()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterGetPrinterResolutionCount instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetPrinterResolutionCount( var count: UInt32 ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetPrinterResolution()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterGetPrinterResolution instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetPrinterResolution( tag: PMTag; var res: PMResolution ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetIndexedPrinterResolution()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterGetIndexedPrinterResolution instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetIndexedPrinterResolution( index: UInt32; var res: PMResolution ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ ColorSync & PostScript Support }
{
 *  PMEnableColorSync()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use Quartz drawing instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMEnableColorSync: OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMDisableColorSync()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use Quartz drawing instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMDisableColorSync: OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPostScriptBegin()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPostScriptBegin: OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPostScriptEnd()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPostScriptEnd: OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPostScriptHandle()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPostScriptHandle( psHandle: Handle ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPostScriptData()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPostScriptData( psPtr: Ptr; len: Size ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPostScriptFile()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterPrintWithFile or PMPrinterPrintWithProvider instead.
 *
 *    For using EPS data together with other application drawing
 *    using Quartz, see PMCGImageCreateWithEPSDataProvider.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPostScriptFile( var psFile: FSSpec ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ Error }
{
 *  PMError()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionError instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMError: OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSetError()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionSetError instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetError( printError: OSStatus ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$endc} {PM_USE_SESSION_APIS}

{
 *  PMGetJobName()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrintSettingsGetJobName instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetJobName( printSettings: PMPrintSettings; name: StringPtr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  PMSetJobName()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrintSettingsSetJobName instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetJobName( printSettings: PMPrintSettings; const name: Str255 ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetPhysicalPaperSize()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMGetUnadjustedPaperRect instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetPhysicalPaperSize( pageFormat: PMPageFormat; var paperSize: PMRect ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSetAdjustedPageRect() *** DEPRECATED ***
 *  
 *  Discussion:
 *    To set a particular paper size and margins create a PMPaper first then call
 *    PMCreatePageFormatWithPMPaper.
 *
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetAdjustedPageRect( pageFormat: PMPageFormat; const var pageRect: PMRect ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMSetPhysicalPaperSize()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    To set a particular paper size create a PMPaper first then call
 *    PMCreatePageFormatWithPMPaper.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetPhysicalPaperSize( pageFormat: PMPageFormat; const var paperSize: PMRect ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  PMSetUnadjustedPaperRect()	 *** DEPRECATED ***
 *  
 *  Discussion:
 *    To set a particular paper size create a PMPaper first then call
 *    PMCreatePageFormatWithPMPaper.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetUnadjustedPaperRect( pageFormat: PMPageFormat; const var paperRect: PMRect ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMGetPhysicalPageSize()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMGetUnadjustedPageRect instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetPhysicalPageSize( pageFormat: PMPageFormat; var pageSize: PMRect ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetColorMode()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetColorMode( printSettings: PMPrintSettings; var colorMode: PMColorMode ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSetColorMode()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetColorMode( printSettings: PMPrintSettings; colorMode: PMColorMode ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  PMGetDestination()	*** DEPRECATED ***
 *
 *  Discussion:
 *    Use PMSessionGetDestinationType, PMSessionCopyDestinationFormat, and/or PMSessionCopyDestinationLocation instead.
 *
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetDestination( printSettings: PMPrintSettings; var destType: PMDestinationType; var fileURL: CFURLRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMPrinterGetDescriptionURL()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterCopyDescriptionURL instead. Please be aware that
 *    PMPrinterGetDescriptionURL will return a copy of the URL which
 *    you'll need to release.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMPrinterGetDescriptionURL( printer: PMPrinter; descriptionType: CFStringRef; var fileURL: CFURLRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPrinterGetDeviceURI()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterCopyDeviceURI instead. Please be aware that
 *    PMPrinterGetDeviceURI will return a copy of the URI which you'll
 *    need to release.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
function PMPrinterGetDeviceURI( printer: PMPrinter; var deviceURI: CFURLRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_2_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetResolution()	*** DEPRECATED ***
 *  
 *  Summary:
 *    Obtains the current application drawing resolution.
 *  
 *  Discussion:
 *
 *    Use Quartz drawing and call CGContextScaleCTM instead.
 *
 *    This is the drawing resolution of an app. This should not be
 *    confused with the resolution of the printer. You can call
 *    PMGetPrinterResolution to see what resolutions are avaliable for
 *    the current printer.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetResolution( pageFormat: PMPageFormat; var res: PMResolution ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMSetResolution()	*** DEPRECATED ***
 *  
 *  Discussion:
 *    Use Quartz drawing and call CGContextScaleCTM instead.
 *
 *    This sets the drawing resolution of an app. This should not be
 *    confused with the resolution of the printer.
 *
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetResolution( pageFormat: PMPageFormat; const var res: PMResolution ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMFlattenPageFormat()    *** DEPRECATED ***
 *  
 *  Summary:
 *    Use PMPageFormatCreateDataRepresentation instead.
 *
 *    Flattens a PMPageFormat object for storage in a user document.
 *  
 *  Discussion:
 *    Flattening a page format should only be necessary if you intend
 *    to preserve the object settings along with a document. A page
 *    format will persist outside of a
 *    PMSessionBeginDocxxx/PMSessionEndDocxxx block. This will allow
 *    you to use any accessors on the object without the need to
 *    flatten and unflatten. Keep in mind accessors make no assumption
 *    on the validity of the value you set. This can only be done thru
 *    PMSessionValidatePageFormat in a
 *    PMSessionBeginDocxxx/PMSessionEndDocxxx block with a valid
 *    session.
 *  
 *  Parameters:
 *    
 *    pageFormat:
 *      A page format object.
 *    
 *    flatFormat:
 *      On return, a handle to a flattened PMPageFormat object. The
 *      handle is allocated by the function. You are responsible for
 *      disposing of the handle.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMFlattenPageFormat( pageFormat: PMPageFormat; var flatFormat: Handle ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMUnflattenPageFormat()    *** DEPRECATED ***
 *  
 *  Summary:
 *    Use PMPageFormatCreateWithDataRepresentation instead.
 *
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMUnflattenPageFormat( flatFormat: Handle; var pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMFlattenPrintSettings()    *** DEPRECATED ***
 *  
 *  Summary:
 *    Use PMPrintSettingsCreateDataRepresentation instead.
 *
 *    Flattens a PMPrintSettings object for storage in a user document.
 *  
 *  Discussion:
 *    Flattening a print settings should only be necessary if you
 *    intend to preserve the object settings along with a document. A
 *    print settings will persist outside of a
 *    PMSessionBeginDocxxx/PMSessionEndDocxxx block. This allows you to
 *    use any accessors on the object without the need to flatten and
 *    unflatten. Keep in mind the accessors make no assumption on the
 *    validity of the value. This can only be done thru
 *    PMSessionValidatePrintSettings in a
 *    PMSessionBeginDocxxx/PMSessionEndDocxxx block with a valid
 *    session.
 *  
 *  Parameters:
 *    
 *    printSettings:
 *      A print settings object.
 *    
 *    flatSettings:
 *      On return, a handle to a flattened PMPrintSettings object. The
 *      handle is allocated by the function. You are responsible for
 *      disposing of the handle.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMFlattenPrintSettings( printSettings: PMPrintSettings; var flatSettings: Handle ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMUnflattenPrintSettings()    *** DEPRECATED ***
 *  
 *  Summary:
 *    Use PMPrintSettingsCreateWithDataRepresentation instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMUnflattenPrintSettings( flatSettings: Handle; var printSettings: PMPrintSettings ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMGetJobNameCFString()    *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrintSettingsGetJobName instead.
 *    
 *    Please be aware that PMGetJobNameCFString will return a copy of 
 *    the job name which you'll need to release.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetJobNameCFString( printSettings: PMPrintSettings; var name: CFStringRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMSetJobNameCFString()    *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrintSettingsSetJobName instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetJobNameCFString( printSettings: PMPrintSettings; name: CFStringRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  PMPrinterGetPrinterResolution()    *** DEPRECATED ***
 *
 *  Discussion:
 *    Use PMPrinterGetPrinterResolutionCount and PMPrinterGetIndexedPrinterResolution to examine the available printer resolutions.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMPrinterGetPrinterResolution( printer: PMPrinter; tag: PMTag; var res: PMResolution ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMPaperCreate()			*** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMPrinterGetPaperList to list the PMPaper instances available for a given printer or use PMPaperCreateCustom
 *    to create a PMPaper that represents a custom paper instance.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.3 and later
 *    Non-Carbon CFM:   not available
 }
function PMPaperCreate( printer: PMPrinter; id: CFStringRef; name: CFStringRef; width: Float64; height: Float64; const var margins: PMPaperMargins; var paperP: PMPaper ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
