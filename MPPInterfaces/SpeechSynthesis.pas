{
     File:       SpeechSynthesis/SpeechSynthesis.h
 
     Contains:   Speech Interfaces.
 
     Version:    SpeechSynthesis-4.1.10~14
 
     Copyright:  � 1989-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit SpeechSynthesis;
interface
uses MacTypes,Files,CFBase,CFDictionary,CFError,CFURL;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

const
	kTextToSpeechSynthType = FOUR_CHAR_CODE('ttsc');
	kTextToSpeechVoiceType = FOUR_CHAR_CODE('ttvd');
	kTextToSpeechVoiceFileType = FOUR_CHAR_CODE('ttvf');
	kTextToSpeechVoiceBundleType = FOUR_CHAR_CODE('ttvb');

const
	kNoEndingProsody = 1;
	kNoSpeechInterrupt = 2;
	kPreflightThenPause = 4;

const
	kImmediate = 0;
	kEndOfWord = 1;
	kEndOfSentence = 2;


{------------------------------------------}
{ GetSpeechInfo & SetSpeechInfo selectors  }
{------------------------------------------}
const
	soStatus = FOUR_CHAR_CODE('stat');
	soErrors = FOUR_CHAR_CODE('erro');
	soInputMode = FOUR_CHAR_CODE('inpt');
	soCharacterMode = FOUR_CHAR_CODE('char');
	soNumberMode = FOUR_CHAR_CODE('nmbr');
	soRate = FOUR_CHAR_CODE('rate');
	soPitchBase = FOUR_CHAR_CODE('pbas');
	soPitchMod = FOUR_CHAR_CODE('pmod');
	soVolume = FOUR_CHAR_CODE('volm');
	soSynthType = FOUR_CHAR_CODE('vers');
	soRecentSync = FOUR_CHAR_CODE('sync');
	soPhonemeSymbols = FOUR_CHAR_CODE('phsy');
	soCurrentVoice = FOUR_CHAR_CODE('cvox');
	soCommandDelimiter = FOUR_CHAR_CODE('dlim');
	soReset = FOUR_CHAR_CODE('rset');
	soCurrentA5 = FOUR_CHAR_CODE('myA5');
	soRefCon = FOUR_CHAR_CODE('refc');
	soTextDoneCallBack = FOUR_CHAR_CODE('tdcb'); { use with SpeechTextDoneProcPtr}
	soSpeechDoneCallBack = FOUR_CHAR_CODE('sdcb'); { use with SpeechDoneProcPtr}
	soSyncCallBack = FOUR_CHAR_CODE('sycb'); { use with SpeechSyncProcPtr}
	soErrorCallBack = FOUR_CHAR_CODE('ercb'); { use with SpeechErrorProcPtr}
	soPhonemeCallBack = FOUR_CHAR_CODE('phcb'); { use with SpeechPhonemeProcPtr}
	soWordCallBack = FOUR_CHAR_CODE('wdcb'); { use with SpeechWordProcPtr}
	soSynthExtension = FOUR_CHAR_CODE('xtnd');
	soSoundOutput = FOUR_CHAR_CODE('sndo');
	soOutputToFileWithCFURL = FOUR_CHAR_CODE('opaf'); { Pass a CFURLRef to write to this file, NULL to generate sound.}
	soOutputToExtAudioFile = FOUR_CHAR_CODE('opax'); { Pass a ExtAudioFileRef to write to this file, NULL to generate sound. Available in 10.6 and later.}
	soOutputToAudioDevice = FOUR_CHAR_CODE('opad'); { Pass an AudioDeviceID to play to this file, 0 to play to default output}
	soPhonemeOptions = FOUR_CHAR_CODE('popt'); { Available in 10.6 and later}


{ Type for stopSpeakingAtBoundary: and pauseSpeakingAtBoundary:}
const
	kSpeechImmediateBoundary = 0;
	kSpeechWordBoundary = 1;
	kSpeechSentenceBoundary = 2;

type
	SpeechBoundary = UInt32;
{------------------------------------------}
{ Speaking Mode Constants                  }
{------------------------------------------}
const
	modeText = FOUR_CHAR_CODE('TEXT'); { input mode constants             }
	modePhonemes = FOUR_CHAR_CODE('PHON');
	modeTune = FOUR_CHAR_CODE('TUNE');
	modeNormal = FOUR_CHAR_CODE('NORM'); { character mode and number mode constants }
	modeLiteral = FOUR_CHAR_CODE('LTRL');


const
	soVoiceDescription = FOUR_CHAR_CODE('info');
	soVoiceFile = FOUR_CHAR_CODE('fref');

{------------------------------------------}
{ Flags for phoneme generation.            }
{------------------------------------------}
const
	kSpeechGenerateTune = 1;    { Generate detailed "tune" instead of just phonemes  }
	kSpeechRelativePitch = 2;    { Pitch relative to voice baseline             }
	kSpeechRelativeDuration = 4;    { Duration relative to speech rate             }
	kSpeechShowSyllables = 8;     { Show all syllable marks                              }

{------------------------------------------}
{ AudioUnit constants - new in 10.5        }
{------------------------------------------}
const
	kAudioUnitSubType_SpeechSynthesis = FOUR_CHAR_CODE('ttsp'); { kAudioUnitType_Generator }
	kAudioUnitProperty_Voice = 3330; { Get/Set (VoiceSpec)      }
	kAudioUnitProperty_SpeechChannel = 3331; { Get (SpeechChannel)      }

{
   The speech manager sources may or may not need SpeechChannelRecord.
   If not, the .i file should be changed to use the opaque mechanism.
}
type
	SpeechChannelRecordPtr = ^SpeechChannelRecord;
	SpeechChannelRecord = record
		data: array [0..1-1] of SIGNEDLONG;
	end;
type
	SpeechChannel = SpeechChannelRecordPtr;
	SpeechChannelPtr = ^SpeechChannel;  { when a var xx:SpeechChannel parameter can be nil, it is changed to xx: SpeechChannelPtr }

type
	VoiceSpec = record
		creator: OSType;
		id: OSType;
	end;
	VoiceSpecPtr = ^VoiceSpec;

const
	kNeuter = 0;
	kMale = 1;
	kFemale = 2;


type
	VoiceDescriptionPtr = ^VoiceDescription;
	VoiceDescription = record
		length: SInt32;
		voice: VoiceSpec;
		version: SInt32;
		name: Str63;
		comment: Str255;
		gender: SInt16;
		age: SInt16;
		script: SInt16;
		language: SInt16;
		region: SInt16;
		reserved: array [0..4-1] of SInt32;
	end;


type
	VoiceFileInfoPtr = ^VoiceFileInfo;
	VoiceFileInfo = record
		fileSpec: FSSpec;
		resID: SInt16;
	end;
type
	SpeechStatusInfoPtr = ^SpeechStatusInfo;
	SpeechStatusInfo = record
		outputBusy: Boolean;
		outputPaused: Boolean;
		inputBytesLeft: SIGNEDLONG;
		phonemeCode: SInt16;
	end;


type
	SpeechErrorInfoPtr = ^SpeechErrorInfo;
	SpeechErrorInfo = record
		count: SInt16;
		oldest: OSErr;
		oldPos: SIGNEDLONG;
		newest: OSErr;
		newPos: SIGNEDLONG;
	end;


type
	SpeechVersionInfoPtr = ^SpeechVersionInfo;
	SpeechVersionInfo = record
		synthType: OSType;
		synthSubType: OSType;
		synthManufacturer: OSType;
		synthFlags: SInt32;
		synthVersion: NumVersion;
	end;


type
	PhonemeInfoPtr = ^PhonemeInfo;
	PhonemeInfo = record
		opcode: SInt16;
		phStr: Str15;
		exampleStr: Str31;
		hiliteStart: SInt16;
		hiliteEnd: SInt16;
	end;

type
	PhonemeDescriptorPtr = ^PhonemeDescriptor;
	PhonemeDescriptor = record
		phonemeCount: SInt16;
		thePhonemes: array [0..1-1] of PhonemeInfo;
	end;
type
	SpeechXtndDataPtr = ^SpeechXtndData;
	SpeechXtndData = record
		synthCreator: OSType;
		synthData: array [0..2-1] of Byte;
	end;

type
	DelimiterInfoPtr = ^DelimiterInfo;
	DelimiterInfo = record
		startDelimiter: array [0..2-1] of Byte;
		endDelimiter: array [0..2-1] of Byte;
	end;
{ Synthesizer Properties }
{
 *  kSpeechStatusProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechStatusProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechErrorsProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechErrorsProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechInputModeProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechInputModeProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechCharacterModeProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechCharacterModeProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechNumberModeProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechNumberModeProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechRateProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechRateProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPitchBaseProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPitchBaseProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPitchModProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPitchModProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechVolumeProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechVolumeProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechSynthesizerInfoProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechSynthesizerInfoProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechRecentSyncProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechRecentSyncProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPhonemeSymbolsProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPhonemeSymbolsProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechCurrentVoiceProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechCurrentVoiceProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechCommandDelimiterProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechCommandDelimiterProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechResetProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechResetProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechOutputToFileURLProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechOutputToFileURLProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechOutputToExtAudioFileProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.6 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechOutputToExtAudioFileProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;
{
 *  kSpeechOutputToAudioDeviceProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.6 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechOutputToAudioDeviceProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;
{
 *  kSpeechRefConProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechRefConProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechTextDoneCallBack
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechTextDoneCallBack: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechSpeechDoneCallBack
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechSpeechDoneCallBack: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechSyncCallBack
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechSyncCallBack: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPhonemeCallBack
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPhonemeCallBack: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechErrorCFCallBack
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechErrorCFCallBack: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechWordCFCallBack
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechWordCFCallBack: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPhonemeOptionsProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.6 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPhonemeOptionsProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;
{
 *  kSpeechAudioUnitProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.6 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechAudioUnitProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;
{
 *  kSpeechAudioGraphProperty
 *  
 *  Availability:
 *    Mac OS X:         in version 10.6 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechAudioGraphProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;
{ Speaking Modes}
{
 *  kSpeechModeText
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechModeText: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechModePhoneme
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechModePhoneme: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechModeTune
 *  
 *  Availability:
 *    Mac OS X:         in version 10.6 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechModeTune: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;
{
 *  kSpeechModeNormal
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechModeNormal: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechModeLiteral
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechModeLiteral: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{ Dictionary keys for options parameter in SpeakCFString}
{
 *  kSpeechNoEndingProsody
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechNoEndingProsody: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechNoSpeechInterrupt
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechNoSpeechInterrupt: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPreflightThenPause
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPreflightThenPause: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{ Dictionary keys returned by kSpeechStatusProperty}
{
 *  kSpeechStatusOutputBusy
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechStatusOutputBusy: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechStatusOutputPaused
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechStatusOutputPaused: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechStatusNumberOfCharactersLeft
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechStatusNumberOfCharactersLeft: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechStatusPhonemeCode
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechStatusPhonemeCode: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{ Dictionary keys returned by kSpeechErrorProperty}
{
 *  kSpeechErrorCount
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechErrorCount: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechErrorOldest
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechErrorOldest: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechErrorOldestCharacterOffset
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechErrorOldestCharacterOffset: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechErrorNewest
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechErrorNewest: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechErrorNewestCharacterOffset
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechErrorNewestCharacterOffset: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{ Dictionary keys returned by kSpeechSynthesizerInfoProperty}
{
 *  kSpeechSynthesizerInfoIdentifier
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechSynthesizerInfoIdentifier: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechSynthesizerInfoManufacturer
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechSynthesizerInfoManufacturer: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechSynthesizerInfoVersion
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechSynthesizerInfoVersion: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{ Dictionary keys returned by kSpeechPhonemeSymbolsProperty}
{
 *  kSpeechPhonemeInfoOpcode
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPhonemeInfoOpcode: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPhonemeInfoSymbol
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPhonemeInfoSymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPhonemeInfoExample
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPhonemeInfoExample: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPhonemeInfoHiliteStart
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPhonemeInfoHiliteStart: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechPhonemeInfoHiliteEnd
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechPhonemeInfoHiliteEnd: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{ Dictionary keys returned by kSpeechCurrentVoiceProperty}
{
 *  kSpeechVoiceCreator
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechVoiceCreator: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechVoiceID
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechVoiceID: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{ Dictionary keys returned by kSpeechCommandDelimiterProperty}
{
 *  kSpeechCommandPrefix
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechCommandPrefix: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechCommandSuffix
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechCommandSuffix: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{ Use with useSpeechDictionary:}
{
 *  kSpeechDictionaryLocaleIdentifier
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechDictionaryLocaleIdentifier: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechDictionaryModificationDate
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechDictionaryModificationDate: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechDictionaryPronunciations
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechDictionaryPronunciations: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechDictionaryAbbreviations
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechDictionaryAbbreviations: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechDictionaryEntrySpelling
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechDictionaryEntrySpelling: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechDictionaryEntryPhonemes
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechDictionaryEntryPhonemes: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{ Error callback user info keys}
{
 *  kSpeechErrorCallbackSpokenString
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechErrorCallbackSpokenString: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
{
 *  kSpeechErrorCallbackCharacterOffset
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kSpeechErrorCallbackCharacterOffset: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;

type
	SpeechTextDoneProcPtr = procedure( chan: SpeechChannel; refCon: SRefCon; {const} nextBuf: UnivPtrPtr; var byteLen: UNSIGNEDLONG; var controlFlags: SInt32 );
	SpeechDoneProcPtr = procedure( chan: SpeechChannel; refCon: SRefCon );
	SpeechSyncProcPtr = procedure( chan: SpeechChannel; refCon: SRefCon; syncMessage: OSType );
	SpeechErrorProcPtr = procedure( chan: SpeechChannel; refCon: SRefCon; theError: OSErr; bytePos: SIGNEDLONG );
	SpeechPhonemeProcPtr = procedure( chan: SpeechChannel; refCon: SRefCon; phonemeOpcode: SInt16 );
	SpeechWordProcPtr = procedure( chan: SpeechChannel; refCon: SRefCon; wordPos: UNSIGNEDLONG; wordLen: UInt16 );
{GPC-ONLY-START}
	SpeechTextDoneUPP = UniversalProcPtr; // should be SpeechTextDoneProcPtr
{GPC-ONLY-ELSE}
	SpeechTextDoneUPP = SpeechTextDoneProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	SpeechDoneUPP = UniversalProcPtr; // should be SpeechDoneProcPtr
{GPC-ONLY-ELSE}
	SpeechDoneUPP = SpeechDoneProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	SpeechSyncUPP = UniversalProcPtr; // should be SpeechSyncProcPtr
{GPC-ONLY-ELSE}
	SpeechSyncUPP = SpeechSyncProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	SpeechErrorUPP = UniversalProcPtr; // should be SpeechErrorProcPtr
{GPC-ONLY-ELSE}
	SpeechErrorUPP = SpeechErrorProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	SpeechPhonemeUPP = UniversalProcPtr; // should be SpeechPhonemeProcPtr
{GPC-ONLY-ELSE}
	SpeechPhonemeUPP = SpeechPhonemeProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	SpeechWordUPP = UniversalProcPtr; // should be SpeechWordProcPtr
{GPC-ONLY-ELSE}
	SpeechWordUPP = SpeechWordProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewSpeechTextDoneUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSpeechTextDoneUPP( userRoutine: SpeechTextDoneProcPtr ): SpeechTextDoneUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  NewSpeechDoneUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSpeechDoneUPP( userRoutine: SpeechDoneProcPtr ): SpeechDoneUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  NewSpeechSyncUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSpeechSyncUPP( userRoutine: SpeechSyncProcPtr ): SpeechSyncUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  NewSpeechErrorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSpeechErrorUPP( userRoutine: SpeechErrorProcPtr ): SpeechErrorUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  NewSpeechPhonemeUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSpeechPhonemeUPP( userRoutine: SpeechPhonemeProcPtr ): SpeechPhonemeUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  NewSpeechWordUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSpeechWordUPP( userRoutine: SpeechWordProcPtr ): SpeechWordUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  DisposeSpeechTextDoneUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSpeechTextDoneUPP( userUPP: SpeechTextDoneUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  DisposeSpeechDoneUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSpeechDoneUPP( userUPP: SpeechDoneUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  DisposeSpeechSyncUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSpeechSyncUPP( userUPP: SpeechSyncUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  DisposeSpeechErrorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSpeechErrorUPP( userUPP: SpeechErrorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  DisposeSpeechPhonemeUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSpeechPhonemeUPP( userUPP: SpeechPhonemeUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  DisposeSpeechWordUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSpeechWordUPP( userUPP: SpeechWordUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  InvokeSpeechTextDoneUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSpeechTextDoneUPP( chan: SpeechChannel; refCon: SRefCon; {const} var nextBuf: univ Ptr; var byteLen: UNSIGNEDLONG; var controlFlags: SInt32; userUPP: SpeechTextDoneUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  InvokeSpeechDoneUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSpeechDoneUPP( chan: SpeechChannel; refCon: SRefCon; userUPP: SpeechDoneUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  InvokeSpeechSyncUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSpeechSyncUPP( chan: SpeechChannel; refCon: SRefCon; syncMessage: OSType; userUPP: SpeechSyncUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  InvokeSpeechErrorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSpeechErrorUPP( chan: SpeechChannel; refCon: SRefCon; theError: OSErr; bytePos: SIGNEDLONG; userUPP: SpeechErrorUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  InvokeSpeechPhonemeUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSpeechPhonemeUPP( chan: SpeechChannel; refCon: SRefCon; phonemeOpcode: SInt16; userUPP: SpeechPhonemeUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  InvokeSpeechWordUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSpeechWordUPP( chan: SpeechChannel; refCon: SRefCon; wordPos: UNSIGNEDLONG; wordLen: UInt16; userUPP: SpeechWordUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

type
	SpeechErrorCFProcPtr = procedure( chan: SpeechChannel; refCon: SRefCon; theError: CFErrorRef );
	SpeechWordCFProcPtr = procedure( chan: SpeechChannel; refCon: SRefCon; aString: CFStringRef; wordRange: CFRange );

{
 *  SpeechManagerVersion()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function SpeechManagerVersion: NumVersion;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  MakeVoiceSpec()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function MakeVoiceSpec( creator: OSType; id: OSType; var voice: VoiceSpec ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CountVoices()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function CountVoices( var numVoices: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetIndVoice()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function GetIndVoice( index: SInt16; var voice: VoiceSpec ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetVoiceDescription()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function GetVoiceDescription( const var voice: VoiceSpec; var info: VoiceDescription; infoLength: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetVoiceInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function GetVoiceInfo( const var voice: VoiceSpec; selector: OSType; voiceInfo: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  NewSpeechChannel()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function NewSpeechChannel( voice: VoiceSpecPtr { can be NULL }; var chan: SpeechChannel ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DisposeSpeechChannel()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function DisposeSpeechChannel( chan: SpeechChannel ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SpeakString()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SpeakCFString instead
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework but deprecated in 10.8
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function SpeakString( const var textToBeSpoken: Str255 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  SpeakText()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SpeakCFString instead
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework but deprecated in 10.8
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function SpeakText( chan: SpeechChannel; textBuf: {const} univ Ptr; textBytes: UNSIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  SpeakBuffer()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SpeakCFString instead
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework but deprecated in 10.8
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function SpeakBuffer( chan: SpeechChannel; textBuf: {const} univ Ptr; textBytes: UNSIGNEDLONG; controlFlags: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  StopSpeech()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function StopSpeech( chan: SpeechChannel ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  StopSpeechAt()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function StopSpeechAt( chan: SpeechChannel; whereToStop: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  PauseSpeechAt()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function PauseSpeechAt( chan: SpeechChannel; whereToPause: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ContinueSpeech()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function ContinueSpeech( chan: SpeechChannel ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SpeechBusy()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function SpeechBusy: SInt16;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SpeechBusySystemWide()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function SpeechBusySystemWide: SInt16;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetSpeechRate()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function SetSpeechRate( chan: SpeechChannel; rate: Fixed ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetSpeechRate()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function GetSpeechRate( chan: SpeechChannel; var rate: Fixed ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetSpeechPitch()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function SetSpeechPitch( chan: SpeechChannel; pitch: Fixed ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetSpeechPitch()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function GetSpeechPitch( chan: SpeechChannel; var pitch: Fixed ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetSpeechInfo()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SetSpeechProperty instead
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework but deprecated in 10.8
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function SetSpeechInfo( chan: SpeechChannel; selector: OSType; speechInfo: {const} univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  GetSpeechInfo()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use CopySpeechProperty instead
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework but deprecated in 10.8
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function GetSpeechInfo( chan: SpeechChannel; selector: OSType; speechInfo: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  TextToPhonemes()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use CopyPhonemesFromText instead
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework but deprecated in 10.8
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function TextToPhonemes( chan: SpeechChannel; textBuf: {const} univ Ptr; textBytes: UNSIGNEDLONG; phonemeBuf: Handle; var phonemeBytes: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  UseDictionary()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use UseSpeechDictionary instead
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework but deprecated in 10.8
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechLib 1.0 and later
 }
function UseDictionary( chan: SpeechChannel; dictionary: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{ Replaces SpeakBuffer}
{
 *  SpeakCFString()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SpeakCFString( chan: SpeechChannel; aString: CFStringRef; options: CFDictionaryRef ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{ Replaces UseDictionary}
{
 *  UseSpeechDictionary()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function UseSpeechDictionary( chan: SpeechChannel; speechDictionary: CFDictionaryRef ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{ Replaces TextToPhonemes}
{
 *  CopyPhonemesFromText()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CopyPhonemesFromText( chan: SpeechChannel; text: CFStringRef; var phonemes: CFStringRef ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{ Replaces GetSpeechInfo}
{
 *  CopySpeechProperty()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CopySpeechProperty( chan: SpeechChannel; property: CFStringRef; var objct: CFTypeRef ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{ Replaces SetSpeechInfo}
{
 *  SetSpeechProperty()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SetSpeechProperty( chan: SpeechChannel; property: CFStringRef; objct: CFTypeRef ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{ Support loading and unloading synthesizers and voices from locations other than the standard directories.}
{
 *  SpeechSynthesisRegisterModuleURL()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.6 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SpeechSynthesisRegisterModuleURL( url: CFURLRef ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;


{
 *  SpeechSynthesisUnregisterModuleURL()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.6 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SpeechSynthesisUnregisterModuleURL( url: CFURLRef ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;


{$endc} {TARGET_OS_MAC}


end.
