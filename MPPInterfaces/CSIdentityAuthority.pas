{
     File:       OSServices/CSIdentityAuthority.h
 
     Contains:   CSIdentityAuthority APIs
 
     Copyright:  (c) 2006-2011 Apple Inc. All rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit CSIdentityAuthority;
interface
uses MacTypes,CFBase;

{$ALIGN POWER}


{
 *  CSIdentityAuthorityRef
 *  
 *  Discussion:
 *    A reference to an identity authority object. An identity
 *    authority is a logical repository for identities.
 }
type
	CSIdentityAuthorityRef = ^__CSIdentityAuthority; { an opaque type }
	__CSIdentityAuthority = record end;
{
 *  CSIdentityAuthorityGetTypeID()
 *  
 *  Summary:
 *    Returns the CSIdentityAuthority type identifier
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Result:
 *    The CFTypeID of the CSIdentityAuthority Core Foundation type
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityAuthorityGetTypeID: CFTypeID;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_5_0);


{$ifc not TARGET_OS_IPHONE and not TARGET_IPHONE_SIMULATOR}
{
 *  CSGetDefaultIdentityAuthority()
 *  
 *  Summary:
 *    Returns the system's default identity authority
 *  
 *  Discussion:
 *    The default identity authority is a pseudo-authority representing
 *    the union of the local identity authority and the managed
 *    identity authority. The function CSIdentityGetAuthority will
 *    never return the default authority instance.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Result:
 *    The CSIdentityAuthorityRef of the default authority
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSGetDefaultIdentityAuthority: CSIdentityAuthorityRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);


{
 *  CSGetLocalIdentityAuthority()
 *  
 *  Summary:
 *    Returns the identity authority for identities defined on the
 *    local host
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Result:
 *    The CSIdentityAuthorityRef of the local authority
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSGetLocalIdentityAuthority: CSIdentityAuthorityRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);


{
 *  CSGetManagedIdentityAuthority()
 *  
 *  Summary:
 *    Returns the identity authority for identities defined in the
 *    system's managed directory server(s)
 *  
 *  Discussion:
 *    There is always a valid managed identity authority instance, but
 *    if the system is not bound to any managed directory servers, the
 *    managed identity authority will contain no identities.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Result:
 *    The CSIdentityAuthorityRef of the managed authority
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSGetManagedIdentityAuthority: CSIdentityAuthorityRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);
{$endc} {!TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR}

{
 *  CSIdentityAuthorityCopyLocalizedName()
 *  
 *  Summary:
 *    Returns the localized name of an identity authority
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    authority:
 *      The identity authority to access
 *  
 *  Result:
 *    A CFStringRef containing the localized authority name
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CSIdentityAuthorityCopyLocalizedName( authority: CSIdentityAuthorityRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_5_0);


end.
