{
     File:       SecurityHI/KeychainHI.h
 
     Contains:   Keychain API's with Human Interfaces
 
     Version:    SecurityHI-55002~751
 
     Copyright:  � 2000-2008 by Apple Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit KeychainHI;
interface
uses MacTypes,CFBase,KeychainCore,CFString,CFArray,CFDate;

{$ifc TARGET_OS_MAC}

{ High-level interface for storing passwords }
{
 *  KCAddAppleSharePassword()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainAddInternetPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
// overloading not available
// function KCAddAppleSharePassword( var serverSignature: AFPServerSignature; serverAddress: StringPtr; serverName: StringPtr; volumeName: StringPtr; accountName: StringPtr; passwordLength: UInt32; passwordData: {const} univ Ptr; item: KCItemRefPtr { can be NULL } ): OSStatus;
// AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  KCAddInternetPassword()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainAddInternetPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
// overloading not available
// function KCAddInternetPassword( serverName: StringPtr; securityDomain: StringPtr; accountName: StringPtr; port: UInt16; protocol: OSType; authType: OSType; passwordLength: UInt32; passwordData: {const} univ Ptr; item: KCItemRefPtr { can be NULL } ): OSStatus;
// AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  KCAddInternetPasswordWithPath()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainAddInternetPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
// overloading not available
// function KCAddInternetPasswordWithPath( serverName: StringPtr; securityDomain: StringPtr; accountName: StringPtr; path: StringPtr; port: UInt16; protocol: OSType; authType: OSType; passwordLength: UInt32; passwordData: {const} univ Ptr; item: KCItemRefPtr { can be NULL } ): OSStatus;
// AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  KCAddGenericPassword()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainAddGenericPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
// overloading not available
// function KCAddGenericPassword( serviceName: StringPtr; accountName: StringPtr; passwordLength: UInt32; passwordData: {const} univ Ptr; item: KCItemRefPtr { can be NULL } ): OSStatus;
// AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{ Low-level interface for storing keychain items }
{
 *  KCAddItem()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemCreateFromContent
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCAddItem( item: KCItemRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{ Managing keychains }
{
 *  KCUnlock()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainUnlock
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
// overloading not available
//function KCUnlock( keychain: KCRef { can be NULL }; password: StringPtr { can be NULL } ): OSStatus;
//AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  KCCreateKeychain()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainCreate
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
// overloading not available
// function KCCreateKeychain( password: StringPtr { can be NULL }; keychain: KCRefPtr { can be NULL } ): OSStatus;
// AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  KCChangeSettings()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainSetSettings
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCChangeSettings( keychain: KCRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  kcunlock()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainUnlock
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function kcunlock( keychain: KCRef { can be NULL }; password: ConstCStringPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  kccreatekeychain()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainCreate
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function kccreatekeychain( password: ConstCStringPtr { can be NULL }; keychain: KCRefPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  kcaddapplesharepassword()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainAddInternetPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function kcaddapplesharepassword( var serverSignature: AFPServerSignature; serverAddress: ConstCStringPtr; serverName: ConstCStringPtr; volumeName: ConstCStringPtr; accountName: ConstCStringPtr; passwordLength: UInt32; passwordData: {const} univ Ptr; item: KCItemRefPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  kcaddinternetpassword()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainAddInternetPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function kcaddinternetpassword( serverName: ConstCStringPtr; securityDomain: ConstCStringPtr; accountName: ConstCStringPtr; port: UInt16; protocol: OSType; authType: OSType; passwordLength: UInt32; passwordData: {const} univ Ptr; item: KCItemRefPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  kcaddinternetpasswordwithpath()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainAddInternetPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function kcaddinternetpasswordwithpath( serverName: ConstCStringPtr; securityDomain: ConstCStringPtr; accountName: ConstCStringPtr; path: ConstCStringPtr; port: UInt16; protocol: OSType; authType: OSType; passwordLength: UInt32; passwordData: {const} univ Ptr; item: KCItemRefPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{
 *  kcaddgenericpassword()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainAddGenericPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function kcaddgenericpassword( serviceName: ConstCStringPtr; accountName: ConstCStringPtr; passwordLength: UInt32; passwordData: {const} univ Ptr; item: KCItemRefPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_6;


{ Working with certificates }
{
 *  KCFindX509Certificates()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }


{
 *  KCChooseCertificate()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }

{$endc} {TARGET_OS_MAC}

end.
