{
     File:       CarbonCore/ToolUtils.h
 
     Contains:   Toolbox Utilities Interfaces.
                 The contents of this header file are deprecated.
 
     Copyright:  © 1990-2011 by Apple Inc. All rights reserved.
}
unit ToolUtils;
interface
uses MacTypes,FixMath,TextUtils,IconsCore,QuickdrawTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{
ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
    Note: 
    
    The following routines that used to be in this header file, have moved to
    more appropriate headers.  
    
        FixMath.h:      FixMul
                        FixRatio
                        FixRound
        
        Icons.h:        GetIcon
                        PlotIcon
                        
        Quickdraw.h:    AngleFromSlope
                        DeltaPoint
                        GetCursor
                        GetIndPattern
                        GetPattern
                        GetPicture
                        PackBits
                        ScreenRes
                        ShieldCursor
                        SlopeFromAngle
                        UnpackBits
                        
        TextUtils.h:    Munger
                        GetIndString
                        GetString
                        NewString
                        SetString
ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
}

{
 *  BitTst()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function BitTst( bytePtr: {const} univ Ptr; bitNum: SIGNEDLONG ): Boolean;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  BitSet()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure BitSet( bytePtr: univ Ptr; bitNum: SIGNEDLONG );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  BitClr()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure BitClr( bytePtr: univ Ptr; bitNum: SIGNEDLONG );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  BitAnd()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function BitAnd( value1: SIGNEDLONG; value2: SIGNEDLONG ): SIGNEDLONG;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  BitOr()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function BitOr( value1: SIGNEDLONG; value2: SIGNEDLONG ): SIGNEDLONG;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  BitXor()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function BitXor( value1: SIGNEDLONG; value2: SIGNEDLONG ): SIGNEDLONG;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  BitNot()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function BitNot( value: SIGNEDLONG ): SIGNEDLONG;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  BitShift()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function BitShift( value: SIGNEDLONG; count: SInt16 ): SIGNEDLONG;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{
   HiWord and LoWord are not in Carbon, but Metrowerks 
   supplies an implementation on PPC
}
{
 *  HiWord()
 *  
 *  Availability:
 *    Non-Carbon CFM:   not available
 *    CarbonLib:        in CarbonLib H.a.c.k and later
 *    Mac OS X:         not available
 }

{
 *  LoWord()
 *  
 *  Availability:
 *    Non-Carbon CFM:   not available
 *    CarbonLib:        in CarbonLib H.a.c.k and later
 *    Mac OS X:         not available
 }

{FPC-ONLY-START}
implemented function HiWord(arg: SInt32): SInt16; inline; overload;
implemented function HiWord(arg: UInt32): UInt16; inline; overload;
implemented function LoWord(arg: SInt32): SInt16; inline; overload;
implemented function LoWord(arg: UInt32): UInt16; inline; overload;
{FPC-ONLY-FINISH}


{MW-GPC-ONLY-START}
{$mwgpcdefinec HiWord( arg ) (SInt16(SInt32(arg) shr 16))}
{$mwgpcdefinec LoWord( arg ) (SInt16(arg and $ffff))}
{MW-GPC-ONLY-FINISH}

{$ALIGN MAC68K}

{$endc} {TARGET_OS_MAC}

implementation

{$ifc TARGET_OS_MAC}

{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

{FPC-ONLY-START}

function HiWord(arg: SInt32): SInt16; inline;
begin
  HiWord := arg shr 16;
end;


function HiWord(arg: UInt32): UInt16; inline;
begin
  HiWord := arg shr 16;
end;


function LoWord(arg: SInt32): SInt16; inline;
begin
  LoWord := SInt16(arg);
end;


function LoWord(arg: UInt32): UInt16; inline;
begin
  LoWord := UInt16(arg);
end;
  
{FPC-ONLY-FINISH}

{$endc} {TARGET_OS_MAC}

end.
