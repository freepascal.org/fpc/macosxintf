{
     File:       QD/QuickdrawAPI.h and ColorSync/CMTypes.h
 
     Contains:   Definitions shared between these two headers.
 
     Version:    Quickdraw-262~1
 
     Copyright:  © 2005-2008 by Apple Inc. all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
unit QDCMCommon;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{ Caller-supplied progress function for Bitmap & PixMap matching routines }
type
	CMBitmapCallBackProcPtr = function( progress: SIGNEDLONG; refCon: univ Ptr ): Boolean;
{GPC-ONLY-START}
	CMBitmapCallBackUPP = UniversalProcPtr; // should be CMBitmapCallBackProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	CMBitmapCallBackUPP = CMBitmapCallBackProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	CMBitmapCallBackUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{
 *  NewCMBitmapCallBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewCMBitmapCallBackUPP( userRoutine: CMBitmapCallBackProcPtr ): CMBitmapCallBackUPP;

{
 *  DisposeCMBitmapCallBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeCMBitmapCallBackUPP( userUPP: CMBitmapCallBackUPP );

{
 *  InvokeCMBitmapCallBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeCMBitmapCallBackUPP( progress: SIGNEDLONG; refCon: univ Ptr; userUPP: CMBitmapCallBackUPP ): Boolean;

{$endc} {TARGET_OS_MAC}

end.