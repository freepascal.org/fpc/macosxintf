{
 * ColorSync - ColorSyncProfile.h
 * Copyright (c)  2008 Apple Inc.
 * All rights reserved.
 }
{  Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ColorSyncProfile;
interface
uses MacTypes,CFBase,CFArray,CFData,CFDictionary,CFError,CFUUID;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


type
	ColorSyncProfileRef = ^OpaqueColorSyncProfileRef; { an opaque type }
	OpaqueColorSyncProfileRef = record end;

type
	ColorSyncProfile_ = record end;
	ColorSyncMutableProfileRef = ^ColorSyncProfile_;

const kColorSyncGenericGrayProfile: CFStringRef;          { com.apple.ColorSync.GenericGray  }
const kColorSyncGenericGrayGamma22Profile: CFStringRef;   { com.apple.ColorSync.GenericGrayGamma2.2  }
const kColorSyncGenericRGBProfile: CFStringRef;           { com.apple.ColorSync.GenericRGB   }
const kColorSyncGenericCMYKProfile: CFStringRef;          { com.apple.ColorSync.GenericCMYK  }
const kColorSyncSRGBProfile: CFStringRef;                 { com.apple.ColorSync.sRGB         }
const kColorSyncAdobeRGB1998Profile: CFStringRef;         { com.apple.ColorSync.AdobeRGB1998 }
const kColorSyncGenericLabProfile: CFStringRef;           { com.apple.ColorSync.GenericLab }
const kColorSyncGenericXYZProfile: CFStringRef;           { com.apple.ColorSync.GenericXYZ }

const kColorSyncProfileHeader: CFStringRef;      { com.apple.ColorSync.ProfileHeader }
const kColorSyncProfileClass: CFStringRef;       { com.apple.ColorSync.ProfileClass }
const kColorSyncProfileColorSpace: CFStringRef;  { com.apple.ColorSync.ProfileColorSpace }
const kColorSyncProfilePCS: CFStringRef;         { com.apple.ColorSync.PCS }
const kColorSyncProfileURL: CFStringRef;         { com.apple.ColorSync.ProfileURL }
const kColorSyncProfileDescription: CFStringRef; { com.apple.ColorSync.ProfileDescription }
const kColorSyncProfileMD5Digest: CFStringRef;   { com.apple.ColorSync.ProfileMD5Digest }

function ColorSyncProfileGetTypeID: CFTypeID;
   {
    * returns the CFTypeID for ColorSyncProfiles.
    }

function ColorSyncProfileCreate( data: CFDataRef; var error: CFErrorRef ): ColorSyncProfileRef;
   {
    *   data   - profile data
    *   error  - (optional) pointer to the error that will be returned in case of failure
    *   
    *   returns ColorSyncProfileRef or NULL in case of failure
    }

function ColorSyncProfileCreateWithURL( url: CFURLRef; var error: CFErrorRef ): ColorSyncProfileRef;
   {
    *   url    - URL to the profile data (caller needs to have privileges to read url).
    *   error  - (optional) pointer to the error that will be returned in case of failure
    *   
    *   returns ColorSyncProfileRef or NULL in case of failure
    }

function ColorSyncProfileCreateWithName( name: CFStringRef ): ColorSyncProfileRef;
   {
    *   name    - predefined profile name
    *   
    *   returns ColorSyncProfileRef or NULL in case of failure
    }

function ColorSyncProfileCreateWithDisplayID( displayID: UInt32 ): ColorSyncProfileRef;
   {
    *   displayID - system-wide unique display ID (defined by IOKIt); pass 0 for main display.
    *   
    *   returns ColorSyncProfileRef or NULL in case of failure
    }

function ColorSyncProfileCreateDeviceProfile( deviceClass: CFStringRef; deviceID: CFUUIDRef; profileID: CFTypeRef ): ColorSyncProfileRef;
   {
    *   deviceClass - ColorSync device class 
    *   deviceID    - deviceID registered with ColorSync
    *   profileID   - profileID registered with ColorSync; pass kColorSyncDeviceDefaultProfileID to get the default profile.
    *   
    *   See ColorSyncDevice.h for more info on deviceClass, deviceID and profileID
    *
    *   returns ColorSyncProfileRef or NULL in case of failure
    }

function ColorSyncProfileCreateMutable: ColorSyncMutableProfileRef;
   {
    *   returns empty ColorSyncMutableProfileRef or NULL in case of failure
    }

function ColorSyncProfileCreateMutableCopy( prof: ColorSyncProfileRef ): ColorSyncMutableProfileRef;
   {
    *  prof  - profile from which profile data will be copied to the created profile.
    *   
    *   returns ColorSyncMutableProfileRef or NULL in case of failure
    }

function ColorSyncProfileCreateLink( profileInfo: CFArrayRef; options: CFDictionaryRef ): ColorSyncProfileRef;
   {
    *   profileInfo  - array of dictionaries, each one containing a profile object and the
    *                       information on the usage of the profile in the transform.
    *               
    *               Required keys:
    *               ==============
    *                      kColorSyncProfile           : ColorSyncProfileRef
    *                      kColorSyncRenderingIntent   : CFStringRef defining rendering intent
    *                      kColorSyncTransformTag      : CFStringRef defining which tags to use 
    *               Optional key:
    *               =============
    *                    kColorSyncBlackPointCompensation : CFBooleanRef to enable/disable BPC
    *   
    *   options      - dictionary with additional public global options (e.g. preferred CMM, quality,
    *                       etc... It can also contain custom options that are CMM specific.
    *
    *   returns ColorSyncProfileRef or NULL in case of failure
    }

function ColorSyncProfileVerify( prof: ColorSyncProfileRef; var errors: CFErrorRef; var warnings: CFErrorRef ): CBool;
   {
    *   prof    - profile to be verified
    *
    *   errors  - returns error strings in case problems are found which 
    *                  would prevent use of the profile.
    *  
    *   warnings - returns warning strings indicating problems due to lack of
    *                       conformance with the ICC specification, but not preventing
    *                       use of the profile.
    *
    *   returns true if profile can be used or false otherwise
    }

function ColorSyncProfileEstimateGammaWithDisplayID( {const} displayID: SInt32; var error: CFErrorRef ): Float32;
   {
    *   displayID - system-wide unique display ID (defined by IOKIt)
    *   error     - (optional) pointer to the error that will be returned in
    *               case of failure
    *   
    *   returns non-zero value if success or 0.0 in case of error.
    }

function ColorSyncProfileEstimateGamma( prof: ColorSyncProfileRef; var error: CFErrorRef ): Float32;
    {
    *   prof    - profile to perform estimation on
    *   error   - (optional) pointer to the error that will be returned in
    *             case of failure
    *   
    *   returns non-zero value if success or 0.0 in case of error
    }

const
	COLORSYNC_MD5_LENGTH = 16;

type
	ColorSyncMD5 = record
		digest: array [0..COLORSYNC_MD5_LENGTH-1] of 	UInt8;
	end;

function ColorSyncProfileGetMD5( prof: ColorSyncProfileRef ): ColorSyncMD5;
   { 
    *   returns MD5 digest for the profile calculated as defined by
    *           ICC specification or a "zero" signature (filled with zeros)
    *           in case of failure
    }

function ColorSyncProfileCopyData( prof: ColorSyncProfileRef; var error: CFErrorRef ): CFDataRef;
   {
    *   prof    - profile to copy the flattened data from
    *   error  - (optional) pointer to the error that will be returned in case of failure
    *   
    *   returns CFDataRef if success or NULL in case of failure 
    }

function ColorSyncProfileGetURL( prof: ColorSyncProfileRef; var error: CFErrorRef ): CFURLRef;
   {
    *   prof   - profile to get URL from
    *   error  - (optional) pointer to the error that will be returned in case of failure
    *   
    *   returns CFURLRef if success or NULL in case of failure 
    }

function ColorSyncProfileCopyHeader( prof: ColorSyncProfileRef ): CFDataRef;
   {
    *   prof    - profile from which to copy the header
    *   
    *   returns CFDataRef containing profile header (in host endianess) or NULL in case of failure 
    }

procedure ColorSyncProfileSetHeader( prof: ColorSyncMutableProfileRef; header: CFDataRef );
   {
    *   prof        - profile in which to set the header
    *   header  - CFDataRef containing the header data (must be in host endianess)
    }

function ColorSyncProfileCopyDescriptionString( prof: ColorSyncProfileRef ): CFStringRef;
   {
    *   prof    - profile from which to copy description string
    *   
    *   returns CFStringRef containing profile description localized to current locale
    }

function ColorSyncProfileCopyTagSignatures( prof: ColorSyncProfileRef ): CFArrayRef;
   {
    *   prof    - profile from which to copy tag signatures
    *   
    *   returns CFArray with signatures (CFStringRef) of tags in the profile 
    }

function ColorSyncProfileContainsTag( prof: ColorSyncProfileRef; signature: CFStringRef ): CBool;
   {
    *   prof        - profile in which to search for the tag
    *   signature   - signature of the tag to be searched for
    *   
    *   returns true if tag exists or false if does not 
    }

function ColorSyncProfileCopyTag( prof: ColorSyncProfileRef; signature: CFStringRef ): CFDataRef;
   {
    *   prof             - profile from which to copy the tag
    *   signature   - signature of the tag to be copied
    *   
    *   returns CFDataRef containing tag data or NULL in case of failure 
    }

procedure ColorSyncProfileSetTag( prof: ColorSyncMutableProfileRef; signature: CFStringRef; data: CFDataRef );
   {
    *   prof           - profile in which to set the tag
    *   signature - signature of the tag to be set in the profile
    *   data          - CFDataRef containing the tag data
    }

procedure ColorSyncProfileRemoveTag( prof: ColorSyncMutableProfileRef; signature: CFStringRef );
   {
    *   prof              - profile from which to remove the tag
    *   signature    - signature of the tag to be removed
    *   
    *   returns true if success or false in case of failure 
    }

function ColorSyncProfileGetDisplayTransferFormulaFromVCGT( profile: ColorSyncProfileRef; var redMin: Float32; var redMax: Float32; var redGamma: Float32; var greenMin: Float32; var greenMax: Float32; var greenGamma: Float32; var blueMin: Float32; var blueMax: Float32; var blueGamma: Float32 ): CBool;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;
   {
    * An utility function converting vcgt tag (if vcgt tag exists in the profile and conversion possible)
    * to formula components used by CGSetDisplayTransferByFormula.
    }

function ColorSyncProfileCreateDisplayTransferTablesFromVCGT( profile: ColorSyncProfileRef; var nSamplesPerChannel: size_t ): CFDataRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;
   {
    * An utility function creating three tables of floats (redTable, greenTable, blueTable)
    * each of size nSamplesPerChannel, packed into contiguous memory contained in the CFDataRef
    *  to be returned from the vcgt tag of the profile (if vcgt tag exists in the profile).
    * Used by CGSetDisplayTransferByTable.
    }


type
	ColorSyncProfileIterateCallback = function( profileInfo: CFDictionaryRef; userInfo: univ Ptr ): CBool;
   {
    * Notes:
    *   1. Only validated profiles will be passed to the caller
    *   2. if the ColorSyncProfileIterateCallback returns false, the iteration stops
    }

procedure ColorSyncIterateInstalledProfiles( callBack: ColorSyncProfileIterateCallback; seed: UInt32Ptr; userInfo: univ Ptr; var error: CFErrorRef );
   {
    * callBack - pointer to a client provided function (can be NULL)
    * seed     - pointer to a cache seed owned by the client (can be NULL)
    * error    - (optional) pointer to the error that will be returned in case of failure
    *
    }

{$endc} {TARGET_OS_MAC}

end.
