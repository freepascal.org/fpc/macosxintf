{
 *  QLBase.h
 *  Quick Look
 *
 *  Copyright 2007-2010 Apple Inc.
 *  All rights reserved.
 *
 }
{ Pascal Translation: Gorazd Krosl <gorazd_1957@yahoo.ca>, November 2009 }
{ Pascal Translation updated: Jonas Maebe <jonas@freepascal.org>, October 2012 }
unit QLBase;
interface
uses MacTypes;
{$ALIGN POWER}

{$ifc TARGET_OS_MAC}


const
	QUICKLOOK_VERSION = 5550;

{$endc} {TARGET_OS_MAC}

end.
