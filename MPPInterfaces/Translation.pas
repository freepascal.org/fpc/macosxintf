{
     File:       HIToolbox/Translation.h
 
     Contains:   Translation Manager (Macintosh Easy Open) Interfaces.
 
     Version:    HIToolbox-624~3
 
     Copyright:  � 1991-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit Translation;
interface
uses MacTypes,Files,Components,TranslationExtensions;

{$ifc TARGET_OS_MAC}
{$ALIGN MAC68K}

{
   Carbon clients should use Translation Services. The definitions below will NOT work for Carbon and
   are only defined for those files that need to build pre-Carbon applications.
}
{ enumerated types on how a document can be opened}
type
	DocOpenMethod = SInt16;
const
	domCannot = 0;
	domNative = 1;
	domTranslateFirst = 2;
	domWildcard = 3;

{ 0L terminated array of OSTypes, or FileTypes}
type
	TypesBlock = array [0..63] of OSType;
	TypesBlockPtr = ^TypesBlock;
{ Progress dialog resource ID}
const
	kTranslationScrapProgressDialogID = -16555;

{ block of data that describes how to translate}
type
	FileTranslationSpecPtr = ^FileTranslationSpec;
	FileTranslationSpec = record
		componentSignature: OSType;
		translationSystemInfo: {const} UnivPtr;
		src: FileTypeSpec;
		dst: FileTypeSpec;
	end;
type
	FileTranslationSpecArray = array [0..$7F000000 div SizeOf(FileTranslationSpec)-1] of FileTranslationSpec;
	FileTranslationSpecArrayPtr = ^FileTranslationSpecArray;
	FileTranslationSpecArrayHandle = ^FileTranslationSpecArrayPtr;
{$ifc not TARGET_CPU_64}
{
 *  GetFileTypesThatAppCanNativelyOpen()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    There is no direct replacement at this time.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function GetFileTypesThatAppCanNativelyOpen( appVRefNumHint: SInt16; appSignature: OSType; var nativeTypes: TypesBlock ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{
 *  ExtendFileTypeList()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use TranslationCreateWithSourceArray instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function ExtendFileTypeList( originalTypeList: {variable-size-array} FileTypePtr; numberOriginalTypes: SInt16; extendedTypeList: {variable-size-array} FileTypePtr; var numberExtendedTypes: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{
 *  CanDocBeOpened()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use the Launch Services API instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function CanDocBeOpened( const var targetDocument: FSSpec; appVRefNumHint: SInt16; appSignature: OSType; nativeTypes: {variable-size-array} FileTypePtr; onlyNative: Boolean; var howToOpen: DocOpenMethod; var howToTranslate: FileTranslationSpec ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{
 *  GetFileTranslationPaths()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use TranslationCreateWithSourceArray instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function GetFileTranslationPaths( const var srcDocument: FSSpec; dstDocType: FileType; maxResultCount: UInt16; resultBuffer: FileTranslationSpecArrayPtr ): SInt16;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{
 *  GetPathFromTranslationDialog()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    There is no direct replacement at this time, but all the
 *    necessary information can be obtained from the Launch Services,
 *    Translation Services and Uniform Type Identification APIs.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function GetPathFromTranslationDialog( const var theDocument: FSSpec; const var theApplication: FSSpec; typeList: TypesBlockPtr; var howToOpen: DocOpenMethod; var howToTranslate: FileTranslationSpec ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{
 *  TranslateFile()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use TranslationPerformForFile or TranslationPerformForURL instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function TranslateFile( const var sourceDocument: FSSpec; const var destinationDocument: FSSpec; const var howToTranslate: FileTranslationSpec ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{
 *  GetDocumentKindString()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use the Launch Services API instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function GetDocumentKindString( docVRefNum: SInt16; docType: OSType; docCreator: OSType; var kindString: Str63 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{
 *  GetTranslationExtensionName()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    There is no direct replacement at this time, but
 *    TranslationCopySourceType and TranslationCopyDestinationType in
 *    conjunction with UTTypeCopyDescription will provide useful user
 *    level information.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function GetTranslationExtensionName( const var translationMethod: FileTranslationSpec; var extensionName: Str31 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{$endc} {not TARGET_CPU_64}


{
 *  GetScrapDataProcPtr
 }
type
	GetScrapDataProcPtr = function( requestedFormat: ScrapType; dataH: Handle; srcDataGetterRefCon: univ Ptr ): OSErr;
{GPC-ONLY-START}
	GetScrapDataUPP = UniversalProcPtr; // should be GetScrapDataProcPtr
{GPC-ONLY-ELSE}
	GetScrapDataUPP = GetScrapDataProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewGetScrapDataUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewGetScrapDataUPP( userRoutine: GetScrapDataProcPtr ): GetScrapDataUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;

{
 *  DisposeGetScrapDataUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeGetScrapDataUPP( userUPP: GetScrapDataUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;

{
 *  InvokeGetScrapDataUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeGetScrapDataUPP( requestedFormat: ScrapType; dataH: Handle; srcDataGetterRefCon: univ Ptr; userUPP: GetScrapDataUPP ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;

type
	GetScrapData = GetScrapDataUPP;
{$ifc not TARGET_CPU_64}
{
 *  TranslateScrap()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use TranslationPerformForData instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function TranslateScrap( sourceDataGetter: GetScrapDataUPP; sourceDataGetterRefCon: univ Ptr; destinationFormat: ScrapType; destinationData: Handle; progressDialogID: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{$endc} {not TARGET_CPU_64}


{$endc} {TARGET_OS_MAC}


end.
