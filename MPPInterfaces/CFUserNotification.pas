{	CFUserNotification.h
	Copyright (c) 2000-2013, Apple Inc.  All rights reserved.
}
unit CFUserNotification;
interface
uses MacTypes,CFBase,CFDate,CFDictionary,CFString,CFURL,CFRunLoop;
{$ALIGN POWER}


type
	CFUserNotificationRef = ^__CFUserNotification; { an opaque type }
	__CFUserNotification = record end;

{ A CFUserNotification is a notification intended to be presented to a 
user at the console (if one is present).  This is for the use of processes
that do not otherwise have user interfaces, but may need occasional
interaction with a user.  There is a parallel API for this functionality
at the System framework level, described in UNCUserNotification.h.

The contents of the notification can include a header, a message, textfields,
a popup button, radio buttons or checkboxes, a progress indicator, and up to
three ordinary buttons.  All of these items are optional, but a default
button will be supplied even if not specified unless the
kCFUserNotificationNoDefaultButtonFlag is set.

The contents of the notification are specified in the dictionary used to
create the notification, whose keys should be taken from the list of constants
below, and whose values should be either strings or arrays of strings
(except for kCFUserNotificationProgressIndicatorValueKey, in which case the
value should be a number between 0 and 1, for a "definite" progress indicator,
or a boolean, for an "indefinite" progress indicator).  Additionally, URLs can
optionally be supplied for an icon, a sound, and a bundle whose Localizable.strings
files will be used to localize strings.
    
Certain request flags are specified when a notification is created.
These specify an alert level for the notification, determine whether
radio buttons or check boxes are to be used, specify which if any of these
are checked by default, specify whether any of the textfields are to
be secure textfields, and determine which popup item should be selected
by default.  A timeout is also specified, which determines how long the
notification should be supplied to the user (if zero, it will not timeout).
    
A CFUserNotification is dispatched for presentation when it is created.
If any reply is required, it may be awaited in one of two ways:  either
synchronously, using CFUserNotificationReceiveResponse, or asynchronously,
using a run loop source.  CFUserNotificationReceiveResponse has a timeout
parameter that determines how long it will block (zero meaning indefinitely)
and it may be called as many times as necessary until a response arrives.
If a notification has not yet received a response, it may be updated with
new information, or it may be cancelled.  Notifications may not be reused.
    
When a response arrives, it carries with it response flags that describe
which button was used to dismiss the notification, which checkboxes or
radio buttons were checked, and what the selection of the popup was.
It also carries a response dictionary, which describes the contents
of the textfields.  }
    
type
	CFUserNotificationCallBack = procedure( userNotification: CFUserNotificationRef; responseFlags: CFOptionFlags );

function CFUserNotificationGetTypeID: CFTypeID;

function CFUserNotificationCreate( allocator: CFAllocatorRef; timeout: CFTimeInterval; flags: CFOptionFlags; var error: SInt32; dictionary: CFDictionaryRef ): CFUserNotificationRef;

function CFUserNotificationReceiveResponse( userNotification: CFUserNotificationRef; timeout: CFTimeInterval; var responseFlags: CFOptionFlags ): SInt32;

function CFUserNotificationGetResponseValue( userNotification: CFUserNotificationRef; key: CFStringRef; idx: CFIndex ): CFStringRef;

function CFUserNotificationGetResponseDictionary( userNotification: CFUserNotificationRef ): CFDictionaryRef;

function CFUserNotificationUpdate( userNotification: CFUserNotificationRef; timeout: CFTimeInterval; flags: CFOptionFlags; dictionary: CFDictionaryRef ): SInt32;

function CFUserNotificationCancel( userNotification: CFUserNotificationRef ): SInt32;

function CFUserNotificationCreateRunLoopSource( allocator: CFAllocatorRef; userNotification: CFUserNotificationRef; callout: CFUserNotificationCallBack; order: CFIndex ): CFRunLoopSourceRef;

{ Convenience functions for handling the simplest and most common cases:  
a one-way notification, and a notification with up to three buttons. }
    
function CFUserNotificationDisplayNotice( timeout: CFTimeInterval; flags: CFOptionFlags; iconURL: CFURLRef; soundURL: CFURLRef; localizationURL: CFURLRef; alertHeader: CFStringRef; alertMessage: CFStringRef; defaultButtonTitle: CFStringRef ): SInt32;

function CFUserNotificationDisplayAlert( timeout: CFTimeInterval; flags: CFOptionFlags; iconURL: CFURLRef; soundURL: CFURLRef; localizationURL: CFURLRef; alertHeader: CFStringRef; alertMessage: CFStringRef; defaultButtonTitle: CFStringRef; alternateButtonTitle: CFStringRef; otherButtonTitle: CFStringRef; var responseFlags: CFOptionFlags ): SInt32;


{ Flags }

const
	kCFUserNotificationStopAlertLevel = 0;
	kCFUserNotificationNoteAlertLevel = 1;
	kCFUserNotificationCautionAlertLevel = 2;
	kCFUserNotificationPlainAlertLevel = 3;

const
	kCFUserNotificationDefaultResponse = 0;
	kCFUserNotificationAlternateResponse = 1;
	kCFUserNotificationOtherResponse = 2;
	kCFUserNotificationCancelResponse = 3;

const
	kCFUserNotificationNoDefaultButtonFlag = 1 shl 5;
	kCFUserNotificationUseRadioButtonsFlag = 1 shl 6;

{FPC-ONLY-START}
implemented function CFUserNotificationCheckBoxChecked( i: CFIndex ): CFOptionFlags; inline;
implemented function CFUserNotificationSecureTextField( i: CFIndex ): CFOptionFlags; inline;
implemented function CFUserNotificationPopUpSelection( n: CFIndex ): CFOptionFlags; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec CFUserNotificationCheckBoxChecked( i ) CFOptionFlags(1 shl (8+(i)))}
{$mwgpcdefinec CFUserNotificationSecureTextField( i ) CFOptionFlags(1 shl (16+(i)))}
{$mwgpcdefinec CFUserNotificationPopUpSelection( n ) CFOptionFlags((n) shl 24)}
{FPC-ONLY-FINISH}


{ Keys }

const kCFUserNotificationIconURLKey: CFStringRef;

const kCFUserNotificationSoundURLKey: CFStringRef;

const kCFUserNotificationLocalizationURLKey: CFStringRef;

const kCFUserNotificationAlertHeaderKey: CFStringRef;

const kCFUserNotificationAlertMessageKey: CFStringRef;

const kCFUserNotificationDefaultButtonTitleKey: CFStringRef;

const kCFUserNotificationAlternateButtonTitleKey: CFStringRef;

const kCFUserNotificationOtherButtonTitleKey: CFStringRef;

const kCFUserNotificationProgressIndicatorValueKey: CFStringRef;

const kCFUserNotificationPopUpTitlesKey: CFStringRef;

const kCFUserNotificationTextFieldTitlesKey: CFStringRef;

const kCFUserNotificationCheckBoxTitlesKey: CFStringRef;

const kCFUserNotificationTextFieldValuesKey: CFStringRef;

{#if MAC_OS_X_VERSION_10_3 <= MAC_OS_X_VERSION_MAX_ALLOWED}
const kCFUserNotificationPopUpSelectionKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
{#endif}

{$ifc TARGET_OS_IPHONE}
const kCFUserNotificationAlertTopMostKey: CFStringRef;
        
const kCFUserNotificationKeyboardTypesKey: CFStringRef;
{$endc}

implementation

{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

{FPC-ONLY-START}
{$R-}

function CFUserNotificationCheckBoxChecked( i: CFIndex ): CFOptionFlags; inline;
begin
	CFUserNotificationCheckBoxChecked := CFOptionFlags(1 shl (8+i));
end;

function CFUserNotificationSecureTextField( i: CFIndex ): CFOptionFlags; inline;
begin
	CFUserNotificationSecureTextField := CFOptionFlags(1 shl (16+i));
end;

function CFUserNotificationPopUpSelection( n: CFIndex ): CFOptionFlags; inline;
begin
	CFUserNotificationPopUpSelection := CFOptionFlags(n shl 24);
end;

{FPC-ONLY-FINISH}
end.
