{	CFLocale.h
	Copyright (c) 2002-2013, Apple Inc. All rights reserved.
}
unit CFLocale;
interface
uses MacTypes,CFBase,CFArray,CFDictionary;
{$ALIGN POWER}


{#if MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_3}


type
	CFLocaleRef = ^__CFLocale; { an opaque type }
	__CFLocale = record end;

function CFLocaleGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFLocaleGetSystem: CFLocaleRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Returns the "root", canonical locale.  Contains fixed "backstop" settings.

function CFLocaleCopyCurrent: CFLocaleRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Returns the logical "user" locale for the current user.
	// [This is Copy in the sense that you get a retain you have to release,
	// but we may return the same cached object over and over.]  Settings
	// you get from this locale do not change under you as CFPreferences
	// are changed (for safety and correctness).  Generally you would not
	// grab this and hold onto it forever, but use it to do the operations
	// you need to do at the moment, then throw it away.  (The non-changing
	// ensures that all the results of your operations are consistent.)

function CFLocaleCopyAvailableLocaleIdentifiers: CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Returns an array of CFStrings that represents all locales for
	// which locale data is available.

function CFLocaleCopyISOLanguageCodes: CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Returns an array of CFStrings that represents all known legal ISO
	// language codes.  Note: many of these will not have any supporting
	// locale data in Mac OS X.

function CFLocaleCopyISOCountryCodes: CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Returns an array of CFStrings that represents all known legal ISO
	// country codes.  Note: many of these will not have any supporting
	// locale data in Mac OS X.

function CFLocaleCopyISOCurrencyCodes: CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Returns an array of CFStrings that represents all known legal ISO
	// currency codes.  Note: some of these currencies may be obsolete, or
	// represent other financial instruments.

function CFLocaleCopyCommonISOCurrencyCodes: CFArrayRef;
CF_AVAILABLE_STARTING(10_5, 2_0);
	// Returns an array of CFStrings that represents ISO currency codes for
	// currencies in common use.

function CFLocaleCopyPreferredLanguages: CFArrayRef;
CF_AVAILABLE_STARTING(10_5, 2_0);
	// Returns the array of canonicalized CFString locale IDs that the user prefers.

function CFLocaleCreateCanonicalLanguageIdentifierFromString( allocator: CFAllocatorRef; localeIdentifier: CFStringRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Map an arbitrary language identification string (something close at
	// least) to a canonical language identifier.

function CFLocaleCreateCanonicalLocaleIdentifierFromString( allocator: CFAllocatorRef; localeIdentifier: CFStringRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Map an arbitrary locale identification string (something close at
	// least) to the canonical identifier.

function CFLocaleCreateCanonicalLocaleIdentifierFromScriptManagerCodes( allocator: CFAllocatorRef; lcode: LangCode; rcode: RegionCode ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Map a Mac OS LangCode and RegionCode to the canonical locale identifier.

function CFLocaleCreateLocaleIdentifierFromWindowsLocaleCode( allocator: CFAllocatorRef; lcid: UInt32 ): CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);
	// Map a Windows LCID to the canonical locale identifier.

function CFLocaleGetWindowsLocaleCodeFromLocaleIdentifier( localeIdentifier: CFStringRef ): UInt32;
CF_AVAILABLE_STARTING(10_6, 4_0);
	// Map a locale identifier to a Windows LCID.

type
	CFLocaleLanguageDirection = CFIndex;
const
	kCFLocaleLanguageDirectionUnknown = 0;
	kCFLocaleLanguageDirectionLeftToRight = 1;
	kCFLocaleLanguageDirectionRightToLeft = 2;
	kCFLocaleLanguageDirectionTopToBottom = 3;
	kCFLocaleLanguageDirectionBottomToTop = 4;

function CFLocaleGetLanguageCharacterDirection( isoLangCode: CFStringRef ): CFLocaleLanguageDirection;
CF_AVAILABLE_STARTING(10_6, 4_0);

function CFLocaleGetLanguageLineDirection( isoLangCode: CFStringRef ): CFLocaleLanguageDirection;
CF_AVAILABLE_STARTING(10_6, 4_0);

function CFLocaleCreateComponentsFromLocaleIdentifier( allocator: CFAllocatorRef; localeID: CFStringRef ): CFDictionaryRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Parses a locale ID consisting of language, script, country, variant,
	// and keyword/value pairs into a dictionary. The keys are the constant
	// CFStrings corresponding to the locale ID components, and the values
	// will correspond to constants where available.
	// Example: "en_US@calendar=japanese" yields a dictionary with three
	// entries: kCFLocaleLanguageCode=en, kCFLocaleCountryCode=US, and
	// kCFLocaleCalendarIdentifier=kCFJapaneseCalendar.

function CFLocaleCreateLocaleIdentifierFromComponents( allocator: CFAllocatorRef; dictionary: CFDictionaryRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Reverses the actions of CFLocaleCreateDictionaryFromLocaleIdentifier,
	// creating a single string from the data in the dictionary. The
	// dictionary {kCFLocaleLanguageCode=en, kCFLocaleCountryCode=US,
	// kCFLocaleCalendarIdentifier=kCFJapaneseCalendar} becomes
	// "en_US@calendar=japanese".

function CFLocaleCreate( allocator: CFAllocatorRef; localeIdentifier: CFStringRef ): CFLocaleRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Returns a CFLocaleRef for the locale named by the "arbitrary" locale identifier.

function CFLocaleCreateCopy( allocator: CFAllocatorRef; locale: CFLocaleRef ): CFLocaleRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Having gotten a CFLocale from somebody, code should make a copy
	// if it is going to use it for several operations
	// or hold onto it.  In the future, there may be mutable locales.

function CFLocaleGetIdentifier( locale: CFLocaleRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Returns the locale's identifier.  This may not be the same string
	// that the locale was created with (CFLocale may canonicalize it).

function CFLocaleGetValue( locale: CFLocaleRef; key: CFStringRef ): CFTypeRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Returns the value for the given key.  This is how settings and state
	// are accessed via a CFLocale.  Values might be of any CF type.

function CFLocaleCopyDisplayNameForPropertyValue( displayLocale: CFLocaleRef; key: CFStringRef; value: CFStringRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Returns the display name for the given value.  The key tells what
	// the value is, and is one of the usual locale property keys, though
	// not all locale property keys have values with display name values.


const kCFLocaleCurrentLocaleDidChangeNotification: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0);


// Locale Keys
const kCFLocaleIdentifier: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFLocaleLanguageCode: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFLocaleCountryCode: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFLocaleScriptCode: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFLocaleVariantCode: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

const kCFLocaleExemplarCharacterSet: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFLocaleCalendarIdentifier: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFLocaleCalendar: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFLocaleCollationIdentifier: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFLocaleUsesMetricSystem: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFLocaleMeasurementSystem: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER; // "Metric" or "U.S."
const kCFLocaleDecimalSeparator: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kCFLocaleGroupingSeparator: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kCFLocaleCurrencySymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kCFLocaleCurrencyCode: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER; // ISO 3-letter currency code
const kCFLocaleCollatorIdentifier: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);
const kCFLocaleQuotationBeginDelimiterKey: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);
const kCFLocaleQuotationEndDelimiterKey: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);
const kCFLocaleAlternateQuotationBeginDelimiterKey: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);
const kCFLocaleAlternateQuotationEndDelimiterKey: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);

// Values for kCFLocaleCalendarIdentifier
const kCFGregorianCalendar: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kCFBuddhistCalendar: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFChineseCalendar: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFHebrewCalendar: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFIslamicCalendar: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFIslamicCivilCalendar: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFJapaneseCalendar: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCFRepublicOfChinaCalendar: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);
const kCFPersianCalendar: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);
const kCFIndianCalendar: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);
const kCFISO8601Calendar: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);


end.
