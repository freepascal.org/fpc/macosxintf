{ CoreGraphics - CGDirectDisplay.h
   Copyright (c) 2000-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGDirectDisplay;
interface
uses MacTypes,CFBase,CFArray,CFDictionary,CGContext,CGBase,CGGeometry,CGErrors,CGImage;
{$ALIGN POWER}


type
	CGDirectDisplayID = UInt32;
	CGDirectDisplayIDPtr = ^CGDirectDisplayID;  { when a var xx:CGDirectDisplayID parameter can be nil, it is changed to xx: CGDirectDisplayIDPtr }
	CGOpenGLDisplayMask = UInt32;
	CGRefreshRate = Float64;

type
	CGDirectPaletteRef = ^OpaqueCGDirectPaletteRef; { an opaque type }
	OpaqueCGDirectPaletteRef = record end;
	CGDirectPaletteRefPtr = ^CGDirectPaletteRef;  { when a var xx:CGDirectPaletteRef parameter can be nil, it is changed to xx: CGDirectPaletteRefPtr }

type
	CGDisplayModeRef = ^OpaqueCGDisplayModeRef; { an opaque type }
	OpaqueCGDisplayModeRef = record end;
	CGDisplayModeRefPtr = ^CGDisplayModeRef;

const
  kCGNullDirectDisplay = CGDirectDisplayID(0);

{$ifc TARGET_OS_MAC}

{GPC-FPC-ONLY-START}
function kCGDirectMainDisplay__NAMED__CGMainDisplayID: CGDirectDisplayID;
{GPC-FPC-ONLY-END}

{ Return the display ID of the current main display. }

function CGMainDisplayID: CGDirectDisplayID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_NA);

{ Mechanisms used to find screen IDs.

   The following functions take an array length (`maxDisplays') and array of
   pointers to CGDirectDisplayIDs (`displays'). The array is filled in with
   the displays meeting the specified criteria; no more than `maxDisplays'.
   will be stored in `displays'. The number of displays meeting the criteria
   is returned in `matchingDisplayCount'.

   If the `displays' array is NULL, only the number of displays meeting the
   specified criteria is returned in `matchingDisplayCount'. }

function CGGetDisplaysWithPoint( point: CGPoint; maxDisplays: UInt32; displays: CGDirectDisplayIDPtr; var matchingDisplayCount: UInt32 ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

function CGGetDisplaysWithRect( rect: CGRect; maxDisplays: UInt32; displays: CGDirectDisplayIDPtr; var matchingDisplayCount: UInt32 ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

function CGGetDisplaysWithOpenGLDisplayMask( mask: CGOpenGLDisplayMask; maxDisplays: UInt32; displays: CGDirectDisplayIDPtr; var matchingDisplayCount: UInt32 ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);
                            
{ Return a list of active displays.

   If `activeDisplays' is NULL, then `maxDisplays' is ignored, and
   `displayCount' is set to the number of displays. Otherwise, the list of
   active displays is stored in `activeDisplays'; no more than `maxDisplays'
   will be stored in `activeDisplays'.

   The first display returned in the list is the main display (the one with
   the menu bar). When mirroring, this will be the largest drawable display
   in the mirror set, or, if all displays are the same size, the one with
   the deepest pixel depth. }

function CGGetActiveDisplayList( maxDisplays: UInt32; activeDisplays: CGDirectDisplayIDPtr; var displayCount: UInt32 ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return a list of online displays.

   If `onlineDisplays' is NULL, then `maxDisplays' is ignored, and
   `displayCount' is set to the number of displays. Otherwise, the list of
   online displays is stored in `onlineDisplays'; no more than `maxDisplays'
   will be stored in `onlineDisplays'.

   With hardware mirroring, a display may be online but not necessarily
   active or drawable. Programs which manipulate display settings such as
   the palette or gamma tables need access to all displays in use, including
   hardware mirrors which are not drawable. }

function CGGetOnlineDisplayList( maxDisplays: UInt32; onlineDisplays: CGDirectDisplayIDPtr; var displayCount: UInt32 ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_NA);

{ Return the OpenGL display mask for `display', or 0 if `display' is an
   invalid display. }

function CGDisplayIDToOpenGLDisplayMask( display: CGDirectDisplayID ): CGOpenGLDisplayMask;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return the display for the OpenGL display mask `mask', or
   `kCGNullDirectDisplay' if the bits set dont't match a display. A mask
   with multiple bits set returns an arbitrary match. }

function CGOpenGLDisplayMaskToDisplayID( mask: CGOpenGLDisplayMask ): CGDirectDisplayID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_NA);

{ Return the screen size and screen origin of `display' in global
   coordinates, or `CGRectZero' if `display' is invalid. }

function CGDisplayBounds( display: CGDirectDisplayID ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return the width in pixels of `display'. }

function CGDisplayPixelsWide( display: CGDirectDisplayID ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return the height in pixels of `display'. }

function CGDisplayPixelsHigh( display: CGDirectDisplayID ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return an array of all modes for the specified display, or NULL if
   `display' is invalid. The "options" field is reserved for future
   expansion; pass NULL for now. }
  
function CGDisplayCopyAllDisplayModes( display: CGDirectDisplayID; options: CFDictionaryRef ): CFArrayRef;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

const kCGDisplayShowDuplicateLowResolutionModes: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{ Return the current mode of the specified display, or NULL if `display'
   is invalid. }
   
function CGDisplayCopyDisplayMode( display: CGDirectDisplayID ): CGDisplayModeRef;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Switch the display mode of `display' to `mode'. The "options" field is
   reserved for future expansion; pass NULL for now.

   The selected display mode persists for the life of the program, and
   automatically reverts to the permanent setting when the program
   terminates.

   When changing display modes of displays in a mirroring set, other
   displays in the mirroring set will be set to a display mode capable of
   mirroring the bounds of the largest display being explicitly set.

   Note that after switching, display parameters and addresses may change. }

function CGDisplaySetDisplayMode( display: CGDirectDisplayID; mode: CGDisplayModeRef; options: CFDictionaryRef ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return the width in points of the specified display mode. }

function CGDisplayModeGetWidth( mode: CGDisplayModeRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return the height in points of the specified display mode. }

function CGDisplayModeGetHeight( mode: CGDisplayModeRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return a string representing the pixel encoding of the specified display
   mode, expressed as a CFString containing an IOKit graphics mode. }

function CGDisplayModeCopyPixelEncoding( mode: CGDisplayModeRef ): CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return the refresh rate of the specified display mode. }

function CGDisplayModeGetRefreshRate( mode: CGDisplayModeRef ): Float64;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return the IOKit flags of the specified display mode. }

function CGDisplayModeGetIOFlags( mode: CGDisplayModeRef ): UInt32;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return the IOKit display mode ID of the specified display mode. }

function CGDisplayModeGetIODisplayModeID( mode: CGDisplayModeRef ): SInt32;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return true if the specified mode is usable for displaying the
   desktop GUI; false otherwise. }

function CGDisplayModeIsUsableForDesktopGUI( mode: CGDisplayModeRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return the CFTypeID for CGDisplayModeRefs. }

function CGDisplayModeGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Equivalent to `CFRetain(mode)', except it doesn't crash (as CFRetain
   does) if `mode' is NULL. }

function CGDisplayModeRetain( mode: CGDisplayModeRef ): CGDisplayModeRef;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Equivalent to `CFRelease(mode)', except it doesn't crash (as CFRelease
   does) if `mode' is NULL. }

procedure CGDisplayModeRelease( mode: CGDisplayModeRef );
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return the width in pixels of the specified display mode. }

function CGDisplayModeGetPixelWidth( mode: CGDisplayModeRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{ Return the height in pixels of the specified display mode. }

function CGDisplayModeGetPixelHeight( mode: CGDisplayModeRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_8, __IPHONE_NA);

{ Set the gamma function for `display' by specifying the coefficients of
   the gamma transfer function.

   Gamma values must be greater than 0. Minimum values must be in the
   interval [0, 1). Maximum values must be in the interval (0, 1]. Out of
   range values or maximum values greater than or equal to minimum values
   return `kCGErrorRangeCheck'.

   Values are computed by sampling a function for a range of indexes from 0
   to 1:
     value = Min + ((Max - Min) * pow(index, Gamma))
   The resulting values are converted to a machine-specific format and
   loaded into display hardware. }

type
	CGGammaValue = Float32;
	CGGammaValuePtr = ^CGGammaValue;
                                              
function CGSetDisplayTransferByFormula( display: CGDirectDisplayID; redMin: CGGammaValue; redMax: CGGammaValue; redGamma: CGGammaValue; greenMin: CGGammaValue; greenMax: CGGammaValue; greenGamma: CGGammaValue; blueMin: CGGammaValue; blueMax: CGGammaValue; blueGamma: CGGammaValue ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return the coefficients of the gamma transfer function for `display'. }

function CGGetDisplayTransferByFormula( display: CGDirectDisplayID; var redMin: CGGammaValue; var redMax: CGGammaValue; var redGamma: CGGammaValue; var greenMin: CGGammaValue; var greenMax: CGGammaValue; var greenGamma: CGGammaValue; var blueMin: CGGammaValue; var blueMax: CGGammaValue; var blueGamma: CGGammaValue ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return the capacity, or number of entries, in the gamma table for
   `display', or 0 if 'display' is invalid. }

function CGDisplayGammaTableCapacity( display: CGDirectDisplayID ): UInt32;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);

{ Set the gamma function for `display' by specifying the values in the RGB
   gamma tables.

   Values within each table should be in the interval [0, 1] The same table
   may be passed in for red, green, and blue channels. The number of entries
   in the tables is specified by `tableSize'. The tables are interpolated as
   needed to generate the number of samples needed by the display hardware. }

function CGSetDisplayTransferByTable( display: CGDirectDisplayID; tableSize: UInt32; {const var} redTable: CGGammaValuePtr; {const var} greenTable: CGGammaValuePtr; {const var} blueTable: CGGammaValuePtr ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return the RGB gamma table values for `display'.

   The number of entries in each array is specified by `capacity'; no more
   than `capacity' entries will be written to each table. The number of
   entries written is stored in `sampleCount'. }

function CGGetDisplayTransferByTable( display: CGDirectDisplayID; capacity: UInt32; redTable: CGGammaValuePtr; greenTable: CGGammaValuePtr; blueTable: CGGammaValuePtr; var sampleCount: UInt32 ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Set the gamma function for `display' by specifying the values in the RGB
   gamma tables as bytes.

   Values within each table should be in the interval [0, 255] The same
   table may be passed in for red, green, and blue channels. The number of
   entries in the tables is specified by `tableSize'. The tables are
   interpolated as needed to generate the number of samples needed by the
   display hardware. }

function CGSetDisplayTransferByByteTable( display: CGDirectDisplayID; tableSize: UInt32; redTable: {const} UInt8Ptr; greenTable: {const} UInt8Ptr; blueTable: {const} UInt8Ptr ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Restore the gamma tables of all system displays to the values in the
   user's ColorSync display profile. }

procedure CGDisplayRestoreColorSyncSettings;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Options used with `CGDisplayCaptureWithOptions' and
   `CGCaptureAllDisplaysWithOptions'. }

const
	kCGCaptureNoOptions = 0;	{ Default behavior. }
	kCGCaptureNoFill = 1 shl 0;	{ Disables fill with black on capture. }
type
	CGCaptureOptions = UInt32;

{ Return true if `display' is captured; false otherwise. }

function CGDisplayIsCaptured( display: CGDirectDisplayID ): boolean_t;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_9, __IPHONE_NA, __IPHONE_NA);

{ Capture `display' for exclusive use by an application. }

function CGDisplayCapture( display: CGDirectDisplayID ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Capture `display' for exclusive use by an application, using the options
   specified by `options'. }

function CGDisplayCaptureWithOptions( display: CGDirectDisplayID; options: CGCaptureOptions ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);

{ Release the captured display `display'. }

function CGDisplayRelease( display: CGDirectDisplayID ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Capture all displays. This operation provides an immersive environment
   for an appplication, and prevents other applications from trying to
   adjust to display changes. }

function CGCaptureAllDisplays: CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Capture all displays, using the options specified by `options'. This
   operation provides an immersive environment for an appplication, and
   prevents other applications from trying to adjust to display changes. }

function CGCaptureAllDisplaysWithOptions( options: CGCaptureOptions ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);

{ Release all captured displays and restore the display modes to the user's
   preferences. May be used in conjunction with `CGDisplayCapture' or
   `CGCaptureAllDisplays'. }

function CGReleaseAllDisplays: CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Returns window ID of the shield window for the captured display `display',
   or NULL if the display is not not shielded. }

function CGShieldingWindowID( display: CGDirectDisplayID ): UInt32;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Returns the window level of the shield window for the captured display
   `display'. }

function CGShieldingWindowLevel: SInt32;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return an image containing the contents of the display identified by
   `displayID'. }

function CGDisplayCreateImage( displayID: CGDirectDisplayID ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return an image containing the contents of the rectangle `rect',
   specified in display space, of the display identified by `displayID'. The
   actual rectangle used is the rectangle returned from
   `CGRectIntegral(rect)'. }

function CGDisplayCreateImageForRect( display: CGDirectDisplayID; rect: CGRect ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Hide the mouse cursor and increment the hide cursor count. The `display'
   parameter is ignored. }

function CGDisplayHideCursor( display: CGDirectDisplayID ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Decrement the hide cursor count; show the cursor if the hide cursor count
   is zero. The `display' parameter is ignored. }

function CGDisplayShowCursor( display: CGDirectDisplayID ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Move the mouse cursor to the specified point relative to the origin (the
   upper-left corner) of `display'. No events are generated as a result of
   the move. Points that lie outside the desktop are clipped to the
   desktop. }

function CGDisplayMoveCursorToPoint( display: CGDirectDisplayID; point: CGPoint ): CGError;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return the mouse position change since with the last mouse move event
   received by the application. }

procedure CGGetLastMouseDelta( var deltaX: SInt32; var deltaY: SInt32 );
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{ Return a CGContext suitable for drawing to the captured display
   `display', or NULL if `display' has not been captured. The context is
   owned by the device and should not be released by the caller.

   The context remains valid while the display is captured and while the
   display configuration is unchanged. Releasing the captured display or
   reconfiguring the display invalidates the drawing context.

   The determine when the display configuration is changing, use
   `CGDisplayRegisterReconfigurationCallback'. }

function CGDisplayGetDrawingContext( display: CGDirectDisplayID ): CGContextRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);


{
   Keys used in mode dictionaries.  Source C strings shown won't change.
   Some CFM environments cannot import data variables, and so
   the definitions are provided directly.
  
   These keys are used only within the scope of the mode dictionaries,
   so further uniquing, as by prefix, of the source string is not needed.
 }

{ These are deprecated; don't use them. }

const kCGDisplayWidth = CFSTR( 'Width' );
const kCGDisplayHeight = CFSTR( 'Height' );
const kCGDisplayMode = CFSTR( 'Mode' );
const kCGDisplayBitsPerPixel = CFSTR( 'BitsPerPixel' );
const kCGDisplayBitsPerSample = CFSTR( 'BitsPerSample' );
const kCGDisplaySamplesPerPixel = CFSTR( 'SamplesPerPixel' );
const kCGDisplayRefreshRate = CFSTR( 'RefreshRate' );
const kCGDisplayModeUsableForDesktopGUI = CFSTR( 'UsableForDesktopGUI' );
const kCGDisplayIOFlags = CFSTR( 'IOFlags' );
const kCGDisplayBytesPerRow = CFSTR( 'kCGDisplayBytesPerRow' );
const kCGIODisplayModeID = CFSTR( 'IODisplayModeID' );

{
 * Keys to describe optional properties of display modes.
 *
 * The key will only be present if the property applies,
 * and will be associated with a value of kCFBooleanTrue.
 * Keys not relevant to a particular display mode will not
 * appear in the mode dictionary.
 *
 * These strings must remain unchanged in future releases, of course.
 }

{ These are deprecated; don't use them. }

{ Set if display mode doesn't need a confirmation dialog to be set }
const kCGDisplayModeIsSafeForHardware = CFSTR( 'kCGDisplayModeIsSafeForHardware' );

{ The following keys reflect interesting bits of the IOKit display mode flags }
const kCGDisplayModeIsInterlaced = CFSTR( 'kCGDisplayModeIsInterlaced' );
const kCGDisplayModeIsStretched = CFSTR( 'kCGDisplayModeIsStretched' );
const kCGDisplayModeIsTelevisionOutput = CFSTR( 'kCGDisplayModeIsTelevisionOutput' );

{ These types are deprecated; don't use them. }

type
	CGDisplayCount = UInt32;
	CGDisplayErr = CGError;
	CGBeamPosition = UInt32;
	CGByteValue = UInt8;
    CGDisplayCoord = SInt32;

type
	CGMouseDelta = SInt32;
	CGTableCount = UInt32;

const
  CGDisplayNoErr=kCGErrorSuccess;

{ These functions are deprecated; do not use them. }

{ Move the mouse cursor to the specified point relative to the origin (the
   upper-left corner) of `display'. No events are generated as a result of
   the move. Points that lie outside the desktop are clipped to the
   desktop. }

{ Use `CGDisplayCreateImage' instead. }
function CGDisplayBaseAddress( display: CGDirectDisplayID ): UnivPtr;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6,__IPHONE_NA, __IPHONE_NA);

{
 * return address for X,Y in global coordinates;
 *	(0,0) represents the upper left corner of the main display.
 * returns NULL for an invalid display or out of bounds coordinates
 * If the display has not been captured, the returned address may refer
 * to read-only memory.
 }

{ Use `CGDisplayCreateImageForRect' instead. }
function CGDisplayAddressForPosition( display: CGDirectDisplayID; x: CGDisplayCoord; y: CGDisplayCoord ): UnivPtr;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_1, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{
 * Display mode selection
 * Display modes are represented as CFDictionaries
 * All dictionaries and arrays returned via these mechanisms are
 * owned by the framework and should not be released.  The framework
 * will not release them out from under your application.
 *
 * Values associated with the following keys are CFNumber types.
 * With CFNumberGetValue(), use kCFNumberLongType for best results.
 * kCGDisplayRefreshRate encodes a double value, so to get the fractional
 * refresh rate use kCFNumberDoubleType.
 }
 
{ Use `CGDisplayCreateImage' or `CGDisplayCreateImageForRect' instead. }
function CGDisplayBytesPerRow( display: CGDirectDisplayID ): size_t;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{
 * Return a CFArray of CFDictionaries describing all display modes.
 * Returns NULL if the display is invalid.
 }

{ Use the CGDisplayMode APIs instead. }
function CGDisplayAvailableModes( display: CGDirectDisplayID ): CFArrayRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{ Use the CGDisplayMode APIs instead. }
function CGDisplayBestModeForParameters( display: CGDirectDisplayID; bitsPerPixel: size_t; width: size_t; height: size_t; var exactMatch: boolean_t ): CFDictionaryRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{ Use the CGDisplayMode APIs instead. }
function CGDisplayBestModeForParametersAndRefreshRate( display: CGDirectDisplayID; bitsPerPixel: size_t; width: size_t; height: size_t; refreshRate: CGRefreshRate; var exactMatch: boolean_t ): CFDictionaryRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{ Use the CGDisplayMode APIs instead. }
function CGDisplayBestModeForParametersAndRefreshRateWithProperty( display: CGDirectDisplayID; bitsPerPixel: size_t; width: size_t; height: size_t; refreshRate: CGRefreshRate; property: CFStringRef; var exactMatch: boolean_t ): CFDictionaryRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_2, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{
 * Return a CFDictionary describing the current display mode.
 * Returns NULL if display is invalid.
 }

{ Use the CGDisplayMode APIs instead. }
function CGDisplayCurrentMode( display: CGDirectDisplayID ): CFDictionaryRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{
 * Switch display mode.  Note that after switching, 
 * display parameters and addresses may change.
 * The selected display mode persists for the life of the program, and automatically
 * reverts to the permanent setting made by Preferences when the program terminates.
 * The mode dictionary passed in must be a dictionary vended by other CGDirectDisplay
 * APIs such as CGDisplayBestModeForParameters() and CGDisplayAvailableModes().
 *
 * The mode dictionary passed in must be a dictionary vended by other CGDirectDisplay
 * APIs such as CGDisplayBestModeForParameters() and CGDisplayAvailableModes().
 *
 * When changing display modes of displays in a mirroring set, other displays in
 * the mirroring set will be set to a display mode capable of mirroring the bounds
 * of the largest display being explicitly set. 
 }
{ Use the CGDisplayMode APIs instead. }
function CGDisplaySwitchToMode( display: CGDirectDisplayID; mode: CFDictionaryRef ): CGError;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{ Query parameters for current mode }
{ Use the CGDisplayMode APIs instead. }
function CGDisplayBitsPerPixel( display: CGDirectDisplayID ): size_t;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{ Use the CGDisplayMode APIs instead. }
function CGDisplayBitsPerSample( display: CGDirectDisplayID ): size_t;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

{ Use the CGDisplayMode APIs instead. }
function CGDisplaySamplesPerPixel( display: CGDirectDisplayID ): size_t;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6, __IPHONE_NA, __IPHONE_NA);

function CGDisplayCanSetPalette( display: CGDirectDisplayID ): boolean_t;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

function CGDisplaySetPalette( display: CGDirectDisplayID; palette: CGDirectPaletteRef ): CGError;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

function CGDisplayWaitForBeamPositionOutsideLines( display: CGDirectDisplayID; upperScanLine: UInt32; lowerScanLine: UInt32 ): CGError;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

function CGDisplayBeamPosition( display: CGDirectDisplayID ): UInt32;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);


{$endc}

end.
