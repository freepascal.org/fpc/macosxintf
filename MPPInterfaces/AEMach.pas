{
     File:       AE/AEMach.h
 
     Contains:   AppleEvent over mach_msg interfaces
 
    
 
     Copyright:  � 2000-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit AEMach;
interface
uses MacTypes,MacOSXPosix,AEDataModel;

{$ifc TARGET_OS_MAC}

{-
 * AE Mach API --
 *
 * AppleEvents on OS X are implemented in terms of mach messages.
 * To facilitate writing server processes that can send and receive
 * AppleEvents, the following APIs are provided.
 *
 * AppleEvents are directed to a well known port uniquely tied to a
 * process.  The AE framework will discover this port based on the
 * keyAddressAttr of the event (as specifed in AECreateAppleEvent by
 * the target parameter.)  If a port cannot be found,
 * procNotFound (-600) will be returned on AESend.
 *
 * Of note is a new attribute for an AppleEvent, keyReplyPortAttr.
 * This specifies the mach_port_t to which an AppleEvent reply
 * should be directed.  By default, replies are sent to the
 * processes' registered port where they are culled from the normal  
 * event stream if there is an outstanding AESend + kAEWaitReply.
 * But it may be desirable for a client to specify their own port to
 * receive queud replies.
 *
 * In the case of AESendMessage with kAEWaitReply specified, an 
 * anonymous port will be used to block until the reply is received.
 *
 * Not supplied is a convenience routine to block a server and
 * process AppleEvents.  This implementation will be detailed in a
 * tech note.
 *
 * In general, the AppleEvent APIs are thread safe, but the mechanism
 * of their delivery (AEProcessAppleEvent, AEResumeTheCurrentEvent)
 * are not.  If you're attemping to write a thread safe server, you
 * should avoid AppleEvent routines that don't explicitly state their
 * thread safety.
 *
 *}
const
	keyReplyPortAttr = FOUR_CHAR_CODE('repp');

{ typeReplyPortAttr was misnamed and is deprecated; use keyReplyPortAttr instead. }
const
	typeReplyPortAttr = keyReplyPortAttr;

{
 *  AEGetRegisteredMachPort()
 *  
 *  Discussion:
 *    Return the mach_port_t that was registered by the AppleEvent
 *    framework for this process.  This port is considered public, and
 *    will be used by other applications to target your process.  You
 *    are free to use this mach_port_t to add to a port set, if and
 *    only if, you are not also using routines from HIToolbox.  In that
 *    case, HIToolbox retains control of this port and AppleEvents are
 *    dispatched through the main event loop.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function AEGetRegisteredMachPort: mach_port_t;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AEDecodeMessage()
 *  
 *  Discussion:
 *    Decode a mach_msg into an AppleEvent and its related reply.  (The
 *    reply is set up from fields of the event.)  You can call this
 *    routine if you wish to dispatch or handle the event yourself.  To
 *    return a reply to the sender, you should call:
 *     AESendMessage(reply, NULL, kAENoReply, kAENormalPriority,
 *    kAEDefaultTimeout);
 *    If this message is a reply, the 'reply' parameter will be
 *    initialized to ( typeNull, 0 ), and the 'event' parameter will be
 *    the AppleEvent reply with a event class attribute of
 *    typeAppleEvent, class typeAppleEventReply:
 *    The contents of the header are invalid after this call.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    header:
 *      The incoming mach message to be dispatched
 *    
 *    event:
 *      The AppleEvent to decode the message in header into
 *    
 *    reply:
 *      The AppleEvent reply is decoded into reply
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function AEDecodeMessage( var header: mach_msg_header_t; var event: AppleEvent; reply: AppleEventPtr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AEProcessMessage()
 *  
 *  Discussion:
 *    Decodes and dispatches an event to an event handler.  Handles
 *    packaging and returning the reply to the sender.
 *    The contents of the header are invalid after this call.
 *  
 *  Mac OS X threading:
 *    Not thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    header:
 *      The incoming mach message to be dispatched.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function AEProcessMessage( var header: mach_msg_header_t ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AESendMessage()
 *  
 *  Discussion:
 *    Send an AppleEvent to a target process.  If the target is the
 *    current process (as specified by using typeProcessSerialNumber of
 *    ( 0, kCurrentProcess ) it is dispatched directly to the
 *    appropriate event handler in your process and not serialized.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Parameters:
 *    
 *    event:
 *      The event to be sent
 *    
 *    reply:
 *      The reply for the event, if non-NULL
 *    
 *    sendMode:
 *      The mode to send the event
 *    
 *    timeOutInTicks:
 *      The timeout for sending the event, in ticks.  If 0, there is no
 *      timeout.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function AESendMessage( const var event: AppleEvent; reply: AppleEventPtr { can be NULL }; sendMode: AESendMode; timeOutInTicks: SIGNEDLONG ): OSStatus;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{$endc} {TARGET_OS_MAC}

end.
