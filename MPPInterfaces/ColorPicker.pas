{
     File:       CommonPanels/ColorPicker.h
 
     Contains:   Color Picker package Interfaces.
 
     Version:    CommonPanels-94~602
 
     Copyright:  � 1987-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ColorPicker;
interface
uses MacTypes,ColorSyncDeprecated,MixedMode,QuickdrawTypes,Events;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{$ifc not TARGET_CPU_64}
const
{Maximum small fract value, as long}
	kMaximumSmallFract = $0000FFFF;

{$endc} {not TARGET_CPU_64}

{ These are legacy constants. The Color Picker on OS X uses the Cocoa NSColorPanel. }
const
	kDefaultColorPickerWidth = 383;
	kDefaultColorPickerHeight = 238;

type
	DialogPlacementSpec = SInt16;
const
	kAtSpecifiedOrigin = 0;
	kDeepestColorScreen = 1;
	kCenterOnMainScreen = 2;

{ Since OS X uses the Cocoa NSColorPanel, the flags below are no longer used. }
const
	kColorPickerDialogIsMoveable = 1;
	kColorPickerDialogIsModal = 2;
	kColorPickerCanModifyPalette = 4;
	kColorPickerCanAnimatePalette = 8;
	kColorPickerAppIsColorSyncAware = 16;
	kColorPickerInSystemDialog = 32;
	kColorPickerInApplicationDialog = 64;
	kColorPickerInPickerDialog = 128;
	kColorPickerDetachedFromChoices = 256;
	kColorPickerCallColorProcLive = 512;

{$ifc OLDROUTINENAMES}
{$ifc not TARGET_CPU_64}
const
{Maximum small fract value, as long}
	MaxSmallFract = $0000FFFF;

{$endc} {not TARGET_CPU_64}

const
	kDefaultWidth = 383;
	kDefaultHeight = 238;

{ Since OS X uses the Cocoa NSColorPanel, the flags below are no longer used. }
const
	DialogIsMoveable = 1;
	DialogIsModal = 2;
	CanModifyPalette = 4;
	CanAnimatePalette = 8;
	AppIsColorSyncAware = 16;
	InSystemDialog = 32;
	InApplicationDialog = 64;
	InPickerDialog = 128;
	DetachedFromChoices = 256;
	CallColorProcLive = 512;

{$endc} {OLDROUTINENAMES}

{$ifc not TARGET_CPU_64}
{ A SmallFract value is just the fractional part of a Fixed number,
which is the low order word.  They can be
assigned directly to and from INTEGERs. }
{ Unsigned fraction between 0 and 1 }
type
	SmallFract = UInt16;
	HSVColorPtr = ^HSVColor;
	HSVColor = record
		hue: SmallFract;                    { Fraction of circle, red at 0 }
		saturation: SmallFract;             { 0-1, 0 for gray, 1 for pure color }
		value: SmallFract;                  { 0-1, 0 for black, 1 for max intensity }
	end;
type
	HSLColorPtr = ^HSLColor;
	HSLColor = record
		hue: SmallFract;                    { Fraction of circle, red at 0 }
		saturation: SmallFract;             { 0-1, 0 for gray, 1 for pure color }
		lightness: SmallFract;              { 0-1, 0 for black, 1 for white }
	end;
type
	CMYColorPtr = ^CMYColor;
	CMYColor = record
		cyan: SmallFract;
		magenta: SmallFract;
		yellow: SmallFract;
	end;
type
	PMColor = record
		profile: CMProfileHandle;
		color: CMColor;
	end;
	PMColorPtr = ^PMColor;
{$endc} {not TARGET_CPU_64}

type
	NPMColor = record
		profile: CMProfileRef;
		color: CMColor;
	end;
	NPMColorPtr = ^NPMColor;
type
	Picker = ^OpaquePicker; { an opaque type }
	OpaquePicker = record end;
	PickerPtr = ^Picker;  { when a var xx:Picker parameter can be nil, it is changed to xx: PickerPtr }
{ Since OS X uses the Cocoa NSColorPanel, the struct below is no longer used. }
type
	PickerMenuItemInfoPtr = ^PickerMenuItemInfo;
	PickerMenuItemInfo = record
		editMenuID: SInt16;
		cutItem: SInt16;
		copyItem: SInt16;
		pasteItem: SInt16;
		clearItem: SInt16;
		undoItem: SInt16;
	end;
{ The following proc ptr is the only supported way to communicate with the Cocoa NSColorPanel on OS X. }
type
	NColorChangedProcPtr = procedure( userData: SRefCon; var newColor: NPMColor );
{GPC-ONLY-START}
	NColorChangedUPP = UniversalProcPtr; // should be NColorChangedProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	NColorChangedUPP = NColorChangedProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	NColorChangedUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{
 *  NewNColorChangedUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewNColorChangedUPP( userRoutine: NColorChangedProcPtr ): NColorChangedUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeNColorChangedUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeNColorChangedUPP( userUPP: NColorChangedUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeNColorChangedUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeNColorChangedUPP( userData: SRefCon; var newColor: NPMColor; userUPP: NColorChangedUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{$ifc TARGET_CPU_64}

type
{GPC-ONLY-START}
	ColorChangedUPP = UniversalProcPtr; // should be UnivPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	ColorChangedUPP = UnivPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	ColorChangedUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	UserEventUPP = UniversalProcPtr; // should be UnivPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	UserEventUPP = UnivPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	UserEventUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{$elsec}
type
	ColorChangedProcPtr = procedure( userData: SInt32; var newColor: PMColor );
	UserEventProcPtr = function( var event: EventRecord ): Boolean;
{GPC-ONLY-START}
	ColorChangedUPP = UniversalProcPtr; // should be ColorChangedProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	ColorChangedUPP = ColorChangedProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	ColorChangedUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	UserEventUPP = UniversalProcPtr; // should be UserEventProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	UserEventUPP = UserEventProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	UserEventUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{
 *  NewColorChangedUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewColorChangedUPP( userRoutine: ColorChangedProcPtr ): ColorChangedUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewUserEventUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewUserEventUPP( userRoutine: UserEventProcPtr ): UserEventUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeColorChangedUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeColorChangedUPP( userUPP: ColorChangedUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeUserEventUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeUserEventUPP( userUPP: UserEventUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeColorChangedUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeColorChangedUPP( userData: SInt32; var newColor: PMColor; userUPP: ColorChangedUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeUserEventUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeUserEventUPP( var event: EventRecord; userUPP: UserEventUPP ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{$endc} {TARGET_CPU_64}

{$ifc not TARGET_CPU_64}
type
	ColorPickerInfo = record
		theColor: PMColor;
		dstProfile: CMProfileHandle;
		flags: UInt32;
		placeWhere: DialogPlacementSpec;
		dialogOrigin: Point;
		pickerType: OSType;
		eventProc: UserEventUPP;
		colorProc: ColorChangedUPP;
		colorProcData: UInt32;
		prompt: Str255;
		mInfo: PickerMenuItemInfo;
		newColorChosen: Boolean;
		filler: SInt8;
	end;
{$endc} {not TARGET_CPU_64}

type
	NColorPickerInfo = record
		theColor: NPMColor;
		dstProfile: CMProfileRef;             { Currently ignored }
		flags: UInt32;                  { Currently ignored }
		placeWhere: DialogPlacementSpec;            { Currently ignored }
		dialogOrigin: Point;           { Currently ignored }
		pickerType: OSType;             { Currently ignored }
{$ifc not TARGET_CPU_64}

		eventProc: UserEventUPP;              { Ignored }
{$endc} {not TARGET_CPU_64}

		colorProc: NColorChangedUPP;
		colorProcData: URefCon;
		prompt: Str255;                 { Currently ignored }
		mInfo: PickerMenuItemInfo;                  { Ignored }
		newColorChosen: Boolean;
		reserved: UInt8;               { Must be 0 }
	end;

{$ifc not TARGET_CPU_64}
{
 *  Fix2SmallFract()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function Fix2SmallFract( f: Fixed ): SmallFract;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SmallFract2Fix()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SmallFract2Fix( s: SmallFract ): Fixed;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CMY2RGB()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure CMY2RGB( const var cColor: CMYColor; var rColor: RGBColor );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RGB2CMY()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure RGB2CMY( const var rColor: RGBColor; var cColor: CMYColor );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  HSL2RGB()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure HSL2RGB( const var hColor: HSLColor; var rColor: RGBColor );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RGB2HSL()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure RGB2HSL( const var rColor: RGBColor; var hColor: HSLColor );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  HSV2RGB()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure HSV2RGB( const var hColor: HSVColor; var rColor: RGBColor );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RGB2HSV()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure RGB2HSV( const var rColor: RGBColor; var hColor: HSVColor );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{
 *  GetColor()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetColor( where: Point; const var prompt: Str255; const var inColor: RGBColor; var outColor: RGBColor ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$ifc not TARGET_CPU_64}
{
 *  PickColor()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ColorPickerLib 2.0 and later
 }
function PickColor( var theColorInfo: ColorPickerInfo ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{
 *  NPickColor()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ColorPickerLib 2.1 and later
 }
function NPickColor( var theColorInfo: NColorPickerInfo ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {TARGET_OS_MAC}


end.
