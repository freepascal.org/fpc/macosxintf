{
     File:       AE/AEObjects.h
 
     Contains:   Object Support Library Interfaces.
 
    
 
     Copyright:  � 1991-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit AEObjects;
interface
uses MacTypes,AEDataModel,OSUtils,AppleEvents,MacErrors;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

const
{*** LOGICAL OPERATOR CONSTANTS  ***}
	kAEAND = FOUR_CHAR_CODE('AND '); {  0x414e4420  }
	kAEOR = FOUR_CHAR_CODE('OR  '); {  0x4f522020  }
	kAENOT = FOUR_CHAR_CODE('NOT '); {  0x4e4f5420  }
                                        {*** ABSOLUTE ORDINAL CONSTANTS  ***}
	kAEFirst = FOUR_CHAR_CODE('firs'); {  0x66697273  }
	kAELast = FOUR_CHAR_CODE('last'); {  0x6c617374  }
	kAEMiddle = FOUR_CHAR_CODE('midd'); {  0x6d696464  }
	kAEAny = FOUR_CHAR_CODE('any '); {  0x616e7920  }
	kAEAll = FOUR_CHAR_CODE('all '); {  0x616c6c20  }
                                        {*** RELATIVE ORDINAL CONSTANTS  ***}
	kAENext = FOUR_CHAR_CODE('next'); {  0x6e657874  }
	kAEPrevious = FOUR_CHAR_CODE('prev'); {  0x70726576  }
                                        {*** KEYWORD CONSTANT    ***}
	keyAECompOperator = FOUR_CHAR_CODE('relo'); {  0x72656c6f  }
	keyAELogicalTerms = FOUR_CHAR_CODE('term'); {  0x7465726d  }
	keyAELogicalOperator = FOUR_CHAR_CODE('logc'); {  0x6c6f6763  }
	keyAEObject1 = FOUR_CHAR_CODE('obj1'); {  0x6f626a31  }
	keyAEObject2 = FOUR_CHAR_CODE('obj2'); {  0x6f626a32  }
                                        {    ... for Keywords for getting fields out of object specifier records. }
	keyAEDesiredClass = FOUR_CHAR_CODE('want'); {  0x77616e74  }
	keyAEContainer = FOUR_CHAR_CODE('from'); {  0x66726f6d  }
	keyAEKeyForm = FOUR_CHAR_CODE('form'); {  0x666f726d  }
	keyAEKeyData = FOUR_CHAR_CODE('seld'); {  0x73656c64  }

const
{    ... for Keywords for getting fields out of Range specifier records. }
	keyAERangeStart = FOUR_CHAR_CODE('star'); {  0x73746172  }
	keyAERangeStop = FOUR_CHAR_CODE('stop'); {  0x73746f70  }
                                        {    ... special handler selectors for OSL Callbacks. }
	keyDisposeTokenProc = FOUR_CHAR_CODE('xtok'); {  0x78746f6b  }
	keyAECompareProc = FOUR_CHAR_CODE('cmpr'); {  0x636d7072  }
	keyAECountProc = FOUR_CHAR_CODE('cont'); {  0x636f6e74  }
	keyAEMarkTokenProc = FOUR_CHAR_CODE('mkid'); {  0x6d6b6964  }
	keyAEMarkProc = FOUR_CHAR_CODE('mark'); {  0x6d61726b  }
	keyAEAdjustMarksProc = FOUR_CHAR_CODE('adjm'); {  0x61646a6d  }
	keyAEGetErrDescProc = FOUR_CHAR_CODE('indc'); {  0x696e6463  }

{***   VALUE and TYPE CONSTANTS    ***}
const
{    ... possible values for the keyAEKeyForm field of an object specifier. }
	formAbsolutePosition = FOUR_CHAR_CODE('indx'); {  0x696e6478  }
	formRelativePosition = FOUR_CHAR_CODE('rele'); {  0x72656c65  }
	formTest = FOUR_CHAR_CODE('test'); {  0x74657374  }
	formRange = FOUR_CHAR_CODE('rang'); {  0x72616e67  }
	formPropertyID = FOUR_CHAR_CODE('prop'); {  0x70726f70  }
	formName = FOUR_CHAR_CODE('name'); {  0x6e616d65  }
	formUniqueID = FOUR_CHAR_CODE('ID  '); {  0x49442020  }
                                        {    ... relevant types (some of these are often pared with forms above). }
	typeObjectSpecifier = FOUR_CHAR_CODE('obj '); {  0x6f626a20  }
	typeObjectBeingExamined = FOUR_CHAR_CODE('exmn'); {  0x65786d6e  }
	typeCurrentContainer = FOUR_CHAR_CODE('ccnt'); {  0x63636e74  }
	typeToken = FOUR_CHAR_CODE('toke'); {  0x746f6b65  }
	typeRelativeDescriptor = FOUR_CHAR_CODE('rel '); {  0x72656c20  }
	typeAbsoluteOrdinal = FOUR_CHAR_CODE('abso'); {  0x6162736f  }
	typeIndexDescriptor = FOUR_CHAR_CODE('inde'); {  0x696e6465  }
	typeRangeDescriptor = FOUR_CHAR_CODE('rang'); {  0x72616e67  }
	typeLogicalDescriptor = FOUR_CHAR_CODE('logi'); {  0x6c6f6769  }
	typeCompDescriptor = FOUR_CHAR_CODE('cmpd'); {  0x636d7064  }
	typeOSLTokenList = FOUR_CHAR_CODE('ostl'); {  0x6F73746C  }

{ Possible values for flags parameter to AEResolve.  They're additive }
const
	kAEIDoMinimum = $0000;
	kAEIDoWhose = $0001;
	kAEIDoMarking = $0004;
	kAEPassSubDescs = $0008;
	kAEResolveNestedLists = $0010;
	kAEHandleSimpleRanges = $0020;
	kAEUseRelativeIterators = $0040;

{*** SPECIAL CONSTANTS FOR CUSTOM WHOSE-CLAUSE RESOLUTION }
const
	typeWhoseDescriptor = FOUR_CHAR_CODE('whos'); {  0x77686f73  }
	formWhose = FOUR_CHAR_CODE('whos'); {  0x77686f73  }
	typeWhoseRange = FOUR_CHAR_CODE('wrng'); {  0x77726e67  }
	keyAEWhoseRangeStart = FOUR_CHAR_CODE('wstr'); {  0x77737472  }
	keyAEWhoseRangeStop = FOUR_CHAR_CODE('wstp'); {  0x77737470  }
	keyAEIndex = FOUR_CHAR_CODE('kidx'); {  0x6b696478  }
	keyAETest = FOUR_CHAR_CODE('ktst'); {  0x6b747374  }

{
    used for rewriting tokens in place of 'ccnt' descriptors
    This record is only of interest to those who, when they...
    ...get ranges as key data in their accessor procs, choose
    ...to resolve them manually rather than call AEResolve again.
}
type
	ccntTokenRecordPtr = ^ccntTokenRecord;
	ccntTokenRecord = record
		tokenClass: DescType;
		token: AEDesc;
	end;
type
	ccntTokenRecPtr = ccntTokenRecordPtr;
	ccntTokenRecHandle = ^ccntTokenRecPtr;
{$ifc OLDROUTINENAMES}
type
	DescPtr = AEDescPtr;
	DescHandle = DescPtrPtr;
{$endc} {OLDROUTINENAMES}

{ typedefs providing type checking for procedure pointers }
type
	OSLAccessorProcPtr = function( desiredClass: DescType; const var container: AEDesc; containerClass: DescType; form: DescType; const var selectionData: AEDesc; var value: AEDesc; accessorRefcon: SRefCon ): OSErr;
	OSLCompareProcPtr = function( oper: DescType; const var obj1: AEDesc; const var obj2: AEDesc; var result: Boolean ): OSErr;
	OSLCountProcPtr = function( desiredType: DescType; containerClass: DescType; const var container: AEDesc; var result: SIGNEDLONG ): OSErr;
	OSLDisposeTokenProcPtr = function( var unneededToken: AEDesc ): OSErr;
	OSLGetMarkTokenProcPtr = function( const var dContainerToken: AEDesc; containerClass: DescType; var result: AEDesc ): OSErr;
	OSLGetErrDescProcPtr = function( var appDescPtr: AEDescPtr ): OSErr;
	OSLMarkProcPtr = function( const var dToken: AEDesc; const var markToken: AEDesc; index: SIGNEDLONG ): OSErr;
	OSLAdjustMarksProcPtr = function( newStart: SIGNEDLONG; newStop: SIGNEDLONG; const var markToken: AEDesc ): OSErr;
{GPC-ONLY-START}
	OSLAccessorUPP = UniversalProcPtr; // should be OSLAccessorProcPtr
{GPC-ONLY-ELSE}
	OSLAccessorUPP = OSLAccessorProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	OSLCompareUPP = UniversalProcPtr; // should be OSLCompareProcPtr
{GPC-ONLY-ELSE}
	OSLCompareUPP = OSLCompareProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	OSLCountUPP = UniversalProcPtr; // should be OSLCountProcPtr
{GPC-ONLY-ELSE}
	OSLCountUPP = OSLCountProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	OSLDisposeTokenUPP = UniversalProcPtr; // should be OSLDisposeTokenProcPtr
{GPC-ONLY-ELSE}
	OSLDisposeTokenUPP = OSLDisposeTokenProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	OSLGetMarkTokenUPP = UniversalProcPtr; // should be OSLGetMarkTokenProcPtr
{GPC-ONLY-ELSE}
	OSLGetMarkTokenUPP = OSLGetMarkTokenProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	OSLGetErrDescUPP = UniversalProcPtr; // should be OSLGetErrDescProcPtr
{GPC-ONLY-ELSE}
	OSLGetErrDescUPP = OSLGetErrDescProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	OSLMarkUPP = UniversalProcPtr; // should be OSLMarkProcPtr
{GPC-ONLY-ELSE}
	OSLMarkUPP = OSLMarkProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	OSLAdjustMarksUPP = UniversalProcPtr; // should be OSLAdjustMarksProcPtr
{GPC-ONLY-ELSE}
	OSLAdjustMarksUPP = OSLAdjustMarksProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewOSLAccessorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewOSLAccessorUPP( userRoutine: OSLAccessorProcPtr ): OSLAccessorUPP;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  NewOSLCompareUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewOSLCompareUPP( userRoutine: OSLCompareProcPtr ): OSLCompareUPP;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  NewOSLCountUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewOSLCountUPP( userRoutine: OSLCountProcPtr ): OSLCountUPP;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  NewOSLDisposeTokenUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewOSLDisposeTokenUPP( userRoutine: OSLDisposeTokenProcPtr ): OSLDisposeTokenUPP;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  NewOSLGetMarkTokenUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewOSLGetMarkTokenUPP( userRoutine: OSLGetMarkTokenProcPtr ): OSLGetMarkTokenUPP;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  NewOSLGetErrDescUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewOSLGetErrDescUPP( userRoutine: OSLGetErrDescProcPtr ): OSLGetErrDescUPP;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  NewOSLMarkUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewOSLMarkUPP( userRoutine: OSLMarkProcPtr ): OSLMarkUPP;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  NewOSLAdjustMarksUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewOSLAdjustMarksUPP( userRoutine: OSLAdjustMarksProcPtr ): OSLAdjustMarksUPP;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  DisposeOSLAccessorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeOSLAccessorUPP( userUPP: OSLAccessorUPP );
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  DisposeOSLCompareUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeOSLCompareUPP( userUPP: OSLCompareUPP );
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  DisposeOSLCountUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeOSLCountUPP( userUPP: OSLCountUPP );
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  DisposeOSLDisposeTokenUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeOSLDisposeTokenUPP( userUPP: OSLDisposeTokenUPP );
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  DisposeOSLGetMarkTokenUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeOSLGetMarkTokenUPP( userUPP: OSLGetMarkTokenUPP );
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  DisposeOSLGetErrDescUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeOSLGetErrDescUPP( userUPP: OSLGetErrDescUPP );
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  DisposeOSLMarkUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeOSLMarkUPP( userUPP: OSLMarkUPP );
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  DisposeOSLAdjustMarksUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeOSLAdjustMarksUPP( userUPP: OSLAdjustMarksUPP );
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  InvokeOSLAccessorUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeOSLAccessorUPP( desiredClass: DescType; const var container: AEDesc; containerClass: DescType; form: DescType; const var selectionData: AEDesc; var value: AEDesc; accessorRefcon: SRefCon; userUPP: OSLAccessorUPP ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  InvokeOSLCompareUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeOSLCompareUPP( oper: DescType; const var obj1: AEDesc; const var obj2: AEDesc; var result: Boolean; userUPP: OSLCompareUPP ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  InvokeOSLCountUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeOSLCountUPP( desiredType: DescType; containerClass: DescType; const var container: AEDesc; var result: SIGNEDLONG; userUPP: OSLCountUPP ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  InvokeOSLDisposeTokenUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeOSLDisposeTokenUPP( var unneededToken: AEDesc; userUPP: OSLDisposeTokenUPP ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  InvokeOSLGetMarkTokenUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeOSLGetMarkTokenUPP( const var dContainerToken: AEDesc; containerClass: DescType; var result: AEDesc; userUPP: OSLGetMarkTokenUPP ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  InvokeOSLGetErrDescUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeOSLGetErrDescUPP( var appDescPtr: AEDescPtr; userUPP: OSLGetErrDescUPP ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  InvokeOSLMarkUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeOSLMarkUPP( const var dToken: AEDesc; const var markToken: AEDesc; index: SIGNEDLONG; userUPP: OSLMarkUPP ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  InvokeOSLAdjustMarksUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeOSLAdjustMarksUPP( newStart: SIGNEDLONG; newStop: SIGNEDLONG; const var markToken: AEDesc; userUPP: OSLAdjustMarksUPP ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{
 *  AEObjectInit()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function AEObjectInit: OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{ Not done by inline, but by direct linking into code.  It sets up the pack
  such that further calls can be via inline }
{
 *  AESetObjectCallbacks()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function AESetObjectCallbacks( myCompareProc: OSLCompareUPP; myCountProc: OSLCountUPP; myDisposeTokenProc: OSLDisposeTokenUPP; myGetMarkTokenProc: OSLGetMarkTokenUPP; myMarkProc: OSLMarkUPP; myAdjustMarksProc: OSLAdjustMarksUPP; myGetErrDescProcPtr: OSLGetErrDescUPP ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AEResolve()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function AEResolve( const var objectSpecifier: AEDesc; callbackFlags: SInt16; var theToken: AEDesc ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AEInstallObjectAccessor()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function AEInstallObjectAccessor( desiredClass: DescType; containerType: DescType; theAccessor: OSLAccessorUPP; accessorRefcon: SRefCon; isSysHandler: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AERemoveObjectAccessor()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function AERemoveObjectAccessor( desiredClass: DescType; containerType: DescType; theAccessor: OSLAccessorUPP; isSysHandler: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AEGetObjectAccessor()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function AEGetObjectAccessor( desiredClass: DescType; containerType: DescType; var accessor: OSLAccessorUPP; var accessorRefcon: SRefCon; isSysHandler: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AEDisposeToken()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function AEDisposeToken( var theToken: AEDesc ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AECallObjectAccessor()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function AECallObjectAccessor( desiredClass: DescType; const var containerToken: AEDesc; containerClass: DescType; keyForm: DescType; const var keyData: AEDesc; var token: AEDesc ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{$endc} {TARGET_OS_MAC}


end.
