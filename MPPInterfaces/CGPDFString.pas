{ CoreGraphics - CGPDFString.h
 * Copyright (c) 2002-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFString;
interface
uses MacTypes,CFBase,CFDate,CGBase;
{$ALIGN POWER}


// CGPDFStringRef defined in CGBase


{ Return the length of `string'. }

function CGPDFStringGetLength( strng: CGPDFStringRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return a pointer to the bytes of `string'. }

function CGPDFStringGetBytePtr( strng: CGPDFStringRef ): UInt8Ptr;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return a CFString representing `string' as a "text string". See Section
   3.8.1 "Text Strings", PDF Reference: Adobe PDF version 1.6 (5th ed.) for
   more information. }

function CGPDFStringCopyTextString( strng: CGPDFStringRef ): CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Convert `string' to a CFDate. See Section 3.8.3 "Dates", PDF Reference:
   Adobe PDF version 1.6 (5th ed.) for more information. }

function CGPDFStringCopyDate( strng: CGPDFStringRef ): CFDateRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);


end.
