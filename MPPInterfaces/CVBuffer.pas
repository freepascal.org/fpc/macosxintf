{
 *  CVBuffer.h
 *  CoreVideo
 *
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }
 
{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2008 }
{  Pascal Translation Update:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }

unit CVBuffer;
interface
uses MacTypes, CFBase, CFDictionary, CVBase, CVReturns;

{$ALIGN POWER}

 
 {! @header CVBuffer.h
	@copyright 2004 Apple Computer, Inc. All rights reserved.
	@availability Mac OS X 10.4 or later
    @discussion CVBufferRef types are abstract and only define ways to attach meta data to buffers (such as timestamps,
	        colorspace information, etc.).    CVBufferRefs do not imply any particular kind of data storage.  It could
		be compressed data, image data, etc.
		   
}


//#pragma mark CVBufferRef attribute keys

{ The following two keys are useful with the CoreVideo pool and texture cache APIs so that you can specify
   an initial set of default buffer attachments to automatically be attached to the buffer when it is created. }
const kCVBufferPropagatedAttachmentsKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVBufferNonPropagatedAttachmentsKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

//#pragma mark CVBufferRef attachment keys

const kCVBufferMovieTimeKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);	// Generally only available for frames emitted by QuickTime; CFDictionary containing these two keys:
const kCVBufferTimeValueKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
const kCVBufferTimeScaleKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);


//#pragma mark CVBufferRef

const
	kCVAttachmentMode_ShouldNotPropagate = 0;
	kCVAttachmentMode_ShouldPropagate    = 1;
type
	CVAttachmentMode = UInt32;

{!
    @typedef	CVBufferRef
    @abstract   Base type for all CoreVideo buffers

}
type
	CVBufferRef = ^__CVBuffer; { an opaque type }
	__CVBuffer = record end;

{!
    @function   CVBufferRetain
    @abstract   Retains a CVBuffer object
    @discussion Like CFRetain CVBufferRetain increments the retain count of a CVBuffer object. In contrast to the CF call it is NULL safe.
    @param      buffer A CVBuffer object that you want to retain.
    @result     A CVBuffer object that is the same as the passed in buffer.
}
function CVBufferRetain( buffer: CVBufferRef ): CVBufferRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);
{!
    @function   CVBufferRelease
    @abstract   Release a CVBuffer object
    @discussion Like CFRetain CVBufferRetain decrements the retain count of a CVBuffer object. If that count consequently becomes zero the memory allocated to the object is deallocated and the object is destroyed. In contrast to the CF call it is NULL safe.
    @param      buffer A CVBuffer object that you want to release.
}
procedure CVBufferRelease( buffer: CVBufferRef );
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

//#pragma mark CVBufferAttachment

{!
    @function   CVBufferSetAttachment
    @abstract   Sets or adds a attachment of a CVBuffer object
    @discussion You can attach any CF object to a CVBuffer object to store additional information. CVBufferGetAttachment stores an attachement identified by a key. If the key doesn't exist, the attachment will be added. If the key does exist, the existing attachment will be replaced. In bouth cases the retain count of the attachment will be incremented. The value can be any CFType but nil has no defined behavior.
    @param      buffer  Target CVBuffer object.
    @param      key     Key in form of a CFString identifying the desired attachment.
    @param      value	Attachment in form af a CF object.
    @param      attachmentMode	Specifies which attachment mode is desired for this attachment.   A particular attachment key may only exist in
                                a single mode at a time.
}
procedure CVBufferSetAttachment( buffer: CVBufferRef; key: CFStringRef; value: CFTypeRef; attachmentMode: CVAttachmentMode );
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);


{!
    @function   CVBufferGetAttachment
    @abstract   Returns a specific attachment of a CVBuffer object
    @discussion You can attach any CF object to a CVBuffer object to store additional information. CVBufferGetAttachment retrieves an attachement identified by a key.
    @param      buffer  Target CVBuffer object.
    @param      key	Key in form of a CFString identifying the desired attachment.
    @param      attachmentMode.  Returns the mode of the attachment, if desired.  May be NULL.
    @result     If found the attachment object
}
function CVBufferGetAttachment( buffer: CVBufferRef; key: CFStringRef; var attachmentMode: CVAttachmentMode ): CFTypeRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVBufferRemoveAttachment
    @abstract   Removes a specific attachment of a CVBuffer object
    @discussion CVBufferRemoveAttachment removes an attachement identified by a key. If found the attachement is removed and the retain count decremented.
    @param      buffer  Target CVBuffer object.
    @param      key	Key in form of a CFString identifying the desired attachment.
}
procedure CVBufferRemoveAttachment( buffer: CVBufferRef; key: CFStringRef );
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVBufferRemoveAllAttachments
    @abstract   Removes all attachments of a CVBuffer object
    @discussion While CVBufferRemoveAttachment removes a specific attachement identified by a key CVBufferRemoveAllAttachments removes all attachments of a buffer and decrements their retain counts.
    @param      buffer  Target CVBuffer object.
}
procedure CVBufferRemoveAllAttachments( buffer: CVBufferRef );
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVBufferGetAttachments
    @abstract   Returns all attachments of a CVBuffer object
    @discussion CVBufferGetAttachments is a convenience call that returns all attachments with their corresponding keys in a CFDictionary.
    @param      buffer  Target CVBuffer object.
    @result     A CFDictionary with all buffer attachments identified by there keys. If no attachment is present, the dictionary is empty.  Returns NULL
		for invalid attachment mode.
}
function CVBufferGetAttachments( buffer: CVBufferRef; attachmentMode: CVAttachmentMode ): CFDictionaryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVBufferSetAttachments
    @abstract   Sets a set of attachments for a CVBuffer
    @discussion CVBufferSetAttachments is a convenience call that in turn calls CVBufferSetAttachment for each key and value in the given dictionary. All key value pairs must be in the root level of the dictionary.
    @param      buffer  Target CVBuffer object.
}
procedure CVBufferSetAttachments( buffer: CVBufferRef; theAttachments: CFDictionaryRef; attachmentMode: CVAttachmentMode );
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVBufferPropagateAttachments
    @abstract   Copy all propagatable attachments from one buffer to another.
    @discussion CVBufferPropagateAttachments is a convenience call that copies all attachments with a mode of kCVAttachmentMode_ShouldPropagate from one
                buffer to another.
    @param      sourceBuffer  CVBuffer to copy attachments from.
    @param      destinationBuffer  CVBuffer to copy attachments to.
}
procedure CVBufferPropagateAttachments( sourceBuffer: CVBufferRef; destinationBuffer: CVBufferRef );
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);


end.
