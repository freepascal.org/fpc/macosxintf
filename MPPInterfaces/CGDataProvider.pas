{ CoreGraphics - CGDataProvider.h
   Copyright (c) 1999-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGDataProvider;
interface
uses MacTypes,MacOSXPosix,CFBase,CFData,CGBase,CFURL;
{$ALIGN POWER}


type
	CGDataProviderRef = ^OpaqueCGDataProviderRef; { an opaque type }
	OpaqueCGDataProviderRef = record end;


{ This callback is called to copy `count' bytes from the sequential data
   stream to `buffer'. }

type
	CGDataProviderGetBytesCallback = function( info: univ Ptr; buffer: univ Ptr; count: size_t ): size_t;

{ This callback is called to skip `count' bytes forward in the sequential
   data stream. It should return the number of bytes that were actually
   skipped. }

type
	CGDataProviderSkipForwardCallback = function( info: univ Ptr; count: off_t ): off_t;

{ This callback is called to rewind to the beginning of sequential data
   stream. }

type
	CGDataProviderRewindCallback = procedure( info: univ Ptr );

{ This callback is called to release the `info' pointer when the data
   provider is freed. }

type
	CGDataProviderReleaseInfoCallback = procedure( info: univ Ptr );

{ Callbacks for sequentially accessing data.
   `version' is the version of this structure. It should be set to 0.
   `getBytes' is called to copy `count' bytes from the sequential data
     stream to `buffer'. It should return the number of bytes copied, or 0
     if there's no more data.
   `skipForward' is called to skip ahead in the sequential data stream by
     `count' bytes.
   `rewind' is called to rewind the sequential data stream to the beginning
     of the data.
   `releaseInfo', if non-NULL, is called to release the `info' pointer when
     the provider is freed. }

type
	CGDataProviderSequentialCallbacks = record
		version: UInt32;
{$ifc TARGET_CPU_64}
		__alignment_dummy: UInt32;
{$endc}
		getBytes: CGDataProviderGetBytesCallback;
		skipForward: CGDataProviderSkipForwardCallback;
		rewind: CGDataProviderRewindCallback;
		releaseInfo: CGDataProviderReleaseInfoCallback;
	end;

{ This callback is called to get a pointer to the entire block of data. }

type
	CGDataProviderGetBytePointerCallback = function( info: univ Ptr ): UnivPtr;

{ This callback is called to release the pointer to entire block of
   data. }

type
	CGDataProviderReleaseBytePointerCallback = procedure( info: univ Ptr; pointr: {const} univ Ptr );

{ This callback is called to copy `count' bytes at byte offset `position'
   into `buffer'. }

type
	CGDataProviderGetBytesAtPositionCallback = function( info: univ Ptr; buffer: univ Ptr; position: off_t; count: size_t ): size_t;

{ Callbacks for directly accessing data.
   `version' is the version of this structure. It should be set to 0.
   `getBytePointer', if non-NULL, is called to return a pointer to the
     provider's entire block of data.
   `releaseBytePointer', if non-NULL, is called to release a pointer to the
     provider's entire block of data.
   `getBytesAtPosition', if non-NULL, is called to copy `count' bytes at
     offset `position' from the provider's data to `buffer'. It should
     return the number of bytes copied, or 0 if there's no more data.
   `releaseInfo', if non-NULL, is called to release the `info' pointer when
     the provider is freed.

   At least one of `getBytePointer' or `getBytesAtPosition' must be
   non-NULL. }

type
	CGDataProviderDirectCallbacks = record
		version: UInt32;
{$ifc TARGET_CPU_64}
		__alignment_dummy: UInt32;
{$endc}
		getBytePointer: CGDataProviderGetBytePointerCallback;
		releaseBytePointer: CGDataProviderReleaseBytePointerCallback;
		getBytesAtPosition: CGDataProviderGetBytesAtPositionCallback;
		releaseInfo: CGDataProviderReleaseInfoCallback;
	end;

{ Return the CFTypeID for CGDataProviderRefs. }

function CGDataProviderGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Create a sequential-access data provider using `callbacks' to provide the
   data. `info' is passed to each of the callback functions. }

function CGDataProviderCreateSequential( info: univ Ptr; const var callbacks: CGDataProviderSequentialCallbacks ): CGDataProviderRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Create a direct-access data provider using `callbacks' to supply `size'
   bytes of data. `info' is passed to each of the callback functions.
   The underlying data must not change for the life of the data provider. }

function CGDataProviderCreateDirect( info: univ Ptr; size: off_t; const var callbacks: CGDataProviderDirectCallbacks ): CGDataProviderRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ The callback used by `CGDataProviderCreateWithData'. }

type
	CGDataProviderReleaseDataCallback = procedure( info: univ Ptr; data: {const} univ Ptr; size: size_t );

{ Create a direct-access data provider using `data', an array of `size'
   bytes. `releaseData' is called when the data provider is freed, and is
   passed `info' as its first argument. }

function CGDataProviderCreateWithData( info: univ Ptr; data: {const} univ Ptr; size: size_t; releaseData: CGDataProviderReleaseDataCallback ): CGDataProviderRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create a direct-access data provider which reads from `data'. }

function CGDataProviderCreateWithCFData( data: CFDataRef ): CGDataProviderRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Create a data provider reading from `url'. }

function CGDataProviderCreateWithURL( url: CFURLRef ): CGDataProviderRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create a data provider reading from `filename'. }

function CGDataProviderCreateWithFilename( filename: ConstCStringPtr ): CGDataProviderRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRetain(provider)', but doesn't crash (as CFRetain does)
   if `provider' is NULL. }

function CGDataProviderRetain( provider: CGDataProviderRef ): CGDataProviderRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRelease(provider)', but doesn't crash (as CFRelease
   does) if `provider' is NULL. }

procedure CGDataProviderRelease( provider: CGDataProviderRef );
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return a copy of the data specified by provider. Returns NULL if a
   complete copy of the data can't be obtained (for example, if the
   underlying data is too large to fit in memory). }

function CGDataProviderCopyData( provider: CGDataProviderRef ): CFDataRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{$ifc TARGET_OS_MAC}
{ Deprecated API. }

{ This callback is called to skip `count' bytes forward in the sequential
   data stream. }

type
	CGDataProviderSkipBytesCallback = procedure( info: univ Ptr; count: size_t );

{ Old-style callbacks for sequentially accessing data.
   `getBytes' is called to copy `count' bytes from the sequential data
     stream to `buffer'. It should return the number of bytes copied, or 0
     if there's no more data.
   `skipBytes' is called to skip ahead in the sequential data stream by
     `count' bytes.
   `rewind' is called to rewind the sequential data stream to the beginning
     of the data.
   `releaseProvider', if non-NULL, is called to release the `info' pointer
     when the provider is freed. }

type
	CGDataProviderCallbacks = record
		getBytes: CGDataProviderGetBytesCallback;
		skipBytes: CGDataProviderSkipBytesCallback;
		rewind: CGDataProviderRewindCallback;
		releaseProvider: CGDataProviderReleaseInfoCallback;
	end;

{ This callback is called to copy `count' bytes at byte offset `offset'
   into `buffer'. }

type
	CGDataProviderGetBytesAtOffsetCallback = function( info: univ Ptr; buffer: univ Ptr; offset: size_t; count: size_t ): size_t;

{ Callbacks for directly accessing data.
   `getBytePointer', if non-NULL, is called to return a pointer to the
     provider's entire block of data.
   `releaseBytePointer', if non-NULL, is called to release a pointer to the
     provider's entire block of data.
   `getBytes', if non-NULL, is called to copy `count' bytes at offset
     `offset' from the provider's data to `buffer'. It should return the
     number of bytes copied, or 0 if there's no more data.
   `releaseProvider', if non-NULL, is called when the provider is freed.
  
   At least one of `getBytePointer' or `getBytes' must be non-NULL. }

type
	CGDataProviderDirectAccessCallbacks = record
		getBytePointer: CGDataProviderGetBytePointerCallback;
		releaseBytePointer: CGDataProviderReleaseBytePointerCallback;
		getBytes: CGDataProviderGetBytesAtOffsetCallback;
		releaseProvider: CGDataProviderReleaseInfoCallback;
	end;

{ Create a sequential-access data provider using `callbacks' to provide the
   data. `info' is passed to each of the callback functions. }

function CGDataProviderCreate( info: univ Ptr; const var callbacks: CGDataProviderCallbacks ): CGDataProviderRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);

{ Create a direct-access data provider using `callbacks' to supply `size'
   bytes of data. `info' is passed to each of the callback functions. }

function CGDataProviderCreateDirectAccess( info: univ Ptr; size: size_t; const var callbacks: CGDataProviderDirectAccessCallbacks ): CGDataProviderRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);
{$endc}

end.
