{*
 *      filename: MDImporter.h
 *      created : Thu Apr  8 13:33:00 2004
 *      LastEditDate Was "Thu Jul  7 14:09:31 2005"
 *
 }
unit MDImporter;
interface
uses MacTypes,CFBase,CFString,CFUUID,CFDictionary,CFPlugIn,CFPlugInCOM,MDItem;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{!
        @header MDImporter.h
        MDImporter is the link between the metadata contained  within a
        file and an MDItem. The Server listens for changes to files
        and loads in a CFPlugIn (MDImporter) and the importer will then
        extract all interesting data out of the file to store in a MDItem.

}


{!
        @constant kMDImporterTypeID The importer only loads CFPlugIns of type
        kMDImporterTypeID - 8B08C4BF-415B-11D8-B3F9-0003936726FC

        @constant kMDImporterInterfaceID Importers must implement this
        Interface - 6EBC27C4-899C-11D8-84A3-0003936726FC

        @constant kMDExporterInterfaceID Exporters can optionaly also implement this
        Interface - B41C6074-7DFB-4057-969D-31C8E861A8D4

        @constant kMDImporterURLInterfaceID Importers can optionaly also implement this
        Interface - B41C6074-7DFB-4057-969D-31C8E861A8D4
}
{FPC-ONLY-START}
implemented function kMDImporterTypeID: CFUUIDRef; inline;
implemented function kMDImporterInterfaceID: CFUUIDRef; inline; 
implemented function kMDExporterInterfaceID: CFUUIDRef; inline;
implemented function kMDImporterURLInterfaceID: CFUUIDRef; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec kMDImporterTypeID      CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$8B,$08,$C4,$BF,$41,$5B,$11,$D8,$B3,$F9,$00,$03,$93,$67,$26,$FC)}
{$mwgpcdefinec kMDImporterInterfaceID CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$6E,$BC,$27,$C4,$89,$9C,$11,$D8,$84,$AE,$00,$03,$93,$67,$26,$FC)}
{$mwgpcdefinec kMDExporterInterfaceID CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$B4,$1C,$60,$74,$7D,$FB,$40,$57,$96,$9D,$31,$C8,$E8,$61,$A8,$D4)}
{$mwgpcdefinec kMDImporterURLInterfaceID CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$13,$F6,$0F,$02,$36,$22,$4F,$35,$98,$91,$EC,$10,$E6,$CD,$08,$F8)}
{FPC-ONLY-FINISH}

{!
        @typedef MDImporterInterfaceStruct
        Interface for the plugIn. The plugin needs to provide at least
        the ImporterImportData function.

        @function ImporterImportData
        Import data from the file given to by path into the system.

        @param thisInterface this plugin

        @param attributes any attributes that can be gotten from the
        file should be places in the attributes
        CFMutableDictionaryRef. For example if the file has a
        kMDItemDurationSeconds then the key would be kMDItemDurationSeconds and the
        value would be a CFNumberRef containing the duration of the
        song etc. If you want to remove an attribute, then in the
        dictionary place the attribute and set the value of the
        attribute to kCFNull.

        @param contentTypeUTI  The content type (a uniform type identifier)
        of the file that is given to this plugin.

        @param pathToFile location of the file on disk.

        @result Boolean indicating if the import was successful


        @typedef MDExporterInterfaceStruct
        Interface for exporting data from the mdimport system back to the file.

        @function ImporterExportData

        @param thisInterface this plugin

        @param attributes any attributes should be written back to the file

        @param contentTypeUTI  The content type (a uniform type identifier)
        of the file that is given to this plugin.

        @param pathToFile location of the file on disk.

        @result Boolean indicating if the export was successful
}
type
	MDImporterInterfaceStruct = record
		IUNKNOWN_C_GUTS	: IUnknownVTbl;
		ImporterImportData : function(thisInterface: univ Ptr; attributes: CFMutableDictionaryRef; contentTypeUTI: CFStringRef; pathToFile: CFStringRef): Boolean;
	end;
	MDImporterInterfaceStructPtr = ^MDImporterInterfaceStruct;
	
	MDExporterInterfaceStruct = record
		IUNKNOWN_C_GUTS	: IUnknownVTbl;
		ImporterExportData : function(thisInterface: univ Ptr; attributes: CFDictionaryRef; contentTypeUTI: CFStringRef; pathToFile: CFStringRef): Boolean;
	end;
	MDExporterInterfaceStructPtr = ^MDExporterInterfaceStruct;

{$endc} {TARGET_OS_MAC}

implementation

{$ifc TARGET_OS_MAC}

{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

{FPC-ONLY-START}
function kMDImporterTypeID: CFUUIDRef; inline;
begin
	kMDImporterTypeID := CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$8B,$08,$C4,$BF,$41,$5B,$11,$D8,$B3,$F9,$00,$03,$93,$67,$26,$FC)
end;

function kMDImporterInterfaceID: CFUUIDRef; inline;
begin
	kMDImporterInterfaceID := CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$6E,$BC,$27,$C4,$89,$9C,$11,$D8,$84,$AE,$00,$03,$93,$67,$26,$FC)
end;

function kMDExporterInterfaceID: CFUUIDRef; inline;
begin
	kMDExporterInterfaceID := CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$B4,$1C,$60,$74,$7D,$FB,$40,$57,$96,$9D,$31,$C8,$E8,$61,$A8,$D4)
end;

function kMDImporterURLInterfaceID: CFUUIDRef; inline;
begin
	kMDImporterURLInterfaceID := CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault,$13,$F6,$0F,$02,$36,$22,$4F,$35,$98,$91,$EC,$10,$E6,$CD,$08,$F8)
end;


{FPC-ONLY-FINISH}

{$endc} {TARGET_OS_MAC}

end.
