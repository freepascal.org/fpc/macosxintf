{ CoreGraphics - CGColor.h
 * Copyright (c) 2003-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGColor;
interface
uses MacTypes,CFBase,CGBase,CGColorSpace;
{$ALIGN POWER}


// CGColorRef defined in CGBase


{ Create a color in the color space `space' with color components
   (including alpha) specified by `components'. `space' may be any color
   space except a pattern color space. }

function CGColorCreate( space: CGColorSpaceRef; {const} components: {variable-size-array} CGFloatPtr ): CGColorRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{$ifc TARGET_OS_MAC}
{ Create a color in the "Generic" gray color space. }

function CGColorCreateGenericGray( gray: CGFloat; alpha: CGFloat ): CGColorRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ Create a color in the "Generic" RGB color space. }

function CGColorCreateGenericRGB( red: CGFloat; green: CGFloat; blue: CGFloat; alpha: CGFloat ): CGColorRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ Create a color in the "Generic" CMYK color space. }

function CGColorCreateGenericCMYK( cyan: CGFloat; magenta: CGFloat; yellow: CGFloat; black: CGFloat; alpha: CGFloat ): CGColorRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ Return a constant color. As `CGColorGetConstantColor' is not a "Copy" or
   "Create" function, it does not necessarily return a new reference each
   time it's called. As a consequence, you should not release the returned
   value. However, colors returned from `CGColorGetConstantColor' can be
   retained and released in a properly nested fashion, just like any other
   CF type. }

function CGColorGetConstantColor( colorName: CFStringRef ): CGColorRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);
{$endc}

{ Create a color in color space `space' with pattern `pattern' and
   components `components'. `space' must be a pattern color space. }

function CGColorCreateWithPattern( colorspace: CGColorSpaceRef; pattern: CGPatternRef; {const} components: {variable-size-array} CGFloatPtr ): CGColorRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Create a copy of `color'. }

function CGColorCreateCopy( color: CGColorRef ): CGColorRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Create a copy of `color' with alpha set to `alpha'. }

function CGColorCreateCopyWithAlpha( color: CGColorRef; alpha: CGFloat ): CGColorRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Equivalent to `CFRetain(color)', except it doesn't crash (as CFRetain
   does) if `color' is NULL. }

function CGColorRetain( color: CGColorRef ): CGColorRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Equivalent to `CFRelease(color)', except it doesn't crash (as CFRelease
   does) if `color' is NULL. }

procedure CGColorRelease( color: CGColorRef );
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return true if `color1' is equal to `color2'; false otherwise. }

function CGColorEqualToColor( color1: CGColorRef; color2: CGColorRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the number of color components (including alpha) associated with
   `color'. }

function CGColorGetNumberOfComponents( color: CGColorRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the color components (including alpha) associated with `color'. }

function CGColorGetComponents( color: CGColorRef ): CGFloatPtr;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the alpha component associated with `color'. }

function CGColorGetAlpha( color: CGColorRef ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the color space associated with `color'. }

function CGColorGetColorSpace( color: CGColorRef ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the pattern associated with `color', if it's a color in a pattern
   color space; NULL otherwise. }

function CGColorGetPattern( color: CGColorRef ): CGPatternRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the CFTypeID for CGColors. }

function CGColorGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{$ifc TARGET_OS_MAC}
{** Names of colors for use with `CGColorGetConstantColor'. **}

{ Colors in the "Generic" gray color space. }

const kCGColorWhite: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

const kCGColorBlack: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

const kCGColorClear: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);
{$endc}


end.
