{
 * Copyright (c) 1999-2002,2004 Apple Computer, Inc. All Rights Reserved.
 * 
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 *
 * x509defs.h -- Data structures for X509 Certificate Library field values
 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, September 2010 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }
unit x509defs;
interface
uses MacTypes,cssmtype;

{$ifc TARGET_OS_MAC}

{FPC-ONLY-START}
{$packrecords c}
{FPC-ONLY-FINISH}
{GPC-ONLY-START}
{$no-pack-struct, maximum-field-alignment 0}
{GPC-ONLY-FINISH}


type
	CSSM_BER_TAG = UInt8;
const
	BER_TAG_UNKNOWN = 0;
const
	BER_TAG_BOOLEAN = 1;
const
	BER_TAG_INTEGER = 2;
const
	BER_TAG_BIT_STRING = 3;
const
	BER_TAG_OCTET_STRING = 4;
const
	BER_TAG_NULL = 5;
const
	BER_TAG_OID = 6;
const
	BER_TAG_OBJECT_DESCRIPTOR = 7;
const
	BER_TAG_EXTERNAL = 8;
const
	BER_TAG_REAL = 9;
const
	BER_TAG_ENUMERATED = 10;
{ 12 to 15 are reserved for future versions of the recommendation }
const
	BER_TAG_PKIX_UTF8_STRING = 12;
const
	BER_TAG_SEQUENCE = 16;
const
	BER_TAG_SET = 17;
const
	BER_TAG_NUMERIC_STRING = 18;
const
	BER_TAG_PRINTABLE_STRING = 19;
const
	BER_TAG_T61_STRING = 20;
const
  BER_TAG_TELETEX_STRING = BER_TAG_T61_STRING;
const
	BER_TAG_VIDEOTEX_STRING = 21;
const
	BER_TAG_IA5_STRING = 22;
const
	BER_TAG_UTC_TIME = 23;
const
	BER_TAG_GENERALIZED_TIME = 24;
const
	BER_TAG_GRAPHIC_STRING = 25;
const
	BER_TAG_ISO646_STRING = 26;
const
	BER_TAG_GENERAL_STRING = 27;
const
  BER_TAG_VISIBLE_STRING = BER_TAG_ISO646_STRING;
{ 28 - are reserved for future versions of the recommendation }
const
	BER_TAG_PKIX_UNIVERSAL_STRING = 28;
const
	BER_TAG_PKIX_BMP_STRING = 30;


{ Data Structures for X.509 Certificates }

type
	CSSM_X509_ALGORITHM_IDENTIFIER_PTR = ^cssm_x509_algorithm_identifier;
	CSSM_X509_ALGORITHM_IDENTIFIERPtr = ^cssm_x509_algorithm_identifier;
	cssm_x509_algorithm_identifier = record
		algorithm: CSSM_OID;
		parameters: CSSM_DATA;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{ X509 Distinguished name structure }
type
	CSSM_X509_TYPE_VALUE_PAIR_PTR = ^cssm_x509_type_value_pair;
	CSSM_X509_TYPE_VALUE_PAIRPtr = ^cssm_x509_type_value_pair;
	cssm_x509_type_value_pair = record
		typ: CSSM_OID;
		valueType: CSSM_BER_TAG; { The Tag to be used when }
    {this value is BER encoded }
		value: CSSM_DATA;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509_RDN_PTR = ^cssm_x509_rdn;
	CSSM_X509_RDNPtr = ^cssm_x509_rdn;
	cssm_x509_rdn = record
		numberOfPairs: UInt32;
		AttributeTypeAndValue: CSSM_X509_TYPE_VALUE_PAIR_PTR;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509_NAME_PTR = ^cssm_x509_name;
	CSSM_X509_NAMEPtr = ^cssm_x509_name;
	cssm_x509_name = record
		numberOfRDNs: UInt32;
		RelativeDistinguishedName: CSSM_X509_RDN_PTR;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{ Public key info struct }
type
	CSSM_X509_SUBJECT_PUBLIC_KEY_INFO_PTR = ^cssm_x509_subject_public_key_info;
	CSSM_X509_SUBJECT_PUBLIC_KEY_INFOPtr = ^cssm_x509_subject_public_key_info;
	cssm_x509_subject_public_key_info = record
		algorithm: CSSM_X509_ALGORITHM_IDENTIFIER;
		subjectPublicKey: CSSM_DATA;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509_TIME_PTR = ^cssm_x509_time;
	CSSM_X509_TIMEPtr = ^cssm_x509_time;
	cssm_x509_time = record
		timeType: CSSM_BER_TAG;
		time: CSSM_DATA;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{ Validity struct }
type
	CSSM_X509_VALIDITY_PTR = ^CSSM_X509_VALIDITY;
	CSSM_X509_VALIDITYPtr = ^CSSM_X509_VALIDITY;
	CSSM_X509_VALIDITY = record
		notBefore: CSSM_X509_TIME;
		notAfter: CSSM_X509_TIME;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

const
  CSSM_X509_OPTION_PRESENT = CSSM_TRUE;
  CSSM_X509_OPTION_NOT_PRESENT = CSSM_FALSE;

type
	CSSM_X509_OPTION = CSSM_BOOL;

type
	CSSM_X509EXT_BASICCONSTRAINTS_PTR = ^cssm_x509ext_basicConstraints;
	CSSM_X509EXT_BASICCONSTRAINTSPtr = ^cssm_x509ext_basicConstraints;
	cssm_x509ext_basicConstraints = record
		cA: CSSM_BOOL;
		pathLenConstraintPresent: CSSM_X509_OPTION;
		pathLenConstraint: UInt32;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
  CSSM_X509EXT_DATA_FORMAT = UInt32;

const
  CSSM_X509_DATAFORMAT_ENCODED = 0;
  CSSM_X509_DATAFORMAT_PARSED = 1;
  CSSM_X509_DATAFORMAT_PAIR = 2;

type
	CSSM_X509EXT_TAGandVALUE_PTR = ^CSSM_X509EXT_TAGandVALUE;
	CSSM_X509EXT_TAGandVALUEPtr = ^CSSM_X509EXT_TAGandVALUE;
	CSSM_X509EXT_TAGandVALUE = record
		typ: CSSM_BER_TAG;
		value: CSSM_DATA;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509EXT_PAIR_PTR = ^cssm_x509ext_pair;
	CSSM_X509EXT_PAIRPtr = ^cssm_x509ext_pair;
	cssm_x509ext_pair = record
		tagAndValue: CSSM_X509EXT_TAGandVALUE;
		parsedValue: UnivPtr;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{ Extension structure }
type
  __embedded_cssm_x509_extension = record
    case Integer of
      0: (tagAndValue: CSSM_X509EXT_TAGandVALUEPtr);
      1: (parsedValue: UnivPtr);
      2: (valuePair: CSSM_X509EXT_PAIRPtr);
  end;

  CSSM_X509_EXTENSION_PTR = ^cssm_x509_extension;
  CSSM_X509_EXTENSIONPtr = ^cssm_x509_extension;
	cssm_x509_extension = record
		extnId: CSSM_OID;
		critical: CSSM_BOOL;
		format: CSSM_X509EXT_DATA_FORMAT;
		value: __embedded_cssm_x509_extension;
    BERvalue: CSSM_DATA ;
  end;
  DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509_EXTENSIONS_PTR = ^cssm_x509_extensions;
	CSSM_X509_EXTENSIONSPtr = ^cssm_x509_extensions;
	cssm_x509_extensions = record
		numberOfExtensions: UInt32;
		extensions: CSSM_X509_EXTENSION_PTR;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{ X509V3 certificate structure }
type
	CSSM_X509_TBS_CERTIFICATE_PTR = ^cssm_x509_tbs_certificate;
	CSSM_X509_TBS_CERTIFICATEPtr = ^cssm_x509_tbs_certificate;
	cssm_x509_tbs_certificate = record
		version: CSSM_DATA;
		serialNumber: CSSM_DATA;
		signature: CSSM_X509_ALGORITHM_IDENTIFIER;
		issuer: CSSM_X509_NAME;
		validity: CSSM_X509_VALIDITY;
		subject: CSSM_X509_NAME;
		subjectPublicKeyInfo: CSSM_X509_SUBJECT_PUBLIC_KEY_INFO;
		issuerUniqueIdentifier: CSSM_DATA;
		subjectUniqueIdentifier: CSSM_DATA;
		extensions: CSSM_X509_EXTENSIONS;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{ Signature structure }
type
	CSSM_X509_SIGNATURE_PTR = ^cssm_x509_signature;
	CSSM_X509_SIGNATUREPtr = ^cssm_x509_signature;
	cssm_x509_signature = record
		algorithmIdentifier: CSSM_X509_ALGORITHM_IDENTIFIER;
		encrypted: CSSM_DATA;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{ Signed certificate structure }
type
	CSSM_X509_SIGNED_CERTIFICATE_PTR = ^cssm_x509_signed_certificate;
	CSSM_X509_SIGNED_CERTIFICATEPtr = ^cssm_x509_signed_certificate;
	cssm_x509_signed_certificate = record
		certificate: CSSM_X509_TBS_CERTIFICATE;
		signature: CSSM_X509_SIGNATURE;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509EXT_POLICYQUALIFIERINFO_PTR = ^cssm_x509ext_policyQualifierInfo;
	CSSM_X509EXT_POLICYQUALIFIERINFOPtr = ^cssm_x509ext_policyQualifierInfo;
	cssm_x509ext_policyQualifierInfo = record
		policyQualifierId: CSSM_OID;
		value: CSSM_DATA;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509EXT_POLICYQUALIFIERS_PTR = ^cssm_x509ext_policyQualifiers;
	CSSM_X509EXT_POLICYQUALIFIERSPtr = ^cssm_x509ext_policyQualifiers;
	cssm_x509ext_policyQualifiers = record
		numberOfPolicyQualifiers: UInt32;
		policyQualifier: CSSM_X509EXT_POLICYQUALIFIERINFOPtr;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509EXT_POLICYINFO_PTR = ^cssm_x509ext_policyInfo;
	CSSM_X509EXT_POLICYINFOPtr = ^cssm_x509ext_policyInfo;
	cssm_x509ext_policyInfo = record
		policyIdentifier: CSSM_OID;
		policyQualifiers: CSSM_X509EXT_POLICYQUALIFIERS;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;


{ Data Structures for X.509 Certificate Revocations Lists }

{ x509V2 entry in the CRL revokedCertificates sequence }
type
	CSSM_X509_REVOKED_CERT_ENTRY_PTR = ^cssm_x509_revoked_cert_entry;
	CSSM_X509_REVOKED_CERT_ENTRYPtr = ^cssm_x509_revoked_cert_entry;
	cssm_x509_revoked_cert_entry = record
		certificateSerialNumber: CSSM_DATA;
		revocationDate: CSSM_X509_TIME;
		extensions: CSSM_X509_EXTENSIONS;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509_REVOKED_CERT_LIST_PTR = ^cssm_x509_revoked_cert_list;
	CSSM_X509_REVOKED_CERT_LISTPtr = ^cssm_x509_revoked_cert_list;
	cssm_x509_revoked_cert_list = record
		numberOfRevokedCertEntries: UInt32;
		revokedCertEntry: CSSM_X509_REVOKED_CERT_ENTRY_PTR;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{ x509v2 Certificate Revocation List (CRL) (unsigned) structure }
type
	CSSM_X509_TBS_CERTLIST_PTR = ^cssm_x509_tbs_certlist;
	CSSM_X509_TBS_CERTLISTPtr = ^cssm_x509_tbs_certlist;
	cssm_x509_tbs_certlist = record
		version: CSSM_DATA;
		signature: CSSM_X509_ALGORITHM_IDENTIFIER;
		issuer: CSSM_X509_NAME;
		thisUpdate: CSSM_X509_TIME;
		nextUpdate: CSSM_X509_TIME;
		revokedCertificates: CSSM_X509_REVOKED_CERT_LIST_PTR;
		extensions: CSSM_X509_EXTENSIONS;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_X509_SIGNED_CRL_PTR = ^cssm_x509_signed_crl;
	CSSM_X509_SIGNED_CRLPtr = ^cssm_x509_signed_crl;
	cssm_x509_signed_crl = record
		tbsCertList: CSSM_X509_TBS_CERTLIST;
		signature: CSSM_X509_SIGNATURE;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{$endc} {TARGET_OS_MAC}

end.
