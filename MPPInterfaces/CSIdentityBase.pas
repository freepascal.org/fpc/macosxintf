{
     File:       OSServices/CSIdentityBase.h
 
     Contains:   CSIdentity APIs
 
     Copyright:  (c) 2006-2011 Apple Inc. All rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit CSIdentityBase;
interface
uses MacTypes,CFBase,SecBase;

	
{$ALIGN MAC68K}
	
	
{
 The error domain of all CFErrors reported by Identity Services 
 }
{
 *  kCSIdentityErrorDomain
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kCSIdentityErrorDomain: CFStringRef;
__OSX_AVAILABLE_STARTING( __MAC_10_5,__IPHONE_5_0 ); 

{
 *  CSIdentity error codes
 *  
 *  Discussion:
 *    Error codes in the CSIdentity error domain
 }
const
{
	 * The specified authority is not recognized
	 }
	kCSIdentityUnknownAuthorityErr = -1;
	
	{
	 * The specified authority is currently not accessible
	 }
	kCSIdentityAuthorityNotAccessibleErr = -2;
	
	{
	 * The caller does not have permission to perform the operation
	 }
	kCSIdentityPermissionErr = -3;
	
	{
	 * The requested identity has been deteled
	 }
	kCSIdentityDeletedErr = -4;
	
	{
	 * The full name is not valid (length: [1-255])
	 }
	kCSIdentityInvalidFullNameErr = -5;
	
	{
	 * The full name is aleady assigned to another identity
	 }
	kCSIdentityDuplicateFullNameErr = -6;
	
	{
	 * The Posix name is not valid (char set: [a-zA-Z0-9_-] length:
	 * [1-255])
	 }
	kCSIdentityInvalidPosixNameErr = -7;
	
	{
	 * The Posix name is aleady assigned to another identity
	 }
	kCSIdentityDuplicatePosixNameErr = -8;
	


end.
