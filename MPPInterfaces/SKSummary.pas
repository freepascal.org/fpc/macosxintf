{
     File:       SearchKit/SKSummary.h
 
     Contains:   SearchKit Interfaces.
 
     Version:    SearchKit-407~38
 
     Copyright:  � 2004-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit SKSummary;
interface
uses MacTypes,CFBase,CFString;
{$ALIGN POWER}

{$ifc TARGET_OS_MAC}
{
 * Summarization API
 }


{
 *  SKSummaryRef
 *  
 *  Summary:
 *    An opaque data type representing summary information.
 *  
 *  Discussion:
 *    A summary reference contains summary information, from which
 *    summary text can be obtained.
 }
type
	SKSummaryRef = ^__SKSummary; { an opaque type }
	__SKSummary = record end;
	SKSummaryRefPtr = ^SKSummaryRef;
{
 *  SKSummaryGetTypeID()
 *  
 *  Summary:
 *    Returns the type identifier of the SKSummaryRef type.
 *  
 *  Result:
 *    Returns a CFTypeID object, or <tt>NULL</tt> on failure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  SKSummaryCreateWithString()
 *  
 *  Summary:
 *    Creates a summary reference with text string.
 *  
 *  Discussion:
 *    Creates a summary reference that pre-computes what is needed for
 *    fast summarization. This function must be balanced with a call at
 *    a later time to CFRelease.
 *  
 *  Parameters:
 *    
 *    inString:
 *      the text string.
 *  
 *  Result:
 *    Returns a summary reference, or <tt>NULL</tt> on failure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryCreateWithString( inString: CFStringRef ): SKSummaryRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  SKSummaryGetSentenceCount()
 *  
 *  Summary:
 *    Gets the number of sentences available.
 *  
 *  Parameters:
 *    
 *    summary:
 *      A reference to the summary object
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryGetSentenceCount( summary: SKSummaryRef ): CFIndex;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  SKSummaryGetParagraphCount()
 *  
 *  Summary:
 *    Gets the number of paragraphs available.
 *  
 *  Parameters:
 *    
 *    summary:
 *      A reference to the summary object
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryGetParagraphCount( summary: SKSummaryRef ): CFIndex;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  SKSummaryCopySentenceAtIndex()
 *  
 *  Summary:
 *    Gets the ith sentence from the original text.
 *  
 *  Parameters:
 *    
 *    summary:
 *      A reference to the summary object
 *    
 *    i:
 *      zero-based index
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryCopySentenceAtIndex( summary: SKSummaryRef; i: CFIndex ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  SKSummaryCopyParagraphAtIndex()
 *  
 *  Summary:
 *    Gets the ith paragraph from the original text.
 *  
 *  Parameters:
 *    
 *    summary:
 *      A reference to the summary object
 *    
 *    i:
 *      zero-based index
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryCopyParagraphAtIndex( summary: SKSummaryRef; i: CFIndex ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  SKSummaryCopySentenceSummaryString()
 *  
 *  Summary:
 *    Gets a summary string that includes at most the requested number
 *    of sentences.
 *  
 *  Parameters:
 *    
 *    summary:
 *      A reference to the summary object
 *    
 *    numSentences:
 *      the number of sentences desired
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryCopySentenceSummaryString( summary: SKSummaryRef; numSentences: CFIndex ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  SKSummaryCopyParagraphSummaryString()
 *  
 *  Summary:
 *    Gets a summary string that includes at most the requested number
 *    of paragraphs.
 *  
 *  Parameters:
 *    
 *    summary:
 *      A reference to the summary object
 *    
 *    numParagraphs:
 *      the number of paragraphs desired
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryCopyParagraphSummaryString( summary: SKSummaryRef; numParagraphs: CFIndex ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  SKSummaryGetSentenceSummaryInfo()
 *  
 *  Summary:
 *    Get detailed information about a sentence-based summary. Useful
 *    for constructing your own summary string. Arrays must be of size
 *    numSentences or they can be nil. Return value is the number of
 *    sentences actually returned. Sentences are returned in text order
 *    via outSentenceIndexOfSentences.
 *  
 *  Parameters:
 *    
 *    summary:
 *      A reference to the summary object
 *    
 *    numSentencesInSummary:
 *      the number of sentences desired
 *    
 *    outRankOrderOfSentences:
 *      array for returning the rank of each sentence; most important
 *      sentence is rank 1
 *    
 *    outSentenceIndexOfSentences:
 *      array for returning the index of each sentence; use
 *      SKSummaryCopySentenceAtIndex to get sentence
 *    
 *    outParagraphIndexOfSentences:
 *      array for returning the index of the paragraph in which
 *      sentence occured
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryGetSentenceSummaryInfo( summary: SKSummaryRef; numSentencesInSummary: CFIndex; outRankOrderOfSentences: CFIndexPtr { array }; outSentenceIndexOfSentences: CFIndexPtr { array }; outParagraphIndexOfSentences: CFIndexPtr { array } ): CFIndex;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  SKSummaryGetParagraphSummaryInfo()
 *  
 *  Summary:
 *    Get detailed information about a paragraph-based summary. Useful
 *    for constructing your own summary string. Arrays must be of size
 *    numParagraphs or they can be nil. Return value is the number of
 *    paragraphs actually returned. Paragraphs are returned in text
 *    order via outParagraphIndexOfParagraphs.
 *  
 *  Parameters:
 *    
 *    summary:
 *      A reference to the summary object
 *    
 *    numParagraphsInSummary:
 *      the number of sentences desired
 *    
 *    outRankOrderOfParagraphs:
 *      array for returning the rank of each paragraph; most important
 *      paragraph is rank 1
 *    
 *    outParagraphIndexOfParagraphs:
 *      array for returning the index of each paragraph; use
 *      SKSummaryCopyParagraphAtIndex to get paragraph
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKSummaryGetParagraphSummaryInfo( summary: SKSummaryRef; numParagraphsInSummary: CFIndex; outRankOrderOfParagraphs: CFIndexPtr { array }; outParagraphIndexOfParagraphs: CFIndexPtr {array} ): CFIndex;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{$endc} {TARGET_OS_MAC}


end.
