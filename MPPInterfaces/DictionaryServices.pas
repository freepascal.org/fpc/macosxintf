{
	DictionaryServices.h
	DictionaryServices framework
     
	Copyright (c) 2007 Apple Inc. All rights reserved.
}
unit DictionaryServices;
interface
uses MacTypes,CFBase;
{$ALIGN POWER}


{!
	@typedef	DCSDictionaryRef
	@abstract	Opaque CF object that represents a dictionary file
}
type
	DCSDictionaryRef = ^__DCSDictionary; { an opaque type }
	__DCSDictionary = record end;

{!
	@function	DCSGetTermRangeInString
	@abstract	Look for a word or a phrase that contains the specified offset in dictionaries
				activated in Dictionary.app preference
	@param		dictionary
				This parameter is not supported for Leopard. You should always pass NULL.
	@param		textString
				Text that contains the word or phrase to look up
	@param		offset
				Specifies a character offset in textString
	@result		Returns a detected range of word or phrase around the specified offset,
				or (kCFNotFound, 0) is returned if any term is not found in active dictionaries.
				The result range can be used as an input parameter of DCSCopyTextDefinition()
				and HIDictionaryWindowShow() in Carbon framework.
}
function DCSGetTermRangeInString( dictionary: DCSDictionaryRef; textString: CFStringRef; offset: CFIndex ): CFRange;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;

{!
	@function	DCSCopyTextDefinition
	@abstract	Copies definition for a specified range of text
	@param		dictionary
				This parameter is not supported for Leopard. You should always pass NULL.
	@param		textString
				Text that contains the word or phrase to look up
	@param		range
				Range of the target word or phrase in textString
	@result		Returns a definition of the specified term in range in plain text
}
function DCSCopyTextDefinition( dictionary: DCSDictionaryRef; textString: CFStringRef; range: CFRange ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


end.
