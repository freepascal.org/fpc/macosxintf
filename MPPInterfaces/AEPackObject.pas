{
     File:       AE/AEPackObject.h
 
     Contains:   AppleEvents object packing Interfaces.
 
    
 
     Copyright:  � 1991-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit AEPackObject;
interface
uses MacTypes,AEDataModel,AppleEvents;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{ These are the object packing routines.  }
{
 *  CreateOffsetDescriptor()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function CreateOffsetDescriptor( theOffset: SIGNEDLONG; var theDescriptor: AEDesc ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  CreateCompDescriptor()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function CreateCompDescriptor( comparisonOperator: DescType; var operand1: AEDesc; var operand2: AEDesc; disposeInputs: Boolean; var theDescriptor: AEDesc ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  CreateLogicalDescriptor()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function CreateLogicalDescriptor( var theLogicalTerms: AEDescList; theLogicOperator: DescType; disposeInputs: Boolean; var theDescriptor: AEDesc ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  CreateObjSpecifier()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function CreateObjSpecifier( desiredClass: DescType; var theContainer: AEDesc; keyForm: DescType; var keyData: AEDesc; disposeInputs: Boolean; var objSpecifier: AEDesc ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  CreateRangeDescriptor()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in ObjectSupportLib 1.0 and later
 }
function CreateRangeDescriptor( var rangeStart: AEDesc; var rangeStop: AEDesc; disposeInputs: Boolean; var theDescriptor: AEDesc ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );

{$endc} {TARGET_OS_MAC}

end.
