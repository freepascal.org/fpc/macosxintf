{ CoreGraphics - CGPDFArray.h
 * Copyright (c) 2002-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFArray;
interface
uses MacTypes,CGPDFObject,CGBase;
{$ALIGN POWER}


// CGPDFArrayRef defined in CGBase


{ Return the number of items in `array'. }

function CGPDFArrayGetCount( arry: CGPDFArrayRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and return the result in
   `value'. Return true on success; false otherwise. }

function CGPDFArrayGetObject( arry: CGPDFArrayRef; index: size_t; var value: CGPDFObjectRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and, if it's a null, return
   true; otherwise, return false. }

function CGPDFArrayGetNull( arry: CGPDFArrayRef; index: size_t ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and, if it's a boolean, return
   the result in `value'. Return true on success; false otherwise. }

function CGPDFArrayGetBoolean( arry: CGPDFArrayRef; index: size_t; var value: CGPDFBoolean ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and, if it's an integer, return
   the result in `value'. Return true on success; false otherwise. }

function CGPDFArrayGetInteger( arry: CGPDFArrayRef; index: size_t; var value: CGPDFInteger ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and, if it's a number (real or
   integer), return the result in `value'. Return true on success; false
   otherwise. }

function CGPDFArrayGetNumber( arry: CGPDFArrayRef; index: size_t; var value: CGPDFReal ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and, if it's a name, return the
   result in `value'. Return true on success; false otherwise. }

function CGPDFArrayGetName( arry: CGPDFArrayRef; index: size_t; var value: ConstCStringPtr ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and, if it's a string, return
   the result in `value'. Return true on success; false otherwise. }

function CGPDFArrayGetString( arry: CGPDFArrayRef; index: size_t; var value: CGPDFStringRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and, if it's an array, return it
   in `value'. Return true on success; false otherwise. }

function CGPDFArrayGetArray( arry: CGPDFArrayRef; index: size_t; var value: CGPDFArrayRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and, if it's a dictionary,
   return it in `value'. Return true on success; false otherwise. }

function CGPDFArrayGetDictionary( arry: CGPDFArrayRef; index: size_t; var value: CGPDFDictionaryRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object at `index' in `array' and, if it's a stream, return it
   in `value'. Return true on success; false otherwise. }

function CGPDFArrayGetStream( arry: CGPDFArrayRef; index: size_t; var value: CGPDFStreamRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);


end.
