{
 *  AXUIElement.h
 *
 *  Copyright (c) 2002 Apple Computer, Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, 2004 }
{  Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }

unit AXUIElement;
interface
uses MacTypes,CFBase,CFArray,AXErrors,CFRunLoop,CGRemoteOperation,MacOSXPosix;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


function AXAPIEnabled: Boolean;
function AXIsProcessTrusted: Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
// must be called with root privs
function AXMakeProcessTrusted( executablePath: CFStringRef ): AXError;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

type
	AXUIElementRef = ^__AXUIElement; { an opaque type }
	__AXUIElement = record end;

const
	kAXCopyMultipleAttributeOptionStopOnError = $1;
type
	AXCopyMultipleAttributeOptions = UInt32;


function AXUIElementGetTypeID: CFTypeID;

function AXUIElementCopyAttributeNames( element: AXUIElementRef; var names: CFArrayRef ): AXError;
function AXUIElementCopyAttributeValue( element: AXUIElementRef; attribute: CFStringRef; var value: CFTypeRef ): AXError;
function AXUIElementGetAttributeValueCount( element: AXUIElementRef; attribute: CFStringRef; var count: CFIndex ): AXError;
function AXUIElementCopyAttributeValues( element: AXUIElementRef; attribute: CFStringRef; index: CFIndex; maxValues: CFIndex; var values: CFArrayRef ): AXError;
function AXUIElementIsAttributeSettable( element: AXUIElementRef; attribute: CFStringRef; var settable: Boolean ): AXError;
function AXUIElementSetAttributeValue( element: AXUIElementRef; attribute: CFStringRef; value: CFTypeRef ): AXError;
function AXUIElementCopyMultipleAttributeValues( element: AXUIElementRef; attributes: CFArrayRef; options: AXCopyMultipleAttributeOptions; var values: CFArrayRef ): AXError;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function AXUIElementCopyParameterizedAttributeNames( element: AXUIElementRef; var names: CFArrayRef ): AXError;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function AXUIElementCopyParameterizedAttributeValue( element: AXUIElementRef; parameterizedAttribute: CFStringRef; parameter: CFTypeRef; var result: CFTypeRef ): AXError;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function AXUIElementCopyActionNames( element: AXUIElementRef; var names: CFArrayRef ): AXError;
function AXUIElementCopyActionDescription( element: AXUIElementRef; action: CFStringRef; var description: CFStringRef ): AXError;
function AXUIElementPerformAction( element: AXUIElementRef; action: CFStringRef ): AXError;

function AXUIElementCopyElementAtPosition( application: AXUIElementRef; x: Float32; y: Float32; var element: AXUIElementRef ): AXError;

function AXUIElementCreateApplication( pid: pid_t ): AXUIElementRef;
function AXUIElementCreateSystemWide: AXUIElementRef;

function AXUIElementGetPid( element: AXUIElementRef; var pid: pid_t ): AXError;

// pass the SystemWide element (AXUIElementCreateSystemWide) if you want to set the timeout globally for this process
// setting the timeout on another AXUIElementRef sets it only for that ref, not for other AXUIElementRef(s) that are
// equal to it.
// setting timeout to 0 makes this element use the global timeout
function AXUIElementSetMessagingTimeout( element: AXUIElementRef; timeoutInSeconds: Float32 ): AXError;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

// see CGRemoteOperation.h for documentation of parameters
// you can only pass the root or application uielement
function AXUIElementPostKeyboardEvent( application: AXUIElementRef; keyChar: CGCharCode; virtualKey: CGKeyCode; keyDown: Boolean ): AXError;


// Notification APIs
type
	AXObserverRef = ^__AXObserver; { an opaque type }
	__AXObserver = record end;

type
	AXObserverCallback = procedure( observer: AXObserverRef; element: AXUIElementRef; notification: CFStringRef; refcon: univ Ptr );

function AXObserverGetTypeID: CFTypeID;

function AXObserverCreate( application: pid_t; callback: AXObserverCallback; var outObserver: AXObserverRef ): AXError;

function AXObserverAddNotification( observer: AXObserverRef; element: AXUIElementRef; notification: CFStringRef; refcon: univ Ptr ): AXError;
function AXObserverRemoveNotification( observer: AXObserverRef; element: AXUIElementRef; notification: CFStringRef ): AXError;

function AXObserverGetRunLoopSource( observer: AXObserverRef ): CFRunLoopSourceRef;

{$endc} {TARGET_OS_MAC}

end.
