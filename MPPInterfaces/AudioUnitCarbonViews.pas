{!
	@file		AudioUnitCarbonView.h
 	@framework	AudioToolbox.framework
 	@copyright	(c) 2000-2015 Apple, Inc. All rights reserved.
	@abstract	Deprecated interfaces for using Carbon-based views of Audio Units.
}
{	  Pascal Translation:  Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }

unit AudioUnitCarbonViews;
interface
uses MacTypes,MacWindows,HIObject,QuickdrawTypes,AUComponent,Components;
{$ifc TARGET_OS_MAC}
{$ALIGN MAC68K}


{!
	@header		AudioUnitCarbonView
	@abstract	This file defines interfaces for creating and event handling in carbon-based views of Audio Units.
}

{!
	@typedef	AudioUnitCarbonView
	@abstract	An audio unit Carbon view is of type ComponentInstance, defined by the Component Manager.
}
type
	AudioUnitCarbonView = ComponentInstance;

{!
	@enum		Carbon view component types and subtypes
	@constant	kAudioUnitCarbonViewComponentType
					The four char-code type of a carbon-based view component
	@constant	kAUCarbonViewSubType_Generic
					The four char-code subtype of a carbon-based view component
}
const
	kAudioUnitCarbonViewComponentType = FOUR_CHAR_CODE('auvw');
	kAUCarbonViewSubType_Generic = FOUR_CHAR_CODE('gnrc');


{!
	@enum		Carbon view events
	@constant	kAudioUnitCarbonViewEvent_MouseDownInControl
					The event type indicating that the mouse is pressed in a control
	@constant	kAudioUnitCarbonViewEvent_MouseUpInControl
					The event type indicating that the mouse is released in a control
}
const
	kAudioUnitCarbonViewEvent_MouseDownInControl = 0;
	kAudioUnitCarbonViewEvent_MouseUpInControl = 1;

{!
	@typedef	AudioUnitcarbViewEventID
	@abstract	Specifies an event passed to an AudioUnitCarbonViewEventListener callback.
}
type
	AudioUnitCarbonViewEventID = SInt32;

{!
	@typedef	AudioUnitCarbonViewEventListener
	@abstract	Defines a callback function that is called when certain events occur in an
				Audio Unit Carbon view, such as mouse-down and up events on a control.
				
	@param		inUserData
					A user-defined pointer that was passed to an AudioUnitCarbonViewSetEventListener callback.
	@param		inView
					The Audio unit Carbon vIew that generated the message.
	@param		inParameter
					The parameter associated with the control generating the message.
	@param		inEvent
					The type of event, as listed in Audio unit Carbon view events.
	@param		inEventParam
					Further information about the event, dependent on its type.
}
//#ifndef __LP64__
{$ifc not TARGET_CPU_64}
type
	AudioUnitCarbonViewEventListener = procedure( inUserData: univ Ptr; inView: AudioUnitCarbonView; const var inParameter: AudioUnitParameter; inEvent: AudioUnitCarbonViewEventID; inEventParam: {const} univ Ptr );
{$endc}

{!
	@function	AudioUnitCarbonViewCreate
	@abstract	A callback that tells an Audio unit Carbon view to open its user interface (user pane).
	@discussion	The host application specifies the audio unit which the view is to control. The host 
				also provides the window, parent control, and rectangle into which the Audio unit 
				Carbon view component (of type AudioUnitCarbonView) is to create itself.

				The host application is responsible for closing the component (by calling the
				CloseComponent function) before closing its window.
				
	@param		inView
					The view component instance.
	@param		inAudioUnit
					The audio unit component instance which the view is to control.
	@param		inWindow
					The Carbon window in which the user interface is to be opened.
	@param		inParentControl
					The Carbon control into which the user interface is to be embedded. 
					(This is typically the window's root control).
	@param		inLocation
					The host application's requested location for the view. The view should 
					always create itself at the specified location.
	@param		inSize
					The host application's requested size for the view. The view may choose a 
					different size for itself. The actual dimensions of the view are described 
					by the value of the outControl parameter.
	@param		outControl
					The Carbon control which contains the entire user interface for the view.
}
{$ifc not TARGET_CPU_64}
function AudioUnitCarbonViewCreate( inView: AudioUnitCarbonView; inAudioUnit: AudioUnit; inWindow: WindowRef; inParentControl: ControlRef; const inLocation: Float32PointPtr; const inSize: Float32PointPtr; var outControl: ControlRef ): OSStatus;
API_DEPRECATED("no longer supported", macos(10.2, 10.11)) API_UNAVAILABLE(ios, watchos, tvos);

{!
	@function	AudioUnitCarbonViewSetEventListener
	@abstract	Add an event listener to the carbon view.
	@deprecated	in Mac OS X version 10.4
	@discussion	Use the AUEventListener functions in <AudioToolbox/AudioUnitUtilities.h> instead.
	
	@param		inView
					The Carbon view component instance.
	@param		inCallback
					The event listener callback function.
	@param		inUserData
					A user data pointer passed to the callback.
}
function AudioUnitCarbonViewSetEventListener( inView: AudioUnitCarbonView; inCallback: AudioUnitCarbonViewEventListener; inUserData: univ Ptr ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2,__MAC_10_4, __IPHONE_NA, __IPHONE_NA);
{$endc}

{!
	@enum		Selectors for component calls
	@constant	kAudioUnitCarbonViewRange
					Range of selectors
	@constant	kAudioUnitCarbonViewCreateSelect
					Selector for creating the carbon view
	@constant	kAudioUnitCarbonViewSetEventListenerSelect
					Selector for setting the event listener callback
}
const
	kAudioUnitCarbonViewRange = $0300;	// range of selectors
	kAudioUnitCarbonViewCreateSelect = $0301;
	kAudioUnitCarbonViewSetEventListenerSelect = $0302;


{$endc}	{ TARGET_OS_MAC }
end.
