{
 *  CVOpenGLTexture.h
 *  CoreVideo
 *
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }

unit CVOpenGLTexture;
interface
uses MacTypes,CFBase,CVBase,CVReturns,CVImageBuffer,CGLTypes,macgl;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

 
 {! @header CVOpenGLTexture.h
	@copyright 2004 Apple Computer, Inc. All rights reserved.
	@availability Mac OS X 10.4 or later
    @discussion A CoreVideo Texture derives from an ImageBuffer, and is used for supplying source image data to OpenGL.
    		   
}


//#pragma mark CVOpenGLTexture

{!
    @typedef	CVOpenGLTextureRef
    @abstract   OpenGL texture based image buffer

}
type
	CVOpenGLTextureRef = CVImageBufferRef;

function CVOpenGLTextureGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLTextureRetain
    @abstract   Retains a CVOpenGLTexture object
    @discussion Equivalent to CFRetain, but NULL safe
    @param      buffer A CVOpenGLTexture object that you want to retain.
    @result     A CVOpenGLTexture object that is the same as the passed in buffer.
}
function CVOpenGLTextureRetain( texture: CVOpenGLTextureRef ): CVOpenGLTextureRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLTextureRelease
    @abstract   Releases a CVOpenGLTexture object
    @discussion Equivalent to CFRelease, but NULL safe
    @param      buffer A CVOpenGLTexture object that you want to release.
}
procedure CVOpenGLTextureRelease( texture: CVOpenGLTextureRef );
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLTextureGetTarget
    @abstract   Returns the texture target (eg. 2D vs. rect texture extension) of the CVOpenGLTexture
    @param      image Target CVOpenGLTexture
    @result     OpenGL texture target
}
function CVOpenGLTextureGetTarget( image: CVOpenGLTextureRef ): GLenum;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLTextureGetName
    @abstract   Returns the texture target name of the CVOpenGLTexture
    @param      image Target CVOpenGLTexture
    @result     OpenGL texture target name
}
function CVOpenGLTextureGetName( image: CVOpenGLTextureRef ): GLuint;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLTextureIsFlipped
    @abstract   Returns whether the image is flipped vertically or not.
    @param      image Target CVOpenGLTexture
    @result     True if 0,0 in the texture is upper left, false if 0,0 is lower left
}
function CVOpenGLTextureIsFlipped( image: CVOpenGLTextureRef ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLTextureGetCleanTexCoords 
    @abstract   Returns convenient texture coordinates for the part of the image that should be displayed
    @discussion This function automatically takes into account whether or not the texture is flipped.
    @param      image Target CVOpenGLTexture
    @param      lowerLeft  - array of two GLfloats where the s and t texture coordinates of the lower left corner of the image will be stored
    @param      lowerRight - array of two GLfloats where the s and t texture coordinates of the lower right corner of the image will be stored
    @param      upperRight - array of two GLfloats where the s and t texture coordinates of the upper right corner of the image will be stored
    @param      upperLeft  - array of two GLfloats where the s and t texture coordinates of the upper right corner of the image will be stored
}
type
	T2GLfloatArray = array[0..1] of GLfloat;
	T2GLfloatArrayPtr = ^T2GLfloatArray;
procedure CVOpenGLTextureGetCleanTexCoords( image: CVOpenGLTextureRef; lowerLeft : T2GLfloatArrayPtr; lowerRight : T2GLfloatArrayPtr; upperRight : T2GLfloatArrayPtr; upperLeft : T2GLfloatArrayPtr );
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{$endc} // TARGET_OS_MAC

end.
