{
     File:       PMPrintAETypes.h
 
     Contains:   Mac OS X Printing Manager AE definitions.
 
     Version:    Technology: Mac OS X
                 Release:    1.0
 
     Copyright (c) 2003,2008 by Apple Inc. All Rights Reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
}
{    Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
unit PMPrintAETypes;
interface
uses MacTypes,AEDataModel,AERegistry;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


const
	kPMPrintSettingsAEType = FOUR_CHAR_CODE('pset');
	kPMShowPrintDialogAEType = FOUR_CHAR_CODE('pdlg');
	kPMPrinterAEType = FOUR_CHAR_CODE('trpr');

const
	kPMCopiesAEProp = 'copies';
	kPMCopiesAEKey = FOUR_CHAR_CODE('lwcp');
	kPMCopieAEType = typeSInt32;

const
	kPMCollateAEProp = 'collating';
	kPMCollateAEKey = FOUR_CHAR_CODE('lwcl');
	kPMCollateAEType = typeBoolean;

const
	kPMFirstPageAEProp = 'starting page';
	kPMFirstPageAEKey = FOUR_CHAR_CODE('lwfp');
	kPMFirstPageAEType = typeSInt32;

const
	kPMLastPageAEProp = 'ending page';
	kPMLastPageAEKey = FOUR_CHAR_CODE('lwlp');
	kPMLastPageAEType = typeSInt32;

const
	kPMLayoutAcrossAEProp = 'pages across';
	kPMLayoutAcrossAEKey = FOUR_CHAR_CODE('lwla');
	kPMLayoutAcrossAEType = typeSInt32;

const
	kPMLayoutDownAEProp = 'pages down';
	kPMLayoutDownAEKey = FOUR_CHAR_CODE('lwld');
	kPMLayoutDownAEType = typeSInt32;

const
	kPMErrorHandlingAEProp = 'error handling';
	kPMErrorHandlingAEKey = FOUR_CHAR_CODE('lweh');
	kPMErrorHandlingAEType = typeEnumerated;

const
	kPMPrintTimeAEProp = 'requested print time';
	kPMPrintTimeAEKey = FOUR_CHAR_CODE('lwqt');
	kPMPrintTimeAEType = cLongDateTime;

const
	kPMFeatureAEProp = 'printer features';
	kPMFeatureAEKey = FOUR_CHAR_CODE('lwpf');
	kPMFeatureAEType = typeAEList;

const
	kPMFaxNumberAEProp = 'fax number';
	kPMFaxNumberAEKey = FOUR_CHAR_CODE('faxn');
	kPMFaxNumberAEType = typeChar;

const
	kPMTargetPrinterAEProp = 'target printer';
	kPMTargetPrinterAEKey = FOUR_CHAR_CODE('trpr');
	kPMTargetPrinterAEType = typeChar;

{** Enumerations **}

{ For kPMErrorHandlingAEType }
	kPMErrorHandlingStandardEnum = FOUR_CHAR_CODE('lwst');
	kPMErrorHandlingDetailedEnum = FOUR_CHAR_CODE('lwdt');


{$endc} {TARGET_OS_MAC}


end.
