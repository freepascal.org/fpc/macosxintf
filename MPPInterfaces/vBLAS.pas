{
     File:       vecLib/vBLAS.h
 
     Contains:   Header for the Basic Linear Algebra Subprograms, with Apple extensions.
 
     Version:    vecLib-$(vDSP_Version0).$(vDSP_Version1)
 
     Copyright:  � 2000-$(Year) by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit vBLAS;
interface
uses MacTypes,ConditionalMacros;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

{ ==========================================================================================================================}


{
   =================================================================================================
   Definitions of the Basic Linear Algebra Subprograms (BLAS) as provided Apple Computer.
   A few additional functions, unique to Mac OS, have also been provided.
   These are clearly documented as Apple extensions.
   Documentation on the BLAS standard, including reference implementations, can be found on the web
   starting from the BLAS FAQ page at these URLs (verified live as of April 2002):
        http://www.netlib.org/blas/faq.html
        http://www.netlib.org/blas/blast-forum/blast-forum.html
   =================================================================================================
}


{
   =================================================================================================
   Matrix shape and storage
   ========================
   Keeping the various matrix shape and storage parameters straight can be difficult.  The BLAS
   documentation generally makes a distinction between the concpetual "matrix" and the physical
   "array".  However there are a number of places where this becomes fuzzy because of the overall
   bias towards FORTRAN's column major storage.  The confusion is made worse by style differences
   between the level 2 and level 3 functions.  It is amplified further by the explicit choice of row
   or column major storage in the C interface.
   The storage order does not affect the actual computation that is performed.  That is, it does not
   affect the results other than where they appear in memory.  It does affect the values passed
   for so-called "leading dimension" parameters, such as lda in sgemv.  These are always the major
   stride in storage, allowing operations on rectangular subsets of larger matrices.  For row major
   storage this is the number of columns in the parent matrix, and for column major storage this is
   the number of rows in the parent matrix.
   For the level 2 functions, which deal with only a single matrix, the matrix shape parameters are
   always M and N.  These are the logical shape of the matrix, M rows by N columns.  The transpose
   parameter, such as transA in sgemv, defines whether the regular matrix or its transpose is used
   in the operation.  This affects the implicit length of the input and output vectors.  For example,
   if the regular matrix A is used in sgemv, the input vector X has length N, the number of columns
   of A, and the output vector Y has length M, the number of rows of A.  The length of the input and
   output vectors is not affected by the storage order of the matrix.
   The level 3 functions deal with 2 input matrices and one output matrix, the matrix shape parameters
   are M, N, and K.  The logical shape of the output matrix is always M by N, while K is the common
   dimension of the input matrices.  Like level 2, the transpose parameters, such as transA and transB
   in sgemm, define whether the regular input or its transpose is used in the operation.  However
   unlike level 2, in level 3 the transpose parameters affect the implicit shape of the input matrix.
   Consider sgemm, which computes "C = (alpha * A * B) + (beta * C)", where A and B might be regular
   or transposed.  The logical shape of C is always M rows by N columns.  The physical shape depends
   on the storage order parameter.  Using column major storage the declaration of C (the array) in C
   (the language) would be something like "float C[N][M]".  The logical shape of A without transposition
   is M by K, and B is K by N.  The one storage order parameter affects all three matrices.
   For those readers still wondering about the style differences between level 2 and level 3, they
   involve whether the input or output shapes are explicit.  For level 2, the input matrix shape is
   always M by N.  The input and output vector lengths are implicit and vary according to the
   transpose parameter.  For level 3, the output matrix shape is always M by N.  The input matrix
   shapes are implicit and vary according to the transpose parameters.
   =================================================================================================
}


{ ==========================================================================================================================}


{
   ------------------------------------------------------------------------------------------------------------------
   IsAlignedCount   - True if an integer is positive and a multiple of 4.  Negative strides are considered unaligned.
   IsAlignedAddr    - True if an address is a multiple of 16.
}

// #define IsAlignedCount(n)   ( (n > 0) && ((n & 3) == 0) )
// #define IsAlignedAddr(a)    ( ((long)a & 15L) == 0 )

{
   =================================================================================================
   Prototypes for FORTRAN BLAS
   ===========================
   These are prototypes for the FORTRAN callable BLAS functions.  They are implemented in C for
   Mac OS, as thin shims that simply call the C BLAS counterpart.  These routines should never be
   called from C, but need to be included here so they will get output for the stub library.  It
   won't hurt to call them from C, but who would want to since you can't pass literals for sizes?
   FORTRAN compilers are typically MPW tools and use PPCLink, so they will link with the official
   vecLib stub from Apple.
   =================================================================================================
}
{
 *  SDOT()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
function SDOT( const var N: SInt32; {const} X: Float32Ptr; const var incX: SInt32; {const} Y: Float32Ptr; const var incY: SInt32 ): Float32;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SNRM2()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
function SNRM2( const var N: SInt32; {const} X: Float32Ptr; const var incX: SInt32 ): Float32;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SASUM()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
function SASUM( const var N: SInt32; {const} X: Float32Ptr; const var incX: SInt32 ): Float32;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ISAMAX()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
function ISAMAX( const var N: SInt32; {const} X: Float32Ptr; const var incX: SInt32 ): SInt32;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SSWAP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
procedure SSWAP( const var N: SInt32; X: Float32Ptr; const var incX: SInt32; Y: Float32Ptr; const var incY: SInt32 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SCOPY()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
procedure SCOPY( const var N: SInt32; {const} X: Float32Ptr; const var incX: SInt32; Y: Float32Ptr; const var incY: SInt32 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SAXPY()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
procedure SAXPY( const var N: SInt32; const var alpha: Float32; {const} X: Float32Ptr; const var incX: SInt32; Y: Float32Ptr; const var incY: SInt32 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SROT()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
procedure SROT( const var N: SInt32; X: Float32Ptr; const var incX: SInt32; Y: Float32Ptr; const var incY: SInt32; const var c: Float32; const var s: Float32 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SSCAL()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
procedure SSCAL( const var N: SInt32; const var alpha: Float32; X: Float32Ptr; const var incX: SInt32 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SGEMV()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
procedure SGEMV( transA: ConstCStringPtr; const var M: SInt32; const var N: SInt32; const var alpha: Float32; {const} A: Float32Ptr; const var lda: SInt32; {const} X: Float32Ptr; const var incX: SInt32; const var beta: Float32; Y: Float32Ptr; const var incY: SInt32 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SGEMM()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in vecLib.framework
 *    CarbonLib:        not in Carbon, but vecLib is compatible with CarbonLib
 *    Non-Carbon CFM:   in vecLib 1.0.2 and later
 }
procedure SGEMM( transA: ConstCStringPtr; transB: ConstCStringPtr; const var M: SInt32; const var N: SInt32; const var K: SInt32; const var alpha: Float32; {const} A: Float32Ptr; const var lda: SInt32; {const} B: Float32Ptr; const var ldb: SInt32; const var beta: Float32; C: Float32Ptr; const var ldc: SInt32 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ ==========================================================================================================================}
{ ==========================================================================================================================}

{$endc} {TARGET_OS_MAC}


end.
