{------------------------------------------------------------------------------------------------------------------------------
 *
 *  ImageCapture/ICACamera.h
 *
 *  Copyright (c) 2000-2006 Apple Computer, Inc. All rights reserved.
 *
 *  For bug reports, consult the following page onthe World Wide Web:
 *  http://developer.apple.com/bugreporter/
 *
 *----------------------------------------------------------------------------------------------------------------------------}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ICACamera;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

//------------------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------------------
{!
    @header ICACamera.h
    @discussion
        ICACamera.h defines digital still cameras specific constants used with the Image Capture framework APIs. 
}

//-------------------------------------------------------------------------------------------------------- Constant Descriptions

{! 
    @enum Fields in StorageInfo Dataset 
    @discussion
        Refer to section 5.5.3 of the PIMA 15740 (PTP) specification for descriptions and usage notes.
    @constant kICAPropertyCameraStorageType
        Storage type. <BR>Data type: UInt16.
    @constant kICAPropertyCameraFilesystemType
        File system type. <BR>Data type: UInt16.
    @constant kICAPropertyCameraAccessCapability
        Access capability. <BR>Data type: UInt16.
    @constant kICAPropertyCameraMaxCapacity
        Total storage capacity in bytes. <BR>Data type: UInt64.
    @constant kICAPropertyCameraFreeSpaceInBytes
        Free space available on storage in bytes. <BR>Data type: UInt64.
    @constant kICAPropertyCameraFreeSpaceInImages
        Number of images that may still be captured in to this store based on the current image capture settings on the camera. <BR>Data type: UInt32.
    @constant kICAPropertyCameraStorageDescription
        Storage description. <BR>Data type: null terminated string.
    @constant kICAPropertyCameraVolumeLabel
        Volume label. <BR>Data type: null terminated string.
  }
const
	kICAPropertyCameraStorageType = FOUR_CHAR_CODE('stor');
	kICAPropertyCameraFilesystemType = FOUR_CHAR_CODE('fsys');
	kICAPropertyCameraAccessCapability = FOUR_CHAR_CODE('acap');
	kICAPropertyCameraMaxCapacity = FOUR_CHAR_CODE('maxc');
	kICAPropertyCameraFreeSpaceInBytes = FOUR_CHAR_CODE('fres');
	kICAPropertyCameraFreeSpaceInImages = FOUR_CHAR_CODE('frei');
	kICAPropertyCameraStorageDescription = FOUR_CHAR_CODE('stod');
	kICAPropertyCameraVolumeLabel = FOUR_CHAR_CODE('voll');

{!
    @enum Values for kICAPropertyCameraStorageType 
    @discussion
        Values for kICAPropertyCameraStorageType.
    @constant kICAStorageUndefined
        Undefined.
    @constant kICAStorageFixedROM
        Fixed ROM.
    @constant kICAStorageRemovableROM
        Removable ROM.
    @constant kICAStorageFixedRAM
        Fixed RAM.
    @constant kICAStorageRemovableRAM
        Removable RAM.
  }
const
	kICAStorageUndefined = $0000;
	kICAStorageFixedROM = $0001;
	kICAStorageRemovableROM = $0002;
	kICAStorageFixedRAM = $0003;
	kICAStorageRemovableRAM = $0004;

{!
    @enum Values for kICAPropertyCameraFilesystemType 
    @discussion
        Values for kICAPropertyCameraFilesystemType.
    @constant kICAFileystemUndefined
        Undefined.
    @constant kICAFileystemGenericFlat
        Generic flat.
    @constant kICAFileystemGenericHierarchical
        Generic hierarchical.
    @constant kICAFileystemDCF
        DCF-conformant.
  }
const
	kICAFileystemUndefined = $0000;
	kICAFileystemGenericFlat = $0001;
	kICAFileystemGenericHierarchical = $0002;
	kICAFileystemDCF = $0003;

{! 
    @enum Values for kICAPropertyCameraAccessCapability 
    @discussion
        Values for kICAPropertyCameraAccessCapability.
    @constant kICAAccessReadWrite
        Read-write.
    @constant kICAAccessReadOnly
        Read-only without object deletion.
    @constant kICAAccessReadOnlyWithObjectDeletion
        Read-only with object deletion.
  }
const
	kICAAccessReadWrite = $0000;
	kICAAccessReadOnly = $0001;
	kICAAccessReadOnlyWithObjectDeletion = $0002;

{!
    @enum Standard camera properties
    @discussion
        Refer to section 13 of the PIMA 15740 (PTP) specification for descriptions and usage notes for these standard properties.
    @constant kICAPropertyCameraBatteryLevel
        Battery level. <BR>Property data type: UInt8; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraFunctionalMode
        Functional mode. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraImageSize
        Image size. <BR>Property data type: CFString; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraCompressionSetting
        Compression setting. <BR>Property data type: UInt8; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraWhiteBalance
        White balance. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraRGBGain
        RGB gain. <BR>Property data type: null terminated string; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraFNumber
        F-number. <BR>Property data type: UInt8; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraFocalLength
        Focal length. <BR>Property data type: UInt32; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraFocusDistance
        Focus distance. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraFocusMode
        Focus mode. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraExposureMeteringMode
        Exposure Metering mode. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraFlashMode
        Flash mode. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraExposureTime
        Exposure time. <BR>Property data type: UInt32; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraExposureProgramMode
        Exposure program mode. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraExposureIndex
        Exposure index. <BR>Property data type: UInt16; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraExposureBiasCompensation
        Exposure bias compensation. <BR>Property data type: UInt16; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraDateTime
        Date & time. <BR>Property data type: null terminated string; Property desc forms: none.
    @constant kICAPropertyCameraCaptureDelay
        Capture delay. <BR>Property data type: UInt32; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraStillCaptureMode
        Still capture mode. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraContrast
        Contrast. <BR>Property data type: UInt8; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraSharpness
        Sharpness. <BR>Property data type: UInt8; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraDigitalZoom
        Digital zoom. <BR>Property data type: UInt8; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraEffectMode
        Effect mode. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraBurstNumber
        Burst number. <BR>Property data type: UInt16; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraBurstInterval
        Burst interval. <BR>Property data type: UInt16; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraTimelapseNumber
        Timelapse number. <BR>Property data type: UInt16; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraTimelapseInterval
        Timelapse interval. <BR>Property data type: UInt32; Property desc forms: Enum/Range.
    @constant kICAPropertyCameraFocusMeteringMode
        Focus metering mode. <BR>Property data type: UInt16; Property desc forms: Enum.
    @constant kICAPropertyCameraUploadURL
        Upload URL. <BR>Property data type: null terminated string; Property desc forms: none.
    @constant kICAPropertyCameraArtist
        Artist. <BR>Property data type: null terminated string; Property desc forms: none.
    @constant kICAPropertyCameraCopyrightInfo
        Copyright info. <BR>Property data type: null terminated string; Property desc forms: none.
  }
const
	kICAPropertyCameraBatteryLevel = FOUR_CHAR_CODE('5001');
	kICAPropertyCameraFunctionalMode = FOUR_CHAR_CODE('5002');
	kICAPropertyCameraImageSize = FOUR_CHAR_CODE('5003');
	kICAPropertyCameraCompressionSetting = FOUR_CHAR_CODE('5004');
	kICAPropertyCameraWhiteBalance = FOUR_CHAR_CODE('5005');
	kICAPropertyCameraRGBGain = FOUR_CHAR_CODE('5006');
	kICAPropertyCameraFNumber = FOUR_CHAR_CODE('5007');
	kICAPropertyCameraFocalLength = FOUR_CHAR_CODE('5008');
	kICAPropertyCameraFocusDistance = FOUR_CHAR_CODE('5009');
	kICAPropertyCameraFocusMode = FOUR_CHAR_CODE('500A');
	kICAPropertyCameraExposureMeteringMode = FOUR_CHAR_CODE('500B');
	kICAPropertyCameraFlashMode = FOUR_CHAR_CODE('500C');
	kICAPropertyCameraExposureTime = FOUR_CHAR_CODE('500D');
	kICAPropertyCameraExposureProgramMode = FOUR_CHAR_CODE('500E');
	kICAPropertyCameraExposureIndex = FOUR_CHAR_CODE('500F');
	kICAPropertyCameraExposureBiasCompensation = FOUR_CHAR_CODE('5010');
	kICAPropertyCameraDateTime = FOUR_CHAR_CODE('5011');
	kICAPropertyCameraCaptureDelay = FOUR_CHAR_CODE('5012');
	kICAPropertyCameraStillCaptureMode = FOUR_CHAR_CODE('5013');
	kICAPropertyCameraContrast = FOUR_CHAR_CODE('5014');
	kICAPropertyCameraSharpness = FOUR_CHAR_CODE('5015');
	kICAPropertyCameraDigitalZoom = FOUR_CHAR_CODE('5016');
	kICAPropertyCameraEffectMode = FOUR_CHAR_CODE('5017');
	kICAPropertyCameraBurstNumber = FOUR_CHAR_CODE('5018');
	kICAPropertyCameraBurstInterval = FOUR_CHAR_CODE('5019');
	kICAPropertyCameraTimelapseNumber = FOUR_CHAR_CODE('501A');
	kICAPropertyCameraTimelapseInterval = FOUR_CHAR_CODE('501B');
	kICAPropertyCameraFocusMeteringMode = FOUR_CHAR_CODE('501C');
	kICAPropertyCameraUploadURL = FOUR_CHAR_CODE('501D');
	kICAPropertyCameraArtist = FOUR_CHAR_CODE('501E');
	kICAPropertyCameraCopyrightInfo = FOUR_CHAR_CODE('501F');

{!
    @enum ImageCapture framework specific camera properties 
    @discussion
        ImageCapture framework specific camera properties.
    @constant kICAPropertyCameraIcon
        Camera icon in ICAThumbnail format.
    @constant kICAPropertyCameraSupportedMessages
        Messages supported/understood by the camera.
  }
const
	kICAPropertyCameraIcon = FOUR_CHAR_CODE('icon');
	kICAPropertyCameraSupportedMessages = FOUR_CHAR_CODE('msgs');

{!
    @enum Camera messages 
    @discussion
        Messages that can be sent to digital still cameras.
    @constant kICAMessageCameraCaptureNewImage
        Capture a new image using the camera.
    @constant kICAMessageCameraDeleteOne
        Delete one image stored in the camera.
    @constant kICAMessageCameraDeleteAll
        Delete all images stored in the camera.
    @constant kICAMessageCameraSyncClock
        Synchronize camera's clock with the computer's clock.
    @constant kICAMessageCameraUploadData
        Upload data to the camera.
  }
const
	kICAMessageCameraCaptureNewImage = FOUR_CHAR_CODE('ccni');
	kICAMessageCameraDeleteOne = FOUR_CHAR_CODE('del1');
	kICAMessageCameraDeleteAll = FOUR_CHAR_CODE('dela');
	kICAMessageCameraSyncClock = FOUR_CHAR_CODE('sclk');
	kICAMessageCameraUploadData = FOUR_CHAR_CODE('load');

{!
    @enum Camera capabilities 
    @discussion
        Capabilities of digital still cameras.
    @constant kICAMessageCameraCaptureNewImage
        Can capture a new image using the camera.
    @constant kICAMessageCameraDeleteOne
        Can delete one image stored in the camera.
    @constant kICAMessageCameraDeleteAll
        Can delete all images stored in the camera.
    @constant kICAMessageCameraSyncClock
        Can synchronize camera's clock with the computer's clock.
    @constant kICAMessageCameraUploadData
        Can upload data to the camera.
  }
const
	kICACapabilityCanCameraCaptureNewImage = FOUR_CHAR_CODE('ccni');
	kICACapabilityCanCameraDeleteOne = FOUR_CHAR_CODE('del1');
	kICACapabilityCanCameraDeleteAll = FOUR_CHAR_CODE('dela');
	kICACapabilityCanCameraSyncClock = FOUR_CHAR_CODE('sclk');
	kICACapabilityCanCameraUploadData = FOUR_CHAR_CODE('load');
	kICACapabilityMayStoreNewImagesInTempStore = FOUR_CHAR_CODE('temp');

//------------------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------------------

{$endc} {TARGET_OS_MAC}

end.
