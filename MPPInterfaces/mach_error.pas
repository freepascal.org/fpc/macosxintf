{
 * Copyright (c) 2000 Apple Computer, Inc. All rights reserved.
 *
 * @APPLE_OSREFERENCE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. The rights granted to you under the License
 * may not be used to create, or enable the creation or redistribution of,
 * unlawful or unlicensed copies of an Apple operating system, or to
 * circumvent, violate, or enable the circumvention or violation of, any
 * terms of an Apple operating system software license agreement.
 * 
 * Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_OSREFERENCE_LICENSE_HEADER_END@
 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, September 2010 }
unit mach_error;
interface
uses MacTypes,kern_return;

{
 * @OSF_COPYRIGHT@
 }
{ 
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 * 
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 * 
 * Carnegie Mellon requests users of this software to return to
 * 
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 * 
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 }
{
 * File:	mach/error.h
 * Purpose:
 *	error module definitions
 *
 }

type
	mach_error_t = kern_return_t;
	mach_error_fn_t = function: mach_error_t;

{
 *	error number layout as follows:
 *
 *	hi		 		       lo
 *	| system(6) | subsystem(12) | code(14) |
 }


const
  err_none	=	mach_error_t(0);
  ERR_SUCCESS	=	mach_error_t(0);
  ERR_ROUTINE_NIL	= nil;

  system_emask = (((($3f) and $3f) shl 26));
  sub_emask = (((($fff) and $fff) shl 14));
	code_emask = $3fff;


{	major error systems	}
  err_kern = ((($0) and $3f) shl 26);		{ kernel }
  err_us = ((($1) and $3f) shl 26);		{ user space library }
  err_server = ((($2) and $3f) shl 26);		{ user space servers }
  err_ipc = ((($3) and $3f) shl 26);		{ old ipc errors }
  err_mach_ipc = ((($4) and $3f) shl 26);		{ mach-ipc errors }
  err_dipc = ((($7) and $3f) shl 26);		{ distributed ipc }
  err_local = ((($3e) and $3f) shl 26);	{ user defined errors }
  err_ipc_compat = ((($3f) and $3f) shl 26);	{ (compatibility) mach-ipc errors }

const
	err_max_system = $3f;



{MW-GPC-ONLY-START}
{$mwgpcdefinec err_system( x ) (((x) and $3f) shl 26) }
{$mwgpcdefinec err_sub( x ) (((x) and $fff) shl 14) }

{$mwgpcdefinec err_get_system( err ) (((err) shr 26) and $3f) }
{$mwgpcdefinec err_get_sub( err ) (((err) shr 14) and $fff) }
{$mwgpcdefinec err_get_code( err ) ((err) and $3fff) }
  {	unix errors get lumped into one subsystem  }
{$mwgpcdefinec unix_err(errno) (err_kern or (((3) and $fff) shl 14) or errno) }
{MW-GPC-ONLY-FINISH}

{FPC-ONLY-START}
implemented function err_system(x: mach_error_t): mach_error_t; inline;
implemented function err_sub(x: mach_error_t): mach_error_t; inline;

implemented function err_get_system(err: mach_error_t): mach_error_t; inline;
implemented function err_get_sub(err: mach_error_t): mach_error_t; inline;
implemented function err_get_code(err: mach_error_t): mach_error_t; inline;

  {	unix errors get lumped into one subsystem  }
implemented function unix_err(errno: SInt32): mach_error_t; inline;
{FPC-ONLY-FINISH}

implementation

{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

{FPC-ONLY-START}
{$push}
{$R-,Q-}

function err_system(x: mach_error_t): mach_error_t; inline;
begin
  err_system:=(((x) and $3f) shl 26)
end;

function err_sub(x: mach_error_t): mach_error_t; inline;
begin
  err_sub:=(((x) shr 14) and $fff)
end;


function err_get_system(err: mach_error_t): mach_error_t; inline;
begin
  err_get_system:=(((err) shr 26) and $3f)
end;

function err_get_sub(err: mach_error_t): mach_error_t; inline;
begin
  err_get_sub:=(((err) shr 14) and $fff)
end;

function err_get_code(err: mach_error_t): mach_error_t; inline;
begin
  err_get_code:=((err) and $3fff)
end;


function unix_err(errno: SInt32): mach_error_t; inline;
begin
  unix_err:=err_kern or (((3) and $fff) shl 14) or errno;
end;

{$pop}
{FPC-ONLY-FINISH}

end.
