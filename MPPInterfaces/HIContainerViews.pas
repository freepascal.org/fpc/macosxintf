{
     File:       HIToolbox/HIContainerViews.h
 
     Contains:   Definition of the container views provided by HIToolbox.
 
     Version:    HIToolbox-624~3
 
     Copyright:  © 2006-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HIContainerViews;
interface
uses MacTypes,Appearance,CarbonEvents,Controls,Menus,QuickdrawTypes,CFBase,HIObject;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
 *  HIContainerViews.h
 *  
 *  Discussion:
 *    API definitions for the views that can contain other views: group
 *    box, placard, window header, and user pane.
 }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ GROUP BOX (CDEF 10)                                                               }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  The group box CDEF can be use in several ways. It can have no title, a text title,  }
{  a check box as the title, or a popup button as a title. There are two versions of   }
{  group boxes, primary and secondary, which look slightly different.                  }
{ Group Box proc IDs }
const
	kControlGroupBoxTextTitleProc = 160;
	kControlGroupBoxCheckBoxProc = 161;
	kControlGroupBoxPopupButtonProc = 162;
	kControlGroupBoxSecondaryTextTitleProc = 164;
	kControlGroupBoxSecondaryCheckBoxProc = 165;
	kControlGroupBoxSecondaryPopupButtonProc = 166;

{ Control Kind Tag }
const
	kControlKindGroupBox = FOUR_CHAR_CODE('grpb');
	kControlKindCheckGroupBox = FOUR_CHAR_CODE('cgrp');
	kControlKindPopupGroupBox = FOUR_CHAR_CODE('pgrp');

{ The HIObject class ID for the HIGroupBox class. }
const kHIGroupBoxClassID = CFSTR( 'com.apple.HIGroupBox' );
{ The HIObject class ID for the HICheckBoxGroup class. }
const kHICheckBoxGroupClassID = CFSTR( 'com.apple.HICheckBoxGroup' );
{ Creation APIs: Carbon only }
{$ifc not TARGET_CPU_64}
{
 *  CreateGroupBoxControl()
 *  
 *  Summary:
 *    Creates a group box control.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    window:
 *      The window that should contain the control.
 *    
 *    boundsRect:
 *      The bounding box of the control.
 *    
 *    title:
 *      The title of the control.
 *    
 *    primary:
 *      Whether to create a primary or secondary group box.
 *    
 *    outControl:
 *      On exit, contains the new control.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateGroupBoxControl( window: WindowRef; const var boundsRect: Rect; title: CFStringRef; primary: Boolean; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CreateCheckGroupBoxControl()
 *  
 *  Summary:
 *    Creates a group box control that has a check box as its title.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    window:
 *      The window that should contain the control.
 *    
 *    boundsRect:
 *      The bounding box of the control.
 *    
 *    title:
 *      The title of the control (used as the title of the check box).
 *    
 *    initialValue:
 *      The initial value of the check box.
 *    
 *    primary:
 *      Whether to create a primary or secondary group box.
 *    
 *    autoToggle:
 *      Whether to create an auto-toggling check box. Auto-toggling
 *      check box titles are only supported on Mac OS X; this parameter
 *      must be false when used with CarbonLib.
 *    
 *    outControl:
 *      On exit, contains the new control.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateCheckGroupBoxControl( window: WindowRef; const var boundsRect: Rect; title: CFStringRef; initialValue: SInt32; primary: Boolean; autoToggle: Boolean; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CreatePopupGroupBoxControl()
 *  
 *  Summary:
 *    Creates a group box control that has a popup button as its title.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    window:
 *      The window that should contain the control.
 *    
 *    boundsRect:
 *      The bounding box of the control.
 *    
 *    title:
 *      The title of the control (used as the title of the popup
 *      button).
 *    
 *    primary:
 *      Whether to create a primary or secondary group box.
 *    
 *    menuID:
 *      The menu ID of the menu to be displayed by the popup button.
 *    
 *    variableWidth:
 *      Whether the popup button should have a variable-width title.
 *      Fixed-width titles are only supported by Mac OS X; this
 *      parameter must be true when used with CarbonLib.
 *    
 *    titleWidth:
 *      The width in pixels of the popup button title.
 *    
 *    titleJustification:
 *      The justification of the popup button title. Use one of the
 *      TextEdit justification constants here (teFlushDefault,
 *      teCenter, teFlushRight, or teFlushLeft).
 *    
 *    titleStyle:
 *      The QuickDraw text style of the popup button title.
 *    
 *    outControl:
 *      On exit, contains the new control.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreatePopupGroupBoxControl( window: WindowRef; const var boundsRect: Rect; title: CFStringRef; primary: Boolean; menuID_: MenuID; variableWidth: Boolean; titleWidth: SInt16; titleJustification: SInt16; titleStyle: Style; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Tagged data supported by group box }
{$endc} {not TARGET_CPU_64}

const
	kControlGroupBoxMenuHandleTag = FOUR_CHAR_CODE('mhan'); { MenuRef (popup title only)}
	kControlGroupBoxMenuRefTag = FOUR_CHAR_CODE('mhan'); { MenuRef (popup title only)}
	kControlGroupBoxFontStyleTag = kControlFontStyleTag; { ControlFontStyleRec}

{ tags available with Appearance 1.1 or later }
const
	kControlGroupBoxTitleRectTag = FOUR_CHAR_CODE('trec'); { Rect. Rectangle that the title text/control is drawn in. (get only)}


{
 *  Summary:
 *    Tags available with Mac OS X 10.3 or later
 }
const
{
   * Passed data is a Rect.  Returns the full rectangle that content is
   * drawn in (get only). This is slightly different than the content
   * region, as it also includes the frame drawn around the content.
   }
	kControlGroupBoxFrameRectTag = FOUR_CHAR_CODE('frec');


{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ PLACARD (CDEF 14)                                                                 }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{ Placard proc IDs }
const
	kControlPlacardProc = 224;

{ Control Kind Tag }
const
	kControlKindPlacard = FOUR_CHAR_CODE('plac');

{ The HIObject class ID for the HIPlacardView class. }
const kHIPlacardViewClassID = CFSTR( 'com.apple.HIPlacardView' );
{$ifc not TARGET_CPU_64}
{
 *  CreatePlacardControl()
 *  
 *  Summary:
 *    Creates a placard control.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    window:
 *      The window that should contain the control. May be NULL on 10.3
 *      and later.
 *    
 *    boundsRect:
 *      The bounding box of the control.
 *    
 *    outControl:
 *      On exit, contains the new control.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreatePlacardControl( window: WindowRef { can be NULL }; const var boundsRect: Rect; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ WINDOW HEADER (CDEF 21)                                                           }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{ Window Header proc IDs }
{$endc} {not TARGET_CPU_64}

const
	kControlWindowHeaderProc = 336;  { normal header}
	kControlWindowListViewHeaderProc = 337; { variant for list views - no bottom line}

{ Control Kind Tag }
const
	kControlKindWindowHeader = FOUR_CHAR_CODE('whed');


{
 *  Summary:
 *    Tags available with Mac OS X 10.3 or later
 }
const
{
   * Passed data is a Boolean.  Set to true if the control is to draw
   * as a list header.
   }
	kControlWindowHeaderIsListHeaderTag = FOUR_CHAR_CODE('islh');

{ The HIObject class ID for the HIWindowHeaderView class. }
const kHIWindowHeaderViewClassID = CFSTR( 'com.apple.HIWindowHeaderView' );
{ Creation API: Carbon Only }
{$ifc not TARGET_CPU_64}
{
 *  CreateWindowHeaderControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateWindowHeaderControl( window: WindowRef; const var boundsRect: Rect; isListHeader: Boolean; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ USER PANE (CDEF 16)                                                               }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  User panes have two primary purposes: to allow easy implementation of a custom      }
{  control by the developer, and to provide a generic container for embedding other    }
{  controls.                                                                           }
{  In Carbon, with the advent of Carbon-event-based controls, you may find it easier   }
{  to simply write a new control from scratch than to customize a user pane control.   }
{  The set of callbacks provided by the user pane will not be extended to support      }
{  new Control Manager features; instead, you should just write a real control.        }
{  User panes do not, by default, support embedding. If you try to embed a control     }
{  into a user pane, you will get back errControlIsNotEmbedder. You can make a user    }
{  pane support embedding by passing the kControlSupportsEmbedding flag in the 'value' }
{  parameter when you create the control.                                              }
{  User panes support the following overloaded control initialization options:         }
{  Parameter                   What Goes Here                                          }
{  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ         ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ      }
{  Value                       Control feature flags                                   }

{ User Pane proc IDs }
{$endc} {not TARGET_CPU_64}

const
	kControlUserPaneProc = 256;

{ Control Kind Tag }
const
	kControlKindUserPane = FOUR_CHAR_CODE('upan');

{ The HIObject class ID for the HIUserPane class. Valid in Mac OS X 10.4 and later. }
const kHIUserPaneClassID = CFSTR( 'com.apple.HIUserPane' );
{ Creation API: Carbon only }
{$ifc not TARGET_CPU_64}
{
 *  CreateUserPaneControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateUserPaneControl( window: WindowRef; const var boundsRect: Rect; features: UInt32; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Tagged data supported by user panes }
{ Currently, they are all proc ptrs for doing things like drawing and hit testing, etc. }
{$endc} {not TARGET_CPU_64}

const
	kControlUserItemDrawProcTag = FOUR_CHAR_CODE('uidp'); { UserItemUPP}
	kControlUserPaneDrawProcTag = FOUR_CHAR_CODE('draw'); { ControlUserPaneDrawUPP}
	kControlUserPaneHitTestProcTag = FOUR_CHAR_CODE('hitt'); { ControlUserPaneHitTestUPP}
	kControlUserPaneTrackingProcTag = FOUR_CHAR_CODE('trak'); { ControlUserPaneTrackingUPP}
	kControlUserPaneIdleProcTag = FOUR_CHAR_CODE('idle'); { ControlUserPaneIdleUPP}
	kControlUserPaneKeyDownProcTag = FOUR_CHAR_CODE('keyd'); { ControlUserPaneKeyDownUPP}
	kControlUserPaneActivateProcTag = FOUR_CHAR_CODE('acti'); { ControlUserPaneActivateUPP}
	kControlUserPaneFocusProcTag = FOUR_CHAR_CODE('foci'); { ControlUserPaneFocusUPP}
	kControlUserPaneBackgroundProcTag = FOUR_CHAR_CODE('back'); { ControlUserPaneBackgroundUPP (32-bit only)}

type
	ControlUserPaneDrawProcPtr = procedure( control: ControlRef; part: ControlPartCode );
	ControlUserPaneHitTestProcPtr = function( control: ControlRef; where: Point ): ControlPartCode;
	ControlUserPaneTrackingProcPtr = function( control: ControlRef; startPt: Point; actionProc: ControlActionUPP ): ControlPartCode;
	ControlUserPaneIdleProcPtr = procedure( control: ControlRef );
	ControlUserPaneKeyDownProcPtr = function( control: ControlRef; keyCode: SInt16; charCode: SInt16; modifiers: SInt16 ): ControlPartCode;
	ControlUserPaneActivateProcPtr = procedure( control: ControlRef; activating: Boolean );
	ControlUserPaneFocusProcPtr = function( control: ControlRef; action: ControlFocusPart ): ControlPartCode;
{GPC-ONLY-START}
	ControlUserPaneDrawUPP = UniversalProcPtr; // should be ControlUserPaneDrawProcPtr
{GPC-ONLY-ELSE}
	ControlUserPaneDrawUPP = ControlUserPaneDrawProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	ControlUserPaneHitTestUPP = UniversalProcPtr; // should be ControlUserPaneHitTestProcPtr
{GPC-ONLY-ELSE}
	ControlUserPaneHitTestUPP = ControlUserPaneHitTestProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	ControlUserPaneTrackingUPP = UniversalProcPtr; // should be ControlUserPaneTrackingProcPtr
{GPC-ONLY-ELSE}
	ControlUserPaneTrackingUPP = ControlUserPaneTrackingProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	ControlUserPaneIdleUPP = UniversalProcPtr; // should be ControlUserPaneIdleProcPtr
{GPC-ONLY-ELSE}
	ControlUserPaneIdleUPP = ControlUserPaneIdleProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	ControlUserPaneKeyDownUPP = UniversalProcPtr; // should be ControlUserPaneKeyDownProcPtr
{GPC-ONLY-ELSE}
	ControlUserPaneKeyDownUPP = ControlUserPaneKeyDownProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	ControlUserPaneActivateUPP = UniversalProcPtr; // should be ControlUserPaneActivateProcPtr
{GPC-ONLY-ELSE}
	ControlUserPaneActivateUPP = ControlUserPaneActivateProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	ControlUserPaneFocusUPP = UniversalProcPtr; // should be ControlUserPaneFocusProcPtr
{GPC-ONLY-ELSE}
	ControlUserPaneFocusUPP = ControlUserPaneFocusProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewControlUserPaneDrawUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewControlUserPaneDrawUPP( userRoutine: ControlUserPaneDrawProcPtr ): ControlUserPaneDrawUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewControlUserPaneHitTestUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewControlUserPaneHitTestUPP( userRoutine: ControlUserPaneHitTestProcPtr ): ControlUserPaneHitTestUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewControlUserPaneTrackingUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewControlUserPaneTrackingUPP( userRoutine: ControlUserPaneTrackingProcPtr ): ControlUserPaneTrackingUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewControlUserPaneIdleUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewControlUserPaneIdleUPP( userRoutine: ControlUserPaneIdleProcPtr ): ControlUserPaneIdleUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewControlUserPaneKeyDownUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewControlUserPaneKeyDownUPP( userRoutine: ControlUserPaneKeyDownProcPtr ): ControlUserPaneKeyDownUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewControlUserPaneActivateUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewControlUserPaneActivateUPP( userRoutine: ControlUserPaneActivateProcPtr ): ControlUserPaneActivateUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewControlUserPaneFocusUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewControlUserPaneFocusUPP( userRoutine: ControlUserPaneFocusProcPtr ): ControlUserPaneFocusUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeControlUserPaneDrawUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeControlUserPaneDrawUPP( userUPP: ControlUserPaneDrawUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeControlUserPaneHitTestUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeControlUserPaneHitTestUPP( userUPP: ControlUserPaneHitTestUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeControlUserPaneTrackingUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeControlUserPaneTrackingUPP( userUPP: ControlUserPaneTrackingUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeControlUserPaneIdleUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeControlUserPaneIdleUPP( userUPP: ControlUserPaneIdleUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeControlUserPaneKeyDownUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeControlUserPaneKeyDownUPP( userUPP: ControlUserPaneKeyDownUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeControlUserPaneActivateUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeControlUserPaneActivateUPP( userUPP: ControlUserPaneActivateUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeControlUserPaneFocusUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeControlUserPaneFocusUPP( userUPP: ControlUserPaneFocusUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeControlUserPaneDrawUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeControlUserPaneDrawUPP( control: ControlRef; part: ControlPartCode; userUPP: ControlUserPaneDrawUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeControlUserPaneHitTestUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeControlUserPaneHitTestUPP( control: ControlRef; where: Point; userUPP: ControlUserPaneHitTestUPP ): ControlPartCode;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeControlUserPaneTrackingUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeControlUserPaneTrackingUPP( control: ControlRef; startPt: Point; actionProc: ControlActionUPP; userUPP: ControlUserPaneTrackingUPP ): ControlPartCode;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeControlUserPaneIdleUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeControlUserPaneIdleUPP( control: ControlRef; userUPP: ControlUserPaneIdleUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeControlUserPaneKeyDownUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeControlUserPaneKeyDownUPP( control: ControlRef; keyCode: SInt16; charCode: SInt16; modifiers: SInt16; userUPP: ControlUserPaneKeyDownUPP ): ControlPartCode;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeControlUserPaneActivateUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeControlUserPaneActivateUPP( control: ControlRef; activating: Boolean; userUPP: ControlUserPaneActivateUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeControlUserPaneFocusUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeControlUserPaneFocusUPP( control: ControlRef; action: ControlFocusPart; userUPP: ControlUserPaneFocusUPP ): ControlPartCode;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{$ifc not TARGET_CPU_64}
type
	ControlUserPaneBackgroundProcPtr = procedure( control: ControlRef; info: ControlBackgroundPtr );
{GPC-ONLY-START}
	ControlUserPaneBackgroundUPP = UniversalProcPtr; // should be ControlUserPaneBackgroundProcPtr
{GPC-ONLY-ELSE}
	ControlUserPaneBackgroundUPP = ControlUserPaneBackgroundProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewControlUserPaneBackgroundUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewControlUserPaneBackgroundUPP( userRoutine: ControlUserPaneBackgroundProcPtr ): ControlUserPaneBackgroundUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeControlUserPaneBackgroundUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeControlUserPaneBackgroundUPP( userUPP: ControlUserPaneBackgroundUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeControlUserPaneBackgroundUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeControlUserPaneBackgroundUPP( control: ControlRef; info: ControlBackgroundPtr; userUPP: ControlUserPaneBackgroundUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
