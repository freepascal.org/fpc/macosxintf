{
 *  QLThumbnail.h
 *  Quick Look
 *
 *  Copyright 2007-2010 Apple Inc.
 *  All rights reserved.
 *
 }
{ Initial Pascal Translation: Jonas Maebe <jonas@freepascal.org>, October 2012 }
unit QLThumbnail;
interface
uses MacTypes,CFBase,CFURL,CFDictionary,CGGeometry,CGImage,QLBase,QLThumbnailImage;

{$ifc TARGET_OS_MAC}
{$ALIGN POWER}



type
	QLThumbnailRef = ^__QLThumbnail; { an opaque type }
	__QLThumbnail = record end;

function QLThumbnailGetTypeID: CFTypeID;

{
 @function QLThumbnailCreate
 @abstract Creates a thumbnail instance. The thumbnail image will be computed in background.
 @param allocator The allocator to use to create the instance.
 @param url The URL of the document to thumbnail.
 @param maxThumbnailSize Maximum size (in points) allowed for the thumbnail image.
 @param options Optional hints for the thumbnail. (Only kQLThumbnailOptionScaleFactorKey is available for now)
 @result The thumbnail instance.
 }
function QLThumbnailCreate( allocator: CFAllocatorRef; url: CFURLRef; maxThumbnailSize: CGSize; options: CFDictionaryRef ): QLThumbnailRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{
 @function QLThumbnailCopyDocumentURL
 @abstract Returns the URL of the document to thumbnail.
 @param thumbnail The thumbnail to compute.
 }
function QLThumbnailCopyDocumentURL( thumbnail: QLThumbnailRef ): CFURLRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{
 @function QLThumbnailGetMaximumSize
 @abstract Returns the maximum size (in points) allowed for the thumbnail image.
 @param thumbnail The thumbnail to compute.
 }
function QLThumbnailGetMaximumSize( thumbnail: QLThumbnailRef ): CGSize;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{
 @function QLThumbnailCopyOptions
 @abstract Returns the options for the thumbnail.
 @param thumbnail The thumbnail to compute.
 }
function QLThumbnailCopyOptions( thumbnail: QLThumbnailRef ): CFDictionaryRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{$ifdef BLOCKS_SUPPORT}
{
 @function QLThumbnailDispatchAsync
 @abstract Start computing thumbnail in background.
 @param thumbnail The thumbnail to compute.
 @param queue Where the completion block will be dispatched.
 @param completion The completion block called upon thumbnail completion.
 @discussion The completion block will always be called, even if the thumbnail computation has been cancelled.
 }
procedure QLThumbnailDispatchAsync( thumbnail: QLThumbnailRef; queue: dispatch_queue_t; completion: dispatch_block_t );
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;
{$endif}

{
 @function QLThumbnailCopyImage
 @abstract Returns the image computed by Quick Look or NULL if not thumbnail was created.
 @param thumbnail The thumbnail to compute.
 @result The thumbnail image or NULL.
 @discussion If called without a previous of QLThumbnailDispatchAsync(), the call will block until the thumbnail is computed.
             QLThumbnailCopyImage() should not be called during async dispatch (before the completion block has been called)
 }
function QLThumbnailCopyImage( thumbnail: QLThumbnailRef ): CGImageRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{
 @function QLThumbnailGetContentRect
 @abstract Returns the the effective rect within the thumbnail image representing the
           content of the document. In icon mode, this is the part of the image without
           all the image decorations.
 @param thumbnail The thumbnail to compute.
 @result The content rect of the thumbnail expressed in pixel coordinates.
 }
function QLThumbnailGetContentRect( thumbnail: QLThumbnailRef ): CGRect;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;


{
 @function QLThumbnailCancel
 @abstract Cancels the computation of the thumbnail.
 @param thumbnail The thumbnail to compute.
 @discussion If used with QLThumbnailDispatchAsync() the completion callback will be called.
             If used in synchronous mode, QLThumbnailCopyImage() will return immediately NULL.
 }
procedure QLThumbnailCancel( thumbnail: QLThumbnailRef );
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{
 @function QLThumbnailIsCancelled
 @abstract Returns wether the thumbnail computation has been cancelled.
 @param thumbnail The thumbnail to compute.
 }
function QLThumbnailIsCancelled( thumbnail: QLThumbnailRef ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;


{$endc} {TARGET_OS_MAC}

end.
