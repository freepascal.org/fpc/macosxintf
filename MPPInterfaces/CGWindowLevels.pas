{ CoreGraphics - CGWindowLevel.h
   Copyright (c) 2000-2008 Apple Inc.
   All rights reserved. }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGWindowLevels;
interface
uses MacTypes,CGBase;
{$ALIGN POWER}


{ Windows may be assigned to a particular level. When assigned to a level,
   the window is ordered relative to all other windows in that level.
   Windows with a higher level are sorted in front of windows with a lower
   level.

   A common set of window levels is defined here for use within higher level
   frameworks. The levels are accessed via a key and function, so that
   levels may be changed or adjusted in future releases without breaking
   binary compatability. }

type
	CGWindowLevel = SInt32;
	CGWindowLevelKey = SInt32;

const
	kCGBaseWindowLevelKey = 0;
	kCGMinimumWindowLevelKey = 1;
	kCGDesktopWindowLevelKey = 2;
	kCGBackstopMenuLevelKey = 3;
	kCGNormalWindowLevelKey = 4;
	kCGFloatingWindowLevelKey = 5;
	kCGTornOffMenuWindowLevelKey = 6;
	kCGDockWindowLevelKey = 7;
	kCGMainMenuWindowLevelKey = 8;
	kCGStatusWindowLevelKey = 9;
	kCGModalPanelWindowLevelKey = 10;
	kCGPopUpMenuWindowLevelKey = 11;
	kCGDraggingWindowLevelKey = 12;
	kCGScreenSaverWindowLevelKey = 13;
	kCGMaximumWindowLevelKey = 14;
	kCGOverlayWindowLevelKey = 15;
	kCGHelpWindowLevelKey = 16;
	kCGUtilityWindowLevelKey = 17;
	kCGDesktopIconWindowLevelKey = 18;
	kCGCursorWindowLevelKey = 19;
	kCGAssistiveTechHighWindowLevelKey = 20;
	kCGNumberOfWindowLevelKeys = 21;	{ Must be last. }

{$ifc TARGET_OS_MAC}

{ Return the window level that corresponds to one of the standard window
   types. }

function CGWindowLevelForKey( key: CGWindowLevelKey ): CGWindowLevel;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);

{$endc}

{ The number of window levels reserved by Apple for internal use. }
const
	kCGNumReservedWindowLevels = 16;

(*
{ Definitions of older constant values as calls }
#define kCGBaseWindowLevel		CGWindowLevelForKey(kCGBaseWindowLevelKey)	{ LONG_MIN }
#define kCGMinimumWindowLevel 		CGWindowLevelForKey(kCGMinimumWindowLevelKey)	{ (kCGBaseWindowLevel + 1) }
#define kCGDesktopWindowLevel		CGWindowLevelForKey(kCGDesktopWindowLevelKey)	{ kCGMinimumWindowLevel }
#define kCGDesktopIconWindowLevel		CGWindowLevelForKey(kCGDesktopIconWindowLevelKey)	{ kCGMinimumWindowLevel + 20 }
#define kCGBackstopMenuLevel		CGWindowLevelForKey(kCGBackstopMenuLevelKey)	{ -20 }
#define kCGNormalWindowLevel		CGWindowLevelForKey(kCGNormalWindowLevelKey)	{ 0 }
#define kCGFloatingWindowLevel		CGWindowLevelForKey(kCGFloatingWindowLevelKey)	{ 3 }
#define kCGTornOffMenuWindowLevel	CGWindowLevelForKey(kCGTornOffMenuWindowLevelKey)	{ 3 }
#define kCGDockWindowLevel		CGWindowLevelForKey(kCGDockWindowLevelKey)	{ 20 }
#define kCGMainMenuWindowLevel		CGWindowLevelForKey(kCGMainMenuWindowLevelKey)	{ 24 }
#define kCGStatusWindowLevel		CGWindowLevelForKey(kCGStatusWindowLevelKey)	{ 25 }
#define kCGModalPanelWindowLevel	CGWindowLevelForKey(kCGModalPanelWindowLevelKey)	{ 8 }
#define kCGPopUpMenuWindowLevel		CGWindowLevelForKey(kCGPopUpMenuWindowLevelKey)	{ 101 }
#define kCGDraggingWindowLevel		CGWindowLevelForKey(kCGDraggingWindowLevelKey)	{ 500 }
#define kCGScreenSaverWindowLevel	CGWindowLevelForKey(kCGScreenSaverWindowLevelKey)	{ 1000 }
#define kCGCursorWindowLevel		CGWindowLevelForKey(kCGCursorWindowLevelKey)	{ 2000 }
#define kCGOverlayWindowLevel		CGWindowLevelForKey(kCGOverlayWindowLevelKey)	{ 102 }
#define kCGHelpWindowLevel		CGWindowLevelForKey(kCGHelpWindowLevelKey)	{ 102 }
#define kCGUtilityWindowLevel		CGWindowLevelForKey(kCGUtilityWindowLevelKey)	{ 19 }

#define kCGAssistiveTechHighWindowLevel		CGWindowLevelForKey(kCGAssistiveTechHighWindowLevelKey)	{ 1500 }

#define kCGMaximumWindowLevel 		CGWindowLevelForKey(kCGMaximumWindowLevelKey)	{ LONG_MAX - kCGNumReservedWindowLevels }
*)

end.
