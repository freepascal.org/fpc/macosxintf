{	CFStream.h
	Copyright (c) 2000-2013, Apple Inc. All rights reserved.
}
unit CFStream;
interface
uses MacTypes,MacOSXPosix,CFBase,CFString,CFDictionary,CFURL,CFRunLoop,CFSocket,CFError;
{$ALIGN POWER}


const
	kCFStreamStatusNotOpen = 0;
	kCFStreamStatusOpening = 1;  { open is in-progress }
	kCFStreamStatusOpen = 2;
	kCFStreamStatusReading = 3;
	kCFStreamStatusWriting = 4;
	kCFStreamStatusAtEnd = 5;    { no further bytes can be read/written }
	kCFStreamStatusClosed = 6;
	kCFStreamStatusError = 7;
type
	CFStreamStatus = CFIndex;

const
	kCFStreamEventNone = 0;
	kCFStreamEventOpenCompleted = 1;
	kCFStreamEventHasBytesAvailable = 2;
	kCFStreamEventCanAcceptBytes = 4;
	kCFStreamEventErrorOccurred = 8;
	kCFStreamEventEndEncountered = 16;
type
	CFStreamEventType = CFOptionFlags;

type
	CFStreamClientContext = record
		version: CFIndex;
		info: UnivPtr;
		retain: function( info: {const} univ Ptr ): UnivPtr;
		release: procedure( info: {const} univ Ptr );
		copyDescription: function( info: {const} univ Ptr ): CFStringRef;
	end;

type
	CFReadStreamRef = ^__CFReadStream; { an opaque type }
	__CFReadStream = record end;
	CFWriteStreamRefPtr = ^CFWriteStreamRef;
	CFWriteStreamRef = ^__CFWriteStream; { an opaque type }
	__CFWriteStream = record end;
	CFReadStreamRefPtr = ^CFReadStreamRef;

type
	CFReadStreamClientCallBack = procedure( stream: CFReadStreamRef; typ: CFStreamEventType; clientCallBackInfo: univ Ptr );
	CFWriteStreamClientCallBack = procedure( stream: CFWriteStreamRef; typ: CFStreamEventType; clientCallBackInfo: univ Ptr );

function CFReadStreamGetTypeID: CFTypeID;
function CFWriteStreamGetTypeID: CFTypeID;

{ Memory streams }

{ Value will be a CFData containing all bytes thusfar written; used to recover the data written to a memory write stream. }
const kCFStreamPropertyDataWritten: CFStringRef;

{ Pass kCFAllocatorNull for bytesDeallocator to prevent CFReadStream from deallocating bytes; otherwise, CFReadStream will deallocate bytes when the stream is destroyed }
function CFReadStreamCreateWithBytesNoCopy( alloc: CFAllocatorRef; bytes: univ Ptr; length: CFIndex; bytesDeallocator: CFAllocatorRef ): CFReadStreamRef;

{ The stream writes into the buffer given; when bufferCapacity is exhausted, the stream is exhausted (status becomes kCFStreamStatusAtEnd) }
function CFWriteStreamCreateWithBuffer( alloc: CFAllocatorRef; buffer: univ Ptr; bufferCapacity: CFIndex ): CFWriteStreamRef;

{ New buffers are allocated from bufferAllocator as bytes are written to the stream.  At any point, you can recover the bytes thusfar written by asking for the property kCFStreamPropertyDataWritten, above }
function CFWriteStreamCreateWithAllocatedBuffers( alloc: CFAllocatorRef; bufferAllocator: CFAllocatorRef ): CFWriteStreamRef;

{ File streams }
function CFReadStreamCreateWithFile( alloc: CFAllocatorRef; fileURL: CFURLRef ): CFReadStreamRef;
function CFWriteStreamCreateWithFile( alloc: CFAllocatorRef; fileURL: CFURLRef ): CFWriteStreamRef;
{ CF_IMPLICIT_BRIDGING_DISABLED }
procedure CFStreamCreateBoundPair( alloc: CFAllocatorRef; var readStream: CFReadStreamRef; var writeStream: CFWriteStreamRef; transferBufferSize: CFIndex );
{ CF_IMPLICIT_BRIDGING_ENABLED }

{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
{ Property for file write streams; value should be a CFBoolean.  Set to TRUE to append to a file, rather than to replace its contents }
const kCFStreamPropertyAppendToFile: CFStringRef;
{#endif}

// Value is a CFNumber
const kCFStreamPropertyFileCurrentOffset: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

{ Socket stream properties }

{ Value will be a CFData containing the native handle }
const kCFStreamPropertySocketNativeHandle: CFStringRef;

{ Value will be a CFString, or NULL if unknown }
const kCFStreamPropertySocketRemoteHostName: CFStringRef;

{ Value will be a CFNumber, or NULL if unknown }
const kCFStreamPropertySocketRemotePortNumber: CFStringRef;

{ CF_IMPLICIT_BRIDGING_DISABLED }
{ Socket streams; the returned streams are paired such that they use the same socket; pass NULL if you want only the read stream or the write stream }
procedure CFStreamCreatePairWithSocket( alloc: CFAllocatorRef; sock: CFSocketNativeHandle; var readStream: CFReadStreamRef; var writeStream: CFWriteStreamRef );
procedure CFStreamCreatePairWithSocketToHost( alloc: CFAllocatorRef; host: CFStringRef; port: UInt32; var readStream: CFReadStreamRef; var writeStream: CFWriteStreamRef );
{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
procedure CFStreamCreatePairWithPeerSocketSignature( alloc: CFAllocatorRef; const var signature: CFSocketSignature; var readStream: CFReadStreamRef; var writeStream: CFWriteStreamRef );
{#endif}
{ CF_IMPLICIT_BRIDGING_ENABLED }


{ Returns the current state of the stream }
function CFReadStreamGetStatus( stream: CFReadStreamRef ): CFStreamStatus;
function CFWriteStreamGetStatus( stream: CFWriteStreamRef ): CFStreamStatus;

{ Returns NULL if no error has occurred; otherwise returns the error. }
function CFReadStreamCopyError( stream: CFReadStreamRef ): CFErrorRef;
CF_AVAILABLE_STARTING(10_5, 2_0);
function CFWriteStreamCopyError( stream: CFWriteStreamRef ): CFErrorRef;
CF_AVAILABLE_STARTING(10_5, 2_0);

{ Returns success/failure.  Opening a stream causes it to reserve all the system
   resources it requires.  If the stream can open non-blocking, this will always 
   return TRUE; listen to the run loop source to find out when the open completes
   and whether it was successful, or poll using CFRead/WriteStreamGetStatus(), waiting 
   for a status of kCFStreamStatusOpen or kCFStreamStatusError.  }
function CFReadStreamOpen( stream: CFReadStreamRef ): Boolean;
function CFWriteStreamOpen( stream: CFWriteStreamRef ): Boolean;

{ Terminates the flow of bytes; releases any system resources required by the 
   stream.  The stream may not fail to close.  You may call CFStreamClose() to 
   effectively abort a stream. }
procedure CFReadStreamClose( stream: CFReadStreamRef );
procedure CFWriteStreamClose( stream: CFWriteStreamRef );

{ Whether there is data currently available for reading; returns TRUE if it's 
   impossible to tell without trying }
function CFReadStreamHasBytesAvailable( stream: CFReadStreamRef ): Boolean;

{ Returns the number of bytes read, or -1 if an error occurs preventing any 
   bytes from being read, or 0 if the stream's end was encountered.  
   It is an error to try and read from a stream that hasn't been opened first.  
   This call will block until at least one byte is available; it will NOT block
   until the entire buffer can be filled.  To avoid blocking, either poll using
   CFReadStreamHasBytesAvailable() or use the run loop and listen for the 
   kCFStreamCanRead event for notification of data available. }
function CFReadStreamRead( stream: CFReadStreamRef; buffer: univ Ptr; bufferLength: CFIndex ): CFIndex;

{ Returns a pointer to an internal buffer if possible (setting *numBytesRead
   to the length of the returned buffer), otherwise returns NULL; guaranteed
   to return in O(1).  Bytes returned in the buffer are considered read from
   the stream; if maxBytesToRead is greater than 0, not more than maxBytesToRead
   will be returned.  If maxBytesToRead is less than or equal to zero, as many bytes
   as are readily available will be returned.  The returned buffer is good only
   until the next stream operation called on the stream.  Caller should neither
   change the contents of the returned buffer nor attempt to deallocate the buffer;
   it is still owned by the stream. }
function CFReadStreamGetBuffer( stream: CFReadStreamRef; maxBytesToRead: CFIndex; var numBytesRead: CFIndex ): UInt8Ptr;

{ Whether the stream can currently be written to without blocking;
   returns TRUE if it's impossible to tell without trying }
function CFWriteStreamCanAcceptBytes( stream: CFWriteStreamRef ): Boolean;

{ Returns the number of bytes successfully written, -1 if an error has
   occurred, or 0 if the stream has been filled to capacity (for fixed-length
   streams).  If the stream is not full, this call will block until at least
   one byte is written.  To avoid blocking, either poll via CFWriteStreamCanAcceptBytes
   or use the run loop and listen for the kCFStreamCanWrite event. }
function CFWriteStreamWrite( stream: CFWriteStreamRef; buffer: univ Ptr; bufferLength: CFIndex ): CFIndex;

{ Particular streams can name properties and assign meanings to them; you
   access these properties through the following calls.  A property is any interesting
   information about the stream other than the data being transmitted itself.
   Examples include the headers from an HTTP transmission, or the expected 
   number of bytes, or permission information, etc.  Properties that can be set
   configure the behavior of the stream, and may only be settable at particular times
   (like before the stream has been opened).  See the documentation for particular 
   properties to determine their get- and set-ability. }
function CFReadStreamCopyProperty( stream: CFReadStreamRef; propertyName: CFStringRef ): CFTypeRef;
function CFWriteStreamCopyProperty( stream: CFWriteStreamRef; propertyName: CFStringRef ): CFTypeRef;

{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
{ Returns TRUE if the stream recognizes and accepts the given property-value pair; 
   FALSE otherwise. }
function CFReadStreamSetProperty( stream: CFReadStreamRef; propertyName: CFStringRef; propertyValue: CFTypeRef ): Boolean;
function CFWriteStreamSetProperty( stream: CFWriteStreamRef; propertyName: CFStringRef; propertyValue: CFTypeRef ): Boolean;
{#endif}

{ Asynchronous processing - If you wish to neither poll nor block, you may register 
   a client to hear about interesting events that occur on a stream.  Only one client
   per stream is allowed; registering a new client replaces the previous one.
 
   Once you have set a client, the stream must be scheduled to provide the context in
   which the client will be called.  Streams may be scheduled on a single dispatch queue
   or on one or more run loops.  If scheduled on a run loop, it is the caller's responsibility
   to ensure that at least one of the scheduled run loops is being run.

   NOTE: Unlike other CoreFoundation APIs, pasing a NULL clientContext here will remove
   the client.  If you do not care about the client context (i.e. your only concern
   is that your callback be called), you should pass in a valid context where every
   entry is 0 or NULL.

}

function CFReadStreamSetClient( stream: CFReadStreamRef; streamEvents: CFOptionFlags; clientCB: CFReadStreamClientCallBack; var clientContext: CFStreamClientContext ): Boolean;
function CFWriteStreamSetClient( stream: CFWriteStreamRef; streamEvents: CFOptionFlags; clientCB: CFWriteStreamClientCallBack; var clientContext: CFStreamClientContext ): Boolean;

procedure CFReadStreamScheduleWithRunLoop( stream: CFReadStreamRef; runLoop: CFRunLoopRef; runLoopMode: CFStringRef );
procedure CFWriteStreamScheduleWithRunLoop( stream: CFWriteStreamRef; runLoop: CFRunLoopRef; runLoopMode: CFStringRef );

procedure CFReadStreamUnscheduleFromRunLoop( stream: CFReadStreamRef; runLoop: CFRunLoopRef; runLoopMode: CFStringRef );
procedure CFWriteStreamUnscheduleFromRunLoop( stream: CFWriteStreamRef; runLoop: CFRunLoopRef; runLoopMode: CFStringRef );

{
 * Specify the dispatch queue upon which the client callbacks will be invoked.
 * Passing NULL for the queue will prevent future callbacks from being invoked.
 * Specifying a dispatch queue using this API will unschedule the stream from
 * any run loops it had previously been scheduled upon - similarly, scheduling
 * with a runloop will disassociate the stream from any existing dispatch queue.
 }
procedure CFReadStreamSetDispatchQueue( stream: CFReadStreamRef; q: dispatch_queue_t );
CF_AVAILABLE_STARTING(10_9, 7_0);

procedure CFWriteStreamSetDispatchQueue( stream: CFWriteStreamRef; q: dispatch_queue_t );
CF_AVAILABLE_STARTING(10_9, 7_0);

{
 * Returns the previously set dispatch queue with an incremented retain count.  
 * Note that the stream's queue may have been set to NULL if the stream was 
 * scheduled on a runloop subsequent to it having had a dispatch queue set.
 }
function CFReadStreamCopyDispatchQueue( stream: CFReadStreamRef ): dispatch_queue_t;
CF_AVAILABLE_STARTING(10_9, 7_0);

function CFWriteStreamCopyDispatchQueue( stream: CFWriteStreamRef ): dispatch_queue_t;
CF_AVAILABLE_STARTING(10_9, 7_0);

{ The following API is deprecated starting in 10.5; please use CFRead/WriteStreamCopyError(), above, instead }
const
	kCFStreamErrorDomainCustom = -1;      { custom to the kind of stream in question }
	kCFStreamErrorDomainPOSIX = 1;        { POSIX errno; interpret using <sys/errno.h> }
	kCFStreamErrorDomainMacOSStatus = 2;      { OSStatus type from Carbon APIs; interpret using <MacTypes.h> }
type
	CFStreamErrorDomain = CFIndex;

type
	CFStreamError = record
		domain: CFIndex; 
		error: SInt32;
	end;
	CFStreamErrorPtr = ^CFStreamError;

{ 0 is returned if no error has occurred.  errorDomain specifies the domain
   in which the error code should be interpretted; pass NULL if you are not 
   interested. }
function CFReadStreamGetError( stream: CFReadStreamRef ): CFStreamError;
function CFWriteStreamGetError( stream: CFWriteStreamRef ): CFStreamError;

end.
