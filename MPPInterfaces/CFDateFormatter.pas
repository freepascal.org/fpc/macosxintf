{	CFDateFormatter.h
	Copyright (c) 2003-2013, Apple Inc. All rights reserved.
}
unit CFDateFormatter;
interface
uses MacTypes,CFBase,CFDate,CFLocale;
{$ALIGN POWER}


{#if MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_3}

type
	CFDateFormatterRef = ^__CFDateFormatter; { an opaque type }
	__CFDateFormatter = record end;

// CFDateFormatters are not thread-safe.  Do not use one from multiple threads!

function CFDateFormatterCreateDateFormatFromTemplate( allocator: CFAllocatorRef; tmplate: CFStringRef; options: CFOptionFlags; locale: CFLocaleRef ): CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0);
	// no options defined, pass 0 for now

function CFDateFormatterGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

type
	CFDateFormatterStyle = CFIndex;
const
// date and time format styles
	kCFDateFormatterNoStyle = 0;
	kCFDateFormatterShortStyle = 1;
	kCFDateFormatterMediumStyle = 2;
	kCFDateFormatterLongStyle = 3;
	kCFDateFormatterFullStyle = 4;

// The exact formatted result for these date and time styles depends on the
// locale, but generally:
//     Short is completely numeric, such as "12/13/52" or "3:30pm"
//     Medium is longer, such as "Jan 12, 1952"
//     Long is longer, such as "January 12, 1952" or "3:30:32pm"
//     Full is pretty complete; e.g. "Tuesday, April 12, 1952 AD" or "3:30:42pm PST"
// The specifications though are left fuzzy, in part simply because a user's
// preference choices may affect the output, and also the results may change
// from one OS release to another.  To produce an exactly formatted date you
// should not rely on styles and localization, but set the format string and
// use nothing but numbers.

function CFDateFormatterCreate( allocator: CFAllocatorRef; locale: CFLocaleRef; dateStyle: CFDateFormatterStyle; timeStyle: CFDateFormatterStyle ): CFDateFormatterRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Returns a CFDateFormatter, localized to the given locale, which
	// will format dates to the given date and time styles.

function CFDateFormatterGetLocale( formatter: CFDateFormatterRef ): CFLocaleRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFDateFormatterGetDateStyle( formatter: CFDateFormatterRef ): CFDateFormatterStyle;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFDateFormatterGetTimeStyle( formatter: CFDateFormatterRef ): CFDateFormatterStyle;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Get the properties with which the date formatter was created.

function CFDateFormatterGetFormat( formatter: CFDateFormatterRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

procedure CFDateFormatterSetFormat( formatter: CFDateFormatterRef; formatString: CFStringRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Set the format description string of the date formatter.  This
	// overrides the style settings.  The format of the format string
	// is as defined by the ICU library.  The date formatter starts with a
	// default format string defined by the style arguments with
	// which it was created.


function CFDateFormatterCreateStringWithDate( allocator: CFAllocatorRef; formatter: CFDateFormatterRef; date: CFDateRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFDateFormatterCreateStringWithAbsoluteTime( allocator: CFAllocatorRef; formatter: CFDateFormatterRef; at: CFAbsoluteTime ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Create a string representation of the given date or CFAbsoluteTime
	// using the current state of the date formatter.


function CFDateFormatterCreateDateFromString( allocator: CFAllocatorRef; formatter: CFDateFormatterRef; strng: CFStringRef; rangep: CFRangePtr ): CFDateRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFDateFormatterGetAbsoluteTimeFromString( formatter: CFDateFormatterRef; strng: CFStringRef; rangep: CFRangePtr; atp: CFAbsoluteTimePtr ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Parse a string representation of a date using the current state
	// of the date formatter.  The range parameter specifies the range
	// of the string in which the parsing should occur in input, and on
	// output indicates the extent that was used; this parameter can
	// be NULL, in which case the whole string may be used.  The
	// return value indicates whether some date was computed and
	// (if atp is not NULL) stored at the location specified by atp.


procedure CFDateFormatterSetProperty( formatter: CFDateFormatterRef; key: CFStringRef; value: CFTypeRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFDateFormatterCopyProperty( formatter: CFDateFormatterRef; key: CFStringRef ): CFTypeRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Set and get various properties of the date formatter, the set of
	// which may be expanded in the future.

const kCFDateFormatterIsLenient: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFBoolean
const kCFDateFormatterTimeZone: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFTimeZone
const kCFDateFormatterCalendarName: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFString
const kCFDateFormatterDefaultFormat: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFString
const kCFDateFormatterTwoDigitStartDate: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER; // CFDate
const kCFDateFormatterDefaultDate: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFDate
const kCFDateFormatterCalendar: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;		// CFCalendar
const kCFDateFormatterEraSymbols: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFArray of CFString
const kCFDateFormatterMonthSymbols: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFArray of CFString
const kCFDateFormatterShortMonthSymbols: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER; // CFArray of CFString
const kCFDateFormatterWeekdaySymbols: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	// CFArray of CFString
const kCFDateFormatterShortWeekdaySymbols: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER; // CFArray of CFString
const kCFDateFormatterAMSymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;		// CFString
const kCFDateFormatterPMSymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;		// CFString
const kCFDateFormatterLongEraSymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0);   // CFArray of CFString
const kCFDateFormatterVeryShortMonthSymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterStandaloneMonthSymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterShortStandaloneMonthSymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterVeryShortStandaloneMonthSymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterVeryShortWeekdaySymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterStandaloneWeekdaySymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterShortStandaloneWeekdaySymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterVeryShortStandaloneWeekdaySymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterQuarterSymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); 	// CFArray of CFString
const kCFDateFormatterShortQuarterSymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterStandaloneQuarterSymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterShortStandaloneQuarterSymbols: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFArray of CFString
const kCFDateFormatterGregorianStartDate: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFDate
const kCFDateFormatterDoesRelativeDateFormattingKey: CFStringRef;
CF_AVAILABLE_STARTING(10_6, 4_0); // CFBoolean

// See CFLocale.h for these calendar constants:
//	const CFStringRef kCFGregorianCalendar;
//	const CFStringRef kCFBuddhistCalendar;
//	const CFStringRef kCFJapaneseCalendar;
//	const CFStringRef kCFIslamicCalendar;
//	const CFStringRef kCFIslamicCivilCalendar;
//	const CFStringRef kCFHebrewCalendar;
//	const CFStringRef kCFChineseCalendar;
//	const CFStringRef kCFRepublicOfChinaCalendar;
//	const CFStringRef kCFPersianCalendar;
//	const CFStringRef kCFIndianCalendar;
//	const CFStringRef kCFISO8601Calendar;   implemented as of 10.9

{#endif}


end.
