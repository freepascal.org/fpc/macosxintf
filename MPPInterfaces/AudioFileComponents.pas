{!
	@file		AudioFileComponent.h
	@framework	AudioToolbox.framework
	@copyright	(c) 2004-2015 by Apple, Inc., all rights reserved.

	@abstract	Interfaces for components which implement knowledge of audio file formats.
    @discussion
    	Audio file components are not for the use of clients. Rather, they are called by the
    	implementation of the AudioFile API to implement the various semantics of that API.
		Most of these calls match a call in the AudioFile API which calls through to the component.

		A component may be used in two ways, either associated with a file or not. If a component is
		not associated with a file, it may be used to answer questions about the file type in
		general and whether some data is recognized by that file type. A component is associated
		with a file by calling one of the AudioFile Create, Open or Initialize calls. If a component
		is associated with a file, then it can also be asked to perform any of the calls that
		implement the AudioFile API.
}
{  Pascal Translation: Jonas Maebe <jonas@freepascal.org>, July 2019 }
unit AudioFileComponents;
interface
uses MacTypes,CFBase,CoreAudioTypes,Files,AudioFile,AudioComponents;
{$ALIGN POWER}


//==================================================================================================
//	Includes
//==================================================================================================


//CF_ASSUME_NONNULL_BEGIN


{!
    @typedef	AudioFileComponent
    @abstract		represents an instance of an AudioFileComponent.
}
type
	AudioFileComponent = AudioComponentInstance;
	AudioFileComponentPtr = ^AudioFileComponent;
{!
    @typedef	AudioFileComponentPropertyID
    @abstract		a four char code for a property ID.
}
type
	AudioFileComponentPropertyID = UInt32;
	AudioFileComponentPropertyIDPtr = ^AudioFileComponentPropertyID;

{$ifc TARGET_OS_MAC}
{!
    @function	AudioFileComponentCreateURL
    @abstract   creates a new (or initialises an existing) audio file specified by the URL.
    @discussion	creates a new (or initialises an existing) audio file specified by the URL.
    @param inComponent		an AudioFileComponent
    @param inFileRef		an CFURLRef fully specifying the path of the file to create/initialise
    @param inFormat			an AudioStreamBasicDescription describing the data format that will be
							added to the audio file.
    @param inFlags			relevant flags for creating/opening the file. 
								if kAudioFileFlags_EraseFile is set, it will erase an existing file
								 if not set, then the Create call will fail if the URL is an existing file
    @result					returns noErr if successful.
}
function AudioFileComponentCreateURL( inComponent: AudioFileComponent; inFileRef: CFURLRef; const var inFormat: AudioStreamBasicDescription; inFlags: UInt32 ): OSStatus;
API_AVAILABLE(macos(10.5)) API_UNAVAILABLE(ios, watchos, tvos);

{!
    @function				AudioFileComponentOpenURL
    @abstract				Open an existing audio file.
    @discussion				Open an existing audio file for reading or reading and writing.
    @param inComponent		an AudioFileComponent.
    @param inFileRef		the CFURLRef of an existing audio file.
    @param inPermissions	use the permission constants.
    @param inFileDescriptor	an open file descriptor.
    @result					returns noErr if successful.
}
function AudioFileComponentOpenURL( inComponent: AudioFileComponent; inFileRef: CFURLRef; inPermissions: SInt8; inFileDescriptor: SInt32 ): OSStatus;
API_AVAILABLE(macos(10.5)) API_UNAVAILABLE(ios, watchos, tvos);

{!
    @function	AudioFileComponentOpenWithCallbacks
    @abstract   implements AudioFileOpenWithCallbacks
    @param inComponent		an AudioFileComponent
    @param inClientData 	a constant that will be passed to your callbacks.
	@param inReadFunc		a function that will be called when AudioFile needs to read data.
	@param inWriteFunc		a function that will be called when AudioFile needs to write data.
	@param inGetSizeFunc	a function that will be called when AudioFile needs to know the file size.
	@param inSetSizeFunc	a function that will be called when AudioFile needs to set the file size.
    @result					returns noErr if successful.
}
function AudioFileComponentOpenWithCallbacks( inComponent: AudioFileComponent; inClientData: univ Ptr; inReadFunc: AudioFile_ReadProc; inWriteFunc: AudioFile_WriteProc; inGetSizeFunc: AudioFile_GetSizeProc; inSetSizeFunc: AudioFile_SetSizeProc ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);

{!
    @function	AudioFileComponentInitializeWithCallbacks
    @abstract   implements AudioFileInitializeWithCallbacks
    @param inComponent		an AudioFileComponent
    @param inClientData 	a constant that will be passed to your callbacks.
	@param inReadFunc		a function that will be called when AudioFile needs to read data.
	@param inWriteFunc		a function that will be called when AudioFile needs to write data.
	@param inGetSizeFunc	a function that will be called when AudioFile needs to know the file size.
	@param inSetSizeFunc	a function that will be called when AudioFile needs to set the file size.
    @param inFileType 		an AudioFileTypeID indicating the type of audio file to which to initialize the file. 
    @param inFormat			an AudioStreamBasicDescription describing the data format that will be
							added to the audio file.
    @param inFlags			relevant flags for creating/opening the file. Currently zero.
    @result					returns noErr if successful.
}
function AudioFileComponentInitializeWithCallbacks( inComponent: AudioFileComponent; inClientData: univ Ptr; inReadFunc: AudioFile_ReadProc; inWriteFunc: AudioFile_WriteProc; inGetSizeFunc: AudioFile_GetSizeProc; inSetSizeFunc: AudioFile_SetSizeProc; inFileType: UInt32; const var inFormat: AudioStreamBasicDescription; inFlags: UInt32 ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);


{!
    @function	AudioFileComponentCloseFile
    @abstract   implements AudioFileClose.
    @param inComponent		an AudioFileComponent
    @result					returns noErr if successful.
}
function AudioFileComponentCloseFile( inComponent: AudioFileComponent ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);
				
{!
    @function	AudioFileComponentOptimize
    @abstract   implements AudioFileOptimize.
    @param inComponent		an AudioFileComponent
    @result					returns noErr if successful.
}
function AudioFileComponentOptimize( inComponent: AudioFileComponent ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);
				
{!
    @function	AudioFileComponentReadBytes
    @abstract   implements AudioFileReadBytes. 
				
    @discussion				Returns kAudioFileEndOfFileError when read encounters end of file.
    @param inComponent		an AudioFileComponent
    @param inUseCache 		true if it is desired to cache the data upon read, else false
    @param inStartingByte	the byte offset of the audio data desired to be returned
    @param ioNumBytes 		on input, the number of bytes to read, on output, the number of
							bytes actually read.
    @param outBuffer 		outBuffer should be a void * to user allocated memory large enough for the requested bytes. 
    @result					returns noErr if successful.
}
function AudioFileComponentReadBytes( inComponent: AudioFileComponent; inUseCache: Boolean; inStartingByte: SInt64; var ioNumBytes: UInt32; outBuffer: univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);
						
{!
    @function				AudioFileComponentWriteBytes
    @abstract				implements AudioFileWriteBytes.
    @param inComponent		an AudioFileComponent
    @param inUseCache 		true if it is desired to cache the data upon write, else false
    @param inStartingByte	the byte offset where the audio data should be written
    @param ioNumBytes 		on input, the number of bytes to write, on output, the number of
							bytes actually written.
    @param inBuffer 		inBuffer should be a void * containing the bytes to be written 
    @result					returns noErr if successful.
}
function AudioFileComponentWriteBytes( inComponent: AudioFileComponent; inUseCache: Boolean; inStartingByte: SInt64; var ioNumBytes: UInt32; inBuffer: {const} univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);
						
{!
    @function	AudioFileComponentReadPackets
    @abstract   implements AudioFileReadPackets.
    @discussion For all uncompressed formats, packets == frames.
				ioNumPackets less than requested indicates end of file.

    @param inComponent				an AudioFileComponent
    @param inUseCache 				true if it is desired to cache the data upon read, else false
    @param outNumBytes				on output, the number of bytes actually returned
    @param outPacketDescriptions 	on output, an array of packet descriptions describing
									the packets being returned. NULL may be passed for this
									parameter. Nothing will be returned for linear pcm data.   
    @param inStartingPacket 		the packet index of the first packet desired to be returned
    @param ioNumPackets 			on input, the number of packets to read, on output, the number of
									packets actually read.
    @param outBuffer 				outBuffer should be a pointer to user allocated memory of size: 
									number of packets requested times file's maximum (or upper bound on)
									packet size.
    @result							returns noErr if successful.
}
function AudioFileComponentReadPackets( inComponent: AudioFileComponent; inUseCache: Boolean; var outNumBytes: UInt32; outPacketDescriptions: AudioStreamPacketDescriptionPtr {* __nullable}; inStartingPacket: SInt64; var ioNumPackets: UInt32; outBuffer: univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);
									

{!
    @function	AudioFileComponentReadPacketData
    @abstract   implements AudioFileReadPacketData.
    @discussion For all uncompressed formats, packets == frames.
				If the byte size of the number packets requested is 
				less than the buffer size, ioNumBytes will be reduced.
				If the buffer is too small for the number of packets 
				requested, ioNumPackets and ioNumBytes will be reduced 
				to the number of packets that can be accommodated and their byte size.
				Returns kAudioFileEndOfFileError when read encounters end of file.

    @param inComponent				an AudioFileComponent
    @param inUseCache 				true if it is desired to cache the data upon read, else false
    @param ioNumBytes				on input the size of outBuffer in bytes. 
									on output, the number of bytes actually returned.
    @param outPacketDescriptions 	on output, an array of packet descriptions describing
									the packets being returned. NULL may be passed for this
									parameter. Nothing will be returned for linear pcm data.   
    @param inStartingPacket 		the packet index of the first packet desired to be returned
    @param ioNumPackets 			on input, the number of packets to read, on output, the number of
									packets actually read.
    @param outBuffer 				outBuffer should be a pointer to user allocated memory.
    @result							returns noErr if successful.
}
function AudioFileComponentReadPacketData( inComponent: AudioFileComponent; inUseCache: Boolean; var ioNumBytes: UInt32; outPacketDescriptions: AudioStreamPacketDescriptionPtr {* __nullable}; inStartingPacket: SInt64; var ioNumPackets: UInt32; outBuffer: univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);
									
{!
    @function	AudioFileComponentWritePackets
    @abstract   implements AudioFileWritePackets.
    @discussion For all uncompressed formats, packets == frames.
    @param inComponent				an AudioFileComponent
    @param inUseCache 				true if it is desired to cache the data upon write, else false
    @param inNumBytes				the number of bytes being provided for write
    @param inPacketDescriptions 	an array of packet descriptions describing the packets being 
									provided. Not all formats require packet descriptions to be 
									provided. NULL may be passed if no descriptions are required.   
    @param inStartingPacket 		the packet index of where the first packet provided should be placed.
    @param ioNumPackets 			on input, the number of packets to write, on output, the number of
									packets actually written.
    @param inBuffer 				a void * to user allocated memory containing the packets to write.
    @result							returns noErr if successful.
}
function AudioFileComponentWritePackets( inComponent: AudioFileComponent; inUseCache: Boolean; inNumBytes: UInt32; {const} inPacketDescriptions: AudioStreamPacketDescriptionPtr {* __nullable}; inStartingPacket: SInt64; var ioNumPackets: UInt32; inBuffer: {const} univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);


{!
    @function	AudioFileComponentGetPropertyInfo
    @abstract   implements AudioFileGetPropertyInfo.
    @param		inComponent			an AudioFileComponent
    @param      inPropertyID		an AudioFileProperty constant.
    @param      outPropertySize		the size in bytes of the current value of the property. In order to get the property value, 
									you will need a buffer of this size.
    @param      outWritable			will be set to 1 if writable, or 0 if read only.
    @result							returns noErr if successful.
}
function AudioFileComponentGetPropertyInfo( inComponent: AudioFileComponent; inPropertyID: AudioFileComponentPropertyID; outPropertySize: UInt32Ptr {* __nullable}; outWritable: UInt32Ptr {* __nullable} ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);


{!
    @function	AudioFileComponentGetProperty
    @abstract   implements AudioFileGetProperty.
    @param		inComponent			an AudioFileComponent
    @param      inPropertyID		an AudioFileProperty constant.
    @param      ioPropertyDataSize	on input the size of the outPropertyData buffer. On output the number of bytes written to the buffer.
    @param      outPropertyData		the buffer in which to write the property data.
    @result							returns noErr if successful.
}
function AudioFileComponentGetProperty( inComponent: AudioFileComponent; inPropertyID: AudioFileComponentPropertyID; var ioPropertyDataSize: UInt32; outPropertyData: univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);

{!
    @function	AudioFileComponentSetProperty
    @abstract   implements AudioFileSetProperty.
    @param		inComponent			an AudioFileComponent
    @param      inPropertyID		an AudioFileProperty constant.
    @param      inPropertyDataSize	the size of the property data.
    @param      inPropertyData		the buffer containing the property data.
    @result							returns noErr if successful.
}
function AudioFileComponentSetProperty( inComponent: AudioFileComponent; inPropertyID: AudioFileComponentPropertyID; inPropertyDataSize: UInt32; inPropertyData: {const} univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);


{!
    @function	AudioFileComponentCountUserData
    @abstract   implements AudioFileCountUserData
    @discussion		"User Data" refers to chunks in AIFF, CAF and WAVE files, or resources 
					in Sound Designer II files, and possibly other things in other files.
					For simplicity, referred to below as "chunks".
    @param inComponent				an AudioFileComponent
    @param inUserDataID				the four char code of the chunk.
    @param outNumberItems			on output, if successful, number of chunks of this type in the file.
    @result							returns noErr if successful.
}
function AudioFileComponentCountUserData( inComponent: AudioFileComponent; inUserDataID: UInt32; var outNumberItems: UInt32 ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);

{!
    @function	AudioFileComponentGetUserDataSize
    @abstract   implements AudioFileGetUserDataSize
    @param inComponent				an AudioFileComponent
    @param inUserDataID				the four char code of the chunk.
    @param inIndex					an index specifying which chunk if there are more than one.
    @param outUserDataSize			on output, if successful, the size of the user data chunk.
    @result							returns noErr if successful.
}
function AudioFileComponentGetUserDataSize( inComponent: AudioFileComponent; inUserDataID: UInt32; inIndex: UInt32; var outUserDataSize: UInt32 ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);

{!
    @function	AudioFileGetUserData
    @abstract   implements AudioFileGetUserData.
    @param		inComponent			an AudioFileComponent
    @param      inUserDataID		the four char code of the chunk.
    @param      inIndex				an index specifying which chunk if there are more than one.
	@param		ioUserDataSize		the size of the buffer on input, size of bytes copied to buffer on output 
    @param      outUserData			a pointer to a buffer in which to copy the chunk data.
    @result							returns noErr if successful.
}
function AudioFileComponentGetUserData( inComponent: AudioFileComponent; inUserDataID: UInt32; inIndex: UInt32; var ioUserDataSize: UInt32; outUserData: univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);

{!
    @function	AudioFileComponentSetUserData
    @abstract   implements AudioFileSetUserData.
    @param		inComponent			an AudioFileComponent
    @param      inUserDataID		the four char code of the chunk.
    @param      inIndex				an index specifying which chunk if there are more than one.
	@param		inUserDataSize		on input the size of the data to copy, on output, size of bytes copied from the buffer  
    @param      inUserData			a pointer to a buffer from which to copy the chunk data 
									(only the contents of the chunk, not including the chunk header).
    @result							returns noErr if successful.
}
function AudioFileComponentSetUserData( inComponent: AudioFileComponent; inUserDataID: UInt32; inIndex: UInt32; inUserDataSize: UInt32; inUserData: {const} univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);


{!
    @function	AudioFileComponentRemoveUserData
    @abstract   implements AudioFileRemoveUserData.
    @param		inComponent			an AudioFileComponent
    @param      inUserDataID		the four char code of the chunk.
    @param      inIndex				an index specifying which chunk if there are more than one.
    @result							returns noErr if successful.
}
function AudioFileComponentRemoveUserData( inComponent: AudioFileComponent; inUserDataID: UInt32; inIndex: UInt32 ): OSStatus;
API_AVAILABLE(macos(10.5)) API_UNAVAILABLE(ios, watchos, tvos);

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//	The following calls are not made on AudioFile instances.
//  These calls are used to determine the audio file type of some data. 
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
{!
    @function	AudioFileComponentExtensionIsThisFormat
    @abstract   used by the AudioFile API to determine if this component is appropriate for handling a file.
    @param		inComponent			an AudioFileComponent
    @param      inExtension			a CFString containing a file name extension.
    @param      outResult			on output, is set to 1 if the extension is recognized by this component, 0 if not.
    @result							returns noErr if successful.
}
function AudioFileComponentExtensionIsThisFormat( inComponent: AudioFileComponent; inExtension: CFStringRef; var outResult: UInt32 ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);


{!
    @function	AudioFileComponentFileDataIsThisFormat
    @abstract   used by the AudioFile API to determine if this component is appropriate for handling a file.
    @param		inComponent			an AudioFileComponent
    @param      inDataByteSize		the size of inData in bytes.
    @param      inData				a pointer to a buffer of audio file data.
    @param      outResult			on output, is set to 1 if the file is recognized by this component, 0 if not.
    @result							returns noErr if successful.
}
function AudioFileComponentFileDataIsThisFormat( inComponent: AudioFileComponent; inDataByteSize: UInt32; inData: {const} univ Ptr; var outResult: UInt32 ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);

{$ifc false}
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//	The following two calls are deprecated. 
//  Please implement AudioFileComponentFileDataIsThisFormat instead.
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
{!
    @function	AudioFileComponentFileIsThisFormat
    @abstract   deprecated. use AudioFileComponentFileDataIsThisFormat instead.
    @param		inComponent			an AudioFileComponent
    @param      inFileRefNum		a refNum of a file.
    @param      outResult			on output, is set to 1 if the file is recognized by this component, 0 if not.
    @result							returns noErr if successful.
}
function AudioFileComponentFileIsThisFormat( inComponent: AudioFileComponent; inFileRefNum: SInt16; var outResult: UInt32 ): OSStatus;
API_DEPRECATED("no longer supported", macos(10.4, 10.5)) API_UNAVAILABLE(ios, watchos, tvos); 
	
{!
    @function	AudioFileComponentDataIsThisFormat
    @abstract   deprecated. use AudioFileComponentFileDataIsThisFormat instead.
    @param		inComponent			an AudioFileComponent
    @param		inClientData		a constant that will be passed to your callbacks.
	@param		inReadFunc			a function that will be called when AudioFile needs to read data.
	@param		inWriteFunc			a function that will be called when AudioFile needs to write data.
	@param		inGetSizeFunc		a function that will be called when AudioFile needs to know the file size.
	@param		inSetSizeFunc		a function that will be called when AudioFile needs to set the file size.
    @param      outResult			on output, is set to 1 if the file data is recognized by this component, 0 if not.
    @result							returns noErr if successful.
}
function AudioFileComponentDataIsThisFormat( inComponent: AudioFileComponent; inClientData: univ Ptr {__nullable}; inReadFunc: AudioFile_ReadProc {__nullable}; inWriteFunc: AudioFile_WriteProc {__nullable}; inGetSizeFunc: AudioFile_GetSizeProc {__nullable}; inSetSizeFunc: AudioFile_SetSizeProc {__nullable}; var outResult: UInt32 ): OSStatus;
API_DEPRECATED("no longer supported", macos(10.4, 10.5)) API_UNAVAILABLE(ios, watchos, tvos); 
{$endc}

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//	The following calls are not made on AudioFile instances.
//  They implement the AudioFileGetGlobalInfo calls.
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

{!
    @function	AudioFileComponentGetGlobalInfoSize
    @abstract   implements AudioFileGetGlobalInfoSize.
    @param		inComponent			an AudioFileComponent
    @param      inPropertyID		an AudioFileGlobalInfo property constant.
    @param      inSpecifierSize		The size of the specifier data.
    @param      inSpecifier			A specifier is a buffer of data used as an input argument to some of the global info properties.
    @param      outPropertySize		the size in bytes of the current value of the property. In order to get the property value, 
									you will need a buffer of this size.
    @result							returns noErr if successful.
}
function AudioFileComponentGetGlobalInfoSize( inComponent: AudioFileComponent; inPropertyID: AudioFileComponentPropertyID; inSpecifierSize: UInt32; {const} inSpecifier: univ Ptr {__nullable}; var outPropertySize: UInt32 ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);

{!
    @function	AudioFileComponentGetGlobalInfo
    @abstract   implements AudioFileGetGlobalInfo.
    @param		inComponent			an AudioFileComponent
    @param      inPropertyID		an AudioFileGlobalInfo property constant.
    @param      inSpecifierSize		The size of the specifier data.
    @param      inSpecifier			A specifier is a buffer of data used as an input argument to some of the global info properties.
    @param      ioPropertyDataSize	on input the size of the outPropertyData buffer. On output the number of bytes written to the buffer.
    @param      outPropertyData		the buffer in which to write the property data.
    @result							returns noErr if successful.
}
function AudioFileComponentGetGlobalInfo( inComponent: AudioFileComponent; inPropertyID: AudioFileComponentPropertyID; inSpecifierSize: UInt32; {const} inSpecifier: univ Ptr {__nullable}; var ioPropertyDataSize: UInt32; outPropertyData: univ Ptr ): OSStatus;
API_AVAILABLE(macos(10.4)) API_UNAVAILABLE(ios, watchos, tvos);

//==================================================================================================
//	Properties for AudioFileComponentGetGlobalInfo. 
//==================================================================================================

{!
    @enum AudioFileComponent specific properties
    @constant   kAudioFileComponent_CanRead
					Is file type readable? Returns a UInt32 1 or 0. 
    @constant   kAudioFileComponent_CanWrite
					Is file type writeable? Returns a UInt32 1 or 0. 
    @constant   kAudioFileComponent_FileTypeName
					Returns a CFString containing the name for the file type. 
    @constant   kAudioFileComponent_ExtensionsForType
					Returns a CFArray of CFStrings containing the file extensions 
					that are recognized for this file type. 
    @constant   kAudioFileComponent_UTIsForType
					Returns a CFArray of CFStrings containing the universal type identifiers 
					for this file type. 
    @constant   kAudioFileComponent_MIMETypesForType
					Returns a CFArray of CFStrings containing the MIME types 
					for this file type. 
    @constant   kAudioFileComponent_AvailableFormatIDs
					Returns a array of format IDs for formats that can be read. 
    @constant   kAudioFileComponent_AvailableStreamDescriptionsForFormat
					The specifier is the format ID for the requested format.
					Returns an array of AudioStreamBasicDescriptions which have all of the 
					formats for a particular file type and format ID. The AudioStreamBasicDescriptions
					have the following fields filled in: mFormatID, mFormatFlags, mBitsPerChannel
    @constant   kAudioFileComponent_FastDispatchTable
					Deprecated. This selector is no longer called by the implementation. 
    @constant   kAudioFileComponent_HFSTypeCodesForType
					Returns an array of HFSTypeCodes corresponding to this file type.
					The first type in the array is the preferred one for use when
					writing new files.
}
const
	kAudioFileComponent_CanRead = FOUR_CHAR_CODE('cnrd');
	kAudioFileComponent_CanWrite = FOUR_CHAR_CODE('cnwr');
	kAudioFileComponent_FileTypeName = FOUR_CHAR_CODE('ftnm');
	kAudioFileComponent_UTIsForType = FOUR_CHAR_CODE('futi');
	kAudioFileComponent_MIMETypesForType = FOUR_CHAR_CODE('fmim');
	kAudioFileComponent_ExtensionsForType = FOUR_CHAR_CODE('fext');
	kAudioFileComponent_AvailableFormatIDs = FOUR_CHAR_CODE('fmid');
	kAudioFileComponent_AvailableStreamDescriptionsForFormat = FOUR_CHAR_CODE('sdid');
	kAudioFileComponent_FastDispatchTable = FOUR_CHAR_CODE('fdft');
	kAudioFileComponent_HFSTypeCodesForType = FOUR_CHAR_CODE('fhfs'); 

//==================================================================================================
//	Selectors for the component routines 
//==================================================================================================

const
	kAudioFileCreateSelect = $0001;
	kAudioFileOpenSelect = $0002;
	kAudioFileInitializeSelect = $0003;
	kAudioFileOpenWithCallbacksSelect = $0004;
	kAudioFileInitializeWithCallbacksSelect = $0005;
	kAudioFileCloseSelect = $0006;
	kAudioFileOptimizeSelect = $0007;
	kAudioFileReadBytesSelect = $0008;
	kAudioFileWriteBytesSelect = $0009;
	kAudioFileReadPacketsSelect = $000A;
	kAudioFileWritePacketsSelect = $000B;
	kAudioFileGetPropertyInfoSelect = $000C;
	kAudioFileGetPropertySelect = $000D;
	kAudioFileSetPropertySelect = $000E;
	kAudioFileExtensionIsThisFormatSelect = $000F;
	kAudioFileFileIsThisFormatSelect = $0010;
	kAudioFileDataIsThisFormatSelect = $0011;
	kAudioFileGetGlobalInfoSizeSelect = $0012;
	kAudioFileGetGlobalInfoSelect = $0013;
	kAudioFileCountUserDataSelect = $0014;
	kAudioFileGetUserDataSizeSelect = $0015;
	kAudioFileGetUserDataSelect = $0016;
	kAudioFileSetUserDataSelect = $0017;
	kAudioFileRemoveUserDataSelect = $0018;
	kAudioFileCreateURLSelect = $0019;
	kAudioFileOpenURLSelect = $001A;
	kAudioFileFileDataIsThisFormatSelect = $001B;
	kAudioFileReadPacketDataSelect = $001C; 


//#pragma mark -
//#pragma mark Deprecated

//==================================================================================================
// Fast Dispatch Function typedefs. Deprecated. These are no longer used by the implementation.
//==================================================================================================

type
	ReadBytesFDF = function( inComponentStorage: univ Ptr; inUseCache: Boolean; inStartingByte: SInt64; var ioNumBytes: UInt32; outBuffer: univ Ptr ): OSStatus;
								
type
	WriteBytesFDF = function( inComponentStorage: univ Ptr; inUseCache: Boolean; inStartingByte: SInt64; var ioNumBytes: UInt32; inBuffer: {const} univ Ptr ): OSStatus;
							
type
	ReadPacketsFDF = function( inComponentStorage: univ Ptr; inUseCache: Boolean; var outNumBytes: UInt32; outPacketDescriptions: AudioStreamPacketDescriptionPtr {* __nullable}; inStartingPacket: SInt64; var ioNumPackets: UInt32; outBuffer: univ Ptr ): OSStatus;

type
	ReadPacketDataFDF = function( inComponentStorage: univ Ptr; inUseCache: Boolean; var ioNumBytes: UInt32; outPacketDescriptions: AudioStreamPacketDescriptionPtr {* __nullable}; inStartingPacket: SInt64; var ioNumPackets: UInt32; outBuffer: univ Ptr ): OSStatus;

type
	WritePacketsFDF = function( inComponentStorage: univ Ptr; inUseCache: Boolean; inNumBytes: UInt32; {const} inPacketDescriptions: AudioStreamPacketDescriptionPtr {* __nullable}; inStartingPacket: SInt64; var ioNumPackets: UInt32; inBuffer: {const} univ Ptr ): OSStatus;
								
type
	GetPropertyInfoFDF = function( inComponentStorage: univ Ptr; inPropertyID: AudioFilePropertyID; outDataSize: UInt32Ptr {* __nullable}; isWritable: UInt32Ptr {* __nullable} ): OSStatus;
								
type
	GetPropertyFDF = function( inComponentStorage: univ Ptr; inPropertyID: AudioFilePropertyID; var ioDataSize: UInt32; ioPropertyData: univ Ptr ): OSStatus;
							
type
	SetPropertyFDF = function( inComponentStorage: univ Ptr; inPropertyID: AudioFilePropertyID; inDataSize: UInt32; inPropertyData: {const} univ Ptr ): OSStatus;

type
	CountUserDataFDF = function( inComponentStorage: univ Ptr; inUserDataID: UInt32; var outNumberItems: UInt32 ): OSStatus;

type
	GetUserDataSizeFDF = function( inComponentStorage: univ Ptr; inUserDataID: UInt32; inIndex: UInt32; var outDataSize: UInt32 ): OSStatus;

type
	GetUserDataFDF = function( inComponentStorage: univ Ptr; inUserDataID: UInt32; inIndex: UInt32; var ioUserDataSize: UInt32; outUserData: univ Ptr ): OSStatus;

type
	SetUserDataFDF = function( inComponentStorage: univ Ptr; inUserDataID: UInt32; inIndex: UInt32; inUserDataSize: UInt32; inUserData: {const} univ Ptr ): OSStatus;
										
{ no fast dispatch for kAudioFileRemoveUserDataSelect }

//#pragma /mark -
//#pragma /mark Deprecated

//==================================================================================================
// Fast Dispatch Function tables. Deprecated. These are no longer used by the implementation.
//==================================================================================================

type
	AudioFileFDFTable = record
		mComponentStorage: UnivPtr;
	 	mReadBytesFDF: ReadBytesFDF;
		mWriteBytesFDF: WriteBytesFDF;
		mReadPacketsFDF: ReadPacketsFDF;
		mWritePacketsFDF: WritePacketsFDF;

		mGetPropertyInfoFDF: GetPropertyInfoFDF;
		mGetPropertyFDF: GetPropertyFDF;
		mSetPropertyFDF: SetPropertyFDF;
	
		mCountUserDataFDF: CountUserDataFDF;
		mGetUserDataSizeFDF: GetUserDataSizeFDF;
		mGetUserDataFDF: GetUserDataFDF;
		mSetUserDataFDF: SetUserDataFDF;
	end;
	AudioFileFDFTablePtr = ^AudioFileFDFTable;
	(* API_DEPRECATED("no longer supported", macos(10.4, 10.7)) API_UNAVAILABLE(ios, watchos, tvos); *)

type
	AudioFileFDFTableExtended = record
		mComponentStorage: UnivPtr;
	 	mReadBytesFDF: ReadBytesFDF;
		mWriteBytesFDF: WriteBytesFDF;
		mReadPacketsFDF: ReadPacketsFDF;
		mWritePacketsFDF: WritePacketsFDF;

		mGetPropertyInfoFDF: GetPropertyInfoFDF;
		mGetPropertyFDF: GetPropertyFDF;
		mSetPropertyFDF: SetPropertyFDF;
	
		mCountUserDataFDF: CountUserDataFDF;
		mGetUserDataSizeFDF: GetUserDataSizeFDF;
		mGetUserDataFDF: GetUserDataFDF;
		mSetUserDataFDF: SetUserDataFDF;

		mReadPacketDataFDF: ReadPacketDataFDF;
	end;
	AudioFileFDFTableExtendedPtr = ^AudioFileFDFTableExtended;
	(* API_DEPRECATED("no longer supported", macos(10.4, 10.7)) API_UNAVAILABLE(ios, watchos, tvos);*)

{!
	@functiongroup Deprecated AFComponent
	@discussion		These API calls are no longer called on Snow Leopard, instead the URL versions are used.
					They can be provided by the file component for compatibility with Leopard and Tiger systems
}

{!
    @function	AudioFileComponentCreate
    @abstract   implements AudioFileCreate
    @param inComponent		an AudioFileComponent
    @param inParentRef		an FSRef to the directory where  the new file should be created.
    @param inFileName		a CFStringRef containing the name of the file to be created.
    @param inFormat			an AudioStreamBasicDescription describing the data format that will be
							added to the audio file.
    @param inFlags			relevant flags for creating/opening the file. Currently zero.
    @param outNewFileRef	if successful, the FSRef of the newly created file.
    @result					returns noErr if successful.
}
function AudioFileComponentCreate( inComponent: AudioFileComponent; {const} inParentRef: FSRefPtr; inFileName: CFStringRef; const var inFormat: AudioStreamBasicDescription; inFlags: UInt32; outNewFileRef: FSRefPtr ): OSStatus;
API_DEPRECATED("no longer supported", macos(10.4, 10.6)) API_UNAVAILABLE(ios, watchos, tvos);
                                

{!
    @function	AudioFileComponentInitialize
    @abstract   implements AudioFileInitialize
    @param inComponent		an AudioFileComponent
    @param inFileRef		the FSRef of an existing audio file.
    @param inFormat			an AudioStreamBasicDescription describing the data format that will be
							added to the audio file.
    @param inFlags			flags for creating/opening the file. Currently zero.
    @result					returns noErr if successful.
}
function AudioFileComponentInitialize( inComponent: AudioFileComponent; {const} inFileRef: FSRefPtr; const var inFormat: AudioStreamBasicDescription; inFlags: UInt32 ): OSStatus;
API_DEPRECATED("no longer supported", macos(10.4, 10.6)) API_UNAVAILABLE(ios, watchos, tvos);
							
{!
    @function	AudioFileComponentOpenFile
    @abstract   implements AudioFileOpen
    @param inComponent		an AudioFileComponent
    @param inFileRef		the FSRef of an existing audio file.
    @param inPermissions	use the permission constants
    @param inRefNum			the file refNum for the opened file. This avoids opening the file twice
							- once to determine which file type to which to delegate and once to parse it.
    @result					returns noErr if successful.
}
function AudioFileComponentOpenFile( inComponent: AudioFileComponent; {const} inFileRef: FSRefPtr; inPermissions: SInt8; inRefNum: SInt16 ): OSStatus;
API_DEPRECATED("no longer supported", macos(10.4, 10.6)) API_UNAVAILABLE(ios, watchos, tvos);


//=====================================================================================================================

type
	AudioFileComponentCreateURLProc = function( self: univ Ptr; inFileRef: CFURLRef; const var inFormat: AudioStreamBasicDescription; inFlags: UInt32 ): OSStatus;
	AudioFileComponentOpenURLProc = function( self: univ Ptr; inFileRef: CFURLRef; inPermissions: SInt8; inFileDescriptor: SInt32 ): OSStatus;
								
type
	AudioFileComponentOpenWithCallbacksProc = function( self: univ Ptr; inClientData: univ Ptr; inReadFunc: AudioFile_ReadProc; inWriteFunc: AudioFile_WriteProc; inGetSizeFunc: AudioFile_GetSizeProc; inSetSizeFunc: AudioFile_SetSizeProc ): OSStatus;

type
	AudioFileComponentInitializeWithCallbacksProc = function( self: univ Ptr; inClientData: univ Ptr; inReadFunc: AudioFile_ReadProc; inWriteFunc: AudioFile_WriteProc; inGetSizeFunc: AudioFile_GetSizeProc; inSetSizeFunc: AudioFile_SetSizeProc; inFileType: UInt32; const var inFormat: AudioStreamBasicDescription; inFlags: UInt32 ): OSStatus;

type
	AudioFileComponentCloseProc = function( self: univ Ptr ): OSStatus;
				
type
	AudioFileComponentOptimizeProc = function( self: univ Ptr ): OSStatus;
				
type
	AudioFileComponentReadBytesProc = function( self: univ Ptr; inUseCache: Boolean; inStartingByte: SInt64; var ioNumBytes: UInt32; outBuffer: univ Ptr ): OSStatus;	
						
type
	AudioFileComponentWriteBytesProc = function( self: univ Ptr; inUseCache: Boolean; inStartingByte: SInt64; var ioNumBytes: UInt32; inBuffer: {const} univ Ptr ): OSStatus;	
						
type
	AudioFileComponentReadPacketsProc = function( self: univ Ptr; inUseCache: Boolean; var outNumBytes: UInt32; outPacketDescriptions: AudioStreamPacketDescriptionPtr {* __nullable}; inStartingPacket: SInt64; var ioNumPackets: UInt32; outBuffer: univ Ptr ): OSStatus;	
									

type
	AudioFileComponentReadPacketDataProc = function( self: univ Ptr; inUseCache: Boolean; var ioNumBytes: UInt32; outPacketDescriptions: AudioStreamPacketDescription {* __nullable}; inStartingPacket: SInt64; var ioNumPackets: UInt32; outBuffer: univ Ptr ): OSStatus;	
									
type
	AudioFileComponentWritePacketsProc = function( self: univ Ptr; inUseCache: Boolean; inNumBytes: UInt32; {const} inPacketDescriptions: AudioStreamPacketDescription {* __nullable}; inStartingPacket: SInt64; var ioNumPackets: UInt32; inBuffer: {const} univ Ptr ): OSStatus;


type
	AudioFileComponentGetPropertyInfoProc = function( self: univ Ptr; inPropertyID: AudioFileComponentPropertyID; outPropertySize: UInt32Ptr {* __nullable}; outWritable: UInt32Ptr {* __nullable} ): OSStatus;


type
	AudioFileComponentGetPropertyProc = function( self: univ Ptr; inPropertyID: AudioFileComponentPropertyID; var ioPropertyDataSize: UInt32; outPropertyData: univ Ptr ): OSStatus;

type
	AudioFileComponentSetPropertyProc = function( self: univ Ptr; inPropertyID: AudioFileComponentPropertyID; inPropertyDataSize: UInt32; inPropertyData: {const} univ Ptr ): OSStatus;


type
	AudioFileComponentCountUserDataProc = function( self: univ Ptr; inUserDataID: UInt32; var outNumberItems: UInt32 ): OSStatus;

type
	AudioFileComponentGetUserDataSizeProc = function( self: univ Ptr; inUserDataID: UInt32; inIndex: UInt32; var outUserDataSize: UInt32 ): OSStatus;

type
	AudioFileComponentGetUserDataProc = function( self: univ Ptr; inUserDataID: UInt32; inIndex: UInt32; var ioUserDataSize: UInt32; outUserData: univ Ptr ): OSStatus;

type
	AudioFileComponentSetUserDataProc = function( self: univ Ptr; inUserDataID: UInt32; inIndex: UInt32; inUserDataSize: UInt32; inUserData: {const} univ Ptr ): OSStatus;


type
	AudioFileComponentRemoveUserDataProc = function( self: univ Ptr; inUserDataID: UInt32; inIndex: UInt32 ): OSStatus;

type
	AudioFileComponentExtensionIsThisFormatProc = function( self: univ Ptr; inExtension: CFStringRef; var outResult: UInt32 ): OSStatus;	


type
	AudioFileComponentFileDataIsThisFormatProc = function( self: univ Ptr; inDataByteSize: UInt32; inData: {const} univ Ptr; var outResult: UInt32 ): OSStatus;	


type
	AudioFileComponentGetGlobalInfoSizeProc = function( self: univ Ptr; inPropertyID: AudioFileComponentPropertyID; inSpecifierSize: UInt32; {const} inSpecifier: univ Ptr {__nullable}; var outPropertySize: UInt32 ): OSStatus;

type
	AudioFileComponentGetGlobalInfoProc = function( self: univ Ptr; inPropertyID: AudioFileComponentPropertyID; inSpecifierSize: UInt32; {const} inSpecifier: univ Ptr {__nullable}; var ioPropertyDataSize: UInt32; outPropertyData: univ Ptr ): OSStatus;

{$endc} {TARGET_OS_MAC}
//CF_ASSUME_NONNULL_END


end.
