{
 * Copyright (c) 1998-2009 Apple Inc. All Rights Reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 }
{  Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
unit DASession;
interface
uses MacTypes,CFBase,CFRunLoop,CFString;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{!
 * @typedef   DASessionRef
 * Type of a reference to DASession instances.
 }

type
	DASessionRef = ^SInt32; { an opaque type }

{!
 * @function   DASessionGetTypeID
 * @abstract   Returns the type identifier of all DASession instances.
 }

function DASessionGetTypeID: CFTypeID;

{!
 * @function   DASessionCreate
 * @abstract   Creates a new session.
 * @result     A reference to a new DASession.
 * @discussion
 * The caller of this function receives a reference to the returned object.  The
 * caller also implicitly retains the object and is responsible for releasing it.
 }

function DASessionCreate( allocator: CFAllocatorRef ): DASessionRef;

{!
 * @function   DASessionScheduleWithRunLoop
 * @abstract   Schedules the session on a run loop.
 * @param      session     The session which is being scheduled.
 * @param      runLoop     The run loop on which the session should be scheduled.
 * @param      runLoopMode The run loop mode in which the session should be scheduled.
 }

procedure DASessionScheduleWithRunLoop( session: DASessionRef; runLoop: CFRunLoopRef; runLoopMode: CFStringRef );

{!
 * @function   DASessionUnscheduleFromRunLoop
 * @abstract   Unschedules the session from a run loop.
 * @param      session     The session which is being unscheduled.
 * @param      runLoop     The run loop on which the session is scheduled.
 * @param      runLoopMode The run loop mode in which the session is scheduled.
 }

procedure DASessionUnscheduleFromRunLoop( session: DASessionRef; runLoop: CFRunLoopRef; runLoopMode: CFStringRef );

{!
 * @typedef   DAApprovalSessionRef
 * Type of a reference to DAApprovalSession instances.
 }

type
	DAApprovalSessionRef = ^SInt32; { an opaque type }

{!
 * @function   DAApprovalSessionGetTypeID
 * @abstract   Returns the type identifier of all DAApprovalSession instances.
 }

function DAApprovalSessionGetTypeID: CFTypeID;

{!
 * @function   DAApprovalSessionCreate
 * @abstract   Creates a new approval session.
 * @result     A reference to a new DAApprovalSession.
 * @discussion
 * The caller of this function receives a reference to the returned object.  The
 * caller also implicitly retains the object and is responsible for releasing it.
 }

function DAApprovalSessionCreate( allocator: CFAllocatorRef ): DAApprovalSessionRef;

{!
 * @function   DAApprovalSessionScheduleWithRunLoop
 * @abstract   Schedules the approval session on a run loop.
 * @param      session     The approval session which is being scheduled.
 * @param      runLoop     The run loop on which the approval session should be scheduled.
 * @param      runLoopMode The run loop mode in which the approval session should be scheduled.
 }

procedure DAApprovalSessionScheduleWithRunLoop( session: DAApprovalSessionRef; runLoop: CFRunLoopRef; runLoopMode: CFStringRef );

{!
 * @function   DAApprovalSessionUnscheduleFromRunLoop
 * @abstract   Unschedules the approval session from a run loop.
 * @param      session     The approval session which is being unscheduled.
 * @param      runLoop     The run loop on which the approval session is scheduled.
 * @param      runLoopMode The run loop mode in which the approval session is scheduled.
 }

procedure DAApprovalSessionUnscheduleFromRunLoop( session: DAApprovalSessionRef; runLoop: CFRunLoopRef; runLoopMode: CFStringRef );

{$endc} {TARGET_OS_MAC}

end.
