{
     File:       QuickTime/QuickTimeVRFormat.h
 
     Contains:   QuickTime VR interfaces
 
     Version:    QuickTime 7.7.1
 
     Copyright:  � 1997-2012 by Apple Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit QuickTimeVRFormat;
interface
uses MacTypes,Movies,QuickTimeVR;


{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{ QuickTime is not available to 64-bit clients }

{$ifc not TARGET_CPU_64}


{ File Format Version numbers }
const
	kQTVRMajorVersion = 2;
const
	kQTVRMinorVersion = 0;

{ User data type for the Movie Controller type specifier}
const
	kQTControllerType = kQTVRControllerSubType; { Atom & ID of where our}
	kQTControllerID = 1;     { ...controller name is stored}

{ VRWorld atom types}
const
	kQTVRWorldHeaderAtomType = FOUR_CHAR_CODE('vrsc');
	kQTVRImagingParentAtomType = FOUR_CHAR_CODE('imgp');
	kQTVRPanoImagingAtomType = FOUR_CHAR_CODE('impn');
	kQTVRObjectImagingAtomType = FOUR_CHAR_CODE('imob');
	kQTVRNodeParentAtomType = FOUR_CHAR_CODE('vrnp');
	kQTVRNodeIDAtomType = FOUR_CHAR_CODE('vrni');
	kQTVRNodeLocationAtomType = FOUR_CHAR_CODE('nloc');
	kQTVRCursorParentAtomType = FOUR_CHAR_CODE('vrcp'); { New with 2.1}
	kQTVRCursorAtomType = FOUR_CHAR_CODE('CURS'); { New with 2.1}
	kQTVRColorCursorAtomType = FOUR_CHAR_CODE('crsr'); { New with 2.1}

{ NodeInfo atom types}
const
	kQTVRNodeHeaderAtomType = FOUR_CHAR_CODE('ndhd');
	kQTVRHotSpotParentAtomType = FOUR_CHAR_CODE('hspa');
	kQTVRHotSpotAtomType = FOUR_CHAR_CODE('hots');
	kQTVRHotSpotInfoAtomType = FOUR_CHAR_CODE('hsin');
	kQTVRLinkInfoAtomType = FOUR_CHAR_CODE('link');

{ Miscellaneous atom types}
const
	kQTVRStringAtomType = FOUR_CHAR_CODE('vrsg');
	kQTVRStringEncodingAtomType = FOUR_CHAR_CODE('vrse'); { New with 2.1}
	kQTVRPanoSampleDataAtomType = FOUR_CHAR_CODE('pdat');
	kQTVRObjectInfoAtomType = FOUR_CHAR_CODE('obji');
	kQTVRImageTrackRefAtomType = FOUR_CHAR_CODE('imtr'); { Parent is kQTVRObjectInfoAtomType. Required if track ref is not 1 as required by 2.0 format.}
	kQTVRHotSpotTrackRefAtomType = FOUR_CHAR_CODE('hstr'); { Parent is kQTVRObjectInfoAtomType. Required if track ref is not 1 as required by 2.0 format.}
	kQTVRAngleRangeAtomType = FOUR_CHAR_CODE('arng');
	kQTVRTrackRefArrayAtomType = FOUR_CHAR_CODE('tref');
	kQTVRPanConstraintAtomType = FOUR_CHAR_CODE('pcon');
	kQTVRTiltConstraintAtomType = FOUR_CHAR_CODE('tcon');
	kQTVRFOVConstraintAtomType = FOUR_CHAR_CODE('fcon');
	kQTVRCubicViewAtomType = FOUR_CHAR_CODE('cuvw'); { New with 5.0}
	kQTVRCubicFaceDataAtomType = FOUR_CHAR_CODE('cufa'); { New with 5.0}

const
	kQTVRObjectInfoAtomID = 1;
	kQTVRObjectImageTrackRefAtomID = 1;   { New with 2.1, it adds a track reference to select between multiple image tracks}
	kQTVRObjectHotSpotTrackRefAtomID = 1;  { New with 2.1, it adds a track reference to select between multiple hotspot tracks}

{ Track reference types}
const
	kQTVRImageTrackRefType = FOUR_CHAR_CODE('imgt');
	kQTVRHotSpotTrackRefType = FOUR_CHAR_CODE('hott');

{ Old hot spot types}
const
	kQTVRHotSpotNavigableType = FOUR_CHAR_CODE('navg');

{ Valid bits used in QTVRLinkHotSpotAtom}
const
	kQTVRValidPan = 1 shl 0;
	kQTVRValidTilt = 1 shl 1;
	kQTVRValidFOV = 1 shl 2;
	kQTVRValidViewCenter = 1 shl 3;


{ Values for flags field in QTVRPanoSampleAtom}
const
	kQTVRPanoFlagHorizontal = 1 shl 0;
	kQTVRPanoFlagLast = 1 shl 31;


{ Values for locationFlags field in QTVRNodeLocationAtom}
const
	kQTVRSameFile = 0;


{$endc} {not TARGET_CPU_64}

{ Header for QTVR track's Sample Description record (vrWorld atom container is appended)}
type
	QTVRSampleDescription = record
		descSize: UInt32;               { total size of the QTVRSampleDescription}
		descType: UInt32;               { must be 'qtvr'}

		reserved1: UInt32;              { must be zero}
		reserved2: UInt16;              { must be zero}
		dataRefIndex: UInt16;           { must be zero}

		data: UInt32;                   { Will be extended to hold vrWorld QTAtomContainer}
	end;
	QTVRSampleDescriptionPtr = ^QTVRSampleDescription;
type
	QTVRSampleDescriptionHandle = ^QTVRSampleDescriptionPtr;

{$ifc not TARGET_CPU_64}

{
  =================================================================================================
   Definitions and structures used in the VRWorld QTAtomContainer
  -------------------------------------------------------------------------------------------------
}

type
	QTVRStringAtom = record
		stringUsage: UInt16;
		stringLength: UInt16;
		theString: packed array [0..3] of UInt8;           { field previously named "string" }
	end;
	QTVRStringAtomPtr = ^QTVRStringAtom;

type
	QTVRWorldHeaderAtom = record
		majorVersion: UInt16;
		minorVersion: UInt16;

		nameAtomID: QTAtomID;
		defaultNodeID: UInt32;
		vrWorldFlags: UInt32;

		reserved1: UInt32;
		reserved2: UInt32;
	end;
	QTVRWorldHeaderAtomPtr = ^QTVRWorldHeaderAtom;

{ Valid bits used in QTVRPanoImagingAtom}
const
	kQTVRValidCorrection = 1 shl 0;
	kQTVRValidQuality = 1 shl 1;
	kQTVRValidDirectDraw = 1 shl 2;
	kQTVRValidFirstExtraProperty = 1 shl 3;

type
	QTVRPanoImagingAtom = record
		majorVersion: UInt16;
		minorVersion: UInt16;

		imagingMode: UInt32;
		imagingValidFlags: UInt32;

		correction: UInt32;
		quality: UInt32;
		directDraw: UInt32;
		imagingProperties: array [0..5] of UInt32;   { for future properties}

		reserved1: UInt32;
		reserved2: UInt32;
	end;
	QTVRPanoImagingAtomPtr = ^QTVRPanoImagingAtom;
type
	QTVRNodeLocationAtom = record
		majorVersion: UInt16;
		minorVersion: UInt16;

		nodeType: OSType;
		locationFlags: UInt32;
		locationData: UInt32;

		reserved1: UInt32;
		reserved2: UInt32;
	end;
	QTVRNodeLocationAtomPtr = ^QTVRNodeLocationAtom;
{
  =================================================================================================
   Definitions and structures used in the Nodeinfo QTAtomContainer
  -------------------------------------------------------------------------------------------------
}

type
	QTVRNodeHeaderAtom = record
		majorVersion: UInt16;
		minorVersion: UInt16;

		nodeType: OSType;
		nodeID: QTAtomID;
		nameAtomID: QTAtomID;
		commentAtomID: QTAtomID;

		reserved1: UInt32;
		reserved2: UInt32;
	end;
	QTVRNodeHeaderAtomPtr = ^QTVRNodeHeaderAtom;
type
	QTVRAngleRangeAtom = record
		minimumAngle: Float32;
		maximumAngle: Float32;
	end;
	QTVRAngleRangeAtomPtr = ^QTVRAngleRangeAtom;
type
	QTVRHotSpotInfoAtom = record
		majorVersion: UInt16;
		minorVersion: UInt16;

		hotSpotType: OSType;
		nameAtomID: QTAtomID;
		commentAtomID: QTAtomID;

		cursorID: array [0..2] of SInt32;

                                              { canonical view for this hot spot}
		bestPan: Float32;
		bestTilt: Float32;
		bestFOV: Float32;
		bestViewCenter: QTVRFloatPoint;

                                              { Bounding box for this hot spot}
		hotSpotRect: Rect;

		flags: UInt32;
		reserved1: UInt32;
		reserved2: UInt32;
	end;
	QTVRHotSpotInfoAtomPtr = ^QTVRHotSpotInfoAtom;
type
	QTVRLinkHotSpotAtom = record
		majorVersion: UInt16;
		minorVersion: UInt16;

		toNodeID: UInt32;

		fromValidFlags: UInt32;
		fromPan: Float32;
		fromTilt: Float32;
		fromFOV: Float32;
		fromViewCenter: QTVRFloatPoint;

		toValidFlags: UInt32;
		toPan: Float32;
		toTilt: Float32;
		toFOV: Float32;
		toViewCenter: QTVRFloatPoint;

		distance: Float32;

		flags: UInt32;
		reserved1: UInt32;
		reserved2: UInt32;
	end;
	QTVRLinkHotSpotAtomPtr = ^QTVRLinkHotSpotAtom;
{
  =================================================================================================
   Definitions and structures used in Panorama and Object tracks
  -------------------------------------------------------------------------------------------------
}

type
	QTVRPanoSampleAtom = record
		majorVersion: UInt16;
		minorVersion: UInt16;

		imageRefTrackIndex: UInt32;     { track reference index of the full res image track}
		hotSpotRefTrackIndex: UInt32;   { track reference index of the full res hot spot track}

		minPan: Float32;
		maxPan: Float32;
		minTilt: Float32;
		maxTilt: Float32;
		minFieldOfView: Float32;
		maxFieldOfView: Float32;

		defaultPan: Float32;
		defaultTilt: Float32;
		defaultFieldOfView: Float32;

                                              { Info for highest res version of image track}
		imageSizeX: UInt32;             { pixel width of the panorama (e.g. 768)}
		imageSizeY: UInt32;             { pixel height of the panorama (e.g. 2496)}
		imageNumFramesX: UInt16;        { diced frames wide (e.g. 1)}
		imageNumFramesY: UInt16;        { diced frames high (e.g. 24)}

                                              { Info for highest res version of hotSpot track}
		hotSpotSizeX: UInt32;           { pixel width of the hot spot panorama (e.g. 768)}
		hotSpotSizeY: UInt32;           { pixel height of the hot spot panorama (e.g. 2496)}
		hotSpotNumFramesX: UInt16;      { diced frames wide (e.g. 1)}
		hotSpotNumFramesY: UInt16;      { diced frames high (e.g. 24)}

		flags: UInt32;
		panoType: OSType;
		reserved2: UInt32;
	end;
	QTVRPanoSampleAtomPtr = ^QTVRPanoSampleAtom;
{
   View atom for cubes (since same fields in QTVRPanoSampleAtom are set to special
   values for backwards compatibility and hence are ignored by the cubic engine)
}
type
	QTVRCubicViewAtom = record
		minPan: Float32;
		maxPan: Float32;
		minTilt: Float32;
		maxTilt: Float32;
		minFieldOfView: Float32;
		maxFieldOfView: Float32;

		defaultPan: Float32;
		defaultTilt: Float32;
		defaultFieldOfView: Float32;
	end;
	QTVRCubicViewAtomPtr = ^QTVRCubicViewAtom;
type
	QTVRCubicFaceData = record
		orientation: array [0..4-1] of Float32;         { WXYZ quaternion of absolute orientation}
		center: array [0..2-1] of Float32;              { Center of image relative to center of projection (default = (0,0)) in normalized units}
		aspect: Float32;                 { aspect>1 => tall pixels; aspect <1 => squat pixels (default = 1)}
		skew: Float32;                   { skew x by y (default = 0)}
	end;
	QTVRCubicFaceDataPtr = ^QTVRCubicFaceData;
{ Special resolution values for the Image Track Reference Atoms. Use only one value per track reference.}
const
	kQTVRFullTrackRes = kQTVRFullRes;
	kQTVRHalfTrackRes = kQTVRHalfRes;
	kQTVRQuarterTrackRes = kQTVRQuarterRes;
	kQTVRPreviewTrackRes = $8000;

type
	QTVRTrackRefEntryPtr = ^QTVRTrackRefEntry;
	QTVRTrackRefEntry = record
		trackRefType: UInt32;
		trackResolution: UInt16;
		trackRefIndex: UInt32;
	end;
{
  =================================================================================================
   Object File format 2.0
  -------------------------------------------------------------------------------------------------
}
const
	kQTVRObjectAnimateViewFramesOn = 1 shl 0;
	kQTVRObjectPalindromeViewFramesOn = 1 shl 1;
	kQTVRObjectStartFirstViewFrameOn = 1 shl 2;
	kQTVRObjectAnimateViewsOn = 1 shl 3;
	kQTVRObjectPalindromeViewsOn = 1 shl 4;
	kQTVRObjectSyncViewToFrameRate = 1 shl 5;
	kQTVRObjectDontLoopViewFramesOn = 1 shl 6;
	kQTVRObjectPlayEveryViewFrameOn = 1 shl 7;
	kQTVRObjectStreamingViewsOn = 1 shl 8;

const
	kQTVRObjectWrapPanOn = 1 shl 0;
	kQTVRObjectWrapTiltOn = 1 shl 1;
	kQTVRObjectCanZoomOn = 1 shl 2;
	kQTVRObjectReverseHControlOn = 1 shl 3;
	kQTVRObjectReverseVControlOn = 1 shl 4;
	kQTVRObjectSwapHVControlOn = 1 shl 5;
	kQTVRObjectTranslationOn = 1 shl 6;

const
	kGrabberScrollerUI = 1;    { "Object" }
	kOldJoyStickUI = 2;    {  "1.0 Object as Scene"     }
	kJoystickUI = 3;    { "Object In Scene"}
	kGrabberUI = 4;    { "Grabber only"}
	kAbsoluteUI = 5;     { "Absolute pointer"}


type
	QTVRObjectSampleAtom = record
		majorVersion: UInt16;           { kQTVRMajorVersion}
		minorVersion: UInt16;           { kQTVRMinorVersion}
		movieType: UInt16;              { ObjectUITypes}
		viewStateCount: UInt16;         { The number of view states 1 based}
		defaultViewState: UInt16;       { The default view state number. The number must be 1 to viewStateCount}
		mouseDownViewState: UInt16;     { The mouse down view state.   The number must be 1 to viewStateCount}
		viewDuration: UInt32;           { The duration of each view including all animation frames in a view}
		columns: UInt32;                { Number of columns in movie}
		rows: UInt32;                   { Number rows in movie}
		mouseMotionScale: Float32;       { 180.0 for kStandardObject or kQTVRObjectInScene, actual degrees for kOldNavigableMovieScene.}
		minPan: Float32;                 { Start   horizontal pan angle in degrees}
		maxPan: Float32;                 { End     horizontal pan angle in degrees}
		defaultPan: Float32;             { Initial horizontal pan angle in degrees (poster view)}
		minTilt: Float32;                { Start   vertical   pan angle in degrees}
		maxTilt: Float32;                { End     vertical   pan angle in degrees}
		defaultTilt: Float32;            { Initial vertical   pan angle in degrees (poster view)  }
		minFieldOfView: Float32;         { minimum field of view setting (appears as the maximum zoom effect) must be >= 1}
		fieldOfView: Float32;            { the field of view range must be >= 1}
		defaultFieldOfView: Float32;     { must be in minFieldOfView and maxFieldOfView range inclusive}
		defaultViewCenterH: Float32;
		defaultViewCenterV: Float32;

		viewRate: Float32;
		frameRate: Float32;
		animationSettings: UInt32;      { 32 reserved bit fields}
		controlSettings: UInt32;        { 32 reserved bit fields}
	end;
	QTVRObjectSampleAtomPtr = ^QTVRObjectSampleAtom;
{
  =================================================================================================
   QuickTime VR Authoring Components
  -------------------------------------------------------------------------------------------------
}

{
   ComponentDescription constants for QTVR Export components   
    (componentType = MovieExportType; componentSubType = MovieFileType)
}
const
	kQTVRFlattenerManufacturer = FOUR_CHAR_CODE('vrwe'); { aka QTVRFlattenerType}
	kQTVRSplitterManufacturer = FOUR_CHAR_CODE('vrsp');
	kQTVRObjExporterManufacturer = FOUR_CHAR_CODE('vrob');

{ QuickTime VR Flattener atom types}
const
	kQTVRFlattenerSettingsParentAtomType = FOUR_CHAR_CODE('VRWe'); { parent of settings atoms (other than compression)}
	kQTVRFlattenerPreviewResAtomType = FOUR_CHAR_CODE('PRes'); { preview resolution Int16}
	kQTVRFlattenerImportSpecAtomType = FOUR_CHAR_CODE('ISpe'); { import file spec FSSpec}
	kQTVRFlattenerCreatePreviewAtomType = FOUR_CHAR_CODE('Prev'); { Boolean}
	kQTVRFlattenerImportPreviewAtomType = FOUR_CHAR_CODE('IPre'); { Boolean}
	kQTVRFlattenerBlurPreviewAtomType = FOUR_CHAR_CODE('Blur'); { Boolean}

{ QuickTime VR Splitter atom types}
const
	kQTVRSplitterSettingsParentAtomType = FOUR_CHAR_CODE('VRSp'); { parent of settings atoms (other than compression)}
	kQTVRSplitterGenerateHTMLAtomType = FOUR_CHAR_CODE('Ghtm'); { Boolean}
	kQTVRSplitterOverwriteFilesAtomType = FOUR_CHAR_CODE('Owfi'); { Boolean}
	kQTVRSplitterUseFlattenerAtomType = FOUR_CHAR_CODE('Usef'); { Boolean}
	kQTVRSplitterShowControllerAtomType = FOUR_CHAR_CODE('Shco'); { Boolean}
	kQTVRSplitterTargetMyselfAtomType = FOUR_CHAR_CODE('Tgtm'); { Boolean}

{ QuickTime VR Object Exporter atom types}
const
	kQTVRObjExporterSettingsBlockSize = FOUR_CHAR_CODE('bsiz'); { block size for compression}
	kQTVRObjExporterSettingsTargetSize = FOUR_CHAR_CODE('tsiz'); { target file size}


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}


end.
