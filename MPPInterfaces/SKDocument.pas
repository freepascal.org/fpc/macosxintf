{
     File:       SearchKit/SKDocument.h
 
     Contains:   SearchKit Interfaces.
 
     Version:    SearchKit-407~38
 
     Copyright:  � 2003-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit SKDocument;
interface
uses MacTypes,CFBase,CFURL;
{$ALIGN POWER}

{$ifc TARGET_OS_MAC}

{
 *  SKDocumentRef
 *  
 *  Summary:
 *    An opaque data type representing a document.
 *  
 *  Discussion:
 *    A document reference is a generic descriptor to a document. It is
 *    built from a document scheme, a parent document, and a document
 *    name.
 }
type
	SKDocumentRef = CFTypeRef;
	SKDocumentRefPtr = ^SKDocumentRef;
{
 *  SKDocumentGetTypeID()
 *  
 *  Summary:
 *    Returns the type identifier of the SKDocument type.
 *  
 *  Result:
 *    Returns a CFTypeID object, or <tt>NULL</tt> on failure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKDocumentGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;


{
 *  SKDocumentCreateWithURL()
 *  
 *  Summary:
 *    Creates a reference to a document with a URL.
 *  
 *  Discussion:
 *    Use SKDocumentCreateWithURL to create a reference to a file or
 *    other URL. This function must be balanced with a call at a later
 *    time to CFRelease.
 *  
 *  Parameters:
 *    
 *    inURL:
 *      Only "file:" URLs can be used with the SKIndexAddDocument
 *      function, but the URL scheme may be anything you like if you
 *      use the SKIndexAddDocumentWithText function. The scheme of the
 *      document created is set to the scheme of the URL used.
 *  
 *  Result:
 *    Returns a reference to the document, or <tt>NULL</tt> on failure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKDocumentCreateWithURL( inURL: CFURLRef ): SKDocumentRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;


{
 *  SKDocumentCopyURL()
 *  
 *  Summary:
 *    Builds a CFURL object from a document reference.
 *  
 *  Result:
 *    Returns a CFURL object, or <tt>NULL</tt> on failure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKDocumentCopyURL( inDocument: SKDocumentRef ): CFURLRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;


{
 *  SKDocumentCreate()
 *  
 *  Summary:
 *    Create a document based on a scheme, parent, and name.
 *  
 *  Discussion:
 *    The parent can be <tt>NULL</tt>, but either a scheme or a parent
 *    must be specified. This function must be balanced with a call at
 *    a later time to CFRelease
 *  
 *  Parameters:
 *    
 *    inScheme:
 *      Analogous to the scheme of a URL. Documents with a "file"
 *      scheme can be read by the <tt>SKIndexAddDocument</tt> function
 *      (see SearchKit.h). The scheme may be anything you like if you
 *      use the SKIndexAddDocumentWithText function. If the scheme is
 *      <tt>NULL</tt>, it will be set to be the same as the parent.
 *    
 *    inParent:
 *      The reference to the document or container one step up in the
 *      document hierarchy.
 *    
 *    inName:
 *      The name of this document. For a "file" scheme, it is the name
 *      of the file or the container, not its path. The path can be
 *      constructed by following parent links.
 *  
 *  Result:
 *    Returns a reference to the document, or <tt>NULL</tt> on failure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKDocumentCreate( inScheme: CFStringRef; inParent: SKDocumentRef { can be NULL }; inName: CFStringRef ): SKDocumentRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;


{
 *  SKDocumentGetSchemeName()
 *  
 *  Summary:
 *    Gets the scheme name of a document.
 *  
 *  Parameters:
 *    
 *    inDocument:
 *      The document whose scheme name you want to get.
 *  
 *  Result:
 *    Returns a CFString object, or <tt>NULL</tt> on failure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKDocumentGetSchemeName( inDocument: SKDocumentRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;


{
 *  SKDocumentGetName()
 *  
 *  Summary:
 *    Gets the name of a document.
 *  
 *  Parameters:
 *    
 *    inDocument:
 *      The document whose name you want to get.
 *  
 *  Result:
 *    Returns a CFString object, or <tt>NULL</tt> on failure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKDocumentGetName( inDocument: SKDocumentRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;


{
 *  SKDocumentGetParent()
 *  
 *  Summary:
 *    Gets a reference to the parent document of a document.
 *  
 *  Parameters:
 *    
 *    inDocument:
 *      The document whose parent you want to get.
 *  
 *  Result:
 *    Returns a reference to the parent document, or <tt>NULL</tt> on
 *    failure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SKDocumentGetParent( inDocument: SKDocumentRef ): SKDocumentRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

{$endc} {TARGET_OS_MAC}

end.
