{
 * Copyright (c) 1998-2009 Apple Inc. All Rights Reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 }
{  Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
unit DADisk;
interface
uses MacTypes,DASession,CFBase,CFDictionary,CFString;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


const kDADiskDescriptionVolumeKindKey: CFStringRef;      { ( CFString     ) }
const kDADiskDescriptionVolumeMountableKey: CFStringRef; { ( CFBoolean    ) }
const kDADiskDescriptionVolumeNameKey: CFStringRef;      { ( CFString     ) }
const kDADiskDescriptionVolumeNetworkKey: CFStringRef;   { ( CFBoolean    ) }
const kDADiskDescriptionVolumePathKey: CFStringRef;      { ( CFURL        ) }
const kDADiskDescriptionVolumeUUIDKey: CFStringRef;      { ( CFUUID       ) }

const kDADiskDescriptionMediaBlockSizeKey: CFStringRef;  { ( CFNumber     ) }
const kDADiskDescriptionMediaBSDMajorKey: CFStringRef;   { ( CFNumber     ) }
const kDADiskDescriptionMediaBSDMinorKey: CFStringRef;   { ( CFNumber     ) }
const kDADiskDescriptionMediaBSDNameKey: CFStringRef;    { ( CFString     ) }
const kDADiskDescriptionMediaBSDUnitKey: CFStringRef;    { ( CFNumber     ) }
const kDADiskDescriptionMediaContentKey: CFStringRef;    { ( CFString     ) }
const kDADiskDescriptionMediaEjectableKey: CFStringRef;  { ( CFBoolean    ) }
const kDADiskDescriptionMediaIconKey: CFStringRef;       { ( CFDictionary ) }
const kDADiskDescriptionMediaKindKey: CFStringRef;       { ( CFString     ) }
const kDADiskDescriptionMediaLeafKey: CFStringRef;       { ( CFBoolean    ) }
const kDADiskDescriptionMediaNameKey: CFStringRef;       { ( CFString     ) }
const kDADiskDescriptionMediaPathKey: CFStringRef;       { ( CFString     ) }
const kDADiskDescriptionMediaRemovableKey: CFStringRef;  { ( CFBoolean    ) }
const kDADiskDescriptionMediaSizeKey: CFStringRef;       { ( CFNumber     ) }
const kDADiskDescriptionMediaTypeKey: CFStringRef;       { ( CFString     ) }
const kDADiskDescriptionMediaUUIDKey: CFStringRef;       { ( CFUUID       ) }
const kDADiskDescriptionMediaWholeKey: CFStringRef;      { ( CFBoolean    ) }
const kDADiskDescriptionMediaWritableKey: CFStringRef;   { ( CFBoolean    ) }

const kDADiskDescriptionDeviceGUIDKey: CFStringRef;      { ( CFData       ) }
const kDADiskDescriptionDeviceInternalKey: CFStringRef;  { ( CFBoolean    ) }
const kDADiskDescriptionDeviceModelKey: CFStringRef;     { ( CFString     ) }
const kDADiskDescriptionDevicePathKey: CFStringRef;      { ( CFString     ) }
const kDADiskDescriptionDeviceProtocolKey: CFStringRef;  { ( CFString     ) }
const kDADiskDescriptionDeviceRevisionKey: CFStringRef;  { ( CFString     ) }
const kDADiskDescriptionDeviceUnitKey: CFStringRef;      { ( CFNumber     ) }
const kDADiskDescriptionDeviceVendorKey: CFStringRef;    { ( CFString     ) }

const kDADiskDescriptionBusNameKey: CFStringRef;         { ( CFString     ) }
const kDADiskDescriptionBusPathKey: CFStringRef;         { ( CFString     ) }


{!
 * @typedef    DADiskRef
 * Type of a reference to DADisk instances.
 }

type
	DADiskRef = ^SInt32; { an opaque type }

{!
 * @function   DADiskGetTypeID
 * @abstract   Returns the type identifier of all DADisk instances.
 }

function DADiskGetTypeID: CFTypeID;

{!
 * @function   DADiskCreateFromBSDName
 * @abstract   Creates a new disk object.
 * @param      allocator The allocator object to be used to allocate memory.
 * @param      session   The DASession in which to contact Disk Arbitration.
 * @param      name      The BSD device name.
 * @result     A reference to a new DADisk.
 * @discussion
 * The caller of this function receives a reference to the returned object.  The
 * caller also implicitly retains the object and is responsible for releasing it
 * with CFRelease().
 }

function DADiskCreateFromBSDName( allocator: CFAllocatorRef; session: DASessionRef; name: ConstCStringPtr ): DADiskRef;

(*
Requires IOKit translation

{!
 * @function   DADiskCreateFromIOMedia
 * @abstract   Creates a new disk object.
 * @param      allocator The allocator object to be used to allocate memory.
 * @param      session   The DASession in which to contact Disk Arbitration.
 * @param      media     The I/O Kit media object.
 * @result     A reference to a new DADisk.
 * @discussion
 * The caller of this function receives a reference to the returned object.  The
 * caller also implicitly retains the object and is responsible for releasing it
 * with CFRelease().
 }

function DADiskCreateFromIOMedia( allocator: CFAllocatorRef; session: DASessionRef; media: io_service_t ): DADiskRef;
*)

{!
 * @function   DADiskGetBSDName
 * @abstract   Obtains the BSD device name for the specified disk.
 * @param      disk The DADisk for which to obtain the BSD device name.
 * @result     The disk's BSD device name.
 * @discussion
 * The BSD device name can be used with opendev() to open the BSD device.
 }

function DADiskGetBSDName( disk: DADiskRef ): CStringPtr;

(*
Requires IOKit translation

{!
 * @function   DADiskCopyIOMedia
 * @abstract   Obtains the I/O Kit media object for the specified disk.
 * @param      disk The DADisk for which to obtain the I/O Kit media object.
 * @result     The disk's I/O Kit media object.
 * @discussion
 * The caller of this function receives a reference to the returned object.  The
 * caller also implicitly retains the object and is responsible for releasing it
 * with IOObjectRelease().
 }

function DADiskCopyIOMedia( disk: DADiskRef ): io_service_t;
*)

{!
 * @function   DADiskCopyDescription
 * @abstract   Obtains the Disk Arbitration description of the specified disk.
 * @param      disk The DADisk for which to obtain the Disk Arbitration description.
 * @result     The disk's Disk Arbitration description.
 * @discussion
 * This function will contact Disk Arbitration to acquire the latest description
 * of the specified disk, unless this function is called on a disk object passed
 * within the context of a registered callback, in which case the description is
 * current as of that callback event.
 *
 * The caller of this function receives a reference to the returned object.  The
 * caller also implicitly retains the object and is responsible for releasing it
 * with CFRelease().
 }

function DADiskCopyDescription( disk: DADiskRef ): CFDictionaryRef;

{!
 * @function   DADiskCopyWholeDisk
 * @abstract   Obtain the associated whole disk object for the specified disk.
 * @param      disk The disk object.
 * @result     The disk's associated whole disk object.
 * @discussion
 * The caller of this function receives a reference to the returned object.  The
 * caller also implicitly retains the object and is responsible for releasing it
 * with CFRelease().
 }

function DADiskCopyWholeDisk( disk: DADiskRef ): DADiskRef;

{$endc} {TARGET_OS_MAC}

end.
