{ CoreGraphics - CGDataConsumer.h
 * Copyright (c) 1999-2004 Apple Computer, Inc.
 * All rights reserved.
 }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGDataConsumer;
interface
uses MacTypes,CFBase,CFData,CGBase,CFURL;
{$ALIGN POWER}


type
	CGDataConsumerRef = ^OpaqueCGDataConsumerRef; { an opaque type }
	OpaqueCGDataConsumerRef = record end;


{ This callback is called to copy `count' bytes from `buffer' to the data
   consumer. }

type
	CGDataConsumerPutBytesCallback = function( info: univ Ptr; buffer: {const} univ Ptr; count: size_t ): size_t;

{ This callback is called to release the `info' pointer when the data
   provider is freed. }

type
	CGDataConsumerReleaseInfoCallback = procedure( info: univ Ptr );

{ Callbacks for writing data.
   `putBytes' copies `count' bytes from `buffer' to the consumer, and
     returns the number of bytes copied. It should return 0 if no more data
     can be written to the consumer.
   `releaseConsumer', if non-NULL, is called when the consumer is freed. }

type
	CGDataConsumerCallbacks = record
		putBytes: CGDataConsumerPutBytesCallback;
		releaseConsumer: CGDataConsumerReleaseInfoCallback;
	end;

{ Return the CFTypeID for CGDataConsumerRefs. }

function CGDataConsumerGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Create a data consumer using `callbacks' to handle the data. `info' is
   passed to each of the callback functions. }

function CGDataConsumerCreate( info: univ Ptr; const var callbacks: CGDataConsumerCallbacks ): CGDataConsumerRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create a data consumer which writes data to `url'. }

function CGDataConsumerCreateWithURL( url: CFURLRef ): CGDataConsumerRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create a data consumer which writes to `data'. }

function CGDataConsumerCreateWithCFData( data: CFMutableDataRef ): CGDataConsumerRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Equivalent to `CFRetain(consumer)'. }

function CGDataConsumerRetain( consumer: CGDataConsumerRef ): CGDataConsumerRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRelease(consumer)'. }

procedure CGDataConsumerRelease( consumer: CGDataConsumerRef );
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);


end.
