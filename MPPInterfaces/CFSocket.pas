{	CFSocket.h
	Copyright (c) 1999-2013, Apple Inc.  All rights reserved.
}
unit CFSocket;
interface
uses MacTypes,CFBase,CFData,CFString,CFRunLoop,CFDate;
{$ALIGN POWER}


type
	CFSocketNativeHandle = SInt32;


type
	CFSocketRef = ^__CFSocket; { an opaque type }
	__CFSocket = record end;

{ A CFSocket contains a native socket within a structure that can 
be used to read from the socket in the background and make the data
thus read available using a runloop source.  The callback used for
this may be of three types, as specified by the callBackTypes
argument when creating the CFSocket.

If kCFSocketReadCallBack is used, then data will not be
automatically read, but the callback will be called when data
is available to be read, or a new child socket is waiting to be
accepted.
    
If kCFSocketAcceptCallBack is used, then new child sockets will be
accepted and passed to the callback, with the data argument being
a pointer to a CFSocketNativeHandle.  This is usable only with
connection rendezvous sockets.

If kCFSocketDataCallBack is used, then data will be read in chunks
in the background and passed to the callback, with the data argument
being a CFDataRef.

These three types are mutually exclusive, but any one of them may
have kCFSocketConnectCallBack added to it, if the socket will be
used to connect in the background.  Connect in the background occurs
if CFSocketConnectToAddress is called with a negative timeout
value, in which case the call returns immediately, and a
kCFSocketConnectCallBack is generated when the connect finishes.
In this case the data argument is either NULL, or a pointer to
an SInt32 error code if the connect failed.  kCFSocketConnectCallBack
will never be sent more than once for a given socket.

The callback types may also have kCFSocketWriteCallBack added to
them, if large amounts of data are to be sent rapidly over the 
socket and notification is desired when there is space in the
kernel buffers so that the socket is writable again.  

With a connection-oriented socket, if the connection is broken from the
other end, then one final kCFSocketReadCallBack or kCFSocketDataCallBack 
will occur.  In the case of kCFSocketReadCallBack, the underlying socket 
will have 0 bytes available to read.  In the case of kCFSocketDataCallBack,
the data argument will be a CFDataRef of length 0.

There are socket flags that may be set to control whether callbacks of 
a given type are automatically reenabled after they are triggered, and 
whether the underlying native socket will be closed when the CFSocket
is invalidated.  By default read, accept, and data callbacks are 
automatically reenabled; write callbacks are not, and connect callbacks
may not be, since they are sent once only.  Be careful about automatically
reenabling read and write callbacks, since this implies that the 
callbacks will be sent repeatedly if the socket remains readable or
writable respectively.  Be sure to set these flags only for callbacks
that your CFSocket actually possesses; the result of setting them for
other callback types is undefined.

Individual callbacks may also be enabled and disabled manually, whether 
they are automatically reenabled or not.  If they are not automatically 
reenabled, then they will need to be manually reenabled when the callback 
is ready to be received again (and not sooner).  Even if they are 
automatically reenabled, there may be occasions when it will be useful
to be able to manually disable them temporarily and then reenable them.
Be sure to enable and disable only callbacks that your CFSocket actually
possesses; the result of enabling and disabling other callback types is
undefined.

By default the underlying native socket will be closed when the CFSocket 
is invalidated, but it will not be if kCFSocketCloseOnInvalidate is 
turned off.  This can be useful in order to destroy a CFSocket but 
continue to use the underlying native socket.  The CFSocket must 
still be invalidated when it will no longer be used.  Do not in 
either case close the underlying native socket without invalidating 
the CFSocket.

Addresses are stored as CFDatas containing a struct sockaddr
appropriate for the protocol family; make sure that all fields are
filled in properly when passing in an address.  

}

type
	CFSocketError = CFIndex;
const
    kCFSocketSuccess = 0;
    kCFSocketError = -1;
    kCFSocketTimeout = -2;

type
	CFSocketSignature = record
		protocolFamily: SInt32;
		socketType: SInt32;
		protocol: SInt32;
		address: CFDataRef;
	end;

type
	CFSocketCallBackType = CFOptionFlags;
const
    kCFSocketNoCallBack = 0;
    kCFSocketReadCallBack = 1;
    kCFSocketAcceptCallBack = 2;
    kCFSocketDataCallBack = 3;
    kCFSocketConnectCallBack = 4;
{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
    kCFSocketWriteCallBack = 8;
{#endif}

{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
{ Socket flags }
const
	kCFSocketAutomaticallyReenableReadCallBack = 1;
	kCFSocketAutomaticallyReenableAcceptCallBack = 2;
	kCFSocketAutomaticallyReenableDataCallBack = 3;
	kCFSocketAutomaticallyReenableWriteCallBack = 8;
{#if MAC_OS_X_VERSION_10_5 <= MAC_OS_X_VERSION_MAX_ALLOWED}
	kCFSocketLeaveErrors = 64;
{#endif}
	kCFSocketCloseOnInvalidate = 128; CF_AVAILABLE_STARTING(10_5, 2_0);
{#endif}

type
	CFSocketCallBack = procedure( s: CFSocketRef; typ: CFSocketCallBackType; address: CFDataRef; data: {const} univ Ptr; info: univ Ptr );
{ If the callback wishes to keep hold of address or data after the point that it returns, then it must copy them. }

type
	CFSocketContext = record
		version: CFIndex;
		info: UnivPtr;
		retain: function( info: {const} univ Ptr ): UnivPtr;
		release: procedure( info: {const} univ Ptr );
		copyDescription: function( info: {const} univ Ptr ): CFStringRef;
	end;

function CFSocketGetTypeID: CFTypeID;

function CFSocketCreate( allocator: CFAllocatorRef; protocolFamily: SInt32; socketType: SInt32; protocol: SInt32; callBackTypes: CFOptionFlags; callout: CFSocketCallBack; const var context: CFSocketContext ): CFSocketRef;
function CFSocketCreateWithNative( allocator: CFAllocatorRef; sock: CFSocketNativeHandle; callBackTypes: CFOptionFlags; callout: CFSocketCallBack; const var context: CFSocketContext ): CFSocketRef;
function CFSocketCreateWithSocketSignature( allocator: CFAllocatorRef; const var signature: CFSocketSignature; callBackTypes: CFOptionFlags; callout: CFSocketCallBack; const var context: CFSocketContext ): CFSocketRef;
{ CFSocketCreateWithSocketSignature() creates a socket of the requested type and binds its address (using CFSocketSetAddress()) to the requested address.  If this fails, it returns NULL. }
function CFSocketCreateConnectedToSocketSignature( allocator: CFAllocatorRef; const var signature: CFSocketSignature; callBackTypes: CFOptionFlags; callout: CFSocketCallBack; const var context: CFSocketContext; timeout: CFTimeInterval ): CFSocketRef;
{ CFSocketCreateConnectedToSocketSignature() creates a socket suitable for connecting to the requested type and address, and connects it (using CFSocketConnectToAddress()).  If this fails, it returns NULL. }

function CFSocketSetAddress( s: CFSocketRef; address: CFDataRef ): CFSocketError;
function CFSocketConnectToAddress( s: CFSocketRef; address: CFDataRef; timeout: CFTimeInterval ): CFSocketError;
procedure CFSocketInvalidate( s: CFSocketRef );

function CFSocketIsValid( s: CFSocketRef ): Boolean;
function CFSocketCopyAddress( s: CFSocketRef ): CFDataRef;
function CFSocketCopyPeerAddress( s: CFSocketRef ): CFDataRef;
procedure CFSocketGetContext( s: CFSocketRef; var context: CFSocketContext );
function CFSocketGetNative( s: CFSocketRef ): CFSocketNativeHandle;

function CFSocketCreateRunLoopSource( allocator: CFAllocatorRef; s: CFSocketRef; order: CFIndex ): CFRunLoopSourceRef;

{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
function CFSocketGetSocketFlags( s: CFSocketRef ): CFOptionFlags;
procedure CFSocketSetSocketFlags( s: CFSocketRef; flags: CFOptionFlags );
procedure CFSocketDisableCallBacks( s: CFSocketRef; callBackTypes: CFOptionFlags );
procedure CFSocketEnableCallBacks( s: CFSocketRef; callBackTypes: CFOptionFlags );
{#endif}

{ For convenience, a function is provided to send data using the socket with a timeout.  The timeout will be used only if the specified value is positive.  The address should be left NULL if the socket is already connected. }
function CFSocketSendData( s: CFSocketRef; address: CFDataRef; data: CFDataRef; timeout: CFTimeInterval ): CFSocketError;

{ Generic name registry functionality (CFSocketRegisterValue, 
CFSocketCopyRegisteredValue) allows the registration of any property
list type.  Functions specific to CFSockets (CFSocketRegisterSocketData,
CFSocketCopyRegisteredSocketData) register a CFData containing the
components of a socket signature (protocol family, socket type,
protocol, and address).  In each function the nameServerSignature
may be NULL, or any component of it may be 0, to use default values
(TCP, INADDR_LOOPBACK, port as set).  Name registration servers might
not allow registration with other than TCP and INADDR_LOOPBACK.
The actual address of the server responding to a query may be obtained
by using the nameServerAddress argument.  This address, the address
returned by CFSocketCopyRegisteredSocketSignature, and the value
returned by CFSocketCopyRegisteredValue must (if non-null) be released
by the caller.  CFSocketUnregister removes any registration associated
with the specified name.
}

function CFSocketRegisterValue( const var nameServerSignature: CFSocketSignature; timeout: CFTimeInterval; name: CFStringRef; value: CFPropertyListRef ): CFSocketError;
function CFSocketCopyRegisteredValue( const var nameServerSignature: CFSocketSignature; timeout: CFTimeInterval; name: CFStringRef; var value: CFPropertyListRef; var nameServerAddress: CFDataRef ): CFSocketError;

function CFSocketRegisterSocketSignature( const var nameServerSignature: CFSocketSignature; timeout: CFTimeInterval; name: CFStringRef; const var signature: CFSocketSignature ): CFSocketError;
function CFSocketCopyRegisteredSocketSignature( const var nameServerSignature: CFSocketSignature; timeout: CFTimeInterval; name: CFStringRef; var signature: CFSocketSignature; var nameServerAddress: CFDataRef ): CFSocketError;

function CFSocketUnregister( const var nameServerSignature: CFSocketSignature; timeout: CFTimeInterval; name: CFStringRef ): CFSocketError;

procedure CFSocketSetDefaultNameRegistryPortNumber( port: UInt16 );
function CFSocketGetDefaultNameRegistryPortNumber: UInt16;

{ Constants used in name registry server communications }
const kCFSocketCommandKey: CFStringRef;
const kCFSocketNameKey: CFStringRef;
const kCFSocketValueKey: CFStringRef;
const kCFSocketResultKey: CFStringRef;
const kCFSocketErrorKey: CFStringRef;
const kCFSocketRegisterCommand: CFStringRef;
const kCFSocketRetrieveCommand: CFStringRef;


end.
