{
 * ColorSync - ColorSyncDevice.h
 * Copyright (c)  2008 Apple Inc.
 * All rights reserved.
 }
{  Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ColorSyncDevice;
interface
uses MacTypes,ColorSyncProfile,CFBase,CFDictionary,CFUUID;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


const kColorSyncDeviceID: CFStringRef;                { CFUUIDRef }
const kColorSyncDeviceClass: CFStringRef;             { one of the below : }
const kColorSyncCameraDeviceClass: CFStringRef;   { cmra }
const kColorSyncDisplayDeviceClass: CFStringRef;  { mntr }
const kColorSyncPrinterDeviceClass: CFStringRef;  { prtr }
const kColorSyncScannerDeviceClass: CFStringRef;  { scnr }

const kColorSyncDeviceProfileURL: CFStringRef;

const kColorSyncDeviceDescription: CFStringRef;       { CFString with a name in current locale }
const kColorSyncDeviceDescriptions: CFStringRef;      { CFDictionary with localized names }

const kColorSyncFactoryProfiles: CFStringRef;         { CFDictionary containing factory profile info }
const kColorSyncCustomProfiles: CFStringRef;          { CFDictionary containing custom profile info }

const kColorSyncDeviceModeDescription: CFStringRef;   { CFString, e.g. Glossy, Best Quality }
const kColorSyncDeviceModeDescriptions: CFStringRef;  { CFDictionary with localized mode names }
const kColorSyncDeviceDefaultProfileID: CFStringRef;  { see below }
const kColorSyncDeviceHostScope: CFStringRef;         { kCFPreferences(Current,Any)Host }
const kColorSyncDeviceUserScope: CFStringRef;         { kCFPreferences(Current,Any)User }
const kColorSyncProfileHostScope: CFStringRef;        { kCFPreferences(Current,Any)Host }
const kColorSyncProfileUserScope: CFStringRef;        { kCFPreferences(Current,Any)User }

const kColorSyncDeviceProfileIsFactory: CFStringRef;  { Present in ColorSyncDeviceProfileInfo dictionary.}
                                                        { See ColorSyncDeviceProfileIterateCallback below. }
const kColorSyncDeviceProfileIsDefault: CFStringRef;  { ditto }
const kColorSyncDeviceProfileIsCurrent: CFStringRef;  { ditto }
const kColorSyncDeviceProfileID: CFStringRef;         { ditto }

const kColorSyncDeviceRegisteredNotification: CFStringRef;        { com.apple.ColorSync.DeviceRegisteredNotification }
const kColorSyncDeviceUnregisteredNotification: CFStringRef;      { com.apple.ColorSync.DeviceUnregisteredNotification }
const kColorSyncDeviceProfilesNotification: CFStringRef;         { com.apple.ColorSync.DeviceProfilesNotification }
const kColorSyncDisplayDeviceProfilesNotification: CFStringRef;   { com.apple.ColorSync.DisplayProfileNotification }

function ColorSyncRegisterDevice( deviceClass: CFStringRef; deviceID: CFUUIDRef; deviceInfo: CFDictionaryRef ): CBool;
   {
    *   deviceInfo  -a dictionary containing information needed to register a device.
    *   ----------------------------------------------------------------------------
    *               
    *               Required keys:
    *               ==============
    *                   kColorSyncDeviceDescriptions: CFDictionary with localized names of the device.
    *                                                 Localization keys must be five character strings
    *                                                 containing language code and region code in the
    *                                                 lc_RG format and it must contain (at least) the "en_US" locale.
    *                   kColorSyncFactoryProfiles   : CFDictionary with factory profile info CFDictionaries
    *                                                 The keys are the profile IDs and the values
    *                                                 are the profile info dictionaries.
    *               Optional keys:
    *               ==============
    *                   kColorSyncDeviceHostScope   : host scope of the device;
    *                                                 one of kCFPreferences(Current,Any)Host;
    *                                                 if unspecified kCFPreferencesCurrentHost is
    *                                                 assumed.
    *                   kColorSyncDeviceUserScope   : user scope of the device;
    *                                                 one of kCFPreferences(Current,Any)User;
    *                                                 if unspecified kCFPreferencesCurrentUser is
    *                                                 assumed.
    *
    *           factory profiles dictionary - value for the key kColorSyncFactoryProfiles in deviceInfo
    *           -------------------------------------------------------------------------------------
    *               Required keys and values:
    *               ========================
    *                   Each profile is identified by a ProfileID (of CFStringRef type) which used as the key.
    *                   Value associated with the key is a  profile info dictionary
    *                   that describes an individual device profile.
    *
    *                   kColorSyncDeviceDefaultProfileID: the associated value must be one of the ProfileID
    *                                                     present in the dictionary. Presence of this 
    *                                                     key is not required if there is only one factory profile.
    *
    *                   profile info CFDictionary
    *                   --------------------------------
    *                   Required keys:
    *                   ==============
    *                       kColorSyncDeviceProfileURL      :CFURLRef of the profile to be registered
    *                       kColorSyncDeviceModeDescriptions:CFDictionary with localized device mode
    *                                                        names for the profile. Localization keys 
    *                                                        must be five character strings containing
    *                                                        language code and region code in the lc_RG 
    *                                                        format and it must contain (at least) the
    *                                                        "en_US" locale.
    *                                                        E.g. "en_US" "Glossy Paper with best quality"
    *
    * Example of deviceInfo dictionary:
    *
    *   <<
    *       kColorSyncDeviceDescriptions   <<
    *                                           en_US  My Little Printer
    *                                           de_DE  Mein Kleiner Drucker
    *                                           fr_FR  Mon petit immprimeur
    *                                           ...
    *                                       >>
    *       kColorSyncFactoryProfiles       <<
    *                                           CFSTR("Profile 1")  <<
    *                                                                   kColorSyncDeviceProfileURL    (CFURLRef)
    *
    *                                                                   kColorSyncDeviceModeDescriptions    <<
    *                                                                                                           en_US Glossy Paper
    *                                                                                                           de_DE Glanzpapier
    *                                                                                                           fr_FR Papier glace
    *                                                                                                           ...
    *                                                                                                       >>
    *                                           ...
    *                                           
    *                                           kColorSyncDeviceDefaultProfileID  CFSTR("Profile 1")
    *                                       >>
    *       kColorSyncDeviceUserScope   kCFPreferencesAnyUser
    *
    *       kColorSyncDeviceHostScope   kCFPreferencesCurrentHost
    *   <<
    *
    * Notes:    1. Scope for factory profiles is exactly the same as the device scope.
    *           2. Pass kCFNull in lieu of the profile URL or no URl key/value pair at all if 
    *              factory profile is not available. This will enable setting custom profile.
    *           3. For the reasons of compatibility with legacy API, it is recommended that the
    *              profile keys are created as CFStrings from uint32_t numbers as follows:
    *              CFStringRef key = CFStringCreateWithFormat(NULL, NULL, CFSTR("%u"), (uint32_t) i);
    *
    *   returns true on success and false in case of failure
    }

function ColorSyncUnregisterDevice( deviceClass: CFStringRef; deviceID: CFUUIDRef ): CBool;
   {
    *   Unregister a device of given deviceClass and deviceID.
    *
    *   returns true on success and false in case of failure
    }

function ColorSyncDeviceSetCustomProfiles( deviceClass: CFStringRef; deviceID: CFUUIDRef; profileInfo: CFDictionaryRef ): CBool;
   {
    *                   profileInfo is a CFDictionary containing the information about 
    *                   custom profiles to be set in lieu of factory profiles.
    *                   Required keys:
    *                   ==============
    *                       ProfileIDs which must be the subset of the ProfileIDs that device was registered with
    *                       or kColorSyncDeviceDefaultProfileID for setting custom default profile.
    *
    *                   Required values:
    *                   ==============
    *                       CFURLRef of the profile to be set as a custom profile.
    *
    *                   Optional keys:
    *                   ==============
    *                       kColorSyncProfileHostScope  : host scope of the profile;
    *                                                     one of kCFPreferences(Current,Any)Host;
    *                                                     if unspecified kCFPreferencesCurrentHost
    *                                                     is assumed.
    *                       kColorSyncProfileUserScope  : user scope of the profile;
    *                                                     one of kCFPreferences(Current,Any)User;
    *                                                     if unspecified kCFPreferencesCurrentUser
    *                                                     is assumed.
    *
    *
    * Notes:    1. Profile scope for custom profiles cannot exceed scope of the factory profiles.
    *           2. There is only one host scope and user scope per dictionary (i.e. per call)
    *           3. Pass kCFNull in lieu of the profile URL to unset the custom profile and
    *              reset the current profile to the factory profile.
    *
    *   returns true on success and false in case of failure
    }

function ColorSyncDeviceCopyDeviceInfo( deviceClass: CFStringRef; devID: CFUUIDRef ): CFDictionaryRef;
   {
    *   Returns a dictionary with the following keys and values resolved for the current host and current user.
    *
    *   <<
    *       kColorSyncDeviceClass                   (camera, display, printer, scanner)
    *       kColorSyncDeviceID                      (CFUUIDRef registered with ColorSync)
    *       kColorSyncDeviceDescription             (localized device description)
    *       kColorSyncFactoryProfiles  (dictionary) <<
    *                                                   (ProfileID)    (dictionary) <<
    *                                                                                   kColorSyncDeviceProfileURL      (CFURLRef or kCFNull)
    *                                                                                   kColorSyncDeviceModeDescription (localized mode description)
    *                                                                               >>
    *                                                    ...
    *                                                   kColorSyncDeviceDefaultProfileID (ProfileID)
    *                                               >>
    *       kColorSyncCustomProfiles  (dictionary) <<
    *                                                   (ProfileID)    (CFURLRef or kCFNull)
    *                                                   ...
    *                                              <<
    *       kColorSyncDeviceUserScope              (kCFPreferencesAnyUser or kCFPreferencesCurrentUser)
    *       kColorSyncDeviceHostScope              (kCFPreferencesAnyHost or kCFPreferencesCurrentHost)
    *   >>
    }
    
type
	ColorSyncDeviceProfileIterateCallback = function( colorSyncDeviceProfileInfo: CFDictionaryRef; userInfo: univ Ptr ): CBool;
   {
    *   colorSyncDeviceProfileInfo contains the following keys: 
    *   <<
    *       kColorSyncDeviceClass                   (camera, display, printer, scanner)
    *       kColorSyncDeviceID                      (CFUUIDRef registered with ColorSync)
    *       kColorSyncDeviceDescription             (localized device description)
    *       kColorSyncDeviceModeDescription         (localized device mode description)
    *       kColorSyncDeviceProfileID               (ProfileID registered with ColorSync)
    *       kColorSyncDeviceProfileURL              (CFURLRef registered with ColorSync)
    *       kColorSyncDeviceProfileIsFactory        (kCFBooleanTrue or kCFBooleanFalse)
    *       kColorSyncDeviceProfileIsDefault        (kCFBooleanTrue or kCFBooleanFalse)
    *       kColorSyncDeviceProfileIsCurrent        (kCFBooleanTrue or kCFBooleanFalse)
    *   >>
    }
                                                       
procedure ColorSyncIterateDeviceProfiles( callBack: ColorSyncDeviceProfileIterateCallback; userInfo: univ Ptr );


    {
     * A utility function converting displayID to CFUUIDRef
     }
function CGDisplayCreateUUIDFromDisplayID( displayID: UInt32 ): CFUUIDRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;
    
    {
     * A utility function converting first 32 bits of CFUUIDRef to displayID
     }
function CGDisplayGetDisplayIDFromUUID( uuid: CFUUIDRef ): UInt32;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;

{$endc} {TARGET_OS_MAC}

end.
