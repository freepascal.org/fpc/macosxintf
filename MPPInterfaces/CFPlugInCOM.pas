{	CFPlugInCOM.h
	Copyright (c) 1999-2013, Apple Inc.  All rights reserved.
}
unit CFPlugInCOM;
interface
uses MacTypes,CFPlugIn,CFUUID;
{$ALIGN POWER}


{ ================= IUnknown definition (C struct) ================= }

{ All interface structs must have an IUnknownStruct at the beginning. }
{ The _reserved field is part of the Microsoft COM binary standard on Macintosh. }
{ You can declare new C struct interfaces by defining a new struct that includes "IUNKNOWN_C_GUTS;" before the first field of the struct. }

type
	HRESULT = SInt32;
	ULONG = UInt32;
	LPVOID = UnivPtr;
	REFIID = CFUUIDBytes;

{ Macros for more detailed HRESULT analysis }

{FPC-ONLY-START}
implemented function SUCCEEDED( Status: HRESULT ): Boolean; inline;
implemented function FAILED( Status: HRESULT ): Boolean; inline;
implemented function IS_ERROR( Status: HRESULT ): Boolean; inline;
implemented function HRESULT_CODE( hr: HRESULT ): HRESULT; inline;
implemented function HRESULT_FACILITY( hr: HRESULT ): HRESULT; inline;
implemented function HRESULT_SEVERITY( hr: HRESULT ): HRESULT; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec SUCCEEDED( Status ) (HRESULT(Status) >= 0)}
{$mwgpcdefinec FAILED( Status ) (HRESULT(Status) < 0)}
{$mwgpcdefinec IS_ERROR( Status ) (ULONG(Status) shr 31 = SEVERITY_ERROR)}
{$mwgpcdefinec HRESULT_CODE( hr ) (hr and $FFFF)}
{$mwgpcdefinec HRESULT_FACILITY( hr ) ((hr shr 16) and $1FFF)}
{$mwgpcdefinec HRESULT_SEVERITY( hr ) ((hr shr 31) and $01)}
{FPC-ONLY-FINISH}

const
	SEVERITY_SUCCESS = 0;
const
	SEVERITY_ERROR = 1;

{ Creating an HRESULT from its component pieces }
{FPC-ONLY-START}
implemented function MAKE_HRESULT( sev, fac, code: ULONG ): HRESULT; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec( sev, fac, code ) HRESULT((ULONG(sev) shl 31) or (ULONG(fac) shl 16) or ULONG(code)) }
{FPC-ONLY-FINISH}

{ Pre-defined success HRESULTS }
const
	S_OK = 0;
const
	S_FALSE = 1;

{ Common error HRESULTS }
const
	E_UNEXPECTED = $8000FFFF;
	E_NOTIMPL = $80000001;
	E_OUTOFMEMORY = $80000002;
	E_INVALIDARG = $80000003;
	E_NOINTERFACE = $80000004;
	E_POINTER = $80000005;
	E_HANDLE = $80000006;
	E_ABORT = $80000007;
	E_FAIL = $80000008;
	E_ACCESSDENIED = $80000009;

{ This macro should be used when defining all interface functions (as it is for the IUnknown functions below). }
{#define STDMETHODCALLTYPE}

{ The __RPC_FAR macro is for COM source compatibility only. This macro is used a lot in COM interface definitions.  If your CFPlugIn interfaces need to be COM interfaces as well, you can use this macro to get better source compatibility.  It is not used in the IUnknown definition below, because when doing COM, you will be using the Microsoft supplied IUnknown interface anyway. }
{#define __RPC_FAR}

{ The IUnknown interface }
{FPC-ONLY-START}
implemented function IUnknownUUID: CFUUIDRef; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec IUnknownUUID CFUUIDGetConstantUUIDWithBytes( nil, $00, $00, $00, $00, $00, $00, $00, $00, $C0, $00, $00, $00, $00, $00, $00, $46 )}
{FPC-ONLY-FINISH}

type
	IUnknownVTbl = record
		_reserved: UnivPtr;
		QueryInterface: function( thisPointer: UnivPtr; iid: REFIID; var ppv: LPVOID ): HRESULT;
		AddRef: function( thisPointer: UnivPtr ): ULONG;
		Release: function( thisPointer: UnivPtr ): ULONG;
	end;

{ End of extern "C" stuff }


{ C++ specific stuff }

implementation

{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

{FPC-ONLY-START}
{$R-}

function SUCCEEDED( Status: HRESULT ): Boolean; inline;
begin
	SUCCEEDED := Status >= 0;
end;

function FAILED( Status: HRESULT ): Boolean; inline;
begin
	FAILED := Status < 0;
end;

function IS_ERROR( Status: HRESULT ): Boolean; inline;
begin
	IS_ERROR := Status shr 31 = SEVERITY_ERROR;
end;

function HRESULT_CODE( hr: HRESULT ): HRESULT; inline;
begin
	HRESULT_CODE := hr and $FFFF;
end;

function HRESULT_FACILITY( hr: HRESULT ): HRESULT; inline;
begin
	HRESULT_FACILITY := (hr shr 16) and $1FFF;
end;

function HRESULT_SEVERITY( hr: HRESULT ): HRESULT; inline;
begin
	HRESULT_SEVERITY := (hr shr 31) and $01;
end;

function MAKE_HRESULT( sev, fac, code: ULONG ): HRESULT; inline;
begin
	MAKE_HRESULT := HRESULT((sev shl 31) or (fac shl 16) or code);
end;

function IUnknownUUID: CFUUIDRef; inline;
begin
	IUnknownUUID:= CFUUIDGetConstantUUIDWithBytes( nil, $00, $00, $00, $00, $00, $00, $00, $00, $C0, $00, $00, $00, $00, $00, $00, $46 )
end;

{FPC-ONLY-FINISH}

end.
