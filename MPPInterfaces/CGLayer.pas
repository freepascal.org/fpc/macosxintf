{ CoreGraphics - CGLayer.h
 * Copyright (c) 2004-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGLayer;
interface
uses MacTypes,CFBase,CFDictionary,CGBase,CGGeometry,CGContext;
{$ALIGN POWER}


type
	CGLayerRef = ^OpaqueCGLayerRef; { an opaque type }
	OpaqueCGLayerRef = record end;


{ Create a layer of size `size' relative to the context `context'. The
   value of `size' is specified in default user space (base space) units.
   The parameter `auxiliaryInfo' should be NULL; it is reserved for future
   expansion. }

function CGLayerCreateWithContext( context: CGContextRef; size: CGSize; auxiliaryInfo: CFDictionaryRef ): CGLayerRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Equivalent to `CFRetain(layer)', except it doesn't crash (as CFRetain
   does) if `layer' is NULL. }

function CGLayerRetain( layer: CGLayerRef ): CGLayerRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Equivalent to `CFRelease(layer)', except it doesn't crash (as CFRelease
   does) if `layer' is NULL. }

procedure CGLayerRelease( layer: CGLayerRef );
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the size of the layer `layer'. }

function CGLayerGetSize( layer: CGLayerRef ): CGSize;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the context of `layer'. }

function CGLayerGetContext( layer: CGLayerRef ): CGContextRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Draw the contents of `layer' into `rect' of `context'. The contents are
   scaled, if necessary, to fit into `rect'; the rectangle `rect' is in user
   space. }

procedure CGContextDrawLayerInRect( context: CGContextRef; rect: CGRect; layer: CGLayerRef );
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Draw the contents of `layer' at `point' in `context'. This is equivalent
   to calling "CGContextDrawLayerInRect" with a rectangle having origin at
   `point' and size equal to the size of `layer'. }

procedure CGContextDrawLayerAtPoint( context: CGContextRef; point: CGPoint; layer: CGLayerRef );
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the CFTypeID for CGLayerRefs. }

function CGLayerGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);


end.
