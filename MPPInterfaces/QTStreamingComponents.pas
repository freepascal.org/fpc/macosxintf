{
     File:       QuickTime/QTStreamingComponents.h
 
     Contains:   QuickTime Interfaces.
 
     Version:    QuickTime 7.7.1
 
     Copyright:  � 1990-2012 by Apple Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit QTStreamingComponents;
interface
uses MacTypes,Components,Dialogs,Movies,QuickTimeStreaming;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}


{ QuickTime is not available to 64-bit clients }

{$ifc not TARGET_CPU_64}

{============================================================================
        Stream Sourcer
============================================================================}
const
	kQTSSourcerType = FOUR_CHAR_CODE('srcr');

type
	QTSSourcer = ComponentInstance;
const
	kQTSSGChannelSourcerType = FOUR_CHAR_CODE('sgch');
	kQTSMovieTrackSourcerType = FOUR_CHAR_CODE('trak');
	kQTSPushDataSourcerType = FOUR_CHAR_CODE('push');

{ flags for sourcer data }
const
	kQTSSourcerDataFlag_SyncSample = $00000001;
	kQTSPushDataSourcerFlag_SampleTimeIsValid = $80000000;


const
	kQTSSourcerInitParamsVersion1 = 1;

type
	QTSSourcerInitParamsPtr = ^QTSSourcerInitParams;
	QTSSourcerInitParams = record
		version: SInt32;
		flags: SInt32;
		dataType: OSType;
		data: UnivPtr;
		dataLength: UInt32;
	end;
{
 *  QTSNewSourcer()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 }
function QTSNewSourcer( params: univ Ptr; const var inInitParams: QTSSourcerInitParams; inFlags: SInt32; var outSourcer: ComponentInstance ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ info selectors for sourcers - get and set }
const
	kQTSInfo_Track = FOUR_CHAR_CODE('trak'); { QTSTrackParams* }
	kQTSInfo_Loop = FOUR_CHAR_CODE('loop'); { QTSLoopParams* }
	kQTSInfo_SourcerTiming = FOUR_CHAR_CODE('stim'); { QTSSourcerTimingParams* }
	kQTSInfo_TargetFrameRate = FOUR_CHAR_CODE('tfps'); { Fixed * in frames per second }
	kQTSInfo_PushData = FOUR_CHAR_CODE('push'); { QTSPushDataParams* }
	kQTSInfo_SourcerCallbackProc = FOUR_CHAR_CODE('scbp'); { QTSSourcerCallbackProcParams* }
	kQTSInfo_TargetDataRate = FOUR_CHAR_CODE('tdrt'); { UInt32 * in bytes per second }
	kQTSInfo_AudioAutoGainOnOff = FOUR_CHAR_CODE('agc '); { Boolean*  - error if unavailable}
	kQTSInfo_AudioGain = FOUR_CHAR_CODE('gain'); { Fixed* kFixed1 is unity gain }
	kQTSInfo_CroppedInputRect = FOUR_CHAR_CODE('crpr'); { Rect* - defined relative to kQTSInfo_FullInputRect below }
	kQTSInfo_SpatialSettings = FOUR_CHAR_CODE('sptl'); { pointer to SCSpatialSettings struct}
	kQTSInfo_TemporalSettings = FOUR_CHAR_CODE('tprl'); { pointer to SCTemporalSettings struct}
	kQTSInfo_DataRateSettings = FOUR_CHAR_CODE('drat'); { pointer to SCDataRateSettings struct}
	kQTSInfo_CodecFlags = FOUR_CHAR_CODE('cflg'); { pointer to CodecFlags}
	kQTSInfo_CodecSettings = FOUR_CHAR_CODE('cdec'); { pointer to Handle}
	kQTSInfo_ForceKeyValue = FOUR_CHAR_CODE('ksim'); { pointer to long}
	kQTSInfo_SoundSampleRate = FOUR_CHAR_CODE('ssrt'); { pointer to UnsignedFixed}
	kQTSInfo_SoundSampleSize = FOUR_CHAR_CODE('ssss'); { pointer to short}
	kQTSInfo_SoundChannelCount = FOUR_CHAR_CODE('sscc'); { pointer to short}
	kQTSInfo_SoundCompression = FOUR_CHAR_CODE('ssct'); { pointer to OSType}
	kQTSInfo_CompressionList = FOUR_CHAR_CODE('ctyl'); { pointer to OSType Handle}
	kQTSInfo_VideoHue = FOUR_CHAR_CODE('hue '); { UInt16* }
	kQTSInfo_VideoSaturation = FOUR_CHAR_CODE('satr'); { UInt16* }
	kQTSInfo_VideoContrast = FOUR_CHAR_CODE('trst'); { UInt16* }
	kQTSInfo_VideoBrightness = FOUR_CHAR_CODE('brit'); { UInt16* }
	kQTSInfo_VideoSharpness = FOUR_CHAR_CODE('shrp'); { UInt16* }
	kQTSInfo_TimeScale = FOUR_CHAR_CODE('scal'); { UInt32* }
	kQTSInfo_SGChannelDeviceName = FOUR_CHAR_CODE('innm'); { Handle* }
	kQTSInfo_SGChannelDeviceList = FOUR_CHAR_CODE('srdl'); { SGDeviceList* }
	kQTSInfo_SGChannelDeviceInput = FOUR_CHAR_CODE('sdii'); { short* }
	kQTSInfo_SGChannelSettings = FOUR_CHAR_CODE('sesg'); { QTSSGChannelSettingsParams }
	kQTSInfo_PreviewWhileRecordingMode = FOUR_CHAR_CODE('srpr'); { Boolean* }
	kQTSInfo_CompressionParams = FOUR_CHAR_CODE('sccp'); { QTAtomContainer* }

{ info selectors for sourcers - get only}
const
	kQTSInfo_SGChannel = FOUR_CHAR_CODE('sgch'); { SGChannel* }
	kQTSInfo_SGChannelInputName = FOUR_CHAR_CODE('srnm'); { Handle* }
	kQTSInfo_FullInputRect = FOUR_CHAR_CODE('fulr'); { Rect* }

{ loop flags }
const
	kQTSLoopFlag_Loop = $00000001;

const
	kQTSLoopParamsVersion1 = 1;

type
	QTSLoopParamsPtr = ^QTSLoopParams;
	QTSLoopParams = record
		version: SInt32;
		flags: SInt32;
		loopFlags: SInt32;
		flagsMask: SInt32;
		numLoops: SInt32;
	end;
const
	kQTSTrackParamsVersion1 = 1;

type
	QTSTrackParamsPtr = ^QTSTrackParams;
	QTSTrackParams = record
		version: SInt32;
		flags: SInt32;
		track: Track_fix;
		trackStartOffset: TimeValue64;       { to start other than at the beginning otherwise set to 0}
		duration: TimeValue64;               { to limit the duration otherwise set to 0}
		loopParams: QTSLoopParamsPtr;             { set to NULL if not using; default is no looping }
	end;
const
	kQTSSourcerTimingParamsVersion1 = 1;

type
	QTSSourcerTimingParamsPtr = ^QTSSourcerTimingParams;
	QTSSourcerTimingParams = record
		version: SInt32;
		flags: SInt32;
		timeScale_: TimeScale;
		presentationStartTime: TimeValue64;
		presentationEndTime: TimeValue64;
		presentationCurrentTime: TimeValue64;
		localStartTime: TimeValue64;
		localEndTime: TimeValue64;
		localCurrentTime: TimeValue64;
	end;
const
	kQTSPushDataParamsVersion1 = 1;

const
	kQTSPushDataFlag_SampleTimeIsValid = $00000001;
	kQTSPushDataFlag_DurationIsValid = $00000002;

type
	QTSPushDataParamsPtr = ^QTSPushDataParams;
	QTSPushDataParams = record
		version: SInt32;
		flags: SInt32;
		sampleDescription: SampleDescriptionHandle; { caller owns the handle }
		sampleDescSeed: UInt32;
		sampleTime: TimeValue64;             { also set flag if you set this }
		duration: TimeValue64;               { also set flag if you set this }
		dataLength: UInt32;
		dataPtr: UnivPtr;                { this does not have to be a real macintosh Ptr }
	end;
const
	kQTSSourcerCallbackProcParamsVersion1 = 1;


type
	QTSSourcerCallbackProcParamsPtr = ^QTSSourcerCallbackProcParams;
	QTSSourcerCallbackProcParams = record
		version: SInt32;
		flags: SInt32;
		proc: QTSNotificationUPP;
		refCon: UnivPtr;
	end;
{ track sourcer callback selectors}
const
	kQTSSourcerCallback_Done = FOUR_CHAR_CODE('done'); { QTSSourcerDoneParams* }


{ push data sourcer callback selectors}
const
	kQTSPushDataSourcerCallback_HasCharacteristic = $050D; { QTSPushDataHasCharacteristicParams* }
	kQTSPushDataSourcerCallback_SetInfo = $0507; { QTSPushDataInfoParams* }
	kQTSPushDataSourcerCallback_GetInfo = $0508; { QTSPushDataInfoParams* }

type
	QTSPushDataHasCharacteristicParamsPtr = ^QTSPushDataHasCharacteristicParams;
	QTSPushDataHasCharacteristicParams = record
		version: SInt32;
		flags: SInt32;
		characteristic: OSType;
		returnedHasIt: Boolean;
		reserved1: SInt8;
		reserved2: SInt8;
		reserved3: SInt8;
	end;
type
	QTSPushDataInfoParamsPtr = ^QTSPushDataInfoParams;
	QTSPushDataInfoParams = record
		version: SInt32;
		flags: SInt32;
		selector: OSType;
		ioParams: UnivPtr;
	end;
const
	kQTSSourcerDoneParamsVersion1 = 1;

type
	QTSSourcerDoneParamsPtr = ^QTSSourcerDoneParams;
	QTSSourcerDoneParams = record
		version: SInt32;
		flags: SInt32;
		sourcer: ComponentInstance;
	end;
type
	QTSSGChannelSettingsParamsPtr = ^QTSSGChannelSettingsParams;
	QTSSGChannelSettingsParams = record
		settings: UserData;
		flags: SInt32;
	end;

{-----------------------------------------
    Stream Sourcer Selectors
-----------------------------------------}
const
	kQTSSourcerInitializeSelect = $0500;
	kQTSSourcerSetEnableSelect = $0503;
	kQTSSourcerGetEnableSelect = $0504;
	kQTSSourcerSetInfoSelect = $0507;
	kQTSSourcerGetInfoSelect = $0508;
	kQTSSourcerSetTimeScaleSelect = $050E;
	kQTSSourcerGetTimeScaleSelect = $050F;
	kQTSSourcerIdleSelect = $0516;

{-----------------------------------------
    Stream Sourcer Prototypes
-----------------------------------------}
{
 *  QTSSourcerInitialize()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.1 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.6 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0.1 and later
 }
function QTSSourcerInitialize( inSourcer: QTSSourcer; const var inInitParams: QTSSourcerInitParams ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_1_AND_LATER;


{
 *  QTSSourcerIdle()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 }
function QTSSourcerIdle( inSourcer: QTSSourcer; const var inTime: TimeValue64; inFlags: SInt32; var outFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSSourcerSetEnable()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 }
function QTSSourcerSetEnable( inSourcer: QTSSourcer; inEnableMode: Boolean; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSSourcerGetEnable()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 }
function QTSSourcerGetEnable( inSourcer: QTSSourcer; var outEnableMode: Boolean; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSSourcerSetTimeScale()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 }
function QTSSourcerSetTimeScale( inSourcer: QTSSourcer; inTimeScale: TimeScale ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSSourcerGetTimeScale()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 }
function QTSSourcerGetTimeScale( inSourcer: QTSSourcer; var outTimeScale: TimeScale ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSSourcerSetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 }
function QTSSourcerSetInfo( inSourcer: QTSSourcer; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSSourcerGetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 }
function QTSSourcerGetInfo( inSourcer: QTSSourcer; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


const
	kQTSInfo_InputDeviceName = FOUR_CHAR_CODE('innm'); { Handle* }
	kQTSInfo_InputSourceName = FOUR_CHAR_CODE('srnm'); { Handle* }


{============================================================================
        Stream Handler
============================================================================}

{
    Server edits are only valid for the current chunk
}
type
	SHServerEditParametersPtr = ^SHServerEditParameters;
	SHServerEditParameters = record
		version: UInt32;
		editRate: Fixed;
		dataStartTime_mediaAxis: TimeValue64;
		dataEndTime_mediaAxis: TimeValue64;
	end;
const
	kSHNoChunkDispatchFlags = 0;
	kSHChunkFlagSyncSample = 1 shl 2;
	kSHChunkFlagDataLoss = 1 shl 4;
	kSHChunkFlagExtended = 1 shl 5;

type
	SHChunkRecordPtr = ^SHChunkRecord;
	SHChunkRecord = record
		version: UInt32;
		reserved1: SIGNEDLONG;
		flags: SInt32;
		dataSize: UInt32;
		dataPtr: UInt8Ptr;
		reserved2: SIGNEDLONG;
		reserved3: SIGNEDLONG;
		presentationTime: TimeValue64;

		reserved4: SIGNEDLONG;
		reserved5: SIGNEDLONG;
		serverEditParameters: {const} SHServerEditParametersPtr;
		reserved6: SIGNEDLONG;
		reserved7: SIGNEDLONG;
	end;
const
	kSHNumExtendedDataLongs = 10;

const
	kSHExtendedChunkFlag_HasSampleCount = 1 shl 0;
	kSHExtendedChunkFlag_HasFrameLengths = 1 shl 1;

type
	SHExtendedChunkRecordPtr = ^SHExtendedChunkRecord;
	SHExtendedChunkRecord = record
		chunk: SHChunkRecord;
		extendedFlags: SInt32;
		extendedData: array [0..9] of SInt32;
	end;


{============================================================================
        RTP Components
============================================================================}

type
	RTPSSRC = UInt32;
const
	kRTPInvalidSSRC = 0;


{ RTP standard content encodings for audio }
const
	kRTPPayload_PCMU = 0;    { 8kHz PCM mu-law mono }
	kRTPPayload_1016 = 1;    { 8kHz CELP (Fed Std 1016) mono }
	kRTPPayload_G721 = 2;    { 8kHz G.721 ADPCM mono }
	kRTPPayload_GSM = 3;    { 8kHz GSM mono }
	kRTPPayload_G723 = 4;    { 8kHz G.723 ADPCM mono }
	kRTPPayload_DVI_8 = 5;    { 8kHz Intel DVI ADPCM mono }
	kRTPPayload_DVI_16 = 6;    { 16kHz Intel DVI ADPCM mono }
	kRTPPayload_LPC = 7;    { 8kHz LPC }
	kRTPPayload_PCMA = 8;    { 8kHz PCM a-law mono }
	kRTPPayload_L16_44_2 = 10;   { 44.1kHz 16-bit linear stereo }
	kRTPPayload_L16_44_1 = 11;   { 44.1kHz 16-bit linear mono }
	kRTPPayload_PureVoice = 12;   { 8kHz PureVoice mono (QCELP) }
	kRTPPayload_MPEGAUDIO = 14;   { MPEG I and II audio }
	kRTPPayload_DVI_11 = 16;   { 11kHz Intel DVI ADPCM mono }
	kRTPPayload_DVI_22 = 17;    { 22kHz Intel DVI ADPCM mono }

{ RTP standard content encodings for video }
const
	kRTPPayload_CELLB = 25;   { Sun CellB }
	kRTPPayload_JPEG = 26;   { JPEG }
	kRTPPayload_CUSEEME = 27;   { Cornell CU-SeeMe }
	kRTPPayload_NV = 28;   { Xerox PARC nv }
	kRTPPayload_PICWIN = 29;   { BBN Picture Window }
	kRTPPayload_CPV = 30;   { Bolter CPV }
	kRTPPayload_H261 = 31;   { CCITT H.261 }
	kRTPPayload_MPEGVIDEO = 32;   { MPEG I and II video }
	kRTPPayload_H263 = 34;    { CCITT H.263 }

{ Other RTP standard content encodings }
const
	kRTPPayload_MPEG2T = 33;    { MPEG 2 Transport }

{ Dynamic encodings }
const
	kRTPPayload_FirstDynamic = 96;
	kRTPPayload_LastDynamic = 127;
	kRTPPayload_Unknown = $FF;


{
-----------------------------------------
    RTP Info selectors
-----------------------------------------
}
{ ----- these are get and set ----- }
const
	kRTPInfo_SSRC = FOUR_CHAR_CODE('ssrc'); { UInt32* }
	kRTPInfo_NextSeqNum = FOUR_CHAR_CODE('rnsn'); { UInt16* }

{-----------------------------------------
    RTP Statistics
-----------------------------------------}
const
	kRTPTotalReceivedPktsStat = FOUR_CHAR_CODE('trcp');
	kRTPTotalLostPktsStat = FOUR_CHAR_CODE('tlsp');
	kRTPTotalProcessedPktsStat = FOUR_CHAR_CODE('tprp');
	kRTPTotalDroppedPktsStat = FOUR_CHAR_CODE('tdrp');
	kRTPBadHeaderDroppedPktsStat = FOUR_CHAR_CODE('bhdp');
	kRTPOurHeaderDroppedPktsStat = FOUR_CHAR_CODE('ohdp');
	kRTPNotReceivingSenderDroppedPktsStat = FOUR_CHAR_CODE('nsdp');
	kRTPNotProcessingDroppedPktsStat = FOUR_CHAR_CODE('npdp');
	kRTPBadSeqDroppedPktsStat = FOUR_CHAR_CODE('bsdp');
	kRTPArriveTooLatePktsStat = FOUR_CHAR_CODE('artl');
	kRTPWaitForSeqDroppedPktsStat = FOUR_CHAR_CODE('wsdp');
	kRTPBadStateDroppedPktsStat = FOUR_CHAR_CODE('stdp');
	kRTPBadPayloadDroppedPktsStat = FOUR_CHAR_CODE('bpdp');
	kRTPNoTimeScaleDroppedPktsStat = FOUR_CHAR_CODE('ntdp');
	kRTPDupSeqNumDroppedPktsStat = FOUR_CHAR_CODE('dsdp');
	kRTPLostPktsPercentStat = FOUR_CHAR_CODE('lspp');
	kRTPDroppedPktsPercentStat = FOUR_CHAR_CODE('dppp');
	kRTPTotalUnprocessedPktsPercentStat = FOUR_CHAR_CODE('tupp');
	kRTPRTCPDataRateStat = FOUR_CHAR_CODE('rrcd');
	kRTPPayloadIDStat = FOUR_CHAR_CODE('rpid');
	kRTPPayloadNameStat = FOUR_CHAR_CODE('rpnm');
	kRTPNumPktsInQueueStat = FOUR_CHAR_CODE('rnpq');
	kRTPTotalPktsInQueueStat = FOUR_CHAR_CODE('rtpq');
	kRTPTotalOutOfOrderPktsStat = FOUR_CHAR_CODE('rtoo');
	kRTPRetransmissionStat = FOUR_CHAR_CODE('rrtx');


{-----------------------------------------
    Payload Info
-----------------------------------------}
const
	kRTPPayloadSpeedTag = FOUR_CHAR_CODE('sped'); { 0-255, 255 is fastest}
	kRTPPayloadLossRecoveryTag = FOUR_CHAR_CODE('loss'); { 0-255, 0 can't handle any loss, 128 can handle 50% packet loss}
	kRTPPayloadConformanceTag = FOUR_CHAR_CODE('conf'); { more than one of these can be present}

type
	RTPPayloadCharacteristicPtr = ^RTPPayloadCharacteristic;
	RTPPayloadCharacteristic = record
		tag: OSType;
		value: SIGNEDLONG;
	end;
{
    pass RTPPayloadSortRequest to QTSFindMediaPacketizer or QTSFindMediaPacketizerForTrack.
    define the characteristics to sort by. tag is key to sort on. value is positive for ascending
    sort (low value first), negative for descending sort (high value first).
}
type
	RTPPayloadSortRequest = record
		characteristicCount: SIGNEDLONG;
		characteristic: array [0..0] of RTPPayloadCharacteristic; { tag is key to sort on, value is + for ascending, - for descending}
	end;
	RTPPayloadSortRequestPtr = ^RTPPayloadSortRequest;
{ flags for RTPPayloadInfo }
const
	kRTPPayloadTypeStaticFlag = $00000001;
	kRTPPayloadTypeDynamicFlag = $00000002;

type
	RTPPayloadInfo = record
		payloadFlags: SIGNEDLONG;
		payloadID: UInt8;
		reserved1: SInt8;
		reserved2: SInt8;
		reserved3: SInt8;
    payloadName: array [0..1] of SInt8;
	end;
	RTPPayloadInfoPtr = ^RTPPayloadInfo;
type
	RTPPayloadInfoHandle = ^RTPPayloadInfoPtr;
{============================================================================
        RTP Reassembler
============================================================================}
type
	RTPReassembler = ComponentInstance;
const
	kRTPReassemblerType = FOUR_CHAR_CODE('rtpr');

const
	kRTPBaseReassemblerType = FOUR_CHAR_CODE('gnrc');
	kRTP261ReassemblerType = FOUR_CHAR_CODE('h261');
	kRTP263ReassemblerType = FOUR_CHAR_CODE('h263');
	kRTP263PlusReassemblerType = FOUR_CHAR_CODE('263+');
	kRTPAudioReassemblerType = FOUR_CHAR_CODE('soun');
	kRTPQTReassemblerType = FOUR_CHAR_CODE('qtim');
	kRTPPureVoiceReassemblerType = FOUR_CHAR_CODE('Qclp');
	kRTPJPEGReassemblerType = FOUR_CHAR_CODE('jpeg');
	kRTPQDesign2ReassemblerType = FOUR_CHAR_CODE('QDM2');
	kRTPSorensonReassemblerType = FOUR_CHAR_CODE('SVQ1');
	kRTPMP3ReassemblerType = FOUR_CHAR_CODE('mp3 ');
	kRTPMPEG4AudioReassemblerType = FOUR_CHAR_CODE('mp4a');
	kRTPMPEG4VideoReassemblerType = FOUR_CHAR_CODE('mp4v');

type
	RTPRssmInitParamsPtr = ^RTPRssmInitParams;
	RTPRssmInitParams = record
		ssrc: RTPSSRC;
		payloadType: UInt8;
		reserved1: UInt8;
		reserved2: UInt8;
		reserved3: UInt8;
		timeBase_: TimeBase;
		timeScale_: TimeScale;
	end;
type
	RTPDescParamsPtr = ^RTPDescParams;
	RTPDescParams = record
		container: QTAtomContainer;
		presentationParentAtom: QTAtom;
		streamParentAtom: QTAtom;
	end;
type
	RTPRssmMoreInitParamsPtr = ^RTPRssmMoreInitParams;
	RTPRssmMoreInitParams = record
		initParams: RTPRssmInitParams;
		version: SInt32;
		desc: RTPDescParams;
	end;
const
	kRTPRssmMoreInitParamsVersion1 = 1;


{ get/set info selectors}
const
	kRTPRssmInfo_MoreInitParams = FOUR_CHAR_CODE('rrmi');


type
	RTPRssmPacketPtr = ^RTPRssmPacket;
	RTPRssmPacket = record
		next: RTPRssmPacketPtr;
		prev: RTPRssmPacketPtr;
		streamBuffer: QTSStreamBufferPtr;
		paramsFilledIn: Boolean;
		reserved: UInt8;
		sequenceNum: UInt16;
		transportHeaderLength: UInt32;  { filled in by base}
		payloadHeaderLength: UInt32;    { derived adjusts this }
		dataLength: UInt32;
		serverEditParams: SHServerEditParameters;
		timeStamp: TimeValue64;              { lower 32 bits is original rtp timestamp}
		chunkFlags: SInt32;             { these are or'd together}
		flags: SInt32;
	end;
{ flags for RTPRssmPacket struct}
const
	kRTPRssmPacketHasMarkerBitSet = $00000001;
	kRTPRssmPacketHasServerEditFlag = $00010000;

{ flags for RTPRssmSendStreamBufferRange}
const
	kRTPRssmCanRefStreamBuffer = $00000001;

{ flags for RTPRssmSendPacketList}
const
	kRTPRssmLostSomePackets = $00000001;

{ flags for RTPRssmSetFlags}
const
	kRTPRssmEveryPacketAChunkFlag = $00000001;
	kRTPRssmQueueAndUseMarkerBitFlag = $00000002;
	kRTPRssmTrackLostPacketsFlag = $00010000;
	kRTPRssmNoReorderingRequiredFlag = $00020000;


type
	RTPSendStreamBufferRangeParamsPtr = ^RTPSendStreamBufferRangeParams;
	RTPSendStreamBufferRangeParams = record
		streamBuffer: QTSStreamBufferPtr;
		presentationTime: TimeValue64;
		chunkStartPosition: UInt32;
		numDataBytes: UInt32;
		chunkFlags: SInt32;
		flags: SInt32;
		serverEditParams: {const} SHServerEditParametersPtr; { NULL if no edit}
	end;
{ characteristics}
const
	kRTPCharacteristic_RequiresOrderedPackets = FOUR_CHAR_CODE('rrop');
	kRTPCharacteristic_TimeStampsNotMonoIncreasing = FOUR_CHAR_CODE('tsmi');


const
	kRTPReassemblerInfoResType = FOUR_CHAR_CODE('rsmi');

type
	RTPReassemblerInfo = record
		characteristicCount: SIGNEDLONG;
		characteristic: array [0..0] of RTPPayloadCharacteristic;

                                              { after the last characteristic, the payload name (defined by the MediaPacketizerPayloadInfo}
                                              { structure) is present. }
	end;
	RTPReassemblerInfoPtr = ^RTPReassemblerInfo;
type
	RTPReassemblerInfoHandle = ^RTPReassemblerInfoPtr;
	{	 RTPReassemblerInfoElement structs are padded to 32 bits 	}
const
	kRTPReassemblerInfoPadUpToBytes = 4;


{
 *  QTSFindReassemblerForPayloadID()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSFindReassemblerForPayloadID( inPayloadID: UInt8; var inSortInfo: RTPPayloadSortRequest; var outReassemblerList: QTAtomContainer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSFindReassemblerForPayloadName()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSFindReassemblerForPayloadName( inPayloadName: ConstCStringPtr; var inSortInfo: RTPPayloadSortRequest; var outReassemblerList: QTAtomContainer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{-----------------------------------------
    RTP Reassembler Selectors
-----------------------------------------}
const
	kRTPRssmSetCapabilitiesSelect = $0100;
	kRTPRssmGetCapabilitiesSelect = $0101;
	kRTPRssmSetPayloadHeaderLengthSelect = $0102;
	kRTPRssmGetPayloadHeaderLengthSelect = $0103;
	kRTPRssmSetTimeScaleSelect = $0104;
	kRTPRssmGetTimeScaleSelect = $0105;
	kRTPRssmNewStreamHandlerSelect = $0106;
	kRTPRssmSetStreamHandlerSelect = $0107;
	kRTPRssmGetStreamHandlerSelect = $0108;
	kRTPRssmSendStreamHandlerChangedSelect = $0109;
	kRTPRssmSetSampleDescriptionSelect = $010A;
	kRTPRssmGetChunkAndIncrRefCountSelect = $010D;
	kRTPRssmSendChunkAndDecrRefCountSelect = $010E;
	kRTPRssmSendLostChunkSelect = $010F;
	kRTPRssmSendStreamBufferRangeSelect = $0110;
	kRTPRssmClearCachedPackets = $0111;
	kRTPRssmFillPacketListParamsSelect = $0113;
	kRTPRssmReleasePacketListSelect = $0114;
	kRTPRssmIncrChunkRefCountSelect = $0115;
	kRTPRssmDecrChunkRefCountSelect = $0116;
	kRTPRssmGetExtChunkAndIncrRefCountSelect = $0117;
	kRTPRssmInitializeSelect = $0500;
	kRTPRssmHandleNewPacketSelect = $0501;
	kRTPRssmComputeChunkSizeSelect = $0502;
	kRTPRssmAdjustPacketParamsSelect = $0503;
	kRTPRssmCopyDataToChunkSelect = $0504;
	kRTPRssmSendPacketListSelect = $0505;
	kRTPRssmGetTimeScaleFromPacketSelect = $0506;
	kRTPRssmSetInfoSelect = $0509;
	kRTPRssmGetInfoSelect = $050A;
	kRTPRssmHasCharacteristicSelect = $050B;
	kRTPRssmResetSelect = $050C;

{-----------------------------------------
    RTP Reassembler functions - base to derived
-----------------------------------------}

{
 *  RTPRssmInitialize()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmInitialize( rtpr: RTPReassembler; var inInitParams: RTPRssmInitParams ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmHandleNewPacket()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmHandleNewPacket( rtpr: RTPReassembler; var inStreamBuffer: QTSStreamBuffer; inNumWraparounds: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmComputeChunkSize()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmComputeChunkSize( rtpr: RTPReassembler; var inPacketListHead: RTPRssmPacket; inFlags: SInt32; var outChunkDataSize: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmAdjustPacketParams()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmAdjustPacketParams( rtpr: RTPReassembler; var inPacket: RTPRssmPacket; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmCopyDataToChunk()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmCopyDataToChunk( rtpr: RTPReassembler; var inPacketListHead: RTPRssmPacket; inMaxChunkDataSize: UInt32; var inChunk: SHChunkRecord; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmSendPacketList()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSendPacketList( rtpr: RTPReassembler; var inPacketListHead: RTPRssmPacket; const var inLastChunkPresentationTime: TimeValue64; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmGetTimeScaleFromPacket()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmGetTimeScaleFromPacket( rtpr: RTPReassembler; var inStreamBuffer: QTSStreamBuffer; var outTimeScale: TimeScale ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmSetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSetInfo( rtpr: RTPReassembler; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmGetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmGetInfo( rtpr: RTPReassembler; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmHasCharacteristic()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmHasCharacteristic( rtpr: RTPReassembler; inCharacteristic: OSType; var outHasIt: Boolean ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmReset()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmReset( rtpr: RTPReassembler; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{-----------------------------------------
    RTP Reassembler functions - derived to base
-----------------------------------------}
{ ----- setup}
{
 *  RTPRssmSetCapabilities()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSetCapabilities( rtpr: RTPReassembler; inFlags: SInt32; inFlagsMask: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmGetCapabilities()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmGetCapabilities( rtpr: RTPReassembler; var outFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmSetPayloadHeaderLength()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSetPayloadHeaderLength( rtpr: RTPReassembler; inPayloadHeaderLength: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmGetPayloadHeaderLength()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmGetPayloadHeaderLength( rtpr: RTPReassembler; var outPayloadHeaderLength: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmSetTimeScale()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSetTimeScale( rtpr: RTPReassembler; inSHTimeScale: TimeScale ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmGetTimeScale()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmGetTimeScale( rtpr: RTPReassembler; var outSHTimeScale: TimeScale ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmNewStreamHandler()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmNewStreamHandler( rtpr: RTPReassembler; inSHType: OSType; inSampleDescription: SampleDescriptionHandle; inSHTimeScale: TimeScale; var outHandler: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmSetStreamHandler()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSetStreamHandler( rtpr: RTPReassembler; inStreamHandler: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmGetStreamHandler()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmGetStreamHandler( rtpr: RTPReassembler; var outStreamHandler: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmSendStreamHandlerChanged()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSendStreamHandlerChanged( rtpr: RTPReassembler ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmSetSampleDescription()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSetSampleDescription( rtpr: RTPReassembler; inSampleDescription: SampleDescriptionHandle ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ ----- manually sending chunks}
{
 *  RTPRssmGetChunkAndIncrRefCount()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmGetChunkAndIncrRefCount( rtpr: RTPReassembler; inChunkDataSize: UInt32; const var inChunkPresentationTime: TimeValue64; var outChunk: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmGetExtChunkAndIncrRefCount()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.6 and later
 *    Non-Carbon CFM:   in QTStreamLib 6.0 and later
 *    Windows:          in qtmlClient.lib 6.0 and later
 }
function RTPRssmGetExtChunkAndIncrRefCount( rtpr: RTPReassembler; inChunkDataSize: UInt32; const var inChunkPresentationTime: TimeValue64; inFlags: SInt32; var outChunk: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_2_AND_LATER;


{
 *  RTPRssmSendChunkAndDecrRefCount()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSendChunkAndDecrRefCount( rtpr: RTPReassembler; var inChunk: SHChunkRecord; const var inServerEdit: SHServerEditParameters ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmSendLostChunk()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSendLostChunk( rtpr: RTPReassembler; const var inChunkPresentationTime: TimeValue64 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmSendStreamBufferRange()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmSendStreamBufferRange( rtpr: RTPReassembler; var inParams: RTPSendStreamBufferRangeParams ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmClearCachedPackets()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmClearCachedPackets( rtpr: RTPReassembler; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmFillPacketListParams()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmFillPacketListParams( rtpr: RTPReassembler; var inPacketListHead: RTPRssmPacket; inNumWraparounds: SInt32; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmReleasePacketList()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmReleasePacketList( rtpr: RTPReassembler; var inPacketListHead: RTPRssmPacket ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmIncrChunkRefCount()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmIncrChunkRefCount( rtpr: RTPReassembler; var inChunk: SHChunkRecord ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPRssmDecrChunkRefCount()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPRssmDecrChunkRefCount( rtpr: RTPReassembler; var inChunk: SHChunkRecord ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{============================================================================
        RTP Media Packetizer
============================================================================}
const
	kRTPMediaPacketizerType = FOUR_CHAR_CODE('rtpm');

type
	RTPMediaPacketizer = ComponentInstance;
const
	kRTPBaseMediaPacketizerType = FOUR_CHAR_CODE('gnrc');
	kRTP261MediaPacketizerType = FOUR_CHAR_CODE('h261');
	kRTP263PlusMediaPacketizerType = FOUR_CHAR_CODE('263+');
	kRTPAudioMediaPacketizerType = FOUR_CHAR_CODE('soun');
	kRTPQTMediaPacketizerType = FOUR_CHAR_CODE('qtim');
	kRTPPureVoiceMediaPacketizerType = FOUR_CHAR_CODE('Qclp');
	kRTPJPEGMediaPacketizerType = FOUR_CHAR_CODE('jpeg');
	kRTPQDesign2MediaPacketizerType = FOUR_CHAR_CODE('QDM2');
	kRTPSorensonMediaPacketizerType = FOUR_CHAR_CODE('SVQ1');
	kRTPMP3MediaPacketizerType = FOUR_CHAR_CODE('mp3 ');
	kRTPMPEG4AudioMediaPacketizerType = FOUR_CHAR_CODE('mp4a');
	kRTPMPEG4VideoMediaPacketizerType = FOUR_CHAR_CODE('mp4v');
	kRTPAMRMediaPacketizerType = FOUR_CHAR_CODE('amr ');

type
	RTPMPSampleRef = UInt32;
	RTPMPDataReleaseProcPtr = procedure( var inData: UInt8; inRefCon: univ Ptr );
{GPC-ONLY-START}
	RTPMPDataReleaseUPP = UniversalProcPtr; // should be RTPMPDataReleaseProcPtr
{GPC-ONLY-ELSE}
	RTPMPDataReleaseUPP = RTPMPDataReleaseProcPtr;
{GPC-ONLY-FINISH}
const
	kMediaPacketizerCanPackEditRate = 1 shl 0;
	kMediaPacketizerCanPackLayer = 1 shl 1;
	kMediaPacketizerCanPackVolume = 1 shl 2;
	kMediaPacketizerCanPackBalance = 1 shl 3;
	kMediaPacketizerCanPackGraphicsMode = 1 shl 4;
	kMediaPacketizerCanPackEmptyEdit = 1 shl 5;


type
	MediaPacketizerRequirements = record
		mediaType: OSType;              { media type supported (0 for all)}
		dataFormat: OSType;             { data format (e.g., compression) supported (0 for all)}
		capabilityFlags: UInt32;        { ability to handle non-standard track characteristics}
		canPackMatrixType: UInt8;      { can pack any matrix type up to this (identityMatrixType for identity only)}
		reserved1: UInt8;
		reserved2: UInt8;
		reserved3: UInt8;
	end;
	MediaPacketizerRequirementsPtr = ^MediaPacketizerRequirements;
type
	MediaPacketizerInfo = record
		mediaType: OSType;              { media type supported (0 for all)}
		dataFormat: OSType;             { data format (e.g., compression) supported (0 for all)}
		vendor: OSType;                 { manufacturer of this packetizer (e.g., 'appl' for Apple)}
		capabilityFlags: UInt32;        { ability to handle non-standard track characteristics}
		canPackMatrixType: UInt8;      { can pack any matrix type up to this (identityMatrixType for identity only)}
		reserved1: UInt8;
		reserved2: UInt8;
		reserved3: UInt8;
		characteristicCount: SIGNEDLONG;
		characteristic: array [0..0] of RTPPayloadCharacteristic;

                                              { after the last characteristic, the payload name (defined by the RTPPayloadInfo}
                                              { structure) is present. }
	end;
	MediaPacketizerInfoPtr = ^MediaPacketizerInfo;
type
	MediaPacketizerInfoHandle = ^MediaPacketizerInfoPtr;
{ MediaPacketizerInfo structs are padded to 32 bits }
const
	kMediaPacketizerInfoPadUpToBytes = 4;

const
	kRTPMediaPacketizerInfoRezType = FOUR_CHAR_CODE('pcki');


{
 *  QTSFindMediaPacketizer()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSFindMediaPacketizer( inPacketizerinfo: MediaPacketizerRequirementsPtr; inSampleDescription: SampleDescriptionHandle; inSortInfo: RTPPayloadSortRequestPtr; var outPacketizerList: QTAtomContainer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSFindMediaPacketizerForTrack()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSFindMediaPacketizerForTrack( inTrack: Track; inSampleDescriptionIndex: SIGNEDLONG; inSortInfo: RTPPayloadSortRequestPtr; var outPacketizerList: QTAtomContainer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSFindMediaPacketizerForPayloadID()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSFindMediaPacketizerForPayloadID( payloadID: SIGNEDLONG; inSortInfo: RTPPayloadSortRequestPtr; var outPacketizerList: QTAtomContainer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSFindMediaPacketizerForPayloadName()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSFindMediaPacketizerForPayloadName( payloadName: ConstCStringPtr; inSortInfo: RTPPayloadSortRequestPtr; var outPacketizerList: QTAtomContainer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ flags for RTPMPInitialize}
const
	kRTPMPRealtimeModeFlag = $00000001;

{ flags for RTPMPSampleDataParams}
const
	kRTPMPSyncSampleFlag = $00000001;
	kRTPMPRespectDurationFlag = $00000002;

type
	RTPMPSampleDataParamsPtr = ^RTPMPSampleDataParams;
	RTPMPSampleDataParams = record
		version: UInt32;
		timeStamp: UInt32;
		duration: UInt32;               { 0 = unknown duration}
		playOffset: UInt32;
		playRate: Fixed;
		flags: SInt32;
		sampleDescSeed: UInt32;
		sampleDescription: Handle;
		sampleRef: RTPMPSampleRef;
		dataLength: UInt32;
		data: {const} UInt8Ptr;
		releaseProc: RTPMPDataReleaseUPP;
		refCon: UnivPtr;
	end;
{ out flags for idle, RTPMPSetSampleData, and RTPMPFlush}
const
	kRTPMPStillProcessingData = $00000001; { not done with data you've got}

type
	RTPMPPayloadTypeParamsPtr = ^RTPMPPayloadTypeParams;
	RTPMPPayloadTypeParams = record
		flags: UInt32;
		payloadNumber: UInt32;
		nameLength: SInt16;             { in: size of payloadName buffer (counting null terminator) -- this will be reset to needed length and paramErr returned if too small }
		payloadName: CStringPtr;            { caller must provide buffer }
	end;
{-----------------------------------------
    RTP Media Packetizer Info selectors
-----------------------------------------}
{ info selectors - get only }
const
	kRTPMPPayloadTypeInfo = FOUR_CHAR_CODE('rtpp'); { RTPMPPayloadTypeParams* }
	kRTPMPRTPTimeScaleInfo = FOUR_CHAR_CODE('rtpt'); { TimeScale* }
	kRTPMPRequiredSampleDescriptionInfo = FOUR_CHAR_CODE('sdsc'); { SampleDescriptionHandle* }
	kRTPMPMinPayloadSize = FOUR_CHAR_CODE('mins'); { UInt32* in bytes, does not include rtp header; default is 0 }
	kRTPMPMinPacketDuration = FOUR_CHAR_CODE('mind'); { UInt3* in milliseconds; default is no min required }
	kRTPMPSuggestedRepeatPktCountInfo = FOUR_CHAR_CODE('srpc'); { UInt32* }
	kRTPMPSuggestedRepeatPktSpacingInfo = FOUR_CHAR_CODE('srps'); { UInt32* in milliseconds }
	kRTPMPMaxPartialSampleSizeInfo = FOUR_CHAR_CODE('mpss'); { UInt32* in bytes }
	kRTPMPPreferredBufferDelayInfo = FOUR_CHAR_CODE('prbd'); { UInt32* in milliseconds }
	kRTPMPPayloadNameInfo = FOUR_CHAR_CODE('name'); { StringPtr }
	kRTPInfo_FormatString = FOUR_CHAR_CODE('fmtp'); { char **, caller allocates ptr, callee disposes }

{-----------------------------------------
    RTP Media Packetizer Characteristics
-----------------------------------------}
{ also supports relevant ones in Movies.h and QTSToolbox.h }
const
	kRTPMPNoSampleDataRequiredCharacteristic = FOUR_CHAR_CODE('nsdr');
	kRTPMPHasUserSettingsDialogCharacteristic = FOUR_CHAR_CODE('sdlg');
	kRTPMPPrefersReliableTransportCharacteristic = FOUR_CHAR_CODE('rely');
	kRTPMPRequiresOutOfBandDimensionsCharacteristic = FOUR_CHAR_CODE('robd');
	kRTPMPReadsPartialSamplesCharacteristic = FOUR_CHAR_CODE('rpsp');

{-----------------------------------------
    RTP Media Packetizer selectors
-----------------------------------------}
const
	kRTPMPInitializeSelect = $0500;
	kRTPMPPreflightMediaSelect = $0501;
	kRTPMPIdleSelect = $0502;
	kRTPMPSetSampleDataSelect = $0503;
	kRTPMPFlushSelect = $0504;
	kRTPMPResetSelect = $0505;
	kRTPMPSetInfoSelect = $0506;
	kRTPMPGetInfoSelect = $0507;
	kRTPMPSetTimeScaleSelect = $0508;
	kRTPMPGetTimeScaleSelect = $0509;
	kRTPMPSetTimeBaseSelect = $050A;
	kRTPMPGetTimeBaseSelect = $050B;
	kRTPMPHasCharacteristicSelect = $050C;
	kRTPMPSetPacketBuilderSelect = $050E;
	kRTPMPGetPacketBuilderSelect = $050F;
	kRTPMPSetMediaTypeSelect = $0510;
	kRTPMPGetMediaTypeSelect = $0511;
	kRTPMPSetMaxPacketSizeSelect = $0512;
	kRTPMPGetMaxPacketSizeSelect = $0513;
	kRTPMPSetMaxPacketDurationSelect = $0514;
	kRTPMPGetMaxPacketDurationSelect = $0515; { for export component and apps who want to}
                                        { access dialogs for Media-specific settings}
                                        { (such as Pure Voice interleave factor)}
	kRTPMPDoUserDialogSelect = $0516;
	kRTPMPSetSettingsFromAtomContainerAtAtomSelect = $0517;
	kRTPMPGetSettingsIntoAtomContainerAtAtomSelect = $0518;
	kRTPMPGetSettingsAsTextSelect = $0519;
	kRTPMPGetSettingsSelect = $051C;
	kRTPMPSetSettingsSelect = $051D;

{-----------------------------------------
    RTP Media Packetizer functions
-----------------------------------------}

{
 *  RTPMPInitialize()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPInitialize( rtpm: RTPMediaPacketizer; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ return noErr if you can handle this media }
{
 *  RTPMPPreflightMedia()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPPreflightMedia( rtpm: RTPMediaPacketizer; inMediaType: OSType; inSampleDescription: SampleDescriptionHandle ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   do work here if you need to - give up time periodically
   if you're doing time consuming operations
}
{
 *  RTPMPIdle()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPIdle( rtpm: RTPMediaPacketizer; inFlags: SInt32; var outFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   caller owns the RTPMPSampleDataParams struct
   media Packetizer must copy any fields of the struct it wants to keep
   media Packetizer must call release proc when done with the data
   you can do the processing work here if it does not take up too
   much cpu time - otherwise do it in idle
}
{
 *  RTPMPSetSampleData()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPSetSampleData( rtpm: RTPMediaPacketizer; const var inSampleData: RTPMPSampleDataParams; var outFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   send everything you have buffered - you will get idles while
   you set the kRTPMPStillProcessingData flag here and in idle
}
{
 *  RTPMPFlush()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPFlush( rtpm: RTPMediaPacketizer; inFlags: SInt32; var outFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   dispose of anything buffered and get rid of state
   do not send the buffered data (because presumably
   there is no connection for you to send on)
   state should be the same as if you were just initialized
}
{
 *  RTPMPReset()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPReset( rtpm: RTPMediaPacketizer; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{-----------------------------------------
    RTP Media Packetizer get / set functions
-----------------------------------------}
{
 *  RTPMPSetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPSetInfo( rtpm: RTPMediaPacketizer; inSelector: OSType; ioParams: {const} univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPGetInfo( rtpm: RTPMediaPacketizer; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPSetTimeScale()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPSetTimeScale( rtpm: RTPMediaPacketizer; inTimeScale: TimeScale ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetTimeScale()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPGetTimeScale( rtpm: RTPMediaPacketizer; var outTimeScale: TimeScale ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPSetTimeBase()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPSetTimeBase( rtpm: RTPMediaPacketizer; inTimeBase: TimeBase ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetTimeBase()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPGetTimeBase( rtpm: RTPMediaPacketizer; var outTimeBase: TimeBase ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPHasCharacteristic()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPHasCharacteristic( rtpm: RTPMediaPacketizer; inSelector: OSType; var outHasIt: Boolean ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPSetPacketBuilder()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPSetPacketBuilder( rtpm: RTPMediaPacketizer; inPacketBuilder: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetPacketBuilder()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPGetPacketBuilder( rtpm: RTPMediaPacketizer; var outPacketBuilder: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPSetMediaType()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPSetMediaType( rtpm: RTPMediaPacketizer; inMediaType: OSType ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetMediaType()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPGetMediaType( rtpm: RTPMediaPacketizer; var outMediaType: OSType ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ size is in bytes}
{
 *  RTPMPSetMaxPacketSize()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPSetMaxPacketSize( rtpm: RTPMediaPacketizer; inMaxPacketSize: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetMaxPacketSize()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPGetMaxPacketSize( rtpm: RTPMediaPacketizer; var outMaxPacketSize: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ duration is in milliseconds}
{
 *  RTPMPSetMaxPacketDuration()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPSetMaxPacketDuration( rtpm: RTPMediaPacketizer; inMaxPacketDuration: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetMaxPacketDuration()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPGetMaxPacketDuration( rtpm: RTPMediaPacketizer; var outMaxPacketDuration: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPDoUserDialog()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPDoUserDialog( rtpm: RTPMediaPacketizer; inFilterUPP: ModalFilterUPP; var canceled: Boolean ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPSetSettingsFromAtomContainerAtAtom()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPSetSettingsFromAtomContainerAtAtom( rtpm: RTPMediaPacketizer; inContainer: QTAtomContainer; inParentAtom: QTAtom ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetSettingsIntoAtomContainerAtAtom()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPGetSettingsIntoAtomContainerAtAtom( rtpm: RTPMediaPacketizer; inOutContainer: QTAtomContainer; inParentAtom: QTAtom ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetSettingsAsText()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPMPGetSettingsAsText( rtpm: RTPMediaPacketizer; var text: Handle ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPGetSettings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 *    Windows:          in QTSClient.lib 5.0 and later
 }
function RTPMPGetSettings( rtpm: RTPMediaPacketizer; var outSettings: QTAtomContainer; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPMPSetSettings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 *    Windows:          in QTSClient.lib 5.0 and later
 }
function RTPMPSetSettings( rtpm: RTPMediaPacketizer; inSettings: QTAtomSpecPtr; inFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{============================================================================
        RTP Packet Builder
============================================================================}
const
	kRTPPacketBuilderType = FOUR_CHAR_CODE('rtpb');


type
	RTPPacketBuilder = ComponentInstance;
	RTPPacketGroupRef = ^OpaqueRTPPacketGroupRef; { an opaque type }
	OpaqueRTPPacketGroupRef = record end;
	RTPPacketRef = ^OpaqueRTPPacketRef; { an opaque type }
	OpaqueRTPPacketRef = record end;
	RTPPacketRepeatedDataRef = ^OpaqueRTPPacketRepeatedDataRef; { an opaque type }
	OpaqueRTPPacketRepeatedDataRef = record end;
{ flags for RTPPBBegin/EndPacket, RTPPBBegin/EndPacketGroup}
const
	kRTPPBSetMarkerFlag = $00000001;
	kRTPPBRepeatPacketFlag = $00000002;
	kRTPPBSyncSampleFlag = $00010000;
	kRTPPBBFrameFlag = $00020000;
	kRTPPBDontSendFlag = $10000000; { when set in EndPacketGroup, will not add group}

const
	kRTPPBUnknownPacketMediaDataLength = 0;

{ flags for RTPPBGetSampleData}
const
	kRTPPBEndOfDataFlag = $00000001;


type
	RTPPBCallbackProcPtr = procedure( inSelector: OSType; ioParams: univ Ptr; inRefCon: univ Ptr );
{GPC-ONLY-START}
	RTPPBCallbackUPP = UniversalProcPtr; // should be RTPPBCallbackProcPtr
{GPC-ONLY-ELSE}
	RTPPBCallbackUPP = RTPPBCallbackProcPtr;
{GPC-ONLY-FINISH}
{-----------------------------------------
    RTP Packet Builder selectors
-----------------------------------------}
const
	kRTPPBBeginPacketGroupSelect = $0500;
	kRTPPBEndPacketGroupSelect = $0501;
	kRTPPBBeginPacketSelect = $0502;
	kRTPPBEndPacketSelect = $0503;
	kRTPPBAddPacketLiteralDataSelect = $0504;
	kRTPPBAddPacketSampleDataSelect = $0505;
	kRTPPBAddPacketRepeatedDataSelect = $0506;
	kRTPPBReleaseRepeatedDataSelect = $0507;
	kRTPPBSetPacketSequenceNumberSelect = $0508;
	kRTPPBGetPacketSequenceNumberSelect = $0509;
	kRTPPBSetCallbackSelect = $050A;
	kRTPPBGetCallbackSelect = $050B;
	kRTPPBSetInfoSelect = $050C;
	kRTPPBGetInfoSelect = $050D;
	kRTPPBSetPacketTimeStampOffsetSelect = $050E;
	kRTPPBGetPacketTimeStampOffsetSelect = $050F;
	kRTPPBAddPacketSampleData64Select = $0510;
	kRTPPBGetSampleDataSelect = $0511;
	kRTPPBAddRepeatPacketSelect = $0512;

{-----------------------------------------
    RTP Packet Builder functions
-----------------------------------------}
{
 *  RTPPBBeginPacketGroup()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBBeginPacketGroup( rtpb: RTPPacketBuilder; inFlags: SInt32; inTimeStamp: UInt32; var outPacketGroup: RTPPacketGroupRef ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBEndPacketGroup()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBEndPacketGroup( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBBeginPacket()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBBeginPacket( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacketMediaDataLength: UInt32; var outPacket: RTPPacketRef ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBEndPacket()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBEndPacket( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; inTransmissionTimeOffset: UInt32; inDuration: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   non-NULL RTPPacketRepeatedDataRef means this data will be repeated later
   pb must return a repeated data ref
}
{
 *  RTPPBAddPacketLiteralData()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBAddPacketLiteralData( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; var inData: UInt8; inDataLength: UInt32; var outDataRef: RTPPacketRepeatedDataRef ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   non-NULL RTPPacketRepeatedDataRef means this data will be repeated later
   pb must return a repeated data ref
}
{
 *  RTPPBAddPacketSampleData()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBAddPacketSampleData( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; var inSampleDataParams: RTPMPSampleDataParams; inSampleOffset: UInt32; inSampleDataLength: UInt32; var outDataRef: RTPPacketRepeatedDataRef ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   non-NULL RTPPacketRepeatedDataRef means this data will be repeated later
   pb must return a repeated data ref
}
{
 *  RTPPBAddPacketSampleData64()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 *    Windows:          in QTSClient.lib 5.0 and later
 }
function RTPPBAddPacketSampleData64( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; var inSampleDataParams: RTPMPSampleDataParams; const var inSampleOffset: UInt64; inSampleDataLength: UInt32; var outDataRef: RTPPacketRepeatedDataRef ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   call to add the repeated data using the ref you got from
   RTPPBAddPacketLiteralData or RTPPBAddPacketSampleData
}
{
 *  RTPPBAddPacketRepeatedData()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBAddPacketRepeatedData( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; inDataRef: RTPPacketRepeatedDataRef ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ call when done with repeated data}
{
 *  RTPPBReleaseRepeatedData()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBReleaseRepeatedData( rtpb: RTPPacketBuilder; inDataRef: RTPPacketRepeatedDataRef ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   seq number is just relative seq number
   don't call if you don't care when seq # is used
}
{
 *  RTPPBSetPacketSequenceNumber()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBSetPacketSequenceNumber( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; inSequenceNumber: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBGetPacketSequenceNumber()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBGetPacketSequenceNumber( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; var outSequenceNumber: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBSetPacketTimeStampOffset()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 *    Windows:          in QTSClient.lib 5.0 and later
 }
function RTPPBSetPacketTimeStampOffset( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; inTimeStampOffset: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBGetPacketTimeStampOffset()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 *    Windows:          in QTSClient.lib 5.0 and later
 }
function RTPPBGetPacketTimeStampOffset( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; var outTimeStampOffset: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBAddRepeatPacket()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 *    Windows:          in QTSClient.lib 5.0 and later
 }
function RTPPBAddRepeatPacket( rtpb: RTPPacketBuilder; inFlags: SInt32; inPacketGroup: RTPPacketGroupRef; inPacket: RTPPacketRef; inTransmissionOffset: TimeValue; inSequenceNumber: UInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
   used for communicating with the caller of the media packetizers if needed
   NOT used for communicating with the media packetizers themselves
}
{
 *  RTPPBSetCallback()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBSetCallback( rtpb: RTPPacketBuilder; inCallback: RTPPBCallbackUPP; inRefCon: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBGetCallback()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBGetCallback( rtpb: RTPPacketBuilder; var outCallback: RTPPBCallbackUPP; var outRefCon: UnivPtr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBSetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBSetInfo( rtpb: RTPPacketBuilder; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBGetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function RTPPBGetInfo( rtpb: RTPPacketBuilder; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RTPPBGetSampleData()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   in QTStreamLib 5.0 and later
 *    Windows:          in QTSClient.lib 5.0 and later
 }
function RTPPBGetSampleData( rtpb: RTPPacketBuilder; var inParams: RTPMPSampleDataParams; const var inStartOffset: UInt64; var outDataBuffer: UInt8; inBytesToRead: UInt32; var outBytesRead: UInt32; var outFlags: SInt32 ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ UPP call backs }
{
 *  NewRTPMPDataReleaseUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewRTPMPDataReleaseUPP( userRoutine: RTPMPDataReleaseProcPtr ): RTPMPDataReleaseUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewRTPPBCallbackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewRTPPBCallbackUPP( userRoutine: RTPPBCallbackProcPtr ): RTPPBCallbackUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeRTPMPDataReleaseUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeRTPMPDataReleaseUPP( userUPP: RTPMPDataReleaseUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeRTPPBCallbackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeRTPPBCallbackUPP( userUPP: RTPPBCallbackUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeRTPMPDataReleaseUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeRTPMPDataReleaseUPP( var inData: UInt8; inRefCon: univ Ptr; userUPP: RTPMPDataReleaseUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeRTPPBCallbackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.3 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeRTPPBCallbackUPP( inSelector: OSType; ioParams: univ Ptr; inRefCon: univ Ptr; userUPP: RTPPBCallbackUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
