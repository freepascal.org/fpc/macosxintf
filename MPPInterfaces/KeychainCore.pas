{
     File:       OSServices/KeychainCore.h
 
     Contains:   *** DEPRECATED *** Keychain low-level Interfaces
 
     Copyright:  (c) 2000-2011 Apple Inc. All rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit KeychainCore;
interface
uses MacTypes,Files,Aliases,CodeFragments,MacErrors,Processes,Events,SecBase;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

(*
  Also defined in SecBase

{ Data structures and types }
type
	SecKeychainRef = ^OpaqueSecKeychainRef; { an opaque type }
	OpaqueSecKeychainRef = record end;
	SecKeychainRefPtr = ^SecKeychainRef;  { when a var xx:SecKeychainRef parameter can be nil, it is changed to xx: SecKeychainRefPtr }
	SecKeychainItemRef = ^OpaqueSecKeychainItemRef; { an opaque type }
	OpaqueSecKeychainItemRef = record end;
	SecKeychainItemRefPtr = ^SecKeychainItemRef;  { when a var xx:SecKeychainItemRef parameter can be nil, it is changed to xx: SecKeychainItemRefPtr }
	SecKeychainSearchRef = ^OpaqueSecKeychainSearchRef; { an opaque type }
	OpaqueSecKeychainSearchRef = record end;
	SecKeychainSearchRefPtr = ^SecKeychainSearchRef;  { when a var xx:SecKeychainSearchRef parameter can be nil, it is changed to xx: SecKeychainSearchRefPtr }
	SecKeychainAttrType = OSType;
	SecKeychainStatus = UInt32;
	SecKeychainAttribute = record
		tag: SecKeychainAttrType;                   { 4-byte attribute tag }
		length: UInt32;                 { Length of attribute data }
		data: UnivPtr;                   { Pointer to attribute data }
	end;
	SecKeychainAttributePtr = ^SecKeychainAttribute;
type
	SecKeychainAttributeListPtr = ^SecKeychainAttributeList;
	SecKeychainAttributeList = record
		count: UInt32;                  { How many attributes in the array }
		attr: SecKeychainAttributePtr;                { Pointer to first attribute in array }
	end;
*)

type
	KCRef = SecKeychainRef;
	KCItemRef = SecKeychainItemRef;
	KCSearchRef = SecKeychainSearchRef;
	KCRefPtr = ^KCRef;
	KCItemRefPtr = ^KCItemRef;
	KCSearchRefPtr = ^KCSearchRef;
	KCAttribute = SecKeychainAttribute;
	KCAttributePtr = ^KCAttribute;
	KCAttributeList = SecKeychainAttributeList;
	KCAttributeListPtr = ^KCAttributeList;
	KCAttrType = SecKeychainAttrType;
	KCStatus = SecKeychainStatus;
	KCEvent = UInt16;
const
	kIdleKCEvent = 0;    { null event }
	kLockKCEvent = 1;    { a keychain was locked }
	kUnlockKCEvent = 2;    { a keychain was unlocked }
	kAddKCEvent = 3;    { an item was added to a keychain }
	kDeleteKCEvent = 4;    { an item was deleted from a keychain }
	kUpdateKCEvent = 5;    { an item was updated }
	kPasswordChangedKCEvent = 6;    { the keychain identity was changed }
	kSystemKCEvent = 8;    { the keychain client can process events }
	kDefaultChangedKCEvent = 9;    { the default keychain was changed }
	kDataAccessKCEvent = 10;   { a process has accessed a keychain item's data }
	kKeychainListChangedKCEvent = 11;    { the list of keychains has changed }

type
	KCEventMask = UInt16;
const
	kIdleKCEventMask = 1 shl kIdleKCEvent;
	kLockKCEventMask = 1 shl kLockKCEvent;
	kUnlockKCEventMask = 1 shl kUnlockKCEvent;
	kAddKCEventMask = 1 shl kAddKCEvent;
	kDeleteKCEventMask = 1 shl kDeleteKCEvent;
	kUpdateKCEventMask = 1 shl kUpdateKCEvent;
	kPasswordChangedKCEventMask = 1 shl kPasswordChangedKCEvent;
	kSystemEventKCEventMask = 1 shl kSystemKCEvent;
	kDefaultChangedKCEventMask = 1 shl kDefaultChangedKCEvent;
	kDataAccessKCEventMask = 1 shl kDataAccessKCEvent;
	kEveryKCEventMask = $FFFF; { all of the above}

type
	AFPServerSignature = packed array [0..15] of UInt8;
	AFPServerSignaturePtr = ^AFPServerSignature; { when a VAR xx: AFPServerSignature parameter can be nil, it is changed to xx: AFPServerSignaturePtr }
	KCPublicKeyHash = packed array [0..19] of UInt8;
type
	KCCallbackInfoPtr = ^KCCallbackInfo;
	KCCallbackInfo = record
		version: UInt32;
		item: KCItemRef;
		processID: array [0..1] of SInt32;           { unavailable on Mac OS X}
		event: array [0..4] of SInt32;               { unavailable on Mac OS X}
		keychain: KCRef;
	end;
const
	kUnlockStateKCStatus = 1;
	kRdPermKCStatus = 2;
	kWrPermKCStatus = 4;


const
	kCertificateKCItemClass = FOUR_CHAR_CODE('cert'); { Certificate }
	kAppleSharePasswordKCItemClass = FOUR_CHAR_CODE('ashp'); { Appleshare password }
	kInternetPasswordKCItemClass = FOUR_CHAR_CODE('inet'); { Internet password }
	kGenericPasswordKCItemClass = FOUR_CHAR_CODE('genp'); { Generic password }


type
	KCItemClass = FourCharCode;
const
{ Common attributes }
	kClassKCItemAttr = FOUR_CHAR_CODE('clas'); { Item class (KCItemClass) }
	kCreationDateKCItemAttr = FOUR_CHAR_CODE('cdat'); { Date the item was created (UInt32) }
	kModDateKCItemAttr = FOUR_CHAR_CODE('mdat'); { Last time the item was updated (UInt32) }
	kDescriptionKCItemAttr = FOUR_CHAR_CODE('desc'); { User-visible description string (string) }
	kCommentKCItemAttr = FOUR_CHAR_CODE('icmt'); { User's comment about the item (string) }
	kCreatorKCItemAttr = FOUR_CHAR_CODE('crtr'); { Item's creator (OSType) }
	kTypeKCItemAttr = FOUR_CHAR_CODE('type'); { Item's type (OSType) }
	kScriptCodeKCItemAttr = FOUR_CHAR_CODE('scrp'); { Script code for all strings (ScriptCode) }
	kLabelKCItemAttr = FOUR_CHAR_CODE('labl'); { Item label (string) }
	kInvisibleKCItemAttr = FOUR_CHAR_CODE('invi'); { Invisible (boolean) }
	kNegativeKCItemAttr = FOUR_CHAR_CODE('nega'); { Negative (boolean) }
	kCustomIconKCItemAttr = FOUR_CHAR_CODE('cusi'); { Custom icon (boolean) }
	kAccountKCItemAttr = FOUR_CHAR_CODE('acct'); { User account (string) }
                                        { Unique Generic password attributes }
	kServiceKCItemAttr = FOUR_CHAR_CODE('svce'); { Service (string) }
	kGenericKCItemAttr = FOUR_CHAR_CODE('gena'); { User-defined attribute (untyped bytes) }
                                        { Unique Internet password attributes }
	kSecurityDomainKCItemAttr = FOUR_CHAR_CODE('sdmn'); { Security domain (string) }
	kServerKCItemAttr = FOUR_CHAR_CODE('srvr'); { Server's domain name or IP address (string) }
	kAuthTypeKCItemAttr = FOUR_CHAR_CODE('atyp'); { Authentication Type (KCAuthType) }
	kPortKCItemAttr = FOUR_CHAR_CODE('port'); { Port (UInt16) }
	kPathKCItemAttr = FOUR_CHAR_CODE('path'); { Path (string) }
                                        { Unique Appleshare password attributes }
	kVolumeKCItemAttr = FOUR_CHAR_CODE('vlme'); { Volume (string) }
	kAddressKCItemAttr = FOUR_CHAR_CODE('addr'); { Server address (IP or domain name) or zone name (string) }
	kSignatureKCItemAttr = FOUR_CHAR_CODE('ssig'); { Server signature block (AFPServerSignature) }
                                        { Unique AppleShare and Internet attributes }
	kProtocolKCItemAttr = FOUR_CHAR_CODE('ptcl'); { Protocol (KCProtocolType) }
                                        { Certificate attributes }
	kSubjectKCItemAttr = FOUR_CHAR_CODE('subj'); { Subject distinguished name (DER-encoded data) }
	kCommonNameKCItemAttr = FOUR_CHAR_CODE('cn  '); { Common Name (UTF8-encoded string) }
	kIssuerKCItemAttr = FOUR_CHAR_CODE('issu'); { Issuer distinguished name (DER-encoded data) }
	kSerialNumberKCItemAttr = FOUR_CHAR_CODE('snbr'); { Certificate serial number (DER-encoded data) }
	kEMailKCItemAttr = FOUR_CHAR_CODE('mail'); { E-mail address (ASCII-encoded string) }
	kPublicKeyHashKCItemAttr = FOUR_CHAR_CODE('hpky'); { Hash of public key (KCPublicKeyHash), 20 bytes max. }
	kIssuerURLKCItemAttr = FOUR_CHAR_CODE('iurl'); { URL of the certificate issuer (ASCII-encoded string) }
                                        { Shared by keys and certificates }
	kEncryptKCItemAttr = FOUR_CHAR_CODE('encr'); { Encrypt (Boolean) }
	kDecryptKCItemAttr = FOUR_CHAR_CODE('decr'); { Decrypt (Boolean) }
	kSignKCItemAttr = FOUR_CHAR_CODE('sign'); { Sign (Boolean) }
	kVerifyKCItemAttr = FOUR_CHAR_CODE('veri'); { Verify (Boolean) }
	kWrapKCItemAttr = FOUR_CHAR_CODE('wrap'); { Wrap (Boolean) }
	kUnwrapKCItemAttr = FOUR_CHAR_CODE('unwr'); { Unwrap (Boolean) }
	kStartDateKCItemAttr = FOUR_CHAR_CODE('sdat'); { Start Date (UInt32) }
	kEndDateKCItemAttr = FOUR_CHAR_CODE('edat'); { End Date (UInt32) }

type
	KCItemAttr = FourCharCode;
const
	kKCAuthTypeNTLM = FOUR_CHAR_CODE('ntlm');
	kKCAuthTypeMSN = FOUR_CHAR_CODE('msna');
	kKCAuthTypeDPA = FOUR_CHAR_CODE('dpaa');
	kKCAuthTypeRPA = FOUR_CHAR_CODE('rpaa');
	kKCAuthTypeHTTPDigest = FOUR_CHAR_CODE('httd');
	kKCAuthTypeDefault = FOUR_CHAR_CODE('dflt');

type
	KCAuthType = FourCharCode;
const
	kKCProtocolTypeFTP = FOUR_CHAR_CODE('ftp ');
	kKCProtocolTypeFTPAccount = FOUR_CHAR_CODE('ftpa');
	kKCProtocolTypeHTTP = FOUR_CHAR_CODE('http');
	kKCProtocolTypeIRC = FOUR_CHAR_CODE('irc ');
	kKCProtocolTypeNNTP = FOUR_CHAR_CODE('nntp');
	kKCProtocolTypePOP3 = FOUR_CHAR_CODE('pop3');
	kKCProtocolTypeSMTP = FOUR_CHAR_CODE('smtp');
	kKCProtocolTypeSOCKS = FOUR_CHAR_CODE('sox ');
	kKCProtocolTypeIMAP = FOUR_CHAR_CODE('imap');
	kKCProtocolTypeLDAP = FOUR_CHAR_CODE('ldap');
	kKCProtocolTypeAppleTalk = FOUR_CHAR_CODE('atlk');
	kKCProtocolTypeAFP = FOUR_CHAR_CODE('afp ');
	kKCProtocolTypeTelnet = FOUR_CHAR_CODE('teln');

type
	KCProtocolType = FourCharCode;
	KCCertAddOptions = UInt32;
const
	kSecOptionReserved = $000000FF; { First byte reserved for SecOptions flags }
	kCertUsageShift = 8;    { start at bit 8 }
	kCertUsageSigningAdd = 1 shl (kCertUsageShift + 0);
	kCertUsageSigningAskAndAdd = 1 shl (kCertUsageShift + 1);
	kCertUsageVerifyAdd = 1 shl (kCertUsageShift + 2);
	kCertUsageVerifyAskAndAdd = 1 shl (kCertUsageShift + 3);
	kCertUsageEncryptAdd = 1 shl (kCertUsageShift + 4);
	kCertUsageEncryptAskAndAdd = 1 shl (kCertUsageShift + 5);
	kCertUsageDecryptAdd = 1 shl (kCertUsageShift + 6);
	kCertUsageDecryptAskAndAdd = 1 shl (kCertUsageShift + 7);
	kCertUsageKeyExchAdd = 1 shl (kCertUsageShift + 8);
	kCertUsageKeyExchAskAndAdd = 1 shl (kCertUsageShift + 9);
	kCertUsageRootAdd = 1 shl (kCertUsageShift + 10);
	kCertUsageRootAskAndAdd = 1 shl (kCertUsageShift + 11);
	kCertUsageSSLAdd = 1 shl (kCertUsageShift + 12);
	kCertUsageSSLAskAndAdd = 1 shl (kCertUsageShift + 13);
	kCertUsageAllAdd = $7FFFFF00;

type
	KCVerifyStopOn = UInt16;
const
	kPolicyKCStopOn = 0;
	kNoneKCStopOn = 1;
	kFirstPassKCStopOn = 2;
	kFirstFailKCStopOn = 3;

type
	KCCertSearchOptions = UInt32;
const
	kCertSearchShift = 0;    { start at bit 0 }
	kCertSearchSigningIgnored = 0;
	kCertSearchSigningAllowed = 1 shl (kCertSearchShift + 0);
	kCertSearchSigningDisallowed = 1 shl (kCertSearchShift + 1);
	kCertSearchSigningMask = ((kCertSearchSigningAllowed) or (kCertSearchSigningDisallowed));
	kCertSearchVerifyIgnored = 0;
	kCertSearchVerifyAllowed = 1 shl (kCertSearchShift + 2);
	kCertSearchVerifyDisallowed = 1 shl (kCertSearchShift + 3);
	kCertSearchVerifyMask = ((kCertSearchVerifyAllowed) or (kCertSearchVerifyDisallowed));
	kCertSearchEncryptIgnored = 0;
	kCertSearchEncryptAllowed = 1 shl (kCertSearchShift + 4);
	kCertSearchEncryptDisallowed = 1 shl (kCertSearchShift + 5);
	kCertSearchEncryptMask = ((kCertSearchEncryptAllowed) or (kCertSearchEncryptDisallowed));
	kCertSearchDecryptIgnored = 0;
	kCertSearchDecryptAllowed = 1 shl (kCertSearchShift + 6);
	kCertSearchDecryptDisallowed = 1 shl (kCertSearchShift + 7);
	kCertSearchDecryptMask = ((kCertSearchDecryptAllowed) or (kCertSearchDecryptDisallowed));
	kCertSearchWrapIgnored = 0;
	kCertSearchWrapAllowed = 1 shl (kCertSearchShift + 8);
	kCertSearchWrapDisallowed = 1 shl (kCertSearchShift + 9);
	kCertSearchWrapMask = ((kCertSearchWrapAllowed) or (kCertSearchWrapDisallowed));
	kCertSearchUnwrapIgnored = 0;
	kCertSearchUnwrapAllowed = 1 shl (kCertSearchShift + 10);
	kCertSearchUnwrapDisallowed = 1 shl (kCertSearchShift + 11);
	kCertSearchUnwrapMask = ((kCertSearchUnwrapAllowed) or (kCertSearchUnwrapDisallowed));
	kCertSearchPrivKeyRequired = 1 shl (kCertSearchShift + 12);
	kCertSearchAny = 0;

{ Other constants }
const
	kAnyPort = 0;

const
	kAnyProtocol = 0;
	kAnyAuthType = 0;

{ Opening and getting information about the Keychain Manager }
{
 *  KCGetKeychainManagerVersion()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainGetVersion
 *  
 *  Availability:
 *    Mac OS X:         not available but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCGetKeychainManagerVersion( var returnVers: UInt32 ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{$ifc TARGET_RT_MAC_CFM}
{
        KeychainManagerAvailable() is a macro/inline available only in C/C++.  
        To get the same functionality from pascal or assembly, you need
        to test if KCGetKeychainManagerVersion function is not NULL.  For instance:
        
            gKeychainManagerAvailable = FALSE;
            IF @KCGetKeychainManagerVersion <> kUnresolvedCFragSymbolAddress THEN
                gKeychainManagerAvailable = TRUE;
            end
    
    }
{$elsec}
  {$ifc TARGET_RT_MAC_MACHO}
    { Keychain is always available on OS X }
//    #define KeychainManagerAvailable()  (true)
  {$endc}
{$endc}  {  }

{ Managing the Human Interface }
{
 *  KCSetInteractionAllowed()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainSetUserInteractionAllowed
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCSetInteractionAllowed( state: Boolean ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCIsInteractionAllowed()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainGetUserInteractionAllowed
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCIsInteractionAllowed: Boolean;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Creating references to keychains }
{$ifc not TARGET_CPU_64}
{
 *  KCMakeKCRefFromFSSpec()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainOpen
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCMakeKCRefFromFSSpec( var keychainFSSpec: FSSpec; var keychain: KCRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_5,__IPHONE_NA,__IPHONE_NA);


{$endc} {not TARGET_CPU_64}

{
 *  KCMakeKCRefFromFSRef()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainOpen
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCMakeKCRefFromFSRef( var keychainFSRef: FSRef; var keychain: KCRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCMakeKCRefFromAlias()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainOpen
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCMakeKCRefFromAlias( keychainAlias: AliasHandle; var keychain: KCRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCMakeAliasFromKCRef()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainOpen
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCMakeAliasFromKCRef( keychain: KCRef; var keychainAlias: AliasHandle ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCReleaseKeychain()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use CFRelease when releasing SecKeychainRef objects
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCReleaseKeychain( var keychain: KCRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Specifying the default keychain }
{
 *  KCGetDefaultKeychain()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainCopyDefault
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCGetDefaultKeychain( var keychain: KCRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCSetDefaultKeychain()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainSetDefault
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCSetDefaultKeychain( keychain: KCRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Getting information about a keychain }
{
 *  KCGetStatus()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainGetStatus
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCGetStatus( keychain: KCRef { can be NULL }; var keychainStatus: UInt32 ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCGetKeychain()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemCopyKeychain
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCGetKeychain( item: KCItemRef; var keychain: KCRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCGetKeychainName()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainGetPath
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCGetKeychainName( keychain: KCRef; keychainName: StringPtr ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Enumerating available keychains }
{
 *  KCCountKeychains()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainCopySearchList
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCCountKeychains: UInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCGetIndKeychain()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainCopySearchList
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCGetIndKeychain( index: UInt16; var keychain: KCRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


type
	KCCallbackProcPtr = function( keychainEvent: KCEvent; var info: KCCallbackInfo; userContext: univ Ptr ): OSStatus;
{GPC-ONLY-START}
	KCCallbackUPP = UniversalProcPtr; // should be KCCallbackProcPtr
{GPC-ONLY-ELSE}
	KCCallbackUPP = KCCallbackProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewKCCallbackUPP()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewKCCallbackUPP( userRoutine: KCCallbackProcPtr ): KCCallbackUPP;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);

{
 *  DisposeKCCallbackUPP()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeKCCallbackUPP( userUPP: KCCallbackUPP );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);

{
 *  InvokeKCCallbackUPP()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeKCCallbackUPP( keychainEvent: KCEvent; var info: KCCallbackInfo; userContext: univ Ptr; userUPP: KCCallbackUPP ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);

{ High-level interface for retrieving passwords }
{
 *  KCFindAppleSharePassword()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainFindInternetPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCFindAppleSharePassword( serverSignature: AFPServerSignaturePtr { can be NULL }; serverAddress: ConstStringPtr { can be NULL }; serverName: ConstStringPtr { can be NULL }; volumeName: ConstStringPtr { can be NULL }; accountName: ConstStringPtr { can be NULL }; maxLength: UInt32; passwordData: univ Ptr; var actualLength: UInt32; item: KCItemRefPtr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCFindInternetPassword()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainFindInternetPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCFindInternetPassword( serverName: ConstStringPtr { can be NULL }; securityDomain: ConstStringPtr { can be NULL }; accountName: ConstStringPtr { can be NULL }; port: UInt16; protocol: OSType; authType: OSType; maxLength: UInt32; passwordData: univ Ptr; var actualLength: UInt32; item: KCItemRefPtr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCFindInternetPasswordWithPath()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainFindInternetPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCFindInternetPasswordWithPath( serverName: ConstStringPtr { can be NULL }; securityDomain: ConstStringPtr { can be NULL }; accountName: ConstStringPtr { can be NULL }; path: ConstStringPtr { can be NULL }; port: UInt16; protocol: OSType; authType: OSType; maxLength: UInt32; passwordData: univ Ptr; var actualLength: UInt32; item: KCItemRefPtr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCFindGenericPassword()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainFindGenericPassword
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCFindGenericPassword( serviceName: ConstStringPtr { can be NULL }; accountName: ConstStringPtr { can be NULL }; maxLength: UInt32; passwordData: univ Ptr; var actualLength: UInt32; item: KCItemRefPtr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Keychain Manager callbacks }
{
 *  KCAddCallback()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainAddCallback
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCAddCallback( callbackProc: KCCallbackUPP; eventMask: KCEventMask; userContext: univ Ptr ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCRemoveCallback()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainRemoveCallback
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCRemoveCallback( callbackProc: KCCallbackUPP ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Creating and editing a keychain item }
{
 *  KCNewItem()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemCreateFromContent
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCNewItem( itemClass: KCItemClass; itemCreator: OSType; length: UInt32; data: {const} univ Ptr; var item: KCItemRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCSetAttribute()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemModifyAttributesAndData
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCSetAttribute( item: KCItemRef; var attr: KCAttribute ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCGetAttribute()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemCopyAttributesAndData
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCGetAttribute( item: KCItemRef; var attr: KCAttribute; var actualLength: UInt32 ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCSetData()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemModifyAttributesAndData
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCSetData( item: KCItemRef; length: UInt32; data: {const} univ Ptr ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Managing keychain items }
{
 *  KCUpdateItem()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemModifyAttributesAndData
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCUpdateItem( item: KCItemRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCReleaseItem()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use CFRelease when releasing SecKeychainItemRef objects
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCReleaseItem( var item: KCItemRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCCopyItem()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemCreateCopy
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 2.0 and later
 }
function KCCopyItem( item: KCItemRef; destKeychain: KCRef; var copy: KCItemRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Searching and enumerating keychain items }
{
 *  KCFindFirstItem()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainSearchCreateFromAttributes /
 *    SecKeychainSearchCopyNext
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCFindFirstItem( keychain: KCRef { can be NULL }; {const} attrList: KCAttributeListPtr { can be NULL }; var search: KCSearchRef; var item: KCItemRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCFindNextItem()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainSearchCopyNext
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCFindNextItem( search: KCSearchRef; var item: KCItemRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCReleaseSearch()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use CFRelease when releasing SecKeychainSearchRef objects
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCReleaseSearch( var search: KCSearchRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Managing keychain items }
{
 *  KCDeleteItem()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemDelete
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCDeleteItem( item: KCItemRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{
 *  KCGetData()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainItemCopyAttributesAndData
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCGetData( item: KCItemRef; maxLength: UInt32; data: univ Ptr; var actualLength: UInt32 ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{ Locking a keychain }
{
 *  KCLock()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use SecKeychainLock
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework but deprecated in 10.6
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in KeychainLib 1.0 and later
 }
function KCLock( keychain: KCRef ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);


{$endc} {TARGET_OS_MAC}

end.
