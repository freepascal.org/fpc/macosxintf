{
 *  AXError.h
 *
 *  Copyright (c) 2002 Apple Computer, Inc. All rights reserved.
 *
 }
{  Unit name changed to AXErrors to avoid conflict with AXError type }
{  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, 2004 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }


unit AXErrors;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

const
	kAXErrorSuccess = 0;
	kAXErrorFailure = -25200;
	kAXErrorIllegalArgument = -25201;
	kAXErrorInvalidUIElement = -25202;
	kAXErrorInvalidUIElementObserver = -25203;
	kAXErrorCannotComplete = -25204;
	kAXErrorAttributeUnsupported = -25205;
	kAXErrorActionUnsupported = -25206;
	kAXErrorNotificationUnsupported = -25207;
	kAXErrorNotImplemented = -25208;
	kAXErrorNotificationAlreadyRegistered = -25209;
	kAXErrorNotificationNotRegistered = -25210;
	kAXErrorAPIDisabled = -25211;
	kAXErrorNoValue = -25212;
	kAXErrorParameterizedAttributeUnsupported = -25213;
	kAXErrorNotEnoughPrecision = -25214;
type
	AXError = SInt32;

{$endc} {TARGET_OS_MAC}

end.
