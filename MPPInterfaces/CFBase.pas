{	CFBase.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFBase;
interface
uses MacTypes;
{$ALIGN POWER}

type
  { type moved here to avoid circular dependency between Files and CFURL }
	CFURLRef = ^SInt32; { an opaque 32-bit type }
	CFURLRefPtr = ^CFURLRef;

const kCFCoreFoundationVersionNumber: Float64;

{$ifc TARGET_OS_MAC}
const
	kCFCoreFoundationVersionNumber10_0 = 196.40;
const
	kCFCoreFoundationVersionNumber10_0_3 = 196.50;
const
	kCFCoreFoundationVersionNumber10_1 = 226.00;
const
	kCFCoreFoundationVersionNumber10_1_1 = 226.00;
{ Note the next three do not follow the usual numbering policy from the base release }
const
	kCFCoreFoundationVersionNumber10_1_2 = 227.20;
const
	kCFCoreFoundationVersionNumber10_1_3 = 227.20;
const
	kCFCoreFoundationVersionNumber10_1_4 = 227.30;
const
	kCFCoreFoundationVersionNumber10_2 = 263.00;
const
	kCFCoreFoundationVersionNumber10_2_1 = 263.10;
const
	kCFCoreFoundationVersionNumber10_2_2 = 263.10;
const
	kCFCoreFoundationVersionNumber10_2_3 = 263.30;
const
	kCFCoreFoundationVersionNumber10_2_4 = 263.30;
const
	kCFCoreFoundationVersionNumber10_2_5 = 263.50;
const
	kCFCoreFoundationVersionNumber10_2_6 = 263.50;
const
	kCFCoreFoundationVersionNumber10_2_7 = 263.50;
const
	kCFCoreFoundationVersionNumber10_2_8 = 263.50;
const
	kCFCoreFoundationVersionNumber10_3 = 299.00;
const
	kCFCoreFoundationVersionNumber10_3_1 = 299.00;
const
	kCFCoreFoundationVersionNumber10_3_2 = 299.00;
const
	kCFCoreFoundationVersionNumber10_3_3 = 299.30;
const
	kCFCoreFoundationVersionNumber10_3_4 = 299.31;
const
	kCFCoreFoundationVersionNumber10_3_5 = 299.31;
const
	kCFCoreFoundationVersionNumber10_3_6 = 299.32;
const
	kCFCoreFoundationVersionNumber10_3_7 = 299.33;
const
	kCFCoreFoundationVersionNumber10_3_8 = 299.33;
const
	kCFCoreFoundationVersionNumber10_3_9 = 299.35;
const
	kCFCoreFoundationVersionNumber10_4 = 368.00;
const
	kCFCoreFoundationVersionNumber10_4_1 = 368.10;
const
	kCFCoreFoundationVersionNumber10_4_2 = 368.11;
const
	kCFCoreFoundationVersionNumber10_4_3 = 368.18;
const
	kCFCoreFoundationVersionNumber10_4_4_Intel = 368.26;
const
	kCFCoreFoundationVersionNumber10_4_4_PowerPC = 368.25;
const
	kCFCoreFoundationVersionNumber10_4_5_Intel = 368.26;
const
	kCFCoreFoundationVersionNumber10_4_5_PowerPC = 368.25;
const
	kCFCoreFoundationVersionNumber10_4_6_Intel = 368.26;
const
	kCFCoreFoundationVersionNumber10_4_6_PowerPC = 368.25;
const
	kCFCoreFoundationVersionNumber10_4_7 = 368.27;
const
	kCFCoreFoundationVersionNumber10_4_8 = 368.27;
const
	kCFCoreFoundationVersionNumber10_4_9 = 368.28;
const
	kCFCoreFoundationVersionNumber10_4_10 = 368.28;
const
	kCFCoreFoundationVersionNumber10_4_11 = 368.31;
const
	kCFCoreFoundationVersionNumber10_5 = 476.00;
const
	kCFCoreFoundationVersionNumber10_5_1 = 476.00;
const
	kCFCoreFoundationVersionNumber10_5_2 = 476.10;
const
	kCFCoreFoundationVersionNumber10_5_3 = 476.13;
const
	kCFCoreFoundationVersionNumber10_5_4 = 476.14;
const
	kCFCoreFoundationVersionNumber10_5_5 = 476.15;
const
	kCFCoreFoundationVersionNumber10_5_6 = 476.17;
const
	kCFCoreFoundationVersionNumber10_5_7 = 476.18;
const
	kCFCoreFoundationVersionNumber10_5_8 = 476.19;
const
	kCFCoreFoundationVersionNumber10_6 = 550.00;
const
	kCFCoreFoundationVersionNumber10_6_1 = 550.00;
const
	kCFCoreFoundationVersionNumber10_6_2 = 550.13;
const
	kCFCoreFoundationVersionNumber10_6_3 = 550.19;
const
	kCFCoreFoundationVersionNumber10_6_4 = 550.29;
const
	kCFCoreFoundationVersionNumber10_6_5 = 550.42;
const
	kCFCoreFoundationVersionNumber10_6_6 = 550.42;
const
	kCFCoreFoundationVersionNumber10_6_7 = 550.42;
const
	kCFCoreFoundationVersionNumber10_6_8 = 550.43;
const
	kCFCoreFoundationVersionNumber10_7 = 635.00;
const
	kCFCoreFoundationVersionNumber10_7_1 = 635.00;
const
	kCFCoreFoundationVersionNumber10_7_2 = 635.15;
const
	kCFCoreFoundationVersionNumber10_7_3 = 635.19;
const
	kCFCoreFoundationVersionNumber10_7_4 = 635.21;
const
	kCFCoreFoundationVersionNumber10_7_5 = 635.21;
const
	kCFCoreFoundationVersionNumber10_8 = 744.00;
const
	kCFCoreFoundationVersionNumber10_8_1 = 744.00;
const
	kCFCoreFoundationVersionNumber10_8_2 = 744.12;
const
	kCFCoreFoundationVersionNumber10_8_3 = 744.18;
const
	kCFCoreFoundationVersionNumber10_8_4 = 744.19;
{$endc}

{$ifc TARGET_OS_IPHONE}
const
	kCFCoreFoundationVersionNumber_iPhoneOS_2_0 = 478.23;
const
	kCFCoreFoundationVersionNumber_iPhoneOS_2_1 = 478.26;
const
	kCFCoreFoundationVersionNumber_iPhoneOS_2_2 = 478.29;
const
	kCFCoreFoundationVersionNumber_iPhoneOS_3_0 = 478.47;
const
	kCFCoreFoundationVersionNumber_iPhoneOS_3_1 = 478.52;
const
	kCFCoreFoundationVersionNumber_iPhoneOS_3_2 = 478.61;
const
	kCFCoreFoundationVersionNumber_iOS_4_0 = 550.32;
const
	kCFCoreFoundationVersionNumber_iOS_4_1 = 550.38;
const
	kCFCoreFoundationVersionNumber_iOS_4_2 = 550.52;
const
	kCFCoreFoundationVersionNumber_iOS_4_3 = 550.52;
const
	kCFCoreFoundationVersionNumber_iOS_5_0 = 675.00;
const
	kCFCoreFoundationVersionNumber_iOS_5_1 = 690.10;
const
	kCFCoreFoundationVersionNumber_iOS_6_0 = 793.00;
const
	kCFCoreFoundationVersionNumber_iOS_6_1 = 793.00;
{$endc}

type
	CFTypeID = UNSIGNEDLONG;
	CFOptionFlags = UNSIGNEDLONG;
	CFHashCode = UNSIGNEDLONG;
	CFIndex = SIGNEDLONG;
	CFIndexPtr = ^CFIndex;

{ Base "type" of all "CF objects", and polymorphic functions on them }
type
	CFTypeRef = UnivPtr; { an opaque type }
	
{ GK: We need it for passing open arrays of CFTypes in MDQuery.pas }
	CFTypeRefPtr = ^CFTypeRef;
type
	CFStringRef = ^SInt32; { an opaque type }
	CFStringRefPtr = ^CFStringRef;
	CFMutableStringRef = ^SInt32; { an opaque type }
	CFMutableStringRefPtr = ^CFMutableStringRef;

{
        Type to mean any instance of a property list type;
        currently, CFString, CFData, CFNumber, CFBoolean, CFDate,
        CFArray, and CFDictionary.
}
type
	CFPropertyListRef = CFTypeRef;

{ Values returned from comparison functions }
	CFComparisonResult = CFIndex;
const
	kCFCompareLessThan = -1;
	kCFCompareEqualTo = 0;
	kCFCompareGreaterThan = 1;

{ A standard comparison function }
type
	CFComparatorFunction = function( val1: {const} univ Ptr; val2: {const} univ Ptr; context: univ Ptr ): CFComparisonResult;

{ Constant used by some functions to indicate failed searches. }
{ This is of type CFIndex. }
const
	kCFNotFound = -1;


{ Range type }
type
	CFRangePtr = ^CFRange;
	CFRange = record
		location: CFIndex;
		length: CFIndex;
	end;

{MW-ONLY-START}
{$definec CFRangeMake(LOC, LEN) __CFRangeMake(LOC, LEN)}
{MW-ONLY-ELSE}
function CFRangeMake__NAMED____CFRangeMake( loc: CFIndex; len: CFIndex ): CFRange;
{MW-ONLY-FINISH}

{ Private; do not use }
function __CFRangeMake( loc: CFIndex; len: CFIndex ): CFRange;


{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
{ Null representant }

type
	CFNullRef = ^SInt32; { an opaque 32-bit type }

function CFNullGetTypeID: CFTypeID;

const kCFNull: CFNullRef;	// the singleton null instance

{#endif}


{ Allocator API

   Most of the time when specifying an allocator to Create functions, the NULL
   argument indicates "use the default"; this is the same as using kCFAllocatorDefault
   or the return value from CFAllocatorGetDefault().  This assures that you will use
   the allocator in effect at that time.

   You should rarely use kCFAllocatorSystemDefault, the default default allocator.
}
type
	CFAllocatorRef = ^__CFAllocator; { an opaque type }
	__CFAllocator = record end;
	CFAllocatorRefPtr = ^CFAllocatorRef;

{ This is a synonym for NULL, if you'd rather use a named constant. }
const kCFAllocatorDefault: CFAllocatorRef;

{ Default system allocator; you rarely need to use this. }
const kCFAllocatorSystemDefault: CFAllocatorRef;

{ This allocator uses malloc(), realloc(), and free(). This should not be
   generally used; stick to kCFAllocatorDefault whenever possible. This
   allocator is useful as the "bytesDeallocator" in CFData or
   "contentsDeallocator" in CFString where the memory was obtained as a
   result of malloc() type functions.
}
const kCFAllocatorMalloc: CFAllocatorRef;

{ This allocator explicitly uses the default malloc zone, returned by
   malloc_default_zone(). It should only be used when an object is
   safe to be allocated in non-scanned memory.
 }
const kCFAllocatorMallocZone: CFAllocatorRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{ Null allocator which does nothing and allocates no memory. This allocator
   is useful as the "bytesDeallocator" in CFData or "contentsDeallocator"
   in CFString where the memory should not be freed. 
}
const kCFAllocatorNull: CFAllocatorRef;

{ Special allocator argument to CFAllocatorCreate() which means
   "use the functions given in the context to allocate the allocator
   itself as well". 
}
const kCFAllocatorUseContext: CFAllocatorRef;

type
	CFAllocatorRetainCallBack = function( info: {const} univ Ptr ): UnivPtr;
	CFAllocatorReleaseCallBack = procedure( info: {const} univ Ptr );
	CFAllocatorCopyDescriptionCallBack = function( info: {const} univ Ptr ): CFStringRef;
	CFAllocatorAllocateCallBack = function( allocSize: CFIndex; hint: CFOptionFlags; info: univ Ptr ): UnivPtr;
	CFAllocatorReallocateCallBack = function( ptr: univ Ptr; newsize: CFIndex; hint: CFOptionFlags; info: univ Ptr ): UnivPtr;
	CFAllocatorDeallocateCallBack = procedure( ptr: univ Ptr; info: univ Ptr );
	CFAllocatorPreferredSizeCallBack = function( size: CFIndex; hint: CFOptionFlags; info: univ Ptr ): CFIndex;
	CFAllocatorContextPtr = ^CFAllocatorContext;
	CFAllocatorContext = record
		version: CFIndex;
		info: UnivPtr;
		retain: CFAllocatorRetainCallBack;
		release: CFAllocatorReleaseCallBack;        
		copyDescription: CFAllocatorCopyDescriptionCallBack;
		allocate: CFAllocatorAllocateCallBack;
		reallocate: CFAllocatorReallocateCallBack;
		deallocate: CFAllocatorDeallocateCallBack;
		preferredSize: CFAllocatorPreferredSizeCallBack;
	end;

function CFAllocatorGetTypeID: CFTypeID;

{
	CFAllocatorSetDefault() sets the allocator that is used in the current
	thread whenever NULL is specified as an allocator argument. This means
	that most, if not all allocations will go through this allocator. It
	also means that any allocator set as the default needs to be ready to
	deal with arbitrary memory allocation requests; in addition, the size
	and number of requests will change between releases.

	An allocator set as the default will never be released, even if later
	another allocator replaces it as the default. Not only is it impractical
	for it to be released (as there might be caches created under the covers
	that refer to the allocator), in general it's also safer and more
	efficient to keep it around.

	If you wish to use a custom allocator in a context, it's best to provide
	it as the argument to the various creation functions rather than setting
	it as the default. Setting the default allocator is not encouraged.

	If you do set an allocator as the default, either do it for all time in
	your app, or do it in a nested fashion (by restoring the previous allocator
	when you exit your context). The latter might be appropriate for plug-ins
	or libraries that wish to set the default allocator.
}
procedure CFAllocatorSetDefault( allocator: CFAllocatorRef );

function CFAllocatorGetDefault: CFAllocatorRef;

function CFAllocatorCreate( allocator: CFAllocatorRef; var context: CFAllocatorContext ): CFAllocatorRef;

function CFAllocatorAllocate( allocator: CFAllocatorRef; size: CFIndex; hint: CFOptionFlags ): UnivPtr;

function CFAllocatorReallocate( allocator: CFAllocatorRef; ptr: univ Ptr; newsize: CFIndex; hint: CFOptionFlags ): UnivPtr;

procedure CFAllocatorDeallocate( allocator: CFAllocatorRef; ptr: univ Ptr );

function CFAllocatorGetPreferredSizeForSize( allocator: CFAllocatorRef; size: CFIndex; hint: CFOptionFlags ): CFIndex;

procedure CFAllocatorGetContext( allocator: CFAllocatorRef; var context: CFAllocatorContext );


{ Polymorphic CF functions }

function CFGetTypeID( cf: CFTypeRef ): CFTypeID;

function CFCopyTypeIDDescription( type_id: CFTypeID ): CFStringRef;

function CFRetain( cf: CFTypeRef ): CFTypeRef;

procedure CFRelease( cf: CFTypeRef );

function CFAutorelease( arg: CFTypeRef {CF_RELEASES_ARGUMENT} ): CFTypeRef;
CF_AVAILABLE_STARTING(10_9, 7_0);

function CFGetRetainCount( cf: CFTypeRef ): CFIndex;

function CFEqual( cf1: CFTypeRef; cf2: CFTypeRef ): Boolean;

function CFHash( cf: CFTypeRef ): CFHashCode;

function CFCopyDescription( cf: CFTypeRef ): CFStringRef;

function CFGetAllocator( cf: CFTypeRef ): CFAllocatorRef;

// This function is unavailable in ARC mode. Use CFBridgingRelease instead.
{ CF_AUTOMATED_REFCOUNT_UNAVAILABLE }
function CFMakeCollectable( cf: CFTypeRef ): CFTypeRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER; 


end.
