{
 * ColorSync - ColorSyncCMM.h
 * Copyright (c)  2008 Apple Inc.
 * All rights reserved.
 }
{  Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ColorSyncCMM;
interface
uses MacTypes,ColorSyncProfile,ColorSyncTransform,CFBase,CFArray,CFBundle,CFDictionary;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
 * Notes:
 *  - Color conversions are performed by a Color Management Module (CMM) which is a plugin to ColorSync.
 *  - ColorSync contains Apple CMM, which is not replaceable, but third parties can install their own CMMs
 *  - ColorSync provides access to installed CMMs as well as those that can be part of the application bundle.
 *  - CMM can be selected and specified as a preferred CMM per color transform created by the application 
 *  - if the third party CMM fails to perform a task, Apple CMM will take it over
 *  - ColorSyncCMMRef is a light weight wrapper of CFBundleRef
 *  - See /Developer/Examples/ColorSync/DemoCMM
 }

type
	ColorSyncCMMRef = ^OpaqueColorSyncCMMRef; { an opaque type }
	OpaqueColorSyncCMMRef = record end;

function ColorSyncCMMGetTypeID: CFTypeID;
   {
    * returns the CFTypeID for ColorSyncCMMs.
    }

function ColorSyncCMMCreate( cmmBundle: CFBundleRef ): ColorSyncCMMRef;

function ColorSyncCMMGetBundle( cmm: ColorSyncCMMRef ): CFBundleRef; // will return NULL for built-in Apple CMM

function ColorSyncCMMCopyLocalizedName( cmm: ColorSyncCMMRef ): CFStringRef; // needed to get the name of built-in CMM

function ColorSyncCMMCopyCMMIdentifier( cmm: ColorSyncCMMRef ): CFStringRef; // needed to get the identifier of built-in CMM

type
	ColorSyncCMMIterateCallback = function( cmm: ColorSyncCMMRef; userInfo: univ Ptr ): CBool;
   {
    * Note:  If the ColorSyncCMMIterateCallback returns false, the iteration stops
    *
    }

procedure ColorSyncIterateInstalledCMMs( callBack: ColorSyncCMMIterateCallback; userInfo: univ Ptr );
   {
    *  callBack   - pointer to a client provided function
    *  user Info  - (optional) pointer to the userIndo to be passed to the callback
    *
    }


{
* ==========================================================================================
* This part defines the interface for developers of third party CMMs for ColorSync.
* ==========================================================================================
}

type
	CMMInitializeLinkProfileProc = function( profile: ColorSyncMutableProfileRef; profileInfo: CFArrayRef; options: CFDictionaryRef ): CBool;

type
	CMMInitializeTransformProc = function( profile: ColorSyncTransformRef; profileInfo: CFArrayRef; options: CFDictionaryRef ): CBool;

type
	CMMApplyTransformProc = function( transform: ColorSyncTransformRef; width: size_t; height: size_t; dstPlanes: size_t; var dst: univ Ptr; dstDepth: ColorSyncDataDepth; dstFormat: ColorSyncDataLayout; dstBytesPerRow: size_t; srcPlanes: size_t; var src: univ Ptr; srcDepth: ColorSyncDataDepth; srcFormat: ColorSyncDataLayout; srcBytesPerRow: size_t; options: CFDictionaryRef ): CBool;

type
	CMMCreateTransformPropertyProc = function( transform: ColorSyncTransformRef; key: CFTypeRef; options: CFDictionaryRef ): CFTypeRef;

const kCMMInitializeLinkProfileProcName: CFStringRef;     { CMMInitializeLinkProfileProcName   }
const kCMMInitializeTransformProcName: CFStringRef;       { CMMInitializeTransformProcName     }
const kCMMApplyTransformProcName: CFStringRef;            { CMMApplyTransformProcName          }
const kCMMCreateTransformPropertyProcName: CFStringRef;   { CMMCreateTransformPropertyProcName }

{
* Following keys are expected to be present in the CMM bundle info dictionary:
*
* Standard Mac OS X bundle keys:
*              kCFBundleExecutableKey
*              kCFBundleIdentifierKey
*              kCFBundleVersionKey
*              kCFBundleNameKey
*
* CMM specific keys:
*              kCMMInitializeLinkProfileProcName  -  CFStringRef of the name of a CMMInitializeLinkProfile
*                                                    function implemented in the CMM bundle executable.
*
*              kCMMInitializeTransformProcName    -  CFStringRef of the name of a CMMInitializeTransform
*                                                    function implemented in the CMM bundle executable.
*
*              kCMMApplyTransformProcName         -  CFStringRef of the name of a CMMApplyTransform function
*                                                    implemented in the CMM bundle executable.
*
*              kCMMCreateTransformPropertyProcName - CFStringRef of the name of a CMMCreateTransformProperty
*                                                    function implemented in the CMM bundle executable.
*                                                    Optional.
}

{$endc} {TARGET_OS_MAC}

end.
