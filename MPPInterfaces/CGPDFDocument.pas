{ CoreGraphics - CGPDFDocument.h
   Copyright (c) 2000-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFDocument;
interface
uses MacTypes,CFBase,CGBase,CGDataProvider,CGGeometry,CFURL;
{$ALIGN POWER}


type
	CGPDFDocumentRef = ^OpaqueCGPDFDocumentRef; { an opaque type }
	OpaqueCGPDFDocumentRef = record end;


{ Create a PDF document, using `provider' to obtain the document's data. }

function CGPDFDocumentCreateWithProvider( provider: CGDataProviderRef ): CGPDFDocumentRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create a PDF document from `url'. }

function CGPDFDocumentCreateWithURL( url: CFURLRef ): CGPDFDocumentRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRetain(document)', except it doesn't crash (as CFRetain
   does) if `document' is NULL. }

function CGPDFDocumentRetain( document: CGPDFDocumentRef ): CGPDFDocumentRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRelease(document)', except it doesn't crash (as
   CFRelease does) if `document' is NULL. }

procedure CGPDFDocumentRelease( document: CGPDFDocumentRef );
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the major and minor version numbers of `document'. }

procedure CGPDFDocumentGetVersion( document: CGPDFDocumentRef; var majorVersion: SInt32; var minorVersion: SInt32 );
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return true if the PDF file associated with `document' is encrypted;
   false otherwise. If the PDF file is encrypted, then a password must be
   supplied before certain operations are enabled; different passwords may
   enable different operations. }

function CGPDFDocumentIsEncrypted( document: CGPDFDocumentRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Use `password' to decrypt `document' and grant permission for certain
   operations. Returns true if `password' is a valid password; false
   otherwise. }

function CGPDFDocumentUnlockWithPassword( document: CGPDFDocumentRef; password: ConstCStringPtr ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Return true if `document' is unlocked; false otherwise. A document is
   unlocked if it isn't encrypted, or if it is encrypted and a valid
   password was specified with `CGPDFDocumentUnlockWithPassword'. }

function CGPDFDocumentIsUnlocked( document: CGPDFDocumentRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Return true if `document' allows printing; false otherwise. Typically,
   this function returns false only if the document is encrypted and the
   document's current password doesn't grant permission to perform
   printing. }

function CGPDFDocumentAllowsPrinting( document: CGPDFDocumentRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Return true if `document' allows copying; false otherwise. Typically,
   this function returns false only if the document is encrypted and the
   document's current password doesn't grant permission to perform
   copying. }

function CGPDFDocumentAllowsCopying( document: CGPDFDocumentRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Return the number of pages in `document'. }

function CGPDFDocumentGetNumberOfPages( document: CGPDFDocumentRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the page corresponding to `pageNumber', or NULL if no such page
   exists in the document. Pages are numbered starting at 1. }

function CGPDFDocumentGetPage( document: CGPDFDocumentRef; pageNumber: size_t ): CGPDFPageRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the document catalog of `document'. }

function CGPDFDocumentGetCatalog( document: CGPDFDocumentRef ): CGPDFDictionaryRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the info dictionary of `document'. }

function CGPDFDocumentGetInfo( document: CGPDFDocumentRef ): CGPDFDictionaryRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the "file identifier" array of `document'. }

function CGPDFDocumentGetID( document: CGPDFDocumentRef ): CGPDFArrayRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the CFTypeID for CGPDFDocumentRefs. }

function CGPDFDocumentGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{$ifc TARGET_OS_MAC}

{ The following functions are deprecated in favor of the CGPDFPage API. }

{ DEPRECATED; return the media box of page number `page' in `document'. }

function CGPDFDocumentGetMediaBox( document: CGPDFDocumentRef; page: SInt32 ): CGRect;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5,
	__IPHONE_NA, __IPHONE_NA);

{ DEPRECATED; return the crop box of page number `page' in `document'. }

function CGPDFDocumentGetCropBox( document: CGPDFDocumentRef; page: SInt32 ): CGRect;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5,
	__IPHONE_NA, __IPHONE_NA);

{ DEPRECATED; return the bleed box of page number `page' in `document'. }

function CGPDFDocumentGetBleedBox( document: CGPDFDocumentRef; page: SInt32 ): CGRect;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5,
	__IPHONE_NA, __IPHONE_NA);

{ DEPRECATED; return the trim box of page number `page' in `document'. }

function CGPDFDocumentGetTrimBox( document: CGPDFDocumentRef; page: SInt32 ): CGRect;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5,
	__IPHONE_NA, __IPHONE_NA);

{ DEPRECATED; return the art box of page number `page' in `document'. }

function CGPDFDocumentGetArtBox( document: CGPDFDocumentRef; page: SInt32 ): CGRect;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5,
	__IPHONE_NA, __IPHONE_NA);

{ DEPRECATED; return the rotation angle (in degrees) of page number `page'
   in `document'. }

function CGPDFDocumentGetRotationAngle( document: CGPDFDocumentRef; page: SInt32 ): SInt32;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5,
	__IPHONE_NA, __IPHONE_NA);

{$endc}

end.
