{	CFUUID.h
	Copyright (c) 1999-2013, Apple Inc.  All rights reserved.
}
unit CFUUID;
interface
uses MacTypes,CFBase,CFString;
{$ALIGN POWER}


type
	CFUUIDRef = ^__CFUUID; { an opaque type }
	__CFUUID = record end;
	CFUUIDRefPtr = ^CFUUIDRef;

type
	CFUUIDBytes = record
		byte0: UInt8;
		byte1: UInt8;
		byte2: UInt8;
		byte3: UInt8;
		byte4: UInt8;
		byte5: UInt8;
		byte6: UInt8;
		byte7: UInt8;
		byte8: UInt8;
		byte9: UInt8;
		byte10: UInt8;
		byte11: UInt8;
		byte12: UInt8;
		byte13: UInt8;
		byte14: UInt8;
		byte15: UInt8;
	end;
	CFUUIDBytesPtr = ^CFUUIDBytes;
{ The CFUUIDBytes struct is a 128-bit struct that contains the
raw UUID.  A CFUUIDRef can provide such a struct from the
CFUUIDGetUUIDBytes() function.  This struct is suitable for
passing to APIs that expect a raw UUID.
}
        
function CFUUIDGetTypeID: CFTypeID;

function CFUUIDCreate( alloc: CFAllocatorRef ): CFUUIDRef;
    { Create and return a brand new unique identifier }

function CFUUIDCreateWithBytes( alloc: CFAllocatorRef; byte0: UInt8; byte1: UInt8; byte2: UInt8; byte3: UInt8; byte4: UInt8; byte5: UInt8; byte6: UInt8; byte7: UInt8; byte8: UInt8; byte9: UInt8; byte10: UInt8; byte11: UInt8; byte12: UInt8; byte13: UInt8; byte14: UInt8; byte15: UInt8 ): CFUUIDRef;
    { Create and return an identifier with the given contents.  This may return an existing instance with its ref count bumped because of uniquing. }

function CFUUIDCreateFromString( alloc: CFAllocatorRef; uuidStr: CFStringRef ): CFUUIDRef;
    { Converts from a string representation to the UUID.  This may return an existing instance with its ref count bumped because of uniquing. }

function CFUUIDCreateString( alloc: CFAllocatorRef; uuid: CFUUIDRef ): CFStringRef;
    { Converts from a UUID to its string representation. }

function CFUUIDGetConstantUUIDWithBytes( alloc: CFAllocatorRef; byte0: UInt8; byte1: UInt8; byte2: UInt8; byte3: UInt8; byte4: UInt8; byte5: UInt8; byte6: UInt8; byte7: UInt8; byte8: UInt8; byte9: UInt8; byte10: UInt8; byte11: UInt8; byte12: UInt8; byte13: UInt8; byte14: UInt8; byte15: UInt8 ): CFUUIDRef;
    { This returns an immortal CFUUIDRef that should not be released.  It can be used in headers to declare UUID constants with #define. }

function CFUUIDGetUUIDBytes( uuid: CFUUIDRef ): CFUUIDBytes;

function CFUUIDCreateFromUUIDBytes( alloc: CFAllocatorRef; bytes: CFUUIDBytes ): CFUUIDRef;


end.
