{ CoreGraphics - CGFont.h
   Copyright (c) 1999-2009 Apple Inc.
   All rights reserved. }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit CGFont;
interface
uses MacTypes,CFBase,CFData,CFDictionary,CFArray,CGBase,CGDataProvider,CGGeometry;
{$ALIGN POWER}


{ The type used to represent a CoreGraphics font. }

type
	CGFontRef = ^OpaqueCGFontRef; { an opaque type }
	OpaqueCGFontRef = record end;

{ A type to represent indexes in a CGFontRef. }

type
	CGFontIndex = UInt16;

{ A type to represent glyph identifiers in a CGFontRef. }

type
	CGGlyph = CGFontIndex;
	CGGlyphPtr								= ^CGGlyph;

{ The format of a PostScript font subset. Type1 is documented in "Adobe
   Type 1 Font Format"; Type3 in "PostScript Language Reference, 3rd ed."
   and Type42 in "Adobe Technical Note 5012, The Type 42 Font Format
   Specification". }

type
	CGFontPostScriptFormat = SInt32;
const
	kCGFontPostScriptFormatType1 = 1;
	kCGFontPostScriptFormatType3 = 3;
	kCGFontPostScriptFormatType42 = 42;


const
{ The maximum allowed value of a CGFontIndex. Always <= USHRT_MAX - 1. }
	kCGFontIndexMax = (1 shl 16) - 2;

  { A value representing an invalid CGFontIndex. Always <= USHRT_MAX. }
	kCGFontIndexInvalid = (1 shl 16) - 1;

  { The maximum allowed value of a CGGlyph. }
	kCGGlyphMax = kCGFontIndexMax;

{ Return the CFTypeID for CGFontRefs. }

function CGFontGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{$ifc TARGET_OS_MAC}
{ Create a CGFontRef using `platformFontReference', a pointer to a
   platform-specific font reference. For MacOS X, `platformFontReference'
   should be a pointer to an ATSFontRef. }

function CGFontCreateWithPlatformFont( platformFontReference: univ Ptr ): CGFontRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_6,
    __IPHONE_NA, __IPHONE_NA);
{$endc}

{ Return the font defined by the data provided by `provider', or NULL if
   the font can't be created. }

function CGFontCreateWithDataProvider( provider: CGDataProviderRef ): CGFontRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the font identified by `name', corresponding to the font's
   PostScript name or its full name, or NULL if the font can't be
   created. }

function CGFontCreateWithFontName( name: CFStringRef ): CGFontRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return a font based on `font' with the variation specification dictionary
   `variations' applied to `font'. A variation specification dictionary
   contains keys corresponding the variation axis names of the font. Each
   key is a variation axis name; the value for each key is the value
   specified for that particular variation axis represented as a
   CFNumberRef. If a variation axis name is not specified in `variations',
   then the current value from `font' is used. }

function CGFontCreateCopyWithVariations( font: CGFontRef; variations: CFDictionaryRef ): CGFontRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Equivalent to `CFRetain(font)', except it doesn't crash (as CFRetain
   does) if `font' is NULL. }

function CGFontRetain( font: CGFontRef ): CGFontRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRelease(font)', except it doesn't crash (as CFRelease
   does) if `font' is NULL. }

procedure CGFontRelease( font: CGFontRef );
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the number of glyphs in `font'. }

function CGFontGetNumberOfGlyphs( font: CGFontRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the glyph space units/em for `font'. }

function CGFontGetUnitsPerEm( font: CGFontRef ): SInt32;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the PostScript name of `font'. }

function CGFontCopyPostScriptName( font: CGFontRef ): CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the "full name" of `font'. }

function CGFontCopyFullName( font: CGFontRef ): CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the ascent of `font'. The ascent is the maximum distance above the
   baseline of glyphs in a font. The value is specified in glyph space
   units. }

function CGFontGetAscent( font: CGFontRef ): SInt32;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the descent of `font'. The descent is the maximum distance below
   the baseline of glyphs in a font. The value is specified in glyph space
   units. }

function CGFontGetDescent( font: CGFontRef ): SInt32;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the leading of `font'. The leading is the spacing between
   consecutive lines of text in a font. The value is specified in glyph
   space units. }

function CGFontGetLeading( font: CGFontRef ): SInt32;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the cap height of `font'. The cap height is the distance above the
   baseline of the top of flat capital letters of glyphs in a font. The
   value is specified in glyph space units. }

function CGFontGetCapHeight( font: CGFontRef ): SInt32;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the x-height of `font'. The x-height is the distance above the
   baseline of the top of flat, non-ascending lowercase letters (such as
   "x") of glyphs in a font. The value is specified in glyph space units. }

function CGFontGetXHeight( font: CGFontRef ): SInt32;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the font bounding box of `font'. The font bounding box is the
   union of all of the bounding boxes for all the glyphs in a font. The
   value is specified in glyph space units. }

function CGFontGetFontBBox( font: CGFontRef ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the italic angle of `font', measured in degrees counter-clockwise
   from the vertical. }

function CGFontGetItalicAngle( font: CGFontRef ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the thickness of the dominant vertical stems of glyphs in `font'.
   The value is specified in glyph space units. }

function CGFontGetStemV( font: CGFontRef ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return an array of the variation axis dictionaries for `font'. Each
   variation axis dictionary contains values for the variation axis keys
   listed below. This function returns NULL if `font' doesn't support
   variations. }

function CGFontCopyVariationAxes( font: CGFontRef ): CFArrayRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the variation specification dictionary from `font'. This
   dictionary contains keys corresponding the variation axis names of the
   font. Each key is a variation axis name; the value for each key is the
   value specified for that particular variation axis represented as a
   CFNumberRef. This function returns NULL if `font' doesn't support
   variations. }

function CGFontCopyVariations( font: CGFontRef ): CFDictionaryRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Get the advance of each glyph in `glyphs', an array of `count' glyphs,
   and return it in the corresponding entry of `advances', an array of
   `count' integers. The advances are specified in glyph space. Returns
   false if advances can't be retrieved for any reason; true otherwise. }

function CGFontGetGlyphAdvances( font: CGFontRef; {const} glyphs: {variable-size-array} CGGlyphPtr; count: size_t; advances: {variable-size-array} SInt32Ptr ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Get the bounding box of each glyph in `glyphs', an array of `count'
   glyphs, and return it in the corresponding entry of `bboxes', an array of
   `count' rectangles. The bounding boxes are specified in glyph space.
   Returns false if bounding boxes can't be retrieved for any reason; true
   otherwise. }

function CGFontGetGlyphBBoxes( font: CGFontRef; {const} glyphs: {variable-size-array} CGGlyphPtr; count: size_t; bboxes: {variable-size-array} CGRectPtr ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the glyph associated with `name' in `font'. If `name' isn't found
   in the font, return 0. }

function CGFontGetGlyphWithGlyphName( font: CGFontRef; name: CFStringRef ): CGGlyph;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the glyph name of `glyph' in `font', or NULL if `glyph' does not
   appear in `font'. }

function CGFontCopyGlyphNameForGlyph( font: CGFontRef; glyph: CGGlyph ): CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return true if a subset in the PostScript format `format' can be created
   for `font'; false otherwise. }

function CGFontCanCreatePostScriptSubset( font: CGFontRef; format: CGFontPostScriptFormat ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Create a subset of `font' named `subsetName' in the PostScript format
   `format'. The subset will contain the glyphs specified by `glyphs', an
   array of `count' CGGlyphs. If non-NULL, `encoding' specifies the default
   encoding for the subset. }

type
	CGGlyph256Array = array[0..255] of CGGlyph;
function CGFontCreatePostScriptSubset( font: CGFontRef; subsetName: CFStringRef; format: CGFontPostScriptFormat; {const} glyphs: {variable-size-array} CGGlyphPtr; count: size_t; const var encoding: CGGlyph256Array ): CFDataRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return a PostScript encoding of `font' containing glyphs in
   `encoding'. }

function CGFontCreatePostScriptEncoding( font: CGFontRef; const var encoding: CGGlyph256Array ): CFDataRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return an array of font table tags in `font'. Each entry in the array is
   a four-byte value representing a single TrueType or OpenType font table
   tag. }

function CGFontCopyTableTags( font: CGFontRef ): CFArrayRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the table in `font' corresponding to `tag', or NULL if no such
   table exists. }

function CGFontCopyTableForTag( font: CGFontRef; tag: UInt32 ): CFDataRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{** Keys for the font variation axis dictionary. **}

{ The key used to obtain the variation axis name from a variation axis
   dictionary. The value obtained with this key is a CFStringRef specifying
   the name of the variation axis. }

const kCGFontVariationAxisName: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ The key used to obtain the minimum variation axis value from a variation
   axis dictionary. The value obtained with this key is a CFNumberRef
   specifying the minimum value of the variation axis. }

const kCGFontVariationAxisMinValue: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ The key used to obtain the maximum variation axis value from a variation
   axis dictionary. The value obtained with this key is a CFNumberRef
   specifying the maximum value of the variation axis. }

const kCGFontVariationAxisMaxValue: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ The key used to obtain the default variation axis value from a variation
   axis dictionary. The value obtained with this key is a CFNumberRef
   specifying the default value of the variation axis. }

const kCGFontVariationAxisDefaultValue: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Obsolete; don't use these. }

const
	CGGlyphMin = 0;
	CGGlyphMax = kCGGlyphMax;


end.
