{
     File:       HIToolbox/HISeparator.h
 
     Contains:   Definition of the separator view provided by HIToolbox.
 
     Version:    HIToolbox-624~3
 
     Copyright:  © 2006-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HISeparator;
interface
uses MacTypes,Appearance,CarbonEvents,Controls,QuickdrawTypes,HIObject;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
 *  HISeparator.h
 *  
 *  Discussion:
 *    API definitions for the separator view.
 }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ VISUAL SEPARATOR (CDEF 9)                                                         }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  Separator lines determine their orientation (horizontal or vertical) automatically  }
{  based on the relative height and width of their contrlRect.                         }
{ Visual separator proc IDs }
const
	kControlSeparatorLineProc = 144;

{ Control Kind Tag }
const
	kControlKindSeparator = FOUR_CHAR_CODE('sepa');

{ The HIObject class ID for the HIVisualSeparator class. }
const kHIVisualSeparatorClassID = CFSTR( 'com.apple.HIVisualSeparator' );
{ Creation API: Carbon only }
{$ifc not TARGET_CPU_64}
{
 *  CreateSeparatorControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateSeparatorControl( window: WindowRef; const var boundsRect: Rect; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
