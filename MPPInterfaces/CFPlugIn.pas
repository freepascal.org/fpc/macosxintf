{	CFPlugIn.h
	Copyright (c) 1999-2013, Apple Inc.  All rights reserved.
}
unit CFPlugIn;
interface
uses MacTypes,CFBase,CFArray,CFBundle,CFString,CFURL,CFUUID;
{$ALIGN POWER}

{
# if !defined(COREFOUNDATION_CFPLUGINCOM_SEPARATE)
const
	COREFOUNDATION_CFPLUGINCOM_SEPARATE = 1;
# endif
}

{ ================ Standard Info.plist keys for plugIns ================ }

const kCFPlugInDynamicRegistrationKey: CFStringRef;
const kCFPlugInDynamicRegisterFunctionKey: CFStringRef;
const kCFPlugInUnloadFunctionKey: CFStringRef;
const kCFPlugInFactoriesKey: CFStringRef;
const kCFPlugInTypesKey: CFStringRef;

{ ================= Function prototypes for various callbacks ================= }
{ Function types that plugIn authors can implement for various purposes. }

type
	CFPlugInDynamicRegisterFunction = procedure( plugIn: CFPlugInRef );
	CFPlugInUnloadFunction = procedure( plugIn: CFPlugInRef );
	CFPlugInFactoryFunction = function( allocator: CFAllocatorRef; typeUUID: CFUUIDRef ): UnivPtr;

{ ================= Creating PlugIns ================= }

function CFPlugInGetTypeID: CFTypeID;

function CFPlugInCreate( allocator: CFAllocatorRef; plugInURL: CFURLRef ): CFPlugInRef;
    { Might return an existing instance with the ref-count bumped. }

function CFPlugInGetBundle( plugIn: CFPlugInRef ): CFBundleRef;

{ ================= Controlling load on demand ================= }
{ For plugIns. }
{ PlugIns that do static registration are load on demand by default. }
{ PlugIns that do dynamic registration are not load on demand by default. }
{ A dynamic registration function can call CFPlugInSetLoadOnDemand(). }

procedure CFPlugInSetLoadOnDemand( plugIn: CFPlugInRef; flag: Boolean );

function CFPlugInIsLoadOnDemand( plugIn: CFPlugInRef ): Boolean;

{ ================= Finding factories and creating instances ================= }
{ For plugIn hosts. }
{ Functions for finding factories to create specific types and actually creating instances of a type. }

function CFPlugInFindFactoriesForPlugInType( typeUUID: CFUUIDRef ): CFArrayRef;
    { This function finds all the factories from any plugin for the given type.  Returns an array that the caller must release. }
    
function CFPlugInFindFactoriesForPlugInTypeInPlugIn( typeUUID: CFUUIDRef; plugIn: CFPlugInRef ): CFArrayRef;
    { This function restricts the result to factories from the given plug-in that can create the given type.  Returns an array that the caller must release. }

function CFPlugInInstanceCreate( allocator: CFAllocatorRef; factoryUUID: CFUUIDRef; typeUUID: CFUUIDRef ): UnivPtr;
    { This function returns the IUnknown interface for the new instance. }

{ ================= Registering factories and types ================= }
{ For plugIn writers who must dynamically register things. }
{ Functions to register factory functions and to associate factories with types. }

function CFPlugInRegisterFactoryFunction( factoryUUID: CFUUIDRef; func: CFPlugInFactoryFunction ): Boolean;

function CFPlugInRegisterFactoryFunctionByName( factoryUUID: CFUUIDRef; plugIn: CFPlugInRef; functionName: CFStringRef ): Boolean;

function CFPlugInUnregisterFactory( factoryUUID: CFUUIDRef ): Boolean;

function CFPlugInRegisterPlugInType( factoryUUID: CFUUIDRef; typeUUID: CFUUIDRef ): Boolean;

function CFPlugInUnregisterPlugInType( factoryUUID: CFUUIDRef; typeUUID: CFUUIDRef ): Boolean;

{ ================= Registering instances ================= }
{ When a new instance of a type is created, the instance is responsible for registering itself with the factory that created it and unregistering when it deallocates. }
{ This means that an instance must keep track of the CFUUIDRef of the factory that created it so it can unregister when it goes away. }

procedure CFPlugInAddInstanceForFactory( factoryID: CFUUIDRef );

procedure CFPlugInRemoveInstanceForFactory( factoryID: CFUUIDRef );


{ Obsolete API }

type
	CFPlugInInstanceRef = ^__CFPlugInInstance; { an opaque type }
	__CFPlugInInstance = record end;
	CFPlugInInstanceRefPtr = ^CFPlugInInstanceRef;

type
	CFPlugInInstanceGetInterfaceFunction = function( instance: CFPlugInInstanceRef; interfaceName: CFStringRef; var ftbl: univ Ptr ): Boolean;
	CFPlugInInstanceDeallocateInstanceDataFunction = procedure( instanceData: univ Ptr );

function CFPlugInInstanceGetInterfaceFunctionTable( instance: CFPlugInInstanceRef; interfaceName: CFStringRef; var ftbl: univ Ptr ): Boolean;

{ This function returns a retained object on 10.8 or later. }
function CFPlugInInstanceGetFactoryName( instance: CFPlugInInstanceRef ): CFStringRef; {CF_RETURNS_RETAINED; }

function CFPlugInInstanceGetInstanceData( instance: CFPlugInInstanceRef ): UnivPtr;

function CFPlugInInstanceGetTypeID: CFTypeID;

function CFPlugInInstanceCreateWithInstanceDataSize( allocator: CFAllocatorRef; instanceDataSize: CFIndex; deallocateInstanceFunction: CFPlugInInstanceDeallocateInstanceDataFunction; factoryName: CFStringRef; getInterfaceFunction: CFPlugInInstanceGetInterfaceFunction ): CFPlugInInstanceRef;




end.
