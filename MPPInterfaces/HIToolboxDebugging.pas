{       Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HIToolboxDebugging;
interface
uses MacTypes,CarbonEvents,CarbonEventsCore,Dialogs,Events,Files,MacWindows,Menus,QuickdrawTypes,HIObject;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{ CarbonEvents }
{$ifc not TARGET_CPU_64}
{
 *  DebugSetEventTraceEnabled()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugSetEventTraceEnabled( inEnabled: Boolean );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugTraceEvent()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugTraceEvent( inEventClass: OSType; inEventKind: UInt32; inTrace: Boolean );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugTraceEventByName()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugTraceEventByName( eventName: ConstCStringPtr; inTrace: Boolean );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugPrintTracedEvents()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintTracedEvents;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugPrintEventQueue()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintEventQueue( inQueue: EventQueueRef );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{$endc} {not TARGET_CPU_64}

{
 *  DebugPrintMainEventQueue()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintMainEventQueue;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugPrintEvent()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintEvent( inEvent: EventRef );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{ Control specific debugging routines }
{$ifc not TARGET_CPU_64}
{
 *  DebugPrintControl()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintControl( inControl: ControlRef );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugPrintControlHierarchy()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintControlHierarchy( inWindow: WindowRef );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DumpControlHierarchy()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppearanceLib 1.0 and later
 }
function DumpControlHierarchy( inWindow: WindowRef; const var inDumpFile: FSSpec ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Dialogs }
{
 *  DebugPrintDialogInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintDialogInfo( inDialog: DialogRef );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{ HIView }
{
 *  HIViewFlashDirtyArea()
 *  
 *  Discussion:
 *    Debugging aid. Flashes the region which would be redrawn at the
 *    next draw time for an entire window.
 *  
 *  Parameters:
 *    
 *    inWindow:
 *      The window to flash the dirty region for.
 *  
 *  Result:
 *    An operating system result code.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
function HIViewFlashDirtyArea( inWindow: WindowRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_2_AND_LATER;


{ Menus }
{
 *  DebugPrintMenuList()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintMenuList;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugPrintMenu()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintMenu( inMenu: MenuRef );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugPrintMenuItem()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintMenuItem( inMenu: MenuRef; inItem: MenuItemIndex );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{ Windows }
{
 *  DebugPrintWindow()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintWindow( windowRef_: WindowRef );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugPrintWindowGroup()
 *  
 *  Summary:
 *    Prints the contents of a window group to stdout.
 *  
 *  Parameters:
 *    
 *    inGroup:
 *      The group whose contents to print.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintWindowGroup( inGroup: WindowGroupRef );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DebugPrintAllWindowGroups()
 *  
 *  Summary:
 *    Prints the full window group hierarchy, starting at the root
 *    group.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintAllWindowGroups;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DebugPrintWindowList()
 *  
 *  Summary:
 *    Prints the window list to stdout.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugPrintWindowList;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{
 *  DebugFlashWindowVisRgn()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugFlashWindowVisRgn( windowRef_: WindowRef );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED;


{
 *  DebugFlashWindowUpdateRgn()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure DebugFlashWindowUpdateRgn( inWindow: WindowRef );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
