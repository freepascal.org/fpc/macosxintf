{
 *  CVBase.h
 *  CoreVideo
 *
 *  Copyright (c) 2004-2008 Apple Computer, Inc. All rights reserved.
 *
 }

{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2008 }
{  Pascal Translation Update:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }

unit CVBase;
interface
uses MacTypes, CFBase;

{$ALIGN POWER}

 
 {! @header CVBase.h
	@copyright 2004 Apple Computer, Inc. All rights reserved.
	@availability Mac OS X 10.4 or later
    @discussion Here you can find the type declarations for CoreVideo. CoreVideo uses a CVTimeStamp structure to store video display time stamps.
}

 
{
#ifndef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER
#define AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER       WEAK_IMPORT_ATTRIBUTE
#endif

#ifndef __AVAILABILITY_INTERNAL__MAC_10_7
#define __AVAILABILITY_INTERNAL__MAC_10_7        __AVAILABILITY_INTERNAL_WEAK_IMPORT
#endif

#ifndef __AVAILABILITY_INTERNAL__MAC_10_8
#define __AVAILABILITY_INTERNAL__MAC_10_8        __AVAILABILITY_INTERNAL_WEAK_IMPORT
#endif

#ifndef __AVAILABILITY_INTERNAL__MAC_10_9
#define __AVAILABILITY_INTERNAL__MAC_10_9        __AVAILABILITY_INTERNAL_WEAK_IMPORT
#endif


#define COREVIDEO_SUPPORTS_DIRECT3D 	(TARGET_OS_WIN32)
#define COREVIDEO_SUPPORTS_OPENGL 		(TARGET_OS_MAC && ! TARGET_OS_IPHONE)
#define COREVIDEO_SUPPORTS_OPENGLES		(TARGET_OS_MAC && TARGET_OS_IPHONE)
#define COREVIDEO_SUPPORTS_COLORSPACE 	((TARGET_OS_MAC && ! TARGET_OS_IPHONE) || (TARGET_OS_WIN32))
#define COREVIDEO_SUPPORTS_DISPLAYLINK 	(TARGET_OS_MAC && ! TARGET_OS_IPHONE)
#define COREVIDEO_SUPPORTS_IOSURFACE	(TARGET_OS_IPHONE ? TARGET_OS_EMBEDDED : (TARGET_OS_MAC && ((MAC_OS_X_VERSION_MAX_ALLOWED >= 1060))))

#define extern extern 
#define CV_INLINE CF_INLINE
}
{!
    @typedef	CVOptionFlags
    @abstract   Flags to be used for the display and render call back functions.
    @discussion ***Values to be defined***
}
type
	CVOptionFlags = UInt64;

{!
    @struct         CVSMPTETime
    @abstract       A structure for holding a SMPTE time.
    @field          subframes
                        The number of subframes in the full message.
    @field          subframeDivisor
                        The number of subframes per frame (typically 80).
    @field          counter
                        The total number of messages received.
    @field          type
                        The kind of SMPTE time using the SMPTE time type constants.
    @field          flags
                        A set of flags that indicate the SMPTE state.
    @field          hours
                        The number of hours in the full message.
    @field          minutes
                        The number of minutes in the full message.
    @field          seconds
                        The number of seconds in the full message.
    @field          frames
                        The number of frames in the full message.
}
type
	CVSMPTETime = record
		subframes:       SInt16;
		subframeDivisor: SInt16;
		counter:         UInt32;
		typ:             UInt32;
		flags:           UInt32;
		hours:           SInt16;
		minutes:         SInt16;
		seconds:         SInt16;
		frames:          SInt16;
	end;

{!
    @enum           SMPTE Time Types
    @abstract       Constants that describe the type of SMPTE time.
    @constant       kCVSMPTETimeType24
                        24 Frame
    @constant       kCVSMPTETimeType25
                        25 Frame
    @constant       kCVSMPTETimeType30Drop
                        30 Drop Frame
    @constant       kCVSMPTETimeType30
                        30 Frame
    @constant       kCVSMPTETimeType2997
                        29.97 Frame
    @constant       kCVSMPTETimeType2997Drop
                        29.97 Drop Frame
    @constant       kCVSMPTETimeType60
                        60 Frame
    @constant       kCVSMPTETimeType5994
                        59.94 Frame
}
const
	kCVSMPTETimeType24       = 0;
	kCVSMPTETimeType25       = 1;
	kCVSMPTETimeType30Drop   = 2;
	kCVSMPTETimeType30       = 3;
	kCVSMPTETimeType2997     = 4;
	kCVSMPTETimeType2997Drop = 5;
	kCVSMPTETimeType60       = 6;
	kCVSMPTETimeType5994     = 7;

{!
    @enum           SMPTE State Flags
    @abstract       Flags that describe the SMPTE time state.
    @constant       kCVSMPTETimeValid
                        The full time is valid.
    @constant       kCVSMPTETimeRunning
                        Time is running.
}
const
	kCVSMPTETimeValid   = 1 shl 0;
	kCVSMPTETimeRunning = 1 shl 1;


const
	kCVTimeIsIndefinite = 1 shl 0;

type
	CVTime = record
		timeValue: SInt64;
		timeScale: SInt32;
		flags:     SInt32;
	end;

{!
    @struct CVTimeStamp
    @abstract CoreVideo uses a CVTimeStamp structure to store video display time stamps.
    @discussion This structure is purposely very similar to AudioTimeStamp defined in the CoreAudio framework. 
		Most of the CVTimeStamp struct should be fairly self-explanatory. However, it is probably worth pointing out that unlike the audio time stamps, floats are not used to represent the video equivalent of sample times. This was done partly to avoid precision issues, and partly because QuickTime still uses integers for time values and time scales. In the actual implementation it has turned out to be very convenient to use integers, and we can represent framerates like NTSC (30000/1001 fps) exactly. The mHostTime structure field uses the same Mach absolute time base that is used in CoreAudio, so that clients of the CoreVideo API can synchronize between the two subsystems.
    @field videoTimeScale The scale (in units per second) of the videoTime and videoPeriod values
    @field videoTime This represents the start of a frame (or field for interlaced)
    @field hostTime Host root timebase time
    @field rateScalar This is the current rate of the device as measured by the timestamps, divided by the nominal rate
    @field videoPeriod This is the nominal update period of the current output device
    @field smpteTime SMPTE time representation of the time stamp. 
    @field flags Possible values are:		
		kCVTimeStampVideoTimeValid
		kCVTimeStampHostTimeValid
		kCVTimeStampSMPTETimeValid
		kCVTimeStampVideoPeriodValid
		kCVTimeStampRateScalarValid
		There are flags for each field to make it easier to detect interlaced vs progressive output
		kCVTimeStampTopField
		kCVTimeStampBottomField
		Some commonly used combinations of timestamp flags
		kCVTimeStampVideoHostTimeValid
		kCVTimeStampIsInterlaced
    @field version The current CVTimeStamp is version 0.
    @field reserved Reserved. Do not use.

}
type
	CVTimeStampPtr = ^CVTimeStamp;
	CVTimeStamp = record
		version:            UInt32;		// Currently will be 0.
		videoTimeScale:     SInt32;     // Video timescale (units per second)
		videoTime:          SInt64;		// This represents the start of a frame (or field for interlaced) .. think vsync  - still not 100% sure on the name
		hostTime:           UInt64;		// Host root timebase time
		rateScalar:         Float64;	// Current rate as measured by the timestamps divided by the nominal rate
		videoRefreshPeriod: SInt64;    	// Hint for nominal output rate
		smpteTime:          CVSMPTETime;
		flags:              UInt64;
		reserved:           UInt64;
	end; 

// Flags for the CVTimeStamp structure
const
	kCVTimeStampVideoTimeValid          = 1 shl 0;
	kCVTimeStampHostTimeValid           = 1 shl 1;
	kCVTimeStampSMPTETimeValid          = 1 shl 2;
	kCVTimeStampVideoRefreshPeriodValid = 1 shl 3;
	kCVTimeStampRateScalarValid         = 1 shl 4;
    
    // There are flags for each field to make it easier to detect interlaced vs progressive output
	kCVTimeStampTopField    = 1 shl 16;
	kCVTimeStampBottomField = 1 shl 17;

//	Some commonly used combinations of timestamp flags
const
	kCVTimeStampVideoHostTimeValid = kCVTimeStampVideoTimeValid or kCVTimeStampHostTimeValid;
	kCVTimeStampIsInterlaced       = kCVTimeStampTopField or kCVTimeStampBottomField;

const kCVZeroTime:       CVTime;
const kCVIndefiniteTime: CVTime;

end.
