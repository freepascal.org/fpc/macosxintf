{
 *  CTFontTraits.h
 *  CoreText
 *
 *  Copyright (c) 2006-2012 Apple Inc. All rights reserved.
 *
 }
{  Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CTFontTraits;
interface
uses MacTypes,CFBase;

{$ALIGN POWER}


{!
    @defined    kCTFontSymbolicTrait
    @abstract   Dictionary key to access the symbolic traits value.
    @discussion Use this key to access the symbolic traits value from the font traits dictionary. The value is returned as a CFNumberRef.
}
const kCTFontSymbolicTrait: CFStringRef;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);
{!
    @defined    kCTFontWeightTrait
    @abstract   Dictionary key to access the weight trait value.
    @discussion Use this key to access the normalized weigth trait from the font traits dictionary. The value returned is a CFNumberRef representing a float value between -1.0 and 1.0 for normalized weight. The value of 0.0 corresponds to the regular or medium font weight.
}
const kCTFontWeightTrait: CFStringRef;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);
{!
    @defined    kCTFontWidthTrait
    @abstract   Dictionary key to access the width (condense/expand) trait value.
    @discussion Use this key to access the normalized proportion trait from the font traits dictionary. This value corresponds to the relative inter-glyph spacing for a given font. The value returned is a CFNumberRef representing a float between -1.0 and 1.0. The value of 0.0 corresponds to regular glyph spacing while negative values represent condensed glyph spacing.
}
const kCTFontWidthTrait: CFStringRef;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);
{!
    @defined    kCTFontSlantTrait
    @abstract   Dictionary key to access the slant trait value.
    @discussion Use this key to access the normalized slant angle from the font traits dictionary. The value returned is a CFNumberRef representing a float value between -1.0 and 1.0 for normalized slant angle. The value or 0.0 corresponds to 0 degree clockwise rotation from the vertical and 1.0 corresponds to 30 degrees clockwise rotation.
}
const kCTFontSlantTrait: CFStringRef;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);

{!
    @const      kCTFontClassMaskShift
    @abstract   The font class shift.
    @discussion This is used to shift the font class to the upper most 4 bits of the symbolic traits.
}
const
	kCTFontClassMaskShift = 28;

{!
    @enum       CTFontSymbolicTraits
    @abstract   Symbolic representation of stylistic font attributes.
    @discussion CTFontSymbolicTraits symbolically describes stylistic aspects of a font. The upper 16 bits is used to describe appearance of the font whereas the lower 16 bits for typeface. The font appearance information represented by the upper 16 bits can be used for stylistic font matching.
}
const
	kCTFontTraitItalic = 1 shl 0;     // Additional detail available via kCTFontSlantTrait
	kCTFontTraitBold = 1 shl 1;     // Additional detail available via kCTFontWeightTrait
	kCTFontTraitExpanded = 1 shl 5;     // Expanded and condensed traits are mutually exclusive
	kCTFontTraitCondensed = 1 shl 6;     // Additional detail available via kCTFontWidthTrait
	kCTFontTraitMonoSpace = 1 shl 10;    // Use fixed-pitch glyphs if available. May have multiple glyph advances (most CJK glyphs may contain two spaces)
	kCTFontTraitVertical = 1 shl 11;    // Use vertical glyph variants and metrics
	kCTFontTraitUIOptimized = 1 shl 12;    // Synthesize appropriate attributes for UI rendering such as control titles if necessary
    kCTFontTraitColorGlyphs         = 1 shl 13;    // Color bitmap glyphs are available. 
    kCTFontTraitComposite           = 1 shl 14;    // The font is a CFR (Composite font reference), a cascade list is expected per font. 

	kCTFontTraitClassMask = 15 shl kCTFontClassMaskShift; // Mask for the font class

    kCTFontItalicTrait = kCTFontTraitItalic;
    kCTFontBoldTrait = kCTFontTraitBold;
    kCTFontExpandedTrait = kCTFontTraitExpanded;
    kCTFontCondensedTrait = kCTFontTraitCondensed;
    kCTFontMonoSpaceTrait = kCTFontTraitMonoSpace;
    kCTFontVerticalTrait = kCTFontTraitVertical;
    kCTFontUIOptimizedTrait = kCTFontTraitUIOptimized;
    kCTFontColorGlyphsTrait = kCTFontTraitColorGlyphs;
    kCTFontCompositeTrait = kCTFontTraitComposite;
    kCTFontClassMaskTrait = kCTFontTraitClassMask;
type
	CTFontSymbolicTraits = UInt32;

{!
    @enum       CTFontStylisticClass
    @abstract   Stylistic class values.
    @discussion CTFontStylisticClass classifies certain stylistic qualities of the font. These values correspond closely to the font class values in the OpenType 'OS/2' table. The class values are bundled in the upper four bits of the CTFontSymbolicTraits and can be obtained via the kCTFontClassMaskTrait.
}
const
	kCTFontClassUnknown = 0 shl kCTFontClassMaskShift;
	kCTFontClassOldStyleSerifs = 1 shl kCTFontClassMaskShift;
	kCTFontClassTransitionalSerifs = 2 shl kCTFontClassMaskShift;
	kCTFontClassModernSerifs = 3 shl kCTFontClassMaskShift;
	kCTFontClassClarendonSerifs = 4 shl kCTFontClassMaskShift;
	kCTFontClassSlabSerifs = 5 shl kCTFontClassMaskShift;
	kCTFontClassFreeformSerifs = 7 shl kCTFontClassMaskShift;
	kCTFontClassSansSerif = 8 shl kCTFontClassMaskShift;
	kCTFontClassOrnamentals = 9 shl kCTFontClassMaskShift;
	kCTFontClassScripts = 10 shl kCTFontClassMaskShift;
	kCTFontClassSymbolic = 12 shl kCTFontClassMaskShift;

    kCTFontUnknownClass = kCTFontClassUnknown;
    kCTFontOldStyleSerifsClass = kCTFontClassOldStyleSerifs;
    kCTFontTransitionalSerifsClass = kCTFontClassTransitionalSerifs;
    kCTFontModernSerifsClass = kCTFontClassModernSerifs;
    kCTFontClarendonSerifsClass = kCTFontClassClarendonSerifs;
    kCTFontSlabSerifsClass = kCTFontClassSlabSerifs;
    kCTFontFreeformSerifsClass = kCTFontClassFreeformSerifs;
    kCTFontSansSerifClass = kCTFontClassSansSerif;
    kCTFontOrnamentalsClass = kCTFontClassOrnamentals;
    kCTFontScriptsClass = kCTFontClassScripts;
    kCTFontSymbolicClass = kCTFontClassSymbolic;
type
	CTFontStylisticClass = UInt32;

end.
