{	CFNotificationCenter.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFNotificationCenter;
interface
uses MacTypes,CFBase,CFDictionary;
{$ALIGN POWER}


type
	CFNotificationCenterRef = ^__CFNotificationCenter; { an opaque type }
	__CFNotificationCenter = record end;

type
	CFNotificationCallback = procedure( center: CFNotificationCenterRef; observer: univ Ptr; name: CFStringRef; objct: {const} univ Ptr; userInfo: CFDictionaryRef );

type
	CFNotificationSuspensionBehavior = CFIndex;
const
	CFNotificationSuspensionBehaviorDrop = 1;
        // The server will not queue any notifications with this name and object while the process/app is in the background.
	CFNotificationSuspensionBehaviorCoalesce = 2;
        // The server will only queue the last notification of the specified name and object; earlier notifications are dropped. 
	CFNotificationSuspensionBehaviorHold = 3;
        // The server will hold all matching notifications until the queue has been filled (queue size determined by the server) at which point the server may flush queued notifications.
	CFNotificationSuspensionBehaviorDeliverImmediately = 4;
        // The server will deliver notifications matching this registration whether or not the process is in the background.  When a notification with this suspension behavior is matched, it has the effect of first flushing any queued notifications.

function CFNotificationCenterGetTypeID: CFTypeID;

function CFNotificationCenterGetLocalCenter: CFNotificationCenterRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{$ifc TARGET_OS_MAC or TARGET_OS_WIN32}
function CFNotificationCenterGetDistributedCenter: CFNotificationCenterRef;
{$endc}

function CFNotificationCenterGetDarwinNotifyCenter: CFNotificationCenterRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
// The Darwin Notify Center is based on the <notify.h> API.
// For this center, there are limitations in the API. There are no notification "objects",
// "userInfo" cannot be passed in the notification, and there are no suspension behaviors
// (always "deliver immediately"). Other limitations in the <notify.h> API as described in
// that header will also apply.
// - In the CFNotificationCallback, the 'object' and 'userInfo' parameters must be ignored.
// - CFNotificationCenterAddObserver(): the 'object' and 'suspensionBehavior' arguments are ignored.
// - CFNotificationCenterAddObserver(): the 'name' argument may not be NULL (for this center).
// - CFNotificationCenterRemoveObserver(): the 'object' argument is ignored.
// - CFNotificationCenterPostNotification(): the 'object', 'userInfo', and 'deliverImmediately' arguments are ignored.
// - CFNotificationCenterPostNotificationWithOptions(): the 'object', 'userInfo', and 'options' arguments are ignored.
// The Darwin Notify Center has no notion of per-user sessions, all notifications are system-wide.
// As with distributed notifications, the main thread's run loop must be running in one of the
// common modes (usually kCFRunLoopDefaultMode) for Darwin-style notifications to be delivered.
// NOTE: NULL or 0 should be passed for all ignored arguments to ensure future compatibility.


procedure CFNotificationCenterAddObserver( center: CFNotificationCenterRef; observer: {const} univ Ptr; callBack: CFNotificationCallback; name: CFStringRef; objct: {const} univ Ptr; suspensionBehavior: CFNotificationSuspensionBehavior );

procedure CFNotificationCenterRemoveObserver( center: CFNotificationCenterRef; observer: {const} univ Ptr; name: CFStringRef; objct: {const} univ Ptr );
procedure CFNotificationCenterRemoveEveryObserver( center: CFNotificationCenterRef; observer: {const} univ Ptr );

procedure CFNotificationCenterPostNotification( center: CFNotificationCenterRef; name: CFStringRef; objct: {const} univ Ptr; userInfo: CFDictionaryRef; deliverImmediately: Boolean );

{#if MAC_OS_X_VERSION_10_3 <= MAC_OS_X_VERSION_MAX_ALLOWED}

const
	kCFNotificationDeliverImmediately = 1 shl 0;
	kCFNotificationPostToAllSessions = 1 shl 1;

procedure CFNotificationCenterPostNotificationWithOptions( center: CFNotificationCenterRef; name: CFStringRef; objct: {const} univ Ptr; userInfo: CFDictionaryRef; options: CFOptionFlags );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

{#endif}


end.
