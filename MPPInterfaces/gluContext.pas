{
	Copyright:	(c) 1999-2008 Apple Inc. All rights reserved.
}
{       Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }

unit gluContext;
interface
uses MacTypes, macgl, macglu, CGLTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

{
** GLU Context routines
}
function gluBuild1DMipmapsCTX( ctx: CGLContextObj; target: GLenum; internalFormat: GLint; width: GLsizei; format: GLenum; typ: GLenum; data: {const} univ Ptr ): GLint;
function gluBuild2DMipmapsCTX( ctx: CGLContextObj; target: GLenum; internalFormat: GLint; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; data: {const} univ Ptr ): GLint;
function gluBuild3DMipmapsCTX( ctx: CGLContextObj; target: GLenum; internalFormat: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; typ: GLenum; data: {const} univ Ptr ): GLint;
function gluBuild1DMipmapLevelsCTX( ctx: CGLContextObj; target: GLenum; internalFormat: GLint; width: GLsizei; format: GLenum; typ: GLenum; level: GLint; base: GLint; max: GLint; data: {const} univ Ptr ): GLint;
function gluBuild2DMipmapLevelsCTX( ctx: CGLContextObj; target: GLenum; internalFormat: GLint; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; level: GLint; base: GLint; max: GLint; data: {const} univ Ptr ): GLint;
function gluBuild3DMipmapLevelsCTX( ctx: CGLContextObj; target: GLenum; internalFormat: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; typ: GLenum; level: GLint; base: GLint; max: GLint; data: {const} univ Ptr ): GLint;

procedure gluLookAtCTX( ctx: CGLContextObj; eyeX: GLdouble; eyeY: GLdouble; eyeZ: GLdouble; centerX: GLdouble; centerY: GLdouble; centerZ: GLdouble; upX: GLdouble; upY: GLdouble; upZ: GLdouble );
function gluNewNurbsRendererCTX( ctx: CGLContextObj ): PGLUnurbs;
function gluNewQuadricCTX( ctx: CGLContextObj ): PGLUquadric;
function gluNewTessCTX( ctx: CGLContextObj ): PGLUtesselator;
procedure gluOrtho2DCTX( ctx: CGLContextObj; left: GLdouble; right: GLdouble; bottom: GLdouble; top: GLdouble );
procedure gluPerspectiveCTX( ctx: CGLContextObj; fovy: GLdouble; aspect: GLdouble; zNear: GLdouble; zFar: GLdouble );
procedure gluPickMatrixCTX( ctx: CGLContextObj; x: GLdouble; y: GLdouble; delX: GLdouble; delY: GLdouble; var viewport: GLint );
function gluScaleImageCTX( ctx: CGLContextObj; format: GLenum; wIn: GLsizei; hIn: GLsizei; typeIn: GLenum; dataIn: {const} univ Ptr; wOut: GLsizei; hOut: GLsizei; typeOut: GLenum; dataOut: univ Ptr ): GLint;      

{$endc} {TARGET_OS_MAC}

end.
