{
 * ImageIO - CGImageSource.h
 * Copyright (c) 2004-2010 Apple Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit CGImageSource;
interface
uses MacTypes, CFArray, CFBase, CFData, CFDictionary, CFURL, CGDataProvider, CGImage, CGImageMetadata;

{$ALIGN POWER}


type
	CGImageSourceRef = ^OpaqueCGImageSourceRef; { an opaque type }
	OpaqueCGImageSourceRef = record end;


type
	CGImageSourceStatus = SInt32;
const
	kCGImageStatusUnexpectedEOF = -5;
	kCGImageStatusInvalidData = -4;
	kCGImageStatusUnknownType = -3;
	kCGImageStatusReadingHeader = -2;
	kCGImageStatusIncomplete = -1;
	kCGImageStatusComplete = 0;

{* Keys for the options dictionary when creating a CGImageSourceRef. *}

{ Specifies the "best guess" of the type identifier for the format of the
 * image source file. If specified, the value of this key must be a
 * CFStringRef. For more information about type identifiers, see "UTType.h"
 * in the Application Services framework. }

const kCGImageSourceTypeIdentifierHint: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{* Keys for the options dictionary of "CGImageSourceCopyPropertiesAtIndex"
 ** and "CGImageSourceCreateImageAtIndex". *}

{ Specifies whether the image should be cached in a decoded form. The
 * value of this key must be a CFBooleanRef; the default value is
 * kCFBooleanFalse. }

const kCGImageSourceShouldCache: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Specifies whether the image should be returned as a floating
 * point CGImageRef if supported by the file format. Extended
 * range floating point CGImageRef may require additional
 * processing  to render pleasingly.  The value of this key must
 * be a CFBooleanRef; the default value is kCFBooleanFalse. }

const kCGImageSourceShouldAllowFloat: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


{* Keys for the options dictionary of
 ** "CGImageSourceCreateThumbnailAtIndex". *}

{ Specifies whether a thumbnail should be automatically created for an
 * image if a thumbnail isn't present in the image source file.  The
 * thumbnail will be created from the full image, subject to the limit
 * specified by kCGImageSourceThumbnailMaxPixelSize---if a maximum pixel
 * size isn't specified, then the thumbnail will be the size of the full
 * image, which probably isn't what you want. The value of this key must be
 * a CFBooleanRef; the default value of this key is kCFBooleanFalse. }

const kCGImageSourceCreateThumbnailFromImageIfAbsent: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Specifies whether a thumbnail should be created from the full image even
 * if a thumbnail is present in the image source file. The thumbnail will
 * be created from the full image, subject to the limit specified by
 * kCGImageSourceThumbnailMaxPixelSize---if a maximum pixel size isn't
 * specified, then the thumbnail will be the size of the full image, which
 * probably isn't what you want. The value of this key must be a
 * CFBooleanRef; the default value of this key is kCFBooleanFalse. }

const kCGImageSourceCreateThumbnailFromImageAlways: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Specifies the maximum width and height in pixels of a thumbnail.  If
 * this this key is not specified, the width and height of a thumbnail is
 * not limited and thumbnails may be as big as the image itself.  If
 * present, this value of this key must be a CFNumberRef. }

const kCGImageSourceThumbnailMaxPixelSize: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Specifies whether the thumbnail should be rotated and scaled according
 * to the orientation and pixel aspect ratio of the full image. The value
 * of this key must be a CFBooleanRef; the default value of this key is 
 * kCFBooleanFalse. }

const kCGImageSourceCreateThumbnailWithTransform: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


{ Return the CFTypeID for CGImageSources. }

function CGImageSourceGetTypeID: CFTypeID;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Return an array of supported type identifiers. }

function CGImageSourceCopyTypeIdentifiers: CFArrayRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Create an image source reading from the data provider `provider'. The
 * `options' dictionary may be used to request additional creation options;
 * see the list of keys above for more information. }

function CGImageSourceCreateWithDataProvider( provider: CGDataProviderRef; options: CFDictionaryRef ): CGImageSourceRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Create an image source reading from `data'.  The `options' dictionary
 * may be used to request additional creation options; see the list of keys
 * above for more information. }

function CGImageSourceCreateWithData( data: CFDataRef; options: CFDictionaryRef ): CGImageSourceRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Create an image source reading from `url'. The `options' dictionary may
 * be used to request additional creation options; see the list of keys
 * above for more information. }

function CGImageSourceCreateWithURL( url: CFURLRef; options: CFDictionaryRef ): CGImageSourceRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Return the type identifier of the image source `isrc'.  This type is the
 * type of the source "container", which is not necessarily the type of the
 * image(s) in the container.  For example, the .icns format supports
 * embedded JPEG2000 but the source type will be "com.apple.icns". }

function CGImageSourceGetType( isrc: CGImageSourceRef ): CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Return the number of images (not including thumbnails) in the image
 * source `isrc'. }

function CGImageSourceGetCount( isrc: CGImageSourceRef ): size_t;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Return the properties of the image source `isrc'.  These properties
 * apply to the container in general but not necessarily to any individual
 * image that it contains. }

function CGImageSourceCopyProperties( isrc: CGImageSourceRef; options: CFDictionaryRef ): CFDictionaryRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Return the properties of the image at `index' in the image source
 * `isrc'.  The index is zero-based. The `options' dictionary may be used
 * to request additional options; see the list of keys above for more
 * information. }

function CGImageSourceCopyPropertiesAtIndex( isrc: CGImageSourceRef; index: size_t; options: CFDictionaryRef ): CFDictionaryRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{$ifc TARGET_OS_MAC}
{ Return the metadata of the image at `index' in the image source
 * `isrc'. The index is zero-based. The `options' dictionary may be used
 * to request additional options; see the list of keys above for more
 * information. Please refer to CGImageMetadata.h for usage of metadata. }
function CGImageSourceCopyMetadataAtIndex( isrc: CGImageSourceRef; index: size_t; options: CFDictionaryRef ): CGImageMetadataRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_8,__IPHONE_NA);
{$endc} { TARGET_OS_MAC }

{ Return the image at `index' in the image source `isrc'.  The index is
 * zero-based. The `options' dictionary may be used to request additional
 * creation options; see the list of keys above for more information. }

function CGImageSourceCreateImageAtIndex( isrc: CGImageSourceRef; index: size_t; options: CFDictionaryRef ): CGImageRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Return the thumbnail of the image at `index' in the image source `isrc'.
 * The index is zero-based. The `options' dictionary may be used to request
 * additional thumbnail creation options; see the list of keys above for
 * more information. }

function CGImageSourceCreateThumbnailAtIndex( isrc: CGImageSourceRef; index: size_t; options: CFDictionaryRef ): CGImageRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Create an incremental image source. No data is provided at creation
 * time; it is assumed that data will eventually be provided using
 * "CGImageSourceUpdateDataProvider" or "CGImageSourceUpdateData".  The
 * `options' dictionary may be used to request additional creation options;
 * see the list of keys above for more information. }

function CGImageSourceCreateIncremental( options: CFDictionaryRef ): CGImageSourceRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Update the incremental image source `isrc' with new data.  The new data
 * must include all the previous data plus any additional new data. The
 * `final' parameter should be true when the final set of data is provided;
 * false otherwise. }

procedure CGImageSourceUpdateData( isrc: CGImageSourceRef; data: CFDataRef; final: CBool );
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Update the incremental image source `isrc' with a new data provider.
 * The new data provider must provide all the previous data plus any
 * additional new data. The `final' parameter should be true when the final
 * set of data is provided; false otherwise. }

procedure CGImageSourceUpdateDataProvider( isrc: CGImageSourceRef; provider: CGDataProviderRef; final: CBool );
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Return the overall status of the image source `isrc'.  The status is
 * particularly informative for incremental image sources, but may be used
 * by clients providing non-incremental data as well. }

function CGImageSourceGetStatus( isrc: CGImageSourceRef ): CGImageSourceStatus;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Return the current status of the image at `index' in the image source
 * `isrc'. The index is zero-based. The returned status is particularly
 * informative for incremental image sources but may used by clients
 * providing non-incremental data as well. }

function CGImageSourceGetStatusAtIndex( isrc: CGImageSourceRef; index: size_t ): CGImageSourceStatus;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


end.
