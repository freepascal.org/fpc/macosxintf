{
 * Copyright (c) 2004-2011 Apple Inc. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 }
{  Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit SCNetworkConfiguration;
interface
uses MacTypes,CFBase,CFArray,CFDictionary,CFNumber,SCPreferences;
{$ALIGN POWER}


{!
	@header SCNetworkConfiguration
	@discussion The SCNetworkConfiguration API provides access to the
		stored network configuration.  The functions include
		providing access to the network capable devices on the
		system, the network sets, network services, and network
		protocols.

		Note: When using the SCNetworkConfiguraiton APIs you must
		keep in mind that in order for any of your changes to be
		committed to permanent storage a call must be made to the
		SCPreferencesCommitChanges function.
 }


{!
	@group Interface configuration
 }

//#pragma mark -
//#pragma mark SCNetworkInterface configuration (typedefs, consts)

{!
	@typedef SCNetworkInterfaceRef
	@discussion This is the type of a reference to an object that represents
		a network interface.
 }
type
	SCNetworkInterfaceRef = ^__SCNetworkInterface; { an opaque type }
	__SCNetworkInterface = record end;

{ until __IPHONE_NA is translated automatically }
{$ifc TARGET_OS_MAC}

{!
	@const kSCNetworkInterfaceType6to4
 }
const kSCNetworkInterfaceType6to4: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeBluetooth
 }
const kSCNetworkInterfaceTypeBluetooth: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeBond
 }
const kSCNetworkInterfaceTypeBond: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeEthernet
 }
const kSCNetworkInterfaceTypeEthernet: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeFireWire
 }
const kSCNetworkInterfaceTypeFireWire: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeIEEE80211
 }
const kSCNetworkInterfaceTypeIEEE80211: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);	// IEEE 802.11, AirPort

{!
	@const kSCNetworkInterfaceTypeIPSec
 }
const kSCNetworkInterfaceTypeIPSec: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeIrDA
 }
const kSCNetworkInterfaceTypeIrDA: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeL2TP
 }
const kSCNetworkInterfaceTypeL2TP: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeModem
 }
const kSCNetworkInterfaceTypeModem: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypePPP
 }
const kSCNetworkInterfaceTypePPP: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypePPTP
 }
const kSCNetworkInterfaceTypePPTP: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeSerial
 }
const kSCNetworkInterfaceTypeSerial: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeVLAN
 }
const kSCNetworkInterfaceTypeVLAN: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceTypeWWAN
 }
const kSCNetworkInterfaceTypeWWAN: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{ special network interfaces (and types) }

{!
	@const kSCNetworkInterfaceTypeIPv4
 }
const kSCNetworkInterfaceTypeIPv4: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkInterfaceIPv4
	@discussion A network interface that can used for layering other
		interfaces (e.g. 6to4, IPSec, PPTP, L2TP) over an existing
		IPv4 network.
 }
const kSCNetworkInterfaceIPv4: SCNetworkInterfaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@group Interface configuration (Bond)
 }

//#pragma mark -

{!
	@typedef SCBondInterfaceRef
	@discussion This is the type of a reference to an object that represents
		an Ethernet Bond interface.
 }
type
	SCBondInterfaceRef = SCNetworkInterfaceRef;

{!
	@typedef SCBondStatusRef
	@discussion This is the type of a reference to an object that represents
		the status of an Ethernet Bond interface.
 }
type
	SCBondStatusRef = ^__SCBondStatus; { an opaque type }
	__SCBondStatus = record end;

{!
	@enum Ethernet Bond Aggregation Status (kSCBondStatusDeviceAggregationStatus) codes
	@discussion Returned status codes.
	@constant kSCBondStatusOK		Enabled, active, running, ...
	@constant kSCBondStatusLinkInvalid	The link state was not valid (i.e. down, half-duplex, wrong speed)
	@constant kSCBondStatusNoPartner	The port on the switch that the device is connected doesn't seem to have 802.3ad Link Aggregation enabled
	@constant kSCBondStatusNotInActiveGroup	We're talking to a partner, but the link aggregation group is different from the one that's active
	@constant kSCBondStatusUnknown		Non-specific failure
 }
const
	kSCBondStatusOK = 0;	{ enabled, active, running, ... }
	kSCBondStatusLinkInvalid = 1;	{ The link state was not valid (i.e. down, half-duplex, wrong speed) }
	kSCBondStatusNoPartner = 2;	{ The port on the switch that the device is connected doesn't seem to have 802.3ad Link Aggregation enabled }
	kSCBondStatusNotInActiveGroup = 3;	{ We're talking to a partner, but the link aggregation group is different from the one that's active }
	kSCBondStatusUnknown = 999;	{ Non-specific failure }

{!
	@const kSCBondStatusDeviceAggregationStatus
 }
const kSCBondStatusDeviceAggregationStatus	{ CFNumber }: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCBondStatusDeviceCollecting
 }
const kSCBondStatusDeviceCollecting		{ CFNumber (0 or 1) }: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCBondStatusDeviceDistributing
 }
const kSCBondStatusDeviceDistributing	{ CFNumber (0 or 1) }: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@group Interface configuration (VLAN)
 }

//#pragma mark -

{!
	@typedef SCVLANInterfaceRef
	@discussion This is the type of a reference to an object that represents
		a Virtual LAN (VLAN) interface.
 }
type
	SCVLANInterfaceRef = SCNetworkInterfaceRef;


{!
	@group Protocol configuration
 }

//#pragma mark -
//#pragma mark SCNetworkProtocol configuration (typedefs, consts)

{!
	@typedef SCNetworkProtocolRef
	@discussion This is the type of a reference to an object that represents
		a network protocol.
 }
type
	SCNetworkProtocolRef = ^__SCNetworkProtocol; { an opaque type }
	__SCNetworkProtocol = record end;

{ network "protocol" types }

{!
	@const kSCNetworkProtocolTypeAppleTalk
 }
const kSCNetworkProtocolTypeAppleTalk: CFStringRef;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);

{!
	@const kSCNetworkProtocolTypeDNS
 }
const kSCNetworkProtocolTypeDNS: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkProtocolTypeIPv4
 }
const kSCNetworkProtocolTypeIPv4: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkProtocolTypeIPv6
 }
const kSCNetworkProtocolTypeIPv6: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkProtocolTypeProxies
 }
const kSCNetworkProtocolTypeProxies: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@const kSCNetworkProtocolTypeSMB
 }
const kSCNetworkProtocolTypeSMB: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@group Service configuration
 }

//#pragma mark -
//#pragma mark SCNetworkService configuration (typedefs, consts)

{!
	@typedef SCNetworkServiceRef
	@discussion This is the type of a reference to an object that represents
		a network service.
 }
type
	SCNetworkServiceRef = ^__SCNetworkService; { an opaque type }
	__SCNetworkService = record end;


{!
	@group Set configuration
 }

//#pragma mark -
//#pragma mark SCNetworkSet configuration (typedefs, consts)

{!
	@typedef SCNetworkSetRef
	@discussion This is the type of a reference to an object that represents
		a network set.
 }
type
	__SCNetworkSet = record end;
	SCNetworkSetRef = ^__SCNetworkSet;



{ --------------------------------------------------------------------------------
 * INTERFACES
 * -------------------------------------------------------------------------------- }

{!
	@group Interface configuration
 }

//#pragma mark -
//#pragma mark SCNetworkInterface configuration (APIs)

{!
	@function SCNetworkInterfaceGetTypeID
	@discussion Returns the type identifier of all SCNetworkInterface instances.
 }
function SCNetworkInterfaceGetTypeID: CFTypeID;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceCopyAll
	@discussion Returns all network capable interfaces on the system.
	@result The list of interfaces on the system.
		You must release the returned value.
 }
function SCNetworkInterfaceCopyAll: CFArrayRef { of SCNetworkInterfaceRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceGetSupportedInterfaceTypes
	@discussion Identify all of the network interface types (e.g. PPP) that
		can be layered on top of this interface.
	@param interface The network interface.
	@result The list of SCNetworkInterface types supported by the interface;
		NULL if no interface types are supported.
 }
function SCNetworkInterfaceGetSupportedInterfaceTypes( interface_: SCNetworkInterfaceRef ): CFArrayRef { of kSCNetworkInterfaceTypeXXX CFStringRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceGetSupportedProtocolTypes
	@discussion Identify all of the network protocol types (e.g. IPv4, IPv6) that
		can be layered on top of this interface.
	@param interface The network interface.
	@result The list of SCNetworkProtocol types supported by the interface;
		NULL if no protocol types are supported.
 }
function SCNetworkInterfaceGetSupportedProtocolTypes( interface_: SCNetworkInterfaceRef ): CFArrayRef { of kSCNetworkProtocolTypeXXX CFStringRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceCreateWithInterface
	@discussion Create a new network interface layered on top of another.  For
		example, this function would be used to create a "PPP" interface
		on top of a "modem".
	@param interface The network interface.
	@param interfaceType The type of SCNetworkInterface to be layered on
		top of the provided interface.
	@result A reference to the new SCNetworkInterface.
		You must release the returned value.
 }
function SCNetworkInterfaceCreateWithInterface( interface_: SCNetworkInterfaceRef; interfaceType: CFStringRef ): SCNetworkInterfaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceGetBSDName
	@discussion Returns the BSD interface (en0) or device name (modem)
		for the interface.
	@param interface The network interface.
	@result The BSD name associated with the interface (e.g. "en0");
		NULL if no BSD name is available.
 }
function SCNetworkInterfaceGetBSDName( interface_: SCNetworkInterfaceRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceGetConfiguration
	@discussion Returns the configuration settings associated with a interface.
	@param interface The network interface.
	@result The configuration settings associated with the interface;
		NULL if no configuration settings are associated with the interface
		or an error was encountered.
 }
function SCNetworkInterfaceGetConfiguration( interface_: SCNetworkInterfaceRef ): CFDictionaryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceGetExtendedConfiguration
	@discussion Returns the configuration settings associated with a interface.
	@param interface The network interface.
	@param extendedType A string representing the type of extended information (e.g. EAPOL).
	@result The configuration settings associated with the interface;
		NULL if no configuration settings are associated with the interface
		or an error was encountered.
 }
function SCNetworkInterfaceGetExtendedConfiguration( interface_: SCNetworkInterfaceRef; extendedType: CFStringRef ): CFDictionaryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCNetworkInterfaceGetHardwareAddressString
	@discussion Returns a displayable link layer address for the interface.
	@param interface The network interface.
	@result A string representing the hardware (MAC) address for the interface.
 }
function SCNetworkInterfaceGetHardwareAddressString( interface_: SCNetworkInterfaceRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceGetInterface
	@discussion For layered network interfaces, return the underlying interface.
	@param interface The network interface.
	@result The underlying network interface;
		NULL if this is a leaf interface.
 }
function SCNetworkInterfaceGetInterface( interface_: SCNetworkInterfaceRef ): SCNetworkInterfaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceGetInterfaceType
	@discussion Returns the associated network interface type.
	@param interface The network interface.
	@result The interface type.
 }
function SCNetworkInterfaceGetInterfaceType( interface_: SCNetworkInterfaceRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceGetLocalizedDisplayName
	@discussion Returns the localized name (e.g. "Ethernet", "FireWire") for
		the interface.
	@param interface The network interface.
	@result A localized, display name for the interface;
		NULL if no name is available.
 }
function SCNetworkInterfaceGetLocalizedDisplayName( interface_: SCNetworkInterfaceRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceSetConfiguration
	@discussion Stores the configuration settings for the interface.
	@param interface The network interface.
	@param config The configuration settings to associate with this interface.
	@result TRUE if the configuration was stored; FALSE if an error was encountered.
 }
function SCNetworkInterfaceSetConfiguration( interface_: SCNetworkInterfaceRef; config: CFDictionaryRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkInterfaceSetExtendedConfiguration
	@discussion Stores the configuration settings for the interface.
	@param interface The network interface.
	@param config The configuration settings to associate with this interface.
	@result TRUE if the configuration was stored; FALSE if an error was encountered.
 }
function SCNetworkInterfaceSetExtendedConfiguration( interface_: SCNetworkInterfaceRef; extendedType: CFStringRef; config: CFDictionaryRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

//#pragma mark -

{!
	@function SCNetworkInterfaceCopyMediaOptions
	@discussion For the specified network interface, returns information
		about the currently requested media options, the active media
		options, and the media options which are available.
	@param interface The desired network interface.
	@param current A pointer to memory that will be filled with a CFDictionaryRef
		representing the currently requested media options (subtype, options).
		If NULL, the current options will not be returned.
	@param active A pointer to memory that will be filled with a CFDictionaryRef
		representing the active media options (subtype, options).
		If NULL, the active options will not be returned.
	@param available A pointer to memory that will be filled with a CFArrayRef
		representing the possible media options (subtype, options).
		If NULL, the available options will not be returned.
	@param filter A boolean indicating whether the available options should be
		filtered to exclude those options which would not normally be
		requested by a user/admin (e.g. hw-loopback).
	@result TRUE if requested information has been returned.
 }
function SCNetworkInterfaceCopyMediaOptions( interface_: SCNetworkInterfaceRef; var current: CFDictionaryRef; var active: CFDictionaryRef; var available: CFArrayRef; filter: Boolean ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCNetworkInterfaceCopyMediaSubTypes
	@discussion For the provided interface configuration options, return a list
		of available media subtypes.
	@param available The available options as returned by the
		SCNetworkInterfaceCopyMediaOptions function.
	@result An array of available media subtypes CFString's (e.g. 10BaseT/UTP,
		100baseTX, etc).  NULL if no subtypes are available.
 }
function SCNetworkInterfaceCopyMediaSubTypes( available: CFArrayRef ): CFArrayRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCNetworkInterfaceCopyMediaSubTypeOptions
	@discussion For the provided interface configuration options and specific
		subtype, return a list of available media options.
	@param available The available options as returned by the
		SCNetworkInterfaceCopyMediaOptions function.
	@param subType The subtype
	@result An array of available media options.  Each of the available options
		is returned as an array of CFString's (e.g. <half-duplex>,
		<full-duplex,flow-control>).  NULL if no options are available.
 }
function SCNetworkInterfaceCopyMediaSubTypeOptions( available: CFArrayRef; subType: CFStringRef ): CFArrayRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCNetworkInterfaceCopyMTU
	@discussion For the specified network interface, returns information
		about the currently MTU setting and the range of allowable
		values.
	@param interface The desired network interface.
	@param mtu_cur A pointer to memory that will be filled with the current
		MTU setting for the interface.
	@param mtu_min A pointer to memory that will be filled with the minimum
		MTU setting for the interface.  If negative, the minimum setting
		could not be determined.
	@param mtu_max A pointer to memory that will be filled with the maximum
		MTU setting for the interface.  If negative, the maximum setting
		could not be determined.
	@result TRUE if requested information has been returned.
 }
function SCNetworkInterfaceCopyMTU( interface_: SCNetworkInterfaceRef; var mtu_cur: SInt32; var mtu_min: SInt32; var mtu_max: SInt32 ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCNetworkInterfaceSetMediaOptions
	@discussion For the specified network interface, sets the requested
		media subtype and options.
	@param interface The desired network interface.
	@param subtype The desired media subtype (e.g. "autoselect", "100baseTX", ...).
	@param options The desired media options (e.g. "half-duplex", "full-duplex", ...).
		If NULL, the active options will not be returned.
	@result TRUE if the configuration was updated; FALSE if an error was encountered.
 }
function SCNetworkInterfaceSetMediaOptions( interface_: SCNetworkInterfaceRef; subtype: CFStringRef; options: CFArrayRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCNetworkInterfaceSetMTU
	@discussion For the specified network interface, sets the
		requested MTU setting.
	@param interface The desired network interface.
	@param mtu The desired MTU setting for the interface.
	@result TRUE if the configuration was updated; FALSE if an error was encountered.
 }
function SCNetworkInterfaceSetMTU( interface_: SCNetworkInterfaceRef; mtu: SInt32 ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCNetworkInterfaceForceConfigurationRefresh
	@discussion Sends a notification to interested network configuration
		agents to immediately retry their configuration. For example,
		calling this function will cause the DHCP client to contact
		the DHCP server immediately rather than waiting until its
		timeout has expired.  The utility of this function is to
		allow the caller to give a hint to the system that the
		network infrastructure or configuration has changed.

		Note: This function requires root (euid==0) privilege or,
		alternatively, you may pass an SCNetworkInterface which
		is derived from a sequence of calls to :

			SCPreferencesCreateWithAuthorization
			SCNetworkSetCopy...
			SCNetworkServiceGetInterface
	@param interface The desired network interface.
	@result Returns TRUE if the notification was sent; FALSE otherwise.
 }
function SCNetworkInterfaceForceConfigurationRefresh( interface_: SCNetworkInterfaceRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@group Interface configuration (Bond)
 }

//#pragma mark -
//#pragma mark SCBondInterface configuration (APIs)

{!
	@function SCBondInterfaceCopyAll
	@discussion Returns all Ethernet Bond interfaces on the system.
	@param prefs The "preferences" session.
	@result The list of Ethernet Bond interfaces on the system.
		You must release the returned value.
 }
function SCBondInterfaceCopyAll( prefs: SCPreferencesRef ): CFArrayRef { of SCBondInterfaceRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondInterfaceCopyAvailableMemberInterfaces
	@discussion Returns all network capable devices on the system
		that can be added to an Ethernet Bond interface.
	@param prefs The "preferences" session.
	@result The list of interfaces.
		You must release the returned value.
 }
function SCBondInterfaceCopyAvailableMemberInterfaces( prefs: SCPreferencesRef ): CFArrayRef { of SCNetworkInterfaceRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondInterfaceCreate
	@discussion Create a new SCBondInterface interface.
	@param prefs The "preferences" session.
	@result A reference to the new SCBondInterface.
		You must release the returned value.
 }
function SCBondInterfaceCreate( prefs: SCPreferencesRef ): SCBondInterfaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondInterfaceRemove
	@discussion Removes the SCBondInterface from the configuration.
	@param bond The SCBondInterface interface.
	@result TRUE if the interface was removed; FALSE if an error was encountered.
 }
function SCBondInterfaceRemove( bond: SCBondInterfaceRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondInterfaceGetMemberInterfaces
	@discussion Returns the member interfaces for the specified Ethernet Bond interface.
	@param bond The SCBondInterface interface.
	@result The list of interfaces.
 }
function SCBondInterfaceGetMemberInterfaces( bond: SCBondInterfaceRef ): CFArrayRef { of SCNetworkInterfaceRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondInterfaceGetOptions
	@discussion Returns the configuration settings associated with a Ethernet Bond interface.
	@param bond The SCBondInterface interface.
	@result The configuration settings associated with the Ethernet Bond interface;
		NULL if no changes to the default configuration have been saved.
 }
function SCBondInterfaceGetOptions( bond: SCBondInterfaceRef ): CFDictionaryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondInterfaceSetMemberInterfaces
	@discussion Sets the member interfaces for the specified Ethernet Bond interface.
	@param bond The SCBondInterface interface.
	@param members The desired member interfaces.
	@result TRUE if the configuration was stored; FALSE if an error was encountered.
 }
function SCBondInterfaceSetMemberInterfaces (bond: SCBondInterfaceRef; members: CFArrayRef { of SCNetworkInterfaceRef's } ): Boolean; 
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondInterfaceSetLocalizedDisplayName
	@discussion Sets the localized display name for the specified Ethernet Bond interface.
	@param bond The SCBondInterface interface.
	@param newName The new display name.
	@result TRUE if the configuration was stored; FALSE if an error was encountered.
 }
function SCBondInterfaceSetLocalizedDisplayName( bond: SCBondInterfaceRef; newName: CFStringRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondInterfaceSetOptions
	@discussion Sets the configuration settings for the specified Ethernet Bond interface.
	@param bond The SCBondInterface interface.
	@param newOptions The new configuration settings.
	@result TRUE if the configuration was stored; FALSE if an error was encountered.
 }
function SCBondInterfaceSetOptions( bond: SCBondInterfaceRef; newOptions: CFDictionaryRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

//#pragma mark -

{!
	@function SCBondInterfaceCopyStatus
	@discussion Returns the status of the specified Ethernet Bond interface.
	@param bond The SCBondInterface interface.
	@result The status associated with the interface.
		You must release the returned value.
 }
function SCBondInterfaceCopyStatus( bond: SCBondInterfaceRef ): SCBondStatusRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondStatusGetTypeID
	@discussion Returns the type identifier of all SCBondStatus instances.
 }
function SCBondStatusGetTypeID: CFTypeID;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondStatusGetMemberInterfaces
	@discussion Returns the member interfaces that are represented with the
		Ethernet Bond interface.
	@param bondStatus The Ethernet Bond status.
	@result The list of interfaces.
 }
function SCBondStatusGetMemberInterfaces( bondStatus: SCBondStatusRef ): CFArrayRef { of SCNetworkInterfaceRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCBondStatusGetInterfaceStatus
	@discussion Returns the status of a specific member interface of an
		Ethernet Bond or the status of the bond as a whole.
	@param bondStatus The Ethernet Bond status.
	@param interface The specific member interface; NULL if you want the
		status of the Ethernet Bond.
	@result The interface status.

	Note: at present, no information about the status of the Ethernet
	      Bond is returned.  As long as one member interface is active
	      then the bond should be operational.
 }
function SCBondStatusGetInterfaceStatus( bondStatus: SCBondStatusRef; interface_: SCNetworkInterfaceRef ): CFDictionaryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@group Interface configuration (VLAN)
 }

//#pragma mark -
//#pragma mark SCVLANInterface configuration (APIs)

{!
	@function SCVLANInterfaceCopyAll
	@discussion Returns all VLAN interfaces on the system.
	@result The list of VLAN interfaces on the system.
		You must release the returned value.
 }
function SCVLANInterfaceCopyAll( prefs: SCPreferencesRef ): CFArrayRef { of SCVLANInterfaceRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCVLANInterfaceCopyAvailablePhysicalInterfaces
	@discussion Returns the network capable devices on the system
		that can be associated with a VLAN interface.
	@result The list of interfaces.
		You must release the returned value.
 }
function SCVLANInterfaceCopyAvailablePhysicalInterfaces: CFArrayRef { of SCNetworkInterfaceRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCVLANInterfaceCreate
	@discussion Create a new SCVLANInterface interface.
	@param prefs The "preferences" session.
	@param physical The physical interface to associate with the VLAN.
	@param tag The tag to associate with the VLAN.
	@result A reference to the new SCVLANInterface.
		You must release the returned value.

	Note: the tag must be in the range (1 <= tag <= 4094)
 }
function SCVLANInterfaceCreate( prefs: SCPreferencesRef; physical: SCNetworkInterfaceRef; tag: CFNumberRef ): SCVLANInterfaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCVLANInterfaceRemove
	@discussion Removes the SCVLANInterface from the configuration.
	@param vlan The SCVLANInterface interface.
	@result TRUE if the interface was removed; FALSE if an error was encountered.
 }
function SCVLANInterfaceRemove( vlan: SCVLANInterfaceRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCVLANInterfaceGetPhysicalInterface
	@discussion Returns the physical interface for the specified VLAN interface.
	@param vlan The SCVLANInterface interface.
	@result The list of interfaces.
 }
function SCVLANInterfaceGetPhysicalInterface( vlan: SCVLANInterfaceRef ): SCNetworkInterfaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCVLANInterfaceGetTag
	@discussion Returns the tag for the specified VLAN interface.
	@param vlan The SCVLANInterface interface.
	@result The tag.
 }
function SCVLANInterfaceGetTag( vlan: SCVLANInterfaceRef ): CFNumberRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCVLANInterfaceGetOptions
	@discussion Returns the configuration settings associated with the VLAN interface.
	@param vlan The SCVLANInterface interface.
	@result The configuration settings associated with the VLAN interface;
		NULL if no changes to the default configuration have been saved.
 }
function SCVLANInterfaceGetOptions( vlan: SCVLANInterfaceRef ): CFDictionaryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCVLANInterfaceSetPhysicalInterfaceAndTag
	@discussion Updates the specified VLAN interface.
	@param vlan The SCVLANInterface interface.
	@param physical The physical interface to associate with the VLAN.
	@param tag The tag to associate with the VLAN.
	@result TRUE if the configuration was stored; FALSE if an error was encountered.

	Note: the tag must be in the range (1 <= tag <= 4094)
 }
function SCVLANInterfaceSetPhysicalInterfaceAndTag( vlan: SCVLANInterfaceRef; physical: SCNetworkInterfaceRef; tag: CFNumberRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCVLANInterfaceSetLocalizedDisplayName
	@discussion Sets the localized display name for the specified VLAN interface.
	@param vlan The SCVLANInterface interface.
	@param newName The new display name.
	@result TRUE if the configuration was stored; FALSE if an error was encountered.
 }
function SCVLANInterfaceSetLocalizedDisplayName( vlan: SCVLANInterfaceRef; newName: CFStringRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCVLANInterfaceSetOptions
	@discussion Sets the configuration settings for the specified VLAN interface.
	@param vlan The SCVLANInterface interface.
	@param newOptions The new configuration settings.
	@result TRUE if the configuration was stored; FALSE if an error was encountered.
 }
function SCVLANInterfaceSetOptions( vlan: SCVLANInterfaceRef; newOptions: CFDictionaryRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);


{ --------------------------------------------------------------------------------
 * PROTOCOLS
 * -------------------------------------------------------------------------------- }

{!
	@group Protocol configuration
 }

//#pragma mark -
//#pragma mark SCNetworkProtocol configuration (APIs)

{!
	@function SCNetworkProtocolGetTypeID
	@discussion Returns the type identifier of all SCNetworkProtocol instances.
 }
function SCNetworkProtocolGetTypeID: CFTypeID;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkProtocolGetConfiguration
	@discussion Returns the configuration settings associated with the protocol.
	@param protocol The network protocol.
	@result The configuration settings associated with the protocol;
		NULL if no configuration settings are associated with the protocol
		or an error was encountered.
 }
function SCNetworkProtocolGetConfiguration( protocol: SCNetworkProtocolRef ): CFDictionaryRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkProtocolGetEnabled
	@discussion Returns whether this protocol has been enabled.
	@param protocol The network protocol.
	@result TRUE if the protocol is enabled.
 }
function SCNetworkProtocolGetEnabled( protocol: SCNetworkProtocolRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkProtocolGetProtocolType
	@discussion Returns the associated network protocol type.
	@param protocol The network protocol.
	@result The protocol type.
 }
function SCNetworkProtocolGetProtocolType( protocol: SCNetworkProtocolRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkProtocolSetConfiguration
	@discussion Stores the configuration settings for the protocol.
	@param protocol The network protocol.
	@param config The configuration settings to associate with this protocol.
	@result TRUE if the configuration was stored; FALSE if an error was encountered.
 }
function SCNetworkProtocolSetConfiguration( protocol: SCNetworkProtocolRef; config: CFDictionaryRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkProtocolSetEnabled
	@discussion Enables or disables the protocol.
	@param protocol The network protocol.
	@param enabled TRUE if the protocol should be enabled.
	@result TRUE if the enabled status was saved; FALSE if an error was encountered.
 }
function SCNetworkProtocolSetEnabled( protocol: SCNetworkProtocolRef; enabled: Boolean ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{ --------------------------------------------------------------------------------
 * SERVICES
 * -------------------------------------------------------------------------------- }

{!
	@group Service configuration
 }

//#pragma mark -
//#pragma mark SCNetworkService configuration (APIs)

{!
	@function SCNetworkServiceGetTypeID
	@discussion Returns the type identifier of all SCNetworkService instances.
 }
function SCNetworkServiceGetTypeID: CFTypeID;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceAddProtocolType
	@discussion Adds a network protocol of the specified type to the
		service.  The protocal configuration is set to default values
		that are appropriate for the interface associated with the
		service.
	@param service The network service.
	@param protocolType The type of SCNetworkProtocol to be added to the service.
	@result TRUE if the protocol was added to the service; FALSE if the
		protocol was already present or an error was encountered.
 }
function SCNetworkServiceAddProtocolType( service: SCNetworkServiceRef; protocolType: CFStringRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceCopyAll
	@discussion Returns all available network services for the specified preferences.
	@param prefs The "preferences" session.
	@result The list of SCNetworkService services associated with the preferences.
		You must release the returned value.
 }
function SCNetworkServiceCopyAll( prefs: SCPreferencesRef ): CFArrayRef { of SCNetworkServiceRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceCopyProtocols
	@discussion Returns all network protocols associated with the service.
	@param service The network service.
	@result The list of SCNetworkProtocol protocols associated with the service.
		You must release the returned value.
 }
function SCNetworkServiceCopyProtocols( service: SCNetworkServiceRef ): CFArrayRef { of SCNetworkProtocolRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceCreate
	@discussion Create a new network service for the specified interface in the
		configuration.
	@param prefs The "preferences" session.
	@result A reference to the new SCNetworkService.
		You must release the returned value.
 }
function SCNetworkServiceCreate( prefs: SCPreferencesRef; interface_: SCNetworkInterfaceRef ): SCNetworkServiceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceCopy
	@discussion Returns the network service with the specified identifier.
	@param prefs The "preferences" session.
	@param serviceID The unique identifier for the service.
	@result A reference to the SCNetworkService from the associated preferences;
		NULL if the serviceID does not exist in the preferences or if an
		error was encountered.
		You must release the returned value.
 }
function SCNetworkServiceCopy( prefs: SCPreferencesRef; serviceID: CFStringRef ): SCNetworkServiceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceEstablishDefaultConfiguration
	@discussion Establishes the "default" configuration for a network
		service.  This configuration includes the addition of
		network protocols for the service (with "default"
		configuration options).
	@param service The network service.
	@result TRUE if the configuration was updated; FALSE if an error was encountered.
}
function SCNetworkServiceEstablishDefaultConfiguration( service: SCNetworkServiceRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCNetworkServiceGetEnabled
	@discussion Returns whether this service has been enabled.
	@param service The network service.
	@result TRUE if the service is enabled.
 }
function SCNetworkServiceGetEnabled( service: SCNetworkServiceRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceGetInterface
	@discussion Returns the network interface associated with the service.
	@param service The network service.
	@result A reference to the SCNetworkInterface associated with the service;
		NULL if an error was encountered.
 }
function SCNetworkServiceGetInterface( service: SCNetworkServiceRef ): SCNetworkInterfaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceGetName
	@discussion Returns the [user specified] name associated with the service.
	@param service The network service.
	@result The [user specified] name.
 }
function SCNetworkServiceGetName( service: SCNetworkServiceRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceCopyProtocol
	@discussion Returns the network protocol of the specified type for
		the service.
	@param service The network service.
	@result A reference to the SCNetworkProtocol associated with the service;
		NULL if this protocol has not been added or if an error was encountered.
		You must release the returned value.
 }
function SCNetworkServiceCopyProtocol( service: SCNetworkServiceRef; protocolType: CFStringRef ): SCNetworkProtocolRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceGetServiceID
	@discussion Returns the identifier for the service.
	@param service The network service.
	@result The service identifier.
 }
function SCNetworkServiceGetServiceID( service: SCNetworkServiceRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceRemove
	@discussion Removes the network service from the configuration.
	@param service The network service.
	@result TRUE if the service was removed; FALSE if an error was encountered.
 }
function SCNetworkServiceRemove( service: SCNetworkServiceRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceRemoveProtocolType
	@discussion Removes the network protocol of the specified type from the service.
	@param service The network service.
	@param protocolType The type of SCNetworkProtocol to be removed from the service.
	@result TRUE if the protocol was removed to the service; FALSE if the
		protocol was not configured or an error was encountered.
 }
function SCNetworkServiceRemoveProtocolType( service: SCNetworkServiceRef; protocolType: CFStringRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceSetEnabled
	@discussion Enables or disables the service.
	@param service The network service.
	@param enabled TRUE if the service should be enabled.
	@result TRUE if the enabled status was saved; FALSE if an error was encountered.
 }
function SCNetworkServiceSetEnabled( service: SCNetworkServiceRef; enabled: Boolean ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkServiceSetName
	@discussion Stores the [user specified] name for the service.
	@param service The network service.
	@param name The [user defined] name to associate with the service.
	@result TRUE if the name was saved; FALSE if an error was encountered.

	Note: although not technically required, the [user specified] names
	for all services within any given set should be unique.  As such, an
	error will be returned if you attemp to name two services with the
	same string.
 }
function SCNetworkServiceSetName( service: SCNetworkServiceRef; name: CFStringRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);


{ --------------------------------------------------------------------------------
 * SETS
 * -------------------------------------------------------------------------------- }

{!
	@group Set configuration
 }

//#pragma mark -
//#pragma mark SCNetworkSet configuration (APIs)

{!
	@function SCNetworkSetGetTypeID
	@discussion Returns the type identifier of all SCNetworkSet instances.
 }
function SCNetworkSetGetTypeID: CFTypeID;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetAddService
	@discussion Adds the network service to the set.
	@param set The network set.
	@param service The service to be added.
	@result TRUE if the service was added to the set; FALSE if the
		service was already present or an error was encountered.

	Note: prior to Mac OS X 10.5, the Network Preferences UI
	did not support having a single service being a member of
	more than one set.  An error will be returned if you attempt
	to add a service to more than one set on a pre-10.5 system.
 }
function SCNetworkSetAddService( set_: SCNetworkSetRef; service: SCNetworkServiceRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetContainsInterface
	@discussion Checks if an interface is represented by at least one
		network service in the specified set.
	@param set The network set.
	@param interface The network interface.
	@result TRUE if the interface is represented in the set; FALSE if not.
 }
function SCNetworkSetContainsInterface( set_: SCNetworkSetRef; interface_: SCNetworkInterfaceRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_NA);

{!
	@function SCNetworkSetCopyAll
	@discussion Returns all available sets for the specified preferences.
	@param prefs The "preferences" session.
	@result The list of SCNetworkSet sets associated with the preferences.
		You must release the returned value.
 }
function SCNetworkSetCopyAll( prefs: SCPreferencesRef ): CFArrayRef { of SCNetworkSetRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetCopyCurrent
	@discussion Returns the "current" set.
	@param prefs The "preferences" session.
	@result The current set; NULL if no current set has been defined.
 }
function SCNetworkSetCopyCurrent( prefs: SCPreferencesRef ): SCNetworkSetRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetCopyServices
	@discussion Returns all network services associated with the set.
	@param set The network set.
	@result The list of SCNetworkService services associated with the set.
		You must release the returned value.
 }
function SCNetworkSetCopyServices( set_: SCNetworkSetRef ): CFArrayRef { of SCNetworkServiceRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetCreate
	@discussion Create a new set in the configuration.
	@param prefs The "preferences" session.
	@result A reference to the new SCNetworkSet.
		You must release the returned value.
 }
function SCNetworkSetCreate( prefs: SCPreferencesRef ): SCNetworkSetRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetCopy
	@discussion Returns the set with the specified identifier.
	@param prefs The "preferences" session.
	@param setID The unique identifier for the set.
	@result A reference to the SCNetworkSet from the associated preferences;
		NULL if the setID does not exist in the preferences or if an
		error was encountered.
		You must release the returned value.
 }
function SCNetworkSetCopy( prefs: SCPreferencesRef; setID: CFStringRef ): SCNetworkSetRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetGetName
	@discussion Returns the [user specified] name associated with the set.
	@param set The network set.
	@result The [user specified] name.
 }
function SCNetworkSetGetName( set_: SCNetworkSetRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetGetSetID
	@discussion Returns the identifier for the set.
	@param set The network set.
	@result The set identifier.
 }
function SCNetworkSetGetSetID( set_: SCNetworkSetRef ): CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetGetServiceOrder
	@discussion Returns the [user specified] ordering of network services
		within the set.
	@param set The network set.
	@result The ordered list of CFStringRef service identifiers associated
		with the set;
		NULL if no service order has been specified or if an error
		was encountered.
 }
function SCNetworkSetGetServiceOrder( set_: SCNetworkSetRef ): CFArrayRef { of serviceID CFStringRef's };
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetRemove
	@discussion Removes the set from the configuration.
	@param set The network set.
	@result TRUE if the set was removed; FALSE if an error was encountered.
 }
function SCNetworkSetRemove( set_: SCNetworkSetRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetRemoveService
	@discussion Removes the network service from the set.
	@param set The network set.
	@param service The service to be removed.
	@result TRUE if the service was removed from the set; FALSE if the
		service was not already present or an error was encountered.
 }
function SCNetworkSetRemoveService( set_: SCNetworkSetRef; service: SCNetworkServiceRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetSetCurrent
	@discussion Specifies the set that should be the "current" set.
	@param set The network set.
	@result TRUE if the current set was updated;
		FALSE if an error was encountered.
 }
function SCNetworkSetSetCurrent( set_: SCNetworkSetRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetSetName
	@discussion Stores the [user specified] name for the set.
	@param set The network set.
	@param name The [user defined] name to associate with the set.
	@result TRUE if the name was saved; FALSE if an error was encountered.

	Note: although not technically required, the [user specified] names
	for all set should be unique.  As such, an error will be returned if
	you attemp to name two sets with the same string.
 }
function SCNetworkSetSetName( set_: SCNetworkSetRef; name: CFStringRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);

{!
	@function SCNetworkSetSetServiceOrder
	@discussion Stores the [user specified] ordering of network services for the set.
	@param set The network set.
	@param newOrder The ordered list of CFStringRef service identifiers for the set.
	@result TRUE if the new service order was saved; FALSE if an error was encountered.
 }
function SCNetworkSetSetServiceOrder( set_: SCNetworkSetRef; newOrder: CFArrayRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_NA);	{ serviceID CFStringRef's }


{$endc} {TARGET_OS_MAC}

end.
