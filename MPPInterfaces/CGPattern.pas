{ CoreGraphics - CGPattern.h
   Copyright (c) 2000-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit CGPattern;
interface
uses MacTypes,CFBase,CGGeometry,CGAffineTransforms,CGBase,CGContext;
{$ALIGN POWER}


// CGPatternRef defined in CGBase


{ kCGPatternTilingNoDistortion: The pattern cell is not distorted when
   painted, however the spacing between pattern cells may vary by as much as
   1 device pixel.

   kCGPatternTilingConstantSpacingMinimalDistortion: Pattern cells are
   spaced consistently, however the pattern cell may be distorted by as much
   as 1 device pixel when the pattern is painted.

   kCGPatternTilingConstantSpacing: Pattern cells are spaced consistently as
   with kCGPatternTilingConstantSpacingMinimalDistortion, however the
   pattern cell may be distorted additionally to permit a more efficient
   implementation. }

type
	CGPatternTiling = SInt32;
const
	kCGPatternTilingNoDistortion = 0;
	kCGPatternTilingConstantSpacingMinimalDistortion = 1;
	kCGPatternTilingConstantSpacing = 2;

{ The drawing of the pattern is delegated to the callbacks. The callbacks
   may be called one or many times to draw the pattern.
    `version' is the version number of the structure passed in as a
      parameter to the CGPattern creation functions. The structure defined
      below is version 0.
    `drawPattern' should draw the pattern in the context `c'. `info' is the
      parameter originally passed to the CGPattern creation functions.
    `releaseInfo' is called when the pattern is deallocated. }

type
	CGPatternDrawPatternCallback = procedure( info: univ Ptr; c: CGContextRef );
	CGPatternReleaseInfoCallback = procedure( info: univ Ptr );

type
	CGPatternCallbacksPtr = ^CGPatternCallbacks;
	CGPatternCallbacks = record
		version: UInt32;
{$ifc TARGET_CPU_64}
		__alignment_dummy: UInt32;
{$endc}
		drawPattern: CGPatternDrawPatternCallback;
		releaseInfo: CGPatternReleaseInfoCallback;
	end;

{ Return the CFTypeID for CGPatternRefs. }

function CGPatternGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Create a pattern. }

function CGPatternCreate( info: univ Ptr; bounds: CGRect; matrix: CGAffineTransform; xStep: CGFloat; yStep: CGFloat; tiling: CGPatternTiling; isColored: CBool; const var callbacks: CGPatternCallbacks ): CGPatternRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRetain(pattern)', except it doesn't crash (as CF does)
   if `pattern' is NULL. }

function CGPatternRetain( pattern: CGPatternRef ): CGPatternRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRelease(pattern)', except it doesn't crash (as CF does)
   if `pattern' is NULL. }

procedure CGPatternRelease( pattern: CGPatternRef );
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);


end.
