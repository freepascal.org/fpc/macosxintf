{
     File:       CarbonCore/AIFF.h
 
     Contains:   Definition of AIFF file format components.
                 The contents of this header file are deprecated.
 
     Copyright:  � 1989-2011 by Apple Inc. All rights reserved.
}
unit AIFF;
interface
uses MacTypes;


{$ALIGN MAC68K}

const
	AIFFID = FOUR_CHAR_CODE('AIFF');
	AIFCID = FOUR_CHAR_CODE('AIFC');
	FormatVersionID = FOUR_CHAR_CODE('FVER');
	CommonID = FOUR_CHAR_CODE('COMM');
	FORMID = FOUR_CHAR_CODE('FORM');
	SoundDataID = FOUR_CHAR_CODE('SSND');
	MarkerID = FOUR_CHAR_CODE('MARK');
	InstrumentID = FOUR_CHAR_CODE('INST');
	MIDIDataID = FOUR_CHAR_CODE('MIDI');
	AudioRecordingID = FOUR_CHAR_CODE('AESD');
	ApplicationSpecificID = FOUR_CHAR_CODE('APPL');
	CommentID = FOUR_CHAR_CODE('COMT');
	NameID = FOUR_CHAR_CODE('NAME');
	AuthorID = FOUR_CHAR_CODE('AUTH');
	CopyrightID = FOUR_CHAR_CODE('(c) ');
	AnnotationID = FOUR_CHAR_CODE('ANNO');

const
	NoLooping = 0;
	ForwardLooping = 1;
	ForwardBackwardLooping = 2;


const
{ AIFF-C Versions }
	AIFCVersion1 = $A2805140;

{ Compression Names }
const
	NoneName = 'not compressed';
const
	ACE2to1Name = 'ACE 2-to-1';
const
	ACE8to3Name = 'ACE 8-to-3';
const
	MACE3to1Name = 'MACE 3-to-1';
const
	MACE6to1Name = 'MACE 6-to-1';
const
{ Compression Types }
	NoneType = FOUR_CHAR_CODE('NONE');
	ACE2Type = FOUR_CHAR_CODE('ACE2');
	ACE8Type = FOUR_CHAR_CODE('ACE8');
	MACE3Type = FOUR_CHAR_CODE('MAC3');
	MACE6Type = FOUR_CHAR_CODE('MAC6');

{
    AIFF.h use to define a type, ID, which causes conflicts with other headers and application which want to use
    this pretty common name as their own type.  If you were previously relying on this being defined here, you 
    should either define it yourself or change your references to it into a UInt32.
    
    typedef UInt32 ID;
}
type
	MarkerIdType = SInt16;
	ChunkHeaderPtr = ^ChunkHeader;
	ChunkHeader = record
		ckID: UInt32;
		ckSize: SInt32;
	end;
type
	ContainerChunkPtr = ^ContainerChunk;
	ContainerChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		formType: UInt32;
	end;
type
	FormatVersionChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		timestamp: UInt32;
	end;
	FormatVersionChunkPtr = ^FormatVersionChunk;
type
	CommonChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		numChannels: SInt16;
		numSampleFrames: UInt32;
		sampleSize: SInt16;
		sampleRate: extended80;
	end;
	CommonChunkPtr = ^CommonChunk;
type
	ExtCommonChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		numChannels: SInt16;
		numSampleFrames: UInt32;
		sampleSize: SInt16;
		sampleRate: extended80;
		compressionType: UInt32;
		compressionName: SInt8;     { variable length array, Pascal string }
	end;
	ExtCommonChunkPtr = ^ExtCommonChunk;
type
	SoundDataChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		offset: UInt32;
		blockSize: UInt32;
	end;
	SoundDataChunkPtr = ^SoundDataChunk;
type
	MarkerPtr = ^Marker;
	Marker = record
		id: MarkerIdType;
		position: UInt32;
		markerName: Str255;
	end;
type
	MarkerChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		numMarkers: UInt16;
		Markers: array [0..0] of Marker;             { variable length array }
	end;
	MarkerChunkPtr = ^MarkerChunk;
type
	AIFFLoopPtr = ^AIFFLoop;
	AIFFLoop = record
		playMode: SInt16;
		beginLoop: MarkerIdType;
		endLoop: MarkerIdType;
	end;
type
	InstrumentChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		baseFrequency: UInt8;
		detune: UInt8;
		lowFrequency: UInt8;
		highFrequency: UInt8;
		lowVelocity: UInt8;
		highVelocity: UInt8;
		gain: SInt16;
		sustainLoop: AIFFLoop;
		releaseLoop: AIFFLoop;
	end;
	InstrumentChunkPtr = ^InstrumentChunk;
type
	MIDIDataChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		MIDIdata: SInt8;            { variable length array }
	end;
	MIDIDataChunkPtr = ^MIDIDataChunk;
type
	AudioRecordingChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		AESChannelStatus:		packed array [0..23] of UInt8;
	end;
	AudioRecordingChunkPtr = ^AudioRecordingChunk;
type
	ApplicationSpecificChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		applicationSignature: OSType;
		data: UInt8;                { variable length array }
	end;
	ApplicationSpecificChunkPtr = ^ApplicationSpecificChunk;
type
	CommentPtr = ^Comment;
	Comment = record
		timeStamp: UInt32;
		marker: MarkerIdType;
		count: UInt16;
		text: SInt8;                { variable length array, Pascal string }
	end;
	Comment_fix = Comment;
type
	CommentsChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		numComments: UInt16;
		Comment: array [0..0] of Comment_fix;            { variable length array }
	end;
	CommentsChunkPtr = ^CommentsChunk;
type
	TextChunk = record
		ckID: UInt32;
		ckSize: SInt32;
		text: SInt8;                { variable length array, Pascal string }
	end;
	TextChunkPtr = ^TextChunk;


end.
