{
//  ABAddressBookC.h
//  AddressBook Framework
//
//  Copyright (c) 2003-2007 Apple Inc.  All rights reserved.
//
//
}
{	  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, 2004 }
{	  Pascal Translation Updated:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, November 2009 }
{     Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2018 }

unit ABAddressBook;
interface
uses MacTypes,ABTypedefs,ABGlobals,CFBase,CFArray,CFDictionary,CFData;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}



type
	ABRecordRef = UnivPtr;
	ABPersonRef = ^__ABPerson; { an opaque type }
	__ABPerson = record end;
	ABGroupRef = ^__ABGroup; { an opaque type }
	__ABGroup = record end;
	ABSearchElementRef = ^__ABSearchElementRef; { an opaque type }
	__ABSearchElementRef = record end;
	ABAddressBookRef = ^__ABAddressBookRef; { an opaque type }
	__ABAddressBookRef = record end;
	ABMultiValueRef = ^__ABMultiValue; { an opaque type }
	__ABMultiValue = record end;
	ABMutableMultiValueRef = ^__ABMultiValue; { an opaque type }

// --------------------------------------------------------------------------------
//	LSOpenCFURLRef support
// --------------------------------------------------------------------------------
// An application can open the Contacts app and select (and edit) a specific
// person by using the LSOpenCFURLRef API.
//
// To launch (or bring to front) the Contacts app and select a given person
//
// CFStringRef uniqueId = ABRecordCopyUniqueId(aPerson);
// CFStringRef urlString = CFStringCreateWithFormat(NULL, CFSTR(addressbook://%@), uniqueId);
// CFURLRef urlRef = CFURLCreateWithString(NULL, urlString, NULL);
// LSOpenCFURLRef(urlRef, NULL);
// CFRelease(uniqueId);
// CFRelease(urlRef);
// CFRelease(urlString);
//
// To launch (or bring to front) the Contacts app and edit a given person
//
// CFStringRef uniqueId = ABRecordCopyUniqueId(aPerson);
// CFStringRef urlString = CFStringCreateWithFormat(NULL, CFSTR(addressbook://%@?edit), uniqueId);
// CFURLRef urlRef = CFURLCreateWithString(NULL, urlString, NULL);
// LSOpenCFURLRef(urlRef, NULL);
// CFRelease(uniqueId);
// CFRelease(urlRef);
// CFRelease(urlString);

// --------------------------------------------------------------------------------
//      AddressBook
// --------------------------------------------------------------------------------

    // --- There is only one Address Book
function ABGetSharedAddressBook: ABAddressBookRef;

    // --- Searching
function ABCopyArrayOfMatchingRecords( addressBook: ABAddressBookRef; search: ABSearchElementRef ): CFArrayRef;

    // --- Saving
function ABSave( addressBook: ABAddressBookRef ): CBool;
function ABHasUnsavedChanges( addressBook: ABAddressBookRef ): CBool;

    // --- Me
function ABGetMe( addressBook: ABAddressBookRef ): ABPersonRef; // Not retain???
procedure ABSetMe( addressBook: ABAddressBookRef; moi: ABPersonRef );

    // Returns the record class Name for a particular uniqueId
function ABCopyRecordTypeFromUniqueId( addressBook: ABAddressBookRef; uniqueId: CFStringRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

    // --- Properties
    // Property names must be unique for a record type
function ABAddPropertiesAndTypes( addressBook: ABAddressBookRef; recordType: CFStringRef; propertiesAndTypes: CFDictionaryRef ): CFIndex;
function ABRemoveProperties( addressBook: ABAddressBookRef; recordType: CFStringRef; properties: CFArrayRef ): CFIndex;
function ABCopyArrayOfPropertiesForRecordType( addressBook: ABAddressBookRef; recordType: CFStringRef ): CFArrayRef;
function ABTypeOfProperty( addressBook: ABAddressBookRef; recordType: CFStringRef; proprty: CFStringRef ): ABPropertyType;

    // --- Records (Person, Group)
function ABCopyRecordForUniqueId( addressBook: ABAddressBookRef; uniqueId: CFStringRef ): ABRecordRef;
function ABAddRecord( addressBook: ABAddressBookRef; recrd: ABRecordRef ): CBool;
function ABRemoveRecord( addressBook: ABAddressBookRef; recrd: ABRecordRef ): CBool;

    // --- People
function ABCopyArrayOfAllPeople( addressBook: ABAddressBookRef ): CFArrayRef;                  // Array of ABPerson

    // --- Groups
function ABCopyArrayOfAllGroups( addressBook: ABAddressBookRef ): CFArrayRef;                  // Array of ABGroup

// --------------------------------------------------------------------------------
//      ABRecord
// --------------------------------------------------------------------------------

function ABRecordCreateCopy( recrd: ABRecordRef ): ABRecordRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function ABRecordCopyRecordType( recrd: ABRecordRef ): CFStringRef;

    // --- Property value
function ABRecordCopyValue( recrd: ABRecordRef; proprty: CFStringRef ): CFTypeRef;
    // returns a CFDictionary for multi-value properties
function ABRecordSetValue( recrd: ABRecordRef; proprty: CFStringRef; value: CFTypeRef ): CBool;
    // takes a CFDictionary for multi-value properties
function ABRecordRemoveValue( recrd: ABRecordRef; proprty: CFStringRef ): CBool;
   // is the record read only
function ABRecordIsReadOnly( recrd: ABRecordRef ): CBool;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

    // ---- Unique ID access convenience
function ABRecordCopyUniqueId( recrd: ABRecordRef ): CFStringRef;

// --------------------------------------------------------------------------------
//      ABPerson
// --------------------------------------------------------------------------------

function ABPersonCreate: ABPersonRef;

function ABPersonCreateWithVCardRepresentation( vCard: CFDataRef ): ABPersonRef;
function ABPersonCopyVCardRepresentation( person: ABPersonRef ): CFDataRef;

function ABPersonCopyParentGroups( person: ABPersonRef ): CFArrayRef; // Groups this person belongs to

    // --- Search elements
function ABPersonCreateSearchElement( proprty: CFStringRef; labl: CFStringRef; key: CFStringRef; value: CFTypeRef; comparison: ABSearchComparison ): ABSearchElementRef;

// --------------------------------------------------------------------------------
//      ABGroups
// --------------------------------------------------------------------------------

function ABGroupCreate: ABGroupRef;

    // --- Dealing with Persons
function ABGroupCopyArrayOfAllMembers( group: ABGroupRef ): CFArrayRef;
function ABGroupAddMember( group: ABGroupRef; personToAdd: ABPersonRef ): CBool;
function ABGroupRemoveMember( group: ABGroupRef; personToRemove: ABPersonRef ): CBool;

    // --- Dealing with Groups
function ABGroupCopyArrayOfAllSubgroups( group: ABGroupRef ): CFArrayRef;
function ABGroupAddGroup( group: ABGroupRef; groupToAdd: ABGroupRef ): CBool;
function ABGroupRemoveGroup( group: ABGroupRef; groupToRemove: ABGroupRef ): CBool;

    // --- Dealing with Parents
function ABGroupCopyParentGroups( group: ABGroupRef ): CFArrayRef;

    // --- Distribution list
function ABGroupSetDistributionIdentifier( group: ABGroupRef; person: ABPersonRef; proprty: CFStringRef; identifier: CFStringRef ): CBool;
function ABGroupCopyDistributionIdentifier( group: ABGroupRef; person: ABPersonRef; proprty: CFStringRef ): CFStringRef;

    // --- Search elements
function ABGroupCreateSearchElement( proprty: CFStringRef; labl: CFStringRef; key: CFStringRef; value: CFTypeRef; comparison: ABSearchComparison ): ABSearchElementRef;

// --------------------------------------------------------------------------------
//      ABSearchElement
// --------------------------------------------------------------------------------

function ABSearchElementCreateWithConjunction( conjunction: ABSearchConjunction; childrenSearchElement: CFArrayRef ): ABSearchElementRef;

function ABSearchElementMatchesRecord( searchElement: ABSearchElementRef; recrd: ABRecordRef ): CBool;

// --------------------------------------------------------------------------------
//      ABMultiValue
// --------------------------------------------------------------------------------

function ABMultiValueCreate: ABMultiValueRef;
function ABMultiValueCount( multiValue: ABMultiValueRef ): CFIndex;
function ABMultiValueCopyValueAtIndex( multiValue: ABMultiValueRef; index: CFIndex ): CFTypeRef;
function ABMultiValueCopyLabelAtIndex( multiValue: ABMultiValueRef; index: CFIndex ): CFStringRef;
function ABMultiValueCopyPrimaryIdentifier( multiValue: ABMultiValueRef ): CFStringRef;
function ABMultiValueIndexForIdentifier( multiValue: ABMultiValueRef; identifier: CFStringRef ): CFIndex;
function ABMultiValueCopyIdentifierAtIndex( multiValue: ABMultiValueRef; index: CFIndex ): CFStringRef;
function ABMultiValuePropertyType( multiValue: ABMultiValueRef ): ABPropertyType;
function ABMultiValueCreateCopy( multiValue: ABMultiValueRef ): ABMultiValueRef;

// --------------------------------------------------------------------------------
//      ABMutableMultiValue
// --------------------------------------------------------------------------------

function ABMultiValueCreateMutable: ABMutableMultiValueRef;
function ABMultiValueAdd( multiValue: ABMutableMultiValueRef; value: CFTypeRef; labl: CFStringRef; var outIdentifier: CFStringRef ): CBool;
function ABMultiValueInsert( multiValue: ABMutableMultiValueRef; value: CFTypeRef; labl: CFStringRef; index: CFIndex; var outIdentifier: CFStringRef ): CBool;
function ABMultiValueRemove( multiValue: ABMutableMultiValueRef; index: CFIndex ): CBool;
function ABMultiValueReplaceValue( multiValue: ABMutableMultiValueRef; value: CFTypeRef; index: CFIndex ): CBool;
function ABMultiValueReplaceLabel( multiValue: ABMutableMultiValueRef; labl: CFStringRef; index: CFIndex ): CBool;
function ABMultiValueSetPrimaryIdentifier( multiValue: ABMutableMultiValueRef; identifier: CFStringRef ): CBool;
function ABMultiValueCreateMutableCopy( multiValue: ABMultiValueRef ): ABMutableMultiValueRef;

// --------------------------------------------------------------------------------
//      Localization of properties or labels
// --------------------------------------------------------------------------------

function ABCopyLocalizedPropertyOrLabel( labelOrProperty: CFStringRef ): CFStringRef;

// --- Address formatting
function ABCreateFormattedAddressFromDictionary( addressBook: ABAddressBookRef; address: CFDictionaryRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function ABCopyDefaultCountryCode( addressBook: ABAddressBookRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

// --------------------------------------------------------------------------------
//      Person Image Loading
// --------------------------------------------------------------------------------

function ABPersonSetImageData( person: ABPersonRef; imageData: CFDataRef ): CBool;
function ABPersonCopyImageData( person: ABPersonRef ): CFDataRef;

type
	ABImageClientCallback = procedure( imageData: CFDataRef; tag: CFIndex; refcon: univ Ptr );

function ABBeginLoadingImageDataForClient( person: ABPersonRef; callback: ABImageClientCallback; refcon: univ Ptr ): CFIndex;
procedure ABCancelLoadingImageDataForTag( tag: CFIndex );

{$endc} {TARGET_OS_MAC}

end.
