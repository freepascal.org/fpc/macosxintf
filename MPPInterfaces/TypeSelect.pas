{
     File:       HIToolbox/TypeSelect.h
 
     Contains:   TypeSelect Utilties
 
     Version:    HIToolbox-624~3
 
     Copyright:  � 2000-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit TypeSelect;
interface
uses MacTypes,Events;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}


{
 *  TypeSelect
 *  
 *  Discussion:
 *    The TypeSelection API is deprecated in Mac OS X 10.4 and later,
 *    and is not included in the 64-bit version of HIToolbox.
 *    Applications should use the UCTypeSelect API in
 *    UnicodeUtilities.h.
 }
type
	TSCode = SInt16;
const
	tsPreviousSelectMode = -1;
	tsNormalSelectMode = 0;
	tsNextSelectMode = 1;

type
	TypeSelectRecordPtr = ^TypeSelectRecord;
	TypeSelectRecord = record
		tsrLastKeyTime: UInt32;
		tsrScript: ScriptCode;
		tsrKeyStrokes: Str63;
	end;
{$ifc not TARGET_CPU_64}
type
	IndexToStringProcPtr = function( item: SInt16; var itemsScript: ScriptCode; var itemsStringPtr: StringPtr; yourDataPtr: univ Ptr ): Boolean;
{GPC-ONLY-START}
	IndexToStringUPP = UniversalProcPtr; // should be IndexToStringProcPtr
{GPC-ONLY-ELSE}
	IndexToStringUPP = IndexToStringProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewIndexToStringUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewIndexToStringUPP( userRoutine: IndexToStringProcPtr ): IndexToStringUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeIndexToStringUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeIndexToStringUPP( userUPP: IndexToStringUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeIndexToStringUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeIndexToStringUPP( item: SInt16; var itemsScript: ScriptCode; var itemsStringPtr: StringPtr; yourDataPtr: univ Ptr; userUPP: IndexToStringUPP ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$endc} {not TARGET_CPU_64}

{$ifc not TARGET_CPU_64}
{
 *  TypeSelectClear()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use UCTypeSelectFlushSelectorData instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    UCTypeSelectFlushSelectorData instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure TypeSelectClear( var tsr: TypeSelectRecord );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
        Long ago the implementation of TypeSelectNewKey had a bug that
        required the high word of D0 to be zero or the function did not work.
        Although fixed now, we are continuing to clear the high word
        just in case someone tries to run on an older system.
    }
{
 *  TypeSelectNewKey()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use UCTypeSelectAddKeyToSelector instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    UCTypeSelectAddKeyToSelector instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function TypeSelectNewKey( const var theEvent: EventRecord; var tsr: TypeSelectRecord ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  TypeSelectFindItem()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use UCTypeSelectFindItem instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    UCTypeSelectFindItem instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function TypeSelectFindItem( const var tsr: TypeSelectRecord; listSize: SInt16; selectMode: TSCode; getStringProc: IndexToStringUPP; yourDataPtr: univ Ptr ): SInt16;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  TypeSelectCompare()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use UCTypeSelectCompare instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    UCTypeSelectCompare instead.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function TypeSelectCompare( const var tsr: TypeSelectRecord; testStringScript: ScriptCode; testStringPtr: StringPtr ): SInt16;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
