{
     File:       CarbonCore/DriverServices.h
 
     Contains:   Driver Services Interfaces.
                 The contents of this header file are deprecated.
                 Use CFAbsoluteTime or mach time routines instead.
 
     Copyright:  � 1985-2011 by Apple Inc. All rights reserved.
}
unit DriverServices;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


const
	durationMicrosecond = -1;  { Microseconds are negative}
	durationMillisecond = 1;    { Milliseconds are positive}
	durationSecond = 1000; { 1000 * durationMillisecond}
	durationMinute = 60000; { 60 * durationSecond,}
	durationHour = 3600000; { 60 * durationMinute,}
	durationDay = 86400000; { 24 * durationHour,}
	durationNoWait = 0;    { don't block}
	durationForever = $7FFFFFFF; { no time limit}


type
	Nanoseconds = UnsignedWide;
{
 *  UpTime()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function UpTime: AbsoluteTime;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  GetTimeBaseInfo()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }


{
 *  AbsoluteToNanoseconds()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function AbsoluteToNanoseconds( absoluteTime_: AbsoluteTime ): Nanoseconds;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  AbsoluteToDuration()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function AbsoluteToDuration( absoluteTime_: AbsoluteTime ): Duration;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  NanosecondsToAbsolute()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function NanosecondsToAbsolute( nanoseconds_: Nanoseconds ): AbsoluteTime;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  DurationToAbsolute()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function DurationToAbsolute( duration_: Duration ): AbsoluteTime;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  AddAbsoluteToAbsolute()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function AddAbsoluteToAbsolute( absoluteTime1: AbsoluteTime; absoluteTime2: AbsoluteTime ): AbsoluteTime;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  SubAbsoluteFromAbsolute()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function SubAbsoluteFromAbsolute( leftAbsoluteTime: AbsoluteTime; rightAbsoluteTime: AbsoluteTime ): AbsoluteTime;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  AddNanosecondsToAbsolute()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function AddNanosecondsToAbsolute( nanoseconds_: Nanoseconds; absoluteTime_: AbsoluteTime ): AbsoluteTime;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  AddDurationToAbsolute()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function AddDurationToAbsolute( duration_: Duration; absoluteTime_: AbsoluteTime ): AbsoluteTime;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  SubNanosecondsFromAbsolute()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function SubNanosecondsFromAbsolute( nanoseconds_: Nanoseconds; absoluteTime_: AbsoluteTime ): AbsoluteTime;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  SubDurationFromAbsolute()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function SubDurationFromAbsolute( duration_: Duration; absoluteTime_: AbsoluteTime ): AbsoluteTime;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  AbsoluteDeltaToNanoseconds()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function AbsoluteDeltaToNanoseconds( leftAbsoluteTime: AbsoluteTime; rightAbsoluteTime: AbsoluteTime ): Nanoseconds;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  AbsoluteDeltaToDuration()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function AbsoluteDeltaToDuration( leftAbsoluteTime: AbsoluteTime; rightAbsoluteTime: AbsoluteTime ): Duration;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  DurationToNanoseconds()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function DurationToNanoseconds( theDuration: Duration ): Nanoseconds;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  NanosecondsToDuration()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.6 and later
 }
function NanosecondsToDuration( theNanoseconds: Nanoseconds ): Duration;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{$endc} {TARGET_OS_MAC}

end.
