{
 *  ABTypedefs.h
 *  AddressBook Framework
 *
 *  Copyright (c) 2003-2007 Apple Inc.  All rights reserved.
 *
 }
{	  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, 2004 }
{	  Pascal Translation Updated:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, November 2009 }
{     Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2018 }

unit ABTypedefs;
interface
uses MacTypes,CFBase;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


// These typedefs are CFIndexes rather then NSIntegers because
// they're used in the C API as well as the Obj-C one.

// ================================================================
//      Property Type
// ================================================================

const
	kABMultiValueMask  = $100;

type
	ABPropertyType = CFIndex;
const
    kABErrorInProperty           = 0;
    kABStringProperty            = 1;
    kABIntegerProperty           = 2;
    kABRealProperty              = 3;
    kABDateProperty              = 4;
    kABArrayProperty             = 5;
    kABDictionaryProperty        = 6;
    kABDataProperty              = 7;
// #if MAC_OS_X_VERSION_10_7 <= MAC_OS_X_VERSION_MAX_ALLOWED
	kABDateComponentsProperty    = 8;
// #endif
    kABMultiStringProperty       = kABMultiValueMask or kABStringProperty;
    kABMultiIntegerProperty      = kABMultiValueMask or kABIntegerProperty;
    kABMultiRealProperty         = kABMultiValueMask or kABRealProperty;
    kABMultiDateProperty         = kABMultiValueMask or kABDateProperty;
    kABMultiArrayProperty        = kABMultiValueMask or kABArrayProperty;
    kABMultiDictionaryProperty   = kABMultiValueMask or kABDictionaryProperty;
    kABMultiDataProperty         = kABMultiValueMask or kABDataProperty;
// #if MAC_OS_X_VERSION_10_7 <= MAC_OS_X_VERSION_MAX_ALLOWED
	kABMultiDateComponentsProperty = kABMultiValueMask or kABDateComponentsProperty;
// #endif

// ================================================================
//      Search APIs
// ================================================================

type
	ABSearchComparison = CFIndex;
const
	kABEqual 										= 0;
	kABNotEqual 									= 1;
	kABLessThan  		 		 					= 2;
	kABLessThanOrEqual  		 					= 3;
	kABGreaterThan 		 		 					= 4;
	kABGreaterThanOrEqual 		 					= 5;
	kABEqualCaseInsensitive 		 				= 6;
	kABContainsSubString 		 					= 7;
	kABContainsSubStringCaseInsensitive 			= 8;
	kABPrefixMatch 		 		 					= 9;
	kABPrefixMatchCaseInsensitive 					= 10;
// #if MAC_OS_X_VERSION_10_3 <= MAC_OS_X_VERSION_MAX_ALLOWED
	kABBitsInBitFieldMatch 		 					= 11;
// #endif
// #if MAC_OS_X_VERSION_10_4 <= MAC_OS_X_VERSION_MAX_ALLOWED
	kABDoesNotContainSubString 		 		 		= 12;
	kABDoesNotContainSubStringCaseInsensitive 		= 13;
	kABNotEqualCaseInsensitive 		 		 		= 14;
	kABSuffixMatch 		 		 		 		 	= 15;
	kABSuffixMatchCaseInsensitive 		 		 	= 16;
	kABWithinIntervalAroundToday 		 		 	= 17;
	kABWithinIntervalAroundTodayYearless 		 	= 18;
	kABNotWithinIntervalAroundToday 		 		= 19;
	kABNotWithinIntervalAroundTodayYearless 		= 20;
	kABWithinIntervalFromToday 		 		 		= 21;
	kABWithinIntervalFromTodayYearless 		 		= 22;
	kABNotWithinIntervalFromToday 		 		 	= 23;
	kABNotWithinIntervalFromTodayYearless 		 	= 24;
// #endif

type
	ABSearchConjunction = CFIndex;
const
	kABSearchAnd = 0;
	kABSearchOr = 1;

{$endc} {TARGET_OS_MAC}

end.
