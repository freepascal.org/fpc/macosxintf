{
 * Copyright (c) 1999-2001,2004 Apple Computer, Inc. All Rights Reserved.
 * 
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 *
 * cssmkrapi.h -- Application Programmers Interface for Key Recovery Modules
 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, September 2010 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }
unit cssmkrapi;
interface
uses MacTypes,cssmtype;

{$ifc TARGET_OS_MAC}

{FPC-ONLY-START}
{$packrecords c}
{FPC-ONLY-FINISH}
{GPC-ONLY-START}
{$no-pack-struct, maximum-field-alignment 0}
{GPC-ONLY-FINISH}

type
	CSSM_KRSP_HANDLE = UInt32; { Key Recovery Service Provider Handle }

type
	cssm_kr_name = record
		Type_: UInt8; { namespace type }
		Length: UInt8; { name string length }
		Name: CStringPtr; { name string }
	end;

type
	CSSM_KR_PROFILE_PTR = ^cssm_kr_profile;
	CSSM_KR_PROFILEPtr = ^cssm_kr_profile;
	cssm_kr_profile = record
		UserName: CSSM_KR_NAME; { name of the user }
		UserCertificate: CSSM_CERTGROUP_PTR; { public key certificate of the user }
		KRSCertChain: CSSM_CERTGROUP_PTR; { cert chain for the KRSP coordinator }
		LE_KRANum: UInt8; { number of KRA cert chains in the following list }
		LE_KRACertChainList: CSSM_CERTGROUP_PTR; { list of Law enforcement KRA certificate chains }
		ENT_KRANum: UInt8; { number of KRA cert chains in the following list }
		ENT_KRACertChainList: CSSM_CERTGROUP_PTR; { list of Enterprise KRA certificate chains }
		INDIV_KRANum: UInt8; { number of KRA cert chains in the following list }
		INDIV_KRACertChainList: CSSM_CERTGROUP_PTR; { list of Individual KRA certificate chains }
		INDIV_AuthenticationInfo: CSSM_DATA_PTR; { authentication information for individual key recovery }
		KRSPFlags: UInt32; { flag values to be interpreted by KRSP }
		KRSPExtensions: CSSM_DATA_PTR; { reserved for extensions specific to KRSPs }
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_KR_WRAPPEDPRODUCT_INFO_PTR = ^CSSM_KR_WRAPPEDPRODUCT_INFO;
	CSSM_KR_WRAPPEDPRODUCT_INFOPtr = ^CSSM_KR_WRAPPEDPRODUCT_INFO;
	CSSM_KR_WRAPPEDPRODUCT_INFO = record
		StandardVersion: CSSM_VERSION;
		StandardDescription: CSSM_STRING;
		ProductVersion: CSSM_VERSION;
		ProductDescription: CSSM_STRING;
		ProductVendor: CSSM_STRING;
		ProductFlags: UInt32;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_KRSUBSERVICE_PTR = ^cssm_krsubservice;
	CSSM_KRSUBSERVICEPtr = ^cssm_krsubservice;
	cssm_krsubservice = record
		SubServiceId: UInt32;
		Description: CStringPtr; { Description of this sub service }
		WrappedProduct: CSSM_KR_WRAPPEDPRODUCT_INFO;
	end;

type
	CSSM_KR_POLICY_TYPE = UInt32;
const
	CSSM_KR_INDIV_POLICY = $00000001;
const
	CSSM_KR_ENT_POLICY = $00000002;
const
	CSSM_KR_LE_MAN_POLICY = $00000003;
const
	CSSM_KR_LE_USE_POLICY = $00000004;

type
	CSSM_KR_POLICY_FLAGS = UInt32;

const
	CSSM_KR_INDIV = $00000001;
const
	CSSM_KR_ENT = $00000002;
const
	CSSM_KR_LE_MAN = $00000004;
const
	CSSM_KR_LE_USE = $00000008;
const
	CSSM_KR_LE = (CSSM_KR_LE_MAN or CSSM_KR_LE_USE);
const
	CSSM_KR_OPTIMIZE = $00000010;
const
	CSSM_KR_DROP_WORKFACTOR = $00000020;

type
	CSSM_KR_POLICY_LIST_ITEM_PTR = ^cssm_kr_policy_list_item;
	CSSM_KR_POLICY_LIST_ITEMPtr = ^cssm_kr_policy_list_item;
	cssm_kr_policy_list_item = record
		next: CSSM_KR_POLICY_LIST_ITEM_PTR;
		AlgorithmId: CSSM_ALGORITHMS;
		Mode: CSSM_ENCRYPT_MODE;
		MaxKeyLength: UInt32;
		MaxRounds: UInt32;
		WorkFactor: UInt8;
		PolicyFlags: CSSM_KR_POLICY_FLAGS;
		AlgClass: CSSM_CONTEXT_TYPE;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

type
	CSSM_KR_POLICY_INFO_PTR = ^cssm_kr_policy_info;
	CSSM_KR_POLICY_INFOPtr = ^cssm_kr_policy_info;
	cssm_kr_policy_info = record
		krbNotAllowed: CSSM_BOOL;
		numberOfEntries: UInt32;
		policyEntry: CSSM_KR_POLICY_LIST_ITEMPtr;
	end;
	DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;


{ Key Recovery Module Mangement Operations }

function CSSM_KR_SetEnterpriseRecoveryPolicy( const var RecoveryPolicyFileName: CSSM_DATA; const var OldPassPhrase: CSSM_ACCESS_CREDENTIALS; const var NewPassPhrase: CSSM_ACCESS_CREDENTIALS ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;


{ Key Recovery Context Operations }

function CSSM_KR_CreateRecoveryRegistrationContext( KRSPHandle: CSSM_KRSP_HANDLE; var NewContext: CSSM_CC_HANDLE ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

function CSSM_KR_CreateRecoveryEnablementContext( KRSPHandle: CSSM_KRSP_HANDLE; const var LocalProfile: CSSM_KR_PROFILE; const var RemoteProfile: CSSM_KR_PROFILE; var NewContext: CSSM_CC_HANDLE ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

function CSSM_KR_CreateRecoveryRequestContext( KRSPHandle: CSSM_KRSP_HANDLE; const var LocalProfile: CSSM_KR_PROFILE; var NewContext: CSSM_CC_HANDLE ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

function CSSM_KR_GetPolicyInfo( CCHandle: CSSM_CC_HANDLE; var EncryptionProhibited: CSSM_KR_POLICY_FLAGS; var WorkFactor: UInt32 ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;


{ Key Recovery Registration Operations }

function CSSM_KR_RegistrationRequest( RecoveryRegistrationContext: CSSM_CC_HANDLE; const var KRInData: CSSM_DATA; const var AccessCredentials: CSSM_ACCESS_CREDENTIALS; KRFlags: CSSM_KR_POLICY_FLAGS; var EstimatedTime: SInt32; ReferenceHandle: CSSM_HANDLE_PTR ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

function CSSM_KR_RegistrationRetrieve( KRSPHandle: CSSM_KRSP_HANDLE; ReferenceHandle: CSSM_HANDLE; const var AccessCredentials: CSSM_ACCESS_CREDENTIALS; var EstimatedTime: SInt32; KRProfile: CSSM_KR_PROFILE_PTR ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;


{ Key Recovery Enablement Operations }

function CSSM_KR_GenerateRecoveryFields( KeyRecoveryContext: CSSM_CC_HANDLE; CCHandle: CSSM_CC_HANDLE; const var KRSPOptions: CSSM_DATA; KRFlags: CSSM_KR_POLICY_FLAGS; KRFields: CSSM_DATA_PTR; var NewCCHandle: CSSM_CC_HANDLE ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

function CSSM_KR_ProcessRecoveryFields( KeyRecoveryContext: CSSM_CC_HANDLE; CryptoContext: CSSM_CC_HANDLE; const var KRSPOptions: CSSM_DATA; KRFlags: CSSM_KR_POLICY_FLAGS; const var KRFields: CSSM_DATA; var NewCryptoContext: CSSM_CC_HANDLE ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;


{ Key Recovery Request Operations }

function CSSM_KR_RecoveryRequest( RecoveryRequestContext: CSSM_CC_HANDLE; const var KRInData: CSSM_DATA; const var AccessCredentials: CSSM_ACCESS_CREDENTIALS; var EstimatedTime: SInt32; ReferenceHandle: CSSM_HANDLE_PTR ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

function CSSM_KR_RecoveryRetrieve( KRSPHandle: CSSM_KRSP_HANDLE; ReferenceHandle: CSSM_HANDLE; const var AccessCredentials: CSSM_ACCESS_CREDENTIALS; var EstimatedTime: SInt32; CacheHandle: CSSM_HANDLE_PTR; var NumberOfRecoveredKeys: UInt32 ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

function CSSM_KR_GetRecoveredObject( KRSPHandle: CSSM_KRSP_HANDLE; CacheHandle: CSSM_HANDLE; IndexInResults: UInt32; CSPHandle: CSSM_CSP_HANDLE; const var CredAndAclEntry: CSSM_RESOURCE_CONTROL_CONTEXT; Flags: UInt32; RecoveredKey: CSSM_KEY_PTR; OtherInfo: CSSM_DATA_PTR ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

function CSSM_KR_RecoveryRequestAbort( KRSPHandle: CSSM_KRSP_HANDLE; CacheHandle: CSSM_HANDLE ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

function CSSM_KR_QueryPolicyInfo( KRSPHandle: CSSM_KRSP_HANDLE; AlgorithmID: CSSM_ALGORITHMS; Mode: CSSM_ENCRYPT_MODE; Class: CSSM_CONTEXT_TYPE; var PolicyInfoData: CSSM_KR_POLICY_INFO_PTR ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;


{ Extensibility Functions }

function CSSM_KR_PassThrough( KRSPHandle: CSSM_KRSP_HANDLE; KeyRecoveryContext: CSSM_CC_HANDLE; CryptoContext: CSSM_CC_HANDLE; PassThroughId: UInt32; InputParams: {const} univ Ptr; OutputParams: UnivPtrPtr ): CSSM_RETURN;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

{$endc} {TARGET_OS_MAC}

end.
