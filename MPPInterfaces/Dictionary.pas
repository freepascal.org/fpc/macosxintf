{
     File:       LangAnalysis/Dictionary.h
 
     Contains:   Dictionary Manager Interfaces
 
     Version:    LanguageAnalysis-242~23
 
     Copyright:  � 1992-2008 by Apple Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit Dictionary;
interface
uses MacTypes,AEDataModel,Files,AERegistry,CodeFragments,MacErrors;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

{
=============================================================================================
 Modern Dictionary Manager
=============================================================================================
}
{
    Dictionary information
}
const
	kDictionaryFileType = FOUR_CHAR_CODE('dict');
	kDCMDictionaryHeaderSignature = FOUR_CHAR_CODE('dict');
	kDCMDictionaryHeaderVersion = 2;

const
	kDCMAnyFieldTag = typeWildCard;
	kDCMAnyFieldType = typeWildCard;

{
    Contents of a Field Info Record (an AERecord)
}
const
	keyDCMFieldTag = FOUR_CHAR_CODE('ftag'); { typeEnumeration }
	keyDCMFieldType = FOUR_CHAR_CODE('ftyp'); { typeEnumeration }
	keyDCMMaxRecordSize = FOUR_CHAR_CODE('mrsz'); { typeMagnitude }
	keyDCMFieldAttributes = FOUR_CHAR_CODE('fatr');
	keyDCMFieldDefaultData = FOUR_CHAR_CODE('fdef');
	keyDCMFieldName = FOUR_CHAR_CODE('fnam'); { typeChar }
	keyDCMFieldFindMethods = FOUR_CHAR_CODE('ffnd'); { typeAEList of typeDCMFindMethod }

{
    Special types for fields of a Field Info Record
}
const
	typeDCMFieldAttributes = FOUR_CHAR_CODE('fatr');
	typeDCMFindMethod = FOUR_CHAR_CODE('fmth');


{
    Field attributes
}
const
	kDCMIndexedFieldMask = $00000001;
	kDCMRequiredFieldMask = $00000002;
	kDCMIdentifyFieldMask = $00000004;
	kDCMFixedSizeFieldMask = $00000008;
	kDCMHiddenFieldMask = $80000000;

type
	DCMFieldAttributes = OptionBits;
{
    Standard dictionary properties
}
const
	pDCMAccessMethod = FOUR_CHAR_CODE('amtd'); { data type: typeChar ReadOnly }
	pDCMPermission = FOUR_CHAR_CODE('perm'); { data type: typeUInt16 }
	pDCMListing = FOUR_CHAR_CODE('list'); { data type: typeUInt16 }
	pDCMMaintenance = FOUR_CHAR_CODE('mtnc'); { data type: typeUInt16 }
	pDCMLocale = FOUR_CHAR_CODE('locl'); { data type: typeUInt32.  Optional; default = kLocaleIdentifierWildCard }
	pDCMClass = pClass; { data type: typeUInt16 }
	pDCMCopyright = FOUR_CHAR_CODE('info'); { data type: typeChar }

{
    pDCMPermission property constants
}
const
	kDCMReadOnlyDictionary = 0;
	kDCMReadWriteDictionary = 1;

{
    pDCMListing property constants
}
const
	kDCMAllowListing = 0;
	kDCMProhibitListing = 1;

{
    pDCMClass property constants
}
const
	kDCMUserDictionaryClass = 0;
	kDCMSpecificDictionaryClass = 1;
	kDCMBasicDictionaryClass = 2;

{
    Standard search method
}
const
	kDCMFindMethodExactMatch = kAEEquals;
	kDCMFindMethodBeginningMatch = kAEBeginsWith;
	kDCMFindMethodContainsMatch = kAEContains;
	kDCMFindMethodEndingMatch = kAEEndsWith;
	kDCMFindMethodForwardTrie = FOUR_CHAR_CODE('ftri'); { used for morphological analysis}
	kDCMFindMethodBackwardTrie = FOUR_CHAR_CODE('btri'); { used for morphological analysis}

type
	DCMFindMethod = OSType;
	DCMFindMethodPtr = ^DCMFindMethod;
{
    AccessMethod features
}
const
	kDCMCanUseFileDictionaryMask = $00000001;
	kDCMCanUseMemoryDictionaryMask = $00000002;
	kDCMCanStreamDictionaryMask = $00000004;
	kDCMCanHaveMultipleIndexMask = $00000008;
	kDCMCanModifyDictionaryMask = $00000010;
	kDCMCanCreateDictionaryMask = $00000020;
	kDCMCanAddDictionaryFieldMask = $00000040;
	kDCMCanUseTransactionMask = $00000080;

type
	DCMAccessMethodFeature = OptionBits;
	DCMUniqueID = UInt32;
	DCMObjectID = ^OpaqueDCMObjectID; { an opaque type }
	OpaqueDCMObjectID = record end;
	DCMObjectIDPtr = ^DCMObjectID;  { when a var xx:DCMObjectID parameter can be nil, it is changed to xx: DCMObjectIDPtr }
	DCMAccessMethodID = DCMObjectID;
	DCMDictionaryID = DCMObjectID;
	DCMObjectRef = ^OpaqueDCMObjectRef; { an opaque type }
	OpaqueDCMObjectRef = record end;
	DCMObjectRefPtr = ^DCMObjectRef;  { when a var xx:DCMObjectRef parameter can be nil, it is changed to xx: DCMObjectRefPtr }
	DCMDictionaryRef = DCMObjectRef;
	DCMDictionaryStreamRef = DCMObjectRef;
	DCMObjectIterator = ^OpaqueDCMObjectIterator; { an opaque type }
	OpaqueDCMObjectIterator = record end;
	DCMObjectIteratorPtr = ^DCMObjectIterator;  { when a var xx:DCMObjectIterator parameter can be nil, it is changed to xx: DCMObjectIteratorPtr }
	DCMAccessMethodIterator = DCMObjectIterator;
	DCMDictionaryIterator = DCMObjectIterator;
	DCMFoundRecordIterator = ^OpaqueDCMFoundRecordIterator; { an opaque type }
	OpaqueDCMFoundRecordIterator = record end;
	DCMFoundRecordIteratorPtr = ^DCMFoundRecordIterator;  { when a var xx:DCMFoundRecordIterator parameter can be nil, it is changed to xx: DCMFoundRecordIteratorPtr }
{
    Field specification declarations
}
type
	DCMFieldTag = DescType;
	DCMFieldTagPtr = ^DCMFieldTag;
	DCMFieldType = DescType;
{
    Dictionary header information
}
type
	DCMDictionaryHeader = record
		headerSignature: FourCharCode;
		headerVersion: UInt32;
		headerSize: UInt32;
		accessMethod: Str63;
	end;
{
    Callback routines
}
type
	DCMProgressFilterProcPtr = function( determinateProcess: Boolean; percentageComplete: UInt16; callbackUD: UInt32 ): Boolean;
	DCMProgressFilterUPP = DCMProgressFilterProcPtr;
{
 *  NewDCMProgressFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
 *  DisposeDCMProgressFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
 *  InvokeDCMProgressFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{$ifc not TARGET_CPU_64}
{
    Library version
}
{$ifc not TARGET_CPU_64}
{
 *  DCMLibraryVersion()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMLibraryVersion: UInt32;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Create/delete dictionary
}
{
 *  DCMNewDictionary()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMNewDictionary( accessMethodID: DCMAccessMethodID; const var newDictionaryFile: FSSpec; scriptTag: ScriptCode; const var listOfFieldInfoRecords: AEDesc; invisible: Boolean; recordCapacity: ItemCount; var newDictionary: DCMDictionaryID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMDeriveNewDictionary()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMDeriveNewDictionary( srcDictionary: DCMDictionaryID; const var newDictionaryFile: FSSpec; scriptTag: ScriptCode; invisible: Boolean; recordCapacity: ItemCount; var newDictionary: DCMDictionaryID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMDeleteDictionary()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMDeleteDictionary( dictionaryID: DCMDictionaryID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Register dictionary
}
{
 *  DCMRegisterDictionaryFile()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMRegisterDictionaryFile( const var dictionaryFile: FSSpec; var dictionaryID: DCMDictionaryID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMUnregisterDictionary()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMUnregisterDictionary( dictionaryID: DCMDictionaryID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Open dictionary
}
{
 *  DCMOpenDictionary()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMOpenDictionary( dictionaryID: DCMDictionaryID; protectKeySize: ByteCount; protectKey: ConstLogicalAddress; var dictionaryRef: DCMDictionaryRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMCloseDictionary()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMCloseDictionary( dictionaryRef: DCMDictionaryRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Change access privilege
}
{
 *  DCMGetDictionaryWriteAccess()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetDictionaryWriteAccess( dictionaryRef: DCMDictionaryRef; timeOutDuration: Duration ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMReleaseDictionaryWriteAccess()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMReleaseDictionaryWriteAccess( dictionaryRef: DCMDictionaryRef; commitTransaction: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Find records
}
{
 *  DCMFindRecords()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMFindRecords( dictionaryRef: DCMDictionaryRef; keyFieldTag: DCMFieldTag; keySize: ByteCount; keyData: ConstLogicalAddress; findMethod: DCMFindMethod; preFetchedDataNum: ItemCount; preFetchedData: {variable-size-array} DCMFieldTagPtr; skipCount: ItemCount; maxRecordCount: ItemCount; var recordIterator: DCMFoundRecordIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMCountRecordIterator()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMCountRecordIterator( recordIterator: DCMFoundRecordIterator ): ItemCount;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMIterateFoundRecord()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMIterateFoundRecord( recordIterator: DCMFoundRecordIterator; maxKeySize: ByteCount; var actualKeySize: ByteCount; keyData: LogicalAddress; var uniqueID: DCMUniqueID; var dataList: AEDesc ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMDisposeRecordIterator()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMDisposeRecordIterator( recordIterator: DCMFoundRecordIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Dump dictionary
}
{
 *  DCMCountRecord()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMCountRecord( dictionaryID: DCMDictionaryID; var count: ItemCount ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetRecordSequenceNumber()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetRecordSequenceNumber( dictionaryRef: DCMDictionaryRef; keyFieldTag: DCMFieldTag; keySize: ByteCount; keyData: ConstLogicalAddress; uniqueID: DCMUniqueID; var sequenceNum: ItemCount ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetNthRecord()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetNthRecord( dictionaryRef: DCMDictionaryRef; keyFieldTag: DCMFieldTag; serialNum: ItemCount; maxKeySize: ByteCount; var keySize: ByteCount; keyData: LogicalAddress; var uniqueID: DCMUniqueID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetNextRecord()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetNextRecord( dictionaryRef: DCMDictionaryRef; keyFieldTag: DCMFieldTag; keySize: ByteCount; keyData: ConstLogicalAddress; uniqueID: DCMUniqueID; maxKeySize: ByteCount; var nextKeySize: ByteCount; nextKeyData: LogicalAddress; var nextUniqueID: DCMUniqueID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetPrevRecord()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetPrevRecord( dictionaryRef: DCMDictionaryRef; keyFieldTag: DCMFieldTag; keySize: ByteCount; keyData: ConstLogicalAddress; uniqueID: DCMUniqueID; maxKeySize: ByteCount; var prevKeySize: ByteCount; prevKeyData: LogicalAddress; var prevUniqueID: DCMUniqueID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Get field data
}
{
 *  DCMGetFieldData()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetFieldData( dictionaryRef: DCMDictionaryRef; keyFieldTag: DCMFieldTag; keySize: ByteCount; keyData: ConstLogicalAddress; uniqueID: DCMUniqueID; numOfData: ItemCount; {const} dataTag: {variable-size-array} DCMFieldTagPtr; var dataList: AEDesc ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMSetFieldData()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMSetFieldData( dictionaryRef: DCMDictionaryRef; keyFieldTag: DCMFieldTag; keySize: ByteCount; keyData: ConstLogicalAddress; uniqueID: DCMUniqueID; const var dataList: AEDesc ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Add record
}
{
 *  DCMAddRecord()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMAddRecord( dictionaryRef: DCMDictionaryRef; keyFieldTag: DCMFieldTag; keySize: ByteCount; keyData: ConstLogicalAddress; checkOnly: Boolean; const var dataList: AEDesc; var newUniqueID: DCMUniqueID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMDeleteRecord()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMDeleteRecord( dictionaryRef: DCMDictionaryRef; keyFieldTag: DCMFieldTag; keySize: ByteCount; keyData: ConstLogicalAddress; uniqueID: DCMUniqueID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Reorganize/compact dictionary
}
{
 *  DCMReorganizeDictionary()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMReorganizeDictionary( dictionaryID: DCMDictionaryID; extraCapacity: ItemCount; progressProc: DCMProgressFilterUPP; userData: UInt32 ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMCompactDictionary()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMCompactDictionary( dictionaryID: DCMDictionaryID; progressProc: DCMProgressFilterUPP; userData: UInt32 ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    DictionaryID utilities
}
{
 *  DCMGetFileFromDictionaryID()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetFileFromDictionaryID( dictionaryID: DCMDictionaryID; var fileRef: FSSpec ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetDictionaryIDFromFile()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetDictionaryIDFromFile( const var fileRef: FSSpec; var dictionaryID: DCMDictionaryID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetDictionaryIDFromRef()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetDictionaryIDFromRef( dictionaryRef: DCMDictionaryRef ): DCMDictionaryID;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Field information and manipulation
}
{
 *  DCMGetDictionaryFieldInfo()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetDictionaryFieldInfo( dictionaryID: DCMDictionaryID; fieldTag: DCMFieldTag; var fieldInfoRecord: AEDesc ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Dictionary property
}
{
 *  DCMGetDictionaryProperty()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetDictionaryProperty( dictionaryID: DCMDictionaryID; propertyTag: DCMFieldTag; maxPropertySize: ByteCount; var actualSize: ByteCount; propertyValue: LogicalAddress ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMSetDictionaryProperty()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMSetDictionaryProperty( dictionaryID: DCMDictionaryID; propertyTag: DCMFieldTag; propertySize: ByteCount; propertyValue: ConstLogicalAddress ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetDictionaryPropertyList()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetDictionaryPropertyList( dictionaryID: DCMDictionaryID; maxPropertyNum: ItemCount; var numProperties: ItemCount; propertyTag: {variable-size-array} DCMFieldTagPtr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Seaarch dictionary
}
{
 *  DCMCreateDictionaryIterator()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMCreateDictionaryIterator( var dictionaryIterator: DCMDictionaryIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Search AccessMethod
}
{
 *  DCMCreateAccessMethodIterator()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMCreateAccessMethodIterator( var accessMethodIterator: DCMAccessMethodIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Iterator Operation
}
{
 *  DCMCountObjectIterator()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMCountObjectIterator( iterator: DCMObjectIterator ): ItemCount;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMIterateObject()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMIterateObject( iterator: DCMObjectIterator; var objectID: DCMObjectID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMResetObjectIterator()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMResetObjectIterator( iterator: DCMObjectIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMDisposeObjectIterator()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMDisposeObjectIterator( iterator: DCMObjectIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Get AccessMethod information
}
{
 *  DCMGetAccessMethodIDFromName()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetAccessMethodIDFromName( const var accessMethodName: Str63; var accessMethodID: DCMAccessMethodID ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
    Field Info Record routines
}
{
 *  DCMCreateFieldInfoRecord()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMCreateFieldInfoRecord( fieldTag: DescType; fieldType: DescType; maxRecordSize: ByteCount; fieldAttributes: DCMFieldAttributes; var fieldDefaultData: AEDesc; numberOfFindMethods: ItemCount; findMethods: {variable-size-array} DCMFindMethodPtr; var fieldInfoRecord: AEDesc ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetFieldTagAndType()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetFieldTagAndType( const var fieldInfoRecord: AEDesc; var fieldTag: DCMFieldTag; var fieldType: DCMFieldType ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetFieldMaxRecordSize()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetFieldMaxRecordSize( const var fieldInfoRecord: AEDesc; var maxRecordSize: ByteCount ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetFieldAttributes()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetFieldAttributes( const var fieldInfoRecord: AEDesc; var attributes: DCMFieldAttributes ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetFieldDefaultData()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetFieldDefaultData( const var fieldInfoRecord: AEDesc; desiredType: DescType; var fieldDefaultData: AEDesc ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  DCMGetFieldFindMethods()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    The Dictionary Manager is deprecated.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DictionaryMgrLib 1.0 and later
 }
function DCMGetFieldFindMethods( const var fieldInfoRecord: AEDesc; findMethodsArrayMaxSize: ItemCount; findMethods: {variable-size-array} DCMFindMethodPtr; var actualNumberOfFindMethods: ItemCount ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}

{$endc} {not TARGET_CPU_64}

{
    Check Dictionary Manager availability
}
{$ifc TARGET_RT_MAC_CFM}
{
        DCMDictionaryManagerAvailable() is a macro available only in C/C++.  
        To get the same functionality from pascal or assembly, you need
        to test if Dictionary Manager functions are not NULL.
        For instance:
        
            IF @DCMLibraryVersion <> kUnresolvedCFragSymbolAddress THEN
                gDictionaryManagerAvailable = TRUE;
            ELSE
                gDictionaryManagerAvailable = FALSE;
            end
    
}
{$elsec}
  {$ifc TARGET_RT_MAC_MACHO}
{ Dictionary Manager is always available on OS X }
  {$endc}
{$endc}

{
=============================================================================================
    Definitions for Japanese Analysis Module
=============================================================================================
}
{
    Default dictionary access method for Japanese analysis
}
const
	kAppleJapaneseDefaultAccessMethodName = 'DAM:Apple Backward Trie Access Method';
{
    Data length limitations of Apple Japanese dictionaries
}

const
	kMaxYomiLengthInAppleJapaneseDictionary = 40;
	kMaxKanjiLengthInAppleJapaneseDictionary = 64;

{
    Defined field tags of Apple Japanese dictionary
}
const
	kDCMJapaneseYomiTag = FOUR_CHAR_CODE('yomi');
	kDCMJapaneseHyokiTag = FOUR_CHAR_CODE('hyok');
	kDCMJapaneseHinshiTag = FOUR_CHAR_CODE('hins');
	kDCMJapaneseWeightTag = FOUR_CHAR_CODE('hind');
	kDCMJapanesePhoneticTag = FOUR_CHAR_CODE('hton');
	kDCMJapaneseAccentTag = FOUR_CHAR_CODE('acnt');
	kDCMJapaneseOnKunReadingTag = FOUR_CHAR_CODE('OnKn');
	kDCMJapaneseFukugouInfoTag = FOUR_CHAR_CODE('fuku');

const
	kDCMJapaneseYomiType = typeUnicodeText;
	kDCMJapaneseHyokiType = typeUnicodeText;
	kDCMJapaneseHinshiType = FOUR_CHAR_CODE('hins');
	kDCMJapaneseWeightType = typeSInt16;
	kDCMJapanesePhoneticType = typeUnicodeText;
	kDCMJapaneseAccentType = FOUR_CHAR_CODE('byte');
	kDCMJapaneseOnKunReadingType = typeUnicodeText;
	kDCMJapaneseFukugouInfoType = FOUR_CHAR_CODE('fuku');


{$endc} {TARGET_OS_MAC}


end.
