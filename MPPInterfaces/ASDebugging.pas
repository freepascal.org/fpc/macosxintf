{
     File:       OpenScripting/ASDebugging.h
 
     Contains:   AppleScript Debugging Interfaces.
 
     Version:    OSA-148~28
 
     Copyright:  � 1992-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}

{  Pascal Translation Updated: Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ASDebugging;
interface
uses MacTypes,AEDataModel,OSA,Files,Components,AppleEvents,AppleScript,CFBase,CFData,CFURL;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{*************************************************************************
    Mode Flags
*************************************************************************}
{    This mode flag can be passed to OSASetProperty or OSASetHandler
    and will prevent properties or handlers from being defined in a context
    that doesn't already have bindings for them. An error is returned if
    a current binding doesn't already exist. 
}
const
	kOSAModeDontDefine = $0001;

{*************************************************************************
    Component Selectors
*************************************************************************}
const
	kASSelectSetPropertyObsolete = $1101;
	kASSelectGetPropertyObsolete = $1102;
	kASSelectSetHandlerObsolete = $1103;
	kASSelectGetHandlerObsolete = $1104;
	kASSelectGetAppTerminologyObsolete = $1105;
	kASSelectSetProperty = $1106;
	kASSelectGetProperty = $1107;
	kASSelectSetHandler = $1108;
	kASSelectGetHandler = $1109;
	kASSelectGetAppTerminology = $110A;
	kASSelectGetSysTerminology = $110B;
	kASSelectGetPropertyNames = $110C;
	kASSelectGetHandlerNames = $110D;

{*************************************************************************
    Context Accessors
*************************************************************************}
{
 *  OSASetProperty()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSASetProperty( scriptingComponent: ComponentInstance; modeFlags: SInt32; contextID: OSAID; const var variableName: AEDesc; scriptValueID: OSAID ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  OSAGetProperty()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetProperty( scriptingComponent: ComponentInstance; modeFlags: SInt32; contextID: OSAID; const var variableName: AEDesc; var resultingScriptValueID: OSAID ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  OSAGetPropertyNames()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetPropertyNames( scriptingComponent: ComponentInstance; modeFlags: SInt32; contextID: OSAID; var resultingPropertyNames: AEDescList ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  OSASetHandler()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSASetHandler( scriptingComponent: ComponentInstance; modeFlags: SInt32; contextID: OSAID; const var handlerName: AEDesc; compiledScriptID: OSAID ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  OSAGetHandler()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetHandler( scriptingComponent: ComponentInstance; modeFlags: SInt32; contextID: OSAID; const var handlerName: AEDesc; var resultingCompiledScriptID: OSAID ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  OSAGetHandlerNames()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetHandlerNames( scriptingComponent: ComponentInstance; modeFlags: SInt32; contextID: OSAID; var resultingHandlerNames: AEDescList ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


//#if !__LP64__
{$ifc not TARGET_CPU_64}
{
 *  OSAGetAppTerminology()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use OSACopyScriptingDefinition instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetAppTerminology( scriptingComponent: ComponentInstance; modeFlags: SInt32; var fileSpec: FSSpec; terminologyID: SInt16; var didLaunch: Boolean; var terminologyList: AEDesc ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} { TARGET_CPU_64 }

{
 *  OSAGetSysTerminology()
 *  
 *  Discussion:
 *    A terminology ID is derived from script code and language code as
 *    follows: terminologyID = ((scriptCode & 0x7F) << 8) | (langCode &
 *    0xFF)
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetSysTerminology( scriptingComponent: ComponentInstance; modeFlags: SInt32; terminologyID: SInt16; var terminologyList: AEDesc ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  OSACopyScriptingDefinition()
 *  
 *  Discussion:
 *    Gets the scripting definition of the specified bundle.  See
 *    sdef(5) for details of the sdef format.
 *  
 *  Parameters:
 *    
 *    ref:
 *      The file (or bundle) to look in.
 *    
 *    modeFlags:
 *      There are no flags defined at this time; pass 0.
 *    
 *    sdef:
 *      The resulting sdef as XML data.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in Carbon.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function OSACopyScriptingDefinition( const var ref: FSRef; modeFlags: SInt32; var sdef: CFDataRef ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  OSACopyScriptingDefinitionFromURL()
 *  
 *  Discussion:
 *    Gets the scripting definition of the specified URL.  See sdef(5)
 *    for details of the sdef format.  If used with a file: URL, this
 *    call is equivalent to OSACopyScriptingDefinition.
 *  
 *  Parameters:
 *    
 *    url:
 *      The URL to look in; this should be a file: or eppc: URL.
 *    
 *    modeFlags:
 *      There are no flags defined at this time; pass 0.
 *    
 *    sdef:
 *      The resulting sdef as XML data.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function OSACopyScriptingDefinitionFromURL( url: CFURLRef; modeFlags: SInt32; var sdef: CFDataRef ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{*************************************************************************
    Obsolete versions provided for backward compatibility:
}
//#if !__LP64__
{$ifc not TARGET_CPU_64}
{
 *  ASSetProperty()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function ASSetProperty( scriptingComponent: ComponentInstance; contextID: OSAID; const var variableName: AEDesc; scriptValueID: OSAID ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ASGetProperty()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function ASGetProperty( scriptingComponent: ComponentInstance; contextID: OSAID; const var variableName: AEDesc; var resultingScriptValueID: OSAID ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ASSetHandler()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function ASSetHandler( scriptingComponent: ComponentInstance; contextID: OSAID; const var handlerName: AEDesc; compiledScriptID: OSAID ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ASGetHandler()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function ASGetHandler( scriptingComponent: ComponentInstance; contextID: OSAID; const var handlerName: AEDesc; var resultingCompiledScriptID: OSAID ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ASGetAppTerminology()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function ASGetAppTerminology( scriptingComponent: ComponentInstance; var fileSpec: FSSpec; terminologID: SInt16; var didLaunch: Boolean; var terminologyList: AEDesc ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Errors:
        errOSASystemError       operation failed
    }
{************************************************************************}


{$endc}	{ TARGET_CPU_64 }

{$endc} {TARGET_OS_MAC}

end.
