{
     File:       CommonPanels/CMCalibrator.h
 
     Contains:   ColorSync Calibration API
 
     Version:    CommonPanels-94~602
 
     Copyright:  � 1998-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit CMCalibrator;
interface
uses MacTypes,ColorSyncDeprecated,Events;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}


type
	CalibrateEventProcPtr = procedure( var event: EventRecord );
{GPC-ONLY-START}
	CalibrateEventUPP = UniversalProcPtr; // should be CalibrateEventProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	CalibrateEventUPP = CalibrateEventProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	CalibrateEventUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}


{ Interface for new ColorSync monitor calibrators (ColorSync 2.6 and greater) }

const
	kCalibratorNamePrefix = FOUR_CHAR_CODE('cali');

type
	CalibratorInfoPtr = ^CalibratorInfo;
	CalibratorInfo = record
		dataSize: UInt32;               { Size of this structure - compatibility }
		displayID: CMDisplayIDType;              { Contains an hDC on Win32 }
		profileLocationSize: UInt32;    { Max size for returned profile location }
		profileLocationPtr: CMProfileLocationPtr;     { For returning the profile }
		eventProc: CalibrateEventUPP;              { Ignored on Win32 }
		isGood: Boolean;                 { true or false }
	end;
type
	CanCalibrateProcPtr = function( displayID: CMDisplayIDType; var errMessage: Str255 ): Boolean;
	CalibrateProcPtr = function( var theInfo: CalibratorInfo ): OSErr;
{GPC-ONLY-START}
	CanCalibrateUPP = UniversalProcPtr; // should be CanCalibrateProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	CanCalibrateUPP = CanCalibrateProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	CanCalibrateUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	CalibrateUPP = UniversalProcPtr; // should be CalibrateProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	CalibrateUPP = CalibrateProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	CalibrateUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{
 *  NewCalibrateEventUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewCalibrateEventUPP( userRoutine: CalibrateEventProcPtr ): CalibrateEventUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewCanCalibrateUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewCanCalibrateUPP( userRoutine: CanCalibrateProcPtr ): CanCalibrateUPP;

{
 *  NewCalibrateUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewCalibrateUPP( userRoutine: CalibrateProcPtr ): CalibrateUPP;

{
 *  DisposeCalibrateEventUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeCalibrateEventUPP( userUPP: CalibrateEventUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeCanCalibrateUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeCanCalibrateUPP( userUPP: CanCalibrateUPP );

{
 *  DisposeCalibrateUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeCalibrateUPP( userUPP: CalibrateUPP );

{
 *  InvokeCalibrateEventUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeCalibrateEventUPP( var event: EventRecord; userUPP: CalibrateEventUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeCanCalibrateUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeCanCalibrateUPP( displayID: CMDisplayIDType; var errMessage: Str255; userUPP: CanCalibrateUPP ): Boolean;

{
 *  InvokeCalibrateUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeCalibrateUPP( var theInfo: CalibratorInfo; userUPP: CalibrateUPP ): OSErr;

{
 *  CMCalibrateDisplay()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CMCalibrateDisplay( var theInfo: CalibratorInfo ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {TARGET_OS_MAC}


end.
