{
//  ABActionsC.h
//  AddressBook Framework
//
//  Copyright (c) 2003-2007 Apple Inc.  All rights reserved.
//
//
}
{	  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, 2004 }
{	  Pascal Translation Updated:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, November 2009 }
{     Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2018 }

unit ABActions;
interface
uses MacTypes,ABAddressBook,CFBase;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

// --------------------------------------------------------------------------------
//      Action Support
// --------------------------------------------------------------------------------
// This API allows developers to populate Contacts.app's roll-over menus with custom
// entries. Your CFBundle must implement a function named ABActionRegisterCallbacks which
// will return a pointer to an ABActionCallbacks struct. This struct should be filled out
// as follows:
//
// version: The version of this structure is 0.
//
// proprty: A pointer to a function that returns the AddressBook property this action applies
// to. Only items with labels may have actions at this time. (emails, phones, birthdays, etc)
//
// title: A pointer to a function which returns a copy of the title to be displayed. This function
// takes two parameters, the selected person and item identifier. The item identifier will be NULL
// for single value properties. AddressBook will release this string when it's done with it.
//
// enabled: A pointer to a function which returns YES if the action should be enabled for the
// passed ABPersonRef and item identifier. The item identifier will be NULL for single value
// properties. This field may be NULL. Actions with NULL enabled callbacks will always be enabled.
//
// selected. A pointer to a function which will be called when the user selects this action.
// It's passed an ABPersonRef and item identifier. The item identifier will be NULL for single
// value properties.
//
// Action plugins are stored in ~/Library/Address Book Plug-Ins or /Library/Address Book Plug-Ins
//
// There can be only 1 Action plugin per bundle.

type
	ABActionGetPropertyCallback = function: CFStringRef;
	ABActionCopyTitleCallback = function( person: ABPersonRef; identifier: CFStringRef ): CFStringRef;
	ABActionEnabledCallback = function( person: ABPersonRef; identifier: CFStringRef ): Boolean;
	ABActionSelectedCallback = procedure( person: ABPersonRef; identifier: CFStringRef );

type
	ABActionCallbacks = record
		version: CFIndex;
		proprty: ABActionGetPropertyCallback;
		title: ABActionCopyTitleCallback;
		enabled: ABActionEnabledCallback;
		selected: ABActionSelectedCallback;
	end;

// Your CFBundle MUST include a function named ABActionRegisterCallbacks which returns a pointer
// to a filled out ABActionCallbacks struct:
//
// ABActionCallbacks* ABActionRegisterCallbacks(void);

{$endc} {TARGET_OS_MAC}

end.
