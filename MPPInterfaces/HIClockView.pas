{
     File:       HIToolbox/HIClockView.h
 
     Contains:   Definition of the clock view provided by HIToolbox.
 
     Version:    HIToolbox-624~3
 
     Copyright:  © 2006-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HIClockView;
interface
uses MacTypes,Appearance,CarbonEvents,Controls,QuickdrawTypes,HIObject;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
 *  HIClockView.h
 *  
 *  Discussion:
 *    API definitions for the clock view.
 }
{==============================================================================}
{  Clock view events                                                           }
{==============================================================================}
const
	kEventClassClockView = FOUR_CHAR_CODE('cloc');

{
 *  kEventClassClockView / kEventClockDateOrTimeChanged
 *  
 *  Summary:
 *    Allows clients to determine when the user has changed the date or
 *    time in the clock view.
 *  
 *  Discussion:
 *    This event is sent by the clock view when the user has changed
 *    the date or time. Clients could register for this notification in
 *    order to update some state based on the date or time in the
 *    clock, for instance. This event is sent to the view only, it will
 *    not propagate. It is sent to all handlers installed on the view.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    --> kEventParamDirectObject (in, typeControlRef)
 *          The view whose date has changed.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in Carbon.framework
 *    CarbonLib:        not available
 }
const
	kEventClockDateOrTimeChanged = 1;

{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ CLOCK (CDEF 15)                                                                   }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  NOTE:   You can specify more options in the Value paramter when creating the clock. }
{          See below.                                                                  }
{  NOTE:   Under Appearance 1.1, the clock control knows and returns more part codes.  }
{          The new clock-specific part codes are defined with the other control parts. }
{          Besides these clock-specific parts, we also return kControlUpButtonPart     }
{          and kControlDownButtonPart when they hit the up and down arrows.            }
{          The new part codes give you more flexibility for focusing and hit testing.  }
{          The original kControlClockPart is still valid. When hit testing, it means   }
{          that some non-editable area of the clock's whitespace has been clicked.     }
{          When focusing a currently unfocused clock, it changes the focus to the      }
{          first part; it is the same as passing kControlFocusNextPart. When           }
{          re-focusing a focused clock, it will not change the focus at all.           }
{ Clock proc IDs }
const
	kControlClockTimeProc = 240;
	kControlClockTimeSecondsProc = 241;
	kControlClockDateProc = 242;
	kControlClockMonthYearProc = 243;

{ Clock Types }
type
	ControlClockType = UInt16;
const
	kControlClockTypeHourMinute = 0;
	kControlClockTypeHourMinuteSecond = 1;
	kControlClockTypeMonthDayYear = 2;
	kControlClockTypeMonthYear = 3;

{ Clock Flags }
{  These flags can be passed into 'value' field on creation of the control.            }
{  Value is set to 0 after control is created.                                         }
type
	ControlClockFlags = UInt32;
const
	kControlClockFlagStandard = 0;    { editable, non-live}
	kControlClockNoFlags = 0;
	kControlClockFlagDisplayOnly = 1;    { add this to become non-editable}
	kControlClockIsDisplayOnly = 1;
	kControlClockFlagLive = 2;    { automatically shows current time on idle. only valid with display only.}
	kControlClockIsLive = 2;

{ Control Kind Tag }
const
	kControlKindClock = FOUR_CHAR_CODE('clck');

{ The HIObject class ID for the HIClock class. }
const kHIClockViewClassID = CFSTR( 'com.apple.HIClock' );
{ Creation API: Carbon only }
{$ifc not TARGET_CPU_64}
{
 *  CreateClockControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateClockControl( window: WindowRef; const var boundsRect: Rect; clockType: ControlClockType; clockFlags: ControlClockFlags; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Tagged data supported by clocks }
{$endc} {not TARGET_CPU_64}

const
	kControlClockLongDateTag = FOUR_CHAR_CODE('date'); { LongDateRec, 32-bit only}
	kControlClockAbsoluteTimeTag = FOUR_CHAR_CODE('abst'); { CFAbsoluteTime; available in Leopard and later}
	kControlClockFontStyleTag = kControlFontStyleTag; { ControlFontStyleRec}
	kControlClockAnimatingTag = FOUR_CHAR_CODE('anim'); { Boolean}

{$endc} {TARGET_OS_MAC}

end.
