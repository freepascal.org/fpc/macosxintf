{
     File:       OSServices/IconStorage.h

     Contains:   Services to load and share icon family data.

     Copyright:  (c) 2000-2011 Apple Inc. All rights reserved.

     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:

                     http://developer.apple.com/bugreporter/

}
unit IconStorage;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{ virtual ARGB icon types. Each type will be split into separate 24 bit RGB data and 8 bit mask for storage in icns container }
{ the ARGB bitmap must be non-premultiplied }
const
	kIconServices16PixelDataARGB = FOUR_CHAR_CODE('ic04'); { uses kSmall32BitIconIndex and kSmallDeepMaskIconIndex}
	kIconServices32PixelDataARGB = FOUR_CHAR_CODE('ic05'); { uses kLarge32BitIconIndex and kLargeDeepMaskIconIndex}
	kIconServices48PixelDataARGB = FOUR_CHAR_CODE('ic06'); { uses kHuge32BitIconIndex and kHugeDeepMaskIconIndex}
	kIconServices128PixelDataARGB = FOUR_CHAR_CODE('ic07'); { uses kThumbnailDataIndex and kThumbnailMaskIndex}

{ The following icon types can only be used as an icon element }
{ inside a 'icns' icon family }
const
	kIconServices256PixelDataARGB = FOUR_CHAR_CODE('ic08'); { non-premultiplied 256x256 ARGB bitmap}
	kIconServices512PixelDataARGB = FOUR_CHAR_CODE('ic09'); { non-premultiplied 512x512 ARGB bitmap}
	kIconServices1024PixelDataARGB = FOUR_CHAR_CODE('ic10'); { non-premultiplied 1024x1024 ARGB bitmap}
	kThumbnail32BitData = FOUR_CHAR_CODE('it32');
	kThumbnail8BitMask = FOUR_CHAR_CODE('t8mk');

const
	kHuge1BitMask = FOUR_CHAR_CODE('ich#');
	kHuge4BitData = FOUR_CHAR_CODE('ich4');
	kHuge8BitData = FOUR_CHAR_CODE('ich8');
	kHuge32BitData = FOUR_CHAR_CODE('ih32');
	kHuge8BitMask = FOUR_CHAR_CODE('h8mk');

{ The following icon types can be used as a resource type }
{ or as an icon element type inside a 'icns' icon family }
const
	kLarge1BitMask = FOUR_CHAR_CODE('ICN#');
	kLarge4BitData = FOUR_CHAR_CODE('icl4');
	kLarge8BitData = FOUR_CHAR_CODE('icl8');
	kLarge32BitData = FOUR_CHAR_CODE('il32');
	kLarge8BitMask = FOUR_CHAR_CODE('l8mk');
	kSmall1BitMask = FOUR_CHAR_CODE('ics#');
	kSmall4BitData = FOUR_CHAR_CODE('ics4');
	kSmall8BitData = FOUR_CHAR_CODE('ics8');
	kSmall32BitData = FOUR_CHAR_CODE('is32');
	kSmall8BitMask = FOUR_CHAR_CODE('s8mk');
	kMini1BitMask = FOUR_CHAR_CODE('icm#');
	kMini4BitData = FOUR_CHAR_CODE('icm4');
	kMini8BitData = FOUR_CHAR_CODE('icm8');

{ Obsolete. Use names defined above. }
const
	large1BitMask = kLarge1BitMask;
	large4BitData = kLarge4BitData;
	large8BitData = kLarge8BitData;
	small1BitMask = kSmall1BitMask;
	small4BitData = kSmall4BitData;
	small8BitData = kSmall8BitData;
	mini1BitMask = kMini1BitMask;
	mini4BitData = kMini4BitData;
	mini8BitData = kMini8BitData;

{
    IconFamily 'icns' resources contain an entire IconFamily (all sizes and depths).
   For custom icons, icns IconFamily resources of the custom icon resource ID are fetched first before
   the classic custom icons (individual 'ics#, ICN#, etc) are fetched.  If the fetch of the icns resource
   succeeds then the icns is looked at exclusively for the icon data.
   For custom icons, new icon features such as 32-bit deep icons are only fetched from the icns resource.
   This is to avoid incompatibilities with cut & paste of new style icons with an older version of the
   MacOS Finder.
   DriverGestalt is called with code kdgMediaIconSuite by IconServices after calling FSM to determine a
   driver icon for a particular device.  The result of the kdgMediaIconSuite call to DriverGestalt should
   be a pointer an an IconFamily.  In this manner driver vendors can provide rich, detailed drive icons
   instead of the 1-bit variety previously supported.
   The IconFamilyElement and IconFamilyResource data types (which also include the data types
   IconFamilyPtr and IconFamilyHandle) are always big-endian.
}

const
	kIconFamilyType = FOUR_CHAR_CODE('icns');


type
	IconFamilyElement = record
		elementType: OSType;            { 'ICN#', 'icl8', etc...}
		elementSize: SInt32;            { Size of this element}
		elementData : packed array [0..0] of UInt8;
	end;
	IconFamilyElementPtr = ^IconFamilyElement;
type
	IconFamilyResource = record
		resourceType: OSType;           { Always 'icns'}
		resourceSize: SInt32;           { Total size of this resource}
		elements: array[0..0] of IconFamilyElement;
	end;
	IconFamilyResourcePtr = ^IconFamilyResource;
type
	IconFamilyPtr = IconFamilyResourcePtr;
	IconFamilyPtrPtr = ^IconFamilyPtr;
	IconFamilyHandle = IconFamilyPtrPtr;
{  Icon Variants }
{ These can be used as an element of an 'icns' icon family }
{ or as a parameter to GetIconRefVariant }
const
	kTileIconVariant = FOUR_CHAR_CODE('tile');
	kRolloverIconVariant = FOUR_CHAR_CODE('over');
	kDropIconVariant = FOUR_CHAR_CODE('drop');
	kOpenIconVariant = FOUR_CHAR_CODE('open');
	kOpenDropIconVariant = FOUR_CHAR_CODE('odrp');


{$endc} {TARGET_OS_MAC}


end.
