{
	Copyright:	(c) 1999-2008 Apple Inc. All rights reserved.
}
{       Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }

unit CGLDevice;
interface
uses MacTypes,CGLTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

type
	CGLShareGroupRec = record end;
	CGLShareGroup = ^CGLShareGroupRec;
	CGLShareGroupObj = ^CGLShareGroupRec;
	
function CGLGetShareGroup( ctx: CGLContextObj ): CGLShareGroupObj;

{$endc} {TARGET_OS_MAC}

end.
