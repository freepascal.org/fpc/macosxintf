{
     File:       IconsCore.h
 
     Contains:   Icon Utilities and Icon Services Interfaces.
 
     Copyright:  (c) 2003-2012 by Apple Inc. All rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/

}
unit IconsCore;
interface
uses MacTypes,CFBase,CFURL,IconStorage,Components,Files;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{ The following are icons for which there are both icon suites and SICNs. }
{ Avoid using icon resources if possible. Use IconServices instead. }
const
	kGenericDocumentIconResource = -4000;
	kGenericStationeryIconResource = -3985;
	kGenericEditionFileIconResource = -3989;
	kGenericApplicationIconResource = -3996;
	kGenericDeskAccessoryIconResource = -3991;
	kGenericFolderIconResource = -3999;
	kPrivateFolderIconResource = -3994;
	kFloppyIconResource = -3998;
	kTrashIconResource = -3993;
	kGenericRAMDiskIconResource = -3988;
	kGenericCDROMIconResource = -3987;

{ The following are icons for which there are SICNs only. }
{ Avoid using icon resources if possible. Use IconServices instead. }
const
	kDesktopIconResource = -3992;
	kOpenFolderIconResource = -3997;
	kGenericHardDiskIconResource = -3995;
	kGenericFileServerIconResource = -3972;
	kGenericSuitcaseIconResource = -3970;
	kGenericMoverObjectIconResource = -3969;

{ The following are icons for which there are icon suites only. }
{ Avoid using icon resources if possible. Use IconServices instead. }
const
	kGenericPreferencesIconResource = -3971;
	kGenericQueryDocumentIconResource = -16506;
	kGenericExtensionIconResource = -16415;
	kSystemFolderIconResource = -3983;
	kHelpIconResource = -20271;
	kAppleMenuFolderIconResource = -3982;

{ Obsolete. Use named constants defined above. }
const
	genericDocumentIconResource = kGenericDocumentIconResource;
	genericStationeryIconResource = kGenericStationeryIconResource;
	genericEditionFileIconResource = kGenericEditionFileIconResource;
	genericApplicationIconResource = kGenericApplicationIconResource;
	genericDeskAccessoryIconResource = kGenericDeskAccessoryIconResource;
	genericFolderIconResource = kGenericFolderIconResource;
	privateFolderIconResource = kPrivateFolderIconResource;
	floppyIconResource = kFloppyIconResource;
	trashIconResource = kTrashIconResource;
	genericRAMDiskIconResource = kGenericRAMDiskIconResource;
	genericCDROMIconResource = kGenericCDROMIconResource;
	desktopIconResource = kDesktopIconResource;
	openFolderIconResource = kOpenFolderIconResource;
	genericHardDiskIconResource = kGenericHardDiskIconResource;
	genericFileServerIconResource = kGenericFileServerIconResource;
	genericSuitcaseIconResource = kGenericSuitcaseIconResource;
	genericMoverObjectIconResource = kGenericMoverObjectIconResource;
	genericPreferencesIconResource = kGenericPreferencesIconResource;
	genericQueryDocumentIconResource = kGenericQueryDocumentIconResource;
	genericExtensionIconResource = kGenericExtensionIconResource;
	systemFolderIconResource = kSystemFolderIconResource;
	appleMenuFolderIconResource = kAppleMenuFolderIconResource;

{ Avoid using icon resources if possible. Use IconServices instead. }
const
	kStartupFolderIconResource = -3981;
	kOwnedFolderIconResource = -3980;
	kDropFolderIconResource = -3979;
	kSharedFolderIconResource = -3978;
	kMountedFolderIconResource = -3977;
	kControlPanelFolderIconResource = -3976;
	kPrintMonitorFolderIconResource = -3975;
	kPreferencesFolderIconResource = -3974;
	kExtensionsFolderIconResource = -3973;
	kFontsFolderIconResource = -3968;
	kFullTrashIconResource = -3984;

{ Obsolete. Use named constants defined above. }
const
	startupFolderIconResource = kStartupFolderIconResource;
	ownedFolderIconResource = kOwnedFolderIconResource;
	dropFolderIconResource = kDropFolderIconResource;
	sharedFolderIconResource = kSharedFolderIconResource;
	mountedFolderIconResource = kMountedFolderIconResource;
	controlPanelFolderIconResource = kControlPanelFolderIconResource;
	printMonitorFolderIconResource = kPrintMonitorFolderIconResource;
	preferencesFolderIconResource = kPreferencesFolderIconResource;
	extensionsFolderIconResource = kExtensionsFolderIconResource;
	fontsFolderIconResource = kFontsFolderIconResource;
	fullTrashIconResource = kFullTrashIconResource;

{ IconRefs identify cached icon data. IconRef 0 is invalid.}
type
	IconRef = ^OpaqueIconRef; { an opaque type }
	OpaqueIconRef = record end;
	IconRef_fix = IconRef;	{ used as a type identifiers in records containing iconRef field }
	IconRefPtr = ^IconRef;
{
   IconServices is an efficient mechanism to share icon data amongst multiple 
   clients. It avoids duplication of data; it provides efficient caching, 
   releasing memory when the icon data is no longer needed; it can provide
   the appropriate icon for any filesystem object; it can provide commonly 
   used icons (caution, note, help...); it is Appearance-savvy: the icons
   are switched when appropriate.
   IconServices refer to cached icon data using IconRef, a 32-bit opaque
   value. IconRefs are reference counted. When there are no more "owners" 
   of an IconRef, the memory used by the icon bitmap is disposed of.
   Two files of same type and creator with no custom icon will have the same IconRef.
   Files with custom icons will have their own IconRef.
}
{
   Use the special creator kSystemIconsCreator to get "standard" icons 
   that are not associated with a file, such as the help icon.
   Note that all lowercase creators are reserved by Apple.
}
const
	kSystemIconsCreator = FOUR_CHAR_CODE('macs');


{
   Type of the predefined/generic icons. For example, the call:
      err = GetIconRef(kOnSystemDisk, kSystemIconsCreator, kHelpIcon, &iconRef);
   will retun in iconRef the IconRef for the standard help icon.
}

{ Generic Finder icons }
const
	kClipboardIcon = FOUR_CHAR_CODE('CLIP');
	kClippingUnknownTypeIcon = FOUR_CHAR_CODE('clpu');
	kClippingPictureTypeIcon = FOUR_CHAR_CODE('clpp');
	kClippingTextTypeIcon = FOUR_CHAR_CODE('clpt');
	kClippingSoundTypeIcon = FOUR_CHAR_CODE('clps');
	kDesktopIcon = FOUR_CHAR_CODE('desk');
	kFinderIcon = FOUR_CHAR_CODE('FNDR');
	kComputerIcon = FOUR_CHAR_CODE('root');
	kFontSuitcaseIcon = FOUR_CHAR_CODE('FFIL');
	kFullTrashIcon = FOUR_CHAR_CODE('ftrh');
	kGenericApplicationIcon = FOUR_CHAR_CODE('APPL');
	kGenericCDROMIcon = FOUR_CHAR_CODE('cddr');
	kGenericControlPanelIcon = FOUR_CHAR_CODE('APPC');
	kGenericControlStripModuleIcon = FOUR_CHAR_CODE('sdev');
	kGenericComponentIcon = FOUR_CHAR_CODE('thng');
	kGenericDeskAccessoryIcon = FOUR_CHAR_CODE('APPD');
	kGenericDocumentIcon = FOUR_CHAR_CODE('docu');
	kGenericEditionFileIcon = FOUR_CHAR_CODE('edtf');
	kGenericExtensionIcon = FOUR_CHAR_CODE('INIT');
	kGenericFileServerIcon = FOUR_CHAR_CODE('srvr');
	kGenericFontIcon = FOUR_CHAR_CODE('ffil');
	kGenericFontScalerIcon = FOUR_CHAR_CODE('sclr');
	kGenericFloppyIcon = FOUR_CHAR_CODE('flpy');
	kGenericHardDiskIcon = FOUR_CHAR_CODE('hdsk');
	kGenericIDiskIcon = FOUR_CHAR_CODE('idsk');
	kGenericRemovableMediaIcon = FOUR_CHAR_CODE('rmov');
	kGenericMoverObjectIcon = FOUR_CHAR_CODE('movr');
	kGenericPCCardIcon = FOUR_CHAR_CODE('pcmc');
	kGenericPreferencesIcon = FOUR_CHAR_CODE('pref');
	kGenericQueryDocumentIcon = FOUR_CHAR_CODE('qery');
	kGenericRAMDiskIcon = FOUR_CHAR_CODE('ramd');
	kGenericSharedLibaryIcon = FOUR_CHAR_CODE('shlb');
	kGenericStationeryIcon = FOUR_CHAR_CODE('sdoc');
	kGenericSuitcaseIcon = FOUR_CHAR_CODE('suit');
	kGenericURLIcon = FOUR_CHAR_CODE('gurl');
	kGenericWORMIcon = FOUR_CHAR_CODE('worm');
	kInternationalResourcesIcon = FOUR_CHAR_CODE('ifil');
	kKeyboardLayoutIcon = FOUR_CHAR_CODE('kfil');
	kSoundFileIcon = FOUR_CHAR_CODE('sfil');
	kSystemSuitcaseIcon = FOUR_CHAR_CODE('zsys');
	kTrashIcon = FOUR_CHAR_CODE('trsh');
	kTrueTypeFontIcon = FOUR_CHAR_CODE('tfil');
	kTrueTypeFlatFontIcon = FOUR_CHAR_CODE('sfnt');
	kTrueTypeMultiFlatFontIcon = FOUR_CHAR_CODE('ttcf');
	kUserIDiskIcon = FOUR_CHAR_CODE('udsk');
	kUnknownFSObjectIcon = FOUR_CHAR_CODE('unfs');
	kInternationResourcesIcon = kInternationalResourcesIcon; { old name}

{ Internet locations }
const
	kInternetLocationHTTPIcon = FOUR_CHAR_CODE('ilht');
	kInternetLocationFTPIcon = FOUR_CHAR_CODE('ilft');
	kInternetLocationAppleShareIcon = FOUR_CHAR_CODE('ilaf');
	kInternetLocationAppleTalkZoneIcon = FOUR_CHAR_CODE('ilat');
	kInternetLocationFileIcon = FOUR_CHAR_CODE('ilfi');
	kInternetLocationMailIcon = FOUR_CHAR_CODE('ilma');
	kInternetLocationNewsIcon = FOUR_CHAR_CODE('ilnw');
	kInternetLocationNSLNeighborhoodIcon = FOUR_CHAR_CODE('ilns');
	kInternetLocationGenericIcon = FOUR_CHAR_CODE('ilge');

{ Folders }
const
	kGenericFolderIcon = FOUR_CHAR_CODE('fldr');
	kDropFolderIcon = FOUR_CHAR_CODE('dbox');
	kMountedFolderIcon = FOUR_CHAR_CODE('mntd');
	kOpenFolderIcon = FOUR_CHAR_CODE('ofld');
	kOwnedFolderIcon = FOUR_CHAR_CODE('ownd');
	kPrivateFolderIcon = FOUR_CHAR_CODE('prvf');
	kSharedFolderIcon = FOUR_CHAR_CODE('shfl');

{ Sharing Privileges icons }
const
	kSharingPrivsNotApplicableIcon = FOUR_CHAR_CODE('shna');
	kSharingPrivsReadOnlyIcon = FOUR_CHAR_CODE('shro');
	kSharingPrivsReadWriteIcon = FOUR_CHAR_CODE('shrw');
	kSharingPrivsUnknownIcon = FOUR_CHAR_CODE('shuk');
	kSharingPrivsWritableIcon = FOUR_CHAR_CODE('writ');


{ Users and Groups icons }
const
	kUserFolderIcon = FOUR_CHAR_CODE('ufld');
	kWorkgroupFolderIcon = FOUR_CHAR_CODE('wfld');
	kGuestUserIcon = FOUR_CHAR_CODE('gusr');
	kUserIcon = FOUR_CHAR_CODE('user');
	kOwnerIcon = FOUR_CHAR_CODE('susr');
	kGroupIcon = FOUR_CHAR_CODE('grup');

{ Special folders }
const
	kAppearanceFolderIcon = FOUR_CHAR_CODE('appr');
	kAppleExtrasFolderIcon = $616578C4; {'aex�'}
	kAppleMenuFolderIcon = FOUR_CHAR_CODE('amnu');
	kApplicationsFolderIcon = FOUR_CHAR_CODE('apps');
	kApplicationSupportFolderIcon = FOUR_CHAR_CODE('asup');
	kAssistantsFolderIcon = $617374C4; {'ast�'}
	kColorSyncFolderIcon = FOUR_CHAR_CODE('prof');
	kContextualMenuItemsFolderIcon = FOUR_CHAR_CODE('cmnu');
	kControlPanelDisabledFolderIcon = FOUR_CHAR_CODE('ctrD');
	kControlPanelFolderIcon = FOUR_CHAR_CODE('ctrl');
	kControlStripModulesFolderIcon = $736476C4; {'sdv�'}
	kDocumentsFolderIcon = FOUR_CHAR_CODE('docs');
	kExtensionsDisabledFolderIcon = FOUR_CHAR_CODE('extD');
	kExtensionsFolderIcon = FOUR_CHAR_CODE('extn');
	kFavoritesFolderIcon = FOUR_CHAR_CODE('favs');
	kFontsFolderIcon = FOUR_CHAR_CODE('font');
	kHelpFolderIcon = $C4686C70; {'�hlp' }
	kInternetFolderIcon = $696E74C4; {'int�'}
	kInternetPlugInFolderIcon = $C46E6574; {'�net' }
	kInternetSearchSitesFolderIcon = FOUR_CHAR_CODE('issf');
	kLocalesFolderIcon = $C46C6F63; {'�loc' }
	kMacOSReadMeFolderIcon = $6D6F72C4; {'mor�'}
	kPublicFolderIcon = FOUR_CHAR_CODE('pubf');
	kPreferencesFolderIcon = $707266C4; {'prf�'}
	kPrinterDescriptionFolderIcon = FOUR_CHAR_CODE('ppdf');
	kPrinterDriverFolderIcon = $C4707264; {'�prd' }
	kPrintMonitorFolderIcon = FOUR_CHAR_CODE('prnt');
	kRecentApplicationsFolderIcon = FOUR_CHAR_CODE('rapp');
	kRecentDocumentsFolderIcon = FOUR_CHAR_CODE('rdoc');
	kRecentServersFolderIcon = FOUR_CHAR_CODE('rsrv');
	kScriptingAdditionsFolderIcon = $C4736372; {'�scr' }
	kSharedLibrariesFolderIcon = $C46C6962; {'�lib' }
	kScriptsFolderIcon = $736372C4; {'scr�'}
	kShutdownItemsDisabledFolderIcon = FOUR_CHAR_CODE('shdD');
	kShutdownItemsFolderIcon = FOUR_CHAR_CODE('shdf');
	kSpeakableItemsFolder = FOUR_CHAR_CODE('spki');
	kStartupItemsDisabledFolderIcon = FOUR_CHAR_CODE('strD');
	kStartupItemsFolderIcon = FOUR_CHAR_CODE('strt');
	kSystemExtensionDisabledFolderIcon = FOUR_CHAR_CODE('macD');
	kSystemFolderIcon = FOUR_CHAR_CODE('macs');
	kTextEncodingsFolderIcon = $C4746578; {'�tex' }
	kUsersFolderIcon = $757372C4; {'usr�'}
	kUtilitiesFolderIcon = $757469C4; {'uti�'}
	kVoicesFolderIcon = FOUR_CHAR_CODE('fvoc');

{ Badges }
const
	kAppleScriptBadgeIcon = FOUR_CHAR_CODE('scrp');
	kLockedBadgeIcon = FOUR_CHAR_CODE('lbdg');
	kMountedBadgeIcon = FOUR_CHAR_CODE('mbdg');
	kSharedBadgeIcon = FOUR_CHAR_CODE('sbdg');
	kAliasBadgeIcon = FOUR_CHAR_CODE('abdg');
	kAlertCautionBadgeIcon = FOUR_CHAR_CODE('cbdg');

{ Alert icons }
const
	kAlertNoteIcon = FOUR_CHAR_CODE('note');
	kAlertCautionIcon = FOUR_CHAR_CODE('caut');
	kAlertStopIcon = FOUR_CHAR_CODE('stop');

{ Networking icons }
const
	kAppleTalkIcon = FOUR_CHAR_CODE('atlk');
	kAppleTalkZoneIcon = FOUR_CHAR_CODE('atzn');
	kAFPServerIcon = FOUR_CHAR_CODE('afps');
	kFTPServerIcon = FOUR_CHAR_CODE('ftps');
	kHTTPServerIcon = FOUR_CHAR_CODE('htps');
	kGenericNetworkIcon = FOUR_CHAR_CODE('gnet');
	kIPFileServerIcon = FOUR_CHAR_CODE('isrv');

{ Toolbar icons }
const
	kToolbarCustomizeIcon = FOUR_CHAR_CODE('tcus');
	kToolbarDeleteIcon = FOUR_CHAR_CODE('tdel');
	kToolbarFavoritesIcon = FOUR_CHAR_CODE('tfav');
	kToolbarHomeIcon = FOUR_CHAR_CODE('thom');
	kToolbarAdvancedIcon = FOUR_CHAR_CODE('tbav');
	kToolbarInfoIcon = FOUR_CHAR_CODE('tbin');
	kToolbarLabelsIcon = FOUR_CHAR_CODE('tblb');
	kToolbarApplicationsFolderIcon = FOUR_CHAR_CODE('tAps');
	kToolbarDocumentsFolderIcon = FOUR_CHAR_CODE('tDoc');
	kToolbarMovieFolderIcon = FOUR_CHAR_CODE('tMov');
	kToolbarMusicFolderIcon = FOUR_CHAR_CODE('tMus');
	kToolbarPicturesFolderIcon = FOUR_CHAR_CODE('tPic');
	kToolbarPublicFolderIcon = FOUR_CHAR_CODE('tPub');
	kToolbarDesktopFolderIcon = FOUR_CHAR_CODE('tDsk');
	kToolbarDownloadsFolderIcon = FOUR_CHAR_CODE('tDwn');
	kToolbarLibraryFolderIcon = FOUR_CHAR_CODE('tLib');
	kToolbarUtilitiesFolderIcon = FOUR_CHAR_CODE('tUtl');
	kToolbarSitesFolderIcon = FOUR_CHAR_CODE('tSts');

{ Other icons }
const
	kAppleLogoIcon = FOUR_CHAR_CODE('capl');
	kAppleMenuIcon = FOUR_CHAR_CODE('sapl');
	kBackwardArrowIcon = FOUR_CHAR_CODE('baro');
	kFavoriteItemsIcon = FOUR_CHAR_CODE('favr');
	kForwardArrowIcon = FOUR_CHAR_CODE('faro');
	kGridIcon = FOUR_CHAR_CODE('grid');
	kHelpIcon = FOUR_CHAR_CODE('help');
	kKeepArrangedIcon = FOUR_CHAR_CODE('arng');
	kLockedIcon = FOUR_CHAR_CODE('lock');
	kNoFilesIcon = FOUR_CHAR_CODE('nfil');
	kNoFolderIcon = FOUR_CHAR_CODE('nfld');
	kNoWriteIcon = FOUR_CHAR_CODE('nwrt');
	kProtectedApplicationFolderIcon = FOUR_CHAR_CODE('papp');
	kProtectedSystemFolderIcon = FOUR_CHAR_CODE('psys');
	kRecentItemsIcon = FOUR_CHAR_CODE('rcnt');
	kShortcutIcon = FOUR_CHAR_CODE('shrt');
	kSortAscendingIcon = FOUR_CHAR_CODE('asnd');
	kSortDescendingIcon = FOUR_CHAR_CODE('dsnd');
	kUnlockedIcon = FOUR_CHAR_CODE('ulck');
	kConnectToIcon = FOUR_CHAR_CODE('cnct');
	kGenericWindowIcon = FOUR_CHAR_CODE('gwin');
	kQuestionMarkIcon = FOUR_CHAR_CODE('ques');
	kDeleteAliasIcon = FOUR_CHAR_CODE('dali');
	kEjectMediaIcon = FOUR_CHAR_CODE('ejec');
	kBurningIcon = FOUR_CHAR_CODE('burn');
	kRightContainerArrowIcon = FOUR_CHAR_CODE('rcar');


{  IconServicesUsageFlags }
type
	IconServicesUsageFlags = UInt32;
const
	kIconServicesNormalUsageFlag = $00000000;
	kIconServicesNoBadgeFlag = $00000001; { available on Panther and later }
	kIconServicesUpdateIfNeededFlag = $00000002; { available on Panther and later }


{
  kIconServicesCatalogInfoMask - Minimal bitmask for use with
    GetIconRefFromFileInfo(). Use this mask with FSGetCatalogInfo
    before calling GetIconRefFromFileInfo(). Please note kFSCatInfoFinderXInfo flag is
    valid only on MacOS X and must be cleared from CatalogInfoMask before
    passing to GetIconRefFromFileInfo while running under MacOS 9 (or error will be returned)
}
const
	kIconServicesCatalogInfoMask = kFSCatInfoNodeID or kFSCatInfoParentDirID or kFSCatInfoVolume or kFSCatInfoNodeFlags or kFSCatInfoFinderInfo or kFSCatInfoFinderXInfo or kFSCatInfoUserAccess or kFSCatInfoPermissions or kFSCatInfoContentMod;


{
  ==============================================================================
   Reference counting
  ==============================================================================
}


{
   GetIconRefOwners
   
   This routine returns the reference count for the IconRef, or number of owners.
   
   A valid IconRef always has at least one owner.
}

{
 *  GetIconRefOwners()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function GetIconRefOwners( theIconRef: IconRef; var owners: UInt16 ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
   AcquireIconRef
   This routine increments the reference count for the IconRef
}

{
 *  AcquireIconRef()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function AcquireIconRef( theIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
   ReleaseIconRef
   
   This routine decrements the reference count for the IconRef.
   
   When the reference count reaches 0, all memory allocated for the icon
   is disposed. Any subsequent use of the IconRef is invalid.
}

{
 *  ReleaseIconRef()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function ReleaseIconRef( theIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
  ==============================================================================
   Getting an IconRef
  ==============================================================================
}


{
   GetIconRefFromFile
   
   This routine returns an icon ref for the specified file, folder or volume.
   The label information is provided separately, since two files with the same icon 
   but a different label would share the same iconRef. The label can be used in 
   PlotIconRef() for example.
   
   Use this routine if you have no information about the file system object. If 
   you have already done a GetCatInfo on the file and want to save some I/O, 
   call GetIconRefFromFolder() if you know it's a folder with no custom icon or 
   call GetIconRef() if it's a file with no custom icon.
   This routine increments the reference count of the returned IconRef. Call 
   ReleaseIconRef() when you're done with it.
   This call is deprecated. Please use GetIconRefFromFileInfo() instead.
}

{$ifc not TARGET_CPU_64}
{
 *  GetIconRefFromFile()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function GetIconRefFromFile( const var theFile: FSSpec; var theIconRef: IconRef; var theLabel: SInt16 ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);

{$endc}	{ not TARGET_CPU_64 }

{
   GetIconRef
   
   This routine returns an icon ref for an icon in the desktop database or
   for a registered icon.
   The system registers a set of icon such as the help icon with the creator 
   code kSystemIconsCreator. See above for a list of the registered system types.
   The vRefNum is used as a hint on where to look for the icon first. Use 
   kOnSystemDisk if you don't know what to pass.
   This routine increments the reference count of the returned IconRef. Call 
   ReleaseIconRef() when you're done with it.
}

{
 *  GetIconRef()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function GetIconRef( vRefNum: SInt16; creator: OSType; iconType: OSType; var theIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
   GetIconRefFromFolder
   
   This routine returns an icon ref for a folder with no custom icon.
   Use the more generic, but slightly slower, GetIconRefFromFile() if
   you don't already have the necessary info about the file.
   Attributes should be CInfoPBRec.dirInfo.ioFlAttrib for this folder.
   Access privileges should be CInfoPBRec.dirInfo.ioACUser for this folder.
   This routine increments the reference count of the IconRef. Call ReleaseIconRef() 
   when you're done with it.
}

{
 *  GetIconRefFromFolder()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function GetIconRefFromFolder( vRefNum: SInt16; parentFolderID: SInt32; folderID: SInt32; attributes: SInt8; accessPrivileges: SInt8; var theIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ GetIconRefFromFileInfo}
{
 *  GetIconRefFromFileInfo()
 *  
 *  Summary:
 *    This routine returns an IconRef for a file with minimal file I/O.
 *  
 *  Discussion:
 *    To minimize file operations, FSGetCatalogInfo should be called
 *    prior to calling this routine. The FSCatalogInfo should
 *    correspond to kIconServicesCatalogInfoMask The name should be
 *    fetched and passed in. If either the name or the correct catalog
 *    info is not passed in, this routine will do file operations for
 *    this information instead.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Parameters:
 *    
 *    inRef:
 *      An FSRef for the target file
 *    
 *    inFileNameLength:
 *      The length of the name of the target file
 *    
 *    inFileName:
 *      The name of the target file
 *    
 *    inWhichInfo:
 *      The mask of file info already acquired.
 *    
 *    inCatalogInfo:
 *      The catalog info already acquired.
 *    
 *    inUsageFlags:
 *      The usage flags for this call (use
 *      kIconServicesNormalUsageFlag).
 *    
 *    outIconRef:
 *      The output IconRef for the routine.
 *    
 *    outLabel:
 *      The output label for the icon/file.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.1 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.1 and later
 *    Non-Carbon CFM:   not available
 }
function GetIconRefFromFileInfo( const var inRef: FSRef; inFileNameLength: UniCharCount; {const} inFileName: UniCharPtr { can be NULL }; inWhichInfo: FSCatalogInfoBitmap; {const} inCatalogInfo: FSCatalogInfoPtr { can be NULL }; inUsageFlags: IconServicesUsageFlags; var outIconRef: IconRef; outLabel: SInt16Ptr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_1, __IPHONE_NA);


{ GetIconRefFromTypeInfo}
{
 *  GetIconRefFromTypeInfo()
 *  
 *  Summary:
 *    Create an IconRef for a type information.
 *  
 *  Discussion:
 *    Creates IconRef based on provided type info. Any of the input
 *    parameters can be zero (meaning it is unknown). Returns generic
 *    document icon in case if all parameters are zeroes. Calling the
 *    routine with non zero inCreator and inType and zero inExtension
 *    and inMIMEType is equivalent to GetIconRef(kOnSystemDisk,
 *    inCreator, inType).
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    inCreator:
 *      The creator.
 *    
 *    inType:
 *      The type.
 *    
 *    inExtension:
 *      The extension.
 *    
 *    inMIMEType:
 *      The MIME type.
 *    
 *    inUsageFlags:
 *      The usage flags for this call (use
 *      kIconServicesNormalUsageFlag).
 *    
 *    outIconRef:
 *      The output IconRef for the routine.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.3 and later
 *    Non-Carbon CFM:   not available
 }
function GetIconRefFromTypeInfo( inCreator: OSType; inType: OSType; inExtension: CFStringRef; inMIMEType: CFStringRef; inUsageFlags: IconServicesUsageFlags; var outIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);


{ GetIconRefFromIconFamilyPtr}
{
 *  GetIconRefFromIconFamilyPtr()
 *  
 *  Summary:
 *    Create an IconRef for the IconFamilyPtr.
 *  
 *  Discussion:
 *    This routine creates IconRef for the IconFamilyPtr.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    inIconFamilyPtr:
 *      The icon data
 *    
 *    inSize:
 *      The icon data size
 *    
 *    outIconRef:
 *      The output IconRef for the routine.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.3 and later
 *    Non-Carbon CFM:   not available
 }
function GetIconRefFromIconFamilyPtr( const var inIconFamilyPtr: IconFamilyResource; inSize: Size; var outIconRef: IconRef ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);


{ GetIconRefFromComponent}
{
 *  GetIconRefFromComponent()
 *  
 *  Summary:
 *    Create an IconRef for the component.
 *  
 *  Discussion:
 *    Creates IconRef based on componentIconFamily field of component's
 *    'thng' resource.. This routine increments the reference count of
 *    the IconRef. Call ReleaseIconRef() when you're done with it.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    inComponent:
 *      A component identifier.
 *    
 *    outIconRef:
 *      The output IconRef for the routine.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.5 and later
 *    Non-Carbon CFM:   not available
 }
function GetIconRefFromComponent( inComponent: Component; var outIconRef: IconRef ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);


{
  ==============================================================================
   Adding and modifying IconRef
  ==============================================================================
}


{
   RegisterIconRefFromIconFamily
   This routine adds a new entry to the IconRef registry. Other clients will be 
   able to access it using the (creator, iconType) pair specified here.
   Lower-case creators are reserved for the system.
   Consider using RegisterIconRefFromResource() if possible, since the data 
   registered using RegisterIconRefFromFamily() cannot be purged.
   The iconFamily data is copied and the caller is reponsible for disposing of it.
   This routine increments the reference count of the IconRef. Call ReleaseIconRef() 
   when you're done with it.
}

{
 *  RegisterIconRefFromIconFamily()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function RegisterIconRefFromIconFamily( creator: OSType; iconType: OSType; iconFamily: IconFamilyHandle; var theIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
   RegisterIconRefFromResource
   
   Registers an IconRef from a resouce file.  
   Lower-case creators are reserved for the system.
   The icon data to be fetched is either classic icon data or an icon family.  
   The 'icns' icon family is searched for before the classic icon data.
   This routine increments the reference count of the IconRef. Call ReleaseIconRef() 
   when you're done with it.
}

{$ifc not TARGET_CPU_64}
{
 *  RegisterIconRefFromResource()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function RegisterIconRefFromResource( creator: OSType; iconType: OSType; const var resourceFile: FSSpec; resourceID: SInt16; var theIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);

{$endc}	{ not TARGET_CPU_64 }

{ RegisterIconRefFromFSRef}

{
 *  RegisterIconRefFromFSRef()
 *  
 *  Discussion:
 *    This routine registers an IconRef from a ".icns" file and
 *    associates it with a creator/type pair.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Parameters:
 *    
 *    creator:
 *      The creator code for the icns file.
 *    
 *    iconType:
 *      The type code for the icns file
 *    
 *    iconFile:
 *      The FSRef of the icns file.
 *    
 *    theIconRef:
 *      The output IconRef for the routine.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.1 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.1 and later
 *    Non-Carbon CFM:   not available
 }
function RegisterIconRefFromFSRef( creator: OSType; iconType: OSType; const var iconFile: FSRef; var theIconRef: IconRef ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_1, __IPHONE_NA);


{
   UnregisterIconRef
   
   Removes the specified icon from the icon cache (if there are no users of it).  
   If some clients are using this iconRef, then the IconRef will be removed when the 
   last user calls ReleaseIconRef.
}

{
 *  UnregisterIconRef()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function UnregisterIconRef( creator: OSType; iconType: OSType ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
   UpdateIconRef
   
   Call this routine to force an update of the data for iconRef.
   
   For example after changing an icon in the desktop database or changing the custom 
   icon of a file. Note that after _adding_ a custom icon to file or folder, you 
   need to call GetIconRefFromFile() to get a new IconRef specific to this file. 
   
   This routine does nothing if the IconRef is a registered icon.
}

{
 *  UpdateIconRef()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function UpdateIconRef( theIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
   OverrideIconRefFromResource
   
   This routines replaces the bitmaps of the specified IconRef with the ones
   in the specified resource file.
}

{$ifc not TARGET_CPU_64}
{
 *  OverrideIconRefFromResource()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function OverrideIconRefFromResource( theIconRef: IconRef; const var resourceFile: FSSpec; resourceID: SInt16 ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);

{$endc}	{ not TARGET_CPU_64 }

{
   OverrideIconRef
   
   This routines replaces the bitmaps of the specified IconRef with the ones
   from the new IconRef.
}

{
 *  OverrideIconRef()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function OverrideIconRef( oldIconRef: IconRef; newIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
   RemoveIconRefOverride
   This routine remove an override if one was applied to the icon and 
   reverts back to the original bitmap data.
}

{
 *  RemoveIconRefOverride()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function RemoveIconRefOverride( theIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
  ==============================================================================
   Creating composite IconRef
  ==============================================================================
}


{
   CompositeIconRef
   
   Superimposes an IconRef on top of another one
}

{
 *  CompositeIconRef()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function CompositeIconRef( backgroundIconRef: IconRef; foregroundIconRef: IconRef; var compositeIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
   IsIconRefComposite
   Indicates if a given icon ref is a composite of two other icon refs (and which ones)
   If it isn't a composite, backgroundIconRef and foreGroundIconRef will be 0.
}

{
 *  IsIconRefComposite()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function IsIconRefComposite( compositeIconRef: IconRef; var backgroundIconRef: IconRef; var foregroundIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
  ==============================================================================
   Using IconRef
  ==============================================================================
}

{
   IsValidIconRef
   Return true if the iconRef passed in is a valid icon ref
}

{
 *  IsValidIconRef()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function IsValidIconRef( theIconRef: IconRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ IsDataAvailableInIconRef}
{
 *  IsDataAvailableInIconRef()
 *  
 *  Summary:
 *    Check if IconRef has specific data.
 *  
 *  Discussion:
 *    This routine returns true if inIconKind icon data is availabe or
 *    can be created.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    inIconKind:
 *      The icon data kind
 *    
 *    inIconRef:
 *      The IconRef to test.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.3 and later
 *    Non-Carbon CFM:   not available
 }
function IsDataAvailableInIconRef( inIconKind: OSType; inIconRef: IconRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);


{
  ==============================================================================
   Flushing IconRef data
  ==============================================================================
}


{
   FlushIconRefs
   
   Making this call will dispose of all the data for the specified icons if 
   the data can be reacquired, for example if the data is provided from a resource.
   '****' is a wildcard for all types or all creators.
}

{$ifc not TARGET_CPU_64}
{
 *  FlushIconRefs()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function FlushIconRefs( creator: OSType; iconType: OSType ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);


{
   FlushIconRefsByVolume
   
   This routine disposes of the data for the icons related to the indicated volume
   if this data can be reacquired, for example if the data is provided from a 
   resource.
}

{
 *  FlushIconRefsByVolume()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function FlushIconRefsByVolume( vRefNum: SInt16 ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);

{$endc}	{ not TARGET_CPU_64 }

{
  ==============================================================================
   Controling custom icons
  ==============================================================================
}


{
   SetCustomIconsEnabled
   
   Enable or disable custom icons on the specified volume.
}


{
 *  SetCustomIconsEnabled()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function SetCustomIconsEnabled( vRefNum: SInt16; enableCustomIcons: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
   GetCustomIconsEnabled
   
   Return true if custom icons are enabled on the specified volume, false otherwise.
}

{
 *  GetCustomIconsEnabled()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 8.5 and later
 }
function GetCustomIconsEnabled( vRefNum: SInt16; var customIconsEnabled: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
  ==============================================================================
   Icon files (.icns files)
  ==============================================================================
}


{
   RegisterIconRefFromIconFile
   This routine adds a new entry to the IconRef registry. Other clients will be 
   able to access it using the (creator, iconType) pair specified here.
   Lower-case creators are reserved for the system.
   If the creator is kSystemIconsCreator and the iconType is 0, a new IconRef
   is always returned. Otherwise, if the creator and type have already been
   registered, the previously registered IconRef is returned.
   This routine increments the reference count of the IconRef. Call ReleaseIconRef() 
   when you're done with it.
}

{$ifc not TARGET_CPU_64}
{
 *  RegisterIconRefFromIconFile()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jagua
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 9.0 and later
 }
function RegisterIconRefFromIconFile( creator: OSType; iconType: OSType; const var iconFile: FSSpec; var theIconRef: IconRef ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);


{
   ReadIconFile
   Read the specified icon file into the icon family handle.
   The caller is responsible for disposing the iconFamily
}

{
 *  ReadIconFile()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 9.0 and later
 }
function ReadIconFile( const var iconFile: FSSpec; var iconFamily: IconFamilyHandle ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);


{
   WriteIconFile
   Write the iconFamily handle to the specified file
}

{
 *  WriteIconFile()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in IconServicesLib 9.0 and later
 }
function WriteIconFile( iconFamily: IconFamilyHandle; const var iconFile: FSSpec ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);

{$endc}	{ not TARGET_CPU_64 }

{ ReadIconFromFSRef}

{
 *  ReadIconFromFSRef()
 *  
 *  Discussion:
 *    This routine reads an icon (icns) file into memory.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    ref:
 *      The FSRef for the icon file.
 *    
 *    iconFamily:
 *      The handle for the icon family.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.1 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.1 and later
 *    Non-Carbon CFM:   not available
 }
function ReadIconFromFSRef( const var ref: FSRef; var iconFamily: IconFamilyHandle ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_1, __IPHONE_NA);

{$endc} {TARGET_OS_MAC}

end.
