{
 *  ABGlobalsC.h
 *  AddressBook Framework
 *
 *  Copyright (c) 2003-2010 Apple Inc.  All rights reserved.
 *
 }
{	  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, 2004 }
{   Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, Feburary 2006 }
{	  Pascal Translation Updated:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, November 2009 }
{     Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2018 }

unit ABGlobals;
interface
uses MacTypes,CFBase;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


// NOTE: This header is for C programmers. For Objective-C use ABGlobals.h

// ================================================================
//      Global Table properties
// ================================================================

// ----- Properties common to all Records

const kABUIDProperty: CFStringRef;                   // The UID property - kABStringProperty
const kABCreationDateProperty: CFStringRef;          // Creation Date (when first saved) - kABDateProperty
const kABModificationDateProperty: CFStringRef;      // Last saved date - kABDateProperty

// ----- Person specific properties

const kABFirstNameProperty: CFStringRef;             // First name - kABStringProperty
const kABLastNameProperty: CFStringRef;              // Last name - kABStringProperty

const kABFirstNamePhoneticProperty: CFStringRef;     // First name Phonetic - kABStringProperty
const kABLastNamePhoneticProperty: CFStringRef;      // Last name Phonetic - kABStringProperty

const kABNicknameProperty: CFStringRef;              // kABStringProperty
const kABMaidenNameProperty: CFStringRef;            // kABStringProperty

const kABBirthdayProperty: CFStringRef;              // Birth date - kABDateProperty

const kABOrganizationProperty: CFStringRef;          // Company name - kABStringProperty

const kABJobTitleProperty: CFStringRef;              // Job Title - kABStringProperty

// Deprecated in Mac OS 10.4. You should use kABURLsProperty.
const kABHomePageProperty: CFStringRef;              // Home Web page - kABStringProperty

const kABURLsProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;          // URLs - kABMultiStringProperty
const kABHomePageLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER; // Homepage URL

const kABCalendarURIsProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER; // Calendar URIs - kABMultiStringProperty

const kABEmailProperty: CFStringRef;                 // Email(s) - kABMultiStringProperty
const kABEmailWorkLabel: CFStringRef;        // Work email
const kABEmailHomeLabel: CFStringRef;        // Home email

const kABAddressProperty: CFStringRef;                // Street Addresses - kABMultiDictionaryProperty
const kABAddressStreetKey: CFStringRef;           // Street
const kABAddressCityKey: CFStringRef;             // City
const kABAddressStateKey: CFStringRef;            // State
const kABAddressZIPKey: CFStringRef;              // Zip
const kABAddressCountryKey: CFStringRef;          // Country
const kABAddressCountryCodeKey: CFStringRef;      // Country Code
const kABAddressHomeLabel: CFStringRef;       // Home Address
const kABAddressWorkLabel: CFStringRef;       // Work Address

{
 * kABAddressCountryCodeKey code must be one of the following:
 * iso country codes
 *
 *    ar = Argentina
 *    at = Austria
 *    au = Australia
 *    ba = Bosnia and Herzegovina
 *    be = Belgium
 *    bg = Bulgaria
 *    bh = Bahrain
 *    br = Brazil
 *    ca = Canada
 *    ch = Switzerland
 *    cn = China
 *    cs = Czech
 *    de = Germany
 *    dk = Denmark
 *    eg = Egypt
 *    es = Spain
 *    fi = Finland
 *    fr = France
 *    gr = Greece
 *    gl = Greenland
 *    hk = Hong Kong
 *    hr = Croatia
 *    hu = Hungary
 *    ie = Ireland
 *    il = Israel
 *    id = Indonesia
 *    in = India
 *    is = Iceland
 *    it = Italy
 *    ja = Japan
 *    jo = Jordan
 *    kr = South Korea
 *    kw = Kuwait
 *    lb = Lebanon
 *    lu = Luxembourg
 *    mk = Macedonia
 *    mx = Mexico
 *    nl = Netherlands
 *    no = Norway
 *    nz = New Zealand
 *    om = Oman
 *    pl = Poland
 *    pt = Portugal
 *    qa = Qatar
 *    ro = Romania
 *    ru = Russian Federation
 *    sa = Saudi Arabia
 *    se = Sweden
 *    sg = Singapore
 *    si = Slovenia
 *    sk = Slovakia
 *    sy = Syrian Arab Republic
 *    tw = Taiwan
 *    tr = Turkey
 *    ua = Ukraine
 *    uk = United Kingdom
 *    us = United States
 *    ye = Yemen
 *    za = South Africa
 *
 }

const kABOtherDatesProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;       // Dates associated with this person - kABMultiDateProperty - (Person)
const kABAnniversaryLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

const kABRelatedNamesProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;       // names - kABMultiStringProperty
const kABFatherLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABMotherLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABParentLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABBrotherLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABSisterLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABChildLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABFriendLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABSpouseLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABPartnerLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABAssistantLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABManagerLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

const kABDepartmentProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;         // Department name - kABStringProperty - (Person)

const kABPersonFlags: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;     // Various flags - kABIntegerProperty - (Person)

const	
	kABShowAsMask = 7;
	kABShowAsPerson = 0;
	kABShowAsCompany = 1;
	
	kABNameOrderingMask = 7 shl 3;
	kABDefaultNameOrdering = 0 shl 3;
	kABFirstNameFirst = 4 shl 3;
	kABLastNameFirst = 2 shl 3;

const kABPhoneProperty: CFStringRef;                  // Generic phone number - kABMultiStringProperty
const kABPhoneWorkLabel: CFStringRef;         // Work phone
const kABPhoneHomeLabel: CFStringRef;         // Home phone
const kABPhoneMobileLabel: CFStringRef;       // Cell phone
const kABPhoneMainLabel: CFStringRef;         // Main phone
const kABPhoneHomeFAXLabel: CFStringRef;      // FAX number
const kABPhoneWorkFAXLabel: CFStringRef;      // FAX number
const kABPhonePagerLabel: CFStringRef;        // Pager number


// Deprecated in Mac OS 10.7. You should use kABInstantMessageProperty.
const kABAIMInstantProperty: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;	// AIM Instant Messaging - kABMultiStringProperty
const kABAIMWorkLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;
const kABAIMHomeLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

// Deprecated in Mac OS 10.7. You should use kABInstantMessageProperty.
const kABJabberInstantProperty: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;	// Jabber Instant Messaging - kABMultiStringProperty
const kABJabberWorkLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;
const kABJabberHomeLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

// Deprecated in Mac OS 10.7. You should use kABInstantMessageProperty.
const kABMSNInstantProperty: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;	// MSN Instant Messaging - kABMultiStringProperty
const kABMSNWorkLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;
const kABMSNHomeLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

// Deprecated in Mac OS 10.7. You should use kABInstantMessageProperty.
const kABYahooInstantProperty: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;	// Yahoo Instant Messaging - kABMultiStringProperty
const kABYahooWorkLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;
const kABYahooHomeLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

// Deprecated in Mac OS 10.7. You should use kABInstantMessageProperty.
const kABICQInstantProperty: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;	// ICQ Instant Messaging - kABMultiStringProperty
const kABICQWorkLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;
const kABICQHomeLabel: CFStringRef;
DEPRECATED_IN_MAC_OS_X_VERSION_10_7_AND_LATER;

const kABInstantMessageProperty: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;	// Instant Messaging - kABMultiDictionaryProperty
const kABInstantMessageUsernameKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// dictionary key for the instant messaging handle/username
const kABInstantMessageServiceKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// dictionary key for the service type; possible values follow
const kABInstantMessageServiceAIM: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// AIM
const kABInstantMessageServiceFacebook: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// Facebook
const kABInstantMessageServiceGaduGadu: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// Gadu-Gadu
const kABInstantMessageServiceGoogleTalk: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// Google Talk
const kABInstantMessageServiceICQ: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// ICQ
const kABInstantMessageServiceJabber: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// Jabber
const kABInstantMessageServiceMSN: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// MSN
const kABInstantMessageServiceQQ: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// QQ
const kABInstantMessageServiceSkype: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// Skype
const kABInstantMessageServiceYahoo: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;		// Yahoo!

const kABNoteProperty: CFStringRef;                    // Note (string)

const kABMiddleNameProperty: CFStringRef;             // kABStringProperty
const kABMiddleNamePhoneticProperty: CFStringRef;     // kABStringProperty
const kABTitleProperty: CFStringRef;                  // kABStringProperty "Sir" "Duke" "General" "Lord"
const kABSuffixProperty: CFStringRef;                 // kABStringProperty "Sr." "Jr." "III"

// ----- Group Specific Properties

const kABGroupNameProperty: CFStringRef;              // Name of the group - kABStringProperty

// ================================================================
//      Generic Labels
// ================================================================

    // All kABXXXXWorkLabel are equivalent to this label
const kABWorkLabel: CFStringRef;

    // All kABXXXXHomeLabel are equivalent to this label
const kABHomeLabel: CFStringRef;

    // Can be used with any multi-value property
const kABOtherLabel: CFStringRef;

    // MobileMe - for AIM or email values
const kABMobileMeLabel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_7_AND_LATER;


// ================================================================
//      RecordTypes
// ================================================================

    // Type of a ABPersonRef
const kABPersonRecordType: CFStringRef;

    // Type of a ABGroupRef
const kABGroupRecordType: CFStringRef;

// ================================================================
//      Notifications published when something changes
// ================================================================
// These notifications are not sent until ABGetSharedAddressBook()
// has been called somewhere

    // This process has changed the DB
const kABDatabaseChangedNotification: CFStringRef;

    // Another process has changed the DB
const kABDatabaseChangedExternallyNotification: CFStringRef;

    // The user info in the above notifications will contain
    // the following 3 keys, the values of the keys in the dictionary
    // will be the uniqueIds of the Inserted/Updated/Deleted Records
const kABInsertedRecords: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABUpdatedRecords: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
const kABDeletedRecords: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;


// ================================================================
//      Localization of property or label
// ================================================================

    // Returns the localized version of built in properties, labels or keys
    // Returns propertyOrLabel if not found (e.g. if not built in)
function ABLocalizedPropertyOrLabel( propertyOrLabel: CFStringRef ): CFStringRef;

{$endc} {TARGET_OS_MAC}

end.
