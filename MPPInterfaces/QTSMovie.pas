{
     File:       QuickTime/QTSMovie.h
 
     Contains:   QuickTime Interfaces.
 
     Version:    QuickTime 7.6.3
 
     Copyright:  � 1990-2008 by Apple Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
unit QTSMovie;
interface
uses MacTypes,Components,Movies,QuickTimeStreaming;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{ QuickTime is not available to 64-bit clients }

{$ifc not TARGET_CPU_64}

const
	kQTSStreamMediaType = FOUR_CHAR_CODE('strm');

type
	QTSSampleDescription = record
		descSize: SIGNEDLONG;
		dataFormat: SIGNEDLONG;
		resvd1: SIGNEDLONG;                 { set to 0}
		resvd2: SInt16;                 { set to 0}
		dataRefIndex: SInt16;
		version: UInt32;
		resvd3: UInt32;                 { set to 0}
		flags: SInt32;
                                              { qt atoms follow:}
                                              {      long size, long type, some data}
                                              {      repeat as necessary}
	end;
	QTSSampleDescriptionPtr = ^QTSSampleDescription;
type
	QTSSampleDescriptionHandle = ^QTSSampleDescriptionPtr;
const
	kQTSSampleDescriptionVersion1 = 1;

const
	kQTSDefaultMediaTimeScale = 600;

{ sample description flags}
const
	kQTSSampleDescPassSampleDataAsHandleFlag = $00000001;


{============================================================================
        Stream Media Handler
============================================================================}
{-----------------------------------------
    Info Selectors
-----------------------------------------}
{ all indexes start at 1 }

const
	kQTSMediaPresentationInfo = FOUR_CHAR_CODE('pres'); { QTSMediaPresentationParams* }
	kQTSMediaNotificationInfo = FOUR_CHAR_CODE('noti'); { QTSMediaNotificationParams* }
	kQTSMediaTotalDataRateInfo = FOUR_CHAR_CODE('dtrt'); { UInt32*, bits/sec }
	kQTSMediaLostPercentInfo = FOUR_CHAR_CODE('lspc'); { Fixed* }
	kQTSMediaNumStreamsInfo = FOUR_CHAR_CODE('nstr'); { UInt32* }
	kQTSMediaIndSampleDescriptionInfo = FOUR_CHAR_CODE('isdc'); { QTSMediaIndSampleDescriptionParams* }


type
	QTSMediaPresentationParamsPtr = ^QTSMediaPresentationParams;
	QTSMediaPresentationParams = record
		presentationID: QTSPresentation;
	end;
type
	QTSMediaNotificationParamsPtr = ^QTSMediaNotificationParams;
	QTSMediaNotificationParams = record
		notificationProc: QTSNotificationUPP;
		notificationRefCon: UnivPtr;
		flags: SInt32;
	end;
type
	QTSMediaIndSampleDescriptionParamsPtr = ^QTSMediaIndSampleDescriptionParams;
	QTSMediaIndSampleDescriptionParams = record
		index: SInt32;
		returnedMediaType: OSType;
		returnedSampleDescription: SampleDescriptionHandle;
	end;
{-----------------------------------------
    QTS Media Handler Selectors
-----------------------------------------}
const
	kQTSMediaSetInfoSelect = $0100;
	kQTSMediaGetInfoSelect = $0101;
	kQTSMediaSetIndStreamInfoSelect = $0102;
	kQTSMediaGetIndStreamInfoSelect = $0103;

{-----------------------------------------
    QTS Media Handler functions
-----------------------------------------}
{
 *  QTSMediaSetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSMediaSetInfo( mh: MediaHandler; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSMediaGetInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSMediaGetInfo( mh: MediaHandler; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSMediaSetIndStreamInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSMediaSetIndStreamInfo( mh: MediaHandler; inIndex: SInt32; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  QTSMediaGetIndStreamInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in QuickTime.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in QTStreamLib 4.0 and later
 *    Windows:          in QTSClient.lib 4.0 and later
 }
function QTSMediaGetIndStreamInfo( mh: MediaHandler; inIndex: SInt32; inSelector: OSType; ioParams: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{============================================================================
        Hint Media Handler
============================================================================}
const
	kQTSHintMediaType = FOUR_CHAR_CODE('hint');

const
	kQTSHintTrackReference = FOUR_CHAR_CODE('hint');


{ MixedMode ProcInfo constants for component calls }
const
	uppQTSMediaSetInfoProcInfo = $00000FF0;
	uppQTSMediaGetInfoProcInfo = $00000FF0;
	uppQTSMediaSetIndStreamInfoProcInfo = $00003FF0;
	uppQTSMediaGetIndStreamInfoProcInfo = $00003FF0;

{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
