{	CFBitVector.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFBitVector;
interface
uses MacTypes,CFBase;
{$ALIGN POWER}


type
	CFBit = UInt32;

type
	CFBitVectorRef = ^__CFBitVector; { an opaque type }
	__CFBitVector = record end;
	CFMutableBitVectorRef = ^__CFBitVector; { an opaque type }

function CFBitVectorGetTypeID: CFTypeID;

function CFBitVectorCreate( allocator: CFAllocatorRef; bytes: univ Ptr; numBits: CFIndex ): CFBitVectorRef;
function CFBitVectorCreateCopy( allocator: CFAllocatorRef; bv: CFBitVectorRef ): CFBitVectorRef;
function CFBitVectorCreateMutable( allocator: CFAllocatorRef; capacity: CFIndex ): CFMutableBitVectorRef;
function CFBitVectorCreateMutableCopy( allocator: CFAllocatorRef; capacity: CFIndex; bv: CFBitVectorRef ): CFMutableBitVectorRef;

function CFBitVectorGetCount( bv: CFBitVectorRef ): CFIndex;
function CFBitVectorGetCountOfBit( bv: CFBitVectorRef; range: CFRange; value: CFBit ): CFIndex;
function CFBitVectorContainsBit( bv: CFBitVectorRef; range: CFRange; value: CFBit ): Boolean;
function CFBitVectorGetBitAtIndex( bv: CFBitVectorRef; idx: CFIndex ): CFBit;
procedure CFBitVectorGetBits( bv: CFBitVectorRef; range: CFRange; bytes: univ Ptr );
function CFBitVectorGetFirstIndexOfBit( bv: CFBitVectorRef; range: CFRange; value: CFBit ): CFIndex;
function CFBitVectorGetLastIndexOfBit( bv: CFBitVectorRef; range: CFRange; value: CFBit ): CFIndex;

procedure CFBitVectorSetCount( bv: CFMutableBitVectorRef; count: CFIndex );
procedure CFBitVectorFlipBitAtIndex( bv: CFMutableBitVectorRef; idx: CFIndex );
procedure CFBitVectorFlipBits( bv: CFMutableBitVectorRef; range: CFRange );
procedure CFBitVectorSetBitAtIndex( bv: CFMutableBitVectorRef; idx: CFIndex; value: CFBit );
procedure CFBitVectorSetBits( bv: CFMutableBitVectorRef; range: CFRange; value: CFBit );
procedure CFBitVectorSetAllBits( bv: CFMutableBitVectorRef; value: CFBit );


end.
