{ CoreGraphics - CGPDFDictionary.h
 * Copyright (c) 2002-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFDictionary;
interface
uses MacTypes,CGPDFObject,CGBase;
{$ALIGN POWER}


// CGPDFDictionaryRef defined in CGBase


{ Return the number of entries in `dictionary'. }

function CGPDFDictionaryGetCount( dict: CGPDFDictionaryRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object associated with `key' in `dict' and return the result
   in `value'. Return true on success; false otherwise. }

function CGPDFDictionaryGetObject( dict: CGPDFDictionaryRef; key: ConstCStringPtr; var value: CGPDFObjectRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object associated with `key' in `dict' and, if it's a
   boolean, return the result in `value'. Return true on success; false
   otherwise. }

function CGPDFDictionaryGetBoolean( dict: CGPDFDictionaryRef; key: ConstCStringPtr; var value: CGPDFBoolean ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object associated with `key' in `dict' and, if it's an
   integer, return the result in `value'. Return true on success; false
   otherwise. }

function CGPDFDictionaryGetInteger( dict: CGPDFDictionaryRef; key: ConstCStringPtr; var value: CGPDFInteger ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object associated with `key' in `dict' and, if it's a number
   (real or integer), return the result in `value'. Return true on success;
   false otherwise. }

function CGPDFDictionaryGetNumber( dict: CGPDFDictionaryRef; key: ConstCStringPtr; var value: CGPDFReal ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object associated with `key' in `dict' and, if it's a name,
   return the result in `value'. Return true on success; false otherwise. }

function CGPDFDictionaryGetName( dict: CGPDFDictionaryRef; key: ConstCStringPtr; var value: ConstCStringPtr ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object associated with `key' in `dict' and, if it's a string,
   return the result in `value'. Return true on success; false otherwise. }

function CGPDFDictionaryGetString( dict: CGPDFDictionaryRef; key: ConstCStringPtr; var value: CGPDFStringRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object associated with `key' in `dict' and, if it's an array,
   return the result in `value'. Return true on success; false otherwise. }

function CGPDFDictionaryGetArray( dict: CGPDFDictionaryRef; key: ConstCStringPtr; var value: CGPDFArrayRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object associated with `key' in `dict' and, if it's a
   dictionary, return the result in `value'. Return true on success; false
   otherwise. }

function CGPDFDictionaryGetDictionary( dict: CGPDFDictionaryRef; key: ConstCStringPtr; var value: CGPDFDictionaryRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Look up the object associated with `key' in `dict' and, if it's a stream,
   return the result in `value'. Return true on success; false otherwise. }

function CGPDFDictionaryGetStream( dict: CGPDFDictionaryRef; key: ConstCStringPtr; var value: CGPDFStreamRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ The callback for `CGPDFDictionaryApplyFunction'. `key' is the current
   key, `value' is the value for `key', and `info' is the parameter passed
   to `CGPDFDictionaryApplyFunction'. }

type
	CGPDFDictionaryApplierFunction = procedure( key: ConstCStringPtr; value: CGPDFObjectRef; info: univ Ptr );

{ Enumerate all of the keys in `dict', calling `function' once for each
   key/value pair. Passes the current key, the associated value, and `info'
   to `function'. }

procedure CGPDFDictionaryApplyFunction( dict: CGPDFDictionaryRef; func: CGPDFDictionaryApplierFunction; info: univ Ptr );
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);


end.
