{ CoreGraphics - CGBase.h
   Copyright (c) 2000-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGBase;
interface
uses MacTypes,ConditionalMacros;
{$ALIGN POWER}


type
{$ifc TARGET_CPU_X86_64}
	boolean_t							= UInt32;
{$elsec}
	boolean_t							= SInt32;
{$endc}

{$ifc TARGET_CPU_64}
type
  CGFloat = Float64;

const
  CGFloat_Min = Float64_Min;
  CGFloat_Max = Float64_Max;
{$elsec}
type
  CGFloat = Float32;

const
  CGFloat_Min = Float32_Min;
  CGFloat_Max = Float32_Max;
{$endc}

type
  CGFloatPtr = ^CGFloat;

// Avoid cyclic dependencises by putting CGRefs here

type
	CGColorRef = ^OpaqueCGColorRef; { an opaque type }
	OpaqueCGColorRef = record end;
	CGPatternRef = ^OpaqueCGPatternRef; { an opaque type }
	OpaqueCGPatternRef = record end;
	CGPDFStringRef = ^OpaqueCGPDFStringRef; { an opaque type }
	OpaqueCGPDFStringRef = record end;
	CGPDFStreamRef = ^OpaqueCGPDFStreamRef; { an opaque type }
	OpaqueCGPDFStreamRef = record end;
	CGPDFScannerRef = ^OpaqueCGPDFScannerRef; { an opaque type }
	OpaqueCGPDFScannerRef = record end;
	CGPDFContentStreamRef = ^OpaqueCGPDFContentStreamRef; { an opaque type }
	OpaqueCGPDFContentStreamRef = record end;
	CGPDFOperatorTableRef = ^OpaqueCGPDFOperatorTableRef; { an opaque type }
	OpaqueCGPDFOperatorTableRef = record end;
	CGPDFPageRef = ^OpaqueCGPDFPageRef; { an opaque type }
	OpaqueCGPDFPageRef = record end;
	CGPDFDictionaryRef = ^OpaqueCGPDFDictionaryRef; { an opaque type }
	OpaqueCGPDFDictionaryRef = record end;
	CGPDFArrayRef = ^OpaqueCGPDFArrayRef; { an opaque type }
	OpaqueCGPDFArrayRef = record end;
	CGPSConverterRef = ^OpaqueCGPSConverterRef; { an opaque type }
	OpaqueCGPSConverterRef = record end;
	CGGradientRef = ^OpaqueCGGradientRef; { an opaque type }
	OpaqueCGGradientRef = record end;


end.
