{ CoreGraphics - CGPDFPage.h
   Copyright (c) 2001-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFPage;
interface
uses MacTypes,CGAffineTransforms,CFBase,CGBase,CGPDFDocument,CGGeometry;
{$ALIGN POWER}


// CGPDFPageRef defined in CGBase


type
	CGPDFBox = SInt32;
const
	kCGPDFMediaBox = 0;
	kCGPDFCropBox = 1;
	kCGPDFBleedBox = 2;
	kCGPDFTrimBox = 3;
	kCGPDFArtBox = 4;

{ Equivalent to `CFRetain(page)', except it doesn't crash (as CFRetain
   does) if `page' is NULL. }

function CGPDFPageRetain( page: CGPDFPageRef ): CGPDFPageRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Equivalent to `CFRelease(page)', except it doesn't crash (as CFRelease
   does) if `page' is NULL. }

procedure CGPDFPageRelease( page: CGPDFPageRef );
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the document of `page'. }

function CGPDFPageGetDocument( page: CGPDFPageRef ): CGPDFDocumentRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the page number of `page'. }

function CGPDFPageGetPageNumber( page: CGPDFPageRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the rectangle associated with `box' in `page'. This is the value
   of the corresponding entry (such as /MediaBox, /ArtBox, and so on) in the
   page's dictionary. }

function CGPDFPageGetBoxRect( page: CGPDFPageRef; box: CGPDFBox ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the rotation angle (in degrees) of `page'. This is the value of
   the /Rotate entry in the page's dictionary. }

function CGPDFPageGetRotationAngle( page: CGPDFPageRef ): SInt32;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return a transform mapping the box specified by `box' to `rect' as
   follows:
     - Compute the effective rect by intersecting the rect associated with
       `box' and the /MediaBox entry of the page.
     - Rotate the effective rect according to the page's /Rotate entry.
     - Center the resulting rect on `rect'. If `rotation' is non-zero, then
       the rect will rotated clockwise by `rotation' degrees. `rotation'
       must be a multiple of 90.
     - Scale the rect down, if necessary, so that it coincides with the
       edges of `rect'. If `preserveAspectRatio' is true, then the final
       rect will coincide with the edges of `rect' only in the more
       restrictive dimension. }

function CGPDFPageGetDrawingTransform( page: CGPDFPageRef; box: CGPDFBox; rect: CGRect; rotate: SInt32; preserveAspectRatio: CBool ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the dictionary of `page'. }

function CGPDFPageGetDictionary( page: CGPDFPageRef ): CGPDFDictionaryRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the CFTypeID for CGPDFPageRefs. }

function CGPDFPageGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);


end.
