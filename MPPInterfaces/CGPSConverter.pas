{ CoreGraphics - CGPSConverter.h
 * Copyright (c) 2003-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPSConverter;
interface
uses MacTypes,CFBase,CFDictionary,CGBase,CGDataConsumer,CGDataProvider;
{$ALIGN POWER}


// CGPSConverterRef defined in CGBase


{ CGPSConverter callbacks.
 
   `version' is the version number of the structure passed in as a parameter
   to the converter creation functions. The structure defined below is
   version 0.

   `beginDocument', if non-NULL, is called at the beginning of the
   conversion of the PostScript document.

   `endDocument', if non-NULL, is called at the end of conversion of the
   PostScript document.

   `beginPage', if non-NULL, is called at the start of the conversion of
   each page in the PostScript document.

   `endPage', if non-NULL, is called at the end of the conversion of each
   page in the PostScript document.

   `noteProgress', if non-NULL, is called periodically during the conversion
   to indicate that conversion is proceeding.

   `noteMessage', if non-NULL, is called to pass any messages that might
   result during the conversion.

   `releaseInfo', if non-NULL, is called when the converter is
   deallocated. }

type
	CGPSConverterBeginDocumentCallback = procedure( info: univ Ptr );

type
	CGPSConverterEndDocumentCallback = procedure( info: univ Ptr; success: CBool );

type
	CGPSConverterBeginPageCallback = procedure( info: univ Ptr; pageNumber: size_t; pageInfo: CFDictionaryRef );

type
	CGPSConverterEndPageCallback = procedure( info: univ Ptr; pageNumber: size_t; pageInfo: CFDictionaryRef );

type
	CGPSConverterProgressCallback = procedure( info: univ Ptr );

type
	CGPSConverterMessageCallback = procedure( info: univ Ptr; message: CFStringRef );

type
	CGPSConverterReleaseInfoCallback = procedure( info: univ Ptr );

type
	CGPSConverterCallbacks = record
		version: UInt32;
{$ifc TARGET_CPU_64}
		__alignment_dummy: UInt32;
{$endc}
		beginDocument: CGPSConverterBeginDocumentCallback;
		endDocument: CGPSConverterEndDocumentCallback;
		beginPage: CGPSConverterBeginPageCallback;
		endPage: CGPSConverterEndPageCallback;
		noteProgress: CGPSConverterProgressCallback;
		noteMessage: CGPSConverterMessageCallback;
		releaseInfo: CGPSConverterReleaseInfoCallback;
	end;

{$ifc TARGET_OS_MAC}

{ Create a CGPSConverter, using `callbacks' to populate its callback table.
   Each callback will be supplied the `info' value when called. }

function CGPSConverterCreate( info: univ Ptr; const var callbacks: CGPSConverterCallbacks; options: CFDictionaryRef ): CGPSConverterRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);

{ Use `converter' to convert PostScript data to PDF data. The PostScript
   data is supplied by `provider'; the resulting PDF is written to
   `consumer'. Returns true if the conversion succeeded; false otherwise. }

function CGPSConverterConvert( converter: CGPSConverterRef; provider: CGDataProviderRef; consumer: CGDataConsumerRef; options: CFDictionaryRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);

{ Tell the `converter' to abort conversion at the next possible
   opportunity. }

function CGPSConverterAbort( converter: CGPSConverterRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);

{ Return true if `converter' is currently converting data. }

function CGPSConverterIsConverting( converter: CGPSConverterRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);

{ Return the CFTypeID of the CGPSConverter class. }

function CGPSConverterGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);

{$endc}

end.
