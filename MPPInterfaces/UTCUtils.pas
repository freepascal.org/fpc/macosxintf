{
     File:       CarbonCore/UTCUtils.h
 
     Contains:   Interface for UTC to Local Time conversion and 64 Bit Clock routines
 
     Copyright:  � 1999-2011 by Apple Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit UTCUtils;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{ Options for Set & Get DateTime Routines }
const
	kUTCDefaultOptions = 0;

{ 64 Bit Clock Typedefs }
type
	UTCDateTime = record
		highSeconds: UInt16;
		lowSeconds: UInt32;
		fraction: UInt16;
	end;
	UTCDateTimePtr = ^UTCDateTime;
type
	UTCDateTimeHandle = ^UTCDateTimePtr;
	LocalDateTime = record
		highSeconds: UInt16;
		lowSeconds: UInt32;
		fraction: UInt16;
	end;
	LocalDateTimePtr = ^LocalDateTime;
type
	LocalDateTimeHandle = ^LocalDateTimePtr;
	{ Classic 32 bit clock conversion routines }
{$ifc not TARGET_CPU_64}
{
 *  ConvertLocalTimeToUTC()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use
 *    UCConvertUTCDateTimeToCFAbsoluteTime/CFTimeZoneGetSecondsFromGMT
 *    instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    UCConvertUTCDateTimeToCFAbsoluteTime and
 *    CFTimeZoneGetSecondsFromGMT instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in UTCUtils 1.0 and later
 }
function ConvertLocalTimeToUTC( localSeconds: UInt32; var utcSeconds: UInt32 ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  ConvertUTCToLocalTime()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use
 *    UCConvertUTCDateTimeToCFAbsoluteTime/CFTimeZoneGetSecondsFromGMT
 *    instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    UCConvertUTCDateTimeToCFAbsoluteTime and
 *    CFTimeZoneGetSecondsFromGMT instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in UTCUtils 1.0 and later
 }
function ConvertUTCToLocalTime( utcSeconds: UInt32; var localSeconds: UInt32 ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{ 64 bit clock conversion routines }
{
 *  ConvertUTCToLocalDateTime()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use
 *    UCConvertUTCDateTimeToCFAbsoluteTime/CFTimeZoneGetSecondsFromGMT
 *    instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    UCConvertUTCDateTimeToCFAbsoluteTime and
 *    CFTimeZoneGetSecondsFromGMT instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in UTCUtils 1.0 and later
 }
function ConvertUTCToLocalDateTime( const var utcDateTime_: UTCDateTime; var localDateTime_: LocalDateTime ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  ConvertLocalToUTCDateTime()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use
 *    UCConvertUTCDateTimeToCFAbsoluteTime/CFTimeZoneGetSecondsFromGMT
 *    instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    UCConvertUTCDateTimeToCFAbsoluteTime and
 *    CFTimeZoneGetSecondsFromGMT instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in UTCUtils 1.0 and later
 }
function ConvertLocalToUTCDateTime( const var localDateTime_: LocalDateTime; var utcDateTime_: UTCDateTime ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{ Getter and Setter Clock routines using 64 Bit values }
{
 *  GetUTCDateTime()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFAbsoluteTimeGetCurrent instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFAbsoluteTimeGetCurrent instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in UTCUtils 1.0 and later
 }
function GetUTCDateTime( var utcDateTime_: UTCDateTime; options: OptionBits ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  SetUTCDateTime()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use settimeofday (2) instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Setting the time requires
 *    root privileges. If you must, use settimeofday (2)
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in UTCUtils 1.0 and later
 }
function SetUTCDateTime( const var utcDateTime_: UTCDateTime; options: OptionBits ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  GetLocalDateTime()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFAbsoluteTimeGetCurrent/CFTimeZoneGetSecondsFromGMT instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFAbsoluteTimeGetCurrent and CFTimeZoneGetSecondsFromGMT instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in UTCUtils 1.0 and later
 }
function GetLocalDateTime( var localDateTime_: LocalDateTime; options: OptionBits ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  SetLocalDateTime()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    it without replacement
 *  
 *  Discussion:
 *    This function is no longer recommended. There is no replacement.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in UTCUtils 1.0 and later
 }
function SetLocalDateTime( const var localDateTime_: LocalDateTime; options: OptionBits ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);

{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
