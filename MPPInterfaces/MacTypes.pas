{
     File:       CarbonCore/MacTypes.h
 
     Contains:   Basic Macintosh data types.
 
     Version:    CarbonCore-769~1
  
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{	    Pascal Translation Updated:  Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, September 2012 }
unit MacTypes;
interface
uses ConditionalMacros;


{$ALIGN MAC68K}


{*******************************************************************************

    Base integer types for all target OS's and CPU's
    
        UInt8            8-bit unsigned integer 
        SInt8            8-bit signed integer
        UInt16          16-bit unsigned integer 
        SInt16          16-bit signed integer           
        UInt32          32-bit unsigned integer 
        SInt32          32-bit signed integer   
        UInt64          64-bit unsigned integer 
        SInt64          64-bit signed integer   

********************************************************************************}
type
{GPC-ONLY-START}
(*
These defines would be better, but do not display well in gdb currently
We will switch to them when that problem is resolved.  Remember to 
define Byte later on when reverting this change.
    SInt8 = Integer attribute( size = 8 );
    UInt8 = Cardinal attribute( size = 8 );
    SInt16 = Integer attribute( size = 16 );
    UInt16 = Cardinal attribute( size = 16 );
    SInt32 = Integer attribute( size = 32 );
    UInt32 = Cardinal attribute( size = 32 );
    SInt64 = Integer attribute( size = 64 );
    UInt64 = Cardinal attribute( size = 64 );
*)
    SInt8 = ByteInt;
    UInt8 = Byte;
    SInt16 = ShortInt;
    UInt16 = ShortWord;
    SInt32 = CInteger;
    UInt32 = CCardinal;
    SInt64 = LongInt;
    UInt64 = LongWord;

{GPC-ONLY-FINISH}
{MW-ONLY-START}
    SInt8 = -128..127;
    UInt8 = 0..255;
    SInt16 = Integer;
    UInt16 = UNSIGNEDWORD;
    SInt32 = LongInt;
    UInt32 = UNSIGNEDLONG;
{MW-ONLY-FINISH}
{FPC-ONLY-START}
    SInt8 = ShortInt;
    UInt8 = Byte;
    SInt16 = Integer;
    UInt16 = Word;
    SInt32 = LongInt;
    UInt32 = Longword;
    SInt64 = Int64;
    UInt64 = QWord;
{FPC-ONLY-FINISH}

type
    UNSIGNEDBYTE = UInt8;
    SIGNEDBYTE = SInt8;
    UNSIGNEDWORD = UInt16;
    SIGNEDWORD = SInt16;
    UInt32_fix = UInt32;

type
{$ifc TARGET_CPU_64}
    UNSIGNEDLONG = UInt64;
    SIGNEDLONG = SInt64;
{$elsec}
    UNSIGNEDLONG = UInt32;
    SIGNEDLONG = SInt32;
{$endc}
    MacPtrUInt = UNSIGNEDLONG;
    MacPtrSInt = SIGNEDLONG;


{GPC-FPC-ONLY-START}
{$ifc TARGET_RT_BIG_ENDIAN}

type
	wide = record
		case boolean of
			false:
				( hi:		SInt32;
					lo:		UInt32);
			true:
				( int:	SInt64)
	end;

	UnsignedWide = record
		case boolean of
			false:
				( hi:		UInt32;
					lo:		UInt32);
			true:
				( int:	UInt64)
	end;

{$elsec}

type
	wide = record
		case boolean of
			false:
				( lo:		UInt32;
					hi:		SInt32);
			true:
				( int:	SInt64)
	end;

	UnsignedWide = record
		case boolean of
			false:
				( lo:		UInt32;
					hi:		UInt32);
			true:
				( int:	UInt64)
	end;

{$endc}  {TARGET_RT_BIG_ENDIAN}
{GPC-FPC-ONLY-FINISH}


{MW-ONLY-START}
{$ifc TARGET_RT_BIG_ENDIAN}

type
	wide = record
		hi:						SInt32;
		lo:						UInt32;
	end;

	UnsignedWide = record
		hi:						UInt32;
		lo:						UInt32;
	end;

{$elsec}

type
	wide = record
		lo:						UInt32;
		hi:						SInt32;
	end;

	UnsignedWide = record
		lo:						UInt32;
		hi:						UInt32;
	end;

{$endc}  {TARGET_RT_BIG_ENDIAN}

type
    SInt64 = wide;
    UInt64 = UnsignedWide;
{MW-ONLY-FINISH}

type
    SInt8Ptr = ^SInt8;
    UInt8Ptr = ^UInt8;
    SInt16Ptr = ^SInt16;
    UInt16Ptr = ^UInt16;
    SInt32Ptr = ^SInt32;
    UInt32Ptr = ^UInt32;
    SInt64Ptr = ^SInt64;
    UInt64Ptr = ^UInt64;
    widePtr = ^wide;
    UnsignedWidePtr = ^UnsignedWide;
    SIGNEDLONGPtr = ^SIGNEDLONG;
    UNSIGNEDLONGPtr = ^UNSIGNEDLONG;

	{	*******************************************************************************
	
	    Special types for pascal
	    
	        ByteParameter       UInt8 passed as an 8-bit parameter
	        
	    Note:   The conventions for Pascal on 68K require that a UInt8 when
	            passed as a parameter occupy 16-bits on the stack, whereas 
	            an SInt8 would only occupy 8-bits.  To make C and Pascal
	            compatable, in pascal all function parameters of type UInt8
	            or equivalent are changed to ByteParameter.
	
	********************************************************************************	}
{GPC-ONLY-START}
	ByteParameter = UInt8;
{GPC-ONLY-FINISH}
{MW-FPC-ONLY-START}
	ByteParameter = SInt8;
{MW-FPC-ONLY-FINISH}

// For interfaces that use Cs "bool" type
{$ifc TARGET_CPU_PPC}
	CBool = SInt32;
{$elsec}
	CBool = SInt8;
{$endc}

{*******************************************************************************

    Base fixed point types 
    
        Fixed           16-bit signed integer plus 16-bit fraction
        UnsignedFixed   16-bit unsigned integer plus 16-bit fraction
        Fract           2-bit signed integer plus 30-bit fraction
        ShortFixed      8-bit signed integer plus 8-bit fraction
        
********************************************************************************}
type
	Fixed = SInt32;
	FixedPtr = ^Fixed;
	Fract = SInt32;
	FractPtr = ^Fract;
	UnsignedFixed = UInt32;
	UnsignedFixedPtr = ^UnsignedFixed;
	ShortFixed = SInt16;
	ShortFixedPtr = ^ShortFixed;


{*******************************************************************************

    Base floating point types 
    
        Float32         32 bit IEEE float:  1 sign bit, 8 exponent bits, 23 fraction bits
        Float64         64 bit IEEE float:  1 sign bit, 11 exponent bits, 52 fraction bits  
        Float80         80 bit MacOS float: 1 sign bit, 15 exponent bits, 1 integer bit, 63 fraction bits
        Float96         96 bit 68881 float: 1 sign bit, 15 exponent bits, 16 pad bits, 1 integer bit, 63 fraction bits
        
    Note: These are fixed size floating point types, useful when writing a floating
          point value to disk.  If your compiler does not support a particular size 
          float, a struct is used instead.
          Use of of the NCEG types (e.g. double_t) or an ANSI C type (e.g. double) if
          you want a floating point representation that is natural for any given
          compiler, but might be a different size on different compilers.

********************************************************************************}
type
	Float32 = Single;
	Float32Ptr = ^Float32;
	Float64 = Double;
	Float64Ptr = ^Float64;
	LongDouble = Double;
	Float80 = record
		exp: SInt16;
		man: array [0..3] of UInt16;
	end;

const
  Float32_Min = 1.5e-45;
  Float32_Max = 3.4e+38;
  Float64_Min = 5.0e-324;
  Float64_Max = 1.7e+308;

type
	Float96 = record
		exp: SInt16;
		filler: SInt16;
		man: array [0..3] of UInt16;
	end;
type
	Float32Point = record
		x: Float32;
		y: Float32;
	end;
{GK: Need in AudioUnitCarbonViews.pas }
	Float32PointPtr = ^Float32Point;
	
{*******************************************************************************
	Unix compatibility types        
********************************************************************************}
type
	size_t = UNSIGNEDLONG;
	size_t_ptr = ^size_t;
	ssize_t = SIGNEDLONG;
	ssize_t_ptr = ^ssize_t;

{*******************************************************************************

    MacOS Memory Manager types
    
        Ptr             Pointer to a non-relocatable block
        Handle          Pointer to a master pointer to a relocatable block
        Size            The number of bytes in a block (signed for historical reasons)
        
********************************************************************************}
type
	Ptr = ^SInt8;
	PtrPtr = ^Ptr;       
	Handle = ^Ptr;
	Handle_fix = Handle; { used as field type when a record declaration contains a Handle field identifier }
	Size = SIGNEDLONG;
	Size_fix = Size; { used as field type when a record declaration contains a Size field identifier }
	SizePtr = ^Size;
{GPC-FPC-ONLY-START}
	UnivPtr = Pointer;
{GPC-FPC-ONLY-ELSE}
	UnivPtr = Ptr;
{GPC-FPC-ONLY-FINISH}
	UnivPtrPtr = ^UnivPtr;

{*******************************************************************************

    Higher level basic types
    
        OSErr                   16-bit result error code
        OSStatus                32-bit result error code
        LogicalAddress          Address in the clients virtual address space
        ConstLogicalAddress     Address in the clients virtual address space that will only be read
        PhysicalAddress         Real address as used on the hardware bus
        BytePtr                 Pointer to an array of bytes
        ByteCount               The size of an array of bytes
        ByteOffset              An offset into an array of bytes
        ItemCount               32-bit iteration count
        OptionBits              Standard 32-bit set of bit flags
        PBVersion               ?
        Duration                32-bit millisecond timer for drivers
        AbsoluteTime            64-bit clock
        ScriptCode              A particular set of written characters (e.g. Roman vs Cyrillic) and their encoding
        LangCode                A particular language (e.g. English), as represented using a particular ScriptCode
        RegionCode              Designates a language as used in a particular region (e.g. British vs American
                                English) together with other region-dependent characteristics (e.g. date format)
        FourCharCode            A 32-bit value made by packing four 1 byte characters together
        OSType                  A FourCharCode used in the OS and file system (e.g. creator)
        ResType                 A FourCharCode used to tag resources (e.g. 'DLOG')
        
********************************************************************************}
type
	OSErr = SInt16;
	OSStatus = SInt32;
	LogicalAddress = UnivPtr;
	ConstLogicalAddress = UnivPtr;
	PhysicalAddress = UnivPtr;
	BytePtr = UInt8Ptr;
	ByteCount = UNSIGNEDLONG;
 	ByteCountPtr = ^ByteCount;
	ByteOffset = UNSIGNEDLONG;
 	ByteOffsetPtr = ^ByteOffset;
	Duration = SInt32;
	AbsoluteTime = UnsignedWide;
	AbsoluteTimePtr = ^AbsoluteTime;
	OptionBits = UInt32;
	OptionBitsPtr = ^OptionBits;
	ItemCount = UNSIGNEDLONG;
 	ItemCountPtr = ^ItemCount;
	PBVersion = UInt32;
	ScriptCode = SInt16;
 	ScriptCodePtr = ^ScriptCode;
	LangCode = SInt16;
 	LangCodePtr = ^LangCode;
	RegionCode = SInt16;
 	RegionCodePtr = ^RegionCode;
	FourCharCode = UInt32;
	OSType = FourCharCode;
	OSType_fix = OSType; { used as field type when a record declaration contains a OSType field identifier }
	ResType = FourCharCode;
	OSTypePtr = ^OSType;
	OSTypeHandle = ^OSTypePtr;
	ResTypePtr = ^ResType;
{*******************************************************************************

    Boolean types and values
    
        Boolean         Mac OS historic type, sizeof(Boolean)==1
        bool            Defined in stdbool.h, ISO C/C++ standard type
        false           Now defined in stdbool.h
        true            Now defined in stdbool.h
        
********************************************************************************}
type
{ "Boolean", "true", and "false" are built into the Pascal language }
	BooleanPtr = ^Boolean;
	boolean_fix = boolean; { used as field type when a record declaration contains a boolean field identifier }
{*******************************************************************************

    Function Pointer Types
    
        ProcPtr                 Generic pointer to a function
        Register68kProcPtr      Pointer to a 68K function that expects parameters in registers
        UniversalProcPtr        Pointer to classic 68K code or a RoutineDescriptor
        
        ProcHandle              Pointer to a ProcPtr
        UniversalProcHandle     Pointer to a UniversalProcPtr
        
********************************************************************************}
type
	ProcPtr = Ptr;
	Register68kProcPtr = ProcPtr;
	UniversalProcPtr = ProcPtr;

type
	ProcHandle = ^ProcPtr;
	UniversalProcHandle = ^UniversalProcPtr;


{*******************************************************************************

    RefCon Types
    
        For access to private data in callbacks, etc.; refcons are generally
        used as a pointer to something, but in the 32-bit world refcons in
        different APIs have had various types: pointer, unsigned scalar, and
        signed scalar. The RefCon types defined here support the current 32-bit
        usage but provide normalization to pointer types for 64-bit.
        
        PRefCon is preferred for new APIs; URefCon and SRefCon are primarily
        for compatibility with existing APIs.
        
********************************************************************************}
type
	PRefCon = UnivPtr;

{$ifc TARGET_CPU_64}

type
	URefCon = UnivPtr;
	SRefCon = UnivPtr;

{$elsec}

type
	URefCon = UInt32;
	SRefCon = SInt32;

{$endc}

{*******************************************************************************

    Common Constants
    
        noErr                   OSErr: function performed properly - no error
        kNilOptions             OptionBits: all flags false
        kInvalidID              KernelID: NULL is for pointers as kInvalidID is for ID's
        kVariableLengthArray    array bounds: variable length array

    Note: kVariableLengthArray was used in array bounds to specify a variable length array,
          usually the last field in a struct.  Now that the C language supports 
		  the concept of flexible array members, you can instead use: 
		
		struct BarList
		(
			short	listLength;
			Bar		elements[];
		);

		However, this changes the semantics somewhat, as sizeof( BarList ) contains
		no space for any of the elements, so to allocate a list with space for
		the count elements

		struct BarList* l = (struct BarList*) malloc( sizeof(BarList) + count * sizeof(Bar) );
        
********************************************************************************}
const
	noErr = 0;

const
	kNilOptions = 0;

const
	kInvalidID = 0;
const
	kVariableLengthArray = 1;

const
	kUnknownType = $3F3F3F3F; { "????" QuickTime 3.0: default unknown ResType or OSType }


{*******************************************************************************

    String Types and Unicode Types
    
        UnicodeScalarValue,     A complete Unicode character in UTF-32 format, with
        UTF32Char               values from 0 through 0x10FFFF (excluding the surrogate
                                range 0xD800-0xDFFF and certain disallowed values).

        UniChar,                A 16-bit Unicode code value in the default UTF-16 format.
        UTF16Char               UnicodeScalarValues 0-0xFFFF are expressed in UTF-16
                                format using a single UTF16Char with the same value.
                                UnicodeScalarValues 0x10000-0x10FFFF are expressed in
                                UTF-16 format using a pair of UTF16Chars - one in the
                                high surrogate range (0xD800-0xDBFF) followed by one in
                                the low surrogate range (0xDC00-0xDFFF). All of the
                                characters defined in Unicode versions through 3.0 are
                                in the range 0-0xFFFF and can be expressed using a single
                                UTF16Char, thus the term "Unicode character" generally
                                refers to a UniChar = UTF16Char.

        UTF8Char                An 8-bit code value in UTF-8 format. UnicodeScalarValues
                                0-0x7F are expressed in UTF-8 format using one UTF8Char
                                with the same value. UnicodeScalarValues above 0x7F are
                                expressed in UTF-8 format using 2-4 UTF8Chars, all with
                                values in the range 0x80-0xF4 (UnicodeScalarValues
                                0x100-0xFFFF use two or three UTF8Chars,
                                UnicodeScalarValues 0x10000-0x10FFFF use four UTF8Chars).

        UniCharCount            A count of UTF-16 code values in an array or buffer.

        StrNNN                  Pascal string holding up to NNN bytes
        StringPtr               Pointer to a pascal string
        StringHandle            Pointer to a StringPtr
        ConstStringPtr          Pointer to a read-only pascal string
        ConstStrNNNParam        For function parameters only - means string is const
        
        CStringPtr              Pointer to a C string           (in C:  char*)
        ConstCStringPtr         Pointer to a read-only C string (in C:  const char*)
        
    Note: The length of a pascal string is stored as the first byte.
          A pascal string does not have a termination byte.
          A pascal string can hold at most 255 bytes of data.
          The first character in a pascal string is offset one byte from the start of the string. 
          
          A C string is terminated with a byte of value zero.  
          A C string has no length limitation.
          The first character in a C string is the zeroth byte of the string. 
          
        
********************************************************************************}
type
	UnicodeScalarValue = UInt32;
	UTF32Char = UInt32;
	UTF32CharPtr = ^UTF32Char;
	UniChar = UInt16;
	UTF16Char = UInt16;
	UTF8Char = UInt8;
	UniCharPtr = ^UniChar;
	ConstUniCharPtr = UniCharPtr;
	UniCharCount = UNSIGNEDLONG;
	UniCharCountPtr = ^UniCharCount;
{GPC-ONLY-START}
	Str15 = record
					    sLength: UInt8;
					    sChars: packed array[1..15] of char;
					  end;
	Str27 = record
					    sLength: UInt8;
					    sChars: packed array[1..27] of char;
					  end;
	Str31 = record
					    sLength: UInt8;
					    sChars: packed array[1..31] of char;
					  end;
	Str32 = record
					    sLength: UInt8;
					    sChars: packed array[1..32] of char;
					  end;
	Str36 = record
					    sLength: UInt8;
					    sChars: packed array[1..36] of char;
					  end;
	Str63 = record
					    sLength: UInt8;
					    sChars: packed array[1..63] of char;
					  end;
	Str255 = record
					    sLength: UInt8;
					    sChars: packed array[1..255] of char;
					  end;
{GPC-ONLY-FINISH}
{MW-FPC-ONLY-START}
	Str15 = STRING[15];
	Str27 = STRING[27];
	Str31 = STRING[31];
	Str32 = STRING[32];
	Str36 = STRING[36];
	Str63 = STRING[63];
	Str255 = STRING[255];
{MW-FPC-ONLY-FINISH}
	{	
	    The type Str32 is used in many AppleTalk based data structures.
	    It holds up to 32 one byte chars.  The problem is that with the
	    length byte it is 33 bytes long.  This can cause weird alignment
	    problems in structures.  To fix this the type "Str32Field" has
	    been created.  It should only be used to hold 32 chars, but
	    it is 34 bytes long so that there are no alignment problems.
		}
    Str32Field = Str32;
	{	
	    QuickTime 3.0:
	    The type StrFileName is used to make MacOS structs work 
	    cross-platform.  For example FSSpec or SFReply previously
	    contained a Str63 field.  They now contain a StrFileName
	    field which is the same when targeting the MacOS but is
	    a 256 char buffer for Win32 and unix, allowing them to
	    contain long file names.
		}
type
	StrFileName = Str63;
	StringPtr = ^Str255;
	StringHandle = ^StringPtr;
	ConstStringPtr = StringPtr;
{GPC-ONLY-START}
	CStringPtr = CString;
{GPC-ONLY-FINISH}
{MW-ONLY-START}
	CStringPtr = Ptr;
{MW-ONLY-FINISH}
{FPC-ONLY-START}
	CStringPtr = PChar;
{FPC-ONLY-FINISH}
	ConstCStringPtr = CStringPtr;
	CStringPtrPtr = ^CStringPtr;
	ConstCStringPtrPtr = ^ConstCStringPtr;
	ConstStr255Param = Str255;
	ConstStr63Param = Str63;
	ConstStr36Param = Str36;
	ConstStr32Param = Str32;
	ConstStr31Param = Str31;
	ConstStr27Param = Str27;
	ConstStr15Param = Str15;
	ConstStrFileNameParam = ConstStr63Param;

{*******************************************************************************

    Process Manager type ProcessSerialNumber (previously in Processes.h)

********************************************************************************}
{ type for unique process identifier }
type
	ProcessSerialNumber = record
		highLongOfPSN: UInt32;
		lowLongOfPSN: UInt32;
	end;
	ProcessSerialNumberPtr = ^ProcessSerialNumber;
{*******************************************************************************

    Quickdraw Types
    
        Point               2D Quickdraw coordinate, range: -32K to +32K
        Rect                Rectangular Quickdraw area
        Style               Quickdraw font rendering styles
        StyleParameter      Style when used as a parameter (historical 68K convention)
        StyleField          Style when used as a field (historical 68K convention)
        CharParameter       Char when used as a parameter (historical 68K convention)
        
    Note:   The original Macintosh toolbox in 68K Pascal defined Style as a SET.  
            Both Style and CHAR occupy 8-bits in packed records or 16-bits when 
            used as fields in non-packed records or as parameters. 
        
********************************************************************************}
type
	Point = record
		case SInt16 of
		0: (
			v: SInt16;
			h: SInt16;
		   );
		1: (
			vh: array [0..1] of SInt16;
			);
	end;
	PointPtr = ^Point;
type
	Rect = record
		case SInt16 of
		0: (
			top: SInt16;
			left: SInt16;
			bottom: SInt16;
			right: SInt16;
		   );
		1: (
			topLeft: Point;
			botRight: Point;
		   );
	end;
	RectPtr = ^Rect;
type
	FixedPoint = record
		x: Fixed;
		y: Fixed;
	end;
	FixedPointPtr = ^FixedPoint;
type
	FixedRect = record
		left: Fixed;
		top: Fixed;
		right: Fixed;
		bottom: Fixed;
	end;
	FixedRectPtr = ^FixedRect;

type
	CharParameter = Char;
const
	normal = 0;
	bold = 1;
	italic = 2;
	underline = 4;
	outline = 8;
	shadow = $10;
	condense = $20;
	extend = $40;

type
	Style = SInt8;
	Style_fix = Style; { used as field type when a record declaration contains a Style field identifier }
	StyleParameter = SInt16;
	StyleField = Style;


{*******************************************************************************

    QuickTime TimeBase types (previously in Movies.h)
    
        TimeValue           Count of units
        TimeScale           Units per second
        CompTimeValue       64-bit count of units (always a struct) 
        TimeValue64         64-bit count of units (long long or struct) 
        TimeBase            An opaque reference to a time base
        TimeRecord          Package of TimeBase, duration, and scale
        
********************************************************************************}
type
	TimeValue = SInt32;
	TimeScale = SInt32;
	TimeScalePtr = ^TimeScale;
	TimeScale_fix = TimeScale; { used as field type when a record declaration contains a TimeScale field identifier }
	CompTimeValue = wide;
	CompTimeValuePtr = ^CompTimeValue;
	TimeValue64 = SInt64;
	TimeValue64Ptr = ^TimeValue64;
	TimeBase = ^SInt32; { an opaque 32-bit type }
	TimeBase_fix = TimeBase; { used as field type when a record declaration contains a TimeBase field identifier }
	TimeBasePtr = ^TimeBase;
	TimeRecord = record
		value: CompTimeValue;                  { units (duration or absolute) }
		scale: TimeScale;                  { units per second }
		base: TimeBase;                   { refernce to the time base }
	end;
	TimeRecordPtr = ^TimeRecord;

{*******************************************************************************

    MacOS versioning structures
    
        VersRec                 Contents of a 'vers' resource
        VersRecPtr              Pointer to a VersRecPtr
        VersRecHndl             Resource Handle containing a VersRec
        NumVersion              Packed BCD version representation (e.g. "4.2.1a3" is 0x04214003)
        UniversalProcPtr        Pointer to classic 68K code or a RoutineDescriptor
        
        ProcHandle              Pointer to a ProcPtr
        UniversalProcHandle     Pointer to a UniversalProcPtr
        
********************************************************************************}
{$ifc TARGET_RT_BIG_ENDIAN}
type
	NumVersion = packed record
{ Numeric version part of 'vers' resource }
		majorRev: UInt8;               {1st part of version number in BCD}
		minorAndBugRev: UInt8;         {2nd & 3rd part of version number share a byte}
		stage: UInt8;                  {stage code: dev, alpha, beta, final}
		nonRelRev: UInt8;              {revision level of non-released version}
	end;
{$elsec}
type
	NumVersion = packed record
{ Numeric version part of 'vers' resource accessable in little endian format }
		nonRelRev: UInt8;              {revision level of non-released version}
		stage: UInt8;                  {stage code: dev, alpha, beta, final}
		minorAndBugRev: UInt8;         {2nd & 3rd part of version number share a byte}
		majorRev: UInt8;               {1st part of version number in BCD}
	end;
{$endc}  {TARGET_RT_BIG_ENDIAN}
	NumVersionPtr = ^NumVersion;

const
{ Version Release Stage Codes }
	developStage = $20;
	alphaStage = $40;
	betaStage = $60;
	finalStage = $80;

type
	NumVersionVariant = record
		case SInt16 of
																		{  NumVersionVariant is a wrapper so NumVersion can be accessed as a 32-bit value  }
		0: (
			parts:				NumVersion;
			);
		1: (
			whole:				UInt32;
			);
	end;
	NumVersionVariantPtr = ^NumVersionVariant;
	NumVersionVariantHandle = ^NumVersionVariantPtr;
	VersRec = record
{ 'vers' resource format }
		numericVersion: NumVersion;         {encoded version number}
		countryCode: SInt16;            {country code from intl utilities}
		shortVersion: Str255;           {version number string - worst case}
		reserved: Str255;               {longMessage string packed after shortVersion}
	end;
	VersRecPtr = ^VersRec;
type
	VersRecHndl = ^VersRecPtr;
{********************************************************************************

    Old names for types
        
********************************************************************************}
type
{MW-ONLY-START}
	Byte = UInt8;
{MW-ONLY-FINISH}
	extended80 = Float80;
	extended80Ptr = ^extended80;
	extended96 = Float96;
	extended96Ptr = ^extended96;
	VHSelect = SInt8;
{********************************************************************************

    Debugger functions
    
********************************************************************************}

{$ifc TARGET_OS_MAC}
{
 *  Debugger()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure Debugger;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  DebugStr()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure DebugStr( const var debuggerMsg: Str255 );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);
{$endc}

{
 *  debugstr()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{$ifc TARGET_CPU_PPC}
{ Only for Mac OS native drivers }
{
 *  SysDebug()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DriverServicesLib 1.0 and later
 }


{
 *  SysDebugStr()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in DriverServicesLib 1.0 and later
 }


{$endc}

{$ifc TARGET_OS_MAC}
{ SADE break points }
{
 *  SysBreak()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure SysBreak;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  SysBreakStr()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure SysBreakStr( const var debuggerMsg: Str255 );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  SysBreakFunc()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure SysBreakFunc( const var debuggerMsg: Str255 );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);
{$endc}





end.
