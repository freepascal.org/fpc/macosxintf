{
     File:       HIToolbox/Notification.h
 
     Contains:   Notification Manager interfaces
 
     Version:    HIToolbox-624~3
 
     Copyright:  � 1989-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit Notification;
interface
uses MacTypes,OSUtils;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

type
	NMRecPtr = ^NMRec;
	NMProcPtr = procedure( nmReqPtr: NMRecPtr );
{GPC-ONLY-START}
	NMUPP = UniversalProcPtr; // should be NMProcPtr
{GPC-ONLY-ELSE}
	NMUPP = NMProcPtr;
{GPC-ONLY-FINISH}
	NMRec = record
		qLink: QElemPtr;                  { next queue entry}
		qType: SInt16;                  { queue type -- ORD(nmType) = 8}
		nmFlags: SInt16;                { reserved}
		nmPrivate: SRefCon;              { reserved}
		nmReserved: SInt16;             { reserved}
		nmMark: SInt16;                 { item to mark in Apple menu}
		nmIcon: Handle;                 { handle to small icon}
		nmSound: Handle;                { handle to sound record}
		nmStr: StringPtr;                  { string to appear in alert}
		nmResp: NMUPP;                 { pointer to response routine}
		nmRefCon: SRefCon;               { for application use}
	end;

{
 *  NewNMUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewNMUPP( userRoutine: NMProcPtr ): NMUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeNMUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeNMUPP( userUPP: NMUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeNMUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeNMUPP( nmReqPtr: NMRecPtr; userUPP: NMUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{$ifc not TARGET_CPU_64}
{
 *  NMInstall()
 *  
 *  Mac OS X threading:
 *    Thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function NMInstall( nmReqPtr: NMRecPtr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  NMRemove()
 *  
 *  Mac OS X threading:
 *    Thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function NMRemove( nmReqPtr: NMRecPtr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
