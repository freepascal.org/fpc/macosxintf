{ CoreGraphics - CGImage.h
 * Copyright (c) 2000-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, November 2005 }
{       Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2007 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGImage;
interface
uses MacTypes,CFBase,CGBase,CGGeometry,CGColorSpace,CGDataProvider;
{$ALIGN POWER}


type
	CGImageRef = ^OpaqueCGImageRef; { an opaque type }
	OpaqueCGImageRef = record end;


type
	CGImageAlphaInfo = SInt32;
const
	kCGImageAlphaNone = 0;               { For example, RGB. }
	kCGImageAlphaPremultipliedLast = 1;  { For example, premultiplied RGBA }
	kCGImageAlphaPremultipliedFirst = 2; { For example, premultiplied ARGB }
	kCGImageAlphaLast = 3;               { For example, non-premultiplied RGBA }
	kCGImageAlphaFirst = 4;              { For example, non-premultiplied ARGB }
	kCGImageAlphaNoneSkipLast = 5;       { For example, RBGX. }
	kCGImageAlphaNoneSkipFirst = 6;      { For example, XRGB. }
	kCGImageAlphaOnly = 7;                { No color data, alpha data only }

const
	kCGBitmapAlphaInfoMask = $1F;
	kCGBitmapFloatComponents = 1 shl 8;
	kCGBitmapByteOrderMask = $7000;
	kCGBitmapByteOrderDefault = 0 shl 12;
	kCGBitmapByteOrder16Little = 1 shl 12;
	kCGBitmapByteOrder32Little = 2 shl 12;
	kCGBitmapByteOrder16Big = 3 shl 12;
	kCGBitmapByteOrder32Big = 4 shl 12;
type
	CGBitmapInfo = UInt32; { Available in MAC OS X 10.4 & later. }

const
{$ifc TARGET_RT_BIG_ENDIAN}
	kCGBitmapByteOrder16Host = kCGBitmapByteOrder16Big;
	kCGBitmapByteOrder32Host = kCGBitmapByteOrder32Big;
{$elsec}
	kCGBitmapByteOrder16Host = kCGBitmapByteOrder16Little;
	kCGBitmapByteOrder32Host = kCGBitmapByteOrder32Little;
{$endc}

{ Return the CFTypeID for CGImageRefs. }

function CGImageGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Create an image. }

function CGImageCreate( width: size_t; height: size_t; bitsPerComponent: size_t; bitsPerPixel: size_t; bytesPerRow: size_t; space: CGColorSpaceRef; bitmapInfo: CGBitmapInfo; provider: CGDataProviderRef; {const} decode: {variable-size-array} CGFloatPtr; shouldInterpolate: CBool; intent: CGColorRenderingIntent ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create an image mask. }

function CGImageMaskCreate( width: size_t; height: size_t; bitsPerComponent: size_t; bitsPerPixel: size_t; bytesPerRow: size_t; provider: CGDataProviderRef; {const} decode: {variable-size-array} CGFloatPtr; shouldInterpolate: CBool ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return a copy of `image'. Only the image structure itself is copied; the
   underlying data is not. }

function CGImageCreateCopy( image: CGImageRef ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Create an image from `source', a data provider of JPEG-encoded data. }

function CGImageCreateWithJPEGDataProvider( source: CGDataProviderRef; {const} decode: {variable-size-array} CGFloatPtr; shouldInterpolate: CBool; intent: CGColorRenderingIntent ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_1, __IPHONE_2_0);

{ Create an image using `source', a data provider for PNG-encoded data. }

function CGImageCreateWithPNGDataProvider( source: CGDataProviderRef; {const} decode: {variable-size-array} CGFloatPtr; shouldInterpolate: CBool; intent: CGColorRenderingIntent ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Create an image using the data contained within the subrectangle `rect'
   of `image'.

   The new image is created by
     1) adjusting `rect' to integral bounds by calling "CGRectIntegral";
     2) intersecting the result with a rectangle with origin (0, 0) and size
        equal to the size of `image';
     3) referencing the pixels within the resulting rectangle, treating the
        first pixel of the image data as the origin of the image.
   If the resulting rectangle is the null rectangle, this function returns
   NULL.

   If W and H are the width and height of image, respectively, then the
   point (0,0) corresponds to the first pixel of the image data; the point
   (W-1, 0) is the last pixel of the first row of the image data; (0, H-1)
   is the first pixel of the last row of the image data; and (W-1, H-1) is
   the last pixel of the last row of the image data.

   The resulting image retains a reference to the original image, so you may
   release the original image after calling this function. }

function CGImageCreateWithImageInRect( image: CGImageRef; rect: CGRect ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Create a new image from `image' masked by `mask', which may be an image
   mask or an image.

   If `mask' is an image mask, then it indicates which parts of the context
   are to be painted with the image when drawn in a context, and which are
   to be masked out (left unchanged). The source samples of the image mask
   determine which areas are painted, acting as an "inverse alpha": if the
   value of a source sample in the image mask is S, then the corresponding
   region in `image' is blended with the destination using an alpha of
   (1-S). (For example, if S is 1, then the region is not painted, while if
   S is 0, the region is fully painted.)

   If `mask' is an image, then it serves as alpha mask for blending the
   image onto the destination. The source samples of `mask' determine which
   areas are painted: if the value of the source sample in mask is S, then
   the corresponding region in image is blended with the destination with an
   alpha of S. (For example, if S is 0, then the region is not painted,
   while if S is 1, the region is fully painted.)

   The parameter `image' may not be an image mask and may not have an image
   mask or masking color associated with it.
  
   If `mask' is an image, then it must be in the DeviceGray color space, may
   not have alpha, and may not itself be masked by an image mask or a
   masking color. }

function CGImageCreateWithMask( image: CGImageRef; mask: CGImageRef ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Create a new image from `image' masked by `components', an array of 2N
   values ( min[1], max[1], ... min[N], max[N] ) where N is the number of
   components in color space of `image'. Any image sample with color value
   (c[1], ... c[N]) where min[i] <= c[i] <= max[i] for 1 <= i <= N is masked
   out (that is, not painted).

   Each value in `components' must be a valid image sample value: if `image'
   has integral pixel components, then each value of must be in the range
   [0..2**bitsPerComponent - 1] (where `bitsPerComponent' is the number of
   bits/component of `image'); if `image' has floating-point pixel
   components, then each value may be any floating-point number which is a
   valid color component.

   The parameter `image' may not be an image mask, and may not already have
   an image mask or masking color associated with it. }

function CGImageCreateWithMaskingColors( image: CGImageRef; {const} components: {variable-size-array} CGFloatPtr ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Create a copy of `image', replacing the image's color space with `space'.
   Returns NULL if `image' is an image mask, or if the number of components
   of `space' isn't the same as the number of components of the color space
   of `image'. }

function CGImageCreateCopyWithColorSpace( image: CGImageRef; space: CGColorSpaceRef ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Equivalent to `CFRetain(image)'. }

function CGImageRetain( image: CGImageRef ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRelease(image)'. }

procedure CGImageRelease( image: CGImageRef );
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `image' is an image mask, false otherwise. }

function CGImageIsMask( image: CGImageRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the width of `image'. }

function CGImageGetWidth( image: CGImageRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the height of `image'. }

function CGImageGetHeight( image: CGImageRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the number of bits/component of `image'. }

function CGImageGetBitsPerComponent( image: CGImageRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the number of bits/pixel of `image'. }

function CGImageGetBitsPerPixel( image: CGImageRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the number of bytes/row of `image'. }

function CGImageGetBytesPerRow( image: CGImageRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the color space of `image', or NULL if `image' is an image
   mask. }

function CGImageGetColorSpace( image: CGImageRef ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the alpha info of `image'. }

function CGImageGetAlphaInfo( image: CGImageRef ): CGImageAlphaInfo;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the data provider of `image'. }

function CGImageGetDataProvider( image: CGImageRef ): CGDataProviderRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the decode array of `image'. }

function CGImageGetDecode( image: CGImageRef ): CGFloatPtr;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the interpolation parameter of `image'. }

function CGImageGetShouldInterpolate( image: CGImageRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the rendering intent of `image'. }

function CGImageGetRenderingIntent( image: CGImageRef ): CGColorRenderingIntent;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the bitmap info of `image'. }

function CGImageGetBitmapInfo( image: CGImageRef ): CGBitmapInfo;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);


end.
