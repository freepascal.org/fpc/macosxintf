{	MDSchema.h
	Copyright (c) 2003-2004, Apple Computer, Inc. All rights reserved.
}
unit MDSchema;
interface
uses MacTypes,CFBase,CFString,CFArray,CFDictionary,MDItem;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{!
	@header MDSchema

	Functions in MDSchema return meta data about attributes, for example
        the type of an attribute, and a localized string for an attribute that is
        sutable to display to a user.
}

{!
	@function MDSchemaCopyAttributesForContentType
        Returns an dictionary attributes to display or show the
                user for a given UTI type. This function does not walk up the
                UTI hiearchy and perform a union of the information.
        @param utiType the UTI type to be interrogated.
        @result A CFDictionaryRef with keys ==  to kMDAttributeDisplayValues etc..

}
function MDSchemaCopyAttributesForContentType( contentTypeUTI: CFStringRef ): CFDictionaryRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
        @function MDSchemaCopyMetaAttributesForAttribute
        Returns an dictionary of the meta attributes of attribute
        @param name the attribute whose schema you are interested in.
        @result A CFDictionary of the description of the attribute.
}
function MDSchemaCopyMetaAttributesForAttribute( name: CFStringRef ): CFDictionaryRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
        @function MDSchemaCopyAllAttributes
        Returns an array of all of the attributes defined in the schema
        @result A CFArray of the attribute names.
}
function MDSchemaCopyAllAttributes: CFArrayRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
        @function MDSchemaCopyDisplayNameForAttribute
        Returns the localized name of an attribute
        @param name the attribute whose localization you are interested in
        @result the localized name of the passed in attribute, or NULL if there is
                 no localized name is avaliable.
}
function MDSchemaCopyDisplayNameForAttribute( name: CFStringRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
        @function MDSchemaCopyDisplayDescriptionForAttribute
        Returns the localized description of an attribute.
        @param name the attribute whose localization you are interested in
        @result the localized description of the passed in attribute, or NULL if there is
                 no localized description is avaliable.
}
function MDSchemaCopyDisplayDescriptionForAttribute( name: CFStringRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

    { Returned by MDSchemaCopyAttributesForContentType }
const kMDAttributeDisplayValues: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
                                                                { Value == CFArray of CFString attribute names  or
                                                                 * NULL if the type  is not known by the system
                                                                 }
const kMDAttributeAllValues: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;     { Value == CFArray of CFString attribute names  or
                                                                 * NULL if the type  is not known by the system
                                                                 }

const kMDAttributeReadOnlyValues: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
                                                                 { Value == CFArray of CFString attribute names  or
                                                                  * NULL if the type  is not known by the system
                                                                  }

const kMDExporterAvaliable: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
                                                                 { Value == CFBoolean
                                                                  * indicates if an exporter is avaliable for this
                                                                  * uti type
                                                                  }

{ Keys in the dictionary returned from the MDSchemaCopyMetaAttributesForAttribute call }
const kMDAttributeName: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;           { Value == name of attribute (CFStringRef) }
const kMDAttributeType: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;           { Value == type of Attribute (CFNumberRef, CFTypeID) }
const kMDAttributeMultiValued: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;    { Value == if multivalued (CFBooleanRef) }

{$endc} {TARGET_OS_MAC}

end.
