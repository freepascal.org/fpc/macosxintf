{
 *  AXRoleConstants.h
 *  HIServices
 *
 *  Created by John Louch on Wed Feb 25 2004.
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }

{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }

unit AXRoleConstants;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{ Roles                                                                                }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}

{
	Every role offers a variety of attributes. There are some attributes that must be
	supported by every element, regardless of role. Other attributes will be supported
	by an element if/when appropriate data is supplied by the application. These
	attributes' meanings and values are generally obvious. In order to save space, the
	following attributes are not listed in the role documentation unless the role
	handles them in a special fashion:
	
		AXRole
		AXRoleDescription
		AXDescription
		AXHelp
		AXParent
		AXChildren
		AXWindow
		AXTopLevelUIElement
		AXEnabled
		AXSize
		AXPosition
	
	Every attribute supported by a given role may have one or more symbols after its
	name:
		w means the attribute is writable.
		o means it is an optional attribute that doesn't necessarily need to be
			supported by all elements with that role.

	TBD:
		Add a general section answering the following questions:
			When and why would a developer create a new role?
			When and why would a developer create a new subrole?
		Add a Quick Reference section, like the one at the top of the attributes.
}

// standard roles
const kAXApplicationRole = CFSTR( 'AXApplication' );
const kAXSystemWideRole = CFSTR( 'AXSystemWide' );
const kAXWindowRole = CFSTR( 'AXWindow' );
const kAXSheetRole = CFSTR( 'AXSheet' );
const kAXDrawerRole = CFSTR( 'AXDrawer' );
const kAXGrowAreaRole = CFSTR( 'AXGrowArea' );
const kAXImageRole = CFSTR( 'AXImage' );
const kAXUnknownRole = CFSTR( 'AXUnknown' );
const kAXButtonRole = CFSTR( 'AXButton' );
const kAXRadioButtonRole = CFSTR( 'AXRadioButton' );
const kAXCheckBoxRole = CFSTR( 'AXCheckBox' );
const kAXPopUpButtonRole = CFSTR( 'AXPopUpButton' );
const kAXMenuButtonRole = CFSTR( 'AXMenuButton' );
const kAXTabGroupRole = CFSTR( 'AXTabGroup' );
const kAXTableRole = CFSTR( 'AXTable' );
const kAXColumnRole = CFSTR( 'AXColumn' );
const kAXRowRole = CFSTR( 'AXRow' );


{
	kAXOutlineRole
	
	An element that contains row-based data. It may use disclosure triangles to manage the
	display of hierarchies within the data. It may arrange each row's data into columns and
	offer a header button above each column. The best example is the list view in a Finder
	window or Open/Save dialog.

	Outlines are typically children of AXScrollAreas, which manages the horizontal and/or
	vertical scrolling for the outline. Outlines are expected to follow certain conventions
	with respect to their hierarchy of sub-elements. In particular, if the outline uses
	columns, the data should be accessible via either rows or columns. Thus, the data in a
	given cell will be represented as two diffrent elements. Here's a hierarchy for a
	typical outline:
	
		AXScrollArea (parent of the outline)
			AXScrollBar (if necessary, horizontal)
			AXScrollBar (if necessary, vertical)
			AXOutline
				AXGroup (header buttons, optional)
					AXButton, AXMenuButton, or <Varies> (header button)
					...
				AXRow (first row)
					AXStaticText (just one possible example)
					AXButton (just another possible example)
					AXTextField (ditto)
					AXCheckBox (ditto)
				AXRow (as above)
				...
				AXColumn (first column)
					AXStaticText (assumes the first column displays text)
					AXStaticText
					...
				AXColumn (second column)
					AXButton (assumes the second column displays buttons)
					AXButton
					...
				...
				
    Attributes:
        AXFocused (w)
        AXRows - Array of subset of AXChildren that are rows.
        AXVisibleRows - Array of subset of AXRows that are visible.
        AXSelectedRows (w) Array of subset of AXRows that are selected.
        AXColumns - Array of subset of children that are columns.
        AXVisibleColumns - Array of subset of columns that are visible.
        AXSelectedColumns (o) Array of subset of columns that are selected.
        AXHeader (o) The AXGroup element that contains the header buttons.
}
const kAXOutlineRole = CFSTR( 'AXOutline' );


{
	kAXBrowserRole
	
	An element that contains columns of hierarchical data. Examples include the column view
	in Finder windows and Open/Save dialogs. Carbon's Data Browser in column view mode
	represents itself as an AXBrowser. Cocoa's NSBrowser represents itself as an AXBrowser.
	
	Browser elements are expected to have a particular hierarchy of sub-elements within it.
	In particular, the child of an AXBrowser must be an AXScrollArea that manages the
	horizontal scrolling. The horizontal AXScrollArea must include a child for each column
	the interface displays. Columns can be any role that makes sense. Typically, columns
	are vertical AXScrollAreas with AXList children. Here's a hierarchy for a typical
	browser:
	
		AXBrowser
			AXScrollArea (manages the horizontal scrolling)
				AXScrollBar (horizontal scroll bar)
				AXScrollArea (first column)
					AXScrollBar (column's vertical scroll bar)
					AXList (column content is typically a list, but it could be another role)
						<Varies> (cell)
						...
						<Varies> (cell)
				AXScrollArea (second column)
					...
				AXScrollArea (third column)
					...
				AXGroup (preview column)
					...
	
	Attributes:
		AXFocused (w)
		AXColumns - Array of the grandchild column elements, which are typically
			of the AXScrollArea role.
		AXVisibleColumns - Array of the subset of elements in the AXColumns array
			that are currently visible.
		AXColumnTitles (o)
		AXHorizontalScrollBar - The horizontal AXScrollBar of the browser's child
			AXScrollArea.
}
const kAXBrowserRole = CFSTR( 'AXBrowser' );
const kAXScrollAreaRole = CFSTR( 'AXScrollArea' );
const kAXScrollBarRole = CFSTR( 'AXScrollBar' );
const kAXRadioGroupRole = CFSTR( 'AXRadioGroup' );
const kAXListRole = CFSTR( 'AXList' );
const kAXGroupRole = CFSTR( 'AXGroup' );
const kAXValueIndicatorRole = CFSTR( 'AXValueIndicator' );
const kAXComboBoxRole = CFSTR( 'AXComboBox' );
const kAXSliderRole = CFSTR( 'AXSlider' );
const kAXIncrementorRole = CFSTR( 'AXIncrementor' );
const kAXBusyIndicatorRole = CFSTR( 'AXBusyIndicator' );
const kAXProgressIndicatorRole = CFSTR( 'AXProgressIndicator' );
const kAXRelevanceIndicatorRole = CFSTR( 'AXRelevanceIndicator' );
const kAXToolbarRole = CFSTR( 'AXToolbar' );
const kAXDisclosureTriangleRole = CFSTR( 'AXDisclosureTriangle' );


const kAXTextFieldRole = CFSTR( 'AXTextField' );
const kAXTextAreaRole = CFSTR( 'AXTextArea' );
const kAXStaticTextRole = CFSTR( 'AXStaticText' );

const kAXMenuBarRole = CFSTR( 'AXMenuBar' );
const kAXMenuBarItemRole = CFSTR( 'AXMenuBarItem' );
const kAXMenuRole = CFSTR( 'AXMenu' );
const kAXMenuItemRole = CFSTR( 'AXMenuItem' );

const kAXSplitGroupRole = CFSTR( 'AXSplitGroup' );
const kAXSplitterRole = CFSTR( 'AXSplitter' );
const kAXColorWellRole = CFSTR( 'AXColorWell' );

const kAXTimeFieldRole = CFSTR( 'AXTimeField' );
const kAXDateFieldRole = CFSTR( 'AXDateField' );

const kAXHelpTagRole = CFSTR( 'AXHelpTag' );

const kAXMatteRole = CFSTR( 'AXMatteRole' );

const kAXDockItemRole = CFSTR( 'AXDockItem' );

const kAXRulerRole = CFSTR( 'AXRuler' );
const kAXRulerMarkerRole = CFSTR( 'AXRulerMarker' );

const kAXGridRole = CFSTR( 'AXGrid' );

const kAXLevelIndicatorRole = CFSTR( 'AXLevelIndicator' );

const kAXCellRole = CFSTR( 'AXCell' );

const kAXLayoutAreaRole = CFSTR( 'AXLayoutArea' );
const kAXLayoutItemRole = CFSTR( 'AXLayoutItem' );
const kAXHandleRole = CFSTR( 'AXHandle' );

const kAXPopoverRole = CFSTR( 'AXPopover' );

{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{ Subroles                                                                                }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}

// standard subroles
const kAXCloseButtonSubrole = CFSTR( 'AXCloseButton' );
const kAXMinimizeButtonSubrole = CFSTR( 'AXMinimizeButton' );
const kAXZoomButtonSubrole = CFSTR( 'AXZoomButton' );
const kAXToolbarButtonSubrole = CFSTR( 'AXToolbarButton' );
const kAXFullScreenButtonSubrole = CFSTR( 'AXFullScreenButton' );
const kAXSecureTextFieldSubrole = CFSTR( 'AXSecureTextField' );
const kAXTableRowSubrole = CFSTR( 'AXTableRow' );
const kAXOutlineRowSubrole = CFSTR( 'AXOutlineRow' );
const kAXUnknownSubrole = CFSTR( 'AXUnknown' );

// new subroles
const kAXStandardWindowSubrole = CFSTR( 'AXStandardWindow' );
const kAXDialogSubrole = CFSTR( 'AXDialog' );
const kAXSystemDialogSubrole = CFSTR( 'AXSystemDialog' );
const kAXFloatingWindowSubrole = CFSTR( 'AXFloatingWindow' );
const kAXSystemFloatingWindowSubrole = CFSTR( 'AXSystemFloatingWindow' );
const kAXIncrementArrowSubrole = CFSTR( 'AXIncrementArrow' );
const kAXDecrementArrowSubrole = CFSTR( 'AXDecrementArrow' );
const kAXIncrementPageSubrole = CFSTR( 'AXIncrementPage' );
const kAXDecrementPageSubrole = CFSTR( 'AXDecrementPage' );
const kAXSortButtonSubrole = CFSTR( 'AXSortButton' );
const kAXSearchFieldSubrole = CFSTR( 'AXSearchField' );
const kAXTimelineSubrole = CFSTR( 'AXTimeline' );
const kAXRatingIndicatorSubrole = CFSTR( 'AXRatingIndicator' );
const kAXContentListSubrole = CFSTR( 'AXContentList' );
const kAXDefinitionListSubrole = CFSTR( 'AXDefinitionList' );

// dock subroles
const kAXApplicationDockItemSubrole = CFSTR( 'AXApplicationDockItem' );
const kAXDocumentDockItemSubrole = CFSTR( 'AXDocumentDockItem' );
const kAXFolderDockItemSubrole = CFSTR( 'AXFolderDockItem' );
const kAXMinimizedWindowDockItemSubrole = CFSTR( 'AXMinimizedWindowDockItem' );
const kAXURLDockItemSubrole = CFSTR( 'AXURLDockItem' );
const kAXDockExtraDockItemSubrole = CFSTR( 'AXDockExtraDockItem' );
const kAXTrashDockItemSubrole = CFSTR( 'AXTrashDockItem' );
const kAXSeparatorDockItemSubrole = CFSTR( 'AXSeparatorDockItem' );
const kAXProcessSwitcherListSubrole = CFSTR( 'AXProcessSwitcherList' );

{$endc} {TARGET_OS_MAC}

end.
