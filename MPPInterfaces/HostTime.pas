{==================================================================================================
     File:       CoreAudio/HostTime.h

     Contains:   Routines for accessing the host's time base

     Copyright:  (c) 1985-2010 by Apple, Inc., all rights reserved.

     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:

                     http://developer.apple.com/bugreporter/

==================================================================================================}
{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HostTime;
interface
uses MacTypes,CoreAudioTypes;
{$ALIGN POWER}


//==================================================================================================
//#pragma mark    Theory of Operation

{!
    @header HostTime
    This collection of functions provides access to the host's time base. It also provides
    discriptive information about the time base and translations to and from nanoseconds.
}


{!
    @functiongroup  HostTime
}

{!
    @function       AudioGetCurrentHostTime
    @abstract       Gets the current host time.
    @result         A UInt64 containing the current host time.
}
function AudioGetCurrentHostTime: UInt64;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{!
    @function       AudioGetHostClockFrequency
    @abstract       Gets the number of ticks per second in the host time base.
    @result         A Float64 containing the number of ticks per second in the host time base.
}
function AudioGetHostClockFrequency: Float64;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{!
    @function       AudioGetHostClockMinimumTimeDelta
    @abstract       Gets the smallest number of ticks that two succeeding values will ever differ.
                    by.
    @result         A UInt32 containing the smallest number of ticks that two succeeding values will
                    ever differ.
}
function AudioGetHostClockMinimumTimeDelta: UInt32;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{!
    @function       AudioConvertHostTimeToNanos
    @abstract       Convert the given host time into a time in nanoseconds.
    @param          inHostTime
                        A UInt64 containing the host time to convert.
    @result         A UInt64 containining the converted host time.
}
function AudioConvertHostTimeToNanos( inHostTime: UInt64 ): UInt64;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{!
    @function       AudioConvertNanosToHostTime
    @abstract       Convert the given nanosecond time into a host time.
    @param          inNanos
                        A UInt64 containing the nanosecond time to convert.
    @result         A UInt64 containining the converted nanosecond time.
}
function AudioConvertNanosToHostTime( inNanos: UInt64 ): UInt64;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

//==================================================================================================


end.
