{
     File:       ATS/ATSFont.h
 
     Contains:   Public interface to the font access and data management functions of ATS.
 
     Version:    ATS
 
     Copyright:  � 2000-2012 by Apple Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}

{  Pascal Translation Updated: Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ATSFont;
interface
uses MacTypes,CFBase,CFRunLoop,CFPropertyList,ATSTypes,CFString,CFURL,Files,TextCommon,SFNTTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}


const
	kATSOptionFlagsDefault = 0;
	kATSOptionFlagsComposeFontPostScriptName = 1 shl 0; { ATSFontGetPostScriptName }
	kATSOptionFlagsUseDataForkAsResourceFork = 1 shl 8; { ATSFontActivateFromFileSpecification }
	kATSOptionFlagsUseResourceFork = 2 shl 8;
	kATSOptionFlagsUseDataFork = 3 shl 8;

	kATSIterationCompleted		= -980;
	kATSInvalidFontFamilyAccess	= -981;
	kATSInvalidFontAccess		= -982;
	kATSIterationScopeModified	= -983;
	kATSInvalidFontTableAccess	= -984;
	kATSInvalidFontContainerAccess = -985;
    kATSInvalidGlyphAccess        = -986;


{ Activation Option Flags }
type
	ATSFontContext = UInt32;
const
	kATSFontContextUnspecified = 0;
	kATSFontContextGlobal = 1;
	kATSFontContextLocal = 2;

const
	kATSOptionFlagsActivateDisabled = $00000001 shl 5; { Used by activate to activate fonts in the disabled state }
	kATSOptionFlagsProcessSubdirectories = $00000001 shl 6; { Used by activation/deactivation & iteration }
	kATSOptionFlagsDoNotNotify = $00000001 shl 7; { Do not notify after global activation/deactivation }
	kATSOptionFlagsRecordPersistently = $00000001 shl 18; { Used by activated/deactivation to record/remove font references in persistent store to be remembered at next login }

{ Iteration Option Flags }
const
	kATSOptionFlagsIterateByPrecedenceMask = $00000001 shl 5; { Fonts returned from highest to lowest precedece }
	kATSOptionFlagsIncludeDisabledMask = $00000001 shl 7; { Disabled Fonts will show up in iteration, also valid for ATSFontFindFromContainer }
	kATSOptionFlagsIterationScopeMask = $00000007 shl 12; { Mask option bits 12-14 for iteration scopes }
	kATSOptionFlagsDefaultScope = $00000000 shl 12;
	kATSOptionFlagsUnRestrictedScope = $00000001 shl 12;
	kATSOptionFlagsRestrictedScope = $00000002 shl 12;

type
	ATSFontFamilyApplierFunction = function( iFamily: ATSFontFamilyRef; iRefCon: univ Ptr ): OSStatus;
	ATSFontApplierFunction = function( iFont: ATSFontRef; iRefCon: univ Ptr ): OSStatus;
	ATSFontFamilyIterator_ = record end;
	ATSFontFamilyIterator = ^ATSFontFamilyIterator_;
	ATSFontIterator_ = record end;
	ATSFontIterator = ^ATSFontIterator_;
const
	kATSFontFilterCurrentVersion = 0;

type
	ATSFontFilterSelector = SInt32;
const
	kATSFontFilterSelectorUnspecified = 0;
	kATSFontFilterSelectorGeneration = 3;
	kATSFontFilterSelectorFontFamily = 7;
	kATSFontFilterSelectorFontFamilyApplierFunction = 8;
	kATSFontFilterSelectorFontApplierFunction = 9;
	kATSFileReferenceFilterSelector = 10;

type
	ATSFontFilterPtr = ^ATSFontFilter;
	ATSFontFilter = record
		version: UInt32;
		filterSelector: ATSFontFilterSelector;
		case SInt16 of
		0: (
			generationFilter:	ATSGeneration;
			);
		1: (
			fontFamilyFilter:	ATSFontFamilyRef;
			);
		2: (
			fontFamilyApplierFunctionFilter: ATSFontFamilyApplierFunction;
			);
		3: (
			fontApplierFunctionFilter: ATSFontApplierFunction;
			);
		4: (
			fontFileRefFilter: {const} FSRefPtr;
			);
	end;
{ Notification related }

	ATSFontNotificationRef_ = record end;
	ATSFontNotificationRef = ^ATSFontNotificationRef_;
	ATSFontNotificationRefPtr = ^ATSFontNotificationRef;
	ATSFontNotificationInfoRef_ = record end;
	ATSFontNotificationInfoRef = ^ATSFontNotificationInfoRef_;
	ATSFontNotificationInfoRefPtr = ^ATSFontNotificationInfoRef;

{
 *  ATSFontNotifyOption
 *  
 *  Discussion:
 *    Options used with ATSFontNotificationSubscribe.  Any of the
 *    options that follow may be used together in order to alter the
 *    default behavior of ATS notifications.
 }
type
	ATSFontNotifyOption = SInt32;
const
{
   * Default behavior of ATSFontNotificationSubscribe.
   }
	kATSFontNotifyOptionDefault = 0;

  {
   * Normally applications will only receive ATS notifications while in
   * the foreground.   If suspended, the notification will be delivered
   * when then application comes to the foreground.  This is the
   * default.  You should set this option if you are a server or tool
   * that performs font management functions and require immediate
   * notification when something changes.
   }
	kATSFontNotifyOptionReceiveWhileSuspended = 1 shl 0;


{
 *  ATSFontNotifyAction
 *  
 *  Discussion:
 *    Used with ATSFontNotify.   The following is a list of actions you
 *    might wish the ATS server to perform and notify clients if
 *    appropriate.
 }
type
	ATSFontNotifyAction = SInt32;
const
{
   * Used after a batch (de)activation of fonts occurs.   Typically the
   * caller has exercised multiple global (De)Activation calls with the
   * kATSOptionFlagsDoNotNotify set. Once all calls are completed, one
   * may use ATSFontNotify with this action to ask ATS to notify all
   * clients.
   }
	kATSFontNotifyActionFontsChanged = 1;

  {
   * The ATS system with the help of the Finder keeps track of changes
   * to any of the font directories in the system domains ( System,
   * Local, Network, User, & Classic). However, one may wish to
   * add/remove fonts to these locations programmatically. This action
   * is used to let ATS server to rescan these directories and post
   * notifications if necessary.
   }
	kATSFontNotifyActionDirectoriesChanged = 2;


{
 *  ATSNotificationCallback
 *  
 *  Discussion:
 *    Callback delivered for ATS notifications.
 *  
 *  Parameters:
 *    
 *    info:
 *      Parameter is placed here for future improvements.  Initially
 *      the contents of this parameter will be NULL.
 *    
 *    refCon:
 *      User data/state to be supplied to callback function
 }
type
	ATSNotificationCallback = procedure( info: ATSFontNotificationInfoRef; refCon: univ Ptr );
{ ----------------------------------------------------------------------------------------- }
{ Font container                                                                            }
{ ----------------------------------------------------------------------------------------- }
{
 *  ATSGetGeneration()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{
  Register for kCTFontManagerRegisteredFontsChangedNotification notifications
}
function ATSGetGeneration: ATSGeneration;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{$ifc not TARGET_CPU_64}
{
 *  ATSFontActivateFromFileSpecification()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use ATSFontActivateFromFileReference instead.
 *  
 *  Summary:
 *    Activates one or more fonts from a file specification.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Parameters:
 *    
 *    iFile:
 *      A pointer to the file specification that specifies the name and
 *      location of a file or directory that contains the font data you
 *      want to activate.
 *    
 *    iContext:
 *      A value that specifies the context of the activated font.
 *    
 *    iFormat:
 *      A value that represents the format identifier of the font. Pass
 *      kATSFontFormatUnspecified as the system automatically
 *      determines the format of the font.
 *    
 *    iReserved:
 *      This parameter is currently reserved for future use, so you
 *      should pass NULL.
 *    
 *    iOptions:
 *      An options flag.  See developer documentation for appropriate
 *      flags.
 *    
 *    oContainer:
 *      On output, a reference to the font container that is activated
 *      from the file specification. You need this reference when you
 *      deactivate the font by calling the function ATSFontDeactivate.
 *  
 *  Result:
 *    noErr Activated successfully
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{
  Use CTFontManagerRegisterFontsForURL() or CTFontManagerRegisterFontsForURLs()
}
function ATSFontActivateFromFileSpecification( const var iFile: FSSpec; iContext: ATSFontContext; iFormat: ATSFontFormat; iReserved: univ Ptr; iOptions: ATSOptionFlags; var oContainer: ATSFontContainerRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc}  { TARGET_CPU_64 }

{
 *  ATSFontActivateFromFileReference()
 *  
 *  Summary:
 *    Activates one or more fonts from a file reference.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    iFile:
 *      A pointer to the file specification that specifies the name and
 *      location of a file or directory that contains the font data you
 *      want to activate.
 *    
 *    iContext:
 *      A value that specifies the context of the activated font.
 *    
 *    iFormat:
 *      A value that represents the format identifier of the font. Pass
 *      kATSFontFormatUnspecified as the system automatically
 *      determines the format of the font.
 *    
 *    iRefCon:
 *      This parameter is currently reserved for future use, so you
 *      should pass NULL.
 *    
 *    iOptions:
 *      An options flag.  See developer documentation for appropriate
 *      flags.
 *    
 *    oContainer:
 *      On output, a reference to the font container that is activated
 *      from the file specification. You need this reference when you
 *      deactivate the font by calling the function ATSFontDeactivate.
 *  
 *  Result:
 *    noErr Activated successfully
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{
   Use CTFontManagerRegisterFontsForURL() or CTFontManagerRegisterFontsForURLs()
}
function ATSFontActivateFromFileReference( const var iFile: FSRef; iContext: ATSFontContext; iFormat: ATSFontFormat; iRefCon: univ Ptr; iOptions: ATSOptionFlags; var oContainer: ATSFontContainerRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontActivateFromMemory()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{
  Use CTFontManagerRegisterFontForData() or CGFontCreateWithDataProvider() w/ CTFontManagerRegisterGraphicsFont()
}
function ATSFontActivateFromMemory( iData: LogicalAddress; iLength: ByteCount; iContext: ATSFontContext; iFormat: ATSFontFormat; iReserved: univ Ptr; iOptions: ATSOptionFlags; var oContainer: ATSFontContainerRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontDeactivate()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontManagerUnregisterFontsForURL() or CTFontManagerUnregisterFontsForURLs()" }
function ATSFontDeactivate( iContainer: ATSFontContainerRef; iRefCon: univ Ptr; iOptions: ATSOptionFlags ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontGetContainerFromFileReference()
 *  
 *  Summary:
 *    Get the font container reference associated with an activated
 *    file reference.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    iFile:
 *      A pointer to the valid file reference that specificies the
 *      activated font file for which to get the container.
 *    
 *    iContext:
 *      The context that the font file is accessible too. If
 *      kATSFontContextGlobal is specified this function will return
 *      only a valid font container reference that was activated in
 *      kATSFontContextGlobal. If kATSFontContextDefault or
 *      kATSFontContextLocal is used, the container returned will
 *      adhere to precedence rules. In which case a container activated
 *      in kATSFontContextLocal will be preferred over one activated in
 *      kATSFontContextGlobal.
 *    
 *    iOptions:
 *      An options flag.  See developer documentation for appropriate
 *      flags.
 *    
 *    oContainer:
 *      On output, a reference to the font container representing the
 *      file reference activated in the specified context. On error or
 *      for a file that is not activated, this will be set to
 *      kATSFontContainerRefUnspecified.
 *  
 *  Result:
 *    paramErr One or more parameters are invalid.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyAttribute() with kCTFontURLAttribute." }
function ATSFontGetContainerFromFileReference( const var iFile: FSRef; iContext: ATSFontContext; iOptions: ATSOptionFlags; var oContainer: ATSFontContainerRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontGetContainer()
 *  
 *  Summary:
 *    Gets the font container reference for the font.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    iFont:
 *      The font reference.
 *    
 *    iOptions:
 *      An options flag.  See developer documentation for appropriate
 *      flags.
 *    
 *    oContainer:
 *      On output, a reference to the font container that was used to
 *      activate the font reference. On error this will be set to
 *      kATSFontContainerRefUnspecified.
 *  
 *  Result:
 *    kATSInvalidFontContainerAccess The font container is invalid.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyAttribute() with kCTFontURLAttribute." }
function ATSFontGetContainer( iFont: ATSFontRef; iOptions: ATSOptionFlags; var oContainer: ATSFontContainerRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontSetEnabled()
 *  
 *  Summary:
 *    Sets a font's state to enabled or disabled.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    iFont:
 *      The font reference.
 *    
 *    iOptions:
 *      An options flag.  See developer documentation for appropriate
 *      flags.
 *    
 *    iEnabled:
 *      The state to set the font to. True for enabled, false for
 *      disabled.
 *  
 *  Result:
 *    kATSInvalidFontAccess The font reference is invalid in the
 *    current application context.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontManagerEnableFontDescriptors()" }
function ATSFontSetEnabled( iFont: ATSFontRef; iOptions: ATSOptionFlags; iEnabled: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontIsEnabled()
 *  
 *  Summary:
 *    Returns true if the font is enabled.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    iFont:
 *      The font reference
 *  
 *  Result:
 *    false The font is disabled.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyAttribute() with kCTFontEnabledAttribute" }
function ATSFontIsEnabled( iFont: ATSFontRef ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{ ----------------------------------------------------------------------------------------- }
{ Font family                                                                               }
{ ----------------------------------------------------------------------------------------- }
{
 *  ATSFontFamilyApplyFunction()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontManagerCopyAvailableFontFamilyNames()" }
function ATSFontFamilyApplyFunction( iFunction: ATSFontFamilyApplierFunction; iRefCon: univ Ptr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontFamilyIteratorCreate()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontManagerCopyAvailableFontFamilyNames()" }
function ATSFontFamilyIteratorCreate( iContext: ATSFontContext; {const} iFilter: ATSFontFilterPtr { can be NULL }; iRefCon: univ Ptr; iOptions: ATSOptionFlags; var ioIterator: ATSFontFamilyIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontFamilyIteratorRelease()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function ATSFontFamilyIteratorRelease( var ioIterator: ATSFontFamilyIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontFamilyIteratorReset()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function ATSFontFamilyIteratorReset( iContext: ATSFontContext; {const} iFilter: ATSFontFilterPtr { can be NULL }; iRefCon: univ Ptr; iOptions: ATSOptionFlags; var ioIterator: ATSFontFamilyIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontFamilyIteratorNext()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function ATSFontFamilyIteratorNext( iIterator: ATSFontFamilyIterator; var oFamily: ATSFontFamilyRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontFamilyFindFromName()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontDescriptorCreateMatchingFontDescriptors() with kCTFontFamilyNameAttribute" }
function ATSFontFamilyFindFromName( iName: CFStringRef; iOptions: ATSOptionFlags ): ATSFontFamilyRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontFamilyGetGeneration()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Register for kCTFontManagerRegisteredFontsChangedNotification notifications" }
function ATSFontFamilyGetGeneration( iFamily: ATSFontFamilyRef ): ATSGeneration;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontFamilyGetName()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyFamilyName()" }
function ATSFontFamilyGetName( iFamily: ATSFontFamilyRef; iOptions: ATSOptionFlags; var oName: CFStringRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontFamilyGetEncoding()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontGetStringEncoding()" }
function ATSFontFamilyGetEncoding( iFamily: ATSFontFamilyRef ): TextEncoding;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{ ----------------------------------------------------------------------------------------- }
{ Font                                                                                      }
{ ----------------------------------------------------------------------------------------- }
{
 *  ATSFontApplyFunction()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCollectionCreateFromAvailableFonts()" }
function ATSFontApplyFunction( iFunction: ATSFontApplierFunction; iRefCon: univ Ptr ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontIteratorCreate()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCollectionCreateFromAvailableFonts()" }
function ATSFontIteratorCreate( iContext: ATSFontContext; {const} iFilter: ATSFontFilterPtr { can be NULL }; iRefCon: univ Ptr; iOptions: ATSOptionFlags; var ioIterator: ATSFontIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontIteratorRelease()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function ATSFontIteratorRelease( var ioIterator: ATSFontIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontIteratorReset()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function ATSFontIteratorReset( iContext: ATSFontContext; {const} iFilter: ATSFontFilterPtr { can be NULL }; iRefCon: univ Ptr; iOptions: ATSOptionFlags; var ioIterator: ATSFontIterator ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontIteratorNext()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function ATSFontIteratorNext( iIterator: ATSFontIterator; var oFont: ATSFontRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontFindFromName()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCreateWithName()" }
function ATSFontFindFromName( iName: CFStringRef; iOptions: ATSOptionFlags ): ATSFontRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontFindFromPostScriptName()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCreateWithName()" }
function ATSFontFindFromPostScriptName( iName: CFStringRef; iOptions: ATSOptionFlags ): ATSFontRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontFindFromContainer()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontDescriptorCreateMatchingFontDescriptors() with kCTFontURLAttribute or use CTFontManagerCreateFontDescriptorsFromURL()" }
function ATSFontFindFromContainer( iContainer: ATSFontContainerRef; iOptions: ATSOptionFlags; iCount: ItemCount; ioArray: {variable-size-array} ATSFontRefPtr; var oCount: ItemCount ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontGetGeneration()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Register for kCTFontManagerRegisteredFontsChangedNotification notifications" }
function ATSFontGetGeneration( iFont: ATSFontRef ): ATSGeneration;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontGetName()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyFullName()" }
function ATSFontGetName( iFont: ATSFontRef; iOptions: ATSOptionFlags; var oName: CFStringRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontGetPostScriptName()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyPostScriptName()" }
function ATSFontGetPostScriptName( iFont: ATSFontRef; iOptions: ATSOptionFlags; var oName: CFStringRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontGetTableDirectory()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyAvailableTables()" }
function ATSFontGetTableDirectory( iFont: ATSFontRef; iBufferSize: ByteCount; ioBuffer: univ Ptr; oSize: ByteCountPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontGetTable()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyTable()" }
function ATSFontGetTable( iFont: ATSFontRef; iTag: FourCharCode; iOffset: ByteOffset; iBufferSize: ByteCount; ioBuffer: univ Ptr; oSize: ByteCountPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontGetHorizontalMetrics()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontGetXHeight(), CTFontGetAscent(), and friends to find a specific metric." }
function ATSFontGetHorizontalMetrics( iFont: ATSFontRef; iOptions: ATSOptionFlags; var oMetrics: ATSFontMetrics ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontGetVerticalMetrics()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontGetXHeight(), CTFontGetAscent(), and friends to find a specific metric." }
function ATSFontGetVerticalMetrics( iFont: ATSFontRef; iOptions: ATSOptionFlags; var oMetrics: ATSFontMetrics ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{ ----------------------------------------------------------------------------------------- }
{ Compatibility                                                                             }
{ ----------------------------------------------------------------------------------------- }
{
 *  ATSFontFamilyFindFromQuickDrawName()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCreateWithQuickdrawInstance()" }
function ATSFontFamilyFindFromQuickDrawName( const var iName: Str255 ): ATSFontFamilyRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontFamilyGetQuickDrawName()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "QuickDraw is deprecated" }
function ATSFontFamilyGetQuickDrawName( iFamily: ATSFontFamilyRef; var oName: Str255 ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{$ifc TARGET_CPU_64}
{
 *  ATSFontGetFileSpecification()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use ATSFontGetFileReference instead.
 *  
 *  Summary:
 *    Obtains the file specification for a font.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Parameters:
 *    
 *    iFont:
 *      A reference to the font whose file specification you want to
 *      obtain.
 *    
 *    oFile:
 *      On output, points to the file specification that specifies the
 *      name and location of a file or directory that contains the font
 *      data specified by the iFont parameter.
 *  
 *  Result:
 *    noErr File specification obtained successfully.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyAttribute() with kCTFontURLAttribute." }
function ATSFontGetFileSpecification( iFont: ATSFontRef; var oFile: ATSFSSpec ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc}	{ TARGET_CPU_64 }

{
 *  ATSFontGetFileReference()
 *  
 *  Summary:
 *    Obtains the file reference for a font.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    iFont:
 *      A reference to the font whose file specification you want to
 *      obtain.
 *    
 *    oFile:
 *      On output, points to the file reference that specifies the name
 *      and location of a file or directory that contains the font data
 *      specified by the iFont parameter.
 *  
 *  Result:
 *    noErr File specification obtained successfully.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontCopyAttribute() with kCTFontURLAttribute." }
function ATSFontGetFileReference( iFont: ATSFontRef; var oFile: FSRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{
 *  ATSFontGetFontFamilyResource()
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
{ "QuickDraw is deprecated" }
function ATSFontGetFontFamilyResource( iFont: ATSFontRef; iBufferSize: ByteCount; ioBuffer: univ Ptr; oSize: ByteCountPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{ ----------------------------------------------------------------------------------------- }
{ Notification                                                                              }
{ ----------------------------------------------------------------------------------------- }
{
 *  ATSFontNotify()
 *  
 *  Summary:
 *    Used to alert ATS that an action which may require notification
 *    to clients has occurred.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Parameters:
 *    
 *    action:
 *      Action that should be taken by the ATS Server
 *    
 *    info:
 *      Any required or optional information that may be required by
 *      the action taken.
 *  
 *  Result:
 *    noErr Action successfully reported paramErr Invalid action passed
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
{ "Register for kCTFontManagerRegisteredFontsChangedNotification notifications" }
function ATSFontNotify( action: ATSFontNotifyAction; info: univ Ptr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_2_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontNotificationSubscribe()
 *  
 *  Summary:
 *    Ask the ATS System to notify caller when certain events have
 *    occurred.  Note that your application must have a CFRunLoop in
 *    order to receive notifications. Any Appkit or Carbon event loop
 *    based application will have one by default.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Parameters:
 *    
 *    callback:
 *      Function that will be called by the ATS system whenever an
 *      event of interest takes place.
 *    
 *    options:
 *      Set the wanted ATSFontNotificationOptions to modify the default
 *      behavior of ATS Notifications.
 *    
 *    iRefcon:
 *      User data/state which will be passed to the callback funtion
 *    
 *    oNotificationRef:
 *      You may use this reference to un-subscribe to this notification.
 *  
 *  Result:
 *    noErr Subscribed successfully paramErr NULL callback was passed.
 *    memFullErr Could not allocate enough memory for internal data
 *    structures.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
{ "Register for kCTFontManagerRegisteredFontsChangedNotification notifications" }
function ATSFontNotificationSubscribe( callback: ATSNotificationCallback; options: ATSFontNotifyOption; iRefcon: univ Ptr { can be NULL }; oNotificationRef: ATSFontNotificationRefPtr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_2_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontNotificationUnsubscribe()
 *  
 *  Summary:
 *    Release subscription and stop receiving notifications for a given
 *    reference.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Parameters:
 *    
 *    notificationRef:
 *      Notification reference for which you want to stop receiving
 *      notifications. Note, if more than one notification has been
 *      requested of ATS, you will still receive notifications on those
 *      requests.
 *  
 *  Result:
 *    noErr Unsubscribed successfully paramErr NULL/invalid
 *    notificationRef passed
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in ApplicationServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
{ "Unregister for kCTFontManagerRegisteredFontsChangedNotification notifications" }
function ATSFontNotificationUnsubscribe( notificationRef: ATSFontNotificationRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_2_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{ ----------------------------------------------------------------------------------------- }
{ Font query message hooks                                                                  }
{ ----------------------------------------------------------------------------------------- }

{
 *  ATSFontQuerySourceContext
 *  
 *  Summary:
 *    A parameter block for client information to be retained by ATS
 *    and passed back to an ATSFontQueryCallback function.
 }
type
	ATSFontQuerySourceContextPtr = ^ATSFontQuerySourceContext;
	ATSFontQuerySourceContext = record
{
   * A 32-bit unsigned integer that indicates the version of this
   * structure. This should be set to 0.
   }
		version: UInt32;

  {
   * A pointer-sized client datum that should be passed back to an
   * ATSFontQueryCallback function.
   }
		refCon: UnivPtr;

  {
   * The callback used to add a retain to the refCon.
   }
		retain: CFAllocatorRetainCallBack;

  {
   * The callback used to remove a retain to the refCon.
   }
		release: CFAllocatorReleaseCallBack;
	end;

{
 *  ATSFontQueryMessageID
 *  
 *  Discussion:
 *    Constants for ATS font query message types.
 }

	ATSFontQueryMessageID = SInt32;
const
{
   * The message ID for a font request query. The data for a message
   * with this ID is a flattened CFDictionaryRef with keys and values
   * as decribed below. A query dictionary may have any or all of these
   * entries.
   }
	kATSQueryActivateFontMessage = FOUR_CHAR_CODE('atsa');


{
 *  ATSFontQueryCallback
 *  
 *  Summary:
 *    Callback for receiving font-related queries from ATS.
 *  
 *  Parameters:
 *    
 *    msgid:
 *      An ATSFontQueryMessageID that identifies the message type.
 *    
 *    data:
 *      A CFPropertyListRef that represents the query. The content is
 *      message type-specific.
 *    
 *    refCon:
 *      A pointer-sized client datum that was optionally provided to
 *      ATSCreateFontQueryRunLoopSource.
 *  
 *  Result:
 *    A CFPropertyListRef that represents the message type-specific
 *    response to the query. May be NULL.
 }
type
	ATSFontQueryCallback = function( msgid: ATSFontQueryMessageID; data: CFPropertyListRef; refCon: univ Ptr ): CFPropertyListRef;
{
 *  ATSCreateFontQueryRunLoopSource()
 *  
 *  Summary:
 *    Creates a CFRunLoopSourceRef that will be used to convey font
 *    queries from ATS.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Jaguar
 *  
 *  Parameters:
 *    
 *    queryOrder:
 *      A CFIndex that specifies the priority of this query receiver
 *      relative to others. When ATS makes a font query, it will send
 *      the query to each receiver in priority order, from highest to
 *      lowest. "Normal" priority is 0.
 *    
 *    sourceOrder:
 *      The order of the created run loop source.
 *    
 *    callout:
 *      A function pointer of type ATSFontQueryCallback that will be
 *      called to process a font query.
 *    
 *    context:
 *      An ATSFontQuerySourceContext parameter block that provides a
 *      pointer-sized client datum which will be retained by ATS and
 *      passed to the callout function. May be NULL.
 *  
 *  Result:
 *    A CFRunLoopSourceRef. To stop receiving queries, invalidate this
 *    run loop source.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontManagerCreateFontRequestRunLoopSource()" }
function ATSCreateFontQueryRunLoopSource( queryOrder: CFIndex; sourceOrder: CFIndex; callout: ATSFontQueryCallback; {const} context: ATSFontQuerySourceContextPtr { can be NULL } ): CFRunLoopSourceRef;
AVAILABLE_MAC_OS_X_VERSION_10_2_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;


{ ----------------------------------------------------------------------------------------- }
{ Font request query message content                                                        }
{ ----------------------------------------------------------------------------------------- }
{ Keys in a font request query dictionary. }
{ These keys appear in the dictionary for a kATSQueryActivateFontMessage query. }

{
 *  kATSQueryClientPID
 *  
 *  Discussion:
 *    The process ID of the application making the query. The
 *    corresponding value is a CFNumberRef that contains a pid_t.
 }
const kATSQueryClientPID = CFSTR( 'ATS client pid' );

{
 *  kATSQueryQDFamilyName
 *  
 *  Discussion:
 *    The Quickdraw-style family name of the font being requested, e.g.
 *    the name passed to GetFNum. The corresponding value is a
 *    CFStringRef.
 }
const kATSQueryQDFamilyName = CFSTR( 'font family name' );

{
 *  kATSQueryFontName
 *  
 *  Discussion:
 *    The name of the font being requested. The corresponding value is
 *    a CFStringRef suitable as an argument to ATSFontFindFromName().
 *    This should match a candidate font's unique or full name.
 }
const kATSQueryFontName = CFSTR( 'font name' );

{
 *  kATSQueryFontPostScriptName
 *  
 *  Discussion:
 *    The PostScript name of the font being requested. The
 *    corresponding value is a CFStringRef suitable as an argument to
 *    ATSFontFindFromPostScriptName(). This should match either the
 *    PostScript name derived from the font's FOND resource or its sfnt
 *    name table, with preference given to the FOND PostScript name.
 }
const kATSQueryFontPostScriptName = CFSTR( 'font PS name' );

{
 *  kATSQueryFontNameTableEntries
 *  
 *  Discussion:
 *    A descriptor for sfnt name table entries that the requested font
 *    must have. The corresponding value is a CFArrayRef of
 *    CFDictionaryRefs that describe name table entries. A font must
 *    have all of the specified entries to be considered a match.
 }
const kATSQueryFontNameTableEntries = CFSTR( 'font name table entries' );
{ Keys in a font raw name descriptor dictionary. }

{
 *  kATSFontNameTableCode
 *  
 *  Discussion:
 *    The font name's name code. The corresponding value is a
 *    CFNumberRef. If missing, assume kFontNoNameCode.
 }
const kATSFontNameTableCode = CFSTR( 'font name code' );

{
 *  kATSFontNameTablePlatform
 *  
 *  Discussion:
 *    The font name's platform code. The corresponding value is a
 *    CFNumberRef. If missing, assume kFontNoPlatformCode.
 }
const kATSFontNameTablePlatform = CFSTR( 'font platform code' );

{
 *  kATSFontNameTableScript
 *  
 *  Discussion:
 *    The font name's script code. The corresponding value is a
 *    CFNumberRef. If missing, assume kFontNoScriptCode.
 }
const kATSFontNameTableScript = CFSTR( 'font script code' );

{
 *  kATSFontNameTableLanguage
 *  
 *  Discussion:
 *    The font name's language code. The corresponding value is a
 *    CFNumberRef. If missing, assume kFontNoLanguageCode.
 }
const kATSFontNameTableLanguage = CFSTR( 'font language code' );

{
 *  kATSFontNameTableBytes
 *  
 *  Discussion:
 *    The raw bytes of the font name. The corresponding value is a
 *    CFDataRef that contains the raw name bytes.
 }
const kATSFontNameTableBytes = CFSTR( 'font name table bytes' );
{ ----------------------------------------------------------------------------------------- }
{ Auto activation settings                                                                  }
{ ----------------------------------------------------------------------------------------- }

{
 *  ATSAutoActivationSetting
 *  
 *  Summary:
 *    Values for auto activation settings.
 }
const
{
   * Resets the setting the the default state. For application settings
   * this clears the setting. For the global setting it will revert to
   * the initial system setting, kATSFontAutoActivationEnabled.
   }
	kATSFontAutoActivationDefault = 0;
	kATSFontAutoActivationDisabled = 1;
	kATSFontAutoActivationEnabled = 2;

  {
   * Asks the user before auto-activating fonts requested by the
   * application.
   }
	kATSFontAutoActivationAsk = 4;


type
	ATSFontAutoActivationSetting = UInt32;
{
 *  ATSFontSetGlobalAutoActivationSetting()
 *  
 *  Summary:
 *    Sets the user's global auto-activation setting.
 *  
 *  Discussion:
 *    This function can be used to set the user's global
 *    auto-activation setting.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Leopard
 *  
 *  Parameters:
 *    
 *    iSetting:
 *      One of the enumerated constants above specifying the setting
 *      for font auto-activation. Will return paramErr on invalid input.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontManagerSetAutoActivationSetting() with kCTFontManagerBundleIdentifier" }
function ATSFontSetGlobalAutoActivationSetting( iSetting: ATSFontAutoActivationSetting ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontGetGlobalAutoActivationSetting()
 *  
 *  Summary:
 *    Gets the user's global auto-activation setting.
 *  
 *  Discussion:
 *    This function can be used to get the user's global
 *    auto-activation setting.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Leopard
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontManagerGetAutoActivationSetting() with kCTFontManagerBundleIdentifier" }
function ATSFontGetGlobalAutoActivationSetting: ATSFontAutoActivationSetting;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontSetAutoActivationSettingForApplication()
 *  
 *  Summary:
 *    Sets the auto-activation setting for the specified application
 *    bundle.
 *  
 *  Discussion:
 *    This function can be used to set the auto-activation setting for
 *    the specified application. The per-application setting overrides
 *    the global setting.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Leopard
 *  
 *  Parameters:
 *    
 *    iSetting:
 *      One of the enumerated constants above specifying the setting
 *      for font auto-activation. Specifying
 *      kATSFontAutoActivationDefault will clear the application
 *      specific setting and the global setting will be used.
 *    
 *    iApplicationFileURL:
 *      A valid file URL for an application. Passing NULL for this
 *      parameter indicates the current process.
 *  
 *  Result:
 *    An OSStatus code. Will return noErr on success, and paramErr on
 *    any invalid input. May return memFullErr if unable to allocate
 *    temporary structures.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontManagerSetAutoActivationSetting()" }
function ATSFontSetAutoActivationSettingForApplication( iSetting: ATSFontAutoActivationSetting; iApplicationFileURL: CFURLRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{
 *  ATSFontGetAutoActivationSettingForApplication()
 *  
 *  Summary:
 *    Query the activation setting for the specified application.
 *  
 *  Discussion:
 *    This function can be used to query the auto-activation setting
 *    for the specified application. The setting is the app-specific
 *    setting if available, otherwise it is
 *    kATSFontAutoActivationDefault.
 *  
 *  Mac OS X threading:
 *    Thread safe since version Leopard
 *  
 *  Parameters:
 *    
 *    iApplicationFileURL:
 *      A valid file URL for an application. Passing NULL for this
 *      parameter indicates the current process.
 *  
 *  Result:
 *    Returns the setting that will be used for the specified
 *    application. If this returns kATSFontAutoActivationDefault the
 *    global setting will be used.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
{ "Use CTFontManagerGetAutoActivationSetting()" }
function ATSFontGetAutoActivationSettingForApplication( iApplicationFileURL: CFURLRef ): ATSFontAutoActivationSetting;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_8;

{$endc} {TARGET_OS_MAC}

end.
