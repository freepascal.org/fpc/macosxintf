{	CFByteOrder.h
	Copyright (c) 1995-2009, Apple Inc. All rights reserved.
}
{   Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{   Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, September 2005 }
{   Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, April 2006, February 2008, October 2009 }
unit CFByteOrders;
interface
uses MacTypes,CFBase;
{$ALIGN POWER}


type
	CFByteOrder = SIGNEDLONG;
const
	CFByteOrderUnknown = 0;
	CFByteOrderLittleEndian = 1;
	CFByteOrderBigEndian = 2;

implemented function CFByteOrderGetCurrent: CFByteOrder; inline;
implemented function CFSwapInt16( arg: UInt16 ): UInt16; inline;
implemented function CFSwapInt32( arg: UInt32 ): UInt32; inline;
implemented function CFSwapInt64( arg: UInt64 ): UInt64; inline;

{FPC-ONLY-START}
implemented function CFSwapInt16BigToHost( arg: UInt16 ): UInt16; inline;
implemented function CFSwapInt32BigToHost( arg: UInt32 ): UInt32; inline;
implemented function CFSwapInt64BigToHost( arg: UInt64 ): UInt64; inline;
implemented function CFSwapInt16HostToBig( arg: UInt16 ): UInt16; inline;
implemented function CFSwapInt32HostToBig( arg: UInt32 ): UInt32; inline;
implemented function CFSwapInt64HostToBig( arg: UInt64 ): UInt64; inline;
{FPC-ONLY-FINISH}

{$ifc TARGET_RT_BIG_ENDIAN}

{GPC-MW-ONLY-START}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFSwapInt16BigToHost( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFSwapInt32BigToHost( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFSwapInt64BigToHost( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFSwapInt16HostToBig( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFSwapInt32HostToBig( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFSwapInt64HostToBig( arg ) (arg)}
{GPC-MW-ONLY-FINISH}

{$elsec}

{GPC-ONLY-START}
function CFSwapInt16BigToHost__NAMED__CFSwapInt16( arg: UInt16 ): UInt16;
function CFSwapInt32BigToHost__NAMED__CFSwapInt32( arg: UInt32 ): UInt32;
function CFSwapInt64BigToHost__NAMED__CFSwapInt64( arg: UInt64 ): UInt64;
function CFSwapInt16HostToBig__NAMED__CFSwapInt16( arg: UInt16 ): UInt16;
function CFSwapInt32HostToBig__NAMED__CFSwapInt32( arg: UInt32 ): UInt32;
function CFSwapInt64HostToBig__NAMED__CFSwapInt64( arg: UInt64 ): UInt64;
{GPC-ONLY-FINISH}

{MW-ONLY-START}
{$definec CFSwapInt16BigToHost( arg ) CFSwapInt16(arg)}
{$definec CFSwapInt32BigToHost( arg ) CFSwapInt32(arg)}
{$definec CFSwapInt64BigToHost( arg ) CFSwapInt64(arg)}
{$definec CFSwapInt16HostToBig( arg ) CFSwapInt16(arg)}
{$definec CFSwapInt32HostToBig( arg ) CFSwapInt32(arg)}
{$definec CFSwapInt64HostToBig( arg ) CFSwapInt64(arg)}
{MW-ONLY-FINISH}

{$endc}

{FPC-ONLY-START}
implemented function CFSwapInt16LittleToHost( arg: UInt16 ): UInt16; inline;
implemented function CFSwapInt32LittleToHost( arg: UInt32 ): UInt32; inline;
implemented function CFSwapInt64LittleToHost( arg: UInt64 ): UInt64; inline;
implemented function CFSwapInt16HostToLittle( arg: UInt16 ): UInt16; inline;
implemented function CFSwapInt32HostToLittle( arg: UInt32 ): UInt32; inline;
implemented function CFSwapInt64HostToLittle( arg: UInt64 ): UInt64; inline;
{FPC-ONLY-FINISH}

{$ifc TARGET_RT_LITTLE_ENDIAN}

{GPC-MW-ONLY-START}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:CFSwapInt16LittleToHost( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:CFSwapInt32LittleToHost( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:CFSwapInt64LittleToHost( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:CFSwapInt16HostToLittle( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:CFSwapInt32HostToLittle( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:CFSwapInt64HostToLittle( arg ) (arg)}
{GPC-MW-ONLY-FINISH}

{$elsec}

{GPC-ONLY-START}
function CFSwapInt16LittleToHost__NAMED__CFSwapInt16( arg: UInt16 ): UInt16;
function CFSwapInt32LittleToHost__NAMED__CFSwapInt32( arg: UInt32 ): UInt32;
function CFSwapInt64LittleToHost__NAMED__CFSwapInt64( arg: UInt64 ): UInt64;
function CFSwapInt16HostToLittle__NAMED__CFSwapInt16( arg: UInt16 ): UInt16;
function CFSwapInt32HostToLittle__NAMED__CFSwapInt32( arg: UInt32 ): UInt32;
function CFSwapInt64HostToLittle__NAMED__CFSwapInt64( arg: UInt64 ): UInt64;
{GPC-ONLY-FINISH}

{MW-ONLY-START}
{$definec CFSwapInt16LittleToHost( arg ) CFSwapInt16(arg)}
{$definec CFSwapInt32LittleToHost( arg ) CFSwapInt32(arg)}
{$definec CFSwapInt64LittleToHost( arg ) CFSwapInt64(arg)}
{$definec CFSwapInt16HostToLittle( arg ) CFSwapInt16(arg)}
{$definec CFSwapInt32HostToLittle( arg ) CFSwapInt32(arg)}
{$definec CFSwapInt64HostToLittle( arg ) CFSwapInt64(arg)}
{MW-ONLY-FINISH}

{$endc}

type
	CFSwappedFloat32 = record
		v: UInt32;
	end;
type
	CFSwappedFloat64 = record
		v: UInt64;
	end;

{FPC-ONLY-START}
implemented function CFConvertFloat32HostToSwapped( arg: Float32 ): CFSwappedFloat32; inline;
implemented function CFConvertFloat32SwappedToHost( arg: CFSwappedFloat32 ): Float32; inline;
implemented function CFConvertFloat64HostToSwapped( arg: Float64 ): CFSwappedFloat64; inline;
implemented function CFConvertFloat64SwappedToHost( arg: CFSwappedFloat64 ): Float64; inline;
implemented function CFConvertFloatHostToSwapped( arg: Float32 ): CFSwappedFloat32; inline;
implemented function CFConvertFloatSwappedToHost( arg: CFSwappedFloat32 ): Float32; inline;
implemented function CFConvertDoubleHostToSwapped( arg: Float64 ): CFSwappedFloat64; inline;
implemented function CFConvertDoubleSwappedToHost( arg: CFSwappedFloat64 ): Float64; inline;
{FPC-ONLY-FINISH}

{$ifc TARGET_RT_LITTLE_ENDIAN}

{GPC-ONLY-START}
function CFConvertFloat32HostToSwapped__NAMED__CFSwapInt32( arg: Float32 ): CFSwappedFloat32;
function CFConvertFloat32SwappedToHost__NAMED__CFSwapInt32( arg: CFSwappedFloat32 ): Float32;
function CFConvertFloat64HostToSwapped__NAMED__CFSwapInt64( arg: Float64 ): CFSwappedFloat64;
function CFConvertFloat64SwappedToHost__NAMED__CFSwapInt64( arg: CFSwappedFloat64 ): Float64;
function CFConvertFloatHostToSwapped__NAMED__CFSwapInt32( arg: Float32 ): CFSwappedFloat32;
function CFConvertFloatSwappedToHost__NAMED__CFSwapInt32( arg: CFSwappedFloat32 ): Float32;
function CFConvertDoubleHostToSwapped__NAMED__CFSwapInt64( arg: Float64 ): CFSwappedFloat64;
function CFConvertDoubleSwappedToHost__NAMED__CFSwapInt64( arg: CFSwappedFloat64 ): Float64;
{GPC-ONLY-FINISH}

{MW-ONLY-START}
{$definec CFConvertFloat32HostToSwapped( arg ) CFSwappedFloat32(CFSWapInt32(UInt32(arg)))}
{$definec CFConvertFloat32SwappedToHost( arg ) Float32(CFSWapInt32(UInt32(arg)))}
{$definec CFConvertFloat64HostToSwapped( arg ) CFSwappedFloat64(CFSWapInt64(UInt64(arg)))}
{$definec CFConvertFloat64SwappedToHost( arg ) Float64(CFSWapInt64(UInt64(arg)))}
{$definec CFConvertFloatHostToSwapped( arg ) CFSwappedFloat32(CFSWapInt32(UInt32(arg)))}
{$definec CFConvertFloatSwappedToHost( arg ) Float32(CFSWapInt32(UInt32(arg)))}
{$definec CFConvertDoubleHostToSwapped( arg ) CFSwappedFloat64(CFSWapInt64(UInt64(arg)))}
{$definec CFConvertDoubleSwappedToHost( arg ) Float64(CFSWapInt64(UInt64(arg)))}
{MW-ONLY-FINISH}

{$elsec}

{GPC-MW-ONLY-START}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFConvertFloat32HostToSwapped( arg ) CFSwappedFloat32(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFConvertFloat32SwappedToHost( arg ) Float32(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFConvertFloat64HostToSwapped( arg ) CFSwappedFloat64(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFConvertFloat64SwappedToHost( arg ) Float64(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFConvertFloatHostToSwapped( arg ) CFSwappedFloat32(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFConvertFloatSwappedToHost( arg ) Float32(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFConvertDoubleHostToSwapped( arg ) CFSwappedFloat64(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:CFConvertDoubleSwappedToHost( arg ) Float64(arg)}
{GPC-MW-ONLY-FINISH}

{$endc}

implementation

{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

{$R-}

function CFByteOrderGetCurrent: CFByteOrder; inline;
{MW-ONLY-START}
	const
{MW-ONLY-ELSE}
	var
{MW-ONLY-FINISH}
		x: UInt32 = (CFByteOrderBigEndian shl 24) or CFByteOrderLittleEndian;
begin
	CFByteOrderGetCurrent := CFByteOrder(UInt8Ptr(@x)^);
end;

function CFSwapInt16( arg: UInt16 ): UInt16; inline;
begin
	CFSwapInt16 := (( arg shl 8) and $0FF00) or (( arg shr 8) and $00FF);
end;

function CFSwapInt32( arg: UInt32 ): UInt32; inline;
begin
    CFSwapInt32 := ((arg and $FF) shl 24) or ((arg and $0FF00) shl 8) or ((arg shr 8) and $0FF00) or ((arg shr 24) and $FF);
end;

{FPC-GPC-ONLY-START}
function CFSwapInt64( arg: UInt64 ): UInt64; inline;
begin
	CFSwapInt64 := (UInt64(CFSwapInt32( arg and $FFFFFFFF )) shl 32) or CFSwapInt32( (arg shr 32) and $FFFFFFFF );
end;
{FPC-GPC-ONLY-ELSE}
function CFSwapInt64( arg: UInt64 ): UInt64; inline;
	var
		x: UInt64;
begin
	x.lo := CFSwapInt32( arg.hi );
	x.hi := CFSwapInt32( arg.lo );
	CFSwapInt64 := x;
end;
{FPC-GPC-ONLY-FINISH}

{FPC-ONLY-START}
{$ifc TARGET_RT_BIG_ENDIAN}
function CFSwapInt16BigToHost( arg: UInt16 ): UInt16; inline;
begin
  CFSwapInt16BigToHost := arg;
end;

function CFSwapInt32BigToHost( arg: UInt32 ): UInt32; inline;
begin
  CFSwapInt32BigToHost := arg;
end;

function CFSwapInt64BigToHost( arg: UInt64 ): UInt64; inline;
begin
  CFSwapInt64BigToHost := arg;
end;

function CFSwapInt16HostToBig( arg: UInt16 ): UInt16; inline;
begin
  CFSwapInt16HostToBig := arg;
end;

function CFSwapInt32HostToBig( arg: UInt32 ): UInt32; inline;
begin
  CFSwapInt32HostToBig := arg;
end;

function CFSwapInt64HostToBig( arg: UInt64 ): UInt64; inline;
begin
  CFSwapInt64HostToBig := arg;
end;

function CFSwapInt16LittleToHost( arg: UInt16 ): UInt16; inline;
begin
  CFSwapInt16LittleToHost := CFSwapInt16(arg);
end;

function CFSwapInt32LittleToHost( arg: UInt32 ): UInt32; inline;
begin
  CFSwapInt32LittleToHost := CFSwapInt32(arg);
end;

function CFSwapInt64LittleToHost( arg: UInt64 ): UInt64; inline;
begin
  CFSwapInt64LittleToHost := CFSwapInt64(arg);
end;

function CFSwapInt16HostToLittle( arg: UInt16 ): UInt16; inline;
begin
  CFSwapInt16HostToLittle := CFSwapInt16(arg);
end;

function CFSwapInt32HostToLittle( arg: UInt32 ): UInt32; inline;
begin
  CFSwapInt32HostToLittle := CFSwapInt32(arg);
end;

function CFSwapInt64HostToLittle( arg: UInt64 ): UInt64; inline;
begin
  CFSwapInt64HostToLittle := CFSwapInt64(arg);
end;

function CFConvertFloat32HostToSwapped( arg: Float32 ): CFSwappedFloat32; inline;
begin
  CFConvertFloat32HostToSwapped := CFSwappedFloat32(arg);
end;

function CFConvertFloat32SwappedToHost( arg: CFSwappedFloat32 ): Float32; inline;
begin
  CFConvertFloat32SwappedToHost := Float32(arg);
end;

function CFConvertFloat64HostToSwapped( arg: Float64 ): CFSwappedFloat64; inline;
begin
  CFConvertFloat64HostToSwapped := CFSwappedFloat64(arg);
end;

function CFConvertFloat64SwappedToHost( arg: CFSwappedFloat64 ): Float64; inline;
begin
  CFConvertFloat64SwappedToHost := Float64(arg);
end;

function CFConvertFloatHostToSwapped( arg: Float32 ): CFSwappedFloat32; inline;
begin
  CFConvertFloatHostToSwapped := CFSwappedFloat32(arg);
end;

function CFConvertFloatSwappedToHost( arg: CFSwappedFloat32 ): Float32; inline;
begin
  CFConvertFloatSwappedToHost := Float32(arg);
end;

function CFConvertDoubleHostToSwapped( arg: Float64): CFSwappedFloat64; inline;
begin
  CFConvertDoubleHostToSwapped := CFSwappedFloat64(arg);
end;

function CFConvertDoubleSwappedToHost( arg: CFSwappedFloat64 ): Float64; inline;
begin
  CFConvertDoubleSwappedToHost := Float64(arg);
end;

{$elsec}

function CFSwapInt16LittleToHost( arg: UInt16 ): UInt16; inline;
begin
  CFSwapInt16LittleToHost := arg;
end;

function CFSwapInt32LittleToHost( arg: UInt32 ): UInt32; inline;
begin
  CFSwapInt32LittleToHost := arg;
end;

function CFSwapInt64LittleToHost( arg: UInt64 ): UInt64; inline;
begin
  CFSwapInt64LittleToHost := arg;
end;

function CFSwapInt16HostToLittle( arg: UInt16 ): UInt16; inline;
begin
  CFSwapInt16HostToLittle := arg;
end;

function CFSwapInt32HostToLittle( arg: UInt32 ): UInt32; inline;
begin
  CFSwapInt32HostToLittle := arg;
end;

function CFSwapInt64HostToLittle( arg: UInt64 ): UInt64; inline;
begin
  CFSwapInt64HostToLittle := arg;
end;

function CFSwapInt16BigToHost( arg: UInt16 ): UInt16; inline;
begin
  CFSwapInt16BigToHost := CFSwapInt16(arg);
end;

function CFSwapInt32BigToHost( arg: UInt32 ): UInt32; inline;
begin
  CFSwapInt32BigToHost := CFSwapInt32(arg);
end;

function CFSwapInt64BigToHost( arg: UInt64 ): UInt64; inline;
begin
  CFSwapInt64BigToHost := CFSwapInt64(arg);
end;

function CFSwapInt16HostToBig( arg: UInt16 ): UInt16; inline;
begin
  CFSwapInt16HostToBig := CFSwapInt16(arg);
end;

function CFSwapInt32HostToBig( arg: UInt32 ): UInt32; inline;
begin
  CFSwapInt32HostToBig := CFSwapInt32(arg);
end;

function CFSwapInt64HostToBig( arg: UInt64 ): UInt64; inline;
begin
  CFSwapInt64HostToBig := CFSwapInt64(arg);
end;

function CFConvertFloat32HostToSwapped( arg: Float32 ): CFSwappedFloat32; inline;
begin
  CFConvertFloat32HostToSwapped.v := CFSwapInt32(CFSwappedFloat32(arg).v);
end;

function CFConvertFloat32SwappedToHost( arg: CFSwappedFloat32 ): Float32; inline;
begin
  CFConvertFloat32SwappedToHost := Float32(CFSwappedFloat32(CFSwapInt32(arg.v)));
end;

function CFConvertFloat64HostToSwapped( arg: Float64 ): CFSwappedFloat64; inline;
begin
  CFConvertFloat64HostToSwapped.v := CFSwapInt64(CFSwappedFloat64(arg).v);
end;

function CFConvertFloat64SwappedToHost( arg: CFSwappedFloat64 ): Float64; inline;
begin
  CFConvertFloat64SwappedToHost := Float64(CFSwappedFloat64(CFSwapInt64(arg.v)));
end;

function CFConvertFloatHostToSwapped( arg: Float32 ): CFSwappedFloat32; inline;
begin
  CFConvertFloatHostToSwapped.v := CFSwapInt32(CFSwappedFloat32(arg).v);
end;

function CFConvertFloatSwappedToHost( arg: CFSwappedFloat32 ): Float32; inline;
begin
  CFConvertFloatSwappedToHost := Float32(CFSwappedFloat32(CFSwapInt32(arg.v)));
end;

function CFConvertDoubleHostToSwapped( arg: Float64 ): CFSwappedFloat64; inline;
begin
  CFConvertDoubleHostToSwapped.v := CFSwapInt64(CFSwappedFloat64(arg).v);
end;

function CFConvertDoubleSwappedToHost( arg: CFSwappedFloat64 ): Float64; inline;
begin
  CFConvertDoubleSwappedToHost := Float64(CFSwappedFloat64(CFSwapInt64(arg.v)));
end;
{$endc}

{FPC-ONLY-FINISH}

end.
