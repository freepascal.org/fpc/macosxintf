{
     File:       HIToolbox/HIPopupButton.h
 
     Contains:   Definitions of the popup button and popup arrow views provided by HIToolbox.
 
     Version:    HIToolbox-624~3
 
     Copyright:  © 2006-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HIPopupButton;
interface
uses MacTypes,Appearance,CarbonEvents,Controls,Menus,QuickdrawTypes,CFBase,HIObject;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
 *  HIPopupButton.h
 *  
 *  Discussion:
 *    API definitions for the popup button and popup arrow views.
 }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ POPUP BUTTON (CDEF 25)                                                            }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  This is the new Appearance Popup Button. It takes the same variants and does the    }
{  same overloading as the previous popup menu control. There are some differences:    }
{  Passing in a menu ID of -12345 causes the popup not to try and get the menu from a  }
{  resource. Instead, you can build the menu and later stuff the MenuRef field in      }
{  the popup data information.                                                         }
{  You can pass -1 in the Max parameter to have the control calculate the width of the }
{  title on its own instead of guessing and then tweaking to get it right. It adds the }
{  appropriate amount of space between the title and the popup.                        }
{ Theme Popup Button proc IDs }
const
	kControlPopupButtonProc = 400;
	kControlPopupFixedWidthVariant = 1 shl 0;
	kControlPopupVariableWidthVariant = 1 shl 1;
	kControlPopupUseAddResMenuVariant = 1 shl 2;
	kControlPopupUseWFontVariant = kControlUsesOwningWindowsFontVariant;

{ Control Kind Tag }
const
	kControlKindPopupButton = FOUR_CHAR_CODE('popb');

{ The HIObject class ID for the HIPopupButton class. }
const kHIPopupButtonClassID = CFSTR( 'com.apple.HIPopupButton' );
{$ifc not TARGET_CPU_64}
{
 *  CreatePopupButtonControl()
 *  
 *  Summary:
 *    Creates a popup button control.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    window:
 *      The window that should contain the control. May be NULL on 10.3
 *      and later.
 *    
 *    boundsRect:
 *      The bounding box of the control.
 *    
 *    title:
 *      The title of the control.
 *    
 *    menuID:
 *      The ID of a menu that should be used by the control. A menu
 *      with this ID should be inserted into the menubar with
 *      InsertMenu(menu, kInsertHierarchicalMenu). You can also pass
 *      -12345 to have the control delay its acquisition of a menu; in
 *      this case, you can build the menu and later provide it to the
 *      control with SetControlData and kControlPopupButtonMenuRefTag
 *      or kControlPopupButtonOwnedMenuRefTag.
 *    
 *    variableWidth:
 *      Whether the width of the control is allowed to vary according
 *      to the width of the selected menu item text, or should remain
 *      fixed to the original control bounds width.
 *    
 *    titleWidth:
 *      The width of the title.
 *    
 *    titleJustification:
 *      The justification of the title.
 *    
 *    titleStyle:
 *      A QuickDraw style bitfield indicating the font style of the
 *      title.
 *    
 *    outControl:
 *      On exit, contains the new control.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreatePopupButtonControl( window: WindowRef { can be NULL }; const var boundsRect: Rect; title: CFStringRef; menuID_: MenuID; variableWidth: Boolean; titleWidth: SInt16; titleJustification: SInt16; titleStyle: Style; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ These tags are available in 1.0.1 or later of Appearance }
{$endc} {not TARGET_CPU_64}

const
	kControlPopupButtonMenuHandleTag = FOUR_CHAR_CODE('mhan'); { MenuRef}
	kControlPopupButtonMenuRefTag = FOUR_CHAR_CODE('mhan'); { MenuRef}
	kControlPopupButtonMenuIDTag = FOUR_CHAR_CODE('mnid'); { SInt16}

{ These tags are available in 1.1 or later of Appearance }
const
	kControlPopupButtonExtraHeightTag = FOUR_CHAR_CODE('exht'); { SInt16 - extra vertical whitespace within the button}
	kControlPopupButtonOwnedMenuRefTag = FOUR_CHAR_CODE('omrf'); { MenuRef}

{ These tags are available in Mac OS X }
const
	kControlPopupButtonCheckCurrentTag = FOUR_CHAR_CODE('chck'); { Boolean    - whether the popup puts a checkmark next to the current item (defaults to true)}

{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ POPUP ARROW (CDEF 12)                                                             }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  The popup arrow CDEF is used to draw the small arrow normally associated with a     }
{  popup control. The arrow can point in four directions, and a small or large version }
{  can be used. This control is provided to allow clients to draw the arrow in a       }
{  normalized fashion which will take advantage of themes automatically.               }
{ Popup Arrow proc IDs }
const
	kControlPopupArrowEastProc = 192;
	kControlPopupArrowWestProc = 193;
	kControlPopupArrowNorthProc = 194;
	kControlPopupArrowSouthProc = 195;
	kControlPopupArrowSmallEastProc = 196;
	kControlPopupArrowSmallWestProc = 197;
	kControlPopupArrowSmallNorthProc = 198;
	kControlPopupArrowSmallSouthProc = 199;

{ Popup Arrow Orientations }
const
	kControlPopupArrowOrientationEast = 0;
	kControlPopupArrowOrientationWest = 1;
	kControlPopupArrowOrientationNorth = 2;
	kControlPopupArrowOrientationSouth = 3;

type
	ControlPopupArrowOrientation = UInt16;
{ Popup Arrow Size }
const
	kControlPopupArrowSizeNormal = 0;
	kControlPopupArrowSizeSmall = 1;

type
	ControlPopupArrowSize = UInt16;
{ Control Kind Tag }
const
	kControlKindPopupArrow = FOUR_CHAR_CODE('parr');

{ The HIObject class ID for the HIPopupArrow class. }
const kHIPopupArrowClassID = CFSTR( 'com.apple.hipopuparrow' );
{$ifc not TARGET_CPU_64}

{
 *  CreatePopupArrowControl()
 *  
 *  Summary:
 *    Creates a popup arrow control.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    window:
 *      The window that should contain the control. May be NULL on 10.3
 *      and later.
 *    
 *    boundsRect:
 *      The bounding box of the control.
 *    
 *    orientation:
 *      The orientation of the control.
 *    
 *    size:
 *      The size of the control.
 *    
 *    outControl:
 *      On exit, contains the new control.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreatePopupArrowControl( window: WindowRef { can be NULL }; const var boundsRect: Rect; orientation: ControlPopupArrowOrientation; size: ControlPopupArrowSize; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{--------------------------------------------------------------------------------------}
{  ₯ DEPRECATED                                                                        }
{  All declarations below this point are either deprecated (they continue to function  }
{  but are not the most modern nor most efficient solution to a problem), or they are  }
{  completely unavailable on Mac OS X.                                                 }
{--------------------------------------------------------------------------------------}
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ Pop-Up Menu Control Constants                                                     }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{ Variant codes for the System 7 pop-up menu}
{$endc} {not TARGET_CPU_64}

const
	popupFixedWidth = 1 shl 0;
	popupVariableWidth = 1 shl 1;
	popupUseAddResMenu = 1 shl 2;
	popupUseWFont = 1 shl 3;

{ Menu label styles for the System 7 pop-up menu}
const
	popupTitleBold = 1 shl 8;
	popupTitleItalic = 1 shl 9;
	popupTitleUnderline = 1 shl 10;
	popupTitleOutline = 1 shl 11;
	popupTitleShadow = 1 shl 12;
	popupTitleCondense = 1 shl 13;
	popupTitleExtend = 1 shl 14;
	popupTitleNoStyle = 1 shl 15;

{ Menu label justifications for the System 7 pop-up menu}
const
	popupTitleLeftJust = $00000000;
	popupTitleCenterJust = $00000001;
	popupTitleRightJust = $000000FF;

{$endc} {TARGET_OS_MAC}

end.
