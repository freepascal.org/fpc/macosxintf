{
     File:       HIToolbox/TranslationExtensions.h
 
     Contains:   Macintosh Easy Open Translation Extension Interfaces.
 
     Version:    HIToolbox-624~3
 
     Copyright:  � 1993-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit TranslationExtensions;
interface
uses MacTypes,Files,QuickdrawTypes,Components;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{
   Translation Extensions are no longer supported. Carbon clients interested in extending translations
   should use filter services as described in TranslationServices.h.  The definitions below will NOT work
   for Carbon and are only defined for those files that need to build pre-Carbon applications.
}
const
	kSupportsFileTranslation = 1;
	kSupportsScrapTranslation = 2;
	kTranslatorCanGenerateFilename = 4;

{****************************************************************************************}
{ better names for 4-char codes}
type
	FileType = OSType;
	FileTypePtr = ^FileType;
	ScrapType = ResType;
{****************************************************************************************}
type
	TranslationAttributes = UInt32;
const
	taDstDocNeedsResourceFork = 1;
	taDstIsAppTranslation = 2;

{****************************************************************************************}
type
	FileTypeSpecPtr = ^FileTypeSpec;
	FileTypeSpec = record
		format: FileType;
		hint: SIGNEDLONG;
		flags: TranslationAttributes;               { taDstDocNeedsResourceFork, taDstIsAppTranslation}
		catInfoType: OSType;
		catInfoCreator: OSType;
	end;
type
	FileTranslationList = record
		modDate: UNSIGNEDLONG;
		groupCount: UNSIGNEDLONG;

                                              { conceptual declarations:}

                                              {    unsigned long group1SrcCount;}
                                              {    unsigned long group1SrcEntrySize = sizeof(FileTypeSpec);}
                                              {  FileTypeSpec  group1SrcTypes[group1SrcCount]}
                                              {  unsigned long group1DstCount;}
                                              {  unsigned long group1DstEntrySize = sizeof(FileTypeSpec);}
                                              {  FileTypeSpec  group1DstTypes[group1DstCount]}
	end;
	FileTranslationListPtr = ^FileTranslationList;
	FileTranslationListHandle = ^FileTranslationListPtr;
{****************************************************************************************}
type
	ScrapTypeSpecPtr = ^ScrapTypeSpec;
	ScrapTypeSpec = record
		format: ScrapType;
		hint: SIGNEDLONG;
	end;
type
	ScrapTranslationList = record
		modDate: UNSIGNEDLONG;
		groupCount: UNSIGNEDLONG;

                                              { conceptual declarations:}

                                              {    unsigned long     group1SrcCount;}
                                              {    unsigned long     group1SrcEntrySize = sizeof(ScrapTypeSpec);}
                                              {  ScrapTypeSpec     group1SrcTypes[group1SrcCount]}
                                              {  unsigned long     group1DstCount;}
                                              {    unsigned long     group1DstEntrySize = sizeof(ScrapTypeSpec);}
                                              {  ScrapTypeSpec     group1DstTypes[group1DstCount]}
	end;
	ScrapTranslationListPtr = ^ScrapTranslationList;
	ScrapTranslationListHandle = ^ScrapTranslationListPtr;
{******************************************************************************************

    definition of callbacks to update progress dialog

******************************************************************************************}
type
	TranslationRefNum = SIGNEDLONG;
{******************************************************************************************

    This routine sets the advertisement in the top half of the progress dialog.
    It is called once at the beginning of your DoTranslateFile routine.

    Enter   :   refNum          Translation reference supplied to DoTranslateFile.
                advertisement   A handle to the picture to display.  This must be non-purgable.
                                Before returning from DoTranslateFile, you should dispose
                                of the memory.  (Normally, it is in the temp translation heap
                                so it is cleaned up for you.)

    Exit    :   returns         noErr, paramErr, or memFullErr

******************************************************************************************}
{$ifc not TARGET_CPU_64}
{
 *  SetTranslationAdvertisement()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    There is no direct replacement at this time.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 thru 1.0.2
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function SetTranslationAdvertisement( refNum: TranslationRefNum; advertisement: PicHandle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{******************************************************************************************

    This routine updates the progress bar in the progress dialog.
    It is called repeatedly from within your DoTranslateFile routine.
    It should be called often, so that the user will get feedback if
    he tries to cancel.

    Enter   :   refNum      translation reference supplied to DoTranslateFile.
                progress    percent complete (0-100)

    Exit    :   canceled    TRUE if the user clicked the Cancel button, FALSE otherwise

    Return  :   noErr, paramErr, or memFullErr

******************************************************************************************}
{
 *  UpdateTranslationProgress()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    There is no direct replacement at this time.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 thru 1.0.2
 *    Non-Carbon CFM:   in Translation 1.0 and later
 }
function UpdateTranslationProgress( refNum: TranslationRefNum; percentDone: SInt16; var canceled: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_3;


{******************************************************************************************

    Component Manager component selectors for translation extension routines

******************************************************************************************}
{$endc} {not TARGET_CPU_64}

const
	kTranslateGetFileTranslationList = 0;
	kTranslateIdentifyFile = 1;
	kTranslateTranslateFile = 2;
	kTranslateGetTranslatedFilename = 3;
	kTranslateGetScrapTranslationList = 10;
	kTranslateIdentifyScrap = 11;
	kTranslateTranslateScrap = 12;
	kTranslateGetScrapTranslationListConsideringData = 13;


{******************************************************************************************

    routines which implement translation extensions

******************************************************************************************}
type
	DoGetFileTranslationListProcPtr = function( self: ComponentInstance; translationList: FileTranslationListHandle ): ComponentResult;
	DoIdentifyFileProcPtr = function( self: ComponentInstance; const var theDocument: FSSpec; var docType: FileType ): ComponentResult;
	DoTranslateFileProcPtr = function( self: ComponentInstance; refNum: TranslationRefNum; const var sourceDocument: FSSpec; srcType: FileType; srcTypeHint: SIGNEDLONG; const var dstDoc: FSSpec; dstType: FileType; dstTypeHint: SIGNEDLONG ): ComponentResult;
	DoGetTranslatedFilenameProcPtr = function( self: ComponentInstance; dstType: FileType; dstTypeHint: SIGNEDLONG; var theDocument: FSSpec ): ComponentResult;
	DoGetScrapTranslationListProcPtr = function( self: ComponentInstance; list: ScrapTranslationListHandle ): ComponentResult;
	DoIdentifyScrapProcPtr = function( self: ComponentInstance; dataPtr: {const} univ Ptr; dataLength: Size; var dataFormat: ScrapType ): ComponentResult;
	DoTranslateScrapProcPtr = function( self: ComponentInstance; refNum: TranslationRefNum; srcDataPtr: {const} univ Ptr; srcDataLength: Size; srcType: ScrapType; srcTypeHint: SIGNEDLONG; dstData: Handle; dstType: ScrapType; dstTypeHint: SIGNEDLONG ): ComponentResult;

{$endc} {TARGET_OS_MAC}

end.
