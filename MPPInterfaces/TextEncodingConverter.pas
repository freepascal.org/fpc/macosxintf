{
     File:       CarbonCore/TextEncodingConverter.h
 
     Contains:   Text Encoding Conversion Interfaces.
 
     Copyright:  � 1994-2011 Apple Inc. All rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit TextEncodingConverter;
interface
uses MacTypes,TextCommon,CFBase;

{$ifc TARGET_OS_MAC}


{$ALIGN MAC68K}

type
	TECPluginSignature = OSType;
	TECPluginVersion = UInt32;
{ plugin signatures }
const
	kTECSignature = FOUR_CHAR_CODE('encv');
	kTECUnicodePluginSignature = FOUR_CHAR_CODE('puni');
	kTECJapanesePluginSignature = FOUR_CHAR_CODE('pjpn');
	kTECChinesePluginSignature = FOUR_CHAR_CODE('pzho');
	kTECKoreanPluginSignature = FOUR_CHAR_CODE('pkor');


{ converter object reference }
type
	TECObjectRef = ^SInt32; { an opaque type }
	TECObjectRefPtr = ^TECObjectRef;  { when a var xx:TECObjectRef parameter can be nil, it is changed to xx: TECObjectRefPtr }
	TECSnifferObjectRef = ^SInt32; { an opaque type }
	TECSnifferObjectRefPtr = ^TECSnifferObjectRef;  { when a var xx:TECSnifferObjectRef parameter can be nil, it is changed to xx: TECSnifferObjectRefPtr }
	TECPluginSig = OSType;
	TECConversionInfoPtr = ^TECConversionInfo;
	TECConversionInfo = record
		sourceEncoding: TextEncoding;
		destinationEncoding: TextEncoding;
		reserved1: UInt16;
		reserved2: UInt16;
	end;

{
 *  TECInternetNameUsageMask
 *  
 *  Discussion:
 *    Mask values that control the mapping between TextEncoding and
 *    IANA charset name or MIB enum.
 }
type
	TECInternetNameUsageMask = UInt32;
const
{ Use one of the following}

  {
   * Use the default type of mapping given other usage information
   * (none currently defined).
   }
	kTECInternetNameDefaultUsageMask = 0;

  {
   * Use the closest possible match between TextEncoding value and IANA
   * charset name or MIB enum
   }
	kTECInternetNameStrictUsageMask = 1;

  {
   * When mapping from IANA charset name or MIB enum to TextEncoding,
   * map to the largest superset of the encoding specified by the
   * charset name or MIB enum (i.e. be tolerant). When mapping from
   * TextEncoding to IANA charset name or MIB enum, typically map to
   * the most generic or widely recognized charset name or MIB enum.
   }
	kTECInternetNameTolerantUsageMask = 2;

{ Special values for MIB enums }
const
	kTEC_MIBEnumDontCare = -1;

{ Additional control flags for TECSetBasicOptions }
const
	kTECDisableFallbacksBit = 16;
	kTECDisableLooseMappingsBit = 17;

const
	kTECDisableFallbacksMask = 1 shl kTECDisableFallbacksBit;
	kTECDisableLooseMappingsMask = 1 shl kTECDisableLooseMappingsBit;


{ return number of encodings types supported by user's configuraton of the encoding converter }
{
 *  TECCountAvailableTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.1 and later
 }
function TECCountAvailableTextEncodings( var numberEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ fill in an array of type TextEncoding passed in by the user with types of encodings the current configuration of the encoder can handle. }
{
 *  TECGetAvailableTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECGetAvailableTextEncodings( availableEncodings: {variable-size-array} TextEncodingPtr; maxAvailableEncodings: ItemCount; var actualAvailableEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ return number of from-to encoding conversion pairs supported  }
{
 *  TECCountDirectTextEncodingConversions()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECCountDirectTextEncodingConversions( var numberOfEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ fill in an array of type TextEncodingPair passed in by the user with types of encoding pairs the current configuration of the encoder can handle. }
{
 *  TECGetDirectTextEncodingConversions()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECGetDirectTextEncodingConversions( availableConversions: {variable-size-array} TECConversionInfoPtr; maxAvailableConversions: ItemCount; var actualAvailableConversions: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ return number of encodings a given encoding can be converter into }
{
 *  TECCountDestinationTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECCountDestinationTextEncodings( inputEncoding: TextEncoding; var numberOfEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ fill in an array of type TextEncodingPair passed in by the user with types of encodings pairs the current configuration of the encoder can handle. }
{
 *  TECGetDestinationTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECGetDestinationTextEncodings( inputEncoding: TextEncoding; destinationEncodings: {variable-size-array} TextEncodingPtr; maxDestinationEncodings: ItemCount; var actualDestinationEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ get info about a text encoding }
{
 *  TECGetTextEncodingInternetName()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.1 and later
 }
function TECGetTextEncodingInternetName( textEncoding_: TextEncoding; var encodingName: Str255 ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECGetTextEncodingFromInternetName()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.1 and later
 }
function TECGetTextEncodingFromInternetName( var textEncoding_: TextEncoding; const var encodingName: Str255 ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ create/dispose converters }
{
 *  TECCreateConverter()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.1 and later
 }
function TECCreateConverter( var newEncodingConverter: TECObjectRef; inputEncoding: TextEncoding; outputEncoding: TextEncoding ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECCreateConverterFromPath()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECCreateConverterFromPath( var newEncodingConverter: TECObjectRef; {const} inPath: {variable-size-array} TextEncodingPtr; inEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECDisposeConverter()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.1 and later
 }
function TECDisposeConverter( newEncodingConverter: TECObjectRef ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ convert text encodings }
{
 *  TECClearConverterContextInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECClearConverterContextInfo( encodingConverter: TECObjectRef ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECConvertText()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECConvertText( encodingConverter: TECObjectRef; inputBuffer: ConstTextPtr; inputBufferLength: ByteCount; var actualInputLength: ByteCount; outputBuffer: TextPtr; outputBufferLength: ByteCount; var actualOutputLength: ByteCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECFlushText()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECFlushText( encodingConverter: TECObjectRef; outputBuffer: TextPtr; outputBufferLength: ByteCount; var actualOutputLength: ByteCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ one-to-many routines }
{
 *  TECCountSubTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECCountSubTextEncodings( inputEncoding: TextEncoding; var numberOfEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECGetSubTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECGetSubTextEncodings( inputEncoding: TextEncoding; subEncodings: {variable-size-array} TextEncodingPtr; maxSubEncodings: ItemCount; var actualSubEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECGetEncodingList()
 *  
 *  Parameters:
 *    
 *    encodingConverter:
 *      The encodingConverter to return the encoding list for
 *    
 *    numEncodings:
 *      On exit, the number of encodings in encodingList
 *    
 *    encodingList:
 *      On exit, a handle containing numEncodings values of type
 *      TextEncoding, for each known encoding.  Do not dispose of this
 *      handle.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.1 and later
 }
function TECGetEncodingList( encodingConverter: TECObjectRef; var numEncodings: ItemCount; var encodingList: Handle ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECCreateOneToManyConverter()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECCreateOneToManyConverter( var newEncodingConverter: TECObjectRef; inputEncoding: TextEncoding; numOutputEncodings: ItemCount; {const} outputEncodings: {variable-size-array} TextEncodingPtr ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECConvertTextToMultipleEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECConvertTextToMultipleEncodings( encodingConverter: TECObjectRef; inputBuffer: ConstTextPtr; inputBufferLength: ByteCount; var actualInputLength: ByteCount; outputBuffer: TextPtr; outputBufferLength: ByteCount; var actualOutputLength: ByteCount; outEncodingsBuffer: {variable-size-array} TextEncodingRunPtr; maxOutEncodingRuns: ItemCount; var actualOutEncodingRuns: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECFlushMultipleEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECFlushMultipleEncodings( encodingConverter: TECObjectRef; outputBuffer: TextPtr; outputBufferLength: ByteCount; var actualOutputLength: ByteCount; outEncodingsBuffer: {variable-size-array} TextEncodingRunPtr; maxOutEncodingRuns: ItemCount; var actualOutEncodingRuns: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ international internet info }
{
 *  TECCountWebTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECCountWebTextEncodings( locale: RegionCode; var numberEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECGetWebTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECGetWebTextEncodings( locale: RegionCode; availableEncodings: {variable-size-array} TextEncodingPtr; maxAvailableEncodings: ItemCount; var actualAvailableEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECCountMailTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECCountMailTextEncodings( locale: RegionCode; var numberEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECGetMailTextEncodings()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECGetMailTextEncodings( locale: RegionCode; availableEncodings: {variable-size-array} TextEncodingPtr; maxAvailableEncodings: ItemCount; var actualAvailableEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{ examine text encodings }
{
 *  TECCountAvailableSniffers()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECCountAvailableSniffers( var numberOfEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECGetAvailableSniffers()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECGetAvailableSniffers( availableSniffers: {variable-size-array} TextEncodingPtr; maxAvailableSniffers: ItemCount; var actualAvailableSniffers: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECCreateSniffer()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECCreateSniffer( var encodingSniffer: TECSnifferObjectRef; {const} testEncodings: {variable-size-array} TextEncodingPtr; numTextEncodings: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECSniffTextEncoding()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECSniffTextEncoding( encodingSniffer: TECSnifferObjectRef; inputBuffer: ConstTextPtr; inputBufferLength: ByteCount; testEncodings: {variable-size-array} TextEncodingPtr; numTextEncodings: ItemCount; numErrsArray: {variable-size-array} ItemCountPtr; maxErrs: ItemCount; numFeaturesArray: {variable-size-array} ItemCountPtr; maxFeatures: ItemCount ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECDisposeSniffer()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECDisposeSniffer( encodingSniffer: TECSnifferObjectRef ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECClearSnifferContextInfo()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.2 and later
 }
function TECClearSnifferContextInfo( encodingSniffer: TECSnifferObjectRef ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);


{
 *  TECSetBasicOptions()
 *  
 *  Summary:
 *    Sets encodingConverter options affecting
 *    TECConvertText[ToMultipleEncodings].
 *  
 *  Parameters:
 *    
 *    encodingConverter:
 *      The high-level encoding converter object created by
 *      TECCreateConverter or TECCreateOneToManyConverter whose
 *      behavior is to be modified by the options specified in
 *      controlFlags.
 *    
 *    controlFlags:
 *      A bit mask specifying the desired options. The following mask
 *      constants are valid for this parameter; multiple mask constants
 *      may be ORed together to set multiple options; passing 0 for
 *      this parameter clears all options: 
 *      
 *      kUnicodeForceASCIIRangeMask, kUnicodeNoHalfwidthCharsMask
 *      (defined in UnicodeConverter.h) 
 *      
 *      kTECDisableFallbacksMask, kTECDisableLooseMappingsMask (defined
 *      above) - loose and fallback mappings are both enabled by
 *      default for the TextEncodingConverter.h conversion APIs
 *      (TECConvertText, TECConvertTextToMultipleEncodings), unlike the
 *      behavior of the conversion APIs in UnicodeConverter.h. These
 *      options may be used to disable loose and/or fallback mappings
 *      for the TextEncodingConverter.h conversion APIs.
 *  
 *  Result:
 *    The function returns paramErr for invalid masks,
 *    kTECCorruptConverterErr for an invalid encodingConverter, noErr
 *    otherwise.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.3 and later
 *    Non-Carbon CFM:   in TextEncodingConverter 1.5 and later
 }
function TECSetBasicOptions( encodingConverter: TECObjectRef; controlFlags: OptionBits ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);


{ Map TextEncoding values to/from IANA charset names and/or MIB enums, with usage control }
{
 *  TECCopyTextEncodingInternetNameAndMIB()
 *  
 *  Summary:
 *    Converts a TextEncoding value to an IANA charset name and/or a
 *    MIB enum value
 *  
 *  Discussion:
 *    Given a TextEncoding value, this function maps it to an IANA
 *    charset name (if encodingNamePtr is non-NULL) and/or a MIB enum
 *    value (if mibEnumPtr is non-NULL), as specified by the usage
 *    parameter.
 *  
 *  Parameters:
 *    
 *    textEncoding:
 *      A TextEncoding value to map to a charset name and/or MIB enum.
 *    
 *    usage:
 *      Specifies the type of mapping desired (see
 *      TECInternetNameUsageMask above).
 *    
 *    encodingNamePtr:
 *      If non-NULL, is a pointer to a CStringRef for an immutable
 *      CFString created by this function; when the caller is finished
 *      with it, the caller must dispose of it by calling CFRelease.
 *    
 *    mibEnumPtr:
 *      If non-NULL, is a pointer to an SInt32 that will be set to the
 *      appropriate MIB enum value, or to 0 (or kTEC_MIBEnumDontCare)
 *      if there is no appropriate MIB enum value (valid MIB enums
 *      begin at 3).
 *  
 *  Result:
 *    The function returns paramErr if encodingNamePtr and mibEnumPtr
 *    are both NULL. It returns kTextUnsupportedEncodingErr if it has
 *    no data for the supplied textEncoding. It returns noErr if it
 *    found useful data.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function TECCopyTextEncodingInternetNameAndMIB( textEncoding_: TextEncoding; usage: TECInternetNameUsageMask; encodingNamePtr: CFStringRefPtr { can be NULL }; mibEnumPtr: SInt32Ptr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);


{
 *  TECGetTextEncodingFromInternetNameOrMIB()
 *  
 *  Summary:
 *    Converts an IANA charset name or a MIB enum value to a
 *    TextEncoding value
 *  
 *  Discussion:
 *    If encodingName is non-NULL, this function treats it as an IANA
 *    charset name and maps it to a TextEncoding value; in this case
 *    mibEnum is ignored, and may be set to kTEC_MIBEnumDontCare.
 *    Otherwise, this function maps the mibEnum to a TextEncoding
 *    value. In either case, the mapping is controlled by the usage
 *    parameter. The textEncodingPtr parameter must be non-NULL.
 *  
 *  Result:
 *    The function returns paramErr if textEncodingPtr is NULL. It
 *    returns kTextUnsupportedEncodingErr if it has no data for the
 *    supplied encodingName or mibEnum. It returns noErr if it found
 *    useful data.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function TECGetTextEncodingFromInternetNameOrMIB( var textEncodingPtr: TextEncoding; usage: TECInternetNameUsageMask; encodingName: CFStringRef; mibEnum: SInt32 ): OSStatus;
__OSX_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_NA);


{$endc} {TARGET_OS_MAC}


end.
