{
 * Copyright (c) 1993-2011 by Apple Inc.. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 }
unit ConditionalMacros;
interface
{$ALIGN POWER}


{
     File:       ConditionalMacros.h
 
     Contains:   Set up for compiler independent conditionals
 
     Version:    CarbonCore-769~1
  
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}

{***************************************************************************************************
    UNIVERSAL_INTERFACES_VERSION
    
        0x0400 --> version 4.0 (Mac OS X only)
        0x0335 --> version 3.4 
        0x0331 --> version 3.3.1
        0x0330 --> version 3.3
        0x0320 --> version 3.2
        0x0310 --> version 3.1
        0x0301 --> version 3.0.1
        0x0300 --> version 3.0
        0x0210 --> version 2.1
        This conditional did not exist prior to version 2.1
***************************************************************************************************}
// defined in the conversion script as a macro }
// const
//   UNIVERSAL_INTERFACES_VERSION = $0400;
{***************************************************************************************************

    All TARGET_* condtionals are set up by TargetConditionals.h

***************************************************************************************************}


{***************************************************************************************************

    PRAGMA_*
    These conditionals specify whether the compiler supports particular #pragma's
    
        PRAGMA_IMPORT           - Compiler supports: #pragma import on/off/reset
        PRAGMA_ONCE             - Compiler supports: #pragma once
        PRAGMA_STRUCT_ALIGN     - Compiler supports: #pragma options align=mac68k/power/reset
        PRAGMA_STRUCT_PACK      - Compiler supports: #pragma pack(n)
        PRAGMA_STRUCT_PACKPUSH  - Compiler supports: #pragma pack(push, n)/pack(pop)
        PRAGMA_ENUM_PACK        - Compiler supports: #pragma options(!pack_enums)
        PRAGMA_ENUM_ALWAYSINT   - Compiler supports: #pragma enumsalwaysint on/off/reset
        PRAGMA_ENUM_OPTIONS     - Compiler supports: #pragma options enum=int/small/reset


    FOUR_CHAR_CODE
    This conditional is deprecated.  It was used to work around a bug in one obscure compiler that did not pack multiple characters in single quotes rationally.
    It was never intended for endian swapping.

        FOUR_CHAR_CODE('abcd')  - Convert a four-char-code to the correct 32-bit value


    TYPE_*
    These conditionals specify whether the compiler supports particular types.

        TYPE_LONGLONG               - Compiler supports "long long" 64-bit integers
        TYPE_BOOL                   - Compiler supports "bool"
        TYPE_EXTENDED               - Compiler supports "extended" 80/96 bit floating point
        TYPE_LONGDOUBLE_IS_DOUBLE   - Compiler implements "long double" same as "double"


    FUNCTION_*
    These conditionals specify whether the compiler supports particular language extensions
    to function prototypes and definitions.

        FUNCTION_PASCAL         - Compiler supports "pascal void Foo()"
        FUNCTION_DECLSPEC       - Compiler supports "__declspec(xxx) void Foo()"
        FUNCTION_WIN32CC        - Compiler supports "void __cdecl Foo()" and "void __stdcall Foo()"

***************************************************************************************************}

{***************************************************************************************************

    Under MacOS, the classic 68k runtime has two calling conventions: pascal or C
    Under Win32, there are two calling conventions: __cdecl or __stdcall
    Headers and implementation files can use the following macros to make their
    source more portable by hiding the calling convention details:

    EXTERN_API*
    These macros are used to specify the calling convention on a function prototype.

        EXTERN_API              - Classic 68k: pascal, Win32: __cdecl
        EXTERN_API_C            - Classic 68k: C,      Win32: __cdecl
        EXTERN_API_STDCALL      - Classic 68k: pascal, Win32: __stdcall
        EXTERN_API_C_STDCALL    - Classic 68k: C,      Win32: __stdcall


    DEFINE_API*
    These macros are used to specify the calling convention on a function definition.

        DEFINE_API              - Classic 68k: pascal, Win32: __cdecl
        DEFINE_API_C            - Classic 68k: C,      Win32: __cdecl
        DEFINE_API_STDCALL      - Classic 68k: pascal, Win32: __stdcall
        DEFINE_API_C_STDCALL    - Classic 68k: C,      Win32: __stdcall


    CALLBACK_API*
    These macros are used to specify the calling convention of a function pointer.

        CALLBACK_API            - Classic 68k: pascal, Win32: __stdcall
        CALLBACK_API_C          - Classic 68k: C,      Win32: __stdcall
        CALLBACK_API_STDCALL    - Classic 68k: pascal, Win32: __cdecl
        CALLBACK_API_C_STDCALL  - Classic 68k: C,      Win32: __cdecl

***************************************************************************************************}

{***************************************************************************************************
    
    Set up TARGET_API_*_* values

***************************************************************************************************}

{***************************************************************************************************
    Backward compatibility for clients expecting 2.x version on ConditionalMacros.h

    GENERATINGPOWERPC       - Compiler is generating PowerPC instructions
    GENERATING68K           - Compiler is generating 68k family instructions
    GENERATING68881         - Compiler is generating mc68881 floating point instructions
    GENERATINGCFM           - Code being generated assumes CFM calling conventions
    CFMSYSTEMCALLS          - No A-traps.  Systems calls are made using CFM and UPP's
    PRAGMA_ALIGN_SUPPORTED  - Compiler supports: #pragma options align=mac68k/power/reset
    PRAGMA_IMPORT_SUPPORTED - Compiler supports: #pragma import on/off/reset
    CGLUESUPPORTED          - Clients can use all lowercase toolbox functions that take C strings instead of pascal strings

***************************************************************************************************}


{***************************************************************************************************

    OLDROUTINENAMES         - "Old" names for Macintosh system calls are allowed in source code.
                              (e.g. DisposPtr instead of DisposePtr). The names of system routine
                              are now more sensitive to change because CFM binds by name.  In the 
                              past, system routine names were compiled out to just an A-Trap.  
                              Macros have been added that each map an old name to its new name.  
                              This allows old routine names to be used in existing source files,
                              but the macros only work if OLDROUTINENAMES is true.  This support
                              will be removed in the near future.  Thus, all source code should 
                              be changed to use the new names! You can set OLDROUTINENAMES to false
                              to see if your code has any old names left in it.
    
***************************************************************************************************}


{***************************************************************************************************
 The following macros isolate the use of 68K inlines in function prototypes.
    On the Mac OS under the Classic 68K runtime, function prototypes were followed
 by a list of 68K opcodes which the compiler inserted in the generated code instead
 of a JSR.  Under Classic 68K on the Mac OS, this macro will put the opcodes
    in the right syntax.  For all other OS's and runtimes the macro suppress the opcodes.
  Example:
   
       EXTERN_P void DrawPicture(PicHandle myPicture, const Rect *dstRect)
            ONEWORDINLINE(0xA8F6);
 
***************************************************************************************************}

{***************************************************************************************************

    TARGET_CARBON                   - default: false. Switches all of the above as described.  Overrides all others
                                    - NOTE: If you set TARGET_CARBON to 1, then the other switches will be setup by
                                            ConditionalMacros, and should not be set manually.

    If you wish to do development for pre-Carbon Systems, you can set the following:

    OPAQUE_TOOLBOX_STRUCTS          - default: false. True for Carbon builds, hides struct fields.
    OPAQUE_UPP_TYPES                - default: false. True for Carbon builds, UPP types are unique and opaque.
    ACCESSOR_CALLS_ARE_FUNCTIONS    - default: false. True for Carbon builds, enables accessor functions.
    CALL_NOT_IN_CARBON              - default: true.  False for Carbon builds, hides calls not supported in Carbon.
    
    Specifically, if you are building a non-Carbon application (one that links against InterfaceLib)
    but you wish to use some of the accessor functions, you can set ACCESSOR_CALLS_ARE_FUNCTIONS to 1
    and link with CarbonAccessors.o, which implements just the accessor functions. This will help you
    preserve source compatibility between your Carbon and non-Carbon application targets.
    
    MIXEDMODE_CALLS_ARE_FUNCTIONS   - deprecated.

***************************************************************************************************}

{
     * It's possible to have ACCESSOR_CALLS_ARE_FUNCTIONS set to true and OPAQUE_TOOLBOX_STRUCTS
     * set to false, but not the other way around, so make sure the defines are not set this way.
     }

end.
