{
     File:       ATS/ScalerStreamTypes.h
 
     Contains:   Scaler streaming data structures and constants for OFA 1.x
 
     Copyright:  � 1994-2008 by Apple Inc., all rights reserved.
 
     Warning:    *** APPLE INTERNAL USE ONLY ***
                 This file may contain unreleased API's
 
     BuildInfo:  Built by:            root
                 On:                  Fri Jul 24 22:21:51 2009
                 With Interfacer:     3.0d46   (Mac OS X for PowerPC)
                 From:                ScalerStreamTypes.i
                     Revision:        1.5
                     Dated:           2007/01/15 23:28:27
                     Last change by:  kurita
                     Last comment:    <rdar://problem/4916090> updated copyright.
 
     Bugs:       Report bugs to Radar component "System Interfaces", "Latest"
                 List the version information (from above) in the Problem Description.
 
}

{ Pascal Translation: Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }

unit ScalerStreamTypes;
interface
uses MacTypes,SFNTTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{ ScalerStream input/output types }
const
	cexec68K = $00000001;
	truetypeStreamType = $00000001;
	type1StreamType = $00000002;
	type3StreamType = $00000004;
	type42StreamType = $00000008;
	type42GXStreamType = $00000010;
	portableStreamType = $00000020;
	flattenedStreamType = $00000040;
	cidType2StreamType = $00000080;
	cidType0StreamType = $00000100;
	type1CFFStreamType = $00000200;
	evenOddModifierStreamType = $00008000;
	eexecBinaryModifierStreamType = $00010000; { encrypted portion of Type1Stream to be binary }
	unicodeMappingModifierStreamType = $00020000; { include glyph ID to unicode mapping info for PDF }
	scalerSpecifcModifierMask = $0000F000; { for scaler's internal use }
	streamTypeModifierMask = $FFFFF000; { 16 bits for Apple, 4 bits for scaler }

{ Possible streamed font formats }
type
	scalerStreamTypeFlag = UInt32;
const
	downloadStreamAction = 0;    { Transmit the (possibly sparse) font data }
	asciiDownloadStreamAction = 1;    { Transmit font data to a 7-bit ASCII destination }
	fontSizeQueryStreamAction = 2;    { Estimate in-printer memory used if the font were downloaded }
	encodingOnlyStreamAction = 3;    { Transmit only the encoding for the font }
	prerequisiteQueryStreamAction = 4;    { Return a list of prerequisite items needed for the font }
	prerequisiteItemStreamAction = 5;    { Transmit a specified prerequisite item }
	variationQueryStreamAction = 6;    { Return information regarding support for variation streaming }
	variationPSOperatorStreamAction = 7;   { Transmit Postscript code necessary to effect variation of a font }

type
	scalerStreamAction = SInt32;
const
	selectAllVariations = -1;    { Special variationCount value meaning include all variation data }

type
	scalerPrerequisiteItem = record
		enumeration: SInt32;            { Shorthand tag identifying the item }
		size: SInt32;                   { Worst case vm in printer item requires. Never > than 16-bit quantity }
		name: packed array[0..0] of char;          { Name to be used by the client when emitting the item (Pascal string) }
	end;


	scalerStremFontRec = record
		encoding: { const } UInt16Ptr;        { <- Intention is * unsigned short[256] }
		glyphBits: SInt32Ptr;          { <->    Bitvector: a bit for each glyph, 1 = desired/supplied }
		name: PChar               { <->    The printer font name to use/used (C string) }
	end;

	scalerStreamPrerequisiteQueryRec = record
		size: SInt32;               { ->     Size of the prereq. list in bytes (0 indicates no prerequisites)}
		list: { const } UnivPtr;               { <- Pointer to client block to hold list (nil = list size query only) }
	end;

	scalerStreamInfoRec = record
		case SInt16 of
		0:	(	{ Normal font streaming information }
				font: scalerStremFontRec
			);
		1:	(	{ Used to obtain a list of prerequisites from the scaler}
				prerequisiteQuery: scalerStreamPrerequisiteQueryRec
			);
		2:	(
				prerequisiteItem: SInt32;	{ <-     Enumeration value for the prerequisite item to be streamed.}
			);
		3:	(
				variationQueryResult: SInt32;	{ -> Output from the variationQueryStreamAction }
			);
		end;

	scalerStreamPtr = ^scalerStream;
	scalerStream = record
		streamRefCon: { const } UnivPtr;           { <- private reference for client }
		targetVersion: { const } PChar;          { <- e.g. Postscript printer name (C string) }
		types: scalerStreamTypeFlag;                { <->    Data stream formats desired/supplied }
		action: scalerStreamAction;                 { <-     What action to take }
		memorySize: UInt32;             { -> Worst case memory use (vm) in printer or as sfnt }
		variationCount: SInt32;         { <- The number of variations, or selectAllVariations }
		variations: { const } UnivPtr;             { <- A pointer to an array of the variations (gxFontVariation) }
		info: scalerStreamInfoRec;
	end;


	scalerStreamDataPtr = ^scalerStreamData;
	scalerStreamData = record
		hexFlag: SInt32;                { Indicates that the data is to be interpreted as hex, versus binary }
		byteCount: SInt32;              { Number of bytes in the data being streamed }
		data: { const } UnivPtr;                   { Pointer to the data being streamed }
	end;

{$endc} {TARGET_OS_MAC}


end.
