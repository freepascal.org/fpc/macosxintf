{	CFFileDescriptor.h
	Copyright (c) 2006-2013, Apple Inc. All rights reserved.
}
unit CFFileDescriptor;
interface
uses MacTypes,CFBase,CFString,CFRunLoop;
{$ALIGN POWER}


type
	CFFileDescriptorNativeDescriptor = SInt32;

type
	CFFileDescriptorRef = ^__CFFileDescriptor; { an opaque type }
	__CFFileDescriptor = record end;

{ Callback Reason Types }
const
	kCFFileDescriptorReadCallBack = UInt32(1 shl 0);
	kCFFileDescriptorWriteCallBack = UInt32(1 shl 1);

type
	CFFileDescriptorCallBack = procedure( f: CFFileDescriptorRef; callBackTypes: CFOptionFlags; info: univ Ptr );

type
	CFFileDescriptorContext = record
		version: CFIndex;
		info: UnivPtr;
		retain: procedure( info: univ Ptr );
		release: procedure( info: univ Ptr );
		copyDescription: function( info: univ Ptr): CFStringRef;
	end;
	CFFileDescriptorContextPtr = ^CFFileDescriptorContext;

function CFFileDescriptorGetTypeID: CFTypeID;
CF_AVAILABLE_STARTING(10_5, 2_0);

function CFFileDescriptorCreate( allocator: CFAllocatorRef; fd: CFFileDescriptorNativeDescriptor; closeOnInvalidate: Boolean; callout: CFFileDescriptorCallBack; const var context: CFFileDescriptorContext ): CFFileDescriptorRef;
CF_AVAILABLE_STARTING(10_5, 2_0);

function CFFileDescriptorGetNativeDescriptor( f: CFFileDescriptorRef ): CFFileDescriptorNativeDescriptor;
CF_AVAILABLE_STARTING(10_5, 2_0);

procedure CFFileDescriptorGetContext( f: CFFileDescriptorRef; var context: CFFileDescriptorContext );
CF_AVAILABLE_STARTING(10_5, 2_0);

procedure CFFileDescriptorEnableCallBacks( f: CFFileDescriptorRef; callBackTypes: CFOptionFlags );
CF_AVAILABLE_STARTING(10_5, 2_0);
procedure CFFileDescriptorDisableCallBacks( f: CFFileDescriptorRef; callBackTypes: CFOptionFlags );
CF_AVAILABLE_STARTING(10_5, 2_0);

procedure CFFileDescriptorInvalidate( f: CFFileDescriptorRef );
CF_AVAILABLE_STARTING(10_5, 2_0);
function CFFileDescriptorIsValid( f: CFFileDescriptorRef ): Boolean;
CF_AVAILABLE_STARTING(10_5, 2_0);

function CFFileDescriptorCreateRunLoopSource( allocator: CFAllocatorRef; f: CFFileDescriptorRef; order: CFIndex ): CFRunLoopSourceRef;
CF_AVAILABLE_STARTING(10_5, 2_0);



end.
