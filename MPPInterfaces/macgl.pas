{       Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }

unit macgl;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

//#ifndef __gl_h_
//#define __gl_h_


{
** License Applicability. Except to the extent portions of this file are
** made subject to an alternative license as permitted in the SGI Free
** Software License B, Version 1.1 (the "License"), the contents of this
** file are subject only to the provisions of the License. You may not use
** this file except in compliance with the License. You may obtain a copy
** of the License at Silicon Graphics, Inc., attn: Legal Services, 1600
** Amphitheatre Parkway, Mountain View, CA 94043-1351, or at:
** 
** http://oss.sgi.com/projects/FreeB
** 
** Note that, as provided in the License, the Software is distributed on an
** "AS IS" basis, with ALL EXPRESS AND IMPLIED WARRANTIES AND CONDITIONS
** DISCLAIMED, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTIES AND
** CONDITIONS OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A
** PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
** 
** Original Code. The Original Code is: OpenGL Sample Implementation,
** Version 1.2.1, released January 26, 2000, developed by Silicon Graphics,
** Inc. The Original Code is Copyright (c) 1991-2000 Silicon Graphics, Inc.
** Copyright in any portions created by third parties is as indicated
** elsewhere herein. All Rights Reserved.
** 
** Additional Notice Provisions: This software was created using the
** OpenGL(R) version 1.2.1 Sample Implementation published by SGI, but has
** not been independently verified as being compliant with the OpenGL(R)
** version 1.2.1 Specification.
}

// switches to providing function pointers
//#define GL_GLEXT_FUNCTION_POINTERS 1

type
	GLenum = UInt32;
	PGLenum = ^GLenum;
	
	GLboolean = UInt8;
	PGLboolean = ^GLboolean;
	
	GLbitfield = UInt32;
	PGLbitfield = ^GLbitfield;
	
	GLbyte = SInt8;
	PGLbyte = ^GLbyte;
	
	GLshort = SInt16;
	PGLshort = ^GLshort;
	
	GLint = SInt32;
	PGLint = ^GLint;
	
	GLsizei = SInt32;
	PGLsizei = ^GLsizei;
	
	GLubyte = UInt8;
	PGLubyte = ^GLubyte;
	
	GLushort = UInt16;
	PGLushort = ^GLushort;
	
	GLuint = UInt32;
	PGLuint = ^GLuint;
	
	GLfloat = Float32;
	PGLfloat = ^GLfloat;
	
	GLclampf = Float32;
	PGLclampf = ^GLclampf;
	
	GLdouble = Float64;
	PGLdouble = ^GLdouble;
	
	GLclampd = Float64;
	PGLclampd = ^GLclampd;
	
//	GLvoid = void;

type
	GLintptr = SIGNEDLONG;
	GLsizeiptr = SIGNEDLONG;

{$ifc not defined GL_TYPEDEFS_2_0}
{$definec GL_TYPEDEFS_2_0 TRUE}
type
	GLchar = char;
{$endc}


{***********************************************************}

{ Version }
{$definec GL_VERSION_1_1 TRUE}
{$definec GL_VERSION_1_2 TRUE}
{$definec GL_VERSION_1_3 TRUE}
{$definec GL_VERSION_1_4 TRUE}
{$definec GL_VERSION_1_5 TRUE}
{$definec GL_VERSION_2_0 TRUE}
{$definec GL_VERSION_2_1 TRUE}

{ AccumOp }
const GL_ACCUM                          = $0100;
const GL_LOAD                           = $0101;
const GL_RETURN                         = $0102;
const GL_MULT                           = $0103;
const GL_ADD                            = $0104;

{ AlphaFunction }
const GL_NEVER                          = $0200;
const GL_LESS                           = $0201;
const GL_EQUAL                          = $0202;
const GL_LEQUAL                         = $0203;
const GL_GREATER                        = $0204;
const GL_NOTEQUAL                       = $0205;
const GL_GEQUAL                         = $0206;
const GL_ALWAYS                         = $0207;

{ AttribMask }
const GL_CURRENT_BIT                    = $00000001;
const GL_POINT_BIT                      = $00000002;
const GL_LINE_BIT                       = $00000004;
const GL_POLYGON_BIT                    = $00000008;
const GL_POLYGON_STIPPLE_BIT            = $00000010;
const GL_PIXEL_MODE_BIT                 = $00000020;
const GL_LIGHTING_BIT                   = $00000040;
const GL_FOG_BIT                        = $00000080;
const GL_DEPTH_BUFFER_BIT               = $00000100;
const GL_ACCUM_BUFFER_BIT               = $00000200;
const GL_STENCIL_BUFFER_BIT             = $00000400;
const GL_VIEWPORT_BIT                   = $00000800;
const GL_TRANSFORM_BIT                  = $00001000;
const GL_ENABLE_BIT                     = $00002000;
const GL_COLOR_BUFFER_BIT               = $00004000;
const GL_HINT_BIT                       = $00008000;
const GL_EVAL_BIT                       = $00010000;
const GL_LIST_BIT                       = $00020000;
const GL_TEXTURE_BIT                    = $00040000;
const GL_SCISSOR_BIT                    = $00080000;
const GL_ALL_ATTRIB_BITS                = $000fffff;

{ BeginMode }
const GL_POINTS                         = $0000;
const GL_LINES                          = $0001;
const GL_LINE_LOOP                      = $0002;
const GL_LINE_STRIP                     = $0003;
const GL_TRIANGLES                      = $0004;
const GL_TRIANGLE_STRIP                 = $0005;
const GL_TRIANGLE_FAN                   = $0006;
const GL_QUADS                          = $0007;
const GL_QUAD_STRIP                     = $0008;
const GL_POLYGON                        = $0009;

{ BlendEquationMode }
{      GL_LOGIC_OP }
{      GL_FUNC_ADD }
{      GL_MIN }
{      GL_MAX }
{      GL_FUNC_SUBTRACT }
{      GL_FUNC_REVERSE_SUBTRACT }

{ BlendingFactorDest }
const
	GL_ZERO = 0;
const
	GL_ONE = 1;
const GL_SRC_COLOR                      = $0300;
const GL_ONE_MINUS_SRC_COLOR            = $0301;
const GL_SRC_ALPHA                      = $0302;
const GL_ONE_MINUS_SRC_ALPHA            = $0303;
const GL_DST_ALPHA                      = $0304;
const GL_ONE_MINUS_DST_ALPHA            = $0305;
{      GL_CONSTANT_COLOR }
{      GL_ONE_MINUS_CONSTANT_COLOR }
{      GL_CONSTANT_ALPHA }
{      GL_ONE_MINUS_CONSTANT_ALPHA }

{ BlendingFactorSrc }
{      GL_ZERO }
{      GL_ONE }
const GL_DST_COLOR                      = $0306;
const GL_ONE_MINUS_DST_COLOR            = $0307;
const GL_SRC_ALPHA_SATURATE             = $0308;
{      GL_SRC_ALPHA }
{      GL_ONE_MINUS_SRC_ALPHA }
{      GL_DST_ALPHA }
{      GL_ONE_MINUS_DST_ALPHA }
{      GL_CONSTANT_COLOR }
{      GL_ONE_MINUS_CONSTANT_COLOR }
{      GL_CONSTANT_ALPHA }
{      GL_ONE_MINUS_CONSTANT_ALPHA }

{ Boolean }
const
	GL_TRUE = 1;
const
	GL_FALSE = 0;

{ ClearBufferMask }
{      GL_COLOR_BUFFER_BIT }
{      GL_ACCUM_BUFFER_BIT }
{      GL_STENCIL_BUFFER_BIT }
{      GL_DEPTH_BUFFER_BIT }

{ ClientArrayType }
{      GL_VERTEX_ARRAY }
{      GL_NORMAL_ARRAY }
{      GL_COLOR_ARRAY }
{      GL_INDEX_ARRAY }
{      GL_TEXTURE_COORD_ARRAY }
{      GL_EDGE_FLAG_ARRAY }

{ ClipPlaneName }
const GL_CLIP_PLANE0                    = $3000;
const GL_CLIP_PLANE1                    = $3001;
const GL_CLIP_PLANE2                    = $3002;
const GL_CLIP_PLANE3                    = $3003;
const GL_CLIP_PLANE4                    = $3004;
const GL_CLIP_PLANE5                    = $3005;

{ ColorMaterialFace }
{      GL_FRONT }
{      GL_BACK }
{      GL_FRONT_AND_BACK }

{ ColorMaterialParameter }
{      GL_AMBIENT }
{      GL_DIFFUSE }
{      GL_SPECULAR }
{      GL_EMISSION }
{      GL_AMBIENT_AND_DIFFUSE }

{ ColorPointerType }
{      GL_BYTE }
{      GL_UNSIGNED_BYTE }
{      GL_SHORT }
{      GL_UNSIGNED_SHORT }
{      GL_INT }
{      GL_UNSIGNED_INT }
{      GL_FLOAT }
{      GL_DOUBLE }

{ ColorTableParameterPName }
{      GL_COLOR_TABLE_SCALE }
{      GL_COLOR_TABLE_BIAS }

{ ColorTableTarget }
{      GL_COLOR_TABLE }
{      GL_POST_CONVOLUTION_COLOR_TABLE }
{      GL_POST_COLOR_MATRIX_COLOR_TABLE }
{      GL_PROXY_COLOR_TABLE }
{      GL_PROXY_POST_CONVOLUTION_COLOR_TABLE }
{      GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE }

{ ConvolutionBorderMode }
{      GL_REDUCE }
{      GL_IGNORE_BORDER }
{      GL_CONSTANT_BORDER }

{ ConvolutionParameter }
{      GL_CONVOLUTION_BORDER_MODE }
{      GL_CONVOLUTION_FILTER_SCALE }
{      GL_CONVOLUTION_FILTER_BIAS }

{ ConvolutionTarget }
{      GL_CONVOLUTION_1D }
{      GL_CONVOLUTION_2D }

{ CullFaceMode }
{      GL_FRONT }
{      GL_BACK }
{      GL_FRONT_AND_BACK }

{ DataType }
const GL_BYTE                           = $1400;
const GL_UNSIGNED_BYTE                  = $1401;
const GL_SHORT                          = $1402;
const GL_UNSIGNED_SHORT                 = $1403;
const GL_INT                            = $1404;
const GL_UNSIGNED_INT                   = $1405;
const GL_FLOAT                          = $1406;
const GL_2_BYTES                        = $1407;
const GL_3_BYTES                        = $1408;
const GL_4_BYTES                        = $1409;
const GL_DOUBLE                         = $140A;

{ DepthFunction }
{      GL_NEVER }
{      GL_LESS }
{      GL_EQUAL }
{      GL_LEQUAL }
{      GL_GREATER }
{      GL_NOTEQUAL }
{      GL_GEQUAL }
{      GL_ALWAYS }

{ DrawBufferMode }
const
	GL_NONE = 0;
const GL_FRONT_LEFT                     = $0400;
const GL_FRONT_RIGHT                    = $0401;
const GL_BACK_LEFT                      = $0402;
const GL_BACK_RIGHT                     = $0403;
const GL_FRONT                          = $0404;
const GL_BACK                           = $0405;
const GL_LEFT                           = $0406;
const GL_RIGHT                          = $0407;
const GL_FRONT_AND_BACK                 = $0408;
const GL_AUX0                           = $0409;
const GL_AUX1                           = $040A;
const GL_AUX2                           = $040B;
const GL_AUX3                           = $040C;

{ Enable }
{      GL_FOG }
{      GL_LIGHTING }
{      GL_TEXTURE_1D }
{      GL_TEXTURE_2D }
{      GL_LINE_STIPPLE }
{      GL_POLYGON_STIPPLE }
{      GL_CULL_FACE }
{      GL_ALPHA_TEST }
{      GL_BLEND }
{      GL_INDEX_LOGIC_OP }
{      GL_COLOR_LOGIC_OP }
{      GL_DITHER }
{      GL_STENCIL_TEST }
{      GL_DEPTH_TEST }
{      GL_CLIP_PLANE0 }
{      GL_CLIP_PLANE1 }
{      GL_CLIP_PLANE2 }
{      GL_CLIP_PLANE3 }
{      GL_CLIP_PLANE4 }
{      GL_CLIP_PLANE5 }
{      GL_LIGHT0 }
{      GL_LIGHT1 }
{      GL_LIGHT2 }
{      GL_LIGHT3 }
{      GL_LIGHT4 }
{      GL_LIGHT5 }
{      GL_LIGHT6 }
{      GL_LIGHT7 }
{      GL_TEXTURE_GEN_S }
{      GL_TEXTURE_GEN_T }
{      GL_TEXTURE_GEN_R }
{      GL_TEXTURE_GEN_Q }
{      GL_MAP1_VERTEX_3 }
{      GL_MAP1_VERTEX_4 }
{      GL_MAP1_COLOR_4 }
{      GL_MAP1_INDEX }
{      GL_MAP1_NORMAL }
{      GL_MAP1_TEXTURE_COORD_1 }
{      GL_MAP1_TEXTURE_COORD_2 }
{      GL_MAP1_TEXTURE_COORD_3 }
{      GL_MAP1_TEXTURE_COORD_4 }
{      GL_MAP2_VERTEX_3 }
{      GL_MAP2_VERTEX_4 }
{      GL_MAP2_COLOR_4 }
{      GL_MAP2_INDEX }
{      GL_MAP2_NORMAL }
{      GL_MAP2_TEXTURE_COORD_1 }
{      GL_MAP2_TEXTURE_COORD_2 }
{      GL_MAP2_TEXTURE_COORD_3 }
{      GL_MAP2_TEXTURE_COORD_4 }
{      GL_POINT_SMOOTH }
{      GL_LINE_SMOOTH }
{      GL_POLYGON_SMOOTH }
{      GL_SCISSOR_TEST }
{      GL_COLOR_MATERIAL }
{      GL_NORMALIZE }
{      GL_AUTO_NORMAL }
{      GL_VERTEX_ARRAY }
{      GL_NORMAL_ARRAY }
{      GL_COLOR_ARRAY }
{      GL_INDEX_ARRAY }
{      GL_TEXTURE_COORD_ARRAY }
{      GL_EDGE_FLAG_ARRAY }
{      GL_POLYGON_OFFSET_POINT }
{      GL_POLYGON_OFFSET_LINE }
{      GL_POLYGON_OFFSET_FILL }
{      GL_COLOR_TABLE }
{      GL_POST_CONVOLUTION_COLOR_TABLE }
{      GL_POST_COLOR_MATRIX_COLOR_TABLE }
{      GL_CONVOLUTION_1D }
{      GL_CONVOLUTION_2D }
{      GL_SEPARABLE_2D }
{      GL_HISTOGRAM }
{      GL_MINMAX }
{      GL_RESCALE_NORMAL }
{      GL_TEXTURE_3D }

{ ErrorCode }
const
	GL_NO_ERROR = 0;
const GL_INVALID_ENUM                   = $0500;
const GL_INVALID_VALUE                  = $0501;
const GL_INVALID_OPERATION              = $0502;
const GL_STACK_OVERFLOW                 = $0503;
const GL_STACK_UNDERFLOW                = $0504;
const GL_OUT_OF_MEMORY                  = $0505;
{      GL_TABLE_TOO_LARGE }

{ FeedBackMode }
const GL_2D                             = $0600;
const GL_3D                             = $0601;
const GL_3D_COLOR                       = $0602;
const GL_3D_COLOR_TEXTURE               = $0603;
const GL_4D_COLOR_TEXTURE               = $0604;

{ FeedBackToken }
const GL_PASS_THROUGH_TOKEN             = $0700;
const GL_POINT_TOKEN                    = $0701;
const GL_LINE_TOKEN                     = $0702;
const GL_POLYGON_TOKEN                  = $0703;
const GL_BITMAP_TOKEN                   = $0704;
const GL_DRAW_PIXEL_TOKEN               = $0705;
const GL_COPY_PIXEL_TOKEN               = $0706;
const GL_LINE_RESET_TOKEN               = $0707;

{ FogMode }
{      GL_LINEAR }
const GL_EXP                            = $0800;
const GL_EXP2                           = $0801;

{ FogParameter }
{      GL_FOG_COLOR }
{      GL_FOG_DENSITY }
{      GL_FOG_END }
{      GL_FOG_INDEX }
{      GL_FOG_MODE }
{      GL_FOG_START }

{ FrontFaceDirection }
const GL_CW                             = $0900;
const GL_CCW                            = $0901;

{ GetColorTableParameterPName }
{      GL_COLOR_TABLE_SCALE }
{      GL_COLOR_TABLE_BIAS }
{      GL_COLOR_TABLE_FORMAT }
{      GL_COLOR_TABLE_WIDTH }
{      GL_COLOR_TABLE_RED_SIZE }
{      GL_COLOR_TABLE_GREEN_SIZE }
{      GL_COLOR_TABLE_BLUE_SIZE }
{      GL_COLOR_TABLE_ALPHA_SIZE }
{      GL_COLOR_TABLE_LUMINANCE_SIZE }
{      GL_COLOR_TABLE_INTENSITY_SIZE }

{ GetConvolutionParameterPName }
{      GL_CONVOLUTION_BORDER_COLOR }
{      GL_CONVOLUTION_BORDER_MODE }
{      GL_CONVOLUTION_FILTER_SCALE }
{      GL_CONVOLUTION_FILTER_BIAS }
{      GL_CONVOLUTION_FORMAT }
{      GL_CONVOLUTION_WIDTH }
{      GL_CONVOLUTION_HEIGHT }
{      GL_MAX_CONVOLUTION_WIDTH }
{      GL_MAX_CONVOLUTION_HEIGHT }

{ GetHistogramParameterPName }
{      GL_HISTOGRAM_WIDTH }
{      GL_HISTOGRAM_FORMAT }
{      GL_HISTOGRAM_RED_SIZE }
{      GL_HISTOGRAM_GREEN_SIZE }
{      GL_HISTOGRAM_BLUE_SIZE }
{      GL_HISTOGRAM_ALPHA_SIZE }
{      GL_HISTOGRAM_LUMINANCE_SIZE }
{      GL_HISTOGRAM_SINK }

{ GetMapTarget }
const GL_COEFF                          = $0A00;
const GL_ORDER                          = $0A01;
const GL_DOMAIN                         = $0A02;

{ GetMinmaxParameterPName }
{      GL_MINMAX_FORMAT }
{      GL_MINMAX_SINK }

{ GetPixelMap }
{      GL_PIXEL_MAP_I_TO_I }
{      GL_PIXEL_MAP_S_TO_S }
{      GL_PIXEL_MAP_I_TO_R }
{      GL_PIXEL_MAP_I_TO_G }
{      GL_PIXEL_MAP_I_TO_B }
{      GL_PIXEL_MAP_I_TO_A }
{      GL_PIXEL_MAP_R_TO_R }
{      GL_PIXEL_MAP_G_TO_G }
{      GL_PIXEL_MAP_B_TO_B }
{      GL_PIXEL_MAP_A_TO_A }

{ GetPointerTarget }
{      GL_VERTEX_ARRAY_POINTER }
{      GL_NORMAL_ARRAY_POINTER }
{      GL_COLOR_ARRAY_POINTER }
{      GL_INDEX_ARRAY_POINTER }
{      GL_TEXTURE_COORD_ARRAY_POINTER }
{      GL_EDGE_FLAG_ARRAY_POINTER }

{ GetTarget }
const GL_CURRENT_COLOR                  = $0B00;
const GL_CURRENT_INDEX                  = $0B01;
const GL_CURRENT_NORMAL                 = $0B02;
const GL_CURRENT_TEXTURE_COORDS         = $0B03;
const GL_CURRENT_RASTER_COLOR           = $0B04;
const GL_CURRENT_RASTER_INDEX           = $0B05;
const GL_CURRENT_RASTER_TEXTURE_COORDS  = $0B06;
const GL_CURRENT_RASTER_POSITION        = $0B07;
const GL_CURRENT_RASTER_POSITION_VALID  = $0B08;
const GL_CURRENT_RASTER_DISTANCE        = $0B09;
const GL_POINT_SMOOTH                   = $0B10;
const GL_POINT_SIZE                     = $0B11;
const GL_POINT_SIZE_RANGE               = $0B12;
const GL_POINT_SIZE_GRANULARITY         = $0B13;
const GL_LINE_SMOOTH                    = $0B20;
const GL_LINE_WIDTH                     = $0B21;
const GL_LINE_WIDTH_RANGE               = $0B22;
const GL_LINE_WIDTH_GRANULARITY         = $0B23;
const GL_LINE_STIPPLE                   = $0B24;
const GL_LINE_STIPPLE_PATTERN           = $0B25;
const GL_LINE_STIPPLE_REPEAT            = $0B26;
{      GL_SMOOTH_POINT_SIZE_RANGE }
{      GL_SMOOTH_POINT_SIZE_GRANULARITY }
{      GL_SMOOTH_LINE_WIDTH_RANGE }
{      GL_SMOOTH_LINE_WIDTH_GRANULARITY }
{      GL_ALIASED_POINT_SIZE_RANGE }
{      GL_ALIASED_LINE_WIDTH_RANGE }
const GL_LIST_MODE                      = $0B30;
const GL_MAX_LIST_NESTING               = $0B31;
const GL_LIST_BASE                      = $0B32;
const GL_LIST_INDEX                     = $0B33;
const GL_POLYGON_MODE                   = $0B40;
const GL_POLYGON_SMOOTH                 = $0B41;
const GL_POLYGON_STIPPLE                = $0B42;
const GL_EDGE_FLAG                      = $0B43;
const GL_CULL_FACE                      = $0B44;
const GL_CULL_FACE_MODE                 = $0B45;
const GL_FRONT_FACE                     = $0B46;
const GL_LIGHTING                       = $0B50;
const GL_LIGHT_MODEL_LOCAL_VIEWER       = $0B51;
const GL_LIGHT_MODEL_TWO_SIDE           = $0B52;
const GL_LIGHT_MODEL_AMBIENT            = $0B53;
const GL_SHADE_MODEL                    = $0B54;
const GL_COLOR_MATERIAL_FACE            = $0B55;
const GL_COLOR_MATERIAL_PARAMETER       = $0B56;
const GL_COLOR_MATERIAL                 = $0B57;
const GL_FOG                            = $0B60;
const GL_FOG_INDEX                      = $0B61;
const GL_FOG_DENSITY                    = $0B62;
const GL_FOG_START                      = $0B63;
const GL_FOG_END                        = $0B64;
const GL_FOG_MODE                       = $0B65;
const GL_FOG_COLOR                      = $0B66;
const GL_DEPTH_RANGE                    = $0B70;
const GL_DEPTH_TEST                     = $0B71;
const GL_DEPTH_WRITEMASK                = $0B72;
const GL_DEPTH_CLEAR_VALUE              = $0B73;
const GL_DEPTH_FUNC                     = $0B74;
const GL_ACCUM_CLEAR_VALUE              = $0B80;
const GL_STENCIL_TEST                   = $0B90;
const GL_STENCIL_CLEAR_VALUE            = $0B91;
const GL_STENCIL_FUNC                   = $0B92;
const GL_STENCIL_VALUE_MASK             = $0B93;
const GL_STENCIL_FAIL                   = $0B94;
const GL_STENCIL_PASS_DEPTH_FAIL        = $0B95;
const GL_STENCIL_PASS_DEPTH_PASS        = $0B96;
const GL_STENCIL_REF                    = $0B97;
const GL_STENCIL_WRITEMASK              = $0B98;
const GL_MATRIX_MODE                    = $0BA0;
const GL_NORMALIZE                      = $0BA1;
const GL_VIEWPORT                       = $0BA2;
const GL_MODELVIEW_STACK_DEPTH          = $0BA3;
const GL_PROJECTION_STACK_DEPTH         = $0BA4;
const GL_TEXTURE_STACK_DEPTH            = $0BA5;
const GL_MODELVIEW_MATRIX               = $0BA6;
const GL_PROJECTION_MATRIX              = $0BA7;
const GL_TEXTURE_MATRIX                 = $0BA8;
const GL_ATTRIB_STACK_DEPTH             = $0BB0;
const GL_CLIENT_ATTRIB_STACK_DEPTH      = $0BB1;
const GL_ALPHA_TEST                     = $0BC0;
const GL_ALPHA_TEST_FUNC                = $0BC1;
const GL_ALPHA_TEST_REF                 = $0BC2;
const GL_DITHER                         = $0BD0;
const GL_BLEND_DST                      = $0BE0;
const GL_BLEND_SRC                      = $0BE1;
const GL_BLEND                          = $0BE2;
const GL_LOGIC_OP_MODE                  = $0BF0;

const GL_INDEX_LOGIC_OP                 = $0BF1;
{ For compatibility with OpenGL v1.0 }
const GL_LOGIC_OP						= GL_INDEX_LOGIC_OP;

const GL_COLOR_LOGIC_OP                 = $0BF2;
const GL_AUX_BUFFERS                    = $0C00;
const GL_DRAW_BUFFER                    = $0C01;
const GL_READ_BUFFER                    = $0C02;
const GL_SCISSOR_BOX                    = $0C10;
const GL_SCISSOR_TEST                   = $0C11;
const GL_INDEX_CLEAR_VALUE              = $0C20;
const GL_INDEX_WRITEMASK                = $0C21;
const GL_COLOR_CLEAR_VALUE              = $0C22;
const GL_COLOR_WRITEMASK                = $0C23;
const GL_INDEX_MODE                     = $0C30;
const GL_RGBA_MODE                      = $0C31;
const GL_DOUBLEBUFFER                   = $0C32;
const GL_STEREO                         = $0C33;
const GL_RENDER_MODE                    = $0C40;
const GL_PERSPECTIVE_CORRECTION_HINT    = $0C50;
const GL_POINT_SMOOTH_HINT              = $0C51;
const GL_LINE_SMOOTH_HINT               = $0C52;
const GL_POLYGON_SMOOTH_HINT            = $0C53;
const GL_FOG_HINT                       = $0C54;
const GL_TEXTURE_GEN_S                  = $0C60;
const GL_TEXTURE_GEN_T                  = $0C61;
const GL_TEXTURE_GEN_R                  = $0C62;
const GL_TEXTURE_GEN_Q                  = $0C63;
const GL_PIXEL_MAP_I_TO_I               = $0C70;
const GL_PIXEL_MAP_S_TO_S               = $0C71;
const GL_PIXEL_MAP_I_TO_R               = $0C72;
const GL_PIXEL_MAP_I_TO_G               = $0C73;
const GL_PIXEL_MAP_I_TO_B               = $0C74;
const GL_PIXEL_MAP_I_TO_A               = $0C75;
const GL_PIXEL_MAP_R_TO_R               = $0C76;
const GL_PIXEL_MAP_G_TO_G               = $0C77;
const GL_PIXEL_MAP_B_TO_B               = $0C78;
const GL_PIXEL_MAP_A_TO_A               = $0C79;
const GL_PIXEL_MAP_I_TO_I_SIZE          = $0CB0;
const GL_PIXEL_MAP_S_TO_S_SIZE          = $0CB1;
const GL_PIXEL_MAP_I_TO_R_SIZE          = $0CB2;
const GL_PIXEL_MAP_I_TO_G_SIZE          = $0CB3;
const GL_PIXEL_MAP_I_TO_B_SIZE          = $0CB4;
const GL_PIXEL_MAP_I_TO_A_SIZE          = $0CB5;
const GL_PIXEL_MAP_R_TO_R_SIZE          = $0CB6;
const GL_PIXEL_MAP_G_TO_G_SIZE          = $0CB7;
const GL_PIXEL_MAP_B_TO_B_SIZE          = $0CB8;
const GL_PIXEL_MAP_A_TO_A_SIZE          = $0CB9;
const GL_UNPACK_SWAP_BYTES              = $0CF0;
const GL_UNPACK_LSB_FIRST               = $0CF1;
const GL_UNPACK_ROW_LENGTH              = $0CF2;
const GL_UNPACK_SKIP_ROWS               = $0CF3;
const GL_UNPACK_SKIP_PIXELS             = $0CF4;
const GL_UNPACK_ALIGNMENT               = $0CF5;
const GL_PACK_SWAP_BYTES                = $0D00;
const GL_PACK_LSB_FIRST                 = $0D01;
const GL_PACK_ROW_LENGTH                = $0D02;
const GL_PACK_SKIP_ROWS                 = $0D03;
const GL_PACK_SKIP_PIXELS               = $0D04;
const GL_PACK_ALIGNMENT                 = $0D05;
const GL_MAP_COLOR                      = $0D10;
const GL_MAP_STENCIL                    = $0D11;
const GL_INDEX_SHIFT                    = $0D12;
const GL_INDEX_OFFSET                   = $0D13;
const GL_RED_SCALE                      = $0D14;
const GL_RED_BIAS                       = $0D15;
const GL_ZOOM_X                         = $0D16;
const GL_ZOOM_Y                         = $0D17;
const GL_GREEN_SCALE                    = $0D18;
const GL_GREEN_BIAS                     = $0D19;
const GL_BLUE_SCALE                     = $0D1A;
const GL_BLUE_BIAS                      = $0D1B;
const GL_ALPHA_SCALE                    = $0D1C;
const GL_ALPHA_BIAS                     = $0D1D;
const GL_DEPTH_SCALE                    = $0D1E;
const GL_DEPTH_BIAS                     = $0D1F;
const GL_MAX_EVAL_ORDER                 = $0D30;
const GL_MAX_LIGHTS                     = $0D31;
const GL_MAX_CLIP_PLANES                = $0D32;
const GL_MAX_TEXTURE_SIZE               = $0D33;
const GL_MAX_PIXEL_MAP_TABLE            = $0D34;
const GL_MAX_ATTRIB_STACK_DEPTH         = $0D35;
const GL_MAX_MODELVIEW_STACK_DEPTH      = $0D36;
const GL_MAX_NAME_STACK_DEPTH           = $0D37;
const GL_MAX_PROJECTION_STACK_DEPTH     = $0D38;
const GL_MAX_TEXTURE_STACK_DEPTH        = $0D39;
const GL_MAX_VIEWPORT_DIMS              = $0D3A;
const GL_MAX_CLIENT_ATTRIB_STACK_DEPTH  = $0D3B;
const GL_SUBPIXEL_BITS                  = $0D50;
const GL_INDEX_BITS                     = $0D51;
const GL_RED_BITS                       = $0D52;
const GL_GREEN_BITS                     = $0D53;
const GL_BLUE_BITS                      = $0D54;
const GL_ALPHA_BITS                     = $0D55;
const GL_DEPTH_BITS                     = $0D56;
const GL_STENCIL_BITS                   = $0D57;
const GL_ACCUM_RED_BITS                 = $0D58;
const GL_ACCUM_GREEN_BITS               = $0D59;
const GL_ACCUM_BLUE_BITS                = $0D5A;
const GL_ACCUM_ALPHA_BITS               = $0D5B;
const GL_NAME_STACK_DEPTH               = $0D70;
const GL_AUTO_NORMAL                    = $0D80;
const GL_MAP1_COLOR_4                   = $0D90;
const GL_MAP1_INDEX                     = $0D91;
const GL_MAP1_NORMAL                    = $0D92;
const GL_MAP1_TEXTURE_COORD_1           = $0D93;
const GL_MAP1_TEXTURE_COORD_2           = $0D94;
const GL_MAP1_TEXTURE_COORD_3           = $0D95;
const GL_MAP1_TEXTURE_COORD_4           = $0D96;
const GL_MAP1_VERTEX_3                  = $0D97;
const GL_MAP1_VERTEX_4                  = $0D98;
const GL_MAP2_COLOR_4                   = $0DB0;
const GL_MAP2_INDEX                     = $0DB1;
const GL_MAP2_NORMAL                    = $0DB2;
const GL_MAP2_TEXTURE_COORD_1           = $0DB3;
const GL_MAP2_TEXTURE_COORD_2           = $0DB4;
const GL_MAP2_TEXTURE_COORD_3           = $0DB5;
const GL_MAP2_TEXTURE_COORD_4           = $0DB6;
const GL_MAP2_VERTEX_3                  = $0DB7;
const GL_MAP2_VERTEX_4                  = $0DB8;
const GL_MAP1_GRID_DOMAIN               = $0DD0;
const GL_MAP1_GRID_SEGMENTS             = $0DD1;
const GL_MAP2_GRID_DOMAIN               = $0DD2;
const GL_MAP2_GRID_SEGMENTS             = $0DD3;
const GL_TEXTURE_1D                     = $0DE0;
const GL_TEXTURE_2D                     = $0DE1;
const GL_FEEDBACK_BUFFER_POINTER        = $0DF0;
const GL_FEEDBACK_BUFFER_SIZE           = $0DF1;
const GL_FEEDBACK_BUFFER_TYPE           = $0DF2;
const GL_SELECTION_BUFFER_POINTER       = $0DF3;
const GL_SELECTION_BUFFER_SIZE          = $0DF4;
{      GL_TEXTURE_BINDING_1D }
{      GL_TEXTURE_BINDING_2D }
{      GL_TEXTURE_BINDING_3D }
{      GL_VERTEX_ARRAY }
{      GL_NORMAL_ARRAY }
{      GL_COLOR_ARRAY }
{      GL_INDEX_ARRAY }
{      GL_TEXTURE_COORD_ARRAY }
{      GL_EDGE_FLAG_ARRAY }
{      GL_VERTEX_ARRAY_SIZE }
{      GL_VERTEX_ARRAY_TYPE }
{      GL_VERTEX_ARRAY_STRIDE }
{      GL_NORMAL_ARRAY_TYPE }
{      GL_NORMAL_ARRAY_STRIDE }
{      GL_COLOR_ARRAY_SIZE }
{      GL_COLOR_ARRAY_TYPE }
{      GL_COLOR_ARRAY_STRIDE }
{      GL_INDEX_ARRAY_TYPE }
{      GL_INDEX_ARRAY_STRIDE }
{      GL_TEXTURE_COORD_ARRAY_SIZE }
{      GL_TEXTURE_COORD_ARRAY_TYPE }
{      GL_TEXTURE_COORD_ARRAY_STRIDE }
{      GL_EDGE_FLAG_ARRAY_STRIDE }
{      GL_POLYGON_OFFSET_FACTOR }
{      GL_POLYGON_OFFSET_UNITS }
{      GL_COLOR_TABLE }
{      GL_POST_CONVOLUTION_COLOR_TABLE }
{      GL_POST_COLOR_MATRIX_COLOR_TABLE }
{      GL_CONVOLUTION_1D }
{      GL_CONVOLUTION_2D }
{      GL_SEPARABLE_2D }
{      GL_POST_CONVOLUTION_RED_SCALE }
{      GL_POST_CONVOLUTION_GREEN_SCALE }
{      GL_POST_CONVOLUTION_BLUE_SCALE }
{      GL_POST_CONVOLUTION_ALPHA_SCALE }
{      GL_POST_CONVOLUTION_RED_BIAS }
{      GL_POST_CONVOLUTION_GREEN_BIAS }
{      GL_POST_CONVOLUTION_BLUE_BIAS }
{      GL_POST_CONVOLUTION_ALPHA_BIAS }
{      GL_COLOR_MATRIX }
{      GL_COLOR_MATRIX_STACK_DEPTH }
{      GL_MAX_COLOR_MATRIX_STACK_DEPTH }
{      GL_POST_COLOR_MATRIX_RED_SCALE }
{      GL_POST_COLOR_MATRIX_GREEN_SCALE }
{      GL_POST_COLOR_MATRIX_BLUE_SCALE }
{      GL_POST_COLOR_MATRIX_ALPHA_SCALE }
{      GL_POST_COLOR_MATRIX_RED_BIAS }
{      GL_POST_COLOR_MATRIX_GREEN_BIAS }
{      GL_POST_COLOR_MATRIX_BLUE_BIAS }
{      GL_POST_COLOR_MATRIX_ALPHA_BIAS }
{      GL_HISTOGRAM }
{      GL_MINMAX }
{      GL_MAX_ELEMENTS_VERTICES }
{      GL_MAX_ELEMENTS_INDICES }
{      GL_RESCALE_NORMAL }
{      GL_LIGHT_MODEL_COLOR_CONTROL }
{      GL_PACK_SKIP_IMAGES }
{      GL_PACK_IMAGE_HEIGHT }
{      GL_UNPACK_SKIP_IMAGES }
{      GL_UNPACK_IMAGE_HEIGHT }
{      GL_TEXTURE_3D }
{      GL_MAX_3D_TEXTURE_SIZE }
{      GL_BLEND_COLOR }
{      GL_BLEND_EQUATION }

{ GetTextureParameter }
{      GL_TEXTURE_MAG_FILTER }
{      GL_TEXTURE_MIN_FILTER }
{      GL_TEXTURE_WRAP_S }
{      GL_TEXTURE_WRAP_T }
const GL_TEXTURE_WIDTH                  = $1000;
const GL_TEXTURE_HEIGHT                 = $1001;

{ For compatibility with OpenGL v1.0 }
const GL_TEXTURE_INTERNAL_FORMAT        = $1003;
const GL_TEXTURE_COMPONENTS				= GL_TEXTURE_INTERNAL_FORMAT;

const GL_TEXTURE_BORDER_COLOR           = $1004;
const GL_TEXTURE_BORDER                 = $1005;
{      GL_TEXTURE_RED_SIZE }
{      GL_TEXTURE_GREEN_SIZE }
{      GL_TEXTURE_BLUE_SIZE }
{      GL_TEXTURE_ALPHA_SIZE }
{      GL_TEXTURE_LUMINANCE_SIZE }
{      GL_TEXTURE_INTENSITY_SIZE }
{      GL_TEXTURE_PRIORITY }
{      GL_TEXTURE_RESIDENT }
{      GL_TEXTURE_DEPTH }
{      GL_TEXTURE_WRAP_R }
{      GL_TEXTURE_MIN_LOD }
{      GL_TEXTURE_MAX_LOD }
{      GL_TEXTURE_BASE_LEVEL }
{      GL_TEXTURE_MAX_LEVEL }

{ HintMode }
const GL_DONT_CARE                      = $1100;
const GL_FASTEST                        = $1101;
const GL_NICEST                         = $1102;

{ HintTarget }
{      GL_PERSPECTIVE_CORRECTION_HINT }
{      GL_POINT_SMOOTH_HINT }
{      GL_LINE_SMOOTH_HINT }
{      GL_POLYGON_SMOOTH_HINT }
{      GL_FOG_HINT }

{ HistogramTarget }
{      GL_HISTOGRAM }
{      GL_PROXY_HISTOGRAM }

{ IndexPointerType }
{      GL_SHORT }
{      GL_INT }
{      GL_FLOAT }
{      GL_DOUBLE }

{ LightModelColorControl }
{      GL_SINGLE_COLOR }
{      GL_SEPARATE_SPECULAR_COLOR }

{ LightModelParameter }
{      GL_LIGHT_MODEL_AMBIENT }
{      GL_LIGHT_MODEL_LOCAL_VIEWER }
{      GL_LIGHT_MODEL_TWO_SIDE }
{      GL_LIGHT_MODEL_COLOR_CONTROL }

{ LightName }
const GL_LIGHT0                         = $4000;
const GL_LIGHT1                         = $4001;
const GL_LIGHT2                         = $4002;
const GL_LIGHT3                         = $4003;
const GL_LIGHT4                         = $4004;
const GL_LIGHT5                         = $4005;
const GL_LIGHT6                         = $4006;
const GL_LIGHT7                         = $4007;

{ LightParameter }
const GL_AMBIENT                        = $1200;
const GL_DIFFUSE                        = $1201;
const GL_SPECULAR                       = $1202;
const GL_POSITION                       = $1203;
const GL_SPOT_DIRECTION                 = $1204;
const GL_SPOT_EXPONENT                  = $1205;
const GL_SPOT_CUTOFF                    = $1206;
const GL_CONSTANT_ATTENUATION           = $1207;
const GL_LINEAR_ATTENUATION             = $1208;
const GL_QUADRATIC_ATTENUATION          = $1209;

{ InterleavedArrays }
{      GL_V2F }
{      GL_V3F }
{      GL_C4UB_V2F }
{      GL_C4UB_V3F }
{      GL_C3F_V3F }
{      GL_N3F_V3F }
{      GL_C4F_N3F_V3F }
{      GL_T2F_V3F }
{      GL_T4F_V4F }
{      GL_T2F_C4UB_V3F }
{      GL_T2F_C3F_V3F }
{      GL_T2F_N3F_V3F }
{      GL_T2F_C4F_N3F_V3F }
{      GL_T4F_C4F_N3F_V4F }

{ ListMode }
const GL_COMPILE                        = $1300;
const GL_COMPILE_AND_EXECUTE            = $1301;

{ ListNameType }
{      GL_BYTE }
{      GL_UNSIGNED_BYTE }
{      GL_SHORT }
{      GL_UNSIGNED_SHORT }
{      GL_INT }
{      GL_UNSIGNED_INT }
{      GL_FLOAT }
{      GL_2_BYTES }
{      GL_3_BYTES }
{      GL_4_BYTES }

{ LogicOp }
const GL_CLEAR                          = $1500;
const GL_AND                            = $1501;
const GL_AND_REVERSE                    = $1502;
const GL_COPY                           = $1503;
const GL_AND_INVERTED                   = $1504;
const GL_NOOP                           = $1505;
const GL_XOR                            = $1506;
const GL_OR                             = $1507;
const GL_NOR                            = $1508;
const GL_EQUIV                          = $1509;
const GL_INVERT                         = $150A;
const GL_OR_REVERSE                     = $150B;
const GL_COPY_INVERTED                  = $150C;
const GL_OR_INVERTED                    = $150D;
const GL_NAND                           = $150E;
const GL_SET                            = $150F;

{ MapTarget }
{      GL_MAP1_COLOR_4 }
{      GL_MAP1_INDEX }
{      GL_MAP1_NORMAL }
{      GL_MAP1_TEXTURE_COORD_1 }
{      GL_MAP1_TEXTURE_COORD_2 }
{      GL_MAP1_TEXTURE_COORD_3 }
{      GL_MAP1_TEXTURE_COORD_4 }
{      GL_MAP1_VERTEX_3 }
{      GL_MAP1_VERTEX_4 }
{      GL_MAP2_COLOR_4 }
{      GL_MAP2_INDEX }
{      GL_MAP2_NORMAL }
{      GL_MAP2_TEXTURE_COORD_1 }
{      GL_MAP2_TEXTURE_COORD_2 }
{      GL_MAP2_TEXTURE_COORD_3 }
{      GL_MAP2_TEXTURE_COORD_4 }
{      GL_MAP2_VERTEX_3 }
{      GL_MAP2_VERTEX_4 }

{ MaterialFace }
{      GL_FRONT }
{      GL_BACK }
{      GL_FRONT_AND_BACK }

{ MaterialParameter }
const GL_EMISSION                       = $1600;
const GL_SHININESS                      = $1601;
const GL_AMBIENT_AND_DIFFUSE            = $1602;
const GL_COLOR_INDEXES                  = $1603;
{      GL_AMBIENT }
{      GL_DIFFUSE }
{      GL_SPECULAR }

{ MatrixMode }
const GL_MODELVIEW                      = $1700;
const GL_PROJECTION                     = $1701;
const GL_TEXTURE                        = $1702;

{ MeshMode1 }
{      GL_POINT }
{      GL_LINE }

{ MeshMode2 }
{      GL_POINT }
{      GL_LINE }
{      GL_FILL }

{ MinmaxTarget }
{      GL_MINMAX }

{ NormalPointerType }
{      GL_BYTE }
{      GL_SHORT }
{      GL_INT }
{      GL_FLOAT }
{      GL_DOUBLE }

{ PixelCopyType }
const GL_COLOR                          = $1800;
const GL_DEPTH                          = $1801;
const GL_STENCIL                        = $1802;

{ PixelFormat }
const GL_COLOR_INDEX                    = $1900;
const GL_STENCIL_INDEX                  = $1901;
const GL_DEPTH_COMPONENT                = $1902;
const GL_RED                            = $1903;
const GL_GREEN                          = $1904;
const GL_BLUE                           = $1905;
const GL_ALPHA                          = $1906;
const GL_RGB                            = $1907;
const GL_RGBA                           = $1908;
const GL_LUMINANCE                      = $1909;
const GL_LUMINANCE_ALPHA                = $190A;
{      GL_ABGR }

{ PixelInternalFormat }
{      GL_ALPHA4 }
{      GL_ALPHA8 }
{      GL_ALPHA12 }
{      GL_ALPHA16 }
{      GL_LUMINANCE4 }
{      GL_LUMINANCE8 }
{      GL_LUMINANCE12 }
{      GL_LUMINANCE16 }
{      GL_LUMINANCE4_ALPHA4 }
{      GL_LUMINANCE6_ALPHA2 }
{      GL_LUMINANCE8_ALPHA8 }
{      GL_LUMINANCE12_ALPHA4 }
{      GL_LUMINANCE12_ALPHA12 }
{      GL_LUMINANCE16_ALPHA16 }
{      GL_INTENSITY }
{      GL_INTENSITY4 }
{      GL_INTENSITY8 }
{      GL_INTENSITY12 }
{      GL_INTENSITY16 }
{      GL_R3_G3_B2 }
{      GL_RGB4 }
{      GL_RGB5 }
{      GL_RGB8 }
{      GL_RGB10 }
{      GL_RGB12 }
{      GL_RGB16 }
{      GL_RGBA2 }
{      GL_RGBA4 }
{      GL_RGB5_A1 }
{      GL_RGBA8 }
{      GL_RGB10_A2 }
{      GL_RGBA12 }
{      GL_RGBA16 }

{ PixelMap }
{      GL_PIXEL_MAP_I_TO_I }
{      GL_PIXEL_MAP_S_TO_S }
{      GL_PIXEL_MAP_I_TO_R }
{      GL_PIXEL_MAP_I_TO_G }
{      GL_PIXEL_MAP_I_TO_B }
{      GL_PIXEL_MAP_I_TO_A }
{      GL_PIXEL_MAP_R_TO_R }
{      GL_PIXEL_MAP_G_TO_G }
{      GL_PIXEL_MAP_B_TO_B }
{      GL_PIXEL_MAP_A_TO_A }

{ PixelStore }
{      GL_UNPACK_SWAP_BYTES }
{      GL_UNPACK_LSB_FIRST }
{      GL_UNPACK_ROW_LENGTH }
{      GL_UNPACK_SKIP_ROWS }
{      GL_UNPACK_SKIP_PIXELS }
{      GL_UNPACK_ALIGNMENT }
{      GL_PACK_SWAP_BYTES }
{      GL_PACK_LSB_FIRST }
{      GL_PACK_ROW_LENGTH }
{      GL_PACK_SKIP_ROWS }
{      GL_PACK_SKIP_PIXELS }
{      GL_PACK_ALIGNMENT }
{      GL_PACK_SKIP_IMAGES }
{      GL_PACK_IMAGE_HEIGHT }
{      GL_UNPACK_SKIP_IMAGES }
{      GL_UNPACK_IMAGE_HEIGHT }

{ PixelTransfer }
{      GL_MAP_COLOR }
{      GL_MAP_STENCIL }
{      GL_INDEX_SHIFT }
{      GL_INDEX_OFFSET }
{      GL_RED_SCALE }
{      GL_RED_BIAS }
{      GL_GREEN_SCALE }
{      GL_GREEN_BIAS }
{      GL_BLUE_SCALE }
{      GL_BLUE_BIAS }
{      GL_ALPHA_SCALE }
{      GL_ALPHA_BIAS }
{      GL_DEPTH_SCALE }
{      GL_DEPTH_BIAS }
{      GL_POST_CONVOLUTION_RED_SCALE }
{      GL_POST_CONVOLUTION_GREEN_SCALE }
{      GL_POST_CONVOLUTION_BLUE_SCALE }
{      GL_POST_CONVOLUTION_ALPHA_SCALE }
{      GL_POST_CONVOLUTION_RED_BIAS }
{      GL_POST_CONVOLUTION_GREEN_BIAS }
{      GL_POST_CONVOLUTION_BLUE_BIAS }
{      GL_POST_CONVOLUTION_ALPHA_BIAS }
{      GL_POST_COLOR_MATRIX_RED_SCALE }
{      GL_POST_COLOR_MATRIX_GREEN_SCALE }
{      GL_POST_COLOR_MATRIX_BLUE_SCALE }
{      GL_POST_COLOR_MATRIX_ALPHA_SCALE }
{      GL_POST_COLOR_MATRIX_RED_BIAS }
{      GL_POST_COLOR_MATRIX_GREEN_BIAS }
{      GL_POST_COLOR_MATRIX_BLUE_BIAS }
{      GL_POST_COLOR_MATRIX_ALPHA_BIAS }

{ PixelType }
const GL_BITMAP                         = $1A00;
{      GL_BYTE }
{      GL_UNSIGNED_BYTE }
{      GL_SHORT }
{      GL_UNSIGNED_SHORT }
{      GL_INT }
{      GL_UNSIGNED_INT }
{      GL_FLOAT }
{      GL_BGR }
{      GL_BGRA }
{      GL_UNSIGNED_BYTE_3_3_2 }
{      GL_UNSIGNED_SHORT_4_4_4_4 }
{      GL_UNSIGNED_SHORT_5_5_5_1 }
{      GL_UNSIGNED_INT_8_8_8_8 }
{      GL_UNSIGNED_INT_10_10_10_2 }
{      GL_UNSIGNED_SHORT_5_6_5 }
{      GL_UNSIGNED_BYTE_2_3_3_REV }
{      GL_UNSIGNED_SHORT_5_6_5_REV }
{      GL_UNSIGNED_SHORT_4_4_4_4_REV }
{      GL_UNSIGNED_SHORT_1_5_5_5_REV }
{      GL_UNSIGNED_INT_8_8_8_8_REV }
{      GL_UNSIGNED_INT_2_10_10_10_REV }

{ PolygonMode }
const GL_POINT                          = $1B00;
const GL_LINE                           = $1B01;
const GL_FILL                           = $1B02;

{ ReadBufferMode }
{      GL_FRONT_LEFT }
{      GL_FRONT_RIGHT }
{      GL_BACK_LEFT }
{      GL_BACK_RIGHT }
{      GL_FRONT }
{      GL_BACK }
{      GL_LEFT }
{      GL_RIGHT }
{      GL_AUX0 }
{      GL_AUX1 }
{      GL_AUX2 }
{      GL_AUX3 }

{ RenderingMode }
const GL_RENDER                         = $1C00;
const GL_FEEDBACK                       = $1C01;
const GL_SELECT                         = $1C02;

{ SeparableTarget }
{      GL_SEPARABLE_2D }

{ ShadingModel }
const GL_FLAT                           = $1D00;
const GL_SMOOTH                         = $1D01;

{ StencilFunction }
{      GL_NEVER }
{      GL_LESS }
{      GL_EQUAL }
{      GL_LEQUAL }
{      GL_GREATER }
{      GL_NOTEQUAL }
{      GL_GEQUAL }
{      GL_ALWAYS }

{ StencilOp }
{      GL_ZERO }
const GL_KEEP                           = $1E00;
const GL_REPLACE                        = $1E01;
const GL_INCR                           = $1E02;
const GL_DECR                           = $1E03;
{      GL_INVERT }

{ StringName }
const GL_VENDOR                         = $1F00;
const GL_RENDERER                       = $1F01;
const GL_VERSION                        = $1F02;
const GL_EXTENSIONS                     = $1F03;

{ TextureCoordName }
const GL_S                              = $2000;
const GL_T                              = $2001;
const GL_R                              = $2002;
const GL_Q                              = $2003;

{ TexCoordPointerType }
{      GL_SHORT }
{      GL_INT }
{      GL_FLOAT }
{      GL_DOUBLE }

{ TextureEnvMode }
const GL_MODULATE                       = $2100;
const GL_DECAL                          = $2101;
{      GL_BLEND }
{      GL_REPLACE }

{ TextureEnvParameter }
const GL_TEXTURE_ENV_MODE               = $2200;
const GL_TEXTURE_ENV_COLOR              = $2201;

{ TextureEnvTarget }
const GL_TEXTURE_ENV                    = $2300;

{ TextureGenMode }
const GL_EYE_LINEAR                     = $2400;
const GL_OBJECT_LINEAR                  = $2401;
const GL_SPHERE_MAP                     = $2402;

{ TextureGenParameter }
const GL_TEXTURE_GEN_MODE               = $2500;
const GL_OBJECT_PLANE                   = $2501;
const GL_EYE_PLANE                      = $2502;

{ TextureMagFilter }
const GL_NEAREST                        = $2600;
const GL_LINEAR                         = $2601;

{ TextureMinFilter }
{      GL_NEAREST }
{      GL_LINEAR }
const GL_NEAREST_MIPMAP_NEAREST         = $2700;
const GL_LINEAR_MIPMAP_NEAREST          = $2701;
const GL_NEAREST_MIPMAP_LINEAR          = $2702;
const GL_LINEAR_MIPMAP_LINEAR           = $2703;

{ TextureParameterName }
const GL_TEXTURE_MAG_FILTER             = $2800;
const GL_TEXTURE_MIN_FILTER             = $2801;
const GL_TEXTURE_WRAP_S                 = $2802;
const GL_TEXTURE_WRAP_T                 = $2803;
{      GL_TEXTURE_BORDER_COLOR }
{      GL_TEXTURE_PRIORITY }
{      GL_TEXTURE_WRAP_R }
{      GL_TEXTURE_MIN_LOD }
{      GL_TEXTURE_MAX_LOD }
{      GL_TEXTURE_BASE_LEVEL }
{      GL_TEXTURE_MAX_LEVEL }

{ TextureTarget }
{      GL_TEXTURE_1D }
{      GL_TEXTURE_2D }
{      GL_PROXY_TEXTURE_1D }
{      GL_PROXY_TEXTURE_2D }
{      GL_TEXTURE_3D }
{      GL_PROXY_TEXTURE_3D }

{ TextureWrapMode }
const GL_CLAMP                          = $2900;
const GL_REPEAT                         = $2901;
{      GL_CLAMP_TO_EDGE }

{ VertexPointerType }
{      GL_SHORT }
{      GL_INT }
{      GL_FLOAT }
{      GL_DOUBLE }

{ ClientAttribMask }
const GL_CLIENT_PIXEL_STORE_BIT         = $00000001;
const GL_CLIENT_VERTEX_ARRAY_BIT        = $00000002;
const GL_CLIENT_ALL_ATTRIB_BITS         = $ffffffff;

{ polygon_offset }
const GL_POLYGON_OFFSET_FACTOR          = $8038;
const GL_POLYGON_OFFSET_UNITS           = $2A00;
const GL_POLYGON_OFFSET_POINT           = $2A01;
const GL_POLYGON_OFFSET_LINE            = $2A02;
const GL_POLYGON_OFFSET_FILL            = $8037;

{ texture }
const GL_ALPHA4                         = $803B;
const GL_ALPHA8                         = $803C;
const GL_ALPHA12                        = $803D;
const GL_ALPHA16                        = $803E;
const GL_LUMINANCE4                     = $803F;
const GL_LUMINANCE8                     = $8040;
const GL_LUMINANCE12                    = $8041;
const GL_LUMINANCE16                    = $8042;
const GL_LUMINANCE4_ALPHA4              = $8043;
const GL_LUMINANCE6_ALPHA2              = $8044;
const GL_LUMINANCE8_ALPHA8              = $8045;
const GL_LUMINANCE12_ALPHA4             = $8046;
const GL_LUMINANCE12_ALPHA12            = $8047;
const GL_LUMINANCE16_ALPHA16            = $8048;
const GL_INTENSITY                      = $8049;
const GL_INTENSITY4                     = $804A;
const GL_INTENSITY8                     = $804B;
const GL_INTENSITY12                    = $804C;
const GL_INTENSITY16                    = $804D;
const GL_R3_G3_B2                       = $2A10;
const GL_RGB4                           = $804F;
const GL_RGB5                           = $8050;
const GL_RGB8                           = $8051;
const GL_RGB10                          = $8052;
const GL_RGB12                          = $8053;
const GL_RGB16                          = $8054;
const GL_RGBA2                          = $8055;
const GL_RGBA4                          = $8056;
const GL_RGB5_A1                        = $8057;
const GL_RGBA8                          = $8058;
const GL_RGB10_A2                       = $8059;
const GL_RGBA12                         = $805A;
const GL_RGBA16                         = $805B;
const GL_TEXTURE_RED_SIZE               = $805C;
const GL_TEXTURE_GREEN_SIZE             = $805D;
const GL_TEXTURE_BLUE_SIZE              = $805E;
const GL_TEXTURE_ALPHA_SIZE             = $805F;
const GL_TEXTURE_LUMINANCE_SIZE         = $8060;
const GL_TEXTURE_INTENSITY_SIZE         = $8061;
const GL_PROXY_TEXTURE_1D               = $8063;
const GL_PROXY_TEXTURE_2D               = $8064;

{ texture_object }
const GL_TEXTURE_PRIORITY               = $8066;
const GL_TEXTURE_RESIDENT               = $8067;
const GL_TEXTURE_BINDING_1D             = $8068;
const GL_TEXTURE_BINDING_2D             = $8069;
const GL_TEXTURE_BINDING_3D             = $806A;

{ vertex_array }
const GL_VERTEX_ARRAY                   = $8074;
const GL_NORMAL_ARRAY                   = $8075;
const GL_COLOR_ARRAY                    = $8076;
const GL_INDEX_ARRAY                    = $8077;
const GL_TEXTURE_COORD_ARRAY            = $8078;
const GL_EDGE_FLAG_ARRAY                = $8079;
const GL_VERTEX_ARRAY_SIZE              = $807A;
const GL_VERTEX_ARRAY_TYPE              = $807B;
const GL_VERTEX_ARRAY_STRIDE            = $807C;
const GL_NORMAL_ARRAY_TYPE              = $807E;
const GL_NORMAL_ARRAY_STRIDE            = $807F;
const GL_COLOR_ARRAY_SIZE               = $8081;
const GL_COLOR_ARRAY_TYPE               = $8082;
const GL_COLOR_ARRAY_STRIDE             = $8083;
const GL_INDEX_ARRAY_TYPE               = $8085;
const GL_INDEX_ARRAY_STRIDE             = $8086;
const GL_TEXTURE_COORD_ARRAY_SIZE       = $8088;
const GL_TEXTURE_COORD_ARRAY_TYPE       = $8089;
const GL_TEXTURE_COORD_ARRAY_STRIDE     = $808A;
const GL_EDGE_FLAG_ARRAY_STRIDE         = $808C;
const GL_VERTEX_ARRAY_POINTER           = $808E;
const GL_NORMAL_ARRAY_POINTER           = $808F;
const GL_COLOR_ARRAY_POINTER            = $8090;
const GL_INDEX_ARRAY_POINTER            = $8091;
const GL_TEXTURE_COORD_ARRAY_POINTER    = $8092;
const GL_EDGE_FLAG_ARRAY_POINTER        = $8093;
const GL_V2F                            = $2A20;
const GL_V3F                            = $2A21;
const GL_C4UB_V2F                       = $2A22;
const GL_C4UB_V3F                       = $2A23;
const GL_C3F_V3F                        = $2A24;
const GL_N3F_V3F                        = $2A25;
const GL_C4F_N3F_V3F                    = $2A26;
const GL_T2F_V3F                        = $2A27;
const GL_T4F_V4F                        = $2A28;
const GL_T2F_C4UB_V3F                   = $2A29;
const GL_T2F_C3F_V3F                    = $2A2A;
const GL_T2F_N3F_V3F                    = $2A2B;
const GL_T2F_C4F_N3F_V3F                = $2A2C;
const GL_T4F_C4F_N3F_V4F                = $2A2D;

{ bgra }
const GL_BGR                            = $80E0;
const GL_BGRA                           = $80E1;

{ blend_color }
const GL_CONSTANT_COLOR                 = $8001;
const GL_ONE_MINUS_CONSTANT_COLOR       = $8002;
const GL_CONSTANT_ALPHA                 = $8003;
const GL_ONE_MINUS_CONSTANT_ALPHA       = $8004;
const GL_BLEND_COLOR                    = $8005;

{ blend_minmax }
const GL_FUNC_ADD                       = $8006;
const GL_MIN                            = $8007;
const GL_MAX                            = $8008;
const GL_BLEND_EQUATION                 = $8009;

{ blend_equation_separate }
const GL_BLEND_EQUATION_RGB             = $8009;
const GL_BLEND_EQUATION_ALPHA           = $883D;

{ blend_subtract }
const GL_FUNC_SUBTRACT                  = $800A;
const GL_FUNC_REVERSE_SUBTRACT          = $800B;

{ color_matrix }
const GL_COLOR_MATRIX                   = $80B1;
const GL_COLOR_MATRIX_STACK_DEPTH       = $80B2;
const GL_MAX_COLOR_MATRIX_STACK_DEPTH   = $80B3;
const GL_POST_COLOR_MATRIX_RED_SCALE    = $80B4;
const GL_POST_COLOR_MATRIX_GREEN_SCALE  = $80B5;
const GL_POST_COLOR_MATRIX_BLUE_SCALE   = $80B6;
const GL_POST_COLOR_MATRIX_ALPHA_SCALE  = $80B7;
const GL_POST_COLOR_MATRIX_RED_BIAS     = $80B8;
const GL_POST_COLOR_MATRIX_GREEN_BIAS   = $80B9;
const GL_POST_COLOR_MATRIX_BLUE_BIAS    = $80BA;
const GL_POST_COLOR_MATRIX_ALPHA_BIAS   = $80BB;

{ color_table }
const GL_COLOR_TABLE                    = $80D0;
const GL_POST_CONVOLUTION_COLOR_TABLE   = $80D1;
const GL_POST_COLOR_MATRIX_COLOR_TABLE  = $80D2;
const GL_PROXY_COLOR_TABLE              = $80D3;
const GL_PROXY_POST_CONVOLUTION_COLOR_TABLE = $80D4;
const GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE = $80D5;
const GL_COLOR_TABLE_SCALE              = $80D6;
const GL_COLOR_TABLE_BIAS               = $80D7;
const GL_COLOR_TABLE_FORMAT             = $80D8;
const GL_COLOR_TABLE_WIDTH              = $80D9;
const GL_COLOR_TABLE_RED_SIZE           = $80DA;
const GL_COLOR_TABLE_GREEN_SIZE         = $80DB;
const GL_COLOR_TABLE_BLUE_SIZE          = $80DC;
const GL_COLOR_TABLE_ALPHA_SIZE         = $80DD;
const GL_COLOR_TABLE_LUMINANCE_SIZE     = $80DE;
const GL_COLOR_TABLE_INTENSITY_SIZE     = $80DF;

{ convolution }
const GL_CONVOLUTION_1D                 = $8010;
const GL_CONVOLUTION_2D                 = $8011;
const GL_SEPARABLE_2D                   = $8012;
const GL_CONVOLUTION_BORDER_MODE        = $8013;
const GL_CONVOLUTION_FILTER_SCALE       = $8014;
const GL_CONVOLUTION_FILTER_BIAS        = $8015;
const GL_REDUCE                         = $8016;
const GL_CONVOLUTION_FORMAT             = $8017;
const GL_CONVOLUTION_WIDTH              = $8018;
const GL_CONVOLUTION_HEIGHT             = $8019;
const GL_MAX_CONVOLUTION_WIDTH          = $801A;
const GL_MAX_CONVOLUTION_HEIGHT         = $801B;
const GL_POST_CONVOLUTION_RED_SCALE     = $801C;
const GL_POST_CONVOLUTION_GREEN_SCALE   = $801D;
const GL_POST_CONVOLUTION_BLUE_SCALE    = $801E;
const GL_POST_CONVOLUTION_ALPHA_SCALE   = $801F;
const GL_POST_CONVOLUTION_RED_BIAS      = $8020;
const GL_POST_CONVOLUTION_GREEN_BIAS    = $8021;
const GL_POST_CONVOLUTION_BLUE_BIAS     = $8022;
const GL_POST_CONVOLUTION_ALPHA_BIAS    = $8023;
const GL_CONSTANT_BORDER                = $8151;
const GL_REPLICATE_BORDER               = $8153;
const GL_CONVOLUTION_BORDER_COLOR       = $8154;

{ draw_range_elements }
const GL_MAX_ELEMENTS_VERTICES          = $80E8;
const GL_MAX_ELEMENTS_INDICES           = $80E9;

{ histogram }
const GL_HISTOGRAM                      = $8024;
const GL_PROXY_HISTOGRAM                = $8025;
const GL_HISTOGRAM_WIDTH                = $8026;
const GL_HISTOGRAM_FORMAT               = $8027;
const GL_HISTOGRAM_RED_SIZE             = $8028;
const GL_HISTOGRAM_GREEN_SIZE           = $8029;
const GL_HISTOGRAM_BLUE_SIZE            = $802A;
const GL_HISTOGRAM_ALPHA_SIZE           = $802B;
const GL_HISTOGRAM_LUMINANCE_SIZE       = $802C;
const GL_HISTOGRAM_SINK                 = $802D;
const GL_MINMAX                         = $802E;
const GL_MINMAX_FORMAT                  = $802F;
const GL_MINMAX_SINK                    = $8030;
const GL_TABLE_TOO_LARGE                = $8031;

{ packed_pixels }
const GL_UNSIGNED_BYTE_3_3_2            = $8032;
const GL_UNSIGNED_SHORT_4_4_4_4         = $8033;
const GL_UNSIGNED_SHORT_5_5_5_1         = $8034;
const GL_UNSIGNED_INT_8_8_8_8           = $8035;
const GL_UNSIGNED_INT_10_10_10_2        = $8036;
const GL_UNSIGNED_BYTE_2_3_3_REV        = $8362;
const GL_UNSIGNED_SHORT_5_6_5           = $8363;
const GL_UNSIGNED_SHORT_5_6_5_REV       = $8364;
const GL_UNSIGNED_SHORT_4_4_4_4_REV     = $8365;
const GL_UNSIGNED_SHORT_1_5_5_5_REV     = $8366;
const GL_UNSIGNED_INT_8_8_8_8_REV       = $8367;
const GL_UNSIGNED_INT_2_10_10_10_REV    = $8368;

{ rescale_normal }
const GL_RESCALE_NORMAL                 = $803A;

{ separate_specular_color }
const GL_LIGHT_MODEL_COLOR_CONTROL      = $81F8;
const GL_SINGLE_COLOR                   = $81F9;
const GL_SEPARATE_SPECULAR_COLOR        = $81FA;

{ texture3D }
const GL_PACK_SKIP_IMAGES               = $806B;
const GL_PACK_IMAGE_HEIGHT              = $806C;
const GL_UNPACK_SKIP_IMAGES             = $806D;
const GL_UNPACK_IMAGE_HEIGHT            = $806E;
const GL_TEXTURE_3D                     = $806F;
const GL_PROXY_TEXTURE_3D               = $8070;
const GL_TEXTURE_DEPTH                  = $8071;
const GL_TEXTURE_WRAP_R                 = $8072;
const GL_MAX_3D_TEXTURE_SIZE            = $8073;

{ texture_edge_clamp }
const GL_CLAMP_TO_EDGE                  = $812F;
const GL_CLAMP_TO_BORDER                = $812D;

{ texture_lod }
const GL_TEXTURE_MIN_LOD                = $813A;
const GL_TEXTURE_MAX_LOD                = $813B;
const GL_TEXTURE_BASE_LEVEL             = $813C;
const GL_TEXTURE_MAX_LEVEL              = $813D;

{ GetTarget1_2 }
const GL_SMOOTH_POINT_SIZE_RANGE        = $0B12;
const GL_SMOOTH_POINT_SIZE_GRANULARITY  = $0B13;
const GL_SMOOTH_LINE_WIDTH_RANGE        = $0B22;
const GL_SMOOTH_LINE_WIDTH_GRANULARITY  = $0B23;
const GL_ALIASED_POINT_SIZE_RANGE       = $846D;
const GL_ALIASED_LINE_WIDTH_RANGE       = $846E;

const GL_TEXTURE0                       = $84C0;
const GL_TEXTURE1                       = $84C1;
const GL_TEXTURE2                       = $84C2;
const GL_TEXTURE3                       = $84C3;
const GL_TEXTURE4                       = $84C4;
const GL_TEXTURE5                       = $84C5;
const GL_TEXTURE6                       = $84C6;
const GL_TEXTURE7                       = $84C7;
const GL_TEXTURE8                       = $84C8;
const GL_TEXTURE9                       = $84C9;
const GL_TEXTURE10                      = $84CA;
const GL_TEXTURE11                      = $84CB;
const GL_TEXTURE12                      = $84CC;
const GL_TEXTURE13                      = $84CD;
const GL_TEXTURE14                      = $84CE;
const GL_TEXTURE15                      = $84CF;
const GL_TEXTURE16                      = $84D0;
const GL_TEXTURE17                      = $84D1;
const GL_TEXTURE18                      = $84D2;
const GL_TEXTURE19                      = $84D3;
const GL_TEXTURE20                      = $84D4;
const GL_TEXTURE21                      = $84D5;
const GL_TEXTURE22                      = $84D6;
const GL_TEXTURE23                      = $84D7;
const GL_TEXTURE24                      = $84D8;
const GL_TEXTURE25                      = $84D9;
const GL_TEXTURE26                      = $84DA;
const GL_TEXTURE27                      = $84DB;
const GL_TEXTURE28                      = $84DC;
const GL_TEXTURE29                      = $84DD;
const GL_TEXTURE30                      = $84DE;
const GL_TEXTURE31                      = $84DF;
const GL_ACTIVE_TEXTURE                 = $84E0;
const GL_CLIENT_ACTIVE_TEXTURE          = $84E1;
const GL_MAX_TEXTURE_UNITS              = $84E2;

const GL_COMBINE                        = $8570;
const GL_COMBINE_RGB                    = $8571;
const GL_COMBINE_ALPHA                  = $8572;
const GL_RGB_SCALE                      = $8573;
const GL_ADD_SIGNED                     = $8574;
const GL_INTERPOLATE                    = $8575;
const GL_CONSTANT                       = $8576;
const GL_PRIMARY_COLOR                  = $8577;
const GL_PREVIOUS                       = $8578;
const GL_SUBTRACT                       = $84E7;

const GL_SRC0_RGB                       = $8580;
const GL_SRC1_RGB                       = $8581;
const GL_SRC2_RGB                       = $8582;
const GL_SRC3_RGB                       = $8583;
const GL_SRC4_RGB                       = $8584;
const GL_SRC5_RGB                       = $8585;
const GL_SRC6_RGB                       = $8586;
const GL_SRC7_RGB                       = $8587;
const GL_SRC0_ALPHA                     = $8588;
const GL_SRC1_ALPHA                     = $8589;
const GL_SRC2_ALPHA                     = $858A;
const GL_SRC3_ALPHA                     = $858B;
const GL_SRC4_ALPHA                     = $858C;
const GL_SRC5_ALPHA                     = $858D;
const GL_SRC6_ALPHA                     = $858E;
const GL_SRC7_ALPHA                     = $858F;

{ Obsolete }
const GL_SOURCE0_RGB                    = $8580;
const GL_SOURCE1_RGB                    = $8581;
const GL_SOURCE2_RGB                    = $8582;
const GL_SOURCE3_RGB                    = $8583;
const GL_SOURCE4_RGB                    = $8584;
const GL_SOURCE5_RGB                    = $8585;
const GL_SOURCE6_RGB                    = $8586;
const GL_SOURCE7_RGB                    = $8587;
const GL_SOURCE0_ALPHA                  = $8588;
const GL_SOURCE1_ALPHA                  = $8589;
const GL_SOURCE2_ALPHA                  = $858A;
const GL_SOURCE3_ALPHA                  = $858B;
const GL_SOURCE4_ALPHA                  = $858C;
const GL_SOURCE5_ALPHA                  = $858D;
const GL_SOURCE6_ALPHA                  = $858E;
const GL_SOURCE7_ALPHA                  = $858F;

const GL_OPERAND0_RGB                   = $8590;
const GL_OPERAND1_RGB                   = $8591;
const GL_OPERAND2_RGB                   = $8592;
const GL_OPERAND3_RGB                   = $8593;
const GL_OPERAND4_RGB                   = $8594;
const GL_OPERAND5_RGB                   = $8595;
const GL_OPERAND6_RGB                   = $8596;
const GL_OPERAND7_RGB                   = $8597;
const GL_OPERAND0_ALPHA                 = $8598;
const GL_OPERAND1_ALPHA                 = $8599;
const GL_OPERAND2_ALPHA                 = $859A;
const GL_OPERAND3_ALPHA                 = $859B;
const GL_OPERAND4_ALPHA                 = $859C;
const GL_OPERAND5_ALPHA                 = $859D;
const GL_OPERAND6_ALPHA                 = $859E;
const GL_OPERAND7_ALPHA                 = $859F;

const GL_DOT3_RGB                       = $86AE;
const GL_DOT3_RGBA                      = $86AF;

const GL_TRANSPOSE_MODELVIEW_MATRIX     = $84E3;
const GL_TRANSPOSE_PROJECTION_MATRIX    = $84E4;
const GL_TRANSPOSE_TEXTURE_MATRIX       = $84E5;
const GL_TRANSPOSE_COLOR_MATRIX         = $84E6;

const GL_NORMAL_MAP                     = $8511;
const GL_REFLECTION_MAP                 = $8512;
const GL_TEXTURE_CUBE_MAP               = $8513;
const GL_TEXTURE_BINDING_CUBE_MAP       = $8514;
const GL_TEXTURE_CUBE_MAP_POSITIVE_X    = $8515;
const GL_TEXTURE_CUBE_MAP_NEGATIVE_X    = $8516;
const GL_TEXTURE_CUBE_MAP_POSITIVE_Y    = $8517;
const GL_TEXTURE_CUBE_MAP_NEGATIVE_Y    = $8518;
const GL_TEXTURE_CUBE_MAP_POSITIVE_Z    = $8519;
const GL_TEXTURE_CUBE_MAP_NEGATIVE_Z    = $851A;
const GL_PROXY_TEXTURE_CUBE_MAP         = $851B;
const GL_MAX_CUBE_MAP_TEXTURE_SIZE      = $851C;

const GL_COMPRESSED_ALPHA               = $84E9;
const GL_COMPRESSED_LUMINANCE           = $84EA;
const GL_COMPRESSED_LUMINANCE_ALPHA     = $84EB;
const GL_COMPRESSED_INTENSITY           = $84EC;
const GL_COMPRESSED_RGB                 = $84ED;
const GL_COMPRESSED_RGBA                = $84EE;
const GL_TEXTURE_COMPRESSION_HINT       = $84EF;
const GL_TEXTURE_COMPRESSED_IMAGE_SIZE  = $86A0;
const GL_TEXTURE_COMPRESSED             = $86A1;
const GL_NUM_COMPRESSED_TEXTURE_FORMATS = $86A2;
const GL_COMPRESSED_TEXTURE_FORMATS     = $86A3;

const GL_MULTISAMPLE                    = $809D;
const GL_SAMPLE_ALPHA_TO_COVERAGE       = $809E;
const GL_SAMPLE_ALPHA_TO_ONE            = $809F;
const GL_SAMPLE_COVERAGE                = $80A0;
const GL_SAMPLE_BUFFERS                 = $80A8;
const GL_SAMPLES                        = $80A9;
const GL_SAMPLE_COVERAGE_VALUE          = $80AA;
const GL_SAMPLE_COVERAGE_INVERT         = $80AB;
const GL_MULTISAMPLE_BIT                = $20000000;

const GL_DEPTH_COMPONENT16              = $81A5;
const GL_DEPTH_COMPONENT24              = $81A6;
const GL_DEPTH_COMPONENT32              = $81A7;
const GL_TEXTURE_DEPTH_SIZE             = $884A;
const GL_DEPTH_TEXTURE_MODE             = $884B;

const GL_TEXTURE_COMPARE_MODE           = $884C;
const GL_TEXTURE_COMPARE_FUNC           = $884D;
const GL_COMPARE_R_TO_TEXTURE           = $884E;

{ occlusion_query }
const GL_QUERY_COUNTER_BITS             = $8864;
const GL_CURRENT_QUERY                  = $8865;
const GL_QUERY_RESULT                   = $8866;
const GL_QUERY_RESULT_AVAILABLE         = $8867;
const GL_SAMPLES_PASSED                 = $8914;

const GL_FOG_COORD_SRC                  = $8450;
const GL_FOG_COORD                      = $8451;
const GL_FRAGMENT_DEPTH                 = $8452;
const GL_CURRENT_FOG_COORD              = $8453;  
const GL_FOG_COORD_ARRAY_TYPE           = $8454;
const GL_FOG_COORD_ARRAY_STRIDE         = $8455;
const GL_FOG_COORD_ARRAY_POINTER        = $8456;
const GL_FOG_COORD_ARRAY                = $8457;

{ Obsolete }
const GL_FOG_COORDINATE_SOURCE          = $8450;
const GL_FOG_COORDINATE                 = $8451;
const GL_CURRENT_FOG_COORDINATE         = $8453;  
const GL_FOG_COORDINATE_ARRAY_TYPE      = $8454;
const GL_FOG_COORDINATE_ARRAY_STRIDE    = $8455;
const GL_FOG_COORDINATE_ARRAY_POINTER   = $8456;
const GL_FOG_COORDINATE_ARRAY           = $8457;

const GL_COLOR_SUM                      = $8458;
const GL_CURRENT_SECONDARY_COLOR        = $8459;
const GL_SECONDARY_COLOR_ARRAY_SIZE     = $845A;
const GL_SECONDARY_COLOR_ARRAY_TYPE     = $845B;
const GL_SECONDARY_COLOR_ARRAY_STRIDE   = $845C;
const GL_SECONDARY_COLOR_ARRAY_POINTER  = $845D;
const GL_SECONDARY_COLOR_ARRAY          = $845E;

const GL_POINT_SIZE_MIN                 = $8126;
const GL_POINT_SIZE_MAX                 = $8127;
const GL_POINT_FADE_THRESHOLD_SIZE      = $8128;
const GL_POINT_DISTANCE_ATTENUATION     = $8129;

const GL_BLEND_DST_RGB                  = $80C8;
const GL_BLEND_SRC_RGB                  = $80C9;
const GL_BLEND_DST_ALPHA                = $80CA;
const GL_BLEND_SRC_ALPHA                = $80CB;

const GL_GENERATE_MIPMAP                = $8191;
const GL_GENERATE_MIPMAP_HINT           = $8192;

const GL_INCR_WRAP                      = $8507;
const GL_DECR_WRAP                      = $8508;

const GL_MIRRORED_REPEAT                = $8370;

const GL_MAX_TEXTURE_LOD_BIAS           = $84FD;
const GL_TEXTURE_FILTER_CONTROL         = $8500;
const GL_TEXTURE_LOD_BIAS               = $8501;

{ vertex_buffer_object }
const GL_ARRAY_BUFFER                                = $8892;
const GL_ELEMENT_ARRAY_BUFFER                        = $8893;
const GL_ARRAY_BUFFER_BINDING                        = $8894;
const GL_ELEMENT_ARRAY_BUFFER_BINDING                = $8895;
const GL_VERTEX_ARRAY_BUFFER_BINDING                 = $8896;
const GL_NORMAL_ARRAY_BUFFER_BINDING                 = $8897;
const GL_COLOR_ARRAY_BUFFER_BINDING                  = $8898;
const GL_INDEX_ARRAY_BUFFER_BINDING                  = $8899;
const GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING          = $889A;
const GL_EDGE_FLAG_ARRAY_BUFFER_BINDING              = $889B;
const GL_SECONDARY_COLOR_ARRAY_BUFFER_BINDING        = $889C;
const GL_FOG_COORD_ARRAY_BUFFER_BINDING              = $889D;
const GL_WEIGHT_ARRAY_BUFFER_BINDING                 = $889E;
const GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING          = $889F;
const GL_STREAM_DRAW                                 = $88E0;
const GL_STREAM_READ                                 = $88E1;
const GL_STREAM_COPY                                 = $88E2;
const GL_STATIC_DRAW                                 = $88E4;
const GL_STATIC_READ                                 = $88E5;
const GL_STATIC_COPY                                 = $88E6;
const GL_DYNAMIC_DRAW                                = $88E8;
const GL_DYNAMIC_READ                                = $88E9;
const GL_DYNAMIC_COPY                                = $88EA;
const GL_READ_ONLY                                   = $88B8;
const GL_WRITE_ONLY                                  = $88B9;
const GL_READ_WRITE                                  = $88BA;
const GL_BUFFER_SIZE                                 = $8764;
const GL_BUFFER_USAGE                                = $8765;
const GL_BUFFER_ACCESS                               = $88BB;
const GL_BUFFER_MAPPED                               = $88BC;
const GL_BUFFER_MAP_POINTER                          = $88BD;
{ Obsolete }
const GL_FOG_COORDINATE_ARRAY_BUFFER_BINDING         = $889D;

{ OpenGL 2.0 }
const GL_CURRENT_PROGRAM                = $8B8D;
const GL_SHADER_TYPE                    = $8B4F;
const GL_DELETE_STATUS                  = $8B80;
const GL_COMPILE_STATUS                 = $8B81;
const GL_LINK_STATUS                    = $8B82;
const GL_VALIDATE_STATUS                = $8B83;
const GL_INFO_LOG_LENGTH                = $8B84;
const GL_ATTACHED_SHADERS               = $8B85;
const GL_ACTIVE_UNIFORMS                = $8B86;
const GL_ACTIVE_UNIFORM_MAX_LENGTH      = $8B87;
const GL_SHADER_SOURCE_LENGTH           = $8B88;
const GL_FLOAT_VEC2                     = $8B50;
const GL_FLOAT_VEC3                     = $8B51;
const GL_FLOAT_VEC4                     = $8B52;
const GL_INT_VEC2                       = $8B53;
const GL_INT_VEC3                       = $8B54;
const GL_INT_VEC4                       = $8B55;
const GL_BOOL                           = $8B56;
const GL_BOOL_VEC2                      = $8B57;
const GL_BOOL_VEC3                      = $8B58;
const GL_BOOL_VEC4                      = $8B59;
const GL_FLOAT_MAT2                     = $8B5A;
const GL_FLOAT_MAT3                     = $8B5B;
const GL_FLOAT_MAT4                     = $8B5C;
const GL_SAMPLER_1D                     = $8B5D;
const GL_SAMPLER_2D                     = $8B5E;
const GL_SAMPLER_3D                     = $8B5F;
const GL_SAMPLER_CUBE                   = $8B60;
const GL_SAMPLER_1D_SHADOW              = $8B61;
const GL_SAMPLER_2D_SHADOW              = $8B62;
const GL_SHADING_LANGUAGE_VERSION       = $8B8C;
const GL_VERTEX_SHADER                  = $8B31;
const GL_MAX_VERTEX_UNIFORM_COMPONENTS  = $8B4A;
const GL_MAX_VARYING_FLOATS             = $8B4B;
const GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS = $8B4C;
const GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = $8B4D;
const GL_ACTIVE_ATTRIBUTES              = $8B89;
const GL_ACTIVE_ATTRIBUTE_MAX_LENGTH    = $8B8A;
const GL_FRAGMENT_SHADER                = $8B30;
const GL_MAX_FRAGMENT_UNIFORM_COMPONENTS = $8B49;
const GL_FRAGMENT_SHADER_DERIVATIVE_HINT = $8B8B;
const GL_MAX_VERTEX_ATTRIBS             = $8869;
const GL_VERTEX_ATTRIB_ARRAY_ENABLED    = $8622;
const GL_VERTEX_ATTRIB_ARRAY_SIZE       = $8623;
const GL_VERTEX_ATTRIB_ARRAY_STRIDE     = $8624;
const GL_VERTEX_ATTRIB_ARRAY_TYPE       = $8625;
const GL_VERTEX_ATTRIB_ARRAY_NORMALIZED = $886A;
const GL_CURRENT_VERTEX_ATTRIB          = $8626;
const GL_VERTEX_ATTRIB_ARRAY_POINTER    = $8645;
const GL_VERTEX_PROGRAM_POINT_SIZE      = $8642;
const GL_VERTEX_PROGRAM_TWO_SIDE        = $8643;
const GL_MAX_TEXTURE_COORDS             = $8871;
const GL_MAX_TEXTURE_IMAGE_UNITS        = $8872;
const GL_MAX_DRAW_BUFFERS               = $8824;
const GL_DRAW_BUFFER0                   = $8825;
const GL_DRAW_BUFFER1                   = $8826;
const GL_DRAW_BUFFER2                   = $8827;
const GL_DRAW_BUFFER3                   = $8828;
const GL_DRAW_BUFFER4                   = $8829;
const GL_DRAW_BUFFER5                   = $882A;
const GL_DRAW_BUFFER6                   = $882B;
const GL_DRAW_BUFFER7                   = $882C;
const GL_DRAW_BUFFER8                   = $882D;
const GL_DRAW_BUFFER9                   = $882E;
const GL_DRAW_BUFFER10                  = $882F;
const GL_DRAW_BUFFER11                  = $8830;
const GL_DRAW_BUFFER12                  = $8831;
const GL_DRAW_BUFFER13                  = $8832;
const GL_DRAW_BUFFER14                  = $8833;
const GL_DRAW_BUFFER15                  = $8834;
const GL_POINT_SPRITE                   = $8861;
const GL_COORD_REPLACE                  = $8862;
const GL_POINT_SPRITE_COORD_ORIGIN      = $8CA0;
const GL_LOWER_LEFT                     = $8CA1;
const GL_UPPER_LEFT                     = $8CA2;
const GL_STENCIL_BACK_FUNC              = $8800;
const GL_STENCIL_BACK_VALUE_MASK        = $8CA4;
const GL_STENCIL_BACK_REF               = $8CA3;
const GL_STENCIL_BACK_FAIL              = $8801;
const GL_STENCIL_BACK_PASS_DEPTH_FAIL   = $8802;
const GL_STENCIL_BACK_PASS_DEPTH_PASS   = $8803;
const GL_STENCIL_BACK_WRITEMASK         = $8CA5;

{ OpenGL 2.1 }
const GL_CURRENT_RASTER_SECONDARY_COLOR = $845F;
const GL_PIXEL_PACK_BUFFER              = $88EB;
const GL_PIXEL_UNPACK_BUFFER            = $88EC;
const GL_PIXEL_PACK_BUFFER_BINDING      = $88ED;
const GL_PIXEL_UNPACK_BUFFER_BINDING    = $88EF;
const GL_FLOAT_MAT2x3                   = $8B65;
const GL_FLOAT_MAT2x4                   = $8B66;
const GL_FLOAT_MAT3x2                   = $8B67;
const GL_FLOAT_MAT3x4                   = $8B68;
const GL_FLOAT_MAT4x2                   = $8B69;
const GL_FLOAT_MAT4x3                   = $8B6A;
const GL_SRGB                           = $8C40;
const GL_SRGB8                          = $8C41;
const GL_SRGB_ALPHA                     = $8C42;
const GL_SRGB8_ALPHA8                   = $8C43;
const GL_SLUMINANCE_ALPHA               = $8C44;
const GL_SLUMINANCE8_ALPHA8             = $8C45;
const GL_SLUMINANCE                     = $8C46;
const GL_SLUMINANCE8                    = $8C47;
const GL_COMPRESSED_SRGB                = $8C48;
const GL_COMPRESSED_SRGB_ALPHA          = $8C49;
const GL_COMPRESSED_SLUMINANCE          = $8C4A;
const GL_COMPRESSED_SLUMINANCE_ALPHA    = $8C4B;

{***********************************************************}

{$ifc defined GL_GLEXT_FUNCTION_POINTERS and GL_GLEXT_FUNCTION_POINTERS}
type
	glAccumProcPtr = procedure( op: GLenum; value: GLfloat );
	glAlphaFuncProcPtr = procedure( func: GLenum; ref: GLclampf );
	glAreTexturesResidentProcPtr = function( n: GLsizei; const textures: PGLuint; residences: PGLboolean ): GLboolean;
	glArrayElementProcPtr = procedure( i: GLint );
	glBeginProcPtr = procedure( mode: GLenum );
	glBindTextureProcPtr = procedure( target: GLenum; texture: GLuint );
	glBitmapProcPtr = procedure( width: GLsizei; height: GLsizei; xorig: GLfloat; yorig: GLfloat; xmove: GLfloat; ymove: GLfloat; const bitmap: PGLubyte );
	glBlendColorProcPtr = procedure( red: GLclampf; green: GLclampf; blue: GLclampf; alpha: GLclampf );
	glBlendEquationProcPtr = procedure( mode: GLenum );
	glBlendEquationSeparateProcPtr = procedure( modeRGB: GLenum; modeAlpha: GLenum );
	glBlendFuncProcPtr = procedure( sfactor: GLenum; dfactor: GLenum );
	glCallListProcPtr = procedure( list: GLuint );
	glCallListsProcPtr = procedure( n: GLsizei; typ: GLenum; lists: univ ptr );
	glClearProcPtr = procedure( mask: GLbitfield );
	glClearAccumProcPtr = procedure( red: GLfloat; green: GLfloat; blue: GLfloat; alpha: GLfloat );
	glClearColorProcPtr = procedure( red: GLclampf; green: GLclampf; blue: GLclampf; alpha: GLclampf );
	glClearDepthProcPtr = procedure( depth: GLclampd );
	glClearIndexProcPtr = procedure( c: GLfloat );
	glClearStencilProcPtr = procedure( s: GLint );
	glClipPlaneProcPtr = procedure( plane: GLenum; const equation: PGLdouble );
	glColor3bProcPtr = procedure( red: GLbyte; green: GLbyte; blue: GLbyte );
	glColor3bvProcPtr = procedure( const v: PGLbyte );
	glColor3dProcPtr = procedure( red: GLdouble; green: GLdouble; blue: GLdouble );
	glColor3dvProcPtr = procedure( const v: PGLdouble );
	glColor3fProcPtr = procedure( red: GLfloat; green: GLfloat; blue: GLfloat );
	glColor3fvProcPtr = procedure( const v: PGLfloat );
	glColor3iProcPtr = procedure( red: GLint; green: GLint; blue: GLint );
	glColor3ivProcPtr = procedure( const v: PGLint );
	glColor3sProcPtr = procedure( red: GLshort; green: GLshort; blue: GLshort );
	glColor3svProcPtr = procedure( const v: PGLshort );
	glColor3ubProcPtr = procedure( red: GLubyte; green: GLubyte; blue: GLubyte );
	glColor3ubvProcPtr = procedure( const v: PGLubyte );
	glColor3uiProcPtr = procedure( red: GLuint; green: GLuint; blue: GLuint );
	glColor3uivProcPtr = procedure( const v: PGLuint );
	glColor3usProcPtr = procedure( red: GLushort; green: GLushort; blue: GLushort );
	glColor3usvProcPtr = procedure( const v: PGLushort );
	glColor4bProcPtr = procedure( red: GLbyte; green: GLbyte; blue: GLbyte; alpha: GLbyte );
	glColor4bvProcPtr = procedure( const v: PGLbyte );
	glColor4dProcPtr = procedure( red: GLdouble; green: GLdouble; blue: GLdouble; alpha: GLdouble );
	glColor4dvProcPtr = procedure( const v: PGLdouble );
	glColor4fProcPtr = procedure( red: GLfloat; green: GLfloat; blue: GLfloat; alpha: GLfloat );
	glColor4fvProcPtr = procedure( const v: PGLfloat );
	glColor4iProcPtr = procedure( red: GLint; green: GLint; blue: GLint; alpha: GLint );
	glColor4ivProcPtr = procedure( const v: PGLint );
	glColor4sProcPtr = procedure( red: GLshort; green: GLshort; blue: GLshort; alpha: GLshort );
	glColor4svProcPtr = procedure( const v: PGLshort );
	glColor4ubProcPtr = procedure( red: GLubyte; green: GLubyte; blue: GLubyte; alpha: GLubyte );
	glColor4ubvProcPtr = procedure( const v: PGLubyte );
	glColor4uiProcPtr = procedure( red: GLuint; green: GLuint; blue: GLuint; alpha: GLuint );
	glColor4uivProcPtr = procedure( const v: PGLuint );
	glColor4usProcPtr = procedure( red: GLushort; green: GLushort; blue: GLushort; alpha: GLushort );
	glColor4usvProcPtr = procedure( const v: PGLushort );
	glColorMaskProcPtr = procedure( red: GLboolean; green: GLboolean; blue: GLboolean; alpha: GLboolean );
	glColorMaterialProcPtr = procedure( face: GLenum; mode: GLenum );
	glColorPointerProcPtr = procedure( size: GLint; typ: GLenum; stride: GLsizei; pointr: univ Ptr );
	glColorSubTableProcPtr = procedure( target: GLenum; start: GLsizei; count: GLsizei; format: GLenum; typ: GLenum; const data: univ Ptr );
	glColorTableProcPtr = procedure( target: GLenum; internalformat: GLenum; width: GLsizei; format: GLenum; typ: GLenum; const table: univ Ptr );
	glColorTableParameterfvProcPtr = procedure( target: GLenum; pname: GLenum; const params: PGLfloat );
	glColorTableParameterivProcPtr = procedure( target: GLenum; pname: GLenum; const params: PGLint );
	glConvolutionFilter1DProcPtr = procedure( target: GLenum; internalformat: GLenum; width: GLsizei; format: GLenum; typ: GLenum; const image: univ Ptr );
	glConvolutionFilter2DProcPtr = procedure( target: GLenum; internalformat: GLenum; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; const image: univ Ptr );
	glConvolutionParameterfProcPtr = procedure( target: GLenum; pname: GLenum; params: GLfloat );
	glConvolutionParameterfvProcPtr = procedure( target: GLenum; pname: GLenum; const params: PGLfloat );
	glConvolutionParameteriProcPtr = procedure( target: GLenum; pname: GLenum; params: GLint );
	glConvolutionParameterivProcPtr = procedure( target: GLenum; pname: GLenum; const params: PGLint );
	glCopyColorSubTableProcPtr = procedure( target: GLenum; start: GLsizei; x: GLint; y: GLint; width: GLsizei );
	glCopyColorTableProcPtr = procedure( target: GLenum; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei );
	glCopyConvolutionFilter1DProcPtr = procedure( target: GLenum; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei );
	glCopyConvolutionFilter2DProcPtr = procedure( target: GLenum; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei; height: GLsizei );
	glCopyPixelsProcPtr = procedure( x: GLint; y: GLint; width: GLsizei; height: GLsizei; typ: GLenum );
	glCopyTexImage1DProcPtr = procedure( target: GLenum; level: GLint; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei; border: GLint );
	glCopyTexImage2DProcPtr = procedure( target: GLenum; level: GLint; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei; height: GLsizei; border: GLint );
	glCopyTexSubImage1DProcPtr = procedure( target: GLenum; level: GLint; xoffset: GLint; x: GLint; y: GLint; width: GLsizei );
	glCopyTexSubImage2DProcPtr = procedure( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; x: GLint; y: GLint; width: GLsizei; height: GLsizei );
	glCopyTexSubImage3DProcPtr = procedure( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; x: GLint; y: GLint; width: GLsizei; height: GLsizei );
	glCullFaceProcPtr = procedure( mode: GLenum );
	glDeleteListsProcPtr = procedure( list: GLuint; range: GLsizei );
	glDeleteTexturesProcPtr = procedure( n: GLsizei; const textures: PGLuint );
	glDepthFuncProcPtr = procedure( func: GLenum );
	glDepthMaskProcPtr = procedure( flag: GLboolean );
	glDepthRangeProcPtr = procedure( zNear: GLclampd; zFar: GLclampd );
	glDisableProcPtr = procedure( cap: GLenum );
	glDisableClientStateProcPtr = procedure( arry: GLenum );
	glDrawArraysProcPtr = procedure( mode: GLenum; first: GLint; count: GLsizei );
	glDrawBufferProcPtr = procedure( mode: GLenum );
	glDrawElementsProcPtr = procedure( mode: GLenum; count: GLsizei; typ: GLenum; const indices: univ Ptr );
	glDrawPixelsProcPtr = procedure( width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; const pixels: univ Ptr );
	glDrawRangeElementsProcPtr = procedure( mode: GLenum; start: GLuint; finish: GLuint; count: GLsizei; typ: GLenum; const indices: univ Ptr );
	glEdgeFlagProcPtr = procedure( flag: GLboolean );
	glEdgeFlagPointerProcPtr = procedure( stride: GLsizei; const pointr: univ Ptr );
	glEdgeFlagvProcPtr = procedure( const flag: PGLboolean );
	glEnableProcPtr = procedure( cap: GLenum );
	glEnableClientStateProcPtr = procedure( arry: GLenum );
	glEndProcPtr = procedure;
	glEndListProcPtr = procedure;
	glEvalCoord1dProcPtr = procedure( u: GLdouble );
	glEvalCoord1dvProcPtr = procedure( const u: PGLdouble );
	glEvalCoord1fProcPtr = procedure( u: GLfloat );
	glEvalCoord1fvProcPtr = procedure( const u: PGLfloat );
	glEvalCoord2dProcPtr = procedure( u: GLdouble; v: GLdouble );
	glEvalCoord2dvProcPtr = procedure( const u: PGLdouble );
	glEvalCoord2fProcPtr = procedure( u: GLfloat; v: GLfloat );
	glEvalCoord2fvProcPtr = procedure( const u: PGLfloat );
	glEvalMesh1ProcPtr = procedure( mode: GLenum; i1: GLint; i2: GLint );
	glEvalMesh2ProcPtr = procedure( mode: GLenum; i1: GLint; i2: GLint; j1: GLint; j2: GLint );
	glEvalPoint1ProcPtr = procedure( i: GLint );
	glEvalPoint2ProcPtr = procedure( i: GLint; j: GLint );
	glFeedbackBufferProcPtr = procedure( size: GLsizei; typ: GLenum; buffer: PGLfloat );
	glFinishProcPtr = procedure;
	glFlushProcPtr = procedure;
	glFogfProcPtr = procedure( pname: GLenum; param: GLfloat );
	glFogfvProcPtr = procedure( pname: GLenum; const params: PGLfloat );
	glFogiProcPtr = procedure( pname: GLenum; param: GLint );
	glFogivProcPtr = procedure( pname: GLenum; const params: PGLint );
	glFrontFaceProcPtr = procedure( mode: GLenum );
	glFrustumProcPtr = procedure( left: GLdouble; right: GLdouble; bottom: GLdouble; top: GLdouble; zNear: GLdouble; zFar: GLdouble );
	glGenListsProcPtr = function( range: GLsizei ): GLuint;
	glGenTexturesProcPtr = procedure( n: GLsizei; textures: PGLuint );
	glGetBooleanvProcPtr = procedure( pname: GLenum; params: PGLboolean );
	glGetClipPlaneProcPtr = procedure( plane: GLenum; equation: PGLdouble );
	glGetColorTableProcPtr = procedure( target: GLenum; format: GLenum; typ: GLenum; table: univ Ptr );
	glGetColorTableParameterfvProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLfloat );
	glGetColorTableParameterivProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLint );
	glGetConvolutionFilterProcPtr = procedure( target: GLenum; format: GLenum; typ: GLenum; image: univ Ptr );
	glGetConvolutionParameterfvProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLfloat );
	glGetConvolutionParameterivProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLint );
	glGetDoublevProcPtr = procedure( pname: GLenum; params: PGLdouble );
	glGetErrorProcPtr = function: GLenum;
	glGetFloatvProcPtr = procedure( pname: GLenum; params: PGLfloat );
	glGetHistogramProcPtr = procedure( target: GLenum; reset: GLboolean; format: GLenum; typ: GLenum; values: univ Ptr );
	glGetHistogramParameterfvProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLfloat );
	glGetHistogramParameterivProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLint );
	glGetIntegervProcPtr = procedure( pname: GLenum; params: PGLint );
	glGetLightfvProcPtr = procedure( light: GLenum; pname: GLenum; params: PGLfloat );
	glGetLightivProcPtr = procedure( light: GLenum; pname: GLenum; params: PGLint );
	glGetMapdvProcPtr = procedure( target: GLenum; query: GLenum; v: PGLdouble );
	glGetMapfvProcPtr = procedure( target: GLenum; query: GLenum; v: PGLfloat );
	glGetMapivProcPtr = procedure( target: GLenum; query: GLenum; v: PGLint );
	glGetMaterialfvProcPtr = procedure( face: GLenum; pname: GLenum; params: PGLfloat );
	glGetMaterialivProcPtr = procedure( face: GLenum; pname: GLenum; params: PGLint );
	glGetMinmaxProcPtr = procedure( target: GLenum; reset: GLboolean; format: GLenum; typ: GLenum; values: univ Ptr );
	glGetMinmaxParameterfvProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLfloat );
	glGetMinmaxParameterivProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLint );
	glGetPixelMapfvProcPtr = procedure( map: GLenum; values: PGLfloat );
	glGetPixelMapuivProcPtr = procedure( map: GLenum; values: PGLuint );
	glGetPixelMapusvProcPtr = procedure( map: GLenum; values: PGLushort );
	glGetPointervProcPtr = procedure( pname: GLenum; params: univ Ptr );
	glGetPolygonStippleProcPtr = procedure( mask: PGLubyte );
	glGetSeparableFilterProcPtr = procedure( target: GLenum; format: GLenum; typ: GLenum; row: univ Ptr; column: univ Ptr; span: univ Ptr );
	glGetStringProcPtr = function( name: GLenum ): PChar;
	glGetTexEnvfvProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLfloat );
	glGetTexEnvivProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLint );
	glGetTexGendvProcPtr = procedure( coord: GLenum; pname: GLenum; params: PGLdouble );
	glGetTexGenfvProcPtr = procedure( coord: GLenum; pname: GLenum; params: PGLfloat );
	glGetTexGenivProcPtr = procedure( coord: GLenum; pname: GLenum; params: PGLint );
	glGetTexImageProcPtr = procedure( target: GLenum; level: GLint; format: GLenum; typ: GLenum; pixels: univ Ptr );
	glGetTexLevelParameterfvProcPtr = procedure( target: GLenum; level: GLint; pname: GLenum; params: PGLfloat );
	glGetTexLevelParameterivProcPtr = procedure( target: GLenum; level: GLint; pname: GLenum; params: PGLint );
	glGetTexParameterfvProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLfloat );
	glGetTexParameterivProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLint );
	glHintProcPtr = procedure( target: GLenum; mode: GLenum );
	glHistogramProcPtr = procedure( target: GLenum; width: GLsizei; internalformat: GLenum; sink: GLboolean );
	glIndexMaskProcPtr = procedure( mask: GLuint );
	glIndexPointerProcPtr = procedure( typ: GLenum; stride: GLsizei; const pointr: univ Ptr );
	glIndexdProcPtr = procedure( c: GLdouble );
	glIndexdvProcPtr = procedure( const c: PGLdouble );
	glIndexfProcPtr = procedure( c: GLfloat );
	glIndexfvProcPtr = procedure( const c: PGLfloat );
	glIndexiProcPtr = procedure( c: GLint );
	glIndexivProcPtr = procedure( const c: PGLint );
	glIndexsProcPtr = procedure( c: GLshort );
	glIndexsvProcPtr = procedure( const c: PGLshort );
	glIndexubProcPtr = procedure( c: GLubyte );
	glIndexubvProcPtr = procedure( const c: PGLubyte );
	glInitNamesProcPtr = procedure;
	glInterleavedArraysProcPtr = procedure( format: GLenum; stride: GLsizei; const pointr: univ Ptr );
	glIsEnabledProcPtr = function( cap: GLenum ): GLboolean;
	glIsListProcPtr = function( list: GLuint ): GLboolean;
	glIsTextureProcPtr = function( texture: GLuint ): GLboolean;
	glLightModelfProcPtr = procedure( pname: GLenum; param: GLfloat );
	glLightModelfvProcPtr = procedure( pname: GLenum; const params: PGLfloat );
	glLightModeliProcPtr = procedure( pname: GLenum; param: GLint );
	glLightModelivProcPtr = procedure( pname: GLenum; const params: PGLint );
	glLightfProcPtr = procedure( light: GLenum; pname: GLenum; param: GLfloat );
	glLightfvProcPtr = procedure( light: GLenum; pname: GLenum; const params: PGLfloat );
	glLightiProcPtr = procedure( light: GLenum; pname: GLenum; param: GLint );
	glLightivProcPtr = procedure( light: GLenum; pname: GLenum; const params: PGLint );
	glLineStippleProcPtr = procedure( factor: GLint; pattern: GLushort );
	glLineWidthProcPtr = procedure( width: GLfloat );
	glListBaseProcPtr = procedure( base: GLuint );
	glLoadIdentityProcPtr = procedure;
	glLoadMatrixdProcPtr = procedure( const m: PGLdouble );
	glLoadMatrixfProcPtr = procedure( const m: PGLfloat );
	glLoadNameProcPtr = procedure( name: GLuint );
	glLogicOpProcPtr = procedure( opcode: GLenum );
	glMap1dProcPtr = procedure( target: GLenum; u1: GLdouble; u2: GLdouble; stride: GLint; order: GLint; const points: PGLdouble );
	glMap1fProcPtr = procedure( target: GLenum; u1: GLfloat; u2: GLfloat; stride: GLint; order: GLint; const points: PGLfloat );
	glMap2dProcPtr = procedure( target: GLenum; u1: GLdouble; u2: GLdouble; ustride: GLint; uorder: GLint; v1: GLdouble; v2: GLdouble; vstride: GLint; vorder: GLint; const points: PGLdouble );
	glMap2fProcPtr = procedure( target: GLenum; u1: GLfloat; u2: GLfloat; ustride: GLint; uorder: GLint; v1: GLfloat; v2: GLfloat; vstride: GLint; vorder: GLint; const points: PGLfloat );
	glMapGrid1dProcPtr = procedure( un: GLint; u1: GLdouble; u2: GLdouble );
	glMapGrid1fProcPtr = procedure( un: GLint; u1: GLfloat; u2: GLfloat );
	glMapGrid2dProcPtr = procedure( un: GLint; u1: GLdouble; u2: GLdouble; vn: GLint; v1: GLdouble; v2: GLdouble );
	glMapGrid2fProcPtr = procedure( un: GLint; u1: GLfloat; u2: GLfloat; vn: GLint; v1: GLfloat; v2: GLfloat );
	glMaterialfProcPtr = procedure( face: GLenum; pname: GLenum; param: GLfloat );
	glMaterialfvProcPtr = procedure( face: GLenum; pname: GLenum; const params: PGLfloat );
	glMaterialiProcPtr = procedure( face: GLenum; pname: GLenum; param: GLint );
	glMaterialivProcPtr = procedure( face: GLenum; pname: GLenum; const params: PGLint );
	glMatrixModeProcPtr = procedure( mode: GLenum );
	glMinmaxProcPtr = procedure( target: GLenum; internalformat: GLenum; sink: GLboolean );
	glMultMatrixdProcPtr = procedure( const m: PGLdouble );
	glMultMatrixfProcPtr = procedure( const m: PGLfloat );
	glNewListProcPtr = procedure( list: GLuint; mode: GLenum );
	glNormal3bProcPtr = procedure( nx: GLbyte; ny: GLbyte; nz: GLbyte );
	glNormal3bvProcPtr = procedure( const v: PGLbyte );
	glNormal3dProcPtr = procedure( nx: GLdouble; ny: GLdouble; nz: GLdouble );
	glNormal3dvProcPtr = procedure( const v: PGLdouble );
	glNormal3fProcPtr = procedure( nx: GLfloat; ny: GLfloat; nz: GLfloat );
	glNormal3fvProcPtr = procedure( const v: PGLfloat );
	glNormal3iProcPtr = procedure( nx: GLint; ny: GLint; nz: GLint );
	glNormal3ivProcPtr = procedure( const v: PGLint );
	glNormal3sProcPtr = procedure( nx: GLshort; ny: GLshort; nz: GLshort );
	glNormal3svProcPtr = procedure( const v: PGLshort );
	glNormalPointerProcPtr = procedure( typ: GLenum; stride: GLsizei; const pointr: univ Ptr );
	glOrthoProcPtr = procedure( left: GLdouble; right: GLdouble; bottom: GLdouble; top: GLdouble; zNear: GLdouble; zFar: GLdouble );
	glPassThroughProcPtr = procedure( token: GLfloat );
	glPixelMapfvProcPtr = procedure( map: GLenum; mapsize: GLint; const values: PGLfloat );
	glPixelMapuivProcPtr = procedure( map: GLenum; mapsize: GLint; const values: PGLuint );
	glPixelMapusvProcPtr = procedure( map: GLenum; mapsize: GLint; const values: PGLushort );
	glPixelStorefProcPtr = procedure( pname: GLenum; param: GLfloat );
	glPixelStoreiProcPtr = procedure( pname: GLenum; param: GLint );
	glPixelTransferfProcPtr = procedure( pname: GLenum; param: GLfloat );
	glPixelTransferiProcPtr = procedure( pname: GLenum; param: GLint );
	glPixelZoomProcPtr = procedure( xfactor: GLfloat; yfactor: GLfloat );
	glPointSizeProcPtr = procedure( size: GLfloat );
	glPolygonModeProcPtr = procedure( face: GLenum; mode: GLenum );
	glPolygonOffsetProcPtr = procedure( factor: GLfloat; units: GLfloat );
	glPolygonStippleProcPtr = procedure( const mask: PGLubyte );
	glPopAttribProcPtr = procedure;
	glPopClientAttribProcPtr = procedure;
	glPopMatrixProcPtr = procedure;
	glPopNameProcPtr = procedure;
	glPrioritizeTexturesProcPtr = procedure( n: GLsizei; const textures: PGLuint; const priorities: PGLclampf );
	glPushAttribProcPtr = procedure( mask: GLbitfield );
	glPushClientAttribProcPtr = procedure( mask: GLbitfield );
	glPushMatrixProcPtr = procedure;
	glPushNameProcPtr = procedure( name: GLuint );
	glRasterPos2dProcPtr = procedure( x: GLdouble; y: GLdouble );
	glRasterPos2dvProcPtr = procedure( const v: PGLdouble );
	glRasterPos2fProcPtr = procedure( x: GLfloat; y: GLfloat );
	glRasterPos2fvProcPtr = procedure( const v: PGLfloat );
	glRasterPos2iProcPtr = procedure( x: GLint; y: GLint );
	glRasterPos2ivProcPtr = procedure( const v: PGLint );
	glRasterPos2sProcPtr = procedure( x: GLshort; y: GLshort );
	glRasterPos2svProcPtr = procedure( const v: PGLshort );
	glRasterPos3dProcPtr = procedure( x: GLdouble; y: GLdouble; z: GLdouble );
	glRasterPos3dvProcPtr = procedure( const v: PGLdouble );
	glRasterPos3fProcPtr = procedure( x: GLfloat; y: GLfloat; z: GLfloat );
	glRasterPos3fvProcPtr = procedure( const v: PGLfloat );
	glRasterPos3iProcPtr = procedure( x: GLint; y: GLint; z: GLint );
	glRasterPos3ivProcPtr = procedure( const v: PGLint );
	glRasterPos3sProcPtr = procedure( x: GLshort; y: GLshort; z: GLshort );
	glRasterPos3svProcPtr = procedure( const v: PGLshort );
	glRasterPos4dProcPtr = procedure( x: GLdouble; y: GLdouble; z: GLdouble; w: GLdouble );
	glRasterPos4dvProcPtr = procedure( const v: PGLdouble );
	glRasterPos4fProcPtr = procedure( x: GLfloat; y: GLfloat; z: GLfloat; w: GLfloat );
	glRasterPos4fvProcPtr = procedure( const v: PGLfloat );
	glRasterPos4iProcPtr = procedure( x: GLint; y: GLint; z: GLint; w: GLint );
	glRasterPos4ivProcPtr = procedure( const v: PGLint );
	glRasterPos4sProcPtr = procedure( x: GLshort; y: GLshort; z: GLshort; w: GLshort );
	glRasterPos4svProcPtr = procedure( const v: PGLshort );
	glReadBufferProcPtr = procedure( mode: GLenum );
	glReadPixelsProcPtr = procedure( x: GLint; y: GLint; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; pixels: univ Ptr );
	glRectdProcPtr = procedure( x1: GLdouble; y1: GLdouble; x2: GLdouble; y2: GLdouble );
	glRectdvProcPtr = procedure( const v1: PGLdouble; const v2: PGLdouble );
	glRectfProcPtr = procedure( x1: GLfloat; y1: GLfloat; x2: GLfloat; y2: GLfloat );
	glRectfvProcPtr = procedure( const v1: PGLfloat; const v2: PGLfloat );
	glRectiProcPtr = procedure( x1: GLint; y1: GLint; x2: GLint; y2: GLint );
	glRectivProcPtr = procedure( const v1: PGLint; const v2: PGLint );
	glRectsProcPtr = procedure( x1: GLshort; y1: GLshort; x2: GLshort; y2: GLshort );
	glRectsvProcPtr = procedure( const v1: PGLshort; const v2: PGLshort );
	glRenderModeProcPtr = function( mode: GLenum ): GLint;
	glResetHistogramProcPtr = procedure( target: GLenum );
	glResetMinmaxProcPtr = procedure( target: GLenum );
	glRotatedProcPtr = procedure( angle: GLdouble; x: GLdouble; y: GLdouble; z: GLdouble );
	glRotatefProcPtr = procedure( angle: GLfloat; x: GLfloat; y: GLfloat; z: GLfloat );
	glScaledProcPtr = procedure( x: GLdouble; y: GLdouble; z: GLdouble );
	glScalefProcPtr = procedure( x: GLfloat; y: GLfloat; z: GLfloat );
	glScissorProcPtr = procedure( x: GLint; y: GLint; width: GLsizei; height: GLsizei );
	glSelectBufferProcPtr = procedure( size: GLsizei; buffer: PGLuint );
	glSeparableFilter2DProcPtr = procedure( target: GLenum; internalformat: GLenum; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; const row: univ Ptr; const column: univ Ptr );
	glShadeModelProcPtr = procedure( mode: GLenum );
	glStencilFuncProcPtr = procedure( func: GLenum; ref: GLint; mask: GLuint );
	glStencilMaskProcPtr = procedure( mask: GLuint );
	glStencilOpProcPtr = procedure( fail: GLenum; zfail: GLenum; zpass: GLenum );
	glTexCoord1dProcPtr = procedure( s: GLdouble );
	glTexCoord1dvProcPtr = procedure( const v: PGLdouble );
	glTexCoord1fProcPtr = procedure( s: GLfloat );
	glTexCoord1fvProcPtr = procedure( const v: PGLfloat );
	glTexCoord1iProcPtr = procedure( s: GLint );
	glTexCoord1ivProcPtr = procedure( const v: PGLint );
	glTexCoord1sProcPtr = procedure( s: GLshort );
	glTexCoord1svProcPtr = procedure( const v: PGLshort );
	glTexCoord2dProcPtr = procedure( s: GLdouble; t: GLdouble );
	glTexCoord2dvProcPtr = procedure( const v: PGLdouble );
	glTexCoord2fProcPtr = procedure( s: GLfloat; t: GLfloat );
	glTexCoord2fvProcPtr = procedure( const v: PGLfloat );
	glTexCoord2iProcPtr = procedure( s: GLint; t: GLint );
	glTexCoord2ivProcPtr = procedure( const v: PGLint );
	glTexCoord2sProcPtr = procedure( s: GLshort; t: GLshort );
	glTexCoord2svProcPtr = procedure( const v: PGLshort );
	glTexCoord3dProcPtr = procedure( s: GLdouble; t: GLdouble; r: GLdouble );
	glTexCoord3dvProcPtr = procedure( const v: PGLdouble );
	glTexCoord3fProcPtr = procedure( s: GLfloat; t: GLfloat; r: GLfloat );
	glTexCoord3fvProcPtr = procedure( const v: PGLfloat );
	glTexCoord3iProcPtr = procedure( s: GLint; t: GLint; r: GLint );
	glTexCoord3ivProcPtr = procedure( const v: PGLint );
	glTexCoord3sProcPtr = procedure( s: GLshort; t: GLshort; r: GLshort );
	glTexCoord3svProcPtr = procedure( const v: PGLshort );
	glTexCoord4dProcPtr = procedure( s: GLdouble; t: GLdouble; r: GLdouble; q: GLdouble );
	glTexCoord4dvProcPtr = procedure( const v: PGLdouble );
	glTexCoord4fProcPtr = procedure( s: GLfloat; t: GLfloat; r: GLfloat; q: GLfloat );
	glTexCoord4fvProcPtr = procedure( const v: PGLfloat );
	glTexCoord4iProcPtr = procedure( s: GLint; t: GLint; r: GLint; q: GLint );
	glTexCoord4ivProcPtr = procedure( const v: PGLint );
	glTexCoord4sProcPtr = procedure( s: GLshort; t: GLshort; r: GLshort; q: GLshort );
	glTexCoord4svProcPtr = procedure( const v: PGLshort );
	glTexCoordPointerProcPtr = procedure( size: GLint; typ: GLenum; stride: GLsizei; const pointr: univ Ptr );
	glTexEnvfProcPtr = procedure( target: GLenum; pname: GLenum; param: GLfloat );
	glTexEnvfvProcPtr = procedure( target: GLenum; pname: GLenum; const params: PGLfloat );
	glTexEnviProcPtr = procedure( target: GLenum; pname: GLenum; param: GLint );
	glTexEnvivProcPtr = procedure( target: GLenum; pname: GLenum; const params: PGLint );
	glTexGendProcPtr = procedure( coord: GLenum; pname: GLenum; param: GLdouble );
	glTexGendvProcPtr = procedure( coord: GLenum; pname: GLenum; const params: PGLdouble );
	glTexGenfProcPtr = procedure( coord: GLenum; pname: GLenum; param: GLfloat );
	glTexGenfvProcPtr = procedure( coord: GLenum; pname: GLenum; const params: PGLfloat );
	glTexGeniProcPtr = procedure( coord: GLenum; pname: GLenum; param: GLint );
	glTexGenivProcPtr = procedure( coord: GLenum; pname: GLenum; const params: PGLint );
	glTexImage1DProcPtr = procedure( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; border: GLint; format: GLenum; typ: GLenum; const pixels: univ Ptr );
	glTexImage2DProcPtr = procedure( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; border: GLint; format: GLenum; typ: GLenum; const pixels: univ Ptr );
	glTexImage3DProcPtr = procedure( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; border: GLint; format: GLenum; typ: GLenum; const pixels: univ Ptr );
	glTexParameterfProcPtr = procedure( target: GLenum; pname: GLenum; param: GLfloat );
	glTexParameterfvProcPtr = procedure( target: GLenum; pname: GLenum; const params: PGLfloat );
	glTexParameteriProcPtr = procedure( target: GLenum; pname: GLenum; param: GLint );
	glTexParameterivProcPtr = procedure( target: GLenum; pname: GLenum; const params: PGLint );
	glTexSubImage1DProcPtr = procedure( target: GLenum; level: GLint; xoffset: GLint; width: GLsizei; format: GLenum; typ: GLenum; const pixels: univ Ptr );
	glTexSubImage2DProcPtr = procedure( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; const pixels: univ Ptr );
	glTexSubImage3DProcPtr = procedure( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; typ: GLenum; const pixels: univ Ptr );
	glTranslatedProcPtr = procedure( x: GLdouble; y: GLdouble; z: GLdouble );
	glTranslatefProcPtr = procedure( x: GLfloat; y: GLfloat; z: GLfloat );
	glVertex2dProcPtr = procedure( x: GLdouble; y: GLdouble );
	glVertex2dvProcPtr = procedure( const v: PGLdouble );
	glVertex2fProcPtr = procedure( x: GLfloat; y: GLfloat );
	glVertex2fvProcPtr = procedure( const v: PGLfloat );
	glVertex2iProcPtr = procedure( x: GLint; y: GLint );
	glVertex2ivProcPtr = procedure( const v: PGLint );
	glVertex2sProcPtr = procedure( x: GLshort; y: GLshort );
	glVertex2svProcPtr = procedure( const v: PGLshort );
	glVertex3dProcPtr = procedure( x: GLdouble; y: GLdouble; z: GLdouble );
	glVertex3dvProcPtr = procedure( const v: PGLdouble );
	glVertex3fProcPtr = procedure( x: GLfloat; y: GLfloat; z: GLfloat );
	glVertex3fvProcPtr = procedure( const v: PGLfloat );
	glVertex3iProcPtr = procedure( x: GLint; y: GLint; z: GLint );
	glVertex3ivProcPtr = procedure( const v: PGLint );
	glVertex3sProcPtr = procedure( x: GLshort; y: GLshort; z: GLshort );
	glVertex3svProcPtr = procedure( const v: PGLshort );
	glVertex4dProcPtr = procedure( x: GLdouble; y: GLdouble; z: GLdouble; w: GLdouble );
	glVertex4dvProcPtr = procedure( const v: PGLdouble );
	glVertex4fProcPtr = procedure( x: GLfloat; y: GLfloat; z: GLfloat; w: GLfloat );
	glVertex4fvProcPtr = procedure( const v: PGLfloat );
	glVertex4iProcPtr = procedure( x: GLint; y: GLint; z: GLint; w: GLint );
	glVertex4ivProcPtr = procedure( const v: PGLint );
	glVertex4sProcPtr = procedure( x: GLshort; y: GLshort; z: GLshort; w: GLshort );
	glVertex4svProcPtr = procedure( const v: PGLshort );
	glVertexPointerProcPtr = procedure( size: GLint; typ: GLenum; stride: GLsizei; const pointr: univ Ptr );
	glViewportProcPtr = procedure( x: GLint; y: GLint; width: GLsizei; height: GLsizei );

type
	glSampleCoverageProcPtr = procedure( value: GLclampf; invert: GLboolean );
	glSamplePassProcPtr = procedure( pass: GLenum );

type
	glLoadTransposeMatrixfProcPtr = procedure( const m: PGLfloat );
	glLoadTransposeMatrixdProcPtr = procedure( const m: PGLdouble );
	glMultTransposeMatrixfProcPtr = procedure( const m: PGLfloat );
	glMultTransposeMatrixdProcPtr = procedure( const m: PGLdouble );

type
	glCompressedTexImage3DProcPtr = procedure( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; border: GLint; imageSize: GLsizei; const data: univ Ptr );
	glCompressedTexImage2DProcPtr = procedure( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; border: GLint; imageSize: GLsizei; const data: univ Ptr );
	glCompressedTexImage1DProcPtr = procedure( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; border: GLint; imageSize: GLsizei; const data: univ Ptr );
	glCompressedTexSubImage3DProcPtr = procedure( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; imageSize: GLsizei; const data: univ Ptr );
	glCompressedTexSubImage2DProcPtr = procedure( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; width: GLsizei; height: GLsizei; format: GLenum; imageSize: GLsizei; const data: univ Ptr );
	glCompressedTexSubImage1DProcPtr = procedure( target: GLenum; level: GLint; xoffset: GLint; width: GLsizei; format: GLenum; imageSize: GLsizei; const data: univ Ptr );
	glGetCompressedTexImageProcPtr = procedure( target: GLenum; lod: GLint; img: univ Ptr );

type
	glActiveTextureProcPtr = procedure( texture: GLenum );
	glClientActiveTextureProcPtr = procedure( texture: GLenum );
	glMultiTexCoord1dProcPtr = procedure( target: GLenum; s: GLdouble );
	glMultiTexCoord1dvProcPtr = procedure( target: GLenum; const v: PGLdouble );
	glMultiTexCoord1fProcPtr = procedure( target: GLenum; s: GLfloat );
	glMultiTexCoord1fvProcPtr = procedure( target: GLenum; const v: PGLfloat );
	glMultiTexCoord1iProcPtr = procedure( target: GLenum; s: GLint );
	glMultiTexCoord1ivProcPtr = procedure( target: GLenum; const v: PGLint );
	glMultiTexCoord1sProcPtr = procedure( target: GLenum; s: GLshort );
	glMultiTexCoord1svProcPtr = procedure( target: GLenum; const v: PGLshort );
	glMultiTexCoord2dProcPtr = procedure( target: GLenum; s: GLdouble; t: GLdouble );
	glMultiTexCoord2dvProcPtr = procedure( target: GLenum; const v: PGLdouble );
	glMultiTexCoord2fProcPtr = procedure( target: GLenum; s: GLfloat; t: GLfloat );
	glMultiTexCoord2fvProcPtr = procedure( target: GLenum; const v: PGLfloat );
	glMultiTexCoord2iProcPtr = procedure( target: GLenum; s: GLint; t: GLint );
	glMultiTexCoord2ivProcPtr = procedure( target: GLenum; const v: PGLint );
	glMultiTexCoord2sProcPtr = procedure( target: GLenum; s: GLshort; t: GLshort );
	glMultiTexCoord2svProcPtr = procedure( target: GLenum; const v: PGLshort );
	glMultiTexCoord3dProcPtr = procedure( target: GLenum; s: GLdouble; t: GLdouble; r: GLdouble );
	glMultiTexCoord3dvProcPtr = procedure( target: GLenum; const v: PGLdouble );
	glMultiTexCoord3fProcPtr = procedure( target: GLenum; s: GLfloat; t: GLfloat; r: GLfloat );
	glMultiTexCoord3fvProcPtr = procedure( target: GLenum; const v: PGLfloat );
	glMultiTexCoord3iProcPtr = procedure( target: GLenum; s: GLint; t: GLint; r: GLint );
	glMultiTexCoord3ivProcPtr = procedure( target: GLenum; const v: PGLint );
	glMultiTexCoord3sProcPtr = procedure( target: GLenum; s: GLshort; t: GLshort; r: GLshort );
	glMultiTexCoord3svProcPtr = procedure( target: GLenum; const v: PGLshort );
	glMultiTexCoord4dProcPtr = procedure( target: GLenum; s: GLdouble; t: GLdouble; r: GLdouble; q: GLdouble );
	glMultiTexCoord4dvProcPtr = procedure( target: GLenum; const v: PGLdouble );
	glMultiTexCoord4fProcPtr = procedure( target: GLenum; s: GLfloat; t: GLfloat; r: GLfloat; q: GLfloat );
	glMultiTexCoord4fvProcPtr = procedure( target: GLenum; const v: PGLfloat );
	glMultiTexCoord4iProcPtr = procedure( target: GLenum; GLint; s: GLint; t: GLint; r: GLint );
	glMultiTexCoord4ivProcPtr = procedure( target: GLenum; const v: PGLint );
	glMultiTexCoord4sProcPtr = procedure( target: GLenum; s: GLshort; t: GLshort; r: GLshort; q: GLshort );
	glMultiTexCoord4svProcPtr = procedure( target: GLenum; const v: PGLshort );

type
	glFogCoordfProcPtr = procedure( coord: GLfloat );
	glFogCoordfvProcPtr = procedure( const coord: PGLfloat );  
	glFogCoorddProcPtr = procedure( coord: GLdouble );
	glFogCoorddvProcPtr = procedure( const coord: PGLdouble );   
	glFogCoordPointerProcPtr = procedure( typ: GLenum; stride: GLsizei; const pointr: univ Ptr );

type
	glSecondaryColor3bProcPtr = procedure( red: GLbyte; green: GLbyte; blue: GLbyte );
	glSecondaryColor3bvProcPtr = procedure( const v: PGLbyte );
	glSecondaryColor3dProcPtr = procedure( red: GLdouble; green: GLdouble; blue: GLdouble );
	glSecondaryColor3dvProcPtr = procedure( const v: PGLdouble );
	glSecondaryColor3fProcPtr = procedure( red: GLfloat; green: GLfloat; blue: GLfloat );
	glSecondaryColor3fvProcPtr = procedure( const v: PGLfloat );
	glSecondaryColor3iProcPtr = procedure( red: GLint; green: GLint; blue: GLint );
	glSecondaryColor3ivProcPtr = procedure( const v: PGLint );
	glSecondaryColor3sProcPtr = procedure( red: GLshort; green: GLshort; blue: GLshort );
	glSecondaryColor3svProcPtr = procedure( const v: PGLshort );
	glSecondaryColor3ubProcPtr = procedure( red: GLubyte; green: GLubyte; blue: GLubyte );
	glSecondaryColor3ubvProcPtr = procedure( const v: PGLubyte );
	glSecondaryColor3uiProcPtr = procedure( red: GLuint; green: GLuint; blue: GLuint );
	glSecondaryColor3uivProcPtr = procedure( const v: PGLuint );
	glSecondaryColor3usProcPtr = procedure( red: GLushort; green: GLushort; blue: GLushort );
	glSecondaryColor3usvProcPtr = procedure( const v: PGLushort );
	glSecondaryColorPointerProcPtr = procedure( size: GLint; typ: GLenum; stride: GLsizei; const pointr: univ Ptr );

type
	glPointParameterfProcPtr = procedure( pname: GLenum; param: GLfloat ); 
	glPointParameterfvProcPtr = procedure( pname: GLenum; const params: PGLfloat );
	glPointParameteriProcPtr = procedure( pname: GLenum; param: GLint ); 
	glPointParameterivProcPtr = procedure( pname: GLenum; const params: PGLint );

type
	glBlendFuncSeparateProcPtr = procedure( srcRGB: GLenum; dstRGB: GLenum; srcAlpha: GLenum; dstAlpha: GLenum );

type
	glMultiDrawArraysProcPtr = procedure( mode: GLenum; const first: PGLint; const count: PGLsizei; primcount: GLsizei );
	glMultiDrawElementsProcPtr = procedure( mode: GLenum; const count: PGLsizei; typ: GLenum; {const} indices: UnivPtrPtr; primcount: GLsizei );

type
	glWindowPos2dProcPtr = procedure( x: GLdouble; y: GLdouble );
	glWindowPos2dvProcPtr = procedure( const v: PGLdouble );
	glWindowPos2fProcPtr = procedure( x: GLfloat; y: GLfloat );
	glWindowPos2fvProcPtr = procedure( const v: PGLfloat );
	glWindowPos2iProcPtr = procedure( x: GLint; y: GLint ); 
	glWindowPos2ivProcPtr = procedure( const v: PGLint );
	glWindowPos2sProcPtr = procedure( x: GLshort; y: GLshort );
	glWindowPos2svProcPtr = procedure( const v: PGLshort );
	glWindowPos3dProcPtr = procedure( x: GLdouble; y: GLdouble; z: GLdouble );
	glWindowPos3dvProcPtr = procedure( const v: PGLdouble );
	glWindowPos3fProcPtr = procedure( x: GLfloat; y: GLfloat; z: GLfloat );
	glWindowPos3fvProcPtr = procedure( const v: PGLfloat );
	glWindowPos3iProcPtr = procedure( x: GLint; y: GLint; z: GLint );
	glWindowPos3ivProcPtr = procedure( const v: PGLint );
	glWindowPos3sProcPtr = procedure( x: GLshort; y: GLshort; z: GLshort );
	glWindowPos3svProcPtr = procedure( const v: PGLshort );

type
	glGenQueriesProcPtr = procedure( n: GLsizei; ids: PGLuint );
	glDeleteQueriesProcPtr = procedure( n: GLsizei; const ids: PGLuint );
	glIsQueryProcPtr = function( id: GLuint ): GLboolean;
	glBeginQueryProcPtr = procedure( target: GLenum; id: GLuint );
	glEndQueryProcPtr = procedure( target: GLenum );
	glGetQueryivProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLint );
	glGetQueryObjectivProcPtr = procedure( id: GLuint; pname: GLenum; params: PGLint );
	glGetQueryObjectuivProcPtr = procedure( id: GLuint; pname: GLenum; params: PGLuint );

type
	glBindBufferProcPtr = procedure( target: GLenum; buffer: GLuint );
	glDeleteBuffersProcPtr = procedure( n: GLsizei; const buffers: PGLuint );
	glGenBuffersProcPtr = procedure( n: GLsizei; buffers: PGLuint );
	glIsBufferProcPtr = function( buffer: GLuint ): GLboolean;
	glBufferDataProcPtr = procedure( target: GLenum; size: GLsizeiptr; const data: univ Ptr; usage: GLenum );
	glBufferSubDataProcPtr = procedure( target: GLenum; offset: GLintptr; size: GLsizeiptr; const data: univ Ptr );
	glGetBufferSubDataProcPtr = procedure( target: GLenum; offset: GLintptr; size: GLsizeiptr; data: univ Ptr );
	glMapBufferProcPtr = function( target: GLenum; access: GLenum ): univ Ptr;
	glUnmapBufferProcPtr = function( target: GLenum ): GLboolean;
	glGetBufferParameterivProcPtr = procedure( target: GLenum; pname: GLenum; params: PGLint );
	glGetBufferPointervProcPtr = procedure( target: GLenum; pname: GLenum; params: UnivPtrPtr );

type
	glDrawBuffersProcPtr = procedure( n: GLsizei; const bufs: PGLenum );
	glVertexAttrib1dProcPtr = procedure( index: GLuint; x: GLdouble );
	glVertexAttrib1dvProcPtr = procedure( index: GLuint; const v: PGLdouble );
	glVertexAttrib1fProcPtr = procedure( index: GLuint; x: GLfloat );
	glVertexAttrib1fvProcPtr = procedure( index: GLuint; const v: PGLfloat );
	glVertexAttrib1sProcPtr = procedure( index: GLuint; x: GLshort );
	glVertexAttrib1svProcPtr = procedure( index: GLuint; const v: PGLshort );
	glVertexAttrib2dProcPtr = procedure( index: GLuint; x: GLdouble; y: GLdouble );
	glVertexAttrib2dvProcPtr = procedure( index: GLuint; const v: PGLdouble );
	glVertexAttrib2fProcPtr = procedure( index: GLuint; x: GLfloat; y: GLfloat );
	glVertexAttrib2fvProcPtr = procedure( index: GLuint; const v: PGLfloat );
	glVertexAttrib2sProcPtr = procedure( index: GLuint; x: GLshort; y: GLshort );
	glVertexAttrib2svProcPtr = procedure( index: GLuint; const v: PGLshort );
	glVertexAttrib3dProcPtr = procedure( index: GLuint; x: GLdouble; y: GLdouble; z: GLdouble );
	glVertexAttrib3dvProcPtr = procedure( index: GLuint; const v: PGLdouble );
	glVertexAttrib3fProcPtr = procedure( index: GLuint; x: GLfloat; y: GLfloat; z: GLfloat );
	glVertexAttrib3fvProcPtr = procedure( index: GLuint; const v: PGLfloat );
	glVertexAttrib3sProcPtr = procedure( index: GLuint; x: GLshort; y: GLshort; z: GLshort );
	glVertexAttrib3svProcPtr = procedure( index: GLuint; const v: PGLshort );
	glVertexAttrib4NbvProcPtr = procedure( index: GLuint; const v: PGLbyte );
	glVertexAttrib4NivProcPtr = procedure( index: GLuint; const v: PGLint );
	glVertexAttrib4NsvProcPtr = procedure( index: GLuint; const v: PGLshort );
	glVertexAttrib4NubProcPtr = procedure( index: GLuint; x: GLubyte; y: GLubyte; z: GLubyte; w: GLubyte );
	glVertexAttrib4NubvProcPtr = procedure( index: GLuint; const v: PGLubyte );
	glVertexAttrib4NuivProcPtr = procedure( index: GLuint; const v: PGLuint );
	glVertexAttrib4NusvProcPtr = procedure( index: GLuint; const v: PGLushort );
	glVertexAttrib4bvProcPtr = procedure( index: GLuint; const v: PGLbyte );
	glVertexAttrib4dProcPtr = procedure( index: GLuint; x: GLdouble; y: GLdouble; z: GLdouble; w: GLdouble );
	glVertexAttrib4dvProcPtr = procedure( index: GLuint; const v: PGLdouble );
	glVertexAttrib4fProcPtr = procedure( index: GLuint; x: GLfloat; y: GLfloat; z: GLfloat; w: GLfloat );
	glVertexAttrib4fvProcPtr = procedure( index: GLuint; const v: PGLfloat );
	glVertexAttrib4ivProcPtr = procedure( index: GLuint; const v: PGLint );
	glVertexAttrib4sProcPtr = procedure( index: GLuint; x: GLshort; y: GLshort; z: GLshort; w: GLshort );
	glVertexAttrib4svProcPtr = procedure( index: GLuint; const v: PGLshort );
	glVertexAttrib4ubvProcPtr = procedure( index: GLuint; const v: PGLubyte );
	glVertexAttrib4uivProcPtr = procedure( index: GLuint; const v: PGLuint );
	glVertexAttrib4usvProcPtr = procedure( index: GLuint; const v: PGLushort );
	glVertexAttribPointerProcPtr = procedure( index: GLuint; size: GLint; typ: GLenum; normalized: GLboolean; stride: GLsizei; const pointr: univ Ptr );
	glEnableVertexAttribArrayProcPtr = procedure( index: GLuint );
	glDisableVertexAttribArrayProcPtr = procedure( index: GLuint );
	glGetVertexAttribdvProcPtr = procedure( index: GLuint; pname: GLenum; params: PGLdouble );
	glGetVertexAttribfvProcPtr = procedure( index: GLuint; pname: GLenum; params: PGLfloat );
	glGetVertexAttribivProcPtr = procedure( index: GLuint; pname: GLenum; params: PGLint );
	glGetVertexAttribPointervProcPtr = procedure( index: GLuint; pname: GLenum; pointr: UnivPtrPtr );
	glDeleteShaderProcPtr = procedure( shader: GLuint );
	glDetachShaderProcPtr = procedure( program_: GLuint; shader: GLuint );
	glCreateShaderProcPtr = function( typ: GLenum ): GLuint;
{GPC-ONLY-START}
	glShaderSourceProcPtr = procedure( shader: GLuint; count: GLsizei; {const} strng: CStringPtrPtr; const length: PGLint );
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	glShaderSourceProcPtr = procedure( shader: GLuint; count: GLsizei; {const} strng: PPChar; const length: PGLint );
{FPC-ONLY-FINISH}
	glCompileShaderProcPtr = procedure( shader: GLuint );
	glCreateProgramProcPtr = function: GLuint;
	glAttachShaderProcPtr = procedure( program_: GLuint; shader: GLuint );
	glLinkProgramProcPtr = procedure( program_: GLuint );
	glUseProgramProcPtr = procedure( program_: GLuint );
	glDeleteProgramProcPtr = procedure( program_: GLuint );
	glValidateProgramProcPtr = procedure( program_: GLuint );
	glUniform1fProcPtr = procedure( location: GLint; v0: GLfloat );
	glUniform2fProcPtr = procedure( location: GLint; v0: GLfloat; v1: GLfloat );
	glUniform3fProcPtr = procedure( location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat );
	glUniform4fProcPtr = procedure( location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat; v3: GLfloat );
	glUniform1iProcPtr = procedure( location: GLint; v0: GLint );
	glUniform2iProcPtr = procedure( location: GLint; v0: GLint; v1: GLint );
	glUniform3iProcPtr = procedure( location: GLint; v0: GLint; v1: GLint; v2: GLint );
	glUniform4iProcPtr = procedure( location: GLint; v0: GLint; v1: GLint; v2: GLint; v3: GLint );
	glUniform1fvProcPtr = procedure( location: GLint; count: GLsizei; const value: PGLfloat );
	glUniform2fvProcPtr = procedure( location: GLint; count: GLsizei; const value: PGLfloat );
	glUniform3fvProcPtr = procedure( location: GLint; count: GLsizei; const value: PGLfloat );
	glUniform4fvProcPtr = procedure( location: GLint; count: GLsizei; const value: PGLfloat );
	glUniform1ivProcPtr = procedure( location: GLint; count: GLsizei; const value: PGLint );
	glUniform2ivProcPtr = procedure( location: GLint; count: GLsizei; const value: PGLint );
	glUniform3ivProcPtr = procedure( location: GLint; count: GLsizei; const value: PGLint );
	glUniform4ivProcPtr = procedure( location: GLint; count: GLsizei; const value: PGLint );
	glUniformMatrix2fvProcPtr = procedure( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
	glUniformMatrix3fvProcPtr = procedure( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
	glUniformMatrix4fvProcPtr = procedure( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
	glIsShaderProcPtr = function( shader: GLuint ): GLboolean;
	glIsProgramProcPtr = function( program_: GLuint ): GLboolean;
	glGetShaderivProcPtr = procedure( shader: GLuint; pname: GLenum; params: PGLint );
	glGetProgramivProcPtr = procedure( program_: GLuint; pname: GLenum; params: PGLint );
	glGetAttachedShadersProcPtr = procedure( program_: GLuint; maxCount: GLsizei; count: PGLsizei; shaders: PGLuint );
	glGetShaderInfoLogProcPtr = procedure( shader: GLuint; bufSize: GLsizei; length: PGLsizei; infoLog: PChar );
	glGetProgramInfoLogProcPtr = procedure( program_: GLuint; bufSize: GLsizei; length: PGLsizei; infoLog: PChar );
	glGetUniformLocationProcPtr = function( program_: GLuint; const name: PChar ): GLint;
	glGetActiveUniformProcPtr = procedure( program_: GLuint; index: GLuint; bufSize: GLsizei; length: PGLsizei; size: PGLint; typ: PGLenum; name: PChar );
	glGetUniformfvProcPtr = procedure( program_: GLuint; location: GLint; params: PGLfloat );
	glGetUniformivProcPtr = procedure( program_: GLuint; location: GLint; params: PGLint );
	glGetShaderSourceProcPtr = procedure( shader: GLuint; bufSize: GLsizei; length: PGLsizei; source: PChar );
	glBindAttribLocationProcPtr = procedure( program_: GLuint; index: GLuint; const name: PChar );
	glGetActiveAttribProcPtr = procedure( program_: GLuint; index: GLuint; bufSize: GLsizei; length: PGLsizei; size: PGLint; typ: PGLenum; name: PChar );
	glGetAttribLocationProcPtr = function( program_: GLuint; const name: PChar ): GLint;
	glStencilFuncSeparateProcPtr = procedure( face: GLenum; func: GLenum; ref: GLint; mask: GLuint );
	glStencilOpSeparateProcPtr = procedure( face: GLenum; fail: GLenum; zfail: GLenum; zpass: GLenum );
	glStencilMaskSeparateProcPtr = procedure( face: GLenum; mask: GLuint );

type
	glUniformMatrix2x3fvProcPtr = procedure( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
	glUniformMatrix3x2fvProcPtr = procedure( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
	glUniformMatrix2x4fvProcPtr = procedure( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
	glUniformMatrix4x2fvProcPtr = procedure( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
	glUniformMatrix3x4fvProcPtr = procedure( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
	glUniformMatrix4x3fvProcPtr = procedure( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );

{$elsec} { GL_GLEXT_FUNCTION_POINTERS }

procedure glAccum( op: GLenum; value: GLfloat );
procedure glAlphaFunc( func: GLenum; ref: GLclampf );
function glAreTexturesResident( n: GLsizei; const textures: PGLuint; residences: PGLboolean ): GLboolean;
procedure glArrayElement( i: GLint );
procedure glBegin( mode: GLenum );
procedure glBindTexture( target: GLenum; texture: GLuint );
procedure glBitmap( width: GLsizei; height: GLsizei; xorig: GLfloat; yorig: GLfloat; xmove: GLfloat; ymove: GLfloat; const bitmap: PGLubyte );
procedure glBlendColor( red: GLclampf; green: GLclampf; blue: GLclampf; alpha: GLclampf );
procedure glBlendEquation( mode: GLenum );
procedure glBlendEquationSeparate( modeRGB: GLenum; modeAlpha: GLenum );
procedure glBlendFunc( sfactor: GLenum; dfactor: GLenum );
procedure glCallList( list: GLuint );
procedure glCallLists( n: GLsizei; typ: GLenum; const lists: univ Ptr );
procedure glClear( mask: GLbitfield );
procedure glClearAccum( red: GLfloat; green: GLfloat; blue: GLfloat; alpha: GLfloat );
procedure glClearColor( red: GLclampf; green: GLclampf; blue: GLclampf; alpha: GLclampf );
procedure glClearDepth( depth: GLclampd );
procedure glClearIndex( c: GLfloat );
procedure glClearStencil( s: GLint );
procedure glClipPlane( plane: GLenum; const equation: PGLdouble );
procedure glColor3b( red: GLbyte; green: GLbyte; blue: GLbyte );
procedure glColor3bv( const v: PGLbyte );
procedure glColor3d( red: GLdouble; green: GLdouble; blue: GLdouble );
procedure glColor3dv( const v: PGLdouble );
procedure glColor3f( red: GLfloat; green: GLfloat; blue: GLfloat );
procedure glColor3fv( const v: PGLfloat );
procedure glColor3i( red: GLint; green: GLint; blue: GLint );
procedure glColor3iv( const v: PGLint );
procedure glColor3s( red: GLshort; green: GLshort; blue: GLshort );
procedure glColor3sv( const v: PGLshort );
procedure glColor3ub( red: GLubyte; green: GLubyte; blue: GLubyte );
procedure glColor3ubv( const v: PGLubyte );
procedure glColor3ui( red: GLuint; green: GLuint; blue: GLuint );
procedure glColor3uiv( const v: PGLuint );
procedure glColor3us( red: GLushort; green: GLushort; blue: GLushort );
procedure glColor3usv( const v: PGLushort );
procedure glColor4b( red: GLbyte; green: GLbyte; blue: GLbyte; alpha: GLbyte );
procedure glColor4bv( const v: PGLbyte );
procedure glColor4d( red: GLdouble; green: GLdouble; blue: GLdouble; alpha: GLdouble );
procedure glColor4dv( const v: PGLdouble );
procedure glColor4f( red: GLfloat; green: GLfloat; blue: GLfloat; alpha: GLfloat );
procedure glColor4fv( const v: PGLfloat );
procedure glColor4i( red: GLint; green: GLint; blue: GLint; alpha: GLint );
procedure glColor4iv( const v: PGLint );
procedure glColor4s( red: GLshort; green: GLshort; blue: GLshort; alpha: GLshort );
procedure glColor4sv( const v: PGLshort );
procedure glColor4ub( red: GLubyte; green: GLubyte; blue: GLubyte; alpha: GLubyte );
procedure glColor4ubv( const v: PGLubyte );
procedure glColor4ui( red: GLuint; green: GLuint; blue: GLuint; alpha: GLuint );
procedure glColor4uiv( const v: PGLuint );
procedure glColor4us( red: GLushort; green: GLushort; blue: GLushort; alpha: GLushort );
procedure glColor4usv( const v: PGLushort );
procedure glColorMask( red: GLboolean; green: GLboolean; blue: GLboolean; alpha: GLboolean );
procedure glColorMaterial( face: GLenum; mode: GLenum );
procedure glColorPointer( size: GLint; typ: GLenum; stride: GLsizei; const pointr: univ Ptr );
procedure glColorSubTable( target: GLenum; start: GLsizei; count: GLsizei; format: GLenum; typ: GLenum; const data: univ Ptr );
procedure glColorTable( target: GLenum; internalformat: GLenum; width: GLsizei; format: GLenum; typ: GLenum; const table: univ Ptr );
procedure glColorTableParameterfv( target: GLenum; pname: GLenum; const params: PGLfloat );
procedure glColorTableParameteriv( target: GLenum; pname: GLenum; const params: PGLint );
procedure glConvolutionFilter1D( target: GLenum; internalformat: GLenum; width: GLsizei; format: GLenum; typ: GLenum; const image: univ Ptr );
procedure glConvolutionFilter2D( target: GLenum; internalformat: GLenum; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; const image: univ Ptr );
procedure glConvolutionParameterf( target: GLenum; pname: GLenum; params: GLfloat );
procedure glConvolutionParameterfv( target: GLenum; pname: GLenum; const params: PGLfloat );
procedure glConvolutionParameteri( target: GLenum; pname: GLenum; params: GLint );
procedure glConvolutionParameteriv( target: GLenum; pname: GLenum; const params: PGLint );
procedure glCopyColorSubTable( target: GLenum; start: GLsizei; x: GLint; y: GLint; width: GLsizei );
procedure glCopyColorTable( target: GLenum; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei );
procedure glCopyConvolutionFilter1D( target: GLenum; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei );
procedure glCopyConvolutionFilter2D( target: GLenum; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei; height: GLsizei );
procedure glCopyPixels( x: GLint; y: GLint; width: GLsizei; height: GLsizei; typ: GLenum );
procedure glCopyTexImage1D( target: GLenum; level: GLint; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei; border: GLint );
procedure glCopyTexImage2D( target: GLenum; level: GLint; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei; height: GLsizei; border: GLint );
procedure glCopyTexSubImage1D( target: GLenum; level: GLint; xoffset: GLint; x: GLint; y: GLint; width: GLsizei );
procedure glCopyTexSubImage2D( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; x: GLint; y: GLint; width: GLsizei; height: GLsizei );
procedure glCopyTexSubImage3D( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; x: GLint; y: GLint; width: GLsizei; height: GLsizei );
procedure glCullFace( mode: GLenum );
procedure glDeleteLists( list: GLuint; range: GLsizei );
procedure glDeleteTextures( n: GLsizei; const textures: PGLuint );
procedure glDepthFunc( func: GLenum );
procedure glDepthMask( flag: GLboolean );
procedure glDepthRange( zNear: GLclampd; zFar: GLclampd );
procedure glDisable( cap: GLenum );
procedure glDisableClientState( arry: GLenum );
procedure glDrawArrays( mode: GLenum; first: GLint; count: GLsizei );
procedure glDrawBuffer( mode: GLenum );
procedure glDrawElements( mode: GLenum; count: GLsizei; typ: GLenum; const indices: univ Ptr );
procedure glDrawPixels( width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; const pixels: univ Ptr );
procedure glDrawRangeElements( mode: GLenum; start: GLuint; finish: GLuint; count: GLsizei; typ: GLenum; const indices: univ Ptr );
procedure glEdgeFlag( flag: GLboolean );
procedure glEdgeFlagPointer( stride: GLsizei; const pointr: univ Ptr );
procedure glEdgeFlagv( const flag: PGLboolean );
procedure glEnable( cap: GLenum );
procedure glEnableClientState( arry: GLenum );
procedure glEnd;
procedure glEndList;
procedure glEvalCoord1d( u: GLdouble );
procedure glEvalCoord1dv( const u: PGLdouble );
procedure glEvalCoord1f( u: GLfloat );
procedure glEvalCoord1fv( const u: PGLfloat );
procedure glEvalCoord2d( u: GLdouble; v: GLdouble );
procedure glEvalCoord2dv( const u: PGLdouble );
procedure glEvalCoord2f( u: GLfloat; v: GLfloat );
procedure glEvalCoord2fv( const u: PGLfloat );
procedure glEvalMesh1( mode: GLenum; i1: GLint; i2: GLint );
procedure glEvalMesh2( mode: GLenum; i1: GLint; i2: GLint; j1: GLint; j2: GLint );
procedure glEvalPoint1( i: GLint );
procedure glEvalPoint2( i: GLint; j: GLint );
procedure glFeedbackBuffer( size: GLsizei; typ: GLenum; buffer: PGLfloat );
procedure glFinish;
procedure glFlush;
procedure glFogf( pname: GLenum; param: GLfloat );
procedure glFogfv( pname: GLenum; const params: PGLfloat );
procedure glFogi( pname: GLenum; param: GLint );
procedure glFogiv( pname: GLenum; const params: PGLint );
procedure glFrontFace( mode: GLenum );
procedure glFrustum( left: GLdouble; right: GLdouble; bottom: GLdouble; top: GLdouble; zNear: GLdouble; zFar: GLdouble );
function glGenLists( range: GLsizei ): GLuint;
procedure glGenTextures( n: GLsizei; textures: PGLuint );
procedure glGetBooleanv( pname: GLenum; params: PGLboolean );
procedure glGetClipPlane( plane: GLenum; equation: PGLdouble );
procedure glGetColorTable( target: GLenum; format: GLenum; typ: GLenum; table: univ Ptr );
procedure glGetColorTableParameterfv( target: GLenum; pname: GLenum; params: PGLfloat );
procedure glGetColorTableParameteriv( target: GLenum; pname: GLenum; params: PGLint );
procedure glGetConvolutionFilter( target: GLenum; format: GLenum; typ: GLenum; image: univ Ptr );
procedure glGetConvolutionParameterfv( target: GLenum; pname: GLenum; params: PGLfloat );
procedure glGetConvolutionParameteriv( target: GLenum; pname: GLenum; params: PGLint );
procedure glGetDoublev( pname: GLenum; params: PGLdouble );
function glGetError: GLenum;
procedure glGetFloatv( pname: GLenum; params: PGLfloat );
procedure glGetHistogram( target: GLenum; reset: GLboolean; format: GLenum; typ: GLenum; values: univ Ptr );
procedure glGetHistogramParameterfv( target: GLenum; pname: GLenum; params: PGLfloat );
procedure glGetHistogramParameteriv( target: GLenum; pname: GLenum; params: PGLint );
procedure glGetIntegerv( pname: GLenum; params: PGLint );
procedure glGetLightfv( light: GLenum; pname: GLenum; params: PGLfloat );
procedure glGetLightiv( light: GLenum; pname: GLenum; params: PGLint );
procedure glGetMapdv( target: GLenum; query: GLenum; v: PGLdouble );
procedure glGetMapfv( target: GLenum; query: GLenum; v: PGLfloat );
procedure glGetMapiv( target: GLenum; query: GLenum; v: PGLint );
procedure glGetMaterialfv( face: GLenum; pname: GLenum; params: PGLfloat );
procedure glGetMaterialiv( face: GLenum; pname: GLenum; params: PGLint );
procedure glGetMinmax( target: GLenum; reset: GLboolean; format: GLenum; typ: GLenum; values: univ Ptr );
procedure glGetMinmaxParameterfv( target: GLenum; pname: GLenum; params: PGLfloat );
procedure glGetMinmaxParameteriv( target: GLenum; pname: GLenum; params: PGLint );
procedure glGetPixelMapfv( map: GLenum; values: PGLfloat );
procedure glGetPixelMapuiv( map: GLenum; values: PGLuint );
procedure glGetPixelMapusv( map: GLenum; values: PGLushort );
procedure glGetPointerv( pname: GLenum; params: UnivPtrPtr );
procedure glGetPolygonStipple( mask: PGLubyte );
procedure glGetSeparableFilter( target: GLenum; format: GLenum; typ: GLenum; row: univ Ptr; column: univ Ptr; span: univ Ptr );
function glGetString( name: GLenum ): PChar;
procedure glGetTexEnvfv( target: GLenum; pname: GLenum; params: PGLfloat );
procedure glGetTexEnviv( target: GLenum; pname: GLenum; params: PGLint );
procedure glGetTexGendv( coord: GLenum; pname: GLenum; params: PGLdouble );
procedure glGetTexGenfv( coord: GLenum; pname: GLenum; params: PGLfloat );
procedure glGetTexGeniv( coord: GLenum; pname: GLenum; params: PGLint );
procedure glGetTexImage( target: GLenum; level: GLint; format: GLenum; typ: GLenum; pixels: univ Ptr );
procedure glGetTexLevelParameterfv( target: GLenum; level: GLint; pname: GLenum; params: PGLfloat );
procedure glGetTexLevelParameteriv( target: GLenum; level: GLint; pname: GLenum; params: PGLint );
procedure glGetTexParameterfv( target: GLenum; pname: GLenum; params: PGLfloat );
procedure glGetTexParameteriv( target: GLenum; pname: GLenum; params: PGLint );
procedure glHint( target: GLenum; mode: GLenum );
procedure glHistogram( target: GLenum; width: GLsizei; internalformat: GLenum; sink: GLboolean );
procedure glIndexMask( mask: GLuint );
procedure glIndexPointer( typ: GLenum; stride: GLsizei; const pointr: univ Ptr );
procedure glIndexd( c: GLdouble );
procedure glIndexdv( const c: PGLdouble );
procedure glIndexf( c: GLfloat );
procedure glIndexfv( const c: PGLfloat );
procedure glIndexi( c: GLint );
procedure glIndexiv( const c: PGLint );
procedure glIndexs( c: GLshort );
procedure glIndexsv( const c: PGLshort );
procedure glIndexub( c: GLubyte );
procedure glIndexubv( const c: PGLubyte );
procedure glInitNames;
procedure glInterleavedArrays( format: GLenum; stride: GLsizei; const pointr: univ Ptr );
function glIsEnabled( cap: GLenum ): GLboolean;
function glIsList( list: GLuint ): GLboolean;
function glIsTexture( texture: GLuint ): GLboolean;
procedure glLightModelf( pname: GLenum; param: GLfloat );
procedure glLightModelfv( pname: GLenum; const params: PGLfloat );
procedure glLightModeli( pname: GLenum; param: GLint );
procedure glLightModeliv( pname: GLenum; const params: PGLint );
procedure glLightf( light: GLenum; pname: GLenum; param: GLfloat );
procedure glLightfv( light: GLenum; pname: GLenum; const params: PGLfloat );
procedure glLighti( light: GLenum; pname: GLenum; param: GLint );
procedure glLightiv( light: GLenum; pname: GLenum; const params: PGLint );
procedure glLineStipple( factor: GLint; pattern: GLushort );
procedure glLineWidth( width: GLfloat );
procedure glListBase( base: GLuint );
procedure glLoadIdentity;
procedure glLoadMatrixd( const m: PGLdouble );
procedure glLoadMatrixf( const m: PGLfloat );
procedure glLoadName( name: GLuint );
procedure glLogicOp( opcode: GLenum );
procedure glMap1d( target: GLenum; u1: GLdouble; u2: GLdouble; stride: GLint; order: GLint; const points: PGLdouble );
procedure glMap1f( target: GLenum; u1: GLfloat; u2: GLfloat; stride: GLint; order: GLint; const points: PGLfloat );
procedure glMap2d( target: GLenum; u1: GLdouble; u2: GLdouble; ustride: GLint; uorder: GLint; v1: GLdouble; v2: GLdouble; vstride: GLint; vorder: GLint; const points: PGLdouble );
procedure glMap2f( target: GLenum; u1: GLfloat; u2: GLfloat; ustride: GLint; uorder: GLint; v1: GLfloat; v2: GLfloat; vstride: GLint; vorder: GLint; const points: PGLfloat );
procedure glMapGrid1d( un: GLint; u1: GLdouble; u2: GLdouble );
procedure glMapGrid1f( un: GLint; u1: GLfloat; u2: GLfloat );
procedure glMapGrid2d( un: GLint; u1: GLdouble; u2: GLdouble; vn: GLint; v1: GLdouble; v2: GLdouble );
procedure glMapGrid2f( un: GLint; u1: GLfloat; u2: GLfloat; vn: GLint; v1: GLfloat; v2: GLfloat );
procedure glMaterialf( face: GLenum; pname: GLenum; param: GLfloat );
procedure glMaterialfv( face: GLenum; pname: GLenum; const params: PGLfloat );
procedure glMateriali( face: GLenum; pname: GLenum; param: GLint );
procedure glMaterialiv( face: GLenum; pname: GLenum; const params: PGLint );
procedure glMatrixMode( mode: GLenum );
procedure glMinmax( target: GLenum; internalformat: GLenum; sink: GLboolean );
procedure glMultMatrixd( const m: PGLdouble );
procedure glMultMatrixf( const m: PGLfloat );
procedure glNewList( list: GLuint; mode: GLenum );
procedure glNormal3b( nx: GLbyte; ny: GLbyte; nz: GLbyte );
procedure glNormal3bv( const v: PGLbyte );
procedure glNormal3d( nx: GLdouble; ny: GLdouble; nz: GLdouble );
procedure glNormal3dv( const v: PGLdouble );
procedure glNormal3f( nx: GLfloat; ny: GLfloat; nz: GLfloat );
procedure glNormal3fv( const v: PGLfloat );
procedure glNormal3i( nx: GLint; ny: GLint; nz: GLint );
procedure glNormal3iv( const v: PGLint );
procedure glNormal3s( nx: GLshort; ny: GLshort; nz: GLshort );
procedure glNormal3sv( const v: PGLshort );
procedure glNormalPointer( typ: GLenum; stride: GLsizei; const pointr: univ Ptr );
procedure glOrtho( left: GLdouble; right: GLdouble; bottom: GLdouble; top: GLdouble; zNear: GLdouble; zFar: GLdouble );
procedure glPassThrough( token: GLfloat );
procedure glPixelMapfv( map: GLenum; mapsize: GLint; const values: PGLfloat );
procedure glPixelMapuiv( map: GLenum; mapsize: GLint; const values: PGLuint );
procedure glPixelMapusv( map: GLenum; mapsize: GLint; const values: PGLushort );
procedure glPixelStoref( pname: GLenum; param: GLfloat );
procedure glPixelStorei( pname: GLenum; param: GLint );
procedure glPixelTransferf( pname: GLenum; param: GLfloat );
procedure glPixelTransferi( pname: GLenum; param: GLint );
procedure glPixelZoom( xfactor: GLfloat; yfactor: GLfloat );
procedure glPointSize( size: GLfloat );
procedure glPolygonMode( face: GLenum; mode: GLenum );
procedure glPolygonOffset( factor: GLfloat; units: GLfloat );
procedure glPolygonStipple( const mask: PGLubyte );
procedure glPopAttrib;
procedure glPopClientAttrib;
procedure glPopMatrix;
procedure glPopName;
procedure glPrioritizeTextures( n: GLsizei; const textures: PGLuint; const priorities: PGLclampf );
procedure glPushAttrib( mask: GLbitfield );
procedure glPushClientAttrib( mask: GLbitfield );
procedure glPushMatrix;
procedure glPushName( name: GLuint );
procedure glRasterPos2d( x: GLdouble; y: GLdouble );
procedure glRasterPos2dv( const v: PGLdouble );
procedure glRasterPos2f( x: GLfloat; y: GLfloat );
procedure glRasterPos2fv( const v: PGLfloat );
procedure glRasterPos2i( x: GLint; y: GLint );
procedure glRasterPos2iv( const v: PGLint );
procedure glRasterPos2s( x: GLshort; y: GLshort );
procedure glRasterPos2sv( const v: PGLshort );
procedure glRasterPos3d( x: GLdouble; y: GLdouble; z: GLdouble );
procedure glRasterPos3dv( const v: PGLdouble );
procedure glRasterPos3f( x: GLfloat; y: GLfloat; z: GLfloat );
procedure glRasterPos3fv( const v: PGLfloat );
procedure glRasterPos3i( x: GLint; y: GLint; z: GLint );
procedure glRasterPos3iv( const v: PGLint );
procedure glRasterPos3s( x: GLshort; y: GLshort; z: GLshort );
procedure glRasterPos3sv( const v: PGLshort );
procedure glRasterPos4d( x: GLdouble; y: GLdouble; z: GLdouble; w: GLdouble );
procedure glRasterPos4dv( const v: PGLdouble );
procedure glRasterPos4f( x: GLfloat; y: GLfloat; z: GLfloat; w: GLfloat );
procedure glRasterPos4fv( const v: PGLfloat );
procedure glRasterPos4i( x: GLint; y: GLint; z: GLint; w: GLint );
procedure glRasterPos4iv( const v: PGLint );
procedure glRasterPos4s( x: GLshort; y: GLshort; z: GLshort; w: GLshort );
procedure glRasterPos4sv( const v: PGLshort );
procedure glReadBuffer( mode: GLenum );
procedure glReadPixels( x: GLint; y: GLint; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; pixels: univ Ptr );
procedure glRectd( x1: GLdouble; y1: GLdouble; x2: GLdouble; y2: GLdouble );
procedure glRectdv( const v1: PGLdouble; const v2: PGLdouble );
procedure glRectf( x1: GLfloat; y1: GLfloat; x2: GLfloat; y2: GLfloat );
procedure glRectfv( const v1: PGLfloat; const v2: PGLfloat );
procedure glRecti( x1: GLint; y1: GLint; x2: GLint; y2: GLint );
procedure glRectiv( const v1: PGLint; const v2: PGLint );
procedure glRects( x1: GLshort; y1: GLshort; x2: GLshort; y2: GLshort );
procedure glRectsv( const v1: PGLshort; const v2: PGLshort );
function glRenderMode( mode: GLenum ): GLint;
procedure glResetHistogram( target: GLenum );
procedure glResetMinmax( target: GLenum );
procedure glRotated( angle: GLdouble; x: GLdouble; y: GLdouble; z: GLdouble );
procedure glRotatef( angle: GLfloat; x: GLfloat; y: GLfloat; z: GLfloat );
procedure glScaled( x: GLdouble; y: GLdouble; z: GLdouble );
procedure glScalef( x: GLfloat; y: GLfloat; z: GLfloat );
procedure glScissor( x: GLint; y: GLint; width: GLsizei; height: GLsizei );
procedure glSelectBuffer( size: GLsizei; buffer: PGLuint );
procedure glSeparableFilter2D( target: GLenum; internalformat: GLenum; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; const row: univ Ptr; const column: univ Ptr );
procedure glShadeModel( mode: GLenum );
procedure glStencilFunc( func: GLenum; ref: GLint; mask: GLuint );
procedure glStencilMask( mask: GLuint );
procedure glStencilOp( fail: GLenum; zfail: GLenum; zpass: GLenum );
procedure glTexCoord1d( s: GLdouble );
procedure glTexCoord1dv( const v: PGLdouble );
procedure glTexCoord1f( s: GLfloat );
procedure glTexCoord1fv( const v: PGLfloat );
procedure glTexCoord1i( s: GLint );
procedure glTexCoord1iv( const v: PGLint );
procedure glTexCoord1s( s: GLshort );
procedure glTexCoord1sv( const v: PGLshort );
procedure glTexCoord2d( s: GLdouble; t: GLdouble );
procedure glTexCoord2dv( const v: PGLdouble );
procedure glTexCoord2f( s: GLfloat; t: GLfloat );
procedure glTexCoord2fv( const v: PGLfloat );
procedure glTexCoord2i( s: GLint; t: GLint );
procedure glTexCoord2iv( const v: PGLint );
procedure glTexCoord2s( s: GLshort; t: GLshort );
procedure glTexCoord2sv( const v: PGLshort );
procedure glTexCoord3d( s: GLdouble; t: GLdouble; r: GLdouble );
procedure glTexCoord3dv( const v: PGLdouble );
procedure glTexCoord3f( s: GLfloat; t: GLfloat; r: GLfloat );
procedure glTexCoord3fv( const v: PGLfloat );
procedure glTexCoord3i( s: GLint; t: GLint; r: GLint );
procedure glTexCoord3iv( const v: PGLint );
procedure glTexCoord3s( s: GLshort; t: GLshort; r: GLshort );
procedure glTexCoord3sv( const v: PGLshort );
procedure glTexCoord4d( s: GLdouble; t: GLdouble; r: GLdouble; q: GLdouble );
procedure glTexCoord4dv( const v: PGLdouble );
procedure glTexCoord4f( s: GLfloat; t: GLfloat; r: GLfloat; q: GLfloat );
procedure glTexCoord4fv( const v: PGLfloat );
procedure glTexCoord4i( s: GLint; t: GLint; r: GLint; q: GLint );
procedure glTexCoord4iv( const v: PGLint );
procedure glTexCoord4s( s: GLshort; t: GLshort; r: GLshort; q: GLshort );
procedure glTexCoord4sv( const v: PGLshort );
procedure glTexCoordPointer( size: GLint; typ: GLenum; stride: GLsizei; const pointr: univ Ptr );
procedure glTexEnvf( target: GLenum; pname: GLenum; param: GLfloat );
procedure glTexEnvfv( target: GLenum; pname: GLenum; const params: PGLfloat );
procedure glTexEnvi( target: GLenum; pname: GLenum; param: GLint );
procedure glTexEnviv( target: GLenum; pname: GLenum; const params: PGLint );
procedure glTexGend( coord: GLenum; pname: GLenum; param: GLdouble );
procedure glTexGendv( coord: GLenum; pname: GLenum; const params: PGLdouble );
procedure glTexGenf( coord: GLenum; pname: GLenum; param: GLfloat );
procedure glTexGenfv( coord: GLenum; pname: GLenum; const params: PGLfloat );
procedure glTexGeni( coord: GLenum; pname: GLenum; param: GLint );
procedure glTexGeniv( coord: GLenum; pname: GLenum; const params: PGLint );
procedure glTexImage1D( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; border: GLint; format: GLenum; typ: GLenum; const pixels: univ Ptr );
procedure glTexImage2D( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; border: GLint; format: GLenum; typ: GLenum; const pixels: univ Ptr );
procedure glTexImage3D( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; border: GLint; format: GLenum; typ: GLenum; const pixels: univ Ptr );
procedure glTexParameterf( target: GLenum; pname: GLenum; param: GLfloat );
procedure glTexParameterfv( target: GLenum; pname: GLenum; const params: PGLfloat );
procedure glTexParameteri( target: GLenum; pname: GLenum; param: GLint );
procedure glTexParameteriv( target: GLenum; pname: GLenum; const params: PGLint );
procedure glTexSubImage1D( target: GLenum; level: GLint; xoffset: GLint; width: GLsizei; format: GLenum; typ: GLenum; const pixels: univ Ptr );
procedure glTexSubImage2D( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; width: GLsizei; height: GLsizei; format: GLenum; typ: GLenum; const pixels: univ Ptr );
procedure glTexSubImage3D( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; typ: GLenum; const pixels: univ Ptr );
procedure glTranslated( x: GLdouble; y: GLdouble; z: GLdouble );
procedure glTranslatef( x: GLfloat; y: GLfloat; z: GLfloat );
procedure glVertex2d( x: GLdouble; y: GLdouble );
procedure glVertex2dv( const v: PGLdouble );
procedure glVertex2f( x: GLfloat; y: GLfloat );
procedure glVertex2fv( const v: PGLfloat );
procedure glVertex2i( x: GLint; y: GLint );
procedure glVertex2iv( const v: PGLint );
procedure glVertex2s( x: GLshort; y: GLshort );
procedure glVertex2sv( const v: PGLshort );
procedure glVertex3d( x: GLdouble; y: GLdouble; z: GLdouble );
procedure glVertex3dv( const v: PGLdouble );
procedure glVertex3f( x: GLfloat; y: GLfloat; z: GLfloat );
procedure glVertex3fv( const v: PGLfloat );
procedure glVertex3i( x: GLint; y: GLint; z: GLint );
procedure glVertex3iv( const v: PGLint );
procedure glVertex3s( x: GLshort; y: GLshort; z: GLshort );
procedure glVertex3sv( const v: PGLshort );
procedure glVertex4d( x: GLdouble; y: GLdouble; z: GLdouble; w: GLdouble );
procedure glVertex4dv( const v: PGLdouble );
procedure glVertex4f( x: GLfloat; y: GLfloat; z: GLfloat; w: GLfloat );
procedure glVertex4fv( const v: PGLfloat );
procedure glVertex4i( x: GLint; y: GLint; z: GLint; w: GLint );
procedure glVertex4iv( const v: PGLint );
procedure glVertex4s( x: GLshort; y: GLshort; z: GLshort; w: GLshort );
procedure glVertex4sv( const v: PGLshort );
procedure glVertexPointer( size: GLint; typ: GLenum; stride: GLsizei; const pointr: univ Ptr );
procedure glViewport( x: GLint; y: GLint; width: GLsizei; height: GLsizei );

procedure glSampleCoverage( value: GLclampf; invert: GLboolean );
procedure glSamplePass( pass: GLenum );

procedure glLoadTransposeMatrixf( const m: PGLfloat );
procedure glLoadTransposeMatrixd( const m: PGLdouble );
procedure glMultTransposeMatrixf( const m: PGLfloat );
procedure glMultTransposeMatrixd( const m: PGLdouble );

procedure glCompressedTexImage3D( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; border: GLint; imageSize: GLsizei; const data: univ Ptr );
procedure glCompressedTexImage2D( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; border: GLint; imageSize: GLsizei; const data: univ Ptr );
procedure glCompressedTexImage1D( target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; border: GLint; imageSize: GLsizei; const data: univ Ptr );
procedure glCompressedTexSubImage3D( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; imageSize: GLsizei; const data: univ Ptr );
procedure glCompressedTexSubImage2D( target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; width: GLsizei; height: GLsizei; format: GLenum; imageSize: GLsizei; const data: univ Ptr );
procedure glCompressedTexSubImage1D( target: GLenum; level: GLint; xoffset: GLint; width: GLsizei; format: GLenum; imageSize: GLsizei; const data: univ Ptr );
procedure glGetCompressedTexImage( target: GLenum; lod: GLint; img: univ Ptr );

procedure glActiveTexture( texture: GLenum );
procedure glClientActiveTexture( texture: GLenum );
procedure glMultiTexCoord1d( target: GLenum; s: GLdouble );
procedure glMultiTexCoord1dv( target: GLenum; const v: PGLdouble );
procedure glMultiTexCoord1f( target: GLenum; s: GLfloat );
procedure glMultiTexCoord1fv( target: GLenum; const v: PGLfloat );
procedure glMultiTexCoord1i( target: GLenum; s: GLint );
procedure glMultiTexCoord1iv( target: GLenum; const v: PGLint );
procedure glMultiTexCoord1s( target: GLenum; s: GLshort );
procedure glMultiTexCoord1sv( target: GLenum; const v: PGLshort );
procedure glMultiTexCoord2d( target: GLenum; s: GLdouble; t: GLdouble );
procedure glMultiTexCoord2dv( target: GLenum; const v: PGLdouble );
procedure glMultiTexCoord2f( target: GLenum; s: GLfloat; t: GLfloat );
procedure glMultiTexCoord2fv( target: GLenum; const v: PGLfloat );
procedure glMultiTexCoord2i( target: GLenum; s: GLint; t: GLint );
procedure glMultiTexCoord2iv( target: GLenum; const v: PGLint );
procedure glMultiTexCoord2s( target: GLenum; s: GLshort; t: GLshort );
procedure glMultiTexCoord2sv( target: GLenum; const v: PGLshort );
procedure glMultiTexCoord3d( target: GLenum; s: GLdouble; t: GLdouble; r: GLdouble );
procedure glMultiTexCoord3dv( target: GLenum; const v: PGLdouble );
procedure glMultiTexCoord3f( target: GLenum; s: GLfloat; t: GLfloat; r: GLfloat );
procedure glMultiTexCoord3fv( target: GLenum; const v: PGLfloat );
procedure glMultiTexCoord3i( target: GLenum; s: GLint; t: GLint; r: GLint );
procedure glMultiTexCoord3iv( target: GLenum; const v: PGLint );
procedure glMultiTexCoord3s( target: GLenum; s: GLshort; t: GLshort; r: GLshort );
procedure glMultiTexCoord3sv( target: GLenum; const v: PGLshort );
procedure glMultiTexCoord4d( target: GLenum; s: GLdouble; t: GLdouble; r: GLdouble; q: GLdouble );
procedure glMultiTexCoord4dv( target: GLenum; const v: PGLdouble );
procedure glMultiTexCoord4f( target: GLenum; s: GLfloat; t: GLfloat; r: GLfloat; q: GLfloat );
procedure glMultiTexCoord4fv( target: GLenum; const v: PGLfloat );
procedure glMultiTexCoord4i( target: GLenum; param1 : GLint; s: GLint; t: GLint; r: GLint );
procedure glMultiTexCoord4iv( target: GLenum; const v: PGLint );
procedure glMultiTexCoord4s( target: GLenum; s: GLshort; t: GLshort; r: GLshort; q: GLshort );
procedure glMultiTexCoord4sv( target: GLenum; const v: PGLshort );

procedure glFogCoordf( coord: GLfloat );
procedure glFogCoordfv( const coord: PGLfloat );  
procedure glFogCoordd( coord: GLdouble );
procedure glFogCoorddv( const coord: PGLdouble );   
procedure glFogCoordPointer( typ: GLenum; stride: GLsizei; const pointr: univ Ptr );

procedure glSecondaryColor3b( red: GLbyte; green: GLbyte; blue: GLbyte );
procedure glSecondaryColor3bv( const v: PGLbyte );
procedure glSecondaryColor3d( red: GLdouble; green: GLdouble; blue: GLdouble );
procedure glSecondaryColor3dv( const v: PGLdouble );
procedure glSecondaryColor3f( red: GLfloat; green: GLfloat; blue: GLfloat );
procedure glSecondaryColor3fv( const v: PGLfloat );
procedure glSecondaryColor3i( red: GLint; green: GLint; blue: GLint );
procedure glSecondaryColor3iv( const v: PGLint );
procedure glSecondaryColor3s( red: GLshort; green: GLshort; blue: GLshort );
procedure glSecondaryColor3sv( const v: PGLshort );
procedure glSecondaryColor3ub( red: GLubyte; green: GLubyte; blue: GLubyte );
procedure glSecondaryColor3ubv( const v: PGLubyte );
procedure glSecondaryColor3ui( red: GLuint; green: GLuint; blue: GLuint );
procedure glSecondaryColor3uiv( const v: PGLuint );
procedure glSecondaryColor3us( red: GLushort; green: GLushort; blue: GLushort );
procedure glSecondaryColor3usv( const v: PGLushort );
procedure glSecondaryColorPointer( size: GLint; typ: GLenum; stride: GLsizei; const pointr: univ Ptr );

procedure glPointParameterf( pname: GLenum; param: GLfloat ); 
procedure glPointParameterfv( pname: GLenum; const params: PGLfloat );
procedure glPointParameteri( pname: GLenum; param: GLint ); 
procedure glPointParameteriv( pname: GLenum; const params: PGLint );

procedure glBlendFuncSeparate( srcRGB: GLenum; dstRGB: GLenum; srcAlpha: GLenum; dstAlpha: GLenum );

procedure glMultiDrawArrays( mode: GLenum; const first: PGLint; const count: PGLsizei; primcount: GLsizei );
procedure glMultiDrawElements( mode: GLenum; const count: PGLsizei; typ: GLenum; {const} indices: univ Ptr; primcount: GLsizei );

procedure glWindowPos2d( x: GLdouble; y: GLdouble );
procedure glWindowPos2dv( const v: PGLdouble );
procedure glWindowPos2f( x: GLfloat; y: GLfloat );
procedure glWindowPos2fv( const v: PGLfloat );
procedure glWindowPos2i( x: GLint; y: GLint ); 
procedure glWindowPos2iv( const v: PGLint );
procedure glWindowPos2s( x: GLshort; y: GLshort );
procedure glWindowPos2sv( const v: PGLshort );
procedure glWindowPos3d( x: GLdouble; y: GLdouble; z: GLdouble );
procedure glWindowPos3dv( const v: PGLdouble );
procedure glWindowPos3f( x: GLfloat; y: GLfloat; z: GLfloat );
procedure glWindowPos3fv( const v: PGLfloat );
procedure glWindowPos3i( x: GLint; y: GLint; z: GLint );
procedure glWindowPos3iv( const v: PGLint );
procedure glWindowPos3s( x: GLshort; y: GLshort; z: GLshort );
procedure glWindowPos3sv( const v: PGLshort );

procedure glGenQueries( n: GLsizei; ids: PGLuint );
procedure glDeleteQueries( n: GLsizei; const ids: PGLuint );
function glIsQuery( id: GLuint ): GLboolean;
procedure glBeginQuery( target: GLenum; id: GLuint );
procedure glEndQuery( target: GLenum );
procedure glGetQueryiv( target: GLenum; pname: GLenum; params: PGLint );
procedure glGetQueryObjectiv( id: GLuint; pname: GLenum; params: PGLint );
procedure glGetQueryObjectuiv( id: GLuint; pname: GLenum; params: PGLuint );

procedure glBindBuffer( target: GLenum; buffer: GLuint );
procedure glDeleteBuffers( n: GLsizei; const buffers: PGLuint );
procedure glGenBuffers( n: GLsizei; buffers: PGLuint );
function glIsBuffer( buffer: GLuint ): GLboolean;
procedure glBufferData( target: GLenum; size: GLsizeiptr; const data: univ Ptr; usage: GLenum );
procedure glBufferSubData( target: GLenum; offset: GLintptr; size: GLsizeiptr; const data: univ Ptr );
procedure glGetBufferSubData( target: GLenum; offset: GLintptr; size: GLsizeiptr; data: univ Ptr );
function glMapBuffer( target: GLenum; access: GLenum ): UnivPtr;
function glUnmapBuffer( target: GLenum ): GLboolean;
procedure glGetBufferParameteriv( target: GLenum; pname: GLenum; params: PGLint );
procedure glGetBufferPointerv( target: GLenum; pname: GLenum; params: univ Ptr );

procedure glDrawBuffers( n: GLsizei; const bufs: PGLenum );
procedure glVertexAttrib1d( index: GLuint; x: GLdouble );
procedure glVertexAttrib1dv( index: GLuint; const v: PGLdouble );
procedure glVertexAttrib1f( index: GLuint; x: GLfloat );
procedure glVertexAttrib1fv( index: GLuint; const v: PGLfloat );
procedure glVertexAttrib1s( index: GLuint; x: GLshort );
procedure glVertexAttrib1sv( index: GLuint; const v: PGLshort );
procedure glVertexAttrib2d( index: GLuint; x: GLdouble; y: GLdouble );
procedure glVertexAttrib2dv( index: GLuint; const v: PGLdouble );
procedure glVertexAttrib2f( index: GLuint; x: GLfloat; y: GLfloat );
procedure glVertexAttrib2fv( index: GLuint; const v: PGLfloat );
procedure glVertexAttrib2s( index: GLuint; x: GLshort; y: GLshort );
procedure glVertexAttrib2sv( index: GLuint; const v: PGLshort );
procedure glVertexAttrib3d( index: GLuint; x: GLdouble; y: GLdouble; z: GLdouble );
procedure glVertexAttrib3dv( index: GLuint; const v: PGLdouble );
procedure glVertexAttrib3f( index: GLuint; x: GLfloat; y: GLfloat; z: GLfloat );
procedure glVertexAttrib3fv( index: GLuint; const v: PGLfloat );
procedure glVertexAttrib3s( index: GLuint; x: GLshort; y: GLshort; z: GLshort );
procedure glVertexAttrib3sv( index: GLuint; const v: PGLshort );
procedure glVertexAttrib4Nbv( index: GLuint; const v: PGLbyte );
procedure glVertexAttrib4Niv( index: GLuint; const v: PGLint );
procedure glVertexAttrib4Nsv( index: GLuint; const v: PGLshort );
procedure glVertexAttrib4Nub( index: GLuint; x: GLubyte; y: GLubyte; z: GLubyte; w: GLubyte );
procedure glVertexAttrib4Nubv( index: GLuint; const v: PGLubyte );
procedure glVertexAttrib4Nuiv( index: GLuint; const v: PGLuint );
procedure glVertexAttrib4Nusv( index: GLuint; const v: PGLushort );
procedure glVertexAttrib4bv( index: GLuint; const v: PGLbyte );
procedure glVertexAttrib4d( index: GLuint; x: GLdouble; y: GLdouble; z: GLdouble; w: GLdouble );
procedure glVertexAttrib4dv( index: GLuint; const v: PGLdouble );
procedure glVertexAttrib4f( index: GLuint; x: GLfloat; y: GLfloat; z: GLfloat; w: GLfloat );
procedure glVertexAttrib4fv( index: GLuint; const v: PGLfloat );
procedure glVertexAttrib4iv( index: GLuint; const v: PGLint );
procedure glVertexAttrib4s( index: GLuint; x: GLshort; y: GLshort; z: GLshort; w: GLshort );
procedure glVertexAttrib4sv( index: GLuint; const v: PGLshort );
procedure glVertexAttrib4ubv( index: GLuint; const v: PGLubyte );
procedure glVertexAttrib4uiv( index: GLuint; const v: PGLuint );
procedure glVertexAttrib4usv( index: GLuint; const v: PGLushort );
procedure glVertexAttribPointer( index: GLuint; size: GLint; typ: GLenum; normalized: GLboolean; stride: GLsizei; const pointr: univ Ptr );
procedure glEnableVertexAttribArray( index: GLuint );
procedure glDisableVertexAttribArray( index: GLuint );
procedure glGetVertexAttribdv( index: GLuint; pname: GLenum; params: PGLdouble );
procedure glGetVertexAttribfv( index: GLuint; pname: GLenum; params: PGLfloat );
procedure glGetVertexAttribiv( index: GLuint; pname: GLenum; params: PGLint );
procedure glGetVertexAttribPointerv( index: GLuint; pname: GLenum; pointr: univ Ptr );
procedure glDeleteShader( shader: GLuint );
procedure glDetachShader( program_: GLuint; shader: GLuint );
function glCreateShader( typ: GLenum ): GLuint;

{GPC-ONLY-START}
procedure glShaderSource( shader: GLuint; count: GLsizei; {const} strng: CStringPtrPtr; const length: PGLint );
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
procedure glShaderSource( shader: GLuint; count: GLsizei; {const} strng: PPChar; const length: PGLint );
{FPC-ONLY-FINISH}

procedure glCompileShader( shader: GLuint );
function glCreateProgram: GLuint;
procedure glAttachShader( program_: GLuint; shader: GLuint );
procedure glLinkProgram( program_: GLuint );
procedure glUseProgram( program_: GLuint );
procedure glDeleteProgram( program_: GLuint );
procedure glValidateProgram( program_: GLuint );
procedure glUniform1f( location: GLint; v0: GLfloat );
procedure glUniform2f( location: GLint; v0: GLfloat; v1: GLfloat );
procedure glUniform3f( location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat );
procedure glUniform4f( location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat; v3: GLfloat );
procedure glUniform1i( location: GLint; v0: GLint );
procedure glUniform2i( location: GLint; v0: GLint; v1: GLint );
procedure glUniform3i( location: GLint; v0: GLint; v1: GLint; v2: GLint );
procedure glUniform4i( location: GLint; v0: GLint; v1: GLint; v2: GLint; v3: GLint );
procedure glUniform1fv( location: GLint; count: GLsizei; const value: PGLfloat );
procedure glUniform2fv( location: GLint; count: GLsizei; const value: PGLfloat );
procedure glUniform3fv( location: GLint; count: GLsizei; const value: PGLfloat );
procedure glUniform4fv( location: GLint; count: GLsizei; const value: PGLfloat );
procedure glUniform1iv( location: GLint; count: GLsizei; const value: PGLint );
procedure glUniform2iv( location: GLint; count: GLsizei; const value: PGLint );
procedure glUniform3iv( location: GLint; count: GLsizei; const value: PGLint );
procedure glUniform4iv( location: GLint; count: GLsizei; const value: PGLint );
procedure glUniformMatrix2fv( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
procedure glUniformMatrix3fv( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
procedure glUniformMatrix4fv( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
function glIsShader( shader: GLuint ): GLboolean;
function glIsProgram( program_: GLuint ): GLboolean;
procedure glGetShaderiv( shader: GLuint; pname: GLenum; params: PGLint );
procedure glGetProgramiv( program_: GLuint; pname: GLenum; params: PGLint );
procedure glGetAttachedShaders( program_: GLuint; maxCount: GLsizei; count: PGLsizei; shaders: PGLuint );
procedure glGetShaderInfoLog( shader: GLuint; bufSize: GLsizei; length: PGLsizei; infoLog: PChar );
procedure glGetProgramInfoLog( program_: GLuint; bufSize: GLsizei; length: PGLsizei; infoLog: PChar );
function glGetUniformLocation( program_: GLuint; const name: PChar ): GLint;
procedure glGetActiveUniform( program_: GLuint; index: GLuint; bufSize: GLsizei; length: PGLsizei; size: PGLint; typ: PGLenum; name: PChar );
procedure glGetUniformfv( program_: GLuint; location: GLint; params: PGLfloat );
procedure glGetUniformiv( program_: GLuint; location: GLint; params: PGLint );
procedure glGetShaderSource( shader: GLuint; bufSize: GLsizei; length: PGLsizei; source: PChar );
procedure glBindAttribLocation( program_: GLuint; index: GLuint; const name: PChar );
procedure glGetActiveAttrib( program_: GLuint; index: GLuint; bufSize: GLsizei; length: PGLsizei; size: PGLint; typ: PGLenum; name: PChar );
function glGetAttribLocation( program_: GLuint; const name: PChar ): GLint;
procedure glStencilFuncSeparate( face: GLenum; func: GLenum; ref: GLint; mask: GLuint );
procedure glStencilOpSeparate( face: GLenum; fail: GLenum; zfail: GLenum; zpass: GLenum );
procedure glStencilMaskSeparate( face: GLenum; mask: GLuint );

procedure glUniformMatrix2x3fv( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
procedure glUniformMatrix3x2fv( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
procedure glUniformMatrix2x4fv( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
procedure glUniformMatrix4x2fv( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
procedure glUniformMatrix3x4fv( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );
procedure glUniformMatrix4x3fv( location: GLint; count: GLsizei; transpose: GLboolean; const value: PGLfloat );


{$endc} { GL_GLEXT_FUNCTION_POINTERS }

{$endc} {TARGET_OS_MAC}

//#endif { __gl_h_ }

end.
