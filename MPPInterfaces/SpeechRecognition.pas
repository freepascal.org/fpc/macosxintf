{
     File:       SpeechRecognition/SpeechRecognition.h
 
     Contains:   Apple Speech Recognition Toolbox Interfaces.
 
     Version:    SpeechRecognition-4.1.5~11
 
     Copyright:  � 1992-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit SpeechRecognition;
interface
uses MacTypes,Files,AEDataModel;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{ Error Codes [Speech recognition gets -5100 through -5199] }
const
	kSRNotAvailable = -5100; { the service requested is not avail or applicable }
	kSRInternalError = -5101; { a system internal or hardware error condition }
	kSRComponentNotFound = -5102; { a needed system resource was not located }
	kSROutOfMemory = -5103; { an out of memory error occurred in the toolbox memory space }
	kSRNotASpeechObject = -5104; { the object specified is no longer or never was valid }
	kSRBadParameter = -5105; { an invalid parameter was specified }
	kSRParamOutOfRange = -5106; { when we say 0-100, don't pass in 101. }
	kSRBadSelector = -5107; { an unrecognized selector was specified }
	kSRBufferTooSmall = -5108; { returned from attribute access functions }
	kSRNotARecSystem = -5109; { the object used was not a SRRecognitionSystem }
	kSRFeedbackNotAvail = -5110; { there is no feedback window associated with SRRecognizer }
	kSRCantSetProperty = -5111; { a non-settable property was specified }
	kSRCantGetProperty = -5112; { a non-gettable property was specified }
	kSRCantSetDuringRecognition = -5113; { the property can't be set while recognition is in progress -- do before or between utterances. }
	kSRAlreadyListening = -5114; { in response to SRStartListening }
	kSRNotListeningState = -5115; { in response to SRStopListening }
	kSRModelMismatch = -5116; { no acoustical models are avail to match request }
	kSRNoClientLanguageModel = -5117; { trying to access a non-specified SRLanguageModel }
	kSRNoPendingUtterances = -5118; { nothing to continue search on }
	kSRRecognitionCanceled = -5119; { an abort error occurred during search }
	kSRRecognitionDone = -5120; { search has finished, but nothing was recognized }
	kSROtherRecAlreadyModal = -5121; { another recognizer is modal at the moment, so can't set this recognizer's kSRBlockModally property right now }
	kSRHasNoSubItems = -5122; { SRCountItems or related routine was called on an object without subelements -- e.g. a word -- rather than phrase, path, or LM. }
	kSRSubItemNotFound = -5123; { returned when accessing a non-existent sub item of a container }
	kSRLanguageModelTooBig = -5124; { Cant build language models so big }
	kSRAlreadyReleased = -5125; { this object has already been released before }
	kSRAlreadyFinished = -5126; { the language model can't be finished twice }
	kSRWordNotFound = -5127; { the spelling couldn't be found in lookup(s) }
	kSRNotFinishedWithRejection = -5128; { property not found because the LMObj is not finished with rejection }
	kSRExpansionTooDeep = -5129; { Language model is left recursive or is embedded too many levels }
	kSRTooManyElements = -5130; { Too many elements added to phrase or path or other langauge model object }
	kSRCantAdd = -5131; { Can't add given type of object to the base SRLanguageObject (e.g.in SRAddLanguageObject)   }
	kSRSndInSourceDisconnected = -5132; { Sound input source is disconnected }
	kSRCantReadLanguageObject = -5133; { An error while trying to create new Language object from file or pointer -- possibly bad format }
                                        { non-release debugging error codes are included here }
	kSRNotImplementedYet = -5199; { you'd better wait for this feature in a future release }


{ Type Definitions }
type
	SRSpeechObject = ^OpaqueSRSpeechObject; { an opaque type }
	OpaqueSRSpeechObject = record end;
	SRSpeechObjectPtr = ^SRSpeechObject;  { when a var xx:SRSpeechObject parameter can be nil, it is changed to xx: SRSpeechObjectPtr }
	SRRecognitionSystem = SRSpeechObject;
	SRRecognizer = SRSpeechObject;
	SRSpeechSource = SRSpeechObject;
	SRRecognitionResult = SRSpeechSource;
	SRLanguageObject = SRSpeechObject;
	SRLanguageModel = SRLanguageObject;
	SRPath = SRLanguageObject;
	SRPhrase = SRLanguageObject;
	SRWord = SRLanguageObject;
{ between 0 and 100 }
type
	SRSpeedSetting = UInt16;
{ between 0 and 100 }
type
	SRRejectionLevel = UInt16;
{ When an event occurs, the user supplied proc will be called with a pointer   }
{  to the param passed in and a flag to indicate conditions such               }
{  as interrupt time or system background time.                                }
type
	SRCallBackStructPtr = ^SRCallBackStruct;
	SRCallBackStruct = record
		what: UInt32;                   { one of notification flags }
		message: SIGNEDLONG;                { contains SRRecognitionResult id (32 / 64 bits) }
		instance: SRRecognizer;               { ID of recognizer being notified }
		status: OSErr;                 { result status of last search }
		flags: SInt16;                  { non-zero if occurs during interrupt }
		refCon: SRefCon;                 { user defined - set from SRCallBackParam }
	end;
{ Call back procedure definition }
type
	SRCallBackProcPtr = procedure( var param: SRCallBackStruct );
{GPC-ONLY-START}
	SRCallBackUPP = UniversalProcPtr; // should be SRCallBackProcPtr
{GPC-ONLY-ELSE}
	SRCallBackUPP = SRCallBackProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewSRCallBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSRCallBackUPP( userRoutine: SRCallBackProcPtr ): SRCallBackUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeSRCallBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSRCallBackUPP( userUPP: SRCallBackUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeSRCallBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSRCallBackUPP( var param: SRCallBackStruct; userUPP: SRCallBackUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

type
	SRCallBackParamPtr = ^SRCallBackParam;
	SRCallBackParam = record
		callBack: SRCallBackUPP;
		refCon: SRefCon;
	end;
{ Recognition System Types }
const
	kSRDefaultRecognitionSystemID = 0;

{ Recognition System Properties }
const
	kSRFeedbackAndListeningModes = FOUR_CHAR_CODE('fbwn'); { short: one of kSRNoFeedbackHasListenModes, kSRHasFeedbackHasListenModes, kSRNoFeedbackNoListenModes }
	kSRRejectedWord = FOUR_CHAR_CODE('rejq'); { the SRWord used to represent a rejection }
	kSRCleanupOnClientExit = FOUR_CHAR_CODE('clup'); { Boolean: Default is true. The rec system and everything it owns is disposed when the client application quits }

const
	kSRNoFeedbackNoListenModes = 0;    { next allocated recognizer has no feedback window and doesn't use listening modes   }
	kSRHasFeedbackHasListenModes = 1;    { next allocated recognizer has feedback window and uses listening modes          }
	kSRNoFeedbackHasListenModes = 2;     { next allocated recognizer has no feedback window but does use listening modes  }

{ Speech Source Types }
const
	kSRDefaultSpeechSource = 0;
	kSRLiveDesktopSpeechSource = FOUR_CHAR_CODE('dklv'); { live desktop sound input }
	kSRCanned22kHzSpeechSource = FOUR_CHAR_CODE('ca22'); { AIFF file based 16 bit, 22.050 KHz sound input }

{ Notification via Apple Event or Callback }
{ Notification Flags }
const
	kSRNotifyRecognitionBeginning = 1 shl 0; { recognition can begin. client must now call SRContinueRecognition or SRCancelRecognition }
	kSRNotifyRecognitionDone = 1 shl 1; { recognition has terminated. result (if any) is available. }

{ Apple Event selectors }
{ AppleEvent message class  }
const
	kAESpeechSuite = FOUR_CHAR_CODE('sprc');

{ AppleEvent message event ids }
const
	kAESpeechDone = FOUR_CHAR_CODE('srsd');
	kAESpeechDetected = FOUR_CHAR_CODE('srbd');

{ AppleEvent Parameter ids }
const
	keySRRecognizer = FOUR_CHAR_CODE('krec');
	keySRSpeechResult = FOUR_CHAR_CODE('kspr');
	keySRSpeechStatus = FOUR_CHAR_CODE('ksst');

{ AppleEvent Parameter types }
const
	typeSRRecognizer = FOUR_CHAR_CODE('trec');
	typeSRSpeechResult = FOUR_CHAR_CODE('tspr');


{ SRRecognizer Properties }
const
	kSRNotificationParam = FOUR_CHAR_CODE('noti'); { SInt32: See notification flags below }
	kSRCallBackParam = FOUR_CHAR_CODE('call'); { type SRCallBackParam }
	kSRSearchStatusParam = FOUR_CHAR_CODE('stat'); { SInt32: see status flags below }
	kSRAutoFinishingParam = FOUR_CHAR_CODE('afin'); { SInt32: Automatic finishing applied on LM for search }
	kSRForegroundOnly = FOUR_CHAR_CODE('fgon'); { Boolean: Default is true. If true, client recognizer only active when in foreground.   }
	kSRBlockBackground = FOUR_CHAR_CODE('blbg'); { Boolean: Default is false. If true, when client recognizer in foreground, rest of LMs are inactive.    }
	kSRBlockModally = FOUR_CHAR_CODE('blmd'); { Boolean: Default is false. When true, this client's LM is only active LM; all other LMs are inactive. Be nice, don't be modal for long periods! }
	kSRWantsResultTextDrawn = FOUR_CHAR_CODE('txfb'); { Boolean: Default is true. If true, search results are posted to Feedback window }
	kSRWantsAutoFBGestures = FOUR_CHAR_CODE('dfbr'); { Boolean: Default is true. If true, client needn't call SRProcessBegin/End to get default feedback behavior }
	kSRSoundInVolume = FOUR_CHAR_CODE('volu'); { short in [0..100] log scaled sound input power. Can't set this property }
	kSRReadAudioFSSpec = FOUR_CHAR_CODE('aurd'); { *FSSpec: Specify FSSpec where raw audio is to be read (AIFF format) using kSRCanned22kHzSpeechSource. Reads until EOF }
	kSRReadAudioURL = FOUR_CHAR_CODE('aurl'); { CFURLRef: Specify CFURLRef where raw audio is to be read (AIFF format) using kSRCanned22kHzSpeechSource. Reads until EOF }
	kSRCancelOnSoundOut = FOUR_CHAR_CODE('caso'); { Boolean: Default is true.  If any sound is played out during utterance, recognition is aborted. }
	kSRSpeedVsAccuracyParam = FOUR_CHAR_CODE('sped'); { SRSpeedSetting between 0 and 100 }

{ 0 means more accurate but slower. }
{ 100 means (much) less accurate but faster. }
const
	kSRUseToggleListen = 0;    { listen key modes }
	kSRUsePushToTalk = 1;

const
	kSRListenKeyMode = FOUR_CHAR_CODE('lkmd'); { short: either kSRUseToggleListen or kSRUsePushToTalk }
	kSRListenKeyCombo = FOUR_CHAR_CODE('lkey'); { short: Push-To-Talk key combination; high byte is high byte of event->modifiers, the low byte is the keycode from event->message }
	kSRListenKeyName = FOUR_CHAR_CODE('lnam'); { Str63: string representing ListenKeyCombo }
	kSRKeyWord = FOUR_CHAR_CODE('kwrd'); { Str255: keyword preceding spoken commands in kSRUseToggleListen mode }
	kSRKeyExpected = FOUR_CHAR_CODE('kexp'); { Boolean: Must the PTT key be depressed or the key word spoken before recognition can occur? }

{ Operational Status Flags }
const
	kSRIdleRecognizer = 1 shl 0; { engine is not active }
	kSRSearchInProgress = 1 shl 1; { search is in progress }
	kSRSearchWaitForAllClients = 1 shl 2; { search is suspended waiting on all clients' input }
	kSRMustCancelSearch = 1 shl 3; { something has occurred (sound played, non-speech detected) requiring the search to abort }
	kSRPendingSearch = 1 shl 4; { we're about to start searching }

{ Recognition Result Properties }
const
	kSRTEXTFormat = FOUR_CHAR_CODE('TEXT'); { raw text in user supplied memory }
	kSRPhraseFormat = FOUR_CHAR_CODE('lmph'); { SRPhrase containing result words }
	kSRPathFormat = FOUR_CHAR_CODE('lmpt'); { SRPath containing result phrases or words }
	kSRLanguageModelFormat = FOUR_CHAR_CODE('lmfm'); { top level SRLanguageModel for post parse }

{ SRLanguageObject Family Properties }
const
	kSRSpelling = FOUR_CHAR_CODE('spel'); { spelling of a SRWord or SRPhrase or SRPath, or name of a SRLanguageModel }
	kSRLMObjType = FOUR_CHAR_CODE('lmtp'); { Returns one of SRLanguageObject Types listed below }
	kSRRefCon = FOUR_CHAR_CODE('refc'); { long (4/8 bytes) for user storage }
	kSROptional = FOUR_CHAR_CODE('optl'); { Boolean -- true if SRLanguageObject is optional    }
	kSREnabled = FOUR_CHAR_CODE('enbl'); { Boolean -- true if SRLanguageObject enabled }
	kSRRepeatable = FOUR_CHAR_CODE('rptb'); { Boolean -- true if SRLanguageObject is repeatable }
	kSRRejectable = FOUR_CHAR_CODE('rjbl'); { Boolean -- true if SRLanguageObject is rejectable (Recognition System's kSRRejectedWord }
                                        {       object can be returned in place of SRLanguageObject with this property)   }
	kSRRejectionLevel = FOUR_CHAR_CODE('rjct'); { SRRejectionLevel between 0 and 100 }

{ LM Object Types -- returned as kSRLMObjType property of language model objects }
const
	kSRLanguageModelType = FOUR_CHAR_CODE('lmob'); { SRLanguageModel }
	kSRPathType = FOUR_CHAR_CODE('path'); { SRPath }
	kSRPhraseType = FOUR_CHAR_CODE('phra'); { SRPhrase }
	kSRWordType = FOUR_CHAR_CODE('word'); { SRWord }

{ a normal and reasonable rejection level }
const
	kSRDefaultRejectionLevel = 50;

{******************************************************************************}
{                      NOTES ON USING THE API                                  }
{      All operations (with the exception of SRGetRecognitionSystem) are       }
{      directed toward an object allocated or begot from New, Get and Read     }
{      type calls.                                                             }
{      There is a simple rule in dealing with allocation and disposal:         }
{      *   all toolbox allocations are obtained from a SRRecognitionSystem     }
{      *   if you obtain an object via New or Get, then you own a reference    }
{          to that object and it must be released via SRReleaseObject when     }
{          you no longer need it                                               }
{      *   when you receive a SRRecognitionResult object via AppleEvent or     }
{          callback, it has essentially been created on your behalf and so     }
{          you are responsible for releasing it as above                       }
{      *   when you close a SRRecognitionSystem, all remaining objects which       }
{          were allocated with it will be forcefully released and any          }
{          remaining references to those objects will be invalid.              }
{      This translates into a very simple guideline:                           }
{          If you allocate it or have it allocated for you, you must release   }
{          it.  If you are only peeking at it, then don't release it.          }
{******************************************************************************}
{ Opening and Closing of the SRRecognitionSystem }
{
 *  SROpenRecognitionSystem()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SROpenRecognitionSystem( var system: SRRecognitionSystem; systemID: OSType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRCloseRecognitionSystem()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRCloseRecognitionSystem( system: SRRecognitionSystem ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Accessing Properties of any Speech Object }
{
 *  SRSetProperty()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRSetProperty( srObject: SRSpeechObject; selector: OSType; proprty: {const} univ Ptr; propertyLen: Size ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRGetProperty()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRGetProperty( srObject: SRSpeechObject; selector: OSType; proprty: univ Ptr; var propertyLen: Size ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Any object obtained via New or Get type calls must be released }
{
 *  SRReleaseObject()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRReleaseObject( srObject: SRSpeechObject ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRGetReference()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRGetReference( srObject: SRSpeechObject; var newObjectRef: SRSpeechObject ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ SRRecognizer Instance Functions }
{
 *  SRNewRecognizer()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRNewRecognizer( system: SRRecognitionSystem; var recognizer: SRRecognizer; sourceID: OSType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRStartListening()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRStartListening( recognizer: SRRecognizer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRStopListening()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRStopListening( recognizer: SRRecognizer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRSetLanguageModel()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRSetLanguageModel( recognizer: SRRecognizer; languageModel: SRLanguageModel ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRGetLanguageModel()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRGetLanguageModel( recognizer: SRRecognizer; var languageModel: SRLanguageModel ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRContinueRecognition()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRContinueRecognition( recognizer: SRRecognizer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRCancelRecognition()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRCancelRecognition( recognizer: SRRecognizer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRIdle()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRIdle: OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Language Model Building and Manipulation Functions }
{
 *  SRNewLanguageModel()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRNewLanguageModel( system: SRRecognitionSystem; var model: SRLanguageModel; name: {const} univ Ptr; nameLength: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRNewPath()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRNewPath( system: SRRecognitionSystem; var path: SRPath ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRNewPhrase()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRNewPhrase( system: SRRecognitionSystem; var phrase: SRPhrase; text: {const} univ Ptr; textLength: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRNewWord()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRNewWord( system: SRRecognitionSystem; var word: SRWord; text: {const} univ Ptr; textLength: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Operations on any object of the SRLanguageObject family }
{
 *  SRPutLanguageObjectIntoHandle()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRPutLanguageObjectIntoHandle( languageObject: SRLanguageObject; lobjHandle: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRPutLanguageObjectIntoDataFile()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRPutLanguageObjectIntoDataFile( languageObject: SRLanguageObject; fRefNum: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRNewLanguageObjectFromHandle()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRNewLanguageObjectFromHandle( system: SRRecognitionSystem; var languageObject: SRLanguageObject; lObjHandle: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRNewLanguageObjectFromDataFile()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRNewLanguageObjectFromDataFile( system: SRRecognitionSystem; var languageObject: SRLanguageObject; fRefNum: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SREmptyLanguageObject()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SREmptyLanguageObject( languageObject: SRLanguageObject ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRChangeLanguageObject()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRChangeLanguageObject( languageObject: SRLanguageObject; text: {const} univ Ptr; textLength: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRAddLanguageObject()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRAddLanguageObject( base: SRLanguageObject; addon: SRLanguageObject ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRAddText()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRAddText( base: SRLanguageObject; text: {const} univ Ptr; textLength: SInt32; refCon: SRefCon ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRRemoveLanguageObject()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRRemoveLanguageObject( base: SRLanguageObject; toRemove: SRLanguageObject ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Traversing SRRecognitionResults or SRLanguageObjects }
{
 *  SRCountItems()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRCountItems( container: SRSpeechObject; var count: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRGetIndexedItem()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRGetIndexedItem( container: SRSpeechObject; var item: SRSpeechObject; index: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRSetIndexedItem()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRSetIndexedItem( container: SRSpeechObject; item: SRSpeechObject; index: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRRemoveIndexedItem()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRRemoveIndexedItem( container: SRSpeechObject; index: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Utilizing the System Feedback Window }
{
 *  SRDrawText()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRDrawText( recognizer: SRRecognizer; dispText: {const} univ Ptr; dispLength: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRDrawRecognizedText()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRDrawRecognizedText( recognizer: SRRecognizer; dispText: {const} univ Ptr; dispLength: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRSpeakText()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRSpeakText( recognizer: SRRecognizer; speakText: {const} univ Ptr; speakLength: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRSpeakAndDrawText()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRSpeakAndDrawText( recognizer: SRRecognizer; text: {const} univ Ptr; textLength: SInt32 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRStopSpeech()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRStopSpeech( recognizer: SRRecognizer ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRSpeechBusy()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRSpeechBusy( recognizer: SRRecognizer ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRProcessBegin()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRProcessBegin( recognizer: SRRecognizer; failed: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SRProcessEnd()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SpeechRecognitionLib 1.0 and later
 }
function SRProcessEnd( recognizer: SRRecognizer; failed: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {TARGET_OS_MAC}

end.
