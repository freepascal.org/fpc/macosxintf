{!
	@file		AudioOutputUnit.h
 	@framework	AudioToolbox.framework
 	@copyright	(c) 2000-2015 Apple, Inc. All rights reserved.
	@brief		Additional Audio Unit API for audio input/output units.
}
{  Pascal Translation:  Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, July 2019 }

unit AudioOutputUnit;
interface
uses MacTypes,AUComponent;
{$ALIGN POWER}

{
#if !__LP64__
	#if PRAGMA_STRUCT_ALIGN
		#pragma options align=mac68k
	#elif PRAGMA_STRUCT_PACKPUSH
		#pragma pack(push, 2)
	#elif PRAGMA_STRUCT_PACK
		#pragma pack(2)
	#endif
#endif
}
{$ifc not TARGET_CPU_64}
{$ALIGN MAC68K}
{$endc}

//-----------------------------------------------------------------------------
//	Start/stop methods for output units
//-----------------------------------------------------------------------------
function AudioOutputUnitStart( ci: AudioUnit ): OSStatus;
API_AVAILABLE(macos(10.0), ios(2.0), watchos(2.0), tvos(9.0));

function AudioOutputUnitStop( ci: AudioUnit ): OSStatus;
API_AVAILABLE(macos(10.0), ios(2.0), watchos(2.0), tvos(9.0));

//-----------------------------------------------------------------------------
//	Selectors for component and audio plugin calls
//-----------------------------------------------------------------------------
const
	kAudioOutputUnitRange = $0200; 	// selector range
	kAudioOutputUnitStartSelect = $0201;
	kAudioOutputUnitStopSelect = $0202;

{!
}
type
	AudioOutputUnitStartProc = function( self: univ Ptr ): OSStatus;

{!
}
type
	AudioOutputUnitStopProc = function( self: univ Ptr ): OSStatus;

end.
