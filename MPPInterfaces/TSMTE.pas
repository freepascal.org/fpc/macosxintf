{
     File:       HIToolbox/TSMTE.h
 
     Contains:   Text Services Manager for TextEdit Interfaces. All TextEdit functions as well all functions in
 
     Version:    HIToolbox-624~3
 
     Copyright:  � 1991-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit TSMTE;
interface
uses MacTypes,TextEdit,Dialogs,AppleEvents,TextServices;

{****************************************************************************************************************
    
    All Textedit functions as well all functions in this file are deprecated for Mac OS 10.4. The Multilingual Text 
    Engine (MLTE) API is recommended instead. MLTE support for inline input is built-in so there is no need for an 
    API parallel to TSMTE on Mac OS X. Please see MacTextEditor.h file for a description of the MLTE API.

*****************************************************************************************************************}

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{ signature, interface types}
const
	kTSMTESignature = FOUR_CHAR_CODE('tmTE');
	kTSMTEInterfaceType = kTSMTEDocumentInterfaceType;

{
    In Carbon, since DialogRef is opaque, the TSMDialogRecord is removed.
    Only one kind of TSMTE dialog remains, with extended data managed by TSMTE.
    Use kTSMTESignature for the dialog refCon, and use the accessors below,
    i.e. GetTSMTEDialogTSMTERecHandle, to get at the old TSMDialogRecord info.
}

{ update flag for TSMTERec}
const
	kTSMTEAutoScroll = 1;


{ callback procedure definitions}

type
	TSMTEPreUpdateProcPtr = procedure( textH: TEHandle; refCon: SIGNEDLONG );
	TSMTEPostUpdateProcPtr = procedure( textH: TEHandle; fixLen: SIGNEDLONG; inputAreaStart: SIGNEDLONG; inputAreaEnd: SIGNEDLONG; pinStart: SIGNEDLONG; pinEnd: SIGNEDLONG; refCon: SIGNEDLONG );
{GPC-ONLY-START}
	TSMTEPreUpdateUPP = UniversalProcPtr; // should be TSMTEPreUpdateProcPtr
{GPC-ONLY-ELSE}
	TSMTEPreUpdateUPP = TSMTEPreUpdateProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	TSMTEPostUpdateUPP = UniversalProcPtr; // should be TSMTEPostUpdateProcPtr
{GPC-ONLY-ELSE}
	TSMTEPostUpdateUPP = TSMTEPostUpdateProcPtr;
{GPC-ONLY-FINISH}
{ data types}
type
	TSMTERec = record
		textH: TEHandle;
		preUpdateProc: TSMTEPreUpdateUPP;
		postUpdateProc: TSMTEPostUpdateUPP;
		updateFlag: SIGNEDLONG;
		refCon: SIGNEDLONG;
	end;
	TSMTERecPtr = ^TSMTERec;
type
	TSMTERecHandle = ^TSMTERecPtr;
{
 *  NewTSMTEPreUpdateUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewTSMTEPreUpdateUPP( userRoutine: TSMTEPreUpdateProcPtr ): TSMTEPreUpdateUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewTSMTEPostUpdateUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewTSMTEPostUpdateUPP( userRoutine: TSMTEPostUpdateProcPtr ): TSMTEPostUpdateUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeTSMTEPreUpdateUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeTSMTEPreUpdateUPP( userUPP: TSMTEPreUpdateUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeTSMTEPostUpdateUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeTSMTEPostUpdateUPP( userUPP: TSMTEPostUpdateUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeTSMTEPreUpdateUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeTSMTEPreUpdateUPP( textH: TEHandle; refCon: SIGNEDLONG; userUPP: TSMTEPreUpdateUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeTSMTEPostUpdateUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeTSMTEPostUpdateUPP( textH: TEHandle; fixLen: SIGNEDLONG; inputAreaStart: SIGNEDLONG; inputAreaEnd: SIGNEDLONG; pinStart: SIGNEDLONG; pinEnd: SIGNEDLONG; refCon: SIGNEDLONG; userUPP: TSMTEPostUpdateUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$ifc not TARGET_CPU_64}
{
 *  IsTSMTEDialog()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0.2 and later
 }
function IsTSMTEDialog( dialog: DialogRef ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ Getters }
{
 *  GetTSMTEDialogDocumentID()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0.2 and later
 }
function GetTSMTEDialogDocumentID( dialog: DialogRef ): TSMDocumentID;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  GetTSMTEDialogTSMTERecHandle()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0.2 and later
 }
function GetTSMTEDialogTSMTERecHandle( dialog: DialogRef ): TSMTERecHandle;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{ Setters }
{
 *  SetTSMTEDialogDocumentID()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0.2 and later
 }
procedure SetTSMTEDialogDocumentID( dialog: DialogRef; documentID: TSMDocumentID );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  SetTSMTEDialogTSMTERecHandle()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0.2 and later
 }
procedure SetTSMTEDialogTSMTERecHandle( dialog: DialogRef; tsmteRecHandle_: TSMTERecHandle );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
