{
     File:       PrintCore/PMDefinitionsDeprecated.h
 
     Contains:   Deprecated Carbon Printing Manager Interfaces.
 
     Copyright (c) 1998-2006,2008 Apple Inc. All Rights Reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit PMDefinitionsDeprecated;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ifc not TARGET_CPU_64}

{$ALIGN MAC68K}

{ Printing objects }
type
	PMPrintContext = ^SInt32; { an opaque type }
	PMPrintContextPtr = ^PMPrintContext;
	PMDialog = ^SInt32; { an opaque type }
	PMDialogPtr = ^PMDialog;
const
	kSizeOfTPrint = 120;   { size of old TPrint record }

// Ticket items cannot be locked in 10.5 and later.
const
	kPMLocked = true;

type
	PMColorMode = UInt16;
	PMColorModePtr = ^PMColorMode;
const
	kPMBlackAndWhite = 1;
	kPMGray = 2;
	kPMColor = 3;
	kPMColorModeDuotone = 4;    { 2 channels }
	kPMColorModeSpecialColor = 5;     { to allow for special colors such as metalic, light cyan, etc. }

{ Constants to define the ColorSync Intents. These intents may be used }
{ to set an intent part way through a page or for an entire document. }
type
	PMColorSyncIntent = UInt32;
	PMColorSyncIntentPtr = ^PMColorSyncIntent;
const
	kPMColorIntentUndefined = $0000; { User or application have not declared an intent, use the printer's default. }
	kPMColorIntentAutomatic = $0001; { Automatically match for photos and graphics anywhere on the page. }
	kPMColorIntentPhoto = $0002; { Use Photographic (cmPerceptual) intent for all contents. }
	kPMColorIntentBusiness = $0004; { Use Business Graphics (cmSaturation) intent for all contents. }
	kPMColorIntentRelColor = $0008; { Use Relative Colormetrics (Logo Colors) for the page. }
	kPMColorIntentAbsColor = $0010; { Use absolute colormetric for the page. }
	kPMColorIntentUnused = $FFE0; { Remaining bits unused at this time. }

{ Unused duplex binding directions: }
type
	PMDuplexBinding = UInt16;
	PMDuplexBindingPtr = ^PMDuplexBinding;
const
	kPMDuplexBindingLeftRight = 1;
	kPMDuplexBindingTopDown = 2;


type
	PMTag = UInt32;
	PMTagPtr = ^PMTag;
const
{ common tags }
	kPMCurrentValue = FOUR_CHAR_CODE('curr'); { current setting or value }
	kPMDefaultValue = FOUR_CHAR_CODE('dflt'); { default setting or value }
	kPMMinimumValue = FOUR_CHAR_CODE('minv'); { the minimum setting or value }
	kPMMaximumValue = FOUR_CHAR_CODE('maxv'); { the maximum setting or value }
                                        { profile tags }
	kPMSourceProfile = FOUR_CHAR_CODE('srcp'); { source profile }
                                        { resolution tags }
	kPMMinRange = FOUR_CHAR_CODE('mnrg'); { Min range supported by a printer }
	kPMMaxRange = FOUR_CHAR_CODE('mxrg'); { Max range supported by a printer }
	kPMMinSquareResolution = FOUR_CHAR_CODE('mins'); { Min with X and Y resolution equal }
	kPMMaxSquareResolution = FOUR_CHAR_CODE('maxs'); { Max with X and Y resolution equal }
	kPMDefaultResolution = FOUR_CHAR_CODE('dftr'); { printer default resolution }

{ Useful Constants for PostScript Injection }
const
	kPSPageInjectAllPages = -1;   { specifies to inject on all pages }
	kPSInjectionMaxDictSize = 5;     { maximum size of a dictionary used for PSInjection }

{ PostScript Injection values for kPSInjectionPlacementKey }
type
	PSInjectionPlacement = UInt16;
	PSInjectionPlacementPtr = ^PSInjectionPlacement;
const
	kPSInjectionBeforeSubsection = 1;
	kPSInjectionAfterSubsection = 2;
	kPSInjectionReplaceSubsection = 3;

{ PostScript Injection values for kPSInjectionSectionKey }
type
	PSInjectionSection = SInt32;
	PSInjectionSectionPtr = ^PSInjectionSection;
const
{ Job }
	kInjectionSectJob = 1;    { CoverPage }
	kInjectionSectCoverPage = 2;

{ PostScript Injection values for kPSInjectionSubSectionKey }
type
	PSInjectionSubsection = SInt32;
	PSInjectionSubsectionPtr = ^PSInjectionSubsection;
const
	kInjectionSubPSAdobe = 1;    { %!PS-Adobe           }
	kInjectionSubPSAdobeEPS = 2;    { %!PS-Adobe-3.0 EPSF-3.0    }
	kInjectionSubBoundingBox = 3;    { BoundingBox          }
	kInjectionSubEndComments = 4;    { EndComments          }
	kInjectionSubOrientation = 5;    { Orientation          }
	kInjectionSubPages = 6;    { Pages            }
	kInjectionSubPageOrder = 7;    { PageOrder          }
	kInjectionSubBeginProlog = 8;    { BeginProlog          }
	kInjectionSubEndProlog = 9;    { EndProlog          }
	kInjectionSubBeginSetup = 10;   { BeginSetup          }
	kInjectionSubEndSetup = 11;   { EndSetup             }
	kInjectionSubBeginDefaults = 12;   { BeginDefaults       }
	kInjectionSubEndDefaults = 13;   { EndDefaults          }
	kInjectionSubDocFonts = 14;   { DocumentFonts       }
	kInjectionSubDocNeededFonts = 15;   { DocumentNeededFonts       }
	kInjectionSubDocSuppliedFonts = 16;   { DocumentSuppliedFonts  }
	kInjectionSubDocNeededRes = 17;   { DocumentNeededResources    }
	kInjectionSubDocSuppliedRes = 18;   { DocumentSuppliedResources}
	kInjectionSubDocCustomColors = 19;   { DocumentCustomColors     }
	kInjectionSubDocProcessColors = 20;   { DocumentProcessColors  }
	kInjectionSubPlateColor = 21;   { PlateColor          }
	kInjectionSubPageTrailer = 22;   { PageTrailer           }
	kInjectionSubTrailer = 23;   { Trailer              }
	kInjectionSubEOF = 24;   { EOF                 }
	kInjectionSubBeginFont = 25;   { BeginFont          }
	kInjectionSubEndFont = 26;   { EndFont              }
	kInjectionSubBeginResource = 27;   { BeginResource       }
	kInjectionSubEndResource = 28;   { EndResource          }
	kInjectionSubPage = 29;   { Page               }
	kInjectionSubBeginPageSetup = 30;   { BeginPageSetup        }
	kInjectionSubEndPageSetup = 31;    { EndPageSetup          }

{ Document format strings }
const kPMDocumentFormatPICT = CFSTR( 'application/vnd.apple.printing-pict' );
const kPMDocumentFormatPICTPS = CFSTR( 'application/vnd.apple.printing-pict-ps' );
const kPMDocumentFormatPICTPSwPSNormalizer = CFSTR( 'application/vnd.apple.printing-pict-ps-viapsnormalizer' );
{ Data format strings }
const kPMDataFormatPDF = CFSTR( 'application/pdf' );
const kPMDataFormatPS = CFSTR( 'application/postscript' );
const kPMDataFormatPICT = CFSTR( 'application/vnd.apple.printing-pict' );
const kPMDataFormatPICTwPS = CFSTR( 'application/vnd.apple.printing-pict-ps' );

{ Graphic context strings }
const kPMGraphicsContextQuickdraw = CFSTR( 'com.apple.graphicscontext.quickdraw' );

{ PostScript Injection Dictionary Keys }
const kPSInjectionSectionKey = CFSTR( 'section' );
const kPSInjectionSubSectionKey = CFSTR( 'subsection' );
const kPSInjectionPageKey = CFSTR( 'page' );
const kPSInjectionPlacementKey = CFSTR( 'place' );
const kPSInjectionPostScriptKey = CFSTR( 'psdata' );
{ PDF Workflow Keys }
const kPDFWorkflowForlderURLKey = CFSTR( 'folderURL' );

{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
