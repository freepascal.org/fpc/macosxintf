{	CFDate.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFDate;
interface
uses MacTypes,CFBase;
{$ALIGN POWER}


type
	CFTimeInterval = Float64;
	CFAbsoluteTime = CFTimeInterval;
	CFAbsoluteTimePtr = ^CFAbsoluteTime;
{ absolute time is the time interval since the reference date }
{ the reference date (epoch) is 00:00:00 1 January 2001. }

function CFAbsoluteTimeGetCurrent: CFAbsoluteTime;

const kCFAbsoluteTimeIntervalSince1970: CFTimeInterval;
const kCFAbsoluteTimeIntervalSince1904: CFTimeInterval;

type
	CFDateRef = ^__CFDate; { an opaque type }
	__CFDate = record end;
	CFDateRefPtr = ^CFDateRef;

function CFDateGetTypeID: CFTypeID;

function CFDateCreate( allocator: CFAllocatorRef; at: CFAbsoluteTime ): CFDateRef;

function CFDateGetAbsoluteTime( theDate: CFDateRef ): CFAbsoluteTime;

function CFDateGetTimeIntervalSinceDate( theDate: CFDateRef; otherDate: CFDateRef ): CFTimeInterval;

function CFDateCompare( theDate: CFDateRef; otherDate: CFDateRef; context: univ Ptr ): CFComparisonResult;

type
	CFTimeZoneRef = ^__CFTimeZone; { an opaque type }
	__CFTimeZone = record end;
	CFTimeZoneRefPtr = ^CFTimeZoneRef;

type
	CFGregorianDate = record
		year: SInt32;
		month: SInt8;
		day: SInt8;
		hour: SInt8;
		minute: SInt8;
		second: Float64;
	end;
	CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");
	CFGregorianDatePtr = ^CFGregorianDate;

type
	CFGregorianUnits = record
		years: SInt32;
		months: SInt32;
		days: SInt32;
		hours: SInt32;
		minutes: SInt32;
		seconds: Float64;
	end;
	CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");
	CFGregorianUnitsPtr = ^CFGregorianUnits;

type
	CFGregorianUnitFlags = CFOptionFlags;
const
	kCFGregorianUnitsYears = 1 shl 0;
	CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");
	kCFGregorianUnitsMonths = 1 shl 1;
	CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");
	kCFGregorianUnitsDays = 1 shl 2;
	CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");
	kCFGregorianUnitsHours = 1 shl 3;
	CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");
	kCFGregorianUnitsMinutes = 1 shl 4;
	CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");
	kCFGregorianUnitsSeconds = 1 shl 5;
	CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");
	kCFGregorianAllUnits = $00FFFFFF;
	CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");

function CFGregorianDateIsValid( gdate: CFGregorianDate; unitFlags: CFOptionFlags ): Boolean;
CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");

function CFGregorianDateGetAbsoluteTime( gdate: CFGregorianDate; tz: CFTimeZoneRef ): CFAbsoluteTime; 
CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");

function CFAbsoluteTimeGetGregorianDate( at: CFAbsoluteTime; tz: CFTimeZoneRef ): CFGregorianDate;
CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");

function CFAbsoluteTimeAddGregorianUnits( at: CFAbsoluteTime; tz: CFTimeZoneRef; units: CFGregorianUnits ): CFAbsoluteTime;
CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");

function CFAbsoluteTimeGetDifferenceAsGregorianUnits( at1: CFAbsoluteTime; at2: CFAbsoluteTime; tz: CFTimeZoneRef; unitFlags: CFOptionFlags ): CFGregorianUnits;
CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");

function CFAbsoluteTimeGetDayOfWeek( at: CFAbsoluteTime; tz: CFTimeZoneRef ): SInt32;
CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");

function CFAbsoluteTimeGetDayOfYear( at: CFAbsoluteTime; tz: CFTimeZoneRef ): SInt32;
CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");

function CFAbsoluteTimeGetWeekOfYear( at: CFAbsoluteTime; tz: CFTimeZoneRef ): SInt32;
CF_CALENDAR_DEPRECATED(10_4, 10_9, 2_0, 7_0, "Use CFCalendar or NSCalendar API instead");


end.
