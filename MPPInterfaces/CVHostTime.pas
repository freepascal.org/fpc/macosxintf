{
 *  CVHostTime.h
 *  CoreVideo
 *
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }

{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2008 }
{  Pascal Translation Update:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }

unit CVHostTime;
interface
uses MacTypes;
{$ALIGN POWER}


 {! @header CVHostTime.h
	@copyright 2004 Apple Computer, Inc. All rights reserved.
	@availability Mac OS X 10.4 or later
    @discussion Utility functions for retrieving and working with the host time.
}


{!
    @function   CVGetCurrentHostTime
    @abstract   Retrieve the current value of the host time base.
    @discussion On Mac OS X, the host time base for CoreVideo and CoreAudio are identical, and the values returned from either API
                may be used interchangeably.
    @result     The current host time.
}
function CVGetCurrentHostTime: UInt64;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVGetHostClockFrequency
    @abstract   Retrieve the frequency of the host time base.
    @discussion On Mac OS X, the host time base for CoreVideo and CoreAudio are identical, and the values returned from either API
                may be used interchangeably.
    @result     The current host frequency.
}
function CVGetHostClockFrequency: Float64;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVGetHostClockMinimumTimeDelta
    @abstract   Retrieve the smallest possible increment in the host time base.
    @result     The smallest valid increment in the host time base.
}
function CVGetHostClockMinimumTimeDelta: UInt32;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);


end.
