{	CFNumberFormatter.h
	Copyright (c) 2003-2013, Apple Inc. All rights reserved.
}
unit CFNumberFormatter;
interface
uses MacTypes,CFBase,CFNumber,CFLocale;
{$ALIGN POWER}


{#if MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_3}


type
	CFNumberFormatterRef = ^__CFNumberFormatter; { an opaque type }
	__CFNumberFormatter = record end;

// CFNumberFormatters are not thread-safe.  Do not use one from multiple threads!

function CFNumberFormatterGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

// number format styles
type
	CFNumberFormatterStyle = CFIndex;
const
	kCFNumberFormatterNoStyle = 0;
	kCFNumberFormatterDecimalStyle = 1;
	kCFNumberFormatterCurrencyStyle = 2;
	kCFNumberFormatterPercentStyle = 3;
	kCFNumberFormatterScientificStyle = 4;
	kCFNumberFormatterSpellOutStyle = 5;


function CFNumberFormatterCreate( allocator: CFAllocatorRef; locale: CFLocaleRef; style: CFNumberFormatterStyle ): CFNumberFormatterRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Returns a CFNumberFormatter, localized to the given locale, which
	// will format numbers to the given style.

function CFNumberFormatterGetLocale( formatter: CFNumberFormatterRef ): CFLocaleRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFNumberFormatterGetStyle( formatter: CFNumberFormatterRef ): CFNumberFormatterStyle;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Get the properties with which the number formatter was created.

function CFNumberFormatterGetFormat( formatter: CFNumberFormatterRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

procedure CFNumberFormatterSetFormat( formatter: CFNumberFormatterRef; formatString: CFStringRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Set the format description string of the number formatter.  This
	// overrides the style settings.  The format of the format string
	// is as defined by the ICU library, and is similar to that found
	// in Microsoft Excel and NSNumberFormatter.
	// The number formatter starts with a default format string defined
	// by the style argument with which it was created.


function CFNumberFormatterCreateStringWithNumber( allocator: CFAllocatorRef; formatter: CFNumberFormatterRef; number: CFNumberRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFNumberFormatterCreateStringWithValue( allocator: CFAllocatorRef; formatter: CFNumberFormatterRef; numberType: CFNumberType; valuePtr: {const} univ Ptr ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Create a string representation of the given number or value
	// using the current state of the number formatter.


type
	CFNumberFormatterOptionFlags = CFOptionFlags;
const
	kCFNumberFormatterParseIntegersOnly = 1;	{ only parse integers }

function CFNumberFormatterCreateNumberFromString( allocator: CFAllocatorRef; formatter: CFNumberFormatterRef; strng: CFStringRef; rangep: CFRangePtr; options: CFOptionFlags ): CFNumberRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFNumberFormatterGetValueFromString( formatter: CFNumberFormatterRef; strng: CFStringRef; rangep: CFRangePtr; numberType: CFNumberType; valuePtr: univ Ptr ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Parse a string representation of a number using the current state
	// of the number formatter.  The range parameter specifies the range
	// of the string in which the parsing should occur in input, and on
	// output indicates the extent that was used; this parameter can
	// be NULL, in which case the whole string may be used.  The
	// return value indicates whether some number was computed and
	// (if valuePtr is not NULL) stored at the location specified by
	// valuePtr.  The numberType indicates the type of value pointed
	// to by valuePtr.


procedure CFNumberFormatterSetProperty( formatter: CFNumberFormatterRef; key: CFStringRef; value: CFTypeRef );
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CFNumberFormatterCopyProperty( formatter: CFNumberFormatterRef; key: CFStringRef ): CFTypeRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Set and get various properties of the number formatter, the set of
	// which may be expanded in the future.

const kCFNumberFormatterCurrencyCode: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterDecimalSeparator: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFString
const kCFNumberFormatterCurrencyDecimalSeparator: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER; // CFString
const kCFNumberFormatterAlwaysShowDecimalSeparator: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER; // CFBoolean
const kCFNumberFormatterGroupingSeparator: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFString
const kCFNumberFormatterUseGroupingSeparator: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFBoolean
const kCFNumberFormatterPercentSymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterZeroSymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterNaNSymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterInfinitySymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterMinusSign: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterPlusSign: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterCurrencySymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterExponentSymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterMinIntegerDigits: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFNumber
const kCFNumberFormatterMaxIntegerDigits: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFNumber
const kCFNumberFormatterMinFractionDigits: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFNumber
const kCFNumberFormatterMaxFractionDigits: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFNumber
const kCFNumberFormatterGroupingSize: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFNumber
const kCFNumberFormatterSecondaryGroupingSize: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFNumber
const kCFNumberFormatterRoundingMode: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFNumber
const kCFNumberFormatterRoundingIncrement: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFNumber
const kCFNumberFormatterFormatWidth: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFNumber
const kCFNumberFormatterPaddingPosition: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFNumber
const kCFNumberFormatterPaddingCharacter: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;	// CFString
const kCFNumberFormatterDefaultFormat: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;		// CFString
const kCFNumberFormatterMultiplier: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;		// CFNumber
const kCFNumberFormatterPositivePrefix: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;		// CFString
const kCFNumberFormatterPositiveSuffix: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;		// CFString
const kCFNumberFormatterNegativePrefix: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;		// CFString
const kCFNumberFormatterNegativeSuffix: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;		// CFString
const kCFNumberFormatterPerMillSymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;		// CFString
const kCFNumberFormatterInternationalCurrencySymbol: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER; // CFString
const kCFNumberFormatterCurrencyGroupingSeparator: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0); // CFString
const kCFNumberFormatterIsLenient: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0);		// CFBoolean
const kCFNumberFormatterUseSignificantDigits: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0);	// CFBoolean
const kCFNumberFormatterMinSignificantDigits: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0);	// CFNumber
const kCFNumberFormatterMaxSignificantDigits: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0);	// CFNumber

type
	CFNumberFormatterRoundingMode = CFIndex;
const
	kCFNumberFormatterRoundCeiling = 0;
	kCFNumberFormatterRoundFloor = 1;
	kCFNumberFormatterRoundDown = 2;
	kCFNumberFormatterRoundUp = 3;
	kCFNumberFormatterRoundHalfEven = 4;
	kCFNumberFormatterRoundHalfDown = 5;
	kCFNumberFormatterRoundHalfUp = 6;

type
	CFNumberFormatterPadPosition = CFIndex;
const
	kCFNumberFormatterPadBeforePrefix = 0;
	kCFNumberFormatterPadAfterPrefix = 1;
	kCFNumberFormatterPadBeforeSuffix = 2;
	kCFNumberFormatterPadAfterSuffix = 3;


function CFNumberFormatterGetDecimalInfoForCurrencyCode( currencyCode: CFStringRef; defaultFractionDigits: SInt32Ptr; roundingIncrement: Float64Ptr ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
	// Returns the number of fraction digits that should be displayed, and
	// the rounding increment (or 0.0 if no rounding is done by the currency)
	// for the given currency.  Returns false if the currency code is unknown
	// or the information is not available.
	// Not localized because these are properties of the currency.


{#endif}


end.
