{ CoreGraphics - CGPDFScanner.h
   Copyright (c) 2004-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFScanner;
interface
uses MacTypes,CGBase,CGPDFObject;
{$ALIGN POWER}


// CGPDFScannerRef defined in CGBase


{ Create a scanner. }

function CGPDFScannerCreate( cs: CGPDFContentStreamRef; table: CGPDFOperatorTableRef; info: univ Ptr ): CGPDFScannerRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Retain `scanner'. }

function CGPDFScannerRetain( scanner: CGPDFScannerRef ): CGPDFScannerRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Release `scanner'. }

procedure CGPDFScannerRelease( scanner: CGPDFScannerRef );
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Scan the content stream of `scanner'. Returns true if the entire stream
   was scanned successfully; false if scanning failed for some reason (for
   example, if the stream's data is corrupted). }

function CGPDFScannerScan( scanner: CGPDFScannerRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the content stream associated with `scanner'. }

function CGPDFScannerGetContentStream( scanner: CGPDFScannerRef ): CGPDFContentStreamRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Pop an object from the stack of `scanner' and return it in `value'. }

function CGPDFScannerPopObject( scanner: CGPDFScannerRef; var value: CGPDFObjectRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Pop an object from the stack of `scanner' and, if it's a boolean, return
   it in `value'. Return false if the top of the stack isn't a boolean. }

function CGPDFScannerPopBoolean( scanner: CGPDFScannerRef; var value: CGPDFBoolean ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Pop an object from the stack of `scanner' and, if it's an integer, return
   it in `value'. Return false if the top of the stack isn't an integer. }

function CGPDFScannerPopInteger( scanner: CGPDFScannerRef; var value: CGPDFInteger ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Pop an object from the stack of `scanner' and, if it's a number, return
   it in `value'. Return false if the top of the stack isn't a number. }

function CGPDFScannerPopNumber( scanner: CGPDFScannerRef; var value: CGPDFReal ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Pop an object from the stack of `scanner' and, if it's a name, return it
   in `value'. Return false if the top of the stack isn't a name. }

function CGPDFScannerPopName( scanner: CGPDFScannerRef; var value: ConstCStringPtr ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Pop an object from the stack of `scanner' and, if it's a string, return
   it in `value'. Return false if the top of the stack isn't a string. }

function CGPDFScannerPopString( scanner: CGPDFScannerRef; var value: CGPDFStringRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Pop an object from the stack of `scanner' and, if it's an array, return
   it in `value'. Return false if the top of the stack isn't an array. }

function CGPDFScannerPopArray( scanner: CGPDFScannerRef; var value: CGPDFArrayRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Pop an object from the stack of `scanner' and, if it's a dictionary,
   return it in `value'. Return false if the top of the stack isn't a
   dictionary. }

function CGPDFScannerPopDictionary( scanner: CGPDFScannerRef; var value: CGPDFDictionaryRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Pop an object from the stack of `scanner' and, if it's a stream, return
   it in `value'. Return false if the top of the stack isn't a stream. }

function CGPDFScannerPopStream( scanner: CGPDFScannerRef; var value: CGPDFStreamRef ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);


end.
