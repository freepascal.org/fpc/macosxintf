{
 *  CVImageBuffer.h
 *  CoreVideo
 *
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2008 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }
unit CVImageBuffer;
interface
uses CFBase, CFDictionary, CGColorSpace, CGGeometry,CVBuffer;

{$ALIGN POWER}

 
 {! @header CVImageBuffer.h
	@copyright 2004 Apple Computer, Inc. All rights reserved.
	@availability Mac OS X 10.4 or later
    @discussion CVImageBufferRef types are abstract and define various attachments and convenience
		calls for retreiving image related bits of data.
		   
}

//#pragma mark CVImageBufferRef attachment keys

const kCVImageBufferCGColorSpaceKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CGColorSpaceRef

const kCVImageBufferCleanApertureKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CFDictionary containing the following four keys
const kCVImageBufferCleanApertureWidthKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);			// CFNumber
const kCVImageBufferCleanApertureHeightKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);			// CFNumber
const kCVImageBufferCleanApertureHorizontalOffsetKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		// CFNumber, horizontal offset from center of image buffer
const kCVImageBufferCleanApertureVerticalOffsetKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		// CFNumber, vertical offset from center of image buffer
const kCVImageBufferPreferredCleanApertureKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);			// CFDictionary containing same keys as kCVImageBufferCleanApertureKey

const kCVImageBufferFieldCountKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CFNumber
const kCVImageBufferFieldDetailKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CFString with one of the following four values
const kCVImageBufferFieldDetailTemporalTopFirst: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		// CFString
const kCVImageBufferFieldDetailTemporalBottomFirst: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		// CFString
const kCVImageBufferFieldDetailSpatialFirstLineEarly: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		// CFString
const kCVImageBufferFieldDetailSpatialFirstLineLate: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		// CFString

const kCVImageBufferPixelAspectRatioKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CFDictionary with the following two keys
const kCVImageBufferPixelAspectRatioHorizontalSpacingKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);	// CFNumber
const kCVImageBufferPixelAspectRatioVerticalSpacingKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);	// CFNumber

const kCVImageBufferDisplayDimensionsKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CFDictionary with the following two keys
const kCVImageBufferDisplayWidthKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CFNumber
const kCVImageBufferDisplayHeightKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CFNumber

const kCVImageBufferGammaLevelKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CFNumber describing the gamma level, used in absence of (or ignorance of) kCVImageBufferTransferFunctionKey

const kCVImageBufferICCProfileKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0);				// CFData representation of the ICC profile

const kCVImageBufferYCbCrMatrixKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);				// CFString describing the color matrix for YCbCr->RGB. This key can be one of the following values:
const kCVImageBufferYCbCrMatrix_ITU_R_709_2: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);			// CFString
const kCVImageBufferYCbCrMatrix_ITU_R_601_4: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);			// CFString
const kCVImageBufferYCbCrMatrix_SMPTE_240M_1995: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		// CFString

const kCVImageBufferColorPrimariesKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);				// CFString describing the color primaries. This key can be one of the following values
const kCVImageBufferColorPrimaries_ITU_R_709_2: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);
const kCVImageBufferColorPrimaries_EBU_3213: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);
const kCVImageBufferColorPrimaries_SMPTE_C: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);
const kCVImageBufferColorPrimaries_P22: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_8,__IPHONE_6_0);

const kCVImageBufferTransferFunctionKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);				// CFString describing the transfer function. This key can be one of the following values
const kCVImageBufferTransferFunction_ITU_R_709_2: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);
const kCVImageBufferTransferFunction_SMPTE_240M_1995: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0);
const kCVImageBufferTransferFunction_UseGamma: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0);
const kCVImageBufferTransferFunction_EBU_3213: CFStringRef;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_5,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);			// Should not be used.
const kCVImageBufferTransferFunction_SMPTE_C: CFStringRef;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_5,__MAC_10_6,__IPHONE_NA,__IPHONE_NA);			// Should not be used.

{ Chroma siting information. For progressive images, only the TopField value is used. }
const kCVImageBufferChromaLocationTopFieldKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);			// CFString with one of the following CFString values
const kCVImageBufferChromaLocationBottomFieldKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);		// CFString with one of the following CFString values
const kCVImageBufferChromaLocation_Left: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);			    // Chroma sample is horizontally co-sited with the left column of luma samples, but centered vertically.
const kCVImageBufferChromaLocation_Center: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);			    // Chroma sample is fully centered
const kCVImageBufferChromaLocation_TopLeft: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);			    // Chroma sample is co-sited with the top-left luma sample.
const kCVImageBufferChromaLocation_Top: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);			    // Chroma sample is horizontally centered, but co-sited with the top row of luma samples.
const kCVImageBufferChromaLocation_BottomLeft: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);		    // Chroma sample is co-sited with the bottom-left luma sample.
const kCVImageBufferChromaLocation_Bottom: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);			    // Chroma sample is horizontally centered, but co-sited with the bottom row of luma samples.
const kCVImageBufferChromaLocation_DV420: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);			    // Cr and Cb samples are alternately co-sited with the left luma samples of the same field.

// These describe the format of the original subsampled data before conversion to 422/2vuy.   In order to use
// these tags, the data must have been converted to 4:2:2 via simple pixel replication.
const kCVImageBufferChromaSubsamplingKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);		// CFString/CFNumber with one of the following values
const kCVImageBufferChromaSubsampling_420: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);
const kCVImageBufferChromaSubsampling_422: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);
const kCVImageBufferChromaSubsampling_411: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_5,__IPHONE_4_0);

//#pragma mark CVImageBufferRef

{!
    @typedef	CVImageBufferRef
    @abstract   Base type for all CoreVideo image buffers

}
type
	CVImageBufferRef = CVBufferRef;

{!
    @function   CVImageBufferGetEncodedSize
    @abstract   Returns the full encoded dimensions of a CVImageBuffer.  For example, for an NTSC DV frame this would be 720x480
    @discussion Note: When creating a CIImage from a CVImageBuffer, this is the call you should use for retrieving the image size.
    @param      imageBuffer A CVImageBuffer that you wish to retrieve the encoded size from.
    @result     A CGSize returning the full encoded size of the buffer
		Returns zero size if called with a non-CVImageBufferRef type or NULL.
}
function CVImageBufferGetEncodedSize( imageBuffer: CVImageBufferRef ): CGSize;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVImageBufferGetDisplaySize
    @abstract   Returns the nominal output display size (in square pixels) of a CVImageBuffer.  
                For example, for an NTSC DV frame this would be 640x480
    @param      imageBuffer A CVImageBuffer that you wish to retrieve the display size from.
    @result     A CGSize returning the nominal display size of the buffer
		Returns zero size if called with a non-CVImageBufferRef type or NULL.
}
function CVImageBufferGetDisplaySize( imageBuffer: CVImageBufferRef ): CGSize;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVImageBufferGetCleanRect
    @abstract   Returns the source rectangle of a CVImageBuffer that represents the clean aperture
		of the buffer in encoded pixels.    For example, an NTSC DV frame would return a CGRect with an
		origin of 8,0 and a size of 704,480.		
		Note that the origin of this rect always the lower left	corner.   This is the same coordinate system as
		used by CoreImage.
    @param      imageBuffer A CVImageBuffer that you wish to retrieve the display size from.
    @result     A CGSize returning the nominal display size of the buffer
		Returns zero rect if called with a non-CVImageBufferRef type or NULL.
}
function CVImageBufferGetCleanRect( imageBuffer: CVImageBufferRef ): CGRect;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVImageBufferIsFlipped
    @abstract   Returns whether the image is flipped vertically or not.
    @param      CVImageBuffer target
    @result     True if 0,0 in the texture is upper left, false if 0,0 is lower left.
}
function CVImageBufferIsFlipped( imageBuffer: CVImageBufferRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);


{$ifc TARGET_OS_MAC}
{!
    @function   CVImageBufferGetColorSpace
    @abstract   Returns the color space of a CVImageBuffer.
    @param      imageBuffer A CVImageBuffer that you wish to retrieve the color space from.
    @result     A CGColorSpaceRef representing the color space of the buffer.
		Returns NULL if called with a non-CVImageBufferRef type or NULL.
}
function CVImageBufferGetColorSpace( imageBuffer: CVImageBufferRef ): CGColorSpaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
   @function   CVImageBufferCreateColorSpaceFromAttachments
   @abstract   Attempts to synthesize a CGColorSpace from an image buffer's attachments.
   @param      attachments A CFDictionary of attachments for an image buffer, obtained using CVBufferGetAttachments().
   @result     A CGColorSpaceRef representing the color space of the buffer.
		Returns NULL if the attachments dictionary does not contain the information required to synthesize a CGColorSpace.
   @discussion
	To generate a CGColorSpace, the attachments dictionary should include values for either:
		1. kCVImageBufferICCProfile
		2. kCVImageBufferColorPrimariesKey, kCVImageBufferTransferFunctionKey, and kCVImageBufferYCbCrMatrixKey (and possibly kCVImageBufferGammaLevelKey)
	The client is responsible for releasing the CGColorSpaceRef when it is done with it (CGColorSpaceRelease() or CFRelease())
		
}
function CVImageBufferCreateColorSpaceFromAttachments( attachments: CFDictionaryRef ): CGColorSpaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_8,__IPHONE_NA);

{$endc} {TARGET_OS_MAC}


end.
