{
 *  AXActionConstants.h
 *  HIServices
 *
 *  Created by John Louch on Wed Feb 25 2004.
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }

{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 (no changes in 10.6) }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }

unit AXActionConstants;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
	TBD: Explain general philosophy on whether to expose an action or not.
	Our initial philopsophy is to not to have an action where there is a menu item
	or button that does the same thing.
}

// standard actions
const kAXPressAction = CFSTR( 'AXPress' );
const kAXIncrementAction = CFSTR( 'AXIncrement' );
const kAXDecrementAction = CFSTR( 'AXDecrement' );
const kAXConfirmAction = CFSTR( 'AXConfirm' );
const kAXCancelAction = CFSTR( 'AXCancel' );

// new actions
const kAXRaiseAction = CFSTR( 'AXRaise' );
const kAXShowMenuAction = CFSTR( 'AXShowMenu' );

// obsolete actions will be removed soon
const kAXPickAction = CFSTR( 'AXPick' );

{$endc} {TARGET_OS_MAC}

end.
