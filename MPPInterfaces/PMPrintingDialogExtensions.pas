{
     File:       PMPrintingDialogExtensions.h
 
     Contains:   Mac OS X Printing Manager Print Dialog Extensions' Interfaces.
 
     Version:    Technology: Mac OS X
                 Release:    1.0
 
     Copyright  (c) 1998-2008 by Apple Inc. All Rights Reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit PMPrintingDialogExtensions;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Constants
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}

{
Define the Kind IDs for Universal and Standard Printing Dialog Extensions.
}
{ Implemented Universal }
const kPMPageAttributesKindID = CFSTR( 'com.apple.print.pde.PageAttributesKind' );
const kPMCopiesAndPagesPDEKindID = CFSTR( 'com.apple.print.pde.CopiesAndPagesKind' );
const kPMLayoutPDEKindID = CFSTR( 'com.apple.print.pde.LayoutUserOptionKind' );
const kPMOutputOptionsPDEKindID = CFSTR( 'com.apple.print.pde.OutputOptionsKind' );
const kPMDuplexPDEKindID = CFSTR( 'com.apple.print.pde.DuplexKind' );
const kPMCustomPaperSizePDEKindID = CFSTR( 'com.apple.print.pde.CustomPaperSizeKind' );
const kPMCoverPagePDEKindID = CFSTR( 'com.apple.print.pde.CoverPageKind' );
const kPMColorMatchingPDEKindID = CFSTR( 'com.apple.print.pde.ColorMatchingKind' );
const kPMSchedulerPDEKindID = CFSTR( 'com.apple.print.pde.SchedulerKind' );
const kPMImagingOptionsPDEKindID = CFSTR( 'com.apple.print.pde.ImagingOptionsKind' );
const kPMFaxCoverPagePDEKindID = CFSTR( 'com.apple.print.pde.FaxCoverPageKind' );
const kPMFaxModemPDEKindID = CFSTR( 'com.apple.print.pde.FaxModemKind' );
const kPMFaxAddressesPDEKindID = CFSTR( 'com.apple.print.pde.FaxAddressesKind' );
const kPMPaperHandlingPDEKindID = CFSTR( 'com.apple.print.pde.PaperHandlingKind' );
const kPMPDFEffectsPDEKindID = CFSTR( 'com.apple.print.pde.PDFEffects' );
const kPMSummaryPanelKindID = CFSTR( 'com.apple.print.pde.SummaryKind' );
const kPMUniPrinterPDEKindID = CFSTR( 'com.apple.print.pde.UniPrinterKind' );
{ Unimplemented Universal }
const kPMPaperSourcePDEKindID = CFSTR( 'com.apple.print.pde.PaperSourceKind' );
const kPMPriorityPDEKindID = CFSTR( 'com.apple.print.pde.PriorityKind' );
const kPMRotationScalingPDEKindID = CFSTR( 'com.apple.print.pde.RotationScalingKind' );
const kPMUnsupportedPDEKindID = CFSTR( 'com.apple.print.pde.UnsupportedPDEKind' );
{ Implemented Standard }
const kPMErrorHandlingPDEKindID = CFSTR( 'com.apple.print.pde.ErrorHandlingKind' );
const kPMPaperFeedPDEKindID = CFSTR( 'com.apple.print.pde.PaperFeedKind' );
const kPMPrinterFeaturesPDEKindID = CFSTR( 'com.apple.print.pde.PrinterFeaturesKind' );
const kPMInkPDEKindID = CFSTR( 'com.apple.print.pde.InkKind' );
{ Unimplemented Standard }
const kPMColorPDEKindID = CFSTR( 'com.apple.print.pde.ColorKind' );
const kPMQualityMediaPDEKindID = CFSTR( 'com.apple.print.pde.QualityMediaPDEKind' );
const kPMMediaQualityPDEKindID = CFSTR( 'com.apple.print.pde.MediaQualityPDEKind' );

{ Key to represent information about display order for Cocoa summary info }
const SUMMARY_DISPLAY_ORDER = CFSTR( 'Summary Display Order' );

{  Boolean key in the Bundle's Info.plist file that sepecifies whether the PDEPanels created by the Bundle are Sandbox compatible }
const kPMSandboxCompatiblePDEs = CFSTR( 'PMSandboxCompatiblePDEs' );

{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Type Definitions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
{
Basic types...
}

{ Type and Interface IDs. }
const kDialogExtensionIntfIDStr = CFSTR( 'A996FD7E-B738-11D3-8519-0050E4603277' );
const kGeneralPageSetupDialogTypeIDStr = CFSTR( '6E6ED964-B738-11D3-952F-0050E4603277' );
const kGeneralPrintDialogTypeIDStr = CFSTR( 'C1BF838E-B72A-11D3-9644-0050E4603277' );
const kAppPageSetupDialogTypeIDStr = CFSTR( 'B9A0DA98-E57F-11D3-9E83-0050E4603277' );
const kAppPrintDialogTypeIDStr = CFSTR( 'BCB07250-E57F-11D3-8CA6-0050E4603277' );
const kAppPrintThumbnailTypeIDStr = CFSTR( '9320FE03-B5D5-11D5-84D1-003065D6135E' );
const kPrinterModuleTypeIDStr = CFSTR( 'BDB091F4-E57F-11D3-B5CC-0050E4603277' );

	
{$endc} {TARGET_OS_MAC}

end.
