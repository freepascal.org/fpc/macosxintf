{	CFMessagePort.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFMessagePort;
interface
uses MacTypes,CFBase,CFString,CFRunLoop,CFData,CFDate;
{$ALIGN POWER}


type
	CFMessagePortRef = ^__CFMessagePort; { an opaque type }
	__CFMessagePort = record end;

const
	kCFMessagePortSuccess = 0;
	kCFMessagePortSendTimeout = -1;
	kCFMessagePortReceiveTimeout = -2;
	kCFMessagePortIsInvalid = -3;
	kCFMessagePortTransportError = -4;
	kCFMessagePortBecameInvalidError = -5;

type
	CFMessagePortContext = record
		version: CFIndex;
		info: UnivPtr;
		retain: function( info: {const} univ Ptr ): UnivPtr;
		release: procedure( info: {const} univ Ptr );
		copyDescription: function( info: {const} univ Ptr ): CFStringRef;
	end;
	CFMessagePortContextPtr = ^CFMessagePortContext;

type
	CFMessagePortCallBack = function( local: CFMessagePortRef; msgid: SInt32; data: CFDataRef; info: univ Ptr ): CFDataRef;
{ If callout wants to keep a hold of the data past the return of the callout, it must COPY the data. This includes the case where the data is given to some routine which _might_ keep a hold of it; System will release returned CFData. }
type
	CFMessagePortInvalidationCallBack = procedure( ms: CFMessagePortRef; info: univ Ptr );

function CFMessagePortGetTypeID: CFTypeID;

function CFMessagePortCreateLocal( allocator: CFAllocatorRef; name: CFStringRef; callout: CFMessagePortCallBack; var context: CFMessagePortContext; var shouldFreeInfo: Boolean ): CFMessagePortRef;
function CFMessagePortCreateRemote( allocator: CFAllocatorRef; name: CFStringRef ): CFMessagePortRef;

function CFMessagePortIsRemote( ms: CFMessagePortRef ): Boolean;
function CFMessagePortGetName( ms: CFMessagePortRef ): CFStringRef;
function CFMessagePortSetName( ms: CFMessagePortRef; newName: CFStringRef ): Boolean;
procedure CFMessagePortGetContext( ms: CFMessagePortRef; var context: CFMessagePortContext );
procedure CFMessagePortInvalidate( ms: CFMessagePortRef );
function CFMessagePortIsValid( ms: CFMessagePortRef ): Boolean;
{GPC-ONLY-START}
// GPC error: function result must not be a procedural type
function CFMessagePortGetInvalidationCallBack( ms: CFMessagePortRef ): UniversalProcPtr;
{GPC-ONLY-ELSE}
function CFMessagePortGetInvalidationCallBack( ms: CFMessagePortRef ): CFMessagePortInvalidationCallBack;
{GPC-ONLY-FINISH}
procedure CFMessagePortSetInvalidationCallBack( ms: CFMessagePortRef; callout: CFMessagePortInvalidationCallBack );

{ NULL replyMode argument means no return value expected, dont wait for it }
function CFMessagePortSendRequest( remote: CFMessagePortRef; msgid: SInt32; data: CFDataRef; sendTimeout: CFTimeInterval; rcvTimeout: CFTimeInterval; replyMode: CFStringRef; returnData: CFDataRefPtr ): SInt32;

function CFMessagePortCreateRunLoopSource( allocator: CFAllocatorRef; local: CFMessagePortRef; order: CFIndex ): CFRunLoopSourceRef;

{#if (TARGET_OS_MAC && !(TARGET_OS_EMBEDDED || TARGET_OS_IPHONE)) || (TARGET_OS_EMBEDDED || TARGET_OS_IPHONE)}
{
First requires translation of dispatch/dispatch.h and the files it includes

procedure CFMessagePortSetDispatchQueue( ms: CFMessagePortRef; queue: dispatch_queue_t );
CF_AVAILABLE_STARTING(10_6, 4_0);
}

{#endif}

end.
