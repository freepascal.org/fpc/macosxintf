{
     File:       QD/PictUtils.h
 
     Contains:   Picture Utilities Interfaces.
 
     Version:    Quickdraw-262~1
 
     Copyright:  � 1990-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{   Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
unit PictUtils;
interface
uses MacTypes,QuickdrawTypes,Palettes;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{ verbs for the GetPictInfo, GetPixMapInfo, and NewPictInfo calls }
const
	returnColorTable = $0001;
	returnPalette = $0002;
	recordComments = $0004;
	recordFontInfo = $0008;
	suppressBlackAndWhite = $0010;

const
{ color pick methods }
	systemMethod = 0;    { system color pick method }
	popularMethod = 1;    { method that chooses the most popular set of colors }
	medianMethod = 2;     { method that chooses a good average mix of colors }

const
{ color bank types }
	ColorBankIsCustom = -1;
	ColorBankIsExactAnd555 = 0;
	ColorBankIs555 = 1;

type
	PictInfoID = SIGNEDLONG;
	CommentSpec = record
		count: SInt16;                  { number of occurrances of this comment ID }
		ID: SInt16;                     { ID for the comment in the picture }
	end;
	CommentSpecPtr = ^CommentSpec;
type
	CommentSpecHandle = ^CommentSpecPtr;
	FontSpec = record
		pictFontID: SInt16;             { ID of the font in the picture }
		sysFontID: SInt16;              { ID of the same font in the current system file }
		size: array [0..3] of SInt32;                { bit array of all the sizes found (1..127) (bit 0 means > 127) }
		style: SInt16;                  { combined style of all occurrances of the font }
		nameOffset: SIGNEDLONG;             { offset into the fontNamesHdl handle for the font�s name }
	end;
	FontSpecPtr = ^FontSpec;
type
	FontSpecHandle = ^FontSpecPtr;
	PictInfo = record
		version: SInt16;                { this is always zero, for now }
		uniqueColors: SIGNEDLONG;           { the number of actual colors in the picture(s)/pixmap(s) }
		thePalette: PaletteHandle;             { handle to the palette information }
		theColorTable: CTabHandle;          { handle to the color table }
		hRes: Fixed;                   { maximum horizontal resolution for all the pixmaps }
		vRes: Fixed;                   { maximum vertical resolution for all the pixmaps }
		depth: SInt16;                  { maximum depth for all the pixmaps (in the picture) }
		sourceRect: Rect;             { the picture frame rectangle (this contains the entire picture) }
		textCount: SIGNEDLONG;              { total number of text strings in the picture }
		lineCount: SIGNEDLONG;              { total number of lines in the picture }
		rectCount: SIGNEDLONG;              { total number of rectangles in the picture }
		rRectCount: SIGNEDLONG;             { total number of round rectangles in the picture }
		ovalCount: SIGNEDLONG;              { total number of ovals in the picture }
		arcCount: SIGNEDLONG;               { total number of arcs in the picture }
		polyCount: SIGNEDLONG;              { total number of polygons in the picture }
		regionCount: SIGNEDLONG;            { total number of regions in the picture }
		bitMapCount: SIGNEDLONG;            { total number of bitmaps in the picture }
		pixMapCount: SIGNEDLONG;            { total number of pixmaps in the picture }
		commentCount: SIGNEDLONG;           { total number of comments in the picture }
		uniqueComments: SIGNEDLONG;         { the number of unique comments in the picture }
		commentHandle: CommentSpecHandle;          { handle to all the comment information }
		uniqueFonts: SIGNEDLONG;            { the number of unique fonts in the picture }
		fontHandle: FontSpecHandle;             { handle to the FontSpec information }
		fontNamesHandle: Handle;        { handle to the font names }
		reserved1: SIGNEDLONG;
		reserved2: SIGNEDLONG;
	end;
	PictInfoPtr = ^PictInfo;
type
	PictInfoHandle = ^PictInfoPtr;
	InitPickMethodProcPtr = function( colorsRequested: SInt16; var dataRef: UInt32; var colorBankType: SInt16 ): OSErr;
	RecordColorsProcPtr = function( dataRef: UInt32; var colorsArray: RGBColor; colorCount: SInt32; var uniqueColors: SInt32 ): OSErr;
	CalcColorTableProcPtr = function( dataRef: UInt32; colorsRequested: SInt16; colorBankPtr: univ Ptr; resultPtr: CSpecArray ): OSErr;
	DisposeColorPickMethodProcPtr = function( dataRef: UInt32 ): OSErr;
{GPC-ONLY-START}
	InitPickMethodUPP = UniversalProcPtr; // should be InitPickMethodProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	InitPickMethodUPP = InitPickMethodProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	InitPickMethodUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	RecordColorsUPP = UniversalProcPtr; // should be RecordColorsProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	RecordColorsUPP = RecordColorsProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	RecordColorsUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	CalcColorTableUPP = UniversalProcPtr; // should be CalcColorTableProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	CalcColorTableUPP = CalcColorTableProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	CalcColorTableUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{GPC-ONLY-START}
	DisposeColorPickMethodUPP = UniversalProcPtr; // should be DisposeColorPickMethodProcPtr
{GPC-ONLY-FINISH}
{FPC-ONLY-START}
	DisposeColorPickMethodUPP = DisposeColorPickMethodProcPtr;
{FPC-ONLY-FINISH}
{MW-ONLY-START}
	DisposeColorPickMethodUPP = ^SInt32; { an opaque type }
{MW-ONLY-FINISH}
{
 *  NewInitPickMethodUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewInitPickMethodUPP( userRoutine: InitPickMethodProcPtr ): InitPickMethodUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewRecordColorsUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewRecordColorsUPP( userRoutine: RecordColorsProcPtr ): RecordColorsUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewCalcColorTableUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewCalcColorTableUPP( userRoutine: CalcColorTableProcPtr ): CalcColorTableUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewDisposeColorPickMethodUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewDisposeColorPickMethodUPP( userRoutine: DisposeColorPickMethodProcPtr ): DisposeColorPickMethodUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeInitPickMethodUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeInitPickMethodUPP( userUPP: InitPickMethodUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeRecordColorsUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeRecordColorsUPP( userUPP: RecordColorsUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeCalcColorTableUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeCalcColorTableUPP( userUPP: CalcColorTableUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposeDisposeColorPickMethodUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeDisposeColorPickMethodUPP( userUPP: DisposeColorPickMethodUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeInitPickMethodUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeInitPickMethodUPP( colorsRequested: SInt16; var dataRef: UInt32; var colorBankType: SInt16; userUPP: InitPickMethodUPP ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeRecordColorsUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeRecordColorsUPP( dataRef: UInt32; var colorsArray: RGBColor; colorCount: SInt32; var uniqueColors: SInt32; userUPP: RecordColorsUPP ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeCalcColorTableUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeCalcColorTableUPP( dataRef: UInt32; colorsRequested: SInt16; colorBankPtr: univ Ptr; resultPtr: CSpecArray; userUPP: CalcColorTableUPP ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokeDisposeColorPickMethodUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeDisposeColorPickMethodUPP( dataRef: UInt32; userUPP: DisposeColorPickMethodUPP ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$ifc not TARGET_CPU_64}
{
 *  GetPictInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetPictInfo( thePictHandle: PicHandle; var thePictInfo: PictInfo; verb: SInt16; colorsRequested: SInt16; colorPickMethod: SInt16; version: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  GetPixMapInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetPixMapInfo( thePixMapHandle: PixMapHandle; var thePictInfo: PictInfo; verb: SInt16; colorsRequested: SInt16; colorPickMethod: SInt16; version: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  NewPictInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function NewPictInfo( var thePictInfoID: PictInfoID; verb: SInt16; colorsRequested: SInt16; colorPickMethod: SInt16; version: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  RecordPictInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function RecordPictInfo( thePictInfoID: PictInfoID; thePictHandle: PicHandle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  RecordPixMapInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function RecordPixMapInfo( thePictInfoID: PictInfoID; thePixMapHandle: PixMapHandle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  RetrievePictInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function RetrievePictInfo( thePictInfoID: PictInfoID; var thePictInfo: PictInfo; colorsRequested: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  DisposePictInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DisposePictInfo( thePictInfoID: PictInfoID ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
