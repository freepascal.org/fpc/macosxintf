{
 * ImageIO - CGImageProperties.h
 * Copyright (c) 2004-2010 Apple Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit CGImageProperties;
interface
uses MacTypes,CFBase,CGBase;

{$ALIGN POWER}


{ Properties that, if returned by CGImageSourceCopyProperties or 
 * CGImageSourceCopyPropertiesAtIndex, contain a dictionary of file-format 
 * or metadata-format specific key-values. }

const kCGImagePropertyTIFFDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGIFDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyJFIFDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyPNGDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyRawDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyCIFFDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyMakerCanonDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerMinoltaDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerFujiDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerOlympusDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerPentaxDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImageProperty8BIMDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyDNGDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyExifAuxDictionary: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);


{* Properties which may be returned by "CGImageSourceCopyProperties".  The
 ** values apply to the container in general but not necessarily to any
 ** individual image that it contains. *}

{ The size of the image file in bytes, if known. If present, the value of
 * this key is a CFNumberRef. }

const kCGImagePropertyFileSize: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


{* Properties which may be returned by "CGImageSourceCopyPropertiesAtIndex".
 ** The values apply to a single image of an image source file. *}

{ The number of pixels in the x- and y-dimensions. The value of these keys 
 * is a CFNumberRef. }

const kCGImagePropertyPixelHeight: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyPixelWidth: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ The DPI in the x- and y-dimensions, if known. If present, the value of
 * these keys is a CFNumberRef. }

const kCGImagePropertyDPIHeight: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyDPIWidth: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ The number of bits in each color sample of each pixel. The value of this 
 * key is a CFNumberRef. }

const kCGImagePropertyDepth: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ The intended display orientation of the image. If present, the value 
 * of this key is a CFNumberRef with the same value as defined by the 
 * TIFF and Exif specifications.  That is:
 *   1  =  0th row is at the top, and 0th column is on the left.  
 *   2  =  0th row is at the top, and 0th column is on the right.  
 *   3  =  0th row is at the bottom, and 0th column is on the right.  
 *   4  =  0th row is at the bottom, and 0th column is on the left.  
 *   5  =  0th row is on the left, and 0th column is the top.  
 *   6  =  0th row is on the right, and 0th column is the top.  
 *   7  =  0th row is on the right, and 0th column is the bottom.  
 *   8  =  0th row is on the left, and 0th column is the bottom.  
 * If not present, a value of 1 is assumed. } 
 
const kCGImagePropertyOrientation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ The value of this key is kCFBooleanTrue if the image contains floating- 
 * point pixel samples } 
 
const kCGImagePropertyIsFloat: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ The value of this key is kCFBooleanTrue if the image contains indexed 
 * (a.k.a. paletted) pixel samples } 
 
const kCGImagePropertyIsIndexed: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ The value of this key is kCFBooleanTrue if the image contains an alpha 
 * (a.k.a. coverage) channel } 
 
const kCGImagePropertyHasAlpha: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ The color model of the image such as "RGB", "CMYK", "Gray", or "Lab".
 * The value of this key is CFStringRef. } 

const kCGImagePropertyColorModel: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ The name of the optional ICC profile embedded in the image, if known.  
 * If present, the value of this key is a CFStringRef. }

const kCGImagePropertyProfileName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


{ Possible values for kCGImagePropertyColorModel property }

const kCGImagePropertyColorModelRGB: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyColorModelGray: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyColorModelCMYK: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyColorModelLab: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


{ Possible keys for kCGImagePropertyTIFFDictionary }

const kCGImagePropertyTIFFCompression: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFPhotometricInterpretation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFDocumentName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFImageDescription: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFMake: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFModel: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFOrientation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFXResolution: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFYResolution: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFResolutionUnit: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFSoftware: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFTransferFunction: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFDateTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFArtist: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFHostComputer: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFCopyright: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFWhitePoint: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyTIFFPrimaryChromaticities: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Possible keys for kCGImagePropertyJFIFDictionary }

const kCGImagePropertyJFIFVersion: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyJFIFXDensity: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyJFIFYDensity: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyJFIFDensityUnit: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyJFIFIsProgressive: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


{ Possible keys for kCGImagePropertyExifDictionary }

const kCGImagePropertyExifExposureTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifExposureProgram: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSpectralSensitivity: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifISOSpeedRatings: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifOECF: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifVersion: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifDateTimeOriginal: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifDateTimeDigitized: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifComponentsConfiguration: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifCompressedBitsPerPixel: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifShutterSpeedValue: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifApertureValue: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifBrightnessValue: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifExposureBiasValue: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifMaxApertureValue: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSubjectDistance: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifMeteringMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifLightSource: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFlash: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFocalLength: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSubjectArea: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifMakerNote: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifUserComment: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSubsecTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSubsecTimeOrginal: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSubsecTimeDigitized: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFlashPixVersion: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifColorSpace: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifPixelXDimension: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifPixelYDimension: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifRelatedSoundFile: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFlashEnergy: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSpatialFrequencyResponse: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFocalPlaneXResolution: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFocalPlaneYResolution: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFocalPlaneResolutionUnit: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSubjectLocation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifExposureIndex: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSensingMethod: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFileSource: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSceneType: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifCFAPattern: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifCustomRendered: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifExposureMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifWhiteBalance: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifDigitalZoomRatio: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifFocalLenIn35mmFilm: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSceneCaptureType: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifGainControl: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifContrast: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSaturation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSharpness: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifDeviceSettingDescription: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifSubjectDistRange: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyExifImageUniqueID: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

const kCGImagePropertyExifCameraOwnerName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyExifBodySerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyExifLensSpecification: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyExifLensMake: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyExifLensModel: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyExifLensSerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);

const kCGImagePropertyExifGamma: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Possible keys for kCGImagePropertyExifAuxDictionary }
const kCGImagePropertyExifAuxLensInfo: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyExifAuxLensModel: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyExifAuxSerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyExifAuxLensID: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyExifAuxLensSerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyExifAuxImageNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyExifAuxFlashCompensation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyExifAuxOwnerName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyExifAuxFirmware: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);

{ Possible keys for kCGImagePropertyGIFDictionary }

const kCGImagePropertyGIFLoopCount: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGIFDelayTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGIFImageColorMap: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGIFHasGlobalColorMap: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGIFUnclampedDelayTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_4_0);

{ Possible keys for kCGImagePropertyPNGDictionary }

const kCGImagePropertyPNGGamma: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyPNGInterlaceType: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyPNGXPixelsPerMeter: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyPNGYPixelsPerMeter: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyPNGsRGBIntent: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyPNGChromaticities: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

const kCGImagePropertyPNGAuthor: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyPNGCopyright: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyPNGCreationTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyPNGDescription: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyPNGModificationTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyPNGSoftware: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);
const kCGImagePropertyPNGTitle: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_5_0);

{ Possible keys for kCGImagePropertyGPSDictionary }

const kCGImagePropertyGPSVersion: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSLatitudeRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSLatitude: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSLongitudeRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSLongitude: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSAltitudeRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSAltitude: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSTimeStamp: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSSatellites: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSStatus: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSMeasureMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDOP: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSSpeedRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSSpeed: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSTrackRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSTrack: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSImgDirectionRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSImgDirection: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSMapDatum: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDestLatitudeRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDestLatitude: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDestLongitudeRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDestLongitude: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDestBearingRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDestBearing: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDestDistanceRef: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDestDistance: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSProcessingMethod: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSAreaInformation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDateStamp: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyGPSDifferental: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);

{ Possible keys for kCGImagePropertyIPTCDictionary }

const kCGImagePropertyIPTCObjectTypeReference: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCObjectAttributeReference: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCObjectName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCEditStatus: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCEditorialUpdate: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCUrgency: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCSubjectReference: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCCategory: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCSupplementalCategory: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCFixtureIdentifier: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCKeywords: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCContentLocationCode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCContentLocationName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCReleaseDate: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCReleaseTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCExpirationDate: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCExpirationTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCSpecialInstructions: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCActionAdvised: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCReferenceService: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCReferenceDate: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCReferenceNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCDateCreated: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCTimeCreated: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCDigitalCreationDate: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCDigitalCreationTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCOriginatingProgram: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCProgramVersion: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCObjectCycle: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCByline: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCBylineTitle: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCCity: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCSubLocation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCProvinceState: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCCountryPrimaryLocationCode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCCountryPrimaryLocationName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCOriginalTransmissionReference: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCHeadline: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCCredit: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCSource: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCCopyrightNotice: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCContact: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCCaptionAbstract: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCWriterEditor: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCImageType: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCImageOrientation: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCLanguageIdentifier: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCStarRating: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);
const kCGImagePropertyIPTCCreatorContactInfo: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);	// IPTC Core
const kCGImagePropertyIPTCRightsUsageTerms: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);	// IPTC Core
const kCGImagePropertyIPTCScene: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);				// IPTC Core

{ Possible keys for kCGImagePropertyIPTCCreatorContactInfo dictionary (part of IPTC Core - above) }

const kCGImagePropertyIPTCContactInfoCity: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);
const kCGImagePropertyIPTCContactInfoCountry: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);
const kCGImagePropertyIPTCContactInfoAddress: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);
const kCGImagePropertyIPTCContactInfoPostalCode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);
const kCGImagePropertyIPTCContactInfoStateProvince: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);
const kCGImagePropertyIPTCContactInfoEmails: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);
const kCGImagePropertyIPTCContactInfoPhones: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);
const kCGImagePropertyIPTCContactInfoWebURLs: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_4_0);

{ Possible keys for kCGImageProperty8BIMDictionary }

const kCGImageProperty8BIMLayerNames: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_4_0);


{ Possible keys for kCGImagePropertyDNGDictionary }

const kCGImagePropertyDNGVersion: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyDNGBackwardVersion: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyDNGUniqueCameraModel: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyDNGLocalizedCameraModel: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyDNGCameraSerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyDNGLensInfo: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);


{ Possible keys for kCGImagePropertyCIFFDictionary }

const kCGImagePropertyCIFFDescription: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFFirmware: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFOwnerName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFImageName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFImageFileName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFReleaseMethod: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFReleaseTiming: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFRecordID: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFSelfTimingTime: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFCameraSerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFImageSerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFContinuousDrive: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFFocusMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFMeteringMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFShootingMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFLensModel: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFLensMaxMM: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFLensMinMM: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFWhiteBalanceIndex: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFFlashExposureComp: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyCIFFMeasuredEV: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);


{ Possible keys for kCGImagePropertyMakerNikonDictionary }

const kCGImagePropertyMakerNikonISOSetting: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonColorMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonQuality: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonWhiteBalanceMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonSharpenMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonFocusMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonFlashSetting: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonISOSelection: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonFlashExposureComp: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonImageAdjustment: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonLensAdapter: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonLensType: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonLensInfo: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonFocusDistance: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonDigitalZoom: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonShootingMode: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonCameraSerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerNikonShutterCount: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);

{ Possible keys for kCGImagePropertyMakerCanonDictionary }

const kCGImagePropertyMakerCanonOwnerName: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerCanonCameraSerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerCanonImageSerialNumber: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerCanonFlashExposureComp: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerCanonContinuousDrive: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerCanonLensModel: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerCanonFirmware: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);
const kCGImagePropertyMakerCanonAspectRatioInfo: CFStringRef;
IMAGEIO_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_4_0);


end.
