{
     File:       CarbonCore/NumberFormatting.h
 
     Contains:   Utilites for formatting numbers
 
     Copyright:  � 1996-2012 by Apple Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit NumberFormatting;
interface
uses MacTypes,ConditionalMacros,IntlResources;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{

    Here are the current System 7 routine names and the translations to the older forms.
    Please use the newer forms in all new code and migrate the older names out of existing
    code as maintainance permits.
    
    New Name                    Old Name(s)
    
    ExtendedToString            FormatX2Str
    FormatRecToString           Format2Str
    NumToString             
    StringToExtended            FormatStr2X
    StringToFormatRec           Str2Format
    StringToNum             

}

type
	NumFormatStringPtr = ^NumFormatString;
	NumFormatString = packed record
		fLength: UInt8;
		fVersion: UInt8;
		data: packed array [0..253] of char;              { private data }
	end;
type
	NumFormatStringRec = NumFormatString;
	NumFormatStringRecPtr = ^NumFormatStringRec;
	FormatStatus = SInt16;
const
	fVNumber = 0;     { first version of NumFormatString }

type
	FormatClass = SInt8;
const
	fPositive = 0;
	fNegative = 1;
	fZero = 2;

type
	FormatResultType = SInt8;
const
	fFormatOK = 0;
	fBestGuess = 1;
	fOutOfSynch = 2;
	fSpuriousChars = 3;
	fMissingDelimiter = 4;
	fExtraDecimal = 5;
	fMissingLiteral = 6;
	fExtraExp = 7;
	fFormatOverflow = 8;
	fFormStrIsNAN = 9;
	fBadPartsTable = 10;
	fExtraPercent = 11;
	fExtraSeparator = 12;
	fEmptyFormatString = 13;

type
	FVectorPtr = ^FVector;
	FVector = record
		start: SInt16;
		length: SInt16;
	end;
{ index by [fPositive..fZero] }
	TripleInt							= array [0..2] of FVector;
{$ifc not TARGET_CPU_64}
{
 *  StringToNum()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringGetIntValue instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringGetIntValue instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure StringToNum( const var theString: Str255; var theNum: SIGNEDLONG );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  NumToString()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCreateWithFormat instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCreateWithFormat instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure NumToString( theNum: SIGNEDLONG; var theString: Str255 );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{$endc} {not TARGET_CPU_64}

{$ifc not TARGET_CPU_64}
{
 *  ExtendedToString()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFNumberFormatterCreateStringWithNumber instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFNumberFormatterCreateStringWithNumber instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function ExtendedToString( const var x: extended80; const var myCanonical: NumFormatString; const var partsTable: NumberParts; var outString: Str255 ): FormatStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  StringToExtended()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFNumberFormatterCreateNumberFromString instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFNumberFormatterCreateNumberFromString instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function StringToExtended( const var source: Str255; const var myCanonical: NumFormatString; const var partsTable: NumberParts; var x: extended80 ): FormatStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  StringToFormatRec()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFNumberFormatterSetFormat instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFNumberFormatterSetFormat instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function StringToFormatRec( const var inString: Str255; const var partsTable: NumberParts; var outString: NumFormatString ): FormatStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  FormatRecToString()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFNumberFormatterSetFormat instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFNumberFormatterSetFormat instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FormatRecToString( const var myCanonical: NumFormatString; const var partsTable: NumberParts; var outString: Str255; var positions: TripleInt ): FormatStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
