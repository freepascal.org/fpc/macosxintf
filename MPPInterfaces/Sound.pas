{
     File:       CarbonSound/Sound.h
 
     Contains:   Sound Manager Interfaces.
 
     Version:    CarbonSound-115~164
 
     Copyright:  � 1986-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit Sound;
interface
uses MacTypes,Components,MixedMode,Dialogs;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

{$ifc not TARGET_CPU_64}
{
                        * * *  N O T E  * * *

    This file has been updated to include Sound Manager 3.3 interfaces.

    Some of the Sound Manager 3.0 interfaces were not put into the InterfaceLib
    that originally shipped with the PowerMacs. These missing functions and the
    new 3.3 interfaces have been released in the SoundLib library for PowerPC
    developers to link with. The runtime library for these functions are
    installed by the Sound Manager. The following functions are found in SoundLib.

        GetCompressionInfo(), GetSoundPreference(), SetSoundPreference(),
        UnsignedFixedMulDiv(), SndGetInfo(), SndSetInfo(), GetSoundOutputInfo(),
        SetSoundOutputInfo(), GetCompressionName(), SoundConverterOpen(),
        SoundConverterClose(), SoundConverterGetBufferSizes(), SoundConverterBeginConversion(),
        SoundConverterConvertBuffer(), SoundConverterEndConversion(),
        AudioGetBass(), AudioGetInfo(), AudioGetMute(), AudioGetOutputDevice(),
        AudioGetTreble(), AudioGetVolume(), AudioMuteOnEvent(), AudioSetBass(),
        AudioSetMute(), AudioSetToDefaults(), AudioSetTreble(), AudioSetVolume(),
        OpenMixerSoundComponent(), CloseMixerSoundComponent(), SoundComponentAddSource(),
        SoundComponentGetInfo(), SoundComponentGetSource(), SoundComponentGetSourceData(),
        SoundComponentInitOutputDevice(), SoundComponentPauseSource(),
        SoundComponentPlaySourceBuffer(), SoundComponentRemoveSource(),
        SoundComponentSetInfo(), SoundComponentSetOutput(), SoundComponentSetSource(),
        SoundComponentStartSource(), SoundComponentStopSource(),
        ParseAIFFHeader(), ParseSndHeader(), SoundConverterGetInfo(), SoundConverterSetInfo()
}
{
    Interfaces for Sound Driver, !!! OBSOLETE and NOT SUPPORTED !!!

    These items are no longer defined, but appear here so that someone
    searching the interfaces might find them. If you are using one of these
    items, you must change your code to support the Sound Manager.

        swMode, ftMode, ffMode
        FreeWave, FFSynthRec, Tone, SWSynthRec, Wave, FTSoundRec
        SndCompletionProcPtr
        StartSound, StopSound, SoundDone
        SetSoundVol, GetSoundVol
}
{
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   constants
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}
const
	twelfthRootTwo = 1.05946309435;

const
	soundListRsrc = FOUR_CHAR_CODE('snd '); {Resource type used by Sound Manager}
	kSoundCodecInfoResourceType = FOUR_CHAR_CODE('snfo'); {Resource type holding codec information (optional public component resource)}

const
	kSimpleBeepID = 1;     {reserved resource ID for Simple Beep}

const
	rate48khz = $BB800000; {48000.00000 in fixed-point}
	rate44khz = $AC440000; {44100.00000 in fixed-point}
	rate32khz = $7D000000; {32000.00000 in fixed-point}
	rate22050hz = $56220000; {22050.00000 in fixed-point}
	rate22khz = $56EE8BA3; {22254.54545 in fixed-point}
	rate16khz = $3E800000; {16000.00000 in fixed-point}
	rate11khz = $2B7745D1; {11127.27273 in fixed-point}
	rate11025hz = $2B110000; {11025.00000 in fixed-point}
	rate8khz = $1F400000; { 8000.00000 in fixed-point}

{synthesizer numbers for SndNewChannel}
const
	sampledSynth = 5;     {sampled sound synthesizer}

{$ifc CALL_NOT_IN_CARBON}
const
	squareWaveSynth = 1;    {square wave synthesizer}
	waveTableSynth = 3;    {wave table synthesizer}
                                        {old Sound Manager MACE synthesizer numbers}
	MACE3snthID = 11;
	MACE6snthID = 13;

{$endc} {CALL_NOT_IN_CARBON}

const
	kMiddleC = 60;    {MIDI note value for middle C}

const
	kNoVolume = 0;    {setting for no sound volume}
	kFullVolume = $0100; {1.0, setting for full hardware output volume}

const
	stdQLength = 128;

const
	dataOffsetFlag = $8000;

const
	kUseOptionalOutputDevice = -1;    {only for Sound Manager 3.0 or later}

const
	notCompressed = 0;    {compression ID's}
	fixedCompression = -1;   {compression ID for fixed-sized compression}
	variableCompression = -2;    {compression ID for variable-sized compression}

const
	twoToOne = 1;
	eightToThree = 2;
	threeToOne = 3;
	sixToOne = 4;
	sixToOnePacketSize = 8;
	threeToOnePacketSize = 16;

const
	stateBlockSize = 64;
	leftOverBlockSize = 32;

const
	firstSoundFormat = $0001; {general sound format}
	secondSoundFormat = $0002; {special sampled sound format (HyperCard)}

{$ifc CALL_NOT_IN_CARBON}
const
	dbBufferReady = $00000001; {double buffer is filled}
	dbLastBuffer = $00000004; {last double buffer to play}

{$endc} {CALL_NOT_IN_CARBON}

const
	sysBeepDisable = $0000; {SysBeep() enable flags}
	sysBeepEnable = 1 shl 0;
	sysBeepSynchronous = 1 shl 1; {if bit set, make alert sounds synchronous}

const
	unitTypeNoSelection = $FFFF; {unitTypes for AudioSelection.unitType}
	unitTypeSeconds = $0000;

const
	stdSH = $00; {Standard sound header encode value}
	extSH = $FF; {Extended sound header encode value}
	cmpSH = $FE;  {Compressed sound header encode value}

{command numbers for SndDoCommand and SndDoImmediate}
const
	nullCmd = 0;
	quietCmd = 3;
	flushCmd = 4;
	reInitCmd = 5;
	waitCmd = 10;
	pauseCmd = 11;
	resumeCmd = 12;
	callBackCmd = 13;
	syncCmd = 14;
	availableCmd = 24;
	versionCmd = 25;
	volumeCmd = 46;   {sound manager 3.0 or later only}
	getVolumeCmd = 47;   {sound manager 3.0 or later only}
	clockComponentCmd = 50;   {sound manager 3.2.1 or later only}
	getClockComponentCmd = 51;   {sound manager 3.2.1 or later only}
	scheduledSoundCmd = 52;   {sound manager 3.3 or later only}
	linkSoundComponentsCmd = 53;   {sound manager 3.3 or later only}
	soundCmd = 80;
	bufferCmd = 81;
	rateMultiplierCmd = 86;
	getRateMultiplierCmd = 87;

{$ifc CALL_NOT_IN_CARBON}
{command numbers for SndDoCommand and SndDoImmediate that are not available for use in Carbon }
const
	initCmd = 1;
	freeCmd = 2;
	totalLoadCmd = 26;
	loadCmd = 27;
	freqDurationCmd = 40;
	restCmd = 41;
	freqCmd = 42;
	ampCmd = 43;
	timbreCmd = 44;
	getAmpCmd = 45;
	waveTableCmd = 60;
	phaseCmd = 61;
	rateCmd = 82;
	continueCmd = 83;
	doubleBufferCmd = 84;
	getRateCmd = 85;
	sizeCmd = 90;   {obsolete command}
	convertCmd = 91;    {obsolete MACE command}

{$endc} {CALL_NOT_IN_CARBON}

{$ifc OLDROUTINENAMES}
{channel initialization parameters}
const
	waveInitChannelMask = $07;
	waveInitChannel0 = $04; {wave table only, Sound Manager 2.0 and earlier}
	waveInitChannel1 = $05; {wave table only, Sound Manager 2.0 and earlier}
	waveInitChannel2 = $06; {wave table only, Sound Manager 2.0 and earlier}
	waveInitChannel3 = $07; {wave table only, Sound Manager 2.0 and earlier}
	initChan0 = waveInitChannel0; {obsolete spelling}
	initChan1 = waveInitChannel1; {obsolete spelling}
	initChan2 = waveInitChannel2; {obsolete spelling}
	initChan3 = waveInitChannel3; {obsolete spelling}

const
	outsideCmpSH = 0;    {obsolete MACE constant}
	insideCmpSH = 1;    {obsolete MACE constant}
	aceSuccess = 0;    {obsolete MACE constant}
	aceMemFull = 1;    {obsolete MACE constant}
	aceNilBlock = 2;    {obsolete MACE constant}
	aceBadComp = 3;    {obsolete MACE constant}
	aceBadEncode = 4;    {obsolete MACE constant}
	aceBadDest = 5;    {obsolete MACE constant}
	aceBadCmd = 6;     {obsolete MACE constant}

{$endc} {OLDROUTINENAMES}

const
	initChanLeft = $0002; {left stereo channel}
	initChanRight = $0003; {right stereo channel}
	initNoInterp = $0004; {no linear interpolation}
	initNoDrop = $0008; {no drop-sample conversion}
	initMono = $0080; {monophonic channel}
	initStereo = $00C0; {stereo channel}
	initMACE3 = $0300; {MACE 3:1}
	initMACE6 = $0400; {MACE 6:1}
	initPanMask = $0003; {mask for right/left pan values}
	initSRateMask = $0030; {mask for sample rate values}
	initStereoMask = $00C0; {mask for mono/stereo values}
	initCompMask = $FF00; {mask for compression IDs}

{Get&Set Sound Information Selectors}
const
	siActiveChannels = FOUR_CHAR_CODE('chac'); {active channels}
	siActiveLevels = FOUR_CHAR_CODE('lmac'); {active meter levels}
	siAGCOnOff = FOUR_CHAR_CODE('agc '); {automatic gain control state}
	siAsync = FOUR_CHAR_CODE('asyn'); {asynchronous capability}
	siAVDisplayBehavior = FOUR_CHAR_CODE('avdb');
	siChannelAvailable = FOUR_CHAR_CODE('chav'); {number of channels available}
	siCompressionAvailable = FOUR_CHAR_CODE('cmav'); {compression types available}
	siCompressionFactor = FOUR_CHAR_CODE('cmfa'); {current compression factor}
	siCompressionHeader = FOUR_CHAR_CODE('cmhd'); {return compression header}
	siCompressionNames = FOUR_CHAR_CODE('cnam'); {compression type names available}
	siCompressionParams = FOUR_CHAR_CODE('evaw'); {compression parameters}
	siCompressionSampleRate = FOUR_CHAR_CODE('cprt'); { SetInfo only: compressor's sample rate}
	siCompressionChannels = FOUR_CHAR_CODE('cpct'); { SetInfo only: compressor's number of channels}
	siCompressionOutputSampleRate = FOUR_CHAR_CODE('cort'); { GetInfo only: only implemented by compressors that have differing in and out rates }
	siCompressionInputRateList = FOUR_CHAR_CODE('crtl'); { GetInfo only: only implemented by compressors that only take certain input rates }
	siCompressionType = FOUR_CHAR_CODE('comp'); {current compression type}
	siCompressionConfiguration = FOUR_CHAR_CODE('ccfg'); {compression extensions}
	siContinuous = FOUR_CHAR_CODE('cont'); {continous recording}
	siDecompressionParams = FOUR_CHAR_CODE('wave'); {decompression parameters}
	siDecompressionConfiguration = FOUR_CHAR_CODE('dcfg'); {decompression extensions}
	siDeviceBufferInfo = FOUR_CHAR_CODE('dbin'); {size of interrupt buffer}
	siDeviceConnected = FOUR_CHAR_CODE('dcon'); {input device connection status}
	siDeviceIcon = FOUR_CHAR_CODE('icon'); {input device icon}
	siDeviceName = FOUR_CHAR_CODE('name'); {input device name}
	siEQSpectrumBands = FOUR_CHAR_CODE('eqsb'); { number of spectrum bands}
	siEQSpectrumLevels = FOUR_CHAR_CODE('eqlv'); { gets spectum meter levels}
	siEQSpectrumOnOff = FOUR_CHAR_CODE('eqlo'); { turn on/off spectum meter levels}
	siEQSpectrumResolution = FOUR_CHAR_CODE('eqrs'); { set the resolution of the FFT, 0 = low res (<=16 bands), 1 = high res (16-64 bands)}
	siEQToneControlGain = FOUR_CHAR_CODE('eqtg'); { set the bass and treble gain}
	siEQToneControlOnOff = FOUR_CHAR_CODE('eqtc'); { turn on equalizer attenuation}
	siHardwareBalance = FOUR_CHAR_CODE('hbal');
	siHardwareBalanceSteps = FOUR_CHAR_CODE('hbls');
	siHardwareBass = FOUR_CHAR_CODE('hbas');
	siHardwareBassSteps = FOUR_CHAR_CODE('hbst');
	siHardwareBusy = FOUR_CHAR_CODE('hwbs'); {sound hardware is in use}
	siHardwareFormat = FOUR_CHAR_CODE('hwfm'); {get hardware format}
	siHardwareMute = FOUR_CHAR_CODE('hmut'); {mute state of all hardware}
	siHardwareMuteNoPrefs = FOUR_CHAR_CODE('hmnp'); {mute state of all hardware, but don't store in prefs }
	siHardwareTreble = FOUR_CHAR_CODE('htrb');
	siHardwareTrebleSteps = FOUR_CHAR_CODE('hwts');
	siHardwareVolume = FOUR_CHAR_CODE('hvol'); {volume level of all hardware}
	siHardwareVolumeSteps = FOUR_CHAR_CODE('hstp'); {number of volume steps for hardware}
	siHeadphoneMute = FOUR_CHAR_CODE('pmut'); {mute state of headphones}
	siHeadphoneVolume = FOUR_CHAR_CODE('pvol'); {volume level of headphones}
	siHeadphoneVolumeSteps = FOUR_CHAR_CODE('hdst'); {number of volume steps for headphones}
	siInputAvailable = FOUR_CHAR_CODE('inav'); {input sources available}
	siInputGain = FOUR_CHAR_CODE('gain'); {input gain}
	siInputSource = FOUR_CHAR_CODE('sour'); {input source selector}
	siInputSourceNames = FOUR_CHAR_CODE('snam'); {input source names}
	siLevelMeterOnOff = FOUR_CHAR_CODE('lmet'); {level meter state}
	siModemGain = FOUR_CHAR_CODE('mgai'); {modem input gain}
	siMonitorAvailable = FOUR_CHAR_CODE('mnav');
	siMonitorSource = FOUR_CHAR_CODE('mons');
	siNumberChannels = FOUR_CHAR_CODE('chan'); {current number of channels}
	siOptionsDialog = FOUR_CHAR_CODE('optd'); {display options dialog}
	siOSTypeInputSource = FOUR_CHAR_CODE('inpt'); {input source by OSType}
	siOSTypeInputAvailable = FOUR_CHAR_CODE('inav'); {list of available input source OSTypes}
	siOutputDeviceName = FOUR_CHAR_CODE('onam'); {output device name}
	siPlayThruOnOff = FOUR_CHAR_CODE('plth'); {playthrough state}
	siPostMixerSoundComponent = FOUR_CHAR_CODE('psmx'); {install post-mixer effect}
	siPreMixerSoundComponent = FOUR_CHAR_CODE('prmx'); {install pre-mixer effect}
	siQuality = FOUR_CHAR_CODE('qual'); {quality setting}
	siRateMultiplier = FOUR_CHAR_CODE('rmul'); {throttle rate setting}
	siRecordingQuality = FOUR_CHAR_CODE('qual'); {recording quality}
	siSampleRate = FOUR_CHAR_CODE('srat'); {current sample rate}
	siSampleRateAvailable = FOUR_CHAR_CODE('srav'); {sample rates available}
	siSampleSize = FOUR_CHAR_CODE('ssiz'); {current sample size}
	siSampleSizeAvailable = FOUR_CHAR_CODE('ssav'); {sample sizes available}
	siSetupCDAudio = FOUR_CHAR_CODE('sucd'); {setup sound hardware for CD audio}
	siSetupModemAudio = FOUR_CHAR_CODE('sumd'); {setup sound hardware for modem audio}
	siSlopeAndIntercept = FOUR_CHAR_CODE('flap'); {floating point variables for conversion}
	siSoundClock = FOUR_CHAR_CODE('sclk');
	siUseThisSoundClock = FOUR_CHAR_CODE('sclc'); {sdev uses this to tell the mixer to use his sound clock}
	siSpeakerMute = FOUR_CHAR_CODE('smut'); {mute state of all built-in speaker}
	siSpeakerVolume = FOUR_CHAR_CODE('svol'); {volume level of built-in speaker}
	siSSpCPULoadLimit = FOUR_CHAR_CODE('3dll');
	siSSpLocalization = FOUR_CHAR_CODE('3dif');
	siSSpSpeakerSetup = FOUR_CHAR_CODE('3dst');
	siStereoInputGain = FOUR_CHAR_CODE('sgai'); {stereo input gain}
	siSubwooferMute = FOUR_CHAR_CODE('bmut'); {mute state of sub-woofer}
	siTerminalType = FOUR_CHAR_CODE('ttyp'); { usb terminal type }
	siTwosComplementOnOff = FOUR_CHAR_CODE('twos'); {two's complement state}
	siVendorProduct = FOUR_CHAR_CODE('vpro'); { vendor and product ID }
	siVolume = FOUR_CHAR_CODE('volu'); {volume level of source}
	siVoxRecordInfo = FOUR_CHAR_CODE('voxr'); {VOX record parameters}
	siVoxStopInfo = FOUR_CHAR_CODE('voxs'); {VOX stop parameters}
	siWideStereo = FOUR_CHAR_CODE('wide'); {wide stereo setting}
	siSupportedExtendedFlags = FOUR_CHAR_CODE('exfl'); {which flags are supported in Extended sound data structures}
	siRateConverterRollOffSlope = FOUR_CHAR_CODE('rcdb'); {the roll-off slope for the rate converter's filter, in whole dB as a long this value is a long whose range is from 20 (worst quality/fastest performance) to 90 (best quality/slowest performance)}
	siOutputLatency = FOUR_CHAR_CODE('olte'); {latency of sound output component}
	siHALAudioDeviceID = FOUR_CHAR_CODE('hlid'); {audio device id}
	siHALAudioDeviceUniqueID = FOUR_CHAR_CODE('huid'); {audio device unique id}
	siClientAcceptsVBR = FOUR_CHAR_CODE('cvbr'); {client handles VBR}
	siSourceIsExhausted = FOUR_CHAR_CODE('srcx'); {the ultimate source of data has run out (keep asking, but when you get nothing, that's it)}
	siMediaContextID = FOUR_CHAR_CODE('uuid'); {media context id -- UUID }
	siCompressionMaxPacketSize = FOUR_CHAR_CODE('cmxp'); {maximum compressed packet size for current configuration -- unsigned long }
	siAudioCodecPropertyValue = FOUR_CHAR_CODE('spva'); {audio codec property value -- SoundAudioCodecPropertyRequestParams* }
	siAudioCodecPropertyInfo = FOUR_CHAR_CODE('spin'); {audio codec property info -- SoundAudioCodecPropertyRequestParams* }

const
	siCloseDriver = FOUR_CHAR_CODE('clos'); {reserved for internal use only}
	siInitializeDriver = FOUR_CHAR_CODE('init'); {reserved for internal use only}
	siPauseRecording = FOUR_CHAR_CODE('paus'); {reserved for internal use only}
	siUserInterruptProc = FOUR_CHAR_CODE('user'); {reserved for internal use only}

{ input source Types}
const
	kInvalidSource = -1; {this source may be returned from GetInfo if no other source is the monitored source}
	kNoSource = FOUR_CHAR_CODE('none'); {no source selection}
	kCDSource = FOUR_CHAR_CODE('cd  '); {internal CD player input}
	kExtMicSource = FOUR_CHAR_CODE('emic'); {external mic input}
	kSoundInSource = FOUR_CHAR_CODE('sinj'); {sound input jack}
	kRCAInSource = FOUR_CHAR_CODE('irca'); {RCA jack input}
	kTVFMTunerSource = FOUR_CHAR_CODE('tvfm');
	kDAVInSource = FOUR_CHAR_CODE('idav'); {DAV analog input}
	kIntMicSource = FOUR_CHAR_CODE('imic'); {internal mic input}
	kMediaBaySource = FOUR_CHAR_CODE('mbay'); {media bay input}
	kModemSource = FOUR_CHAR_CODE('modm'); {modem input (internal modem on desktops, PCI input on PowerBooks)}
	kPCCardSource = FOUR_CHAR_CODE('pcm '); {PC Card pwm input}
	kZoomVideoSource = FOUR_CHAR_CODE('zvpc'); {zoom video input}
	kDVDSource = FOUR_CHAR_CODE('dvda'); { DVD audio input}
	kMicrophoneArray = FOUR_CHAR_CODE('mica'); { microphone array}

{Sound Component Types and Subtypes}
const
	kNoSoundComponentType = FOUR_CHAR_CODE('****');
	kSoundComponentType = FOUR_CHAR_CODE('sift'); {component type}
	kSoundComponentPPCType = FOUR_CHAR_CODE('nift'); {component type for PowerPC code}
	kRate8SubType = FOUR_CHAR_CODE('ratb'); {8-bit rate converter}
	kRate16SubType = FOUR_CHAR_CODE('ratw'); {16-bit rate converter}
	kConverterSubType = FOUR_CHAR_CODE('conv'); {sample format converter}
	kSndSourceSubType = FOUR_CHAR_CODE('sour'); {generic source component}
	kMixerType = FOUR_CHAR_CODE('mixr');
	kMixer8SubType = FOUR_CHAR_CODE('mixb'); {8-bit mixer}
	kMixer16SubType = FOUR_CHAR_CODE('mixw'); {16-bit mixer}
	kSoundInputDeviceType = FOUR_CHAR_CODE('sinp'); {sound input component}
	kWaveInSubType = FOUR_CHAR_CODE('wavi'); {Windows waveIn}
	kWaveInSnifferSubType = FOUR_CHAR_CODE('wisn'); {Windows waveIn sniffer}
	kSoundOutputDeviceType = FOUR_CHAR_CODE('sdev'); {sound output component}
	kClassicSubType = FOUR_CHAR_CODE('clas'); {classic hardware, i.e. Mac Plus}
	kASCSubType = FOUR_CHAR_CODE('asc '); {Apple Sound Chip device}
	kDSPSubType = FOUR_CHAR_CODE('dsp '); {DSP device}
	kAwacsSubType = FOUR_CHAR_CODE('awac'); {Another of Will's Audio Chips device}
	kGCAwacsSubType = FOUR_CHAR_CODE('awgc'); {Awacs audio with Grand Central DMA}
	kSingerSubType = FOUR_CHAR_CODE('sing'); {Singer (via Whitney) based sound}
	kSinger2SubType = FOUR_CHAR_CODE('sng2'); {Singer 2 (via Whitney) for Acme}
	kWhitSubType = FOUR_CHAR_CODE('whit'); {Whit sound component for PrimeTime 3}
	kSoundBlasterSubType = FOUR_CHAR_CODE('sbls'); {Sound Blaster for CHRP}
	kWaveOutSubType = FOUR_CHAR_CODE('wavo'); {Windows waveOut}
	kWaveOutSnifferSubType = FOUR_CHAR_CODE('wosn'); {Windows waveOut sniffer}
	kDirectSoundSubType = FOUR_CHAR_CODE('dsnd'); {Windows DirectSound}
	kDirectSoundSnifferSubType = FOUR_CHAR_CODE('dssn'); {Windows DirectSound sniffer}
	kUNIXsdevSubType = FOUR_CHAR_CODE('un1x'); {UNIX base sdev}
	kUSBSubType = FOUR_CHAR_CODE('usb '); {USB device}
	kBlueBoxSubType = FOUR_CHAR_CODE('bsnd'); {Blue Box sound component}
	kHALCustomComponentSubType = FOUR_CHAR_CODE('halx'); {Registered by the HAL output component ('hal!') for each HAL output device}
	kSoundCompressor = FOUR_CHAR_CODE('scom');
	kSoundDecompressor = FOUR_CHAR_CODE('sdec');
	kAudioComponentType = FOUR_CHAR_CODE('adio'); {Audio components and sub-types}
	kAwacsPhoneSubType = FOUR_CHAR_CODE('hphn');
	kAudioVisionSpeakerSubType = FOUR_CHAR_CODE('telc');
	kAudioVisionHeadphoneSubType = FOUR_CHAR_CODE('telh');
	kPhilipsFaderSubType = FOUR_CHAR_CODE('tvav');
	kSGSToneSubType = FOUR_CHAR_CODE('sgs0');
	kSoundEffectsType = FOUR_CHAR_CODE('snfx'); {sound effects type}
	kEqualizerSubType = FOUR_CHAR_CODE('eqal'); {frequency equalizer}
	kSSpLocalizationSubType = FOUR_CHAR_CODE('snd3');

{Format Types}
const
	kSoundNotCompressed = FOUR_CHAR_CODE('NONE'); {sound is not compressed}
	k8BitOffsetBinaryFormat = FOUR_CHAR_CODE('raw '); {8-bit offset binary}
	k16BitBigEndianFormat = FOUR_CHAR_CODE('twos'); {16-bit big endian}
	k16BitLittleEndianFormat = FOUR_CHAR_CODE('sowt'); {16-bit little endian}
	kFloat32Format = FOUR_CHAR_CODE('fl32'); {32-bit floating point}
	kFloat64Format = FOUR_CHAR_CODE('fl64'); {64-bit floating point}
	k24BitFormat = FOUR_CHAR_CODE('in24'); {24-bit integer}
	k32BitFormat = FOUR_CHAR_CODE('in32'); {32-bit integer}
	k32BitLittleEndianFormat = FOUR_CHAR_CODE('23ni'); {32-bit little endian integer }
	kMACE3Compression = FOUR_CHAR_CODE('MAC3'); {MACE 3:1}
	kMACE6Compression = FOUR_CHAR_CODE('MAC6'); {MACE 6:1}
	kCDXA4Compression = FOUR_CHAR_CODE('cdx4'); {CD/XA 4:1}
	kCDXA2Compression = FOUR_CHAR_CODE('cdx2'); {CD/XA 2:1}
	kIMACompression = FOUR_CHAR_CODE('ima4'); {IMA 4:1}
	kULawCompression = FOUR_CHAR_CODE('ulaw'); {�Law 2:1}
	kALawCompression = FOUR_CHAR_CODE('alaw'); {aLaw 2:1}
	kMicrosoftADPCMFormat = $6D730002; {Microsoft ADPCM - ACM code 2}
	kDVIIntelIMAFormat = $6D730011; {DVI/Intel IMA ADPCM - ACM code 17}
	kMicrosoftGSMCompression = $6D730031; {Microsoft GSM 6.10 - ACM code 49}
	kDVAudioFormat = FOUR_CHAR_CODE('dvca'); {DV Audio}
	kQDesignCompression = FOUR_CHAR_CODE('QDMC'); {QDesign music}
	kQDesign2Compression = FOUR_CHAR_CODE('QDM2'); {QDesign2 music}
	kQUALCOMMCompression = FOUR_CHAR_CODE('Qclp'); {QUALCOMM PureVoice}
	kOffsetBinary = k8BitOffsetBinaryFormat; {for compatibility}
	kTwosComplement = k16BitBigEndianFormat; {for compatibility}
	kLittleEndianFormat = k16BitLittleEndianFormat; {for compatibility}
	kMPEGLayer3Format = $6D730055; {MPEG Layer 3, CBR only (pre QT4.1)}
	kFullMPEGLay3Format = FOUR_CHAR_CODE('.mp3'); {MPEG Layer 3, CBR & VBR (QT4.1 and later)}
	kVariableDurationDVAudioFormat = FOUR_CHAR_CODE('vdva'); {Variable Duration DV Audio}
	kMPEG4AudioFormat = FOUR_CHAR_CODE('mp4a');

{$ifc TARGET_RT_LITTLE_ENDIAN}
const
	k16BitNativeEndianFormat = k16BitLittleEndianFormat;
	k16BitNonNativeEndianFormat = k16BitBigEndianFormat;

{$elsec}
const
	k16BitNativeEndianFormat = k16BitBigEndianFormat;
	k16BitNonNativeEndianFormat = k16BitLittleEndianFormat;

{$endc} {TARGET_RT_LITTLE_ENDIAN}

{Features Flags}
const
	k8BitRawIn = 1 shl 0; {data description}
	k8BitTwosIn = 1 shl 1;
	k16BitIn = 1 shl 2;
	kStereoIn = 1 shl 3;
	k8BitRawOut = 1 shl 8;
	k8BitTwosOut = 1 shl 9;
	k16BitOut = 1 shl 10;
	kStereoOut = 1 shl 11;
	kReverse = 1 shl 16; {  function description}
	kRateConvert = 1 shl 17;
	kCreateSoundSource = 1 shl 18;
	kVMAwareness = 1 shl 21; { component will hold its memory}
	kHighQuality = 1 shl 22; {  performance description}
	kNonRealTime = 1 shl 23;

{'snfo' Resource Feature Flags}
const
	kSoundCodecInfoFixedCompression = 1 shl 0; { has fixed compression format}
	kSoundCodecInfoVariableCompression = 1 shl 1; { has variable compression format}
	kSoundCodecInfoHasRestrictedInputRates = 1 shl 2; { compressor has restricted set of input sample rates}
	kSoundCodecInfoCanChangeOutputRate = 1 shl 3; { compressor may output a different sample rate than it receives}
	kSoundCodecInfoRequiresExternalFraming = 1 shl 4; { format requires external framing information during decode/encode}
	kSoundCodecInfoVariableDuration = 1 shl 5; { audio packets can vary in duration}

{SoundComponentPlaySourceBuffer action flags}
const
	kSourcePaused = 1 shl 0;
	kPassThrough = 1 shl 16;
	kNoSoundComponentChain = 1 shl 17;

{SoundParamBlock flags, usefull for OpenMixerSoundComponent}
const
	kNoMixing = 1 shl 0; {don't mix source}
	kNoSampleRateConversion = 1 shl 1; {don't convert sample rate (i.e. 11 kHz -> 22 kHz)}
	kNoSampleSizeConversion = 1 shl 2; {don't convert sample size (i.e. 16 -> 8)}
	kNoSampleFormatConversion = 1 shl 3; {don't convert sample format (i.e. 'twos' -> 'raw ')}
	kNoChannelConversion = 1 shl 4; {don't convert stereo/mono}
	kNoDecompression = 1 shl 5; {don't decompress (i.e. 'MAC3' -> 'raw ')}
	kNoVolumeConversion = 1 shl 6; {don't apply volume}
	kNoRealtimeProcessing = 1 shl 7; {won't run at interrupt time}
	kScheduledSource = 1 shl 8; {source is scheduled}
	kNonInterleavedBuffer = 1 shl 9; {buffer is not interleaved samples}
	kNonPagingMixer = 1 shl 10; {if VM is on, use the non-paging mixer}
	kSoundConverterMixer = 1 shl 11; {the mixer is to be used by the SoundConverter}
	kPagingMixer = 1 shl 12; {the mixer is to be used as a paging mixer when VM is on}
	kVMAwareMixer = 1 shl 13; {passed to the output device when the SM is going to deal with VM safety}
	kExtendedSoundData = 1 shl 14; {SoundComponentData record is actually an ExtendedSoundComponentData}

{SoundParamBlock quality settings}
const
	kBestQuality = 1 shl 0; {use interpolation in rate conversion}

{useful bit masks}
const
	kInputMask = $000000FF; {masks off input bits}
	kOutputMask = $0000FF00; {masks off output bits}
	kOutputShift = 8;    {amount output bits are shifted}
	kActionMask = $00FF0000; {masks off action bits}
	kSoundComponentBits = $00FFFFFF;

{audio atom types}
const
	kAudioFormatAtomType = FOUR_CHAR_CODE('frma');
	kAudioEndianAtomType = FOUR_CHAR_CODE('enda');
	kAudioVBRAtomType = FOUR_CHAR_CODE('vbra');
	kAudioTerminatorAtomType = 0;

{siAVDisplayBehavior types}
const
	kAVDisplayHeadphoneRemove = 0;    { monitor does not have a headphone attached}
	kAVDisplayHeadphoneInsert = 1;    { monitor has a headphone attached}
	kAVDisplayPlainTalkRemove = 2;    { monitor either sending no input through CPU input port or unable to tell if input is coming in}
	kAVDisplayPlainTalkInsert = 3;     { monitor sending PlainTalk level microphone source input through sound input port}

{Audio Component constants}
const
{Values for whichChannel parameter}
	audioAllChannels = 0;    {All channels (usually interpreted as both left and right)}
	audioLeftChannel = 1;    {Left channel}
	audioRightChannel = 2;    {Right channel}
                                        {Values for mute parameter}
	audioUnmuted = 0;    {Device is unmuted}
	audioMuted = 1;    {Device is muted}
                                        {Capabilities flags definitions}
	audioDoesMono = 1 shl 0; {Device supports mono output}
	audioDoesStereo = 1 shl 1; {Device supports stereo output}
	audioDoesIndependentChannels = 1 shl 2; {Device supports independent software control of each channel}

{Sound Input Qualities}
const
	siCDQuality = FOUR_CHAR_CODE('cd  '); {44.1kHz, stereo, 16 bit}
	siBestQuality = FOUR_CHAR_CODE('best'); {22kHz, mono, 8 bit}
	siBetterQuality = FOUR_CHAR_CODE('betr'); {22kHz, mono, MACE 3:1}
	siGoodQuality = FOUR_CHAR_CODE('good'); {22kHz, mono, MACE 6:1}
	siNoneQuality = FOUR_CHAR_CODE('none'); {settings don't match any quality for a get call}

const
	siDeviceIsConnected = 1;    {input device is connected and ready for input}
	siDeviceNotConnected = 0;    {input device is not connected}
	siDontKnowIfConnected = -1;   {can't tell if input device is connected}
	siReadPermission = 0;    {permission passed to SPBOpenDevice}
	siWritePermission = 1;     {permission passed to SPBOpenDevice}

{flags that SoundConverterFillBuffer will return}
const
	kSoundConverterDidntFillBuffer = 1 shl 0; {set if the converter couldn't completely satisfy a SoundConverterFillBuffer request}
	kSoundConverterHasLeftOverData = 1 shl 1; {set if the converter had left over data after completely satisfying a SoundConverterFillBuffer call}

{ flags for extendedFlags fields of ExtendedSoundComponentData, ExtendedSoundParamBlock, and ExtendedScheduledSoundHeader}
const
	kExtendedSoundSampleCountNotValid = 1 shl 0; { set if sampleCount of SoundComponentData isn't meaningful; use buffer size instead}
	kExtendedSoundBufferSizeValid = 1 shl 1; { set if bufferSize field is valid}
	kExtendedSoundFrameSizesValid = 1 shl 2; { set if frameSizesArray is valid (will be nil if all sizes are common and kExtendedSoundCommonFrameSizeValid is set}
	kExtendedSoundCommonFrameSizeValid = 1 shl 3; { set if all audio frames have the same size and the commonFrameSize field is valid}
	kExtendedSoundExtensionsValid = 1 shl 4; { set if pointer to extensions array is valid}
	kExtendedSoundBufferFlagsValid = 1 shl 5; { set if buffer flags field is valid}

{ flags passed in bufferFlags/bufferFlagsMask extended fields if kExtendedSoundBufferFlagsValid extended flag is set}
const
	kExtendedSoundBufferIsDiscontinuous = 1 shl 0; { buffer is discontinuous with previous buffer}
	kExtendedSoundBufferIsFirstBuffer = 1 shl 1; { buffer is first buffer}

{
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   typedefs
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}

type
	SndCommandPtr = ^SndCommand;
	SndCommand = record
		cmd: UInt16;
		param1: SInt16;
		param2: SIGNEDLONG;
	end;
type
	SndChannelPtr = ^SndChannel;
	SndCallBackProcPtr = procedure( chan: SndChannelPtr; var cmd: SndCommand );
{GPC-ONLY-START}
	SndCallBackUPP = UniversalProcPtr; // should be SndCallBackProcPtr
{GPC-ONLY-ELSE}
	SndCallBackUPP = SndCallBackProcPtr;
{GPC-ONLY-FINISH}
	SndChannel = record
		nextChan: SndChannelPtr;
		firstMod: Ptr;               { reserved for the Sound Manager }
		callBack: SndCallBackUPP;
		userInfo: SIGNEDLONG;
		wait: SIGNEDLONG;                   { The following is for internal Sound Manager use only.}
		cmdInProgress: SndCommand;
		flags: SInt16;
		qLength: SInt16;
		qHead: SInt16;
		qTail: SInt16;
		queue: array [0..127] of SndCommand;
	end;

{
 *  NewSndCallBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSndCallBackUPP( userRoutine: SndCallBackProcPtr ): SndCallBackUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  DisposeSndCallBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSndCallBackUPP( userUPP: SndCallBackUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  InvokeSndCallBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSndCallBackUPP( chan: SndChannelPtr; var cmd: SndCommand; userUPP: SndCallBackUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{MACE structures}
type
	StateBlock = record
		stateVar: array [0..63] of SInt16;
	end;
	StateBlockPtr = ^StateBlock;
type
	LeftOverBlock = record
		count: UNSIGNEDLONG;
		sampleArea: array [0..31] of SInt8;
	end;
	LeftOverBlockPtr = ^LeftOverBlock;
type
	ModRef = record
		modNumber: UInt16;
		modInit: SIGNEDLONG;
	end;
type
	SndListResourcePtr = ^SndListResource;
	SndListResource = record
		format: SInt16;
		numModifiers: SInt16;
		modifierPart: array [0..0] of ModRef;
		numCommands: SInt16;
		commandPart: array [0..0] of SndCommand;
		dataPart: UInt8;
	end;
type
	SndListPtr = SndListResourcePtr;
	SndListHandle = ^SndListPtr;
	SndListHndl = SndListHandle;
{HyperCard sound resource format}
type
	Snd2ListResourcePtr = ^Snd2ListResource;
	Snd2ListResource = record
		format: SInt16;
		refCount: SInt16;
		numCommands: SInt16;
		commandPart: array [0..0] of SndCommand;
		dataPart: UInt8;
	end;
type
	Snd2ListPtr = Snd2ListResourcePtr;
	Snd2ListHandle = ^Snd2ListPtr;
	Snd2ListHndl = Snd2ListHandle;
	SoundHeader = record
		samplePtr: Ptr;              {if NIL then samples are in sampleArea}
		length: UNSIGNEDLONG;                 {length of sound in bytes}
		sampleRate: UnsignedFixed;             {sample rate for this sound}
		loopStart: UNSIGNEDLONG;              {start of looping portion}
		loopEnd: UNSIGNEDLONG;                {end of looping portion}
		encode: UInt8;                 {header encoding}
		baseFrequency: UInt8;          {baseFrequency value}
		sampleArea: array [0..0] of UInt8;          {space for when samples follow directly}
{GPC-FPC-ONLY-START}
		pad: UInt8;
{GPC-FPC-ONLY-FINISH}
	end;
	SoundHeaderPtr = ^SoundHeader;
type
	CmpSoundHeader = record
		samplePtr: Ptr;              {if nil then samples are in sample area}
		numChannels: UNSIGNEDLONG;            {number of channels i.e. mono = 1}
		sampleRate: UnsignedFixed;             {sample rate in Apples Fixed point representation}
		loopStart: UNSIGNEDLONG;              {loopStart of sound before compression}
		loopEnd: UNSIGNEDLONG;                {loopEnd of sound before compression}
		encode: UInt8;                 {data structure used , stdSH, extSH, or cmpSH}
		baseFrequency: UInt8;          {same meaning as regular SoundHeader}
		numFrames: UNSIGNEDLONG;              {length in frames ( packetFrames or sampleFrames )}
		AIFFSampleRate: extended80;         {IEEE sample rate}
		markerChunk: Ptr;            {sync track}
		format: OSType;                 {data format type, was futureUse1}
		futureUse2: UNSIGNEDLONG;             {reserved by Apple}
		stateVars: StateBlockPtr;              {pointer to State Block}
		leftOverSamples: LeftOverBlockPtr;        {used to save truncated samples between compression calls}
		compressionID: SInt16;          {0 means no compression, non zero means compressionID}
		packetSize: UInt16;             {number of bits in compressed sample packet}
		snthID: UInt16;                 {resource ID of Sound Manager snth that contains NRT C/E}
		sampleSize: UInt16;             {number of bits in non-compressed sample}
		sampleArea: array [0..0] of UInt8;          {space for when samples follow directly}
{GPC-FPC-ONLY-START}
		pad: UInt8;
{GPC-FPC-ONLY-FINISH}
	end;
	CmpSoundHeaderPtr = ^CmpSoundHeader;
type
	ExtSoundHeader = record
		samplePtr: Ptr;              {if nil then samples are in sample area}
		numChannels: UNSIGNEDLONG;            {number of channels,  ie mono = 1}
		sampleRate: UnsignedFixed;             {sample rate in Apples Fixed point representation}
		loopStart: UNSIGNEDLONG;              {same meaning as regular SoundHeader}
		loopEnd: UNSIGNEDLONG;                {same meaning as regular SoundHeader}
		encode: UInt8;                 {data structure used , stdSH, extSH, or cmpSH}
		baseFrequency: UInt8;          {same meaning as regular SoundHeader}
		numFrames: UNSIGNEDLONG;              {length in total number of frames}
		AIFFSampleRate: extended80;         {IEEE sample rate}
		markerChunk: Ptr;            {sync track}
		instrumentChunks: Ptr;       {AIFF instrument chunks}
		AESRecording: Ptr;
		sampleSize: UInt16;             {number of bits in sample}
		futureUse1: UInt16;             {reserved by Apple}
		futureUse2: UNSIGNEDLONG;             {reserved by Apple}
		futureUse3: UNSIGNEDLONG;             {reserved by Apple}
		futureUse4: UNSIGNEDLONG;             {reserved by Apple}
		sampleArea: array [0..0] of UInt8;          {space for when samples follow directly}
{GPC-FPC-ONLY-START}
		pad:					UInt8;
{GPC-FPC-ONLY-FINISH}
	end;
	ExtSoundHeaderPtr = ^ExtSoundHeader;
type
	SoundHeaderUnionPtr = ^SoundHeaderUnion;
	SoundHeaderUnion = record
		case SInt16 of
		0: (
			stdHeader:			SoundHeader;
			);
		1: (
			cmpHeader:			CmpSoundHeader;
			);
		2: (
			extHeader:			ExtSoundHeader;
			);
	end;

	ConversionBlock = record
		destination: SInt16;
		unused: SInt16;
		inputPtr: CmpSoundHeaderPtr;
		outputPtr: CmpSoundHeaderPtr;
	end;
	ConversionBlockPtr = ^ConversionBlock;
{ ScheduledSoundHeader flags}
const
	kScheduledSoundDoScheduled = 1 shl 0;
	kScheduledSoundDoCallBack = 1 shl 1;
	kScheduledSoundExtendedHdr = 1 shl 2;

type
	ScheduledSoundHeader = record
		u: SoundHeaderUnion;
		flags: SIGNEDLONG;
		reserved: SInt16;
		callBackParam1: SInt16;
		callBackParam2: SIGNEDLONG;
		startTime: TimeRecord;
	end;
	ScheduledSoundHeaderPtr = ^ScheduledSoundHeader;
type
	ExtendedScheduledSoundHeader = record
		u: SoundHeaderUnion;
		flags: SIGNEDLONG;
		reserved: SInt16;
		callBackParam1: SInt16;
		callBackParam2: SIGNEDLONG;
		startTime: TimeRecord;
		recordSize: SIGNEDLONG;
		extendedFlags: SIGNEDLONG;
		bufferSize: SIGNEDLONG;
		frameCount: SIGNEDLONG;             { number of audio frames}
		frameSizesArray: SIGNEDLONGPtr;        { pointer to array of longs with frame sizes in bytes}
		commonFrameSize: SIGNEDLONG;        { size of each frame if common}
		extensionsPtr: UnivPtr;          {pointer to set of classic atoms (size,type,data,...)}
		extensionsSize: SIGNEDLONG;         {size of extensions data (extensionsPtr)}
		bufferFlags: UNSIGNEDLONG;            {set or cleared flags}
		bufferFlagsMask: UNSIGNEDLONG;        {which flags are valid}
	end;
	ExtendedScheduledSoundHeaderPtr = ^ExtendedScheduledSoundHeader;
type
	SMStatus = record
		smMaxCPULoad: SInt16;
		smNumChannels: SInt16;
		smCurCPULoad: SInt16;
	end;
	SMStatusPtr = ^SMStatus;
type
	SCStatus = record
		scStartTime: UnsignedFixed;
		scEndTime: UnsignedFixed;
		scCurrentTime: UnsignedFixed;
		scChannelBusy: Boolean;
		scChannelDisposed: Boolean;
		scChannelPaused: Boolean;
		scUnused: Boolean;
		scChannelAttributes: UNSIGNEDLONG;
		scCPULoad: SIGNEDLONG;
	end;
	SCStatusPtr = ^SCStatus;
type
	AudioSelection = record
		unitType: SIGNEDLONG;
		selStart: UnsignedFixed;
		selEnd: UnsignedFixed;
	end;
	AudioSelectionPtr = ^AudioSelection;
{$ifc CALL_NOT_IN_CARBON}
type
	SndDoubleBuffer = record
		dbNumFrames: SIGNEDLONG;
		dbFlags: SIGNEDLONG;
		dbUserInfo: array [0..1] of SInt32;
		dbSoundData: array [0..0] of SInt8;
	end;
	SndDoubleBufferPtr = ^SndDoubleBuffer;


type
	SndDoubleBackProcPtr = procedure( channel: SndChannelPtr; doubleBufferPtr: SndDoubleBufferPtr );
{GPC-ONLY-START}
	SndDoubleBackUPP = UniversalProcPtr; // should be SndDoubleBackProcPtr
{GPC-ONLY-ELSE}
	SndDoubleBackUPP = SndDoubleBackProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewSndDoubleBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
 *  DisposeSndDoubleBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
 *  InvokeSndDoubleBackUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

type
	SndDoubleBufferHeader = record
		dbhNumChannels: SInt16;
		dbhSampleSize: SInt16;
		dbhCompressionID: SInt16;
		dbhPacketSize: SInt16;
		dbhSampleRate: UnsignedFixed;
		dbhBufferPtr: array [0..1] of SndDoubleBufferPtr;
		dbhDoubleBack: SndDoubleBackUPP;
	end;
	SndDoubleBufferHeaderPtr = ^SndDoubleBufferHeader;
type
	SndDoubleBufferHeader2 = record
		dbhNumChannels: SInt16;
		dbhSampleSize: SInt16;
		dbhCompressionID: SInt16;
		dbhPacketSize: SInt16;
		dbhSampleRate: UnsignedFixed;
		dbhBufferPtr:			array [0..1] of SndDoubleBufferPtr;
		dbhDoubleBack: SndDoubleBackUPP;
		dbhFormat: OSType;
	end;
	SndDoubleBufferHeader2Ptr = ^SndDoubleBufferHeader2;
{$endc} {CALL_NOT_IN_CARBON}

type
	SoundInfoList = record
		count: SInt16;
		infoHandle: Handle;
	end;
	SoundInfoListPtr = ^SoundInfoList;
type
	SoundComponentData = record
		flags: SIGNEDLONG;
		format: OSType;
		numChannels: SInt16;
		sampleSize: SInt16;
		sampleRate: UnsignedFixed;
		sampleCount: SIGNEDLONG;
		buffer: BytePtr;
		reserved: SIGNEDLONG;
	end;
	SoundComponentDataPtr = ^SoundComponentData;
type
	ExtendedSoundComponentData = record
		desc: SoundComponentData;                   {description of sound buffer}
		recordSize: SIGNEDLONG;             {size of this record in bytes}
		extendedFlags: SIGNEDLONG;          {flags for extended record}
		bufferSize: SIGNEDLONG;             {size of buffer in bytes}
		frameCount: SIGNEDLONG;             {number of audio frames}
		frameSizesArray: SIGNEDLONGPtr;        {pointer to array of longs with frame sizes in bytes}
		commonFrameSize: SIGNEDLONG;        {size of each frame if common}
		extensionsPtr: UnivPtr;          {pointer to set of classic atoms (size,type,data,...)}
		extensionsSize: SIGNEDLONG;         {size of extensions data (extensionsPtr)}
		bufferFlags: UNSIGNEDLONG;            {set or cleared flags}
		bufferFlagsMask: UNSIGNEDLONG;        {which flags are valid}
	end;
	ExtendedSoundComponentDataPtr = ^ExtendedSoundComponentData;
type
	SoundParamBlockPtr = ^SoundParamBlock;
	SoundParamProcPtr = function( var pb: SoundParamBlockPtr ): Boolean;
{GPC-ONLY-START}
	SoundParamUPP = UniversalProcPtr; // should be SoundParamProcPtr
{GPC-ONLY-ELSE}
	SoundParamUPP = SoundParamProcPtr;
{GPC-ONLY-FINISH}
	SoundParamBlock = record
		recordSize: SIGNEDLONG;             {size of this record in bytes}
		desc: SoundComponentData;                   {description of sound buffer}
		rateMultiplier: UnsignedFixed;         {rate multiplier to apply to sound}
		leftVolume: SInt16;             {volumes to apply to sound}
		rightVolume: SInt16;
		quality: SIGNEDLONG;                {quality to apply to sound}
		filter: ComponentInstance;                 {filter to apply to sound}
		moreRtn: SoundParamUPP;                {routine to call to get more data}
		completionRtn: SoundParamUPP;          {routine to call when buffer is complete}
		refCon: SIGNEDLONG;                 {user refcon}
		result: SInt16;                 {result}
	end;

type
	ExtendedSoundParamBlock = record
		pb: SoundParamBlock;                     {classic SoundParamBlock except recordSize == sizeof(ExtendedSoundParamBlock)}
		reserved: SInt16;
		extendedFlags: SIGNEDLONG;          {flags}
		bufferSize: SIGNEDLONG;             {size of buffer in bytes}
		frameCount: SIGNEDLONG;             {number of audio frames}
		frameSizesArray: SIGNEDLONGPtr;        {pointer to array of longs with frame sizes in bytes}
		commonFrameSize: SIGNEDLONG;        {size of each frame if common}
		extensionsPtr: UnivPtr;          {pointer to set of classic atoms (size,type,data,...)}
		extensionsSize: SIGNEDLONG;         {size of extensions data (extensionsPtr)}
		bufferFlags: UNSIGNEDLONG;            {set or cleared flags}
		bufferFlagsMask: UNSIGNEDLONG;        {which flags are valid}
	end;
	ExtendedSoundParamBlockPtr = ^ExtendedSoundParamBlock;
type
	CompressionInfo = record
		recordSize: SIGNEDLONG;
		format: OSType;
		compressionID: SInt16;
		samplesPerPacket: UInt16;
		bytesPerPacket: UInt16;
		bytesPerFrame: UInt16;
		bytesPerSample: UInt16;
		futureUse1: UInt16;
	end;
	CompressionInfoPtr = ^CompressionInfo;
type
	CompressionInfoHandle = ^CompressionInfoPtr;
{variables for floating point conversion}
type
	SoundSlopeAndInterceptRecordPtr = ^SoundSlopeAndInterceptRecord;
	SoundSlopeAndInterceptRecord = record
		slope: Float64;
		intercept: Float64;
		minClip: Float64;
		maxClip: Float64;
	end;
type
	SoundSlopeAndInterceptPtr = SoundSlopeAndInterceptRecordPtr;
{private thing to use as a reference to a Sound Converter}
type
	SoundConverter = ^OpaqueSoundConverter; { an opaque type }
	OpaqueSoundConverter = record end;
	SoundConverterPtr = ^SoundConverter;  { when a var xx:SoundConverter parameter can be nil, it is changed to xx: SoundConverterPtr }
{callback routine to provide data to the Sound Converter}
type
	SoundConverterFillBufferDataProcPtr = function( var data: SoundComponentDataPtr; refCon: univ Ptr ): Boolean;
{GPC-ONLY-START}
	SoundConverterFillBufferDataUPP = UniversalProcPtr; // should be SoundConverterFillBufferDataProcPtr
{GPC-ONLY-ELSE}
	SoundConverterFillBufferDataUPP = SoundConverterFillBufferDataProcPtr;
{GPC-ONLY-FINISH}
{private thing to use as a reference to a Sound Source}
type
	SoundSource = ^OpaqueSoundSource; { an opaque type }
	OpaqueSoundSource = record end;
	SoundSourcePtr = ^SoundSource;


type
	SoundComponentLink = record
		description: ComponentDescription;          {Describes the sound component}
		mixerID: SoundSource;                {Reserved by Apple}
		linkID: SoundSourcePtr;                 {Reserved by Apple}
	end;
	SoundComponentLinkPtr = ^SoundComponentLink;
type
	AudioInfo = record
		capabilitiesFlags: SIGNEDLONG;      {Describes device capabilities}
		reserved: SIGNEDLONG;               {Reserved by Apple}
		numVolumeSteps: UInt16;         {Number of significant increments between min and max volume}
	end;
	AudioInfoPtr = ^AudioInfo;
type
	AudioFormatAtom = record
		size: SIGNEDLONG;                   { = sizeof(AudioFormatAtom)}
		atomType: OSType;               { = kAudioFormatAtomType}
		format: OSType;
	end;
	AudioFormatAtomPtr = ^AudioFormatAtom;
type
	AudioEndianAtom = record
		size: SIGNEDLONG;                   { = sizeof(AudioEndianAtom)}
		atomType: OSType;               { = kAudioEndianAtomType}
		littleEndian: SInt16;
	end;
	AudioEndianAtomPtr = ^AudioEndianAtom;
type
	AudioTerminatorAtom = record
		size: SIGNEDLONG;                   { = sizeof(AudioTerminatorAtom)}
		atomType: OSType;               { = kAudioTerminatorAtomType}
	end;
	AudioTerminatorAtomPtr = ^AudioTerminatorAtom;
type
	LevelMeterInfo = record
		numChannels: SInt16;            { mono or stereo source}
		leftMeter: UInt8;              { 0-255 range}
		rightMeter: UInt8;             { 0-255 range}
	end;
	LevelMeterInfoPtr = ^LevelMeterInfo;
type
	EQSpectrumBandsRecord = record
		count: SInt16;
		frequency: UnsignedFixedPtr;              { pointer to array of frequencies}
	end;
	EQSpectrumBandsRecordPtr = ^EQSpectrumBandsRecord;
const
	kSoundAudioCodecPropertyWritableFlag = 1 shl 0;

type
	SoundAudioCodecPropertyRequestParams = record
		propertyClass: UInt32;
		propertyID: UInt32;
		propertyDataSize: UInt32;       { out -- GetPropertyInfo, in/out -- GetProperty, in -- SetProperty}
		propertyData: UnivPtr;           { in -- GetPropertyInfo, GetProperty, SetProperty}
		propertyRequestFlags: UInt32;   { out -- GetPropertyInfo}
		propertyDataType: UInt32;       { out -- GetPropertyInfo, often 0}
		propertyRequestResult: ComponentResult;  { out -- GetPropertyInfo, GetProperty, SetProperty}
	end;


{ Sound Input Structures}
type
	SPBPtr = ^SPB;


{user procedures called by sound input routines}
	SIInterruptProcPtr = procedure( inParamPtr: SPBPtr; dataBuffer: Ptr; peakAmplitude: SInt16; sampleSize: SIGNEDLONG );
	SICompletionProcPtr = procedure( inParamPtr: SPBPtr );
{GPC-ONLY-START}
	SIInterruptUPP = UniversalProcPtr; // should be SIInterruptProcPtr
{GPC-ONLY-ELSE}
	SIInterruptUPP = SIInterruptProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	SICompletionUPP = UniversalProcPtr; // should be SICompletionProcPtr
{GPC-ONLY-ELSE}
	SICompletionUPP = SICompletionProcPtr;
{GPC-ONLY-FINISH}


{Sound Input Parameter Block}
	SPB = record
		inRefNum: SIGNEDLONG;               {reference number of sound input device}
		count: UNSIGNEDLONG;                  {number of bytes to record}
		milliseconds: UNSIGNEDLONG;           {number of milliseconds to record}
		bufferLength: UNSIGNEDLONG;           {length of buffer in bytes}
		bufferPtr: Ptr;              {buffer to store sound data in}
		completionRoutine: SICompletionUPP;      {completion routine}
		interruptRoutine: SIInterruptUPP;       {interrupt routine}
		userLong: SIGNEDLONG;               {user-defined field}
		error: OSErr;                  {error}
		unused1: SIGNEDLONG;                {reserved - must be zero}
	end;

{
 *  NewSoundParamUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSoundParamUPP( userRoutine: SoundParamProcPtr ): SoundParamUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  NewSoundConverterFillBufferDataUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSoundConverterFillBufferDataUPP( userRoutine: SoundConverterFillBufferDataProcPtr ): SoundConverterFillBufferDataUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  NewSIInterruptUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSIInterruptUPP( userRoutine: SIInterruptProcPtr ): SIInterruptUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  NewSICompletionUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewSICompletionUPP( userRoutine: SICompletionProcPtr ): SICompletionUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  DisposeSoundParamUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSoundParamUPP( userUPP: SoundParamUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  DisposeSoundConverterFillBufferDataUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSoundConverterFillBufferDataUPP( userUPP: SoundConverterFillBufferDataUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  DisposeSIInterruptUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSIInterruptUPP( userUPP: SIInterruptUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  DisposeSICompletionUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeSICompletionUPP( userUPP: SICompletionUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  InvokeSoundParamUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeSoundParamUPP( var pb: SoundParamBlockPtr; userUPP: SoundParamUPP ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  InvokeSoundConverterFillBufferDataUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeSoundConverterFillBufferDataUPP( var data: SoundComponentDataPtr; refCon: univ Ptr; userUPP: SoundConverterFillBufferDataUPP ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  InvokeSIInterruptUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSIInterruptUPP( inParamPtr: SPBPtr; dataBuffer: Ptr; peakAmplitude: SInt16; sampleSize: SIGNEDLONG; userUPP: SIInterruptUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  InvokeSICompletionUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeSICompletionUPP( inParamPtr: SPBPtr; userUPP: SICompletionUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

type
	FilePlayCompletionProcPtr = procedure( chan: SndChannelPtr );
{GPC-ONLY-START}
	FilePlayCompletionUPP = UniversalProcPtr; // should be FilePlayCompletionProcPtr
{GPC-ONLY-ELSE}
	FilePlayCompletionUPP = FilePlayCompletionProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewFilePlayCompletionUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
 *  DisposeFilePlayCompletionUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
 *  InvokeFilePlayCompletionUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   prototypes
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}


{ Sound Manager routines }
{$ifc not TARGET_CPU_64}
{
 *  SysBeep()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use AudioServicesPlayAlertSound(). Found in
 *    AudioToolbox/AudioServices.h
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure SysBeep( duration: SInt16 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndDoCommand()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndDoCommand( chan: SndChannelPtr; const var cmd: SndCommand; noWait: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndDoImmediate()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndDoImmediate( chan: SndChannelPtr; const var cmd: SndCommand ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndNewChannel()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndNewChannel( var chan: SndChannelPtr; synth: SInt16; init: SIGNEDLONG; userRoutine: SndCallBackUPP ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndDisposeChannel()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndDisposeChannel( chan: SndChannelPtr; quietNow: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndPlay()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndPlay( chan: SndChannelPtr; sndHandle: SndListHandle; async: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}

{$ifc OLDROUTINENAMES}
{
 *  SndAddModifier()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{$endc} {OLDROUTINENAMES}

{
 *  SndControl()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{ Sound Manager 2.0 and later, uses _SoundDispatch }
{$ifc not TARGET_CPU_64}
{
 *  SndSoundManagerVersion()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndSoundManagerVersion: NumVersion;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}

{
 *  SndStartFilePlay()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  SndPauseFilePlay()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  SndStopFilePlay()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{$ifc not TARGET_CPU_64}
{
 *  SndChannelStatus()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndChannelStatus( chan: SndChannelPtr; theLength: SInt16; theStatus: SCStatusPtr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndManagerStatus()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndManagerStatus( theLength: SInt16; theStatus: SMStatusPtr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndGetSysBeepState()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    SysBeep related APIs have been replaced by AudioServices. Found
 *    in AudioToolbox/AudioServices.h
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure SndGetSysBeepState( var sysBeepState: SInt16 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndSetSysBeepState()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    SysBeep related APIs have been replaced by AudioServices. Found
 *    in AudioToolbox/AudioServices.h
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndSetSysBeepState( sysBeepState: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}

{
 *  SndPlayDoubleBuffer()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{ MACE compression routines, uses _SoundDispatch }
{
 *  MACEVersion()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  Comp3to1()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  Exp1to3()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  Comp6to1()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  Exp1to6()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{ Sound Manager 3.0 and later calls, uses _SoundDispatch }
{$ifc not TARGET_CPU_64}
{
 *  GetSysBeepVolume()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetSysBeepVolume( var level: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SetSysBeepVolume()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SetSysBeepVolume( level: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  GetDefaultOutputVolume()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetDefaultOutputVolume( var level: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SetDefaultOutputVolume()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SetDefaultOutputVolume( level: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  GetSoundHeaderOffset()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetSoundHeaderOffset( sndHandle: SndListHandle; var offset: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}

{
 *  UnsignedFixedMulDiv()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
(*
function UnsignedFixedMulDiv( value: UnsignedFixed; multiplier: UnsignedFixed; divisor: UnsignedFixed ): UnsignedFixed;

Duplicated in FixMath.h, also strange that it's included here without availability info.
*)

{$ifc not TARGET_CPU_64}
{
 *  GetCompressionInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function GetCompressionInfo( compressionID: SInt16; format: OSType; numChannels: SInt16; sampleSize: SInt16; cp: CompressionInfoPtr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SetSoundPreference()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SetSoundPreference( theType: OSType; var name: Str255; settings: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  GetSoundPreference()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function GetSoundPreference( theType: OSType; var name: Str255; settings: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  OpenMixerSoundComponent()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function OpenMixerSoundComponent( outputDescription: SoundComponentDataPtr; outputFlags: SIGNEDLONG; var mixerComponent: ComponentInstance ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  CloseMixerSoundComponent()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function CloseMixerSoundComponent( ci: ComponentInstance ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{ Sound Manager 3.1 and later calls, uses _SoundDispatch }
{
 *  SndGetInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.1 and later
 }
function SndGetInfo( chan: SndChannelPtr; selector: OSType; infoPtr: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndSetInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.1 and later
 }
function SndSetInfo( chan: SndChannelPtr; selector: OSType; infoPtr: {const} univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  GetSoundOutputInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.1 and later
 }
function GetSoundOutputInfo( outputDevice: Component; selector: OSType; infoPtr: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SetSoundOutputInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.1 and later
 }
function SetSoundOutputInfo( outputDevice: Component; selector: OSType; infoPtr: {const} univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{ Sound Manager 3.2 and later calls, uses _SoundDispatch }
{
 *  GetCompressionName()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.2 and later
 }
function GetCompressionName( compressionType: OSType; var compressionName: Str255 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundConverterOpen()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.2 and later
 }
function SoundConverterOpen( const var inputFormat: SoundComponentData; const var outputFormat: SoundComponentData; var sc: SoundConverter ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundConverterClose()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.2 and later
 }
function SoundConverterClose( sc: SoundConverter ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundConverterGetBufferSizes()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.2 and later
 }
function SoundConverterGetBufferSizes( sc: SoundConverter; inputBytesTarget: UNSIGNEDLONG; var inputFrames: UNSIGNEDLONG; var inputBytes: UNSIGNEDLONG; var outputBytes: UNSIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundConverterBeginConversion()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.2 and later
 }
function SoundConverterBeginConversion( sc: SoundConverter ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundConverterConvertBuffer()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.2 and later
 }
function SoundConverterConvertBuffer( sc: SoundConverter; inputPtr: {const} univ Ptr; inputFrames: UNSIGNEDLONG; outputPtr: univ Ptr; var outputFrames: UNSIGNEDLONG; var outputBytes: UNSIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundConverterEndConversion()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.2 and later
 }
function SoundConverterEndConversion( sc: SoundConverter; outputPtr: univ Ptr; var outputFrames: UNSIGNEDLONG; var outputBytes: UNSIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{ Sound Manager 3.3 and later calls, uses _SoundDispatch }
{
 *  SoundConverterGetInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.3 and later
 }
function SoundConverterGetInfo( sc: SoundConverter; selector: OSType; infoPtr: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundConverterSetInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.3 and later
 }
function SoundConverterSetInfo( sc: SoundConverter; selector: OSType; infoPtr: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{ Sound Manager 3.6 and later calls, uses _SoundDispatch }
{
 *  SoundConverterFillBuffer()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in SoundLib 3.6 and later
 }
function SoundConverterFillBuffer( sc: SoundConverter; fillBufferDataUPP: SoundConverterFillBufferDataUPP; fillBufferDataRefCon: univ Ptr; outputBuffer: univ Ptr; outputBufferByteSize: UNSIGNEDLONG; var bytesWritten: UNSIGNEDLONG; var framesWritten: UNSIGNEDLONG; var outputFlags: UNSIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundManagerGetInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in SoundLib 3.6 and later
 }
function SoundManagerGetInfo( selector: OSType; infoPtr: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundManagerSetInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in SoundLib 3.6 and later
 }
function SoundManagerSetInfo( selector: OSType; infoPtr: {const} univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
  Sound Component Functions
   basic sound component functions
}

{
 *  SoundComponentInitOutputDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentInitOutputDevice( ti: ComponentInstance; actions: SIGNEDLONG ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundComponentSetSource()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentSetSource( ti: ComponentInstance; sourceID: SoundSource; source: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundComponentGetSource()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentGetSource( ti: ComponentInstance; sourceID: SoundSource; var source: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundComponentGetSourceData()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentGetSourceData( ti: ComponentInstance; var sourceData: SoundComponentDataPtr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundComponentSetOutput()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentSetOutput( ti: ComponentInstance; requested: SoundComponentDataPtr; var actual: SoundComponentDataPtr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{ junction methods for the mixer, must be called at non-interrupt level}
{
 *  SoundComponentAddSource()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentAddSource( ti: ComponentInstance; var sourceID: SoundSource ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundComponentRemoveSource()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentRemoveSource( ti: ComponentInstance; sourceID: SoundSource ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{ info methods}
{
 *  SoundComponentGetInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentGetInfo( ti: ComponentInstance; sourceID: SoundSource; selector: OSType; infoPtr: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundComponentSetInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentSetInfo( ti: ComponentInstance; sourceID: SoundSource; selector: OSType; infoPtr: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{ control methods}
{
 *  SoundComponentStartSource()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentStartSource( ti: ComponentInstance; count: SInt16; var sources: SoundSource ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundComponentStopSource()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentStopSource( ti: ComponentInstance; count: SInt16; var sources: SoundSource ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundComponentPauseSource()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentPauseSource( ti: ComponentInstance; count: SInt16; var sources: SoundSource ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SoundComponentPlaySourceBuffer()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function SoundComponentPlaySourceBuffer( ti: ComponentInstance; sourceID: SoundSource; pb: SoundParamBlockPtr; actions: SIGNEDLONG ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}


{ selectors for component calls }
const
	kSoundComponentInitOutputDeviceSelect = $0001;
	kSoundComponentSetSourceSelect = $0002;
	kSoundComponentGetSourceSelect = $0003;
	kSoundComponentGetSourceDataSelect = $0004;
	kSoundComponentSetOutputSelect = $0005;
	kSoundComponentAddSourceSelect = $0101;
	kSoundComponentRemoveSourceSelect = $0102;
	kSoundComponentGetInfoSelect = $0103;
	kSoundComponentSetInfoSelect = $0104;
	kSoundComponentStartSourceSelect = $0105;
	kSoundComponentStopSourceSelect = $0106;
	kSoundComponentPauseSourceSelect = $0107;
	kSoundComponentPlaySourceBufferSelect = $0108;
{Audio Components}
{Volume is described as a value between 0 and 1, with 0 indicating minimum
  volume and 1 indicating maximum volume; if the device doesn't support
  software control of volume, then a value of unimpErr is returned, indicating
  that these functions are not supported by the device
}
{
 *  AudioGetVolume()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{
 *  AudioSetVolume()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{If the device doesn't support software control of mute, then a value of unimpErr is
returned, indicating that these functions are not supported by the device.}
{
 *  AudioGetMute()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{
 *  AudioSetMute()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{AudioSetToDefaults causes the associated device to reset its volume and mute values
(and perhaps other characteristics, e.g. attenuation) to "factory default" settings}
{
 *  AudioSetToDefaults()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{This routine is required; it must be implemented by all audio components}

{
 *  AudioGetInfo()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{
 *  AudioGetBass()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{
 *  AudioSetBass()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{
 *  AudioGetTreble()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{
 *  AudioSetTreble()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{
 *  AudioGetOutputDevice()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


{This is routine is private to the AudioVision component.  It enables the watching of the mute key.}
{
 *  AudioMuteOnEvent()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }


const
	kDelegatedSoundComponentSelectors = $0100;


{ Sound Input Manager routines, uses _SoundDispatch }
{$ifc not TARGET_CPU_64}
{
 *  SPBVersion()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBVersion: NumVersion;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndRecord()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SndRecord( filterProc: ModalFilterUPP; corner: Point; quality: OSType; var sndHandle: SndListHandle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}

{
 *  SndRecordToFile()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{$ifc not TARGET_CPU_64}
{
 *  SPBSignInDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBSignInDevice( deviceRefNum: SInt16; const var deviceName: Str255 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBSignOutDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBSignOutDevice( deviceRefNum: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBGetIndexedDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBGetIndexedDevice( count: SInt16; var deviceName: Str255; var deviceIconHandle: Handle ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBOpenDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBOpenDevice( const var deviceName: Str255; permission: SInt16; var inRefNum: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBCloseDevice()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBCloseDevice( inRefNum: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBRecord()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBRecord( inParamPtr: SPBPtr; asynchFlag: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}

{
 *  SPBRecordToFile()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{$ifc not TARGET_CPU_64}
{
 *  SPBPauseRecording()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBPauseRecording( inRefNum: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBResumeRecording()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBResumeRecording( inRefNum: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBStopRecording()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBStopRecording( inRefNum: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBGetRecordingStatus()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBGetRecordingStatus( inRefNum: SIGNEDLONG; var recordingStatus: SInt16; var meterLevel: SInt16; var totalSamplesToRecord: UNSIGNEDLONG; var numberOfSamplesRecorded: UNSIGNEDLONG; var totalMsecsToRecord: UNSIGNEDLONG; var numberOfMsecsRecorded: UNSIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBGetDeviceInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBGetDeviceInfo( inRefNum: SIGNEDLONG; infoType: OSType; infoData: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBSetDeviceInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBSetDeviceInfo( inRefNum: SIGNEDLONG; infoType: OSType; infoData: univ Ptr ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBMillisecondsToBytes()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBMillisecondsToBytes( inRefNum: SIGNEDLONG; var milliseconds: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SPBBytesToMilliseconds()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SPBBytesToMilliseconds( inRefNum: SIGNEDLONG; var byteCount: SIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SetupSndHeader()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SetupSndHeader( sndHandle: SndListHandle; numChannels: SInt16; sampleRate: UnsignedFixed; sampleSize: SInt16; compressionType: OSType; baseNote: SInt16; numBytes: UNSIGNEDLONG; var headerLen: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SetupAIFFHeader()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SetupAIFFHeader( fRefNum: SInt16; numChannels: SInt16; sampleRate: UnsignedFixed; sampleSize: SInt16; compressionType: OSType; numBytes: UNSIGNEDLONG; numFrames: UNSIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{ Sound Input Manager 1.1 and later calls, uses _SoundDispatch }
{
 *  ParseAIFFHeader()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function ParseAIFFHeader( fRefNum: SInt16; var sndInfo: SoundComponentData; var numFrames: UNSIGNEDLONG; var dataOffset: UNSIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  ParseSndHeader()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in SoundLib 3.0 and later
 }
function ParseSndHeader( sndHandle: SndListHandle; var sndInfo: SoundComponentData; var numFrames: UNSIGNEDLONG; var dataOffset: UNSIGNEDLONG ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}

{$ifc TARGET_API_MAC_CARBON}
{  Only to be used if you are writing a sound input component; this }
{  is the param block for a read request from the SoundMgr to the   }
{  sound input component.  Not to be confused with the SPB struct   }
{  above, which is the param block for a read request from an app   }
{  to the SoundMgr.                                                 }
type
	SndInputCmpParamPtr = ^SndInputCmpParam;
	SICCompletionProcPtr = procedure( SICParmPtr: SndInputCmpParamPtr );
	SndInputCmpParam = record
		ioCompletion: SICCompletionProcPtr;         { completion routine [pointer]}
		ioInterrupt: SIInterruptProcPtr;            { interrupt routine [pointer]}
		ioResult: OSErr;               { I/O result code [word]}
		pad: SInt16;
		ioReqCount: UNSIGNEDLONG;
		ioActCount: UNSIGNEDLONG;
		ioBuffer: Ptr;
		ioMisc: Ptr;
	end;

{$ifc not TARGET_CPU_64}
{
 *  SndInputReadAsync()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SndInputReadAsync( self: ComponentInstance; SICParmPtr: SndInputCmpParamPtr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndInputReadSync()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SndInputReadSync( self: ComponentInstance; SICParmPtr: SndInputCmpParamPtr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndInputPauseRecording()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SndInputPauseRecording( self: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndInputResumeRecording()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SndInputResumeRecording( self: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndInputStopRecording()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SndInputStopRecording( self: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndInputGetStatus()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SndInputGetStatus( self: ComponentInstance; var recordingStatus: SInt16; var totalSamplesToRecord: UNSIGNEDLONG; var numberOfSamplesRecorded: UNSIGNEDLONG ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndInputGetDeviceInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SndInputGetDeviceInfo( self: ComponentInstance; infoType: OSType; infoData: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndInputSetDeviceInfo()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SndInputSetDeviceInfo( self: ComponentInstance; infoType: OSType; infoData: univ Ptr ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{
 *  SndInputInitHardware()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function SndInputInitHardware( self: ComponentInstance ): ComponentResult;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;


{$endc} {not TARGET_CPU_64}


{ selectors for component calls }
const
	kSndInputReadAsyncSelect = $0001;
	kSndInputReadSyncSelect = $0002;
	kSndInputPauseRecordingSelect = $0003;
	kSndInputResumeRecordingSelect = $0004;
	kSndInputStopRecordingSelect = $0005;
	kSndInputGetStatusSelect = $0006;
	kSndInputGetDeviceInfoSelect = $0007;
	kSndInputSetDeviceInfoSelect = $0008;
	kSndInputInitHardwareSelect = $0009;
{$endc} {TARGET_API_MAC_CARBON}

{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
