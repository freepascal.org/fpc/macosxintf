{
     File:       CarbonCore/HFSVolumes.h
 
     Contains:   On-disk data structures for HFS and HFS Plus volumes.
                 The contents of this header file are deprecated.
 
     Copyright:  � 1984-2011 by Apple Inc. All rights reserved.
}
unit HFSVolumes;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{ CatalogNodeID is used to track catalog objects }
type
	HFSCatalogNodeID = UInt32;

{$endc} {TARGET_OS_MAC}

end.
