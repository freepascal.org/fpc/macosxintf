{
     File:       AE/AppleEvents.h
 
     Contains:   AppleEvent Package Interfaces.
 
    
 
     Copyright:  � 1989-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, September 2012 }
unit AppleEvents;
interface
uses MacTypes,MixedMode,AEDataModel,AEInteraction, CFArray, CFBase, CFRunLoop, CFStream, CFURL;

{$ifc TARGET_OS_MAC}

{
    Note:   The functions and types for the building and parsing AppleEvent  
            messages has moved to AEDataModel.h
}


{$ALIGN MAC68K}

const
{ Keywords for Apple event parameters }
	keyDirectObject = FOUR_CHAR_CODE('----');
	keyErrorNumber = FOUR_CHAR_CODE('errn');
	keyErrorString = FOUR_CHAR_CODE('errs');
	keyProcessSerialNumber = FOUR_CHAR_CODE('psn '); { Keywords for special handlers }
	keyPreDispatch = FOUR_CHAR_CODE('phac'); { preHandler accessor call }
	keySelectProc = FOUR_CHAR_CODE('selh'); { more selector call }
                                        { Keyword for recording }
	keyAERecorderCount = FOUR_CHAR_CODE('recr'); { available only in vers 1.0.1 and greater }
                                        { Keyword for version information }
	keyAEVersion = FOUR_CHAR_CODE('vers'); { available only in vers 1.0.1 and greater }

{ Event Class }
const
	kCoreEventClass = FOUR_CHAR_CODE('aevt');

{ Event ID's }
const
	kAEOpenApplication = FOUR_CHAR_CODE('oapp');
	kAEOpenDocuments = FOUR_CHAR_CODE('odoc');
	kAEPrintDocuments = FOUR_CHAR_CODE('pdoc');
	kAEOpenContents = FOUR_CHAR_CODE('ocon');
	kAEQuitApplication = FOUR_CHAR_CODE('quit'); { may include a property kAEQuitReason indicating what lead to the quit being sent. }
	kAEAnswer = FOUR_CHAR_CODE('ansr');
	kAEApplicationDied = FOUR_CHAR_CODE('obit');
	kAEShowPreferences = FOUR_CHAR_CODE('pref'); { sent by Mac OS X when the user chooses the Preferences item }

{ Constants for recording }
const
	kAEStartRecording = FOUR_CHAR_CODE('reca'); { available only in vers 1.0.1 and greater }
	kAEStopRecording = FOUR_CHAR_CODE('recc'); { available only in vers 1.0.1 and greater }
	kAENotifyStartRecording = FOUR_CHAR_CODE('rec1'); { available only in vers 1.0.1 and greater }
	kAENotifyStopRecording = FOUR_CHAR_CODE('rec0'); { available only in vers 1.0.1 and greater }
	kAENotifyRecording = FOUR_CHAR_CODE('recr'); { available only in vers 1.0.1 and greater }


{
 * AEEventSource is defined as an SInt8 for compatability with pascal.
 * Important note: keyEventSourceAttr is returned by AttributePtr as a typeShortInteger.
 * Be sure to pass at least two bytes of storage to AEGetAttributePtr - the result can be
 * compared directly against the following enums.
 }
type
	AEEventSource = SInt8;
const
	kAEUnknownSource = 0;
	kAEDirectCall = 1;
	kAESameProcess = 2;
	kAELocalProcess = 3;
	kAERemoteProcess = 4;

	
{ if __MAC_OS_X_VERSION_MIN_REQUIRED >= 1080 }
const
	errAETargetAddressNotPermitted = -1742;	{ Mac OS X 10.8 and later, the target of an AppleEvent is not accessible to this process, perhaps due to sandboxing }
	errAEEventNotPermitted = -1743;			{ Mac OS X 10.8 and later, the target of the AppleEvent does not allow this sender to execute this event }
{ endif }
	
	
{*************************************************************************
  These calls are used to set up and modify the event dispatch table.
*************************************************************************}
{
 *  AEInstallEventHandler()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEInstallEventHandler( theAEEventClass: AEEventClass; theAEEventID: AEEventID; handler: AEEventHandlerUPP; handlerRefcon: SRefCon; isSysHandler: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AERemoveEventHandler()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AERemoveEventHandler( theAEEventClass: AEEventClass; theAEEventID: AEEventID; handler: AEEventHandlerUPP; isSysHandler: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AEGetEventHandler()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEGetEventHandler( theAEEventClass: AEEventClass; theAEEventID: AEEventID; var handler: AEEventHandlerUPP; var handlerRefcon: SRefCon; isSysHandler: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{*************************************************************************
  These calls are used to set up and modify special hooks into the
  AppleEvent manager.
*************************************************************************}
{
 *  AEInstallSpecialHandler()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEInstallSpecialHandler( functionClass: AEKeyword; handler: AEEventHandlerUPP; isSysHandler: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AERemoveSpecialHandler()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AERemoveSpecialHandler( functionClass: AEKeyword; handler: AEEventHandlerUPP; isSysHandler: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
 *  AEGetSpecialHandler()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEGetSpecialHandler( functionClass: AEKeyword; var handler: AEEventHandlerUPP; isSysHandler: Boolean ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{*************************************************************************
  This call was added in version 1.0.1. If called with the keyword
  keyAERecorderCount ('recr'), the number of recorders that are
  currently active is returned in 'result'
  (available only in vers 1.0.1 and greater).
*************************************************************************}
{
 *  AEManagerInfo()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in ApplicationServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEManagerInfo( keyWord: AEKeyword; var result: SIGNEDLONG ): OSErr;
__OSX_AVAILABLE_STARTING( __MAC_10_0, __IPHONE_NA );


{
  AERemoteProcessResolver:
  
  These calls subsume the functionality of using the PPCToolbox on Mac
  OS 9 to locate processes on remote computers.  (PPCToolbox is not
  part of Carbon.)  These calls are supported on Mac OS X 10.3 or
  later.
  
  The model is to create a resolver for a particular URL and schedule
  it on a CFRunLoop to retrieve the results asynchronously.  If
  synchronous behavior is desired, just call
  AERemoteProcessResolverGetProcesses to get the array; the call will
  block until the request is completed.
  
  A resolver can only be used once; once it has fetched the data or
  gotten an error it can no longer be scheduled.
  
  The data obtained from the resolver is a CFArrayRef of
  CFDictionaryRef objects.  Each dictionary contains the URL of the
  remote application and its human readable name.
}

{
 *  kAERemoteProcessURLKey
 *  
 *  Discussion:
 *    the full URL to this application, a CFURLRef.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kAERemoteProcessURLKey: CFStringRef;
__OSX_AVAILABLE_STARTING( __MAC_10_3, __IPHONE_NA );
{
 *  kAERemoteProcessNameKey
 *  
 *  Discussion:
 *    the visible name to this application, in the localization
 *    supplied by the server, a CFStringRef.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kAERemoteProcessNameKey: CFStringRef;
__OSX_AVAILABLE_STARTING( __MAC_10_3, __IPHONE_NA );
{
 *  kAERemoteProcessUserIDKey
 *  
 *  Discussion:
 *    the userid of this application, if available.  If present, a
 *    CFNumberRef.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kAERemoteProcessUserIDKey: CFStringRef;
__OSX_AVAILABLE_STARTING( __MAC_10_3, __IPHONE_NA );
{
 *  kAERemoteProcessProcessIDKey
 *  
 *  Discussion:
 *    the process id of this application, if available.  If present, a
 *    CFNumberRef.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
const kAERemoteProcessProcessIDKey: CFStringRef;
__OSX_AVAILABLE_STARTING( __MAC_10_3, __IPHONE_NA );

{
 *  AERemoteProcessResolverContext
 *  
 *  Discussion:
 *    An optional context parameter for asynchronous resolution.  The
 *    context is copied and the info pointer retained.  When the
 *    callback is made, the info pointer is passed to the callback.
 }
type
	AERemoteProcessResolverContextPtr = ^AERemoteProcessResolverContext;
	AERemoteProcessResolverContext = record
{
   * set to zero (0)
   }
		version: CFIndex;

  {
   * info pointer to be passed to the callback
   }
		info: UnivPtr;

  {
   * callback made on the info pointer. This field may be NULL.
   }
		retain: CFAllocatorRetainCallBack;

  {
   * callback made on the info pointer. This field may be NULL.
   }
		release: CFAllocatorReleaseCallBack;

  {
   * callback made on the info pointer. This field may be NULL.
   }
		copyDescription: CFAllocatorCopyDescriptionCallBack;
	end;

{
 *  AERemoteProcessResolverRef
 *  
 *  Discussion:
 *    An opaque reference to an object that encapsulates the mechnanism
 *    by which a list of processes running on a remote machine are
 *    obtained.  Created by AECreateRemoteProcessResolver, and must be
 *    disposed of by AEDisposeRemoteProcessResolver. A
 *    AERemoteProcessResolverRef is not a CFType.
 }
type
	AERemoteProcessResolverRef = ^SInt32; { an opaque type }
{
 *  AECreateRemoteProcessResolver()
 *  
 *  Discussion:
 *    Create a Remote Process List Resolver object.  The allocator is
 *    used for any CoreFoundation types created or returned by this
 *    API.  The resulting object can be scheduled on a run loop, or
 *    queried synchronously.  Once the object has retreived results
 *    from the server, or got an error doing so, it will not re-fetch
 *    the data.  To retrieve a new list of processes, create a new
 *    instance of this object.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    allocator:
 *      a CFAllocatorRef to use when creating CFTypes
 *    
 *    url:
 *      a CFURL identifying the remote host and port.
 *  
 *  Result:
 *    a AECreateRemoteProcessResolverRef, which must be disposed of
 *    with AEDisposeRemoteProcessResolver.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function AECreateRemoteProcessResolver( allocator: CFAllocatorRef; url: CFURLRef ): AERemoteProcessResolverRef;
__OSX_AVAILABLE_STARTING( __MAC_10_3, __IPHONE_NA );


{
 *  AEDisposeRemoteProcessResolver()
 *  
 *  Discussion:
 *    Disposes of a AERemoteProcessResolverRef.  If this resolver is
 *    currently scheduled on a run loop, it is unscheduled.  In this
 *    case, the asynchronous callback will not be executed.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    ref:
 *      The AERemoteProcessResolverRef to dispose
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure AEDisposeRemoteProcessResolver( ref: AERemoteProcessResolverRef );
__OSX_AVAILABLE_STARTING( __MAC_10_3, __IPHONE_NA );


{
 *  AERemoteProcessResolverGetProcesses()
 *  
 *  Discussion:
 *    Returns a CFArrayRef containing CFDictionary objects containing
 *    information about processses running on a remote machine.  If the
 *    result array is NULL, the query failed and the error out
 *    parameter will contain information about the failure.  If the
 *    resolver had not been previously scheduled for execution, this
 *    call will block until the resulting array is available or an
 *    error occurs.  If the resolver had been scheduled but had not yet
 *    completed fetching the array, this call will block until the
 *    resolver does complete.  The array returned is owned by the
 *    resolver, so callers must retain it before disposing of the
 *    resolver object itself.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    ref:
 *      The AERemoteProcessResolverRef to query
 *    
 *    outError:
 *      If the result is NULL, outError will contain a CFStreamError
 *      with information about the type of failure
 *  
 *  Result:
 *    a CFArray of CFDictionary objects containing information about
 *    the remote applications.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function AERemoteProcessResolverGetProcesses( ref: AERemoteProcessResolverRef; var outError: CFStreamError ): CFArrayRef;
__OSX_AVAILABLE_STARTING( __MAC_10_3, __IPHONE_NA );


{
 *  AERemoteProcessResolverCallback
 *  
 *  Discussion:
 *    A callback made when the asynchronous execution of a resolver
 *    completes, either due to success or failure. The data itself
 *    should be obtained with AERemoteProcessResolverGetProcesses.
 }
type
	AERemoteProcessResolverCallback = procedure( ref: AERemoteProcessResolverRef; info: univ Ptr );
{
 *  AERemoteProcessResolverScheduleWithRunLoop()
 *  
 *  Discussion:
 *    Schedules a resolver for execution on a given runloop in a given
 *    mode.   The resolver will move through various internal states as
 *    long as the specified run loop is run.  When the resolver
 *    completes, either with success or an error condition, the
 *    callback is executed.  There is no explicit unschedule of the
 *    resolver; you must dispose of it to remove it from the run loop.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    ref:
 *      The AERemoteProcessResolverRef to scheduile
 *    
 *    runLoop:
 *      a CFRunLoop
 *    
 *    runLoopMode:
 *      a CFString specifying the run loop mode
 *    
 *    callback:
 *      a callback to be executed when the reolver completes
 *    
 *    ctx:
 *      a AERemoteProcessResolverContext.  If this parameter is not
 *      NULL, the info field of this structure will be passed to the
 *      callback (otherwise, the callback info parameter will
 *      explicitly be NULL.)
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in ApplicationServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure AERemoteProcessResolverScheduleWithRunLoop( ref: AERemoteProcessResolverRef; runLoop: CFRunLoopRef; runLoopMode: CFStringRef; callback: AERemoteProcessResolverCallback; {const} ctx: AERemoteProcessResolverContextPtr { can be NULL } );
__OSX_AVAILABLE_STARTING( __MAC_10_3, __IPHONE_NA );



{$endc} {TARGET_OS_MAC}

end.
