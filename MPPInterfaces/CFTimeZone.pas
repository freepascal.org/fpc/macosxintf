{	CFTimeZone.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFTimeZone;
interface
uses MacTypes,CFBase,CFArray,CFData,CFDate,CFDictionary,CFLocale,CFString;
{$ALIGN POWER}


function CFTimeZoneGetTypeID: CFTypeID;

function CFTimeZoneCopySystem: CFTimeZoneRef;

procedure CFTimeZoneResetSystem;

function CFTimeZoneCopyDefault: CFTimeZoneRef;

procedure CFTimeZoneSetDefault( tz: CFTimeZoneRef );

function CFTimeZoneCopyKnownNames: CFArrayRef;

function CFTimeZoneCopyAbbreviationDictionary: CFDictionaryRef;

procedure CFTimeZoneSetAbbreviationDictionary( dict: CFDictionaryRef );

function CFTimeZoneCreate( allocator: CFAllocatorRef; name: CFStringRef; data: CFDataRef ): CFTimeZoneRef;

function CFTimeZoneCreateWithTimeIntervalFromGMT( allocator: CFAllocatorRef; ti: CFTimeInterval ): CFTimeZoneRef;

function CFTimeZoneCreateWithName( allocator: CFAllocatorRef; name: CFStringRef; tryAbbrev: Boolean ): CFTimeZoneRef;

function CFTimeZoneGetName( tz: CFTimeZoneRef ): CFStringRef;

function CFTimeZoneGetData( tz: CFTimeZoneRef ): CFDataRef;

function CFTimeZoneGetSecondsFromGMT( tz: CFTimeZoneRef; at: CFAbsoluteTime ): CFTimeInterval;

function CFTimeZoneCopyAbbreviation( tz: CFTimeZoneRef; at: CFAbsoluteTime ): CFStringRef;

function CFTimeZoneIsDaylightSavingTime( tz: CFTimeZoneRef; at: CFAbsoluteTime ): Boolean;

function CFTimeZoneGetDaylightSavingTimeOffset( tz: CFTimeZoneRef; at: CFAbsoluteTime ): CFTimeInterval;
CF_AVAILABLE_STARTING(10_5, 2_0);

function CFTimeZoneGetNextDaylightSavingTimeTransition( tz: CFTimeZoneRef; at: CFAbsoluteTime ): CFAbsoluteTime;
CF_AVAILABLE_STARTING(10_5, 2_0);

type
	CFTimeZoneNameStyle = CFIndex;

const
	kCFTimeZoneNameStyleStandard = 0;
	kCFTimeZoneNameStyleShortStandard = 1;
	kCFTimeZoneNameStyleDaylightSaving = 2;
	kCFTimeZoneNameStyleShortDaylightSaving = 3;
	kCFTimeZoneNameStyleGeneric = 4;
	kCFTimeZoneNameStyleShortGeneric = 5;

function CFTimeZoneCopyLocalizedName( tz: CFTimeZoneRef; style: CFTimeZoneNameStyle; locale: CFLocaleRef ): CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0);

const kCFTimeZoneSystemTimeZoneDidChangeNotification: CFStringRef;
CF_AVAILABLE_STARTING(10_5, 2_0);



end.
