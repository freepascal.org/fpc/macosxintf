{	CFData.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFData;
interface
uses MacTypes,CFBase;
{$ALIGN POWER}


    
type
	CFDataRef = ^__CFData; { an opaque type }
	__CFData = record end;
	CFDataRefPtr = ^CFDataRef;
	CFMutableDataRef = ^__CFData;
	CFMutableDataRefPtr = ^CFMutableDataRef;

function CFDataGetTypeID: CFTypeID;

function CFDataCreate( allocator: CFAllocatorRef; bytes: univ Ptr; length: CFIndex ): CFDataRef;

function CFDataCreateWithBytesNoCopy( allocator: CFAllocatorRef; bytes: univ Ptr; length: CFIndex; bytesDeallocator: CFAllocatorRef ): CFDataRef;
    { Pass kCFAllocatorNull as bytesDeallocator to assure the bytes aren't freed }

function CFDataCreateCopy( allocator: CFAllocatorRef; theData: CFDataRef ): CFDataRef;

function CFDataCreateMutable( allocator: CFAllocatorRef; capacity: CFIndex ): CFMutableDataRef;

function CFDataCreateMutableCopy( allocator: CFAllocatorRef; capacity: CFIndex; theData: CFDataRef ): CFMutableDataRef;

function CFDataGetLength( theData: CFDataRef ): CFIndex;

function CFDataGetBytePtr( theData: CFDataRef ): UnivPtr;

function CFDataGetMutableBytePtr( theData: CFMutableDataRef ): UnivPtr;

procedure CFDataGetBytes( theData: CFDataRef; range: CFRange; buffer: univ Ptr );

procedure CFDataSetLength( theData: CFMutableDataRef; length: CFIndex );

procedure CFDataIncreaseLength( theData: CFMutableDataRef; extraLength: CFIndex );

procedure CFDataAppendBytes( theData: CFMutableDataRef; bytes: univ Ptr; length: CFIndex );

procedure CFDataReplaceBytes( theData: CFMutableDataRef; range: CFRange; newBytes: univ Ptr; newLength: CFIndex );

procedure CFDataDeleteBytes( theData: CFMutableDataRef; range: CFRange );

{#if MAC_OS_X_VERSION_10_6 <= MAC_OS_X_VERSION_MAX_ALLOWED}
type
	CFDataSearchFlags = CFOptionFlags;

const
	kCFDataSearchBackwards = 1 shl 0; CF_AVAILABLE_STARTING(10_6, 4_0);
	kCFDataSearchAnchored = 1 shl 1; CF_AVAILABLE_STARTING(10_6, 4_0);

function CFDataFind( theData: CFDataRef; dataToFind: CFDataRef; searchRange: CFRange; compareOptions: CFDataSearchFlags ): CFRange;
CF_AVAILABLE_STARTING(10_6, 4_0);
{#endif}

end.
