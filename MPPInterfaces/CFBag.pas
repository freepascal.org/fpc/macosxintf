{	CFBag.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFBag;
interface
uses MacTypes,CFBase;
{$ALIGN POWER}


type
	CFBagRetainCallBack = function( allocator: CFAllocatorRef; value: {const} univ Ptr ): UnivPtr;
	CFBagReleaseCallBack = procedure( allocator: CFAllocatorRef; value: {const} univ Ptr );
	CFBagCopyDescriptionCallBack = function( value: {const} univ Ptr ): CFStringRef;
	CFBagEqualCallBack = function( value1: {const} univ Ptr; value2: {const} univ Ptr ): Boolean;
	CFBagHashCallBack = function( value: {const} univ Ptr ): CFHashCode;
	CFBagCallBacksPtr = ^CFBagCallBacks;
	CFBagCallBacks = record
		version: CFIndex;
		retain: CFBagRetainCallBack;
		release: CFBagReleaseCallBack;
		copyDescription: CFBagCopyDescriptionCallBack;
		equal: CFBagEqualCallBack;
		hash: CFBagHashCallBack;
	end;

const kCFTypeBagCallBacks: CFBagCallBacks;
const kCFCopyStringBagCallBacks: CFBagCallBacks;

type
	CFBagApplierFunction = procedure( value: {const} univ Ptr; context: univ Ptr );

type
	CFBagRef = ^__CFBag; { an opaque type }
	__CFBag = record end;
	CFBagRefPtr = ^CFBagRef;
	CFMutableBagRef = CFBagRef;
	CFMutableBagRefPtr = ^CFMutableBagRef;

function CFBagGetTypeID: CFTypeID;

function CFBagCreate( allocator: CFAllocatorRef; {const} values: {variable-size-array} UnivPtrPtr; numValues: CFIndex; {const} callBacks: CFBagCallBacksPtr { can be NULL } ): CFBagRef;

function CFBagCreateCopy( allocator: CFAllocatorRef; theBag: CFBagRef ): CFBagRef;

function CFBagCreateMutable( allocator: CFAllocatorRef; capacity: CFIndex; callBacks: CFBagCallBacksPtr ): CFMutableBagRef;

function CFBagCreateMutableCopy( allocator: CFAllocatorRef; capacity: CFIndex; theBag: CFBagRef ): CFMutableBagRef;

function CFBagGetCount( theBag: CFBagRef ): CFIndex;

function CFBagGetCountOfValue( theBag: CFBagRef; value: {const} univ Ptr ): CFIndex;

function CFBagContainsValue( theBag: CFBagRef; value: {const} univ Ptr ): Boolean;

function CFBagGetValue( theBag: CFBagRef; value: {const} univ Ptr ): UnivPtr;

function CFBagGetValueIfPresent( theBag: CFBagRef; candidate: {const} univ Ptr; var value: univ Ptr ): Boolean;

procedure CFBagGetValues( theBag: CFBagRef; {const} values: {variable-size-array} UnivPtrPtr );

procedure CFBagApplyFunction( theBag: CFBagRef; applier: CFBagApplierFunction; context: univ Ptr );

procedure CFBagAddValue( theBag: CFMutableBagRef; value: {const} univ Ptr );

procedure CFBagReplaceValue( theBag: CFMutableBagRef; value: {const} univ Ptr );

procedure CFBagSetValue( theBag: CFMutableBagRef; value: {const} univ Ptr );

procedure CFBagRemoveValue( theBag: CFMutableBagRef; value: {const} univ Ptr );

procedure CFBagRemoveAllValues( theBag: CFMutableBagRef );


end.
