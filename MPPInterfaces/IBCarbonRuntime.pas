{
     File:       HIToolbox/IBCarbonRuntime.h
 
     Contains:   Nib support for Carbon
 
     Version:    HIToolbox-624~3
 
     Copyright:  � 2000-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit IBCarbonRuntime;
interface
uses MacTypes,CFBase,QuickdrawTypes,Menus,CFString,CFBundle,MacWindows,ControlDefinitions;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


const
	kIBCarbonRuntimeCantFindNibFile = -10960;
	kIBCarbonRuntimeObjectNotOfRequestedType = -10961;
	kIBCarbonRuntimeCantFindObject = -10962;

{ ----- typedef ------ }
type
	IBNibRef = ^OpaqueIBNibRef; { an opaque type }
	OpaqueIBNibRef = record end;
{ ----- Create & Dispose NIB References ------ }
{$ifc not TARGET_CPU_64}
{
 *  CreateNibReference()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateNibReference( inNibName: CFStringRef; var outNibRef: IBNibRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CreateNibReferenceWithCFBundle()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateNibReferenceWithCFBundle( inBundle: CFBundleRef; inNibName: CFStringRef; var outNibRef: IBNibRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DisposeNibReference()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
procedure DisposeNibReference( inNibRef: IBNibRef );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ ----- Window ------ }
{
 *  CreateWindowFromNib()
 *  
 *  Summary:
 *    Creates a window from a description stored in a nib file.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    inNibRef:
 *      The identifier for the nib file containing the window
 *      description.
 *    
 *    inName:
 *      The name of the window description.
 *    
 *    outWindow:
 *      On exit, contains the window, if creation of the window was
 *      successful.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateWindowFromNib( inNibRef: IBNibRef; inName: CFStringRef; var outWindow: WindowRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ ----- Menu -----}

{
 *  CreateMenuFromNib()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateMenuFromNib( inNibRef: IBNibRef; inName: CFStringRef; var outMenuRef: MenuRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ ----- MenuBar ------}

{
 *  CreateMenuBarFromNib()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateMenuBarFromNib( inNibRef: IBNibRef; inName: CFStringRef; var outMenuBar: Handle ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetMenuBarFromNib()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function SetMenuBarFromNib( inNibRef: IBNibRef; inName: CFStringRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
