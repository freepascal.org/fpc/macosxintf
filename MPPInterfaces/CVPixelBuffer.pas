{
 *  CVPixelBuffer.h
 *  CoreVideo
 *
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2008 }
{  Pascal Translation Update:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }
unit CVPixelBuffer;
interface
uses MacTypes, CFArray, CFBase, CFDictionary, CVBase, CVImageBuffer, CVReturns;

{$ALIGN POWER}

 
  {! @header CVPixelBuffer.h
	@copyright 2004 Apple Computer, Inc. All rights reserved.
	@availability Mac OS X 10.4 or later
    @discussion CVPixelBuffers are CVImageBuffers that hold the pixels in main memory
		   
}

{
CoreVideo pixel format type constants.
CoreVideo does not provide support for all of these formats; this list just defines their names.
}
const
	kCVPixelFormatType_1Monochrome = $00000001; { 1 bit indexed }
	kCVPixelFormatType_2Indexed = $00000002; { 2 bit indexed }
	kCVPixelFormatType_4Indexed = $00000004; { 4 bit indexed }
	kCVPixelFormatType_8Indexed = $00000008; { 8 bit indexed }
	kCVPixelFormatType_1IndexedGray_WhiteIsZero = $00000021; { 1 bit indexed gray, white is zero }
	kCVPixelFormatType_2IndexedGray_WhiteIsZero = $00000022; { 2 bit indexed gray, white is zero }
	kCVPixelFormatType_4IndexedGray_WhiteIsZero = $00000024; { 4 bit indexed gray, white is zero }
	kCVPixelFormatType_8IndexedGray_WhiteIsZero = $00000028; { 8 bit indexed gray, white is zero }
	kCVPixelFormatType_16BE555 = $00000010; { 16 bit BE RGB 555 }
	kCVPixelFormatType_16LE555 = FOUR_CHAR_CODE('L555');     { 16 bit LE RGB 555 }
	kCVPixelFormatType_16LE5551 = FOUR_CHAR_CODE('5551');     { 16 bit LE RGB 5551 }
	kCVPixelFormatType_16BE565 = FOUR_CHAR_CODE('B565');     { 16 bit BE RGB 565 }
	kCVPixelFormatType_16LE565 = FOUR_CHAR_CODE('L565');     { 16 bit LE RGB 565 }
	kCVPixelFormatType_24RGB = $00000018; { 24 bit RGB }
	kCVPixelFormatType_24BGR = FOUR_CHAR_CODE('24BG');     { 24 bit BGR }
	kCVPixelFormatType_32ARGB = $00000020; { 32 bit ARGB }
	kCVPixelFormatType_32BGRA = FOUR_CHAR_CODE('BGRA');     { 32 bit BGRA }
	kCVPixelFormatType_32ABGR = FOUR_CHAR_CODE('ABGR');     { 32 bit ABGR }
	kCVPixelFormatType_32RGBA = FOUR_CHAR_CODE('RGBA');     { 32 bit RGBA }
	kCVPixelFormatType_64ARGB = FOUR_CHAR_CODE('b64a');     { 64 bit ARGB, 16-bit big-endian samples }
	kCVPixelFormatType_48RGB = FOUR_CHAR_CODE('b48r');     { 48 bit RGB, 16-bit big-endian samples }
	kCVPixelFormatType_32AlphaGray = FOUR_CHAR_CODE('b32a');     { 32 bit AlphaGray, 16-bit big-endian samples, black is zero }
	kCVPixelFormatType_16Gray = FOUR_CHAR_CODE('b16g');     { 16 bit Grayscale, 16-bit big-endian samples, black is zero }
	kCVPixelFormatType_30RGB = FOUR_CHAR_CODE('R10k');     { 30 bit RGB, 10-bit big-endian samples, 2 unused padding bits (at least significant end). }
	kCVPixelFormatType_422YpCbCr8 = FOUR_CHAR_CODE('2vuy');     { Component Y'CbCr 8-bit 4:2:2, ordered Cb Y'0 Cr Y'1 }
	kCVPixelFormatType_4444YpCbCrA8 = FOUR_CHAR_CODE('v408');     { Component Y'CbCrA 8-bit 4:4:4:4, ordered Cb Y' Cr A }
	kCVPixelFormatType_4444YpCbCrA8R = FOUR_CHAR_CODE('r408');     { Component Y'CbCrA 8-bit 4:4:4:4, rendering format. full range alpha, zero biased YUV, ordered A Y' Cb Cr }
	kCVPixelFormatType_4444AYpCbCr8 = FOUR_CHAR_CODE('y408');     { Component Y'CbCrA 8-bit 4:4:4:4, ordered A Y' Cb Cr, full range alpha, video range Y'CbCr. }
	kCVPixelFormatType_4444AYpCbCr16 = FOUR_CHAR_CODE('y416');     { Component Y'CbCrA 16-bit 4:4:4:4, ordered A Y' Cb Cr, full range alpha, video range Y'CbCr, 16-bit little-endian samples. }
	kCVPixelFormatType_444YpCbCr8 = FOUR_CHAR_CODE('v308');     { Component Y'CbCr 8-bit 4:4:4 }
	kCVPixelFormatType_422YpCbCr16 = FOUR_CHAR_CODE('v216');     { Component Y'CbCr 10,12,14,16-bit 4:2:2 }
	kCVPixelFormatType_422YpCbCr10 = FOUR_CHAR_CODE('v210');     { Component Y'CbCr 10-bit 4:2:2 }
	kCVPixelFormatType_444YpCbCr10 = FOUR_CHAR_CODE('v410');     { Component Y'CbCr 10-bit 4:4:4 }
	kCVPixelFormatType_420YpCbCr8Planar = FOUR_CHAR_CODE('y420');   { Planar Component Y'CbCr 8-bit 4:2:0.  baseAddr points to a big-endian CVPlanarPixelBufferInfo_YCbCrPlanar struct }
	kCVPixelFormatType_420YpCbCr8PlanarFullRange = FOUR_CHAR_CODE('f420');   { Planar Component Y'CbCr 8-bit 4:2:0, full range.  baseAddr points to a big-endian CVPlanarPixelBufferInfo_YCbCrPlanar struct }
	kCVPixelFormatType_422YpCbCr_4A_8BiPlanar = FOUR_CHAR_CODE('a2vy'); { First plane: Video-range Component Y'CbCr 8-bit 4:2:2, ordered Cb Y'0 Cr Y'1; second plane: alpha 8-bit 0-255 }
	kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange = FOUR_CHAR_CODE('420v'); { Bi-Planar Component Y'CbCr 8-bit 4:2:0, video-range (luma=[16,235] chroma=[16,240]).  baseAddr points to a big-endian CVPlanarPixelBufferInfo_YCbCrBiPlanar struct }
	kCVPixelFormatType_420YpCbCr8BiPlanarFullRange = FOUR_CHAR_CODE('420f'); { Bi-Planar Component Y'CbCr 8-bit 4:2:0, full-range (luma=[0,255] chroma=[1,255]).  baseAddr points to a big-endian CVPlanarPixelBufferInfo_YCbCrBiPlanar struct } 
	kCVPixelFormatType_422YpCbCr8_yuvs = FOUR_CHAR_CODE('yuvs');     { Component Y'CbCr 8-bit 4:2:2, ordered Y'0 Cb Y'1 Cr }
	kCVPixelFormatType_422YpCbCr8FullRange = FOUR_CHAR_CODE('yuvf'); { Component Y'CbCr 8-bit 4:2:2, full range, ordered Y'0 Cb Y'1 Cr }
	kCVPixelFormatType_OneComponent8 = FOUR_CHAR_CODE('L008');     { 8 bit one component, black is zero }
	kCVPixelFormatType_TwoComponent8 = FOUR_CHAR_CODE('2C08');     { 8 bit two component, black is zero }
	kCVPixelFormatType_OneComponent16Half = FOUR_CHAR_CODE('L00h');     { 16 bit one component IEEE half-precision float, 16-bit little-endian samples }
	kCVPixelFormatType_OneComponent32Float = FOUR_CHAR_CODE('L00f');     { 32 bit one component IEEE float, 32-bit little-endian samples }
	kCVPixelFormatType_TwoComponent16Half = FOUR_CHAR_CODE('2C0h');     { 16 bit two component IEEE half-precision float, 16-bit little-endian samples }
	kCVPixelFormatType_TwoComponent32Float = FOUR_CHAR_CODE('2C0f');     { 32 bit two component IEEE float, 32-bit little-endian samples }
	kCVPixelFormatType_64RGBAHalf = FOUR_CHAR_CODE('RGhA');     { 64 bit RGBA IEEE half-precision float, 16-bit little-endian samples }
	kCVPixelFormatType_128RGBAFloat = FOUR_CHAR_CODE('RGfA');     { 128 bit RGBA IEEE float, 32-bit little-endian samples }

	
{!
	@enum Pixel Buffer Locking Flags
	@discussion Flags to pass to CVPixelBufferLockBaseAddress() / CVPixelBufferUnlockBaseAddress()
	@constant kCVPixelBufferLock_ReadOnly
		If you are not going to modify the data while you hold the lock, you should set this flag
		to avoid potentially invalidating any existing caches of the buffer contents.  This flag
		should be passed both to the lock and unlock functions.  Non-symmetrical usage of this
		flag will result in undefined behavior.
}
type
	CVPixelBufferLockFlags = SInt32;
const
	kCVPixelBufferLock_ReadOnly = $00000001;

{
Planar pixel buffers have the following descriptor at their base address.  
Clients should generally use CVPixelBufferGetBaseAddressOfPlane, 
CVPixelBufferGetBytesPerRowOfPlane, etc. instead of accessing it directly.
}
type
	CVPlanarComponentInfoPtr = ^CVPlanarComponentInfo;
	CVPlanarComponentInfo = record
		offset: SInt32;    { offset from main base address to base address of this plane, big-endian }
		rowBytes: UInt32;  { bytes per row of this plane, big-endian }
	end;
type
	CVPlanarPixelBufferInfoPtr = ^CVPlanarPixelBufferInfo;
	CVPlanarPixelBufferInfo = record
		componentInfo: array [0..1-1] of CVPlanarComponentInfo;
	end;
type
	CVPlanarPixelBufferInfo_YCbCrPlanarPtr = ^CVPlanarPixelBufferInfo_YCbCrPlanar;
	CVPlanarPixelBufferInfo_YCbCrPlanar = record
		componentInfoY: CVPlanarComponentInfo;
		componentInfoCb: CVPlanarComponentInfo;
		componentInfoCr: CVPlanarComponentInfo;
	end;
type
    CVPlanarPixelBufferInfo_YCbCrBiPlanarPtr = ^CVPlanarPixelBufferInfo_YCbCrBiPlanar;
	CVPlanarPixelBufferInfo_YCbCrBiPlanar = record
		componentInfoY: CVPlanarComponentInfo;
		componentInfoCbCr: CVPlanarComponentInfo;
	end;

//#pragma mark BufferAttributeKeys
const kCVPixelBufferPixelFormatTypeKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		    // A single CFNumber or a CFArray of CFNumbers (OSTypes)
const kCVPixelBufferMemoryAllocatorKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		    // CFAllocatorRef
const kCVPixelBufferWidthKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);			    // CFNumber
const kCVPixelBufferHeightKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);			    // CFNumber
const kCVPixelBufferExtendedPixelsLeftKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);	    // CFNumber
const kCVPixelBufferExtendedPixelsTopKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);		    // CFNumber
const kCVPixelBufferExtendedPixelsRightKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);	    // CFNumber
const kCVPixelBufferExtendedPixelsBottomKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);	    // CFNumber
const kCVPixelBufferBytesPerRowAlignmentKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);	    // CFNumber
const kCVPixelBufferCGBitmapContextCompatibilityKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);  // CFBoolean
const kCVPixelBufferCGImageCompatibilityKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);	    // CFBoolean
const kCVPixelBufferOpenGLCompatibilityKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);	    // CFBoolean
const kCVPixelBufferPlaneAlignmentKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0);		    // CFNumber
const kCVPixelBufferIOSurfacePropertiesKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0);     // CFDictionary; presence requests buffer allocation via IOSurface
{$ifc TARGET_OS_IPHONE}
const kCVPixelBufferOpenGLESCompatibilityKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0);	    // CFBoolean
{$endc}

{!
    @typedef	CVPixelBufferRef
    @abstract   Based on the image buffer type. The pixel buffer implements the memory storage for an image buffer.

}
type
	CVPixelBufferRef = CVImageBufferRef;

function CVPixelBufferGetTypeID: CFTypeID;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferRetain
    @abstract   Retains a CVPixelBuffer object
    @discussion Equivalent to CFRetain, but NULL safe
    @param      buffer A CVPixelBuffer object that you want to retain.
    @result     A CVPixelBuffer object that is the same as the passed in buffer.
}
function CVPixelBufferRetain( texture: CVPixelBufferRef ): CVPixelBufferRef;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferRelease
    @abstract   Releases a CVPixelBuffer object
    @discussion Equivalent to CFRelease, but NULL safe
    @param      buffer A CVPixelBuffer object that you want to release.
}
procedure CVPixelBufferRelease( texture: CVPixelBufferRef );
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferCreateResolvedAttributesDictionary
    @abstract   Takes a CFArray of CFDictionary objects describing various pixel buffer attributes and tries to resolve them into a
                single dictionary.
    @discussion This is useful when you need to resolve multiple requirements between different potential clients of a buffer.
    @param      attributes CFArray of CFDictionaries containing kCVPixelBuffer key/value pairs.
    @param      resolvedDictionaryOut The resulting dictionary will be placed here.
    @result     Return value that may be useful in discovering why resolution failed.
}
function CVPixelBufferCreateResolvedAttributesDictionary( allocator: CFAllocatorRef; attributes: CFArrayRef; var resolvedDictionaryOut: CFDictionaryRef ): CVReturn;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);


{!
    @function   CVPixelBufferCreate
    @abstract   Call to create a single PixelBuffer for a given size and pixelFormatType.
    @discussion Creates a single PixelBuffer for a given size and pixelFormatType. It allocates the necessary memory based on the pixel dimensions, pixelFormatType and extended pixels described in the pixelBufferAttributes. Not all parameters of the pixelBufferAttributes will be used here.
    @param      width   Width of the PixelBuffer in pixels.
    @param      height  Height of the PixelBuffer in pixels.
    @param	pixelFormatType		Pixel format indentified by its respective OSType.
    @param	pixelBufferAttributes      A dictionary with additional attributes for a a pixel buffer. This parameter is optional. See PixelBufferAttributes for more details.
    @param      pixelBufferOut          The new pixel buffer will be returned here
    @result	returns kCVReturnSuccess on success.
}    
function CVPixelBufferCreate( allocator: CFAllocatorRef; width: size_t; height: size_t; pixelFormatType: OSType; pixelBufferAttributes: CFDictionaryRef; var pixelBufferOut: CVPixelBufferRef ): CVReturn;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

type
	CVPixelBufferReleaseBytesCallback = procedure( releaseRefCon: univ Ptr; baseAddress: {const} univ Ptr );

{!
    @function   CVPixelBufferCreateWithBytes
    @abstract   Call to create a single PixelBuffer for a given size and pixelFormatType based on a passed in piece of memory.
    @discussion Creates a single PixelBuffer for a given size and pixelFormatType. Not all parameters of the pixelBufferAttributes will be used here. It requires a release callback function that will be called, when the PixelBuffer gets destroyed so that the owner of the pixels can free the memory.
    @param      width   Width of the PixelBuffer in pixels
    @param      height  Height of the PixelBuffer in pixels
    @param      pixelFormatType		Pixel format indentified by its respective OSType.
    @param      baseAddress		Address of the memory storing the pixels.
    @param      bytesPerRow		Row bytes of the pixel storage memory.
    @param      releaseCallback         CVPixelBufferReleaseBytePointerCallback function that gets called when the PixelBuffer gets destroyed.
    @param      releaseRefCon           User data identifying the PixelBuffer for the release callback.
    @param      pixelBufferAttributes      A dictionary with additional attributes for a a pixel buffer. This parameter is optional. See PixelBufferAttributes for more details.
    @param      pixelBufferOut          The new pixel buffer will be returned here
    @result	returns kCVReturnSuccess on success.
}
function CVPixelBufferCreateWithBytes( allocator: CFAllocatorRef; width: size_t; height: size_t; pixelFormatType: OSType; baseAddress: univ Ptr; bytesPerRow: size_t; releaseCallback: CVPixelBufferReleaseBytesCallback; releaseRefCon: univ Ptr; pixelBufferAttributes: CFDictionaryRef; var pixelBufferOut: CVPixelBufferRef ): CVReturn;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

type
	CVPixelBufferReleasePlanarBytesCallback = procedure( releaseRefCon: univ Ptr; dataPtr: {const} univ Ptr; dataSize: size_t; numberOfPlanes: size_t; {const} planeAddresses: {variable-size-array} univ Ptr );

{!
    @function   CVPixelBufferCreateWithPlanarBytes
    @abstract   Call to create a single PixelBuffer in planar format for a given size and pixelFormatType based on a passed in piece of memory.
    @discussion Creates a single PixelBuffer for a given size and pixelFormatType. Not all parameters of the pixelBufferAttributes will be used here. It requires a release callback function that will be called, when the PixelBuffer gets destroyed so that the owner of the pixels can free the memory.
    @param      width			Width of the PixelBuffer in pixels
    @param      height			Height of the PixelBuffer in pixels
    @param      pixelFormatType		Pixel format indentified by its respective OSType.
    @param	dataPtr			Pass a pointer to a plane descriptor block, or NULL.
    @param	dataSize		pass size if planes are contiguous, NULL if not.
    @param	numberOfPlanes		Number of planes.
    @param	planeBaseAddress	Array of base addresses for the planes.
    @param	planeWidth		Array of plane widths.
    @param	planeHeight		Array of plane heights.
    @param	planeBytesPerRow	Array of plane bytesPerRow values.
    @param	releaseCallback		CVPixelBufferReleaseBytePointerCallback function that gets called when the PixelBuffer gets destroyed.
    @param	releaseRefCon		User data identifying the PixelBuffer for the release callback.
    @param	pixelBufferAttributes      A dictionary with additional attributes for a a pixel buffer. This parameter is optional. See PixelBufferAttributes for more details.
    @param      pixelBufferOut          The new pixel buffer will be returned here
    @result	returns kCVReturnSuccess on success.
}
function CVPixelBufferCreateWithPlanarBytes( allocator: CFAllocatorRef; width: size_t; height: size_t; pixelFormatType: OSType; dataPtr: {pass a pointer to a plane descriptor block, or NULL} univ Ptr; dataSize: {pass size if planes are contiguous, NULL if not} size_t; numberOfPlanes: size_t; planeAddresses: {variable-size-array} univ Ptr; planeWidth: {variable-size-array} size_t_Ptr; planeHeight: {variable-size-array} size_t_Ptr; planeBytesPerRow: {variable-size-array} size_t_Ptr; releaseCallback: CVPixelBufferReleasePlanarBytesCallback; releaseRefCon: univ Ptr; pixelBufferAttributes: CFDictionaryRef; var pixelBufferOut: CVPixelBufferRef ): CVReturn;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);


{!
	@function   CVPixelBufferLockBaseAddress
	@abstract   Description Locks the BaseAddress of the PixelBuffer to ensure that the memory is accessible.
	@param      pixelBuffer Target PixelBuffer.
	@param      lockFlags See CVPixelBufferLockFlags.
	@result     kCVReturnSuccess if the lock succeeded, or error code on failure
}
function CVPixelBufferLockBaseAddress( pixelBuffer: CVPixelBufferRef; lockFlags: CVOptionFlags ): CVReturn;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
	@function   CVPixelBufferUnlockBaseAddress
	@abstract   Description Unlocks the BaseAddress of the PixelBuffer.
	@param      pixelBuffer Target PixelBuffer.
	@param      unlockFlags See CVPixelBufferLockFlags.
	@result     kCVReturnSuccess if the unlock succeeded, or error code on failure
}
function CVPixelBufferUnlockBaseAddress( pixelBuffer: CVPixelBufferRef; unlockFlags: CVOptionFlags ): CVReturn;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetWidth
    @abstract   Returns the width of the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @result     Width in pixels.
}
function CVPixelBufferGetWidth( pixelBuffer: CVPixelBufferRef ): size_t;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetHeight
    @abstract   Returns the height of the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @result     Height in pixels.
}
function CVPixelBufferGetHeight( pixelBuffer: CVPixelBufferRef ): size_t;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetPixelFormatType
    @abstract   Returns the PixelFormatType of the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @result     OSType identifying the pixel format by its type.
}
function CVPixelBufferGetPixelFormatType( pixelBuffer: CVPixelBufferRef ): OSType;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetBaseAddress
    @abstract   Returns the base address of the PixelBuffer.
    @discussion Retrieving the base address for a PixelBuffer requires that the buffer base address be locked
                via a successful call to CVPixelBufferLockBaseAddress.
    @param      pixelBuffer Target PixelBuffer.
    @result     Base address of the pixels.
		For chunky buffers, this will return a pointer to the pixel at 0,0 in the buffer
		For planar buffers this will return a pointer to a PlanarComponentInfo struct (defined in QuickTime).
}
function CVPixelBufferGetBaseAddress( pixelBuffer: CVPixelBufferRef ): UnivPtr;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetBytesPerRow
    @abstract   Returns the rowBytes of the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @result     Bytes per row of the image data.   For planar buffers this will return a rowBytes value such that bytesPerRow * height
                will cover the entire image including all planes.
}
function CVPixelBufferGetBytesPerRow( pixelBuffer: CVPixelBufferRef ): size_t;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetDataSize
    @abstract   Returns the data size for contigous planes of the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @result     Data size used in CVPixelBufferCreateWithPlanarBytes.
}
function CVPixelBufferGetDataSize( pixelBuffer: CVPixelBufferRef ): size_t;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferIsPlanar
    @abstract   Returns if the PixelBuffer is planar.
    @param      pixelBuffer Target PixelBuffer.
    @result     True if the PixelBuffer was created using CVPixelBufferCreateWithPlanarBytes.
}
function CVPixelBufferIsPlanar( pixelBuffer: CVPixelBufferRef ): Boolean;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetPlaneCount
    @abstract   Returns number of planes of the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @result     Number of planes.  Returns 0 for non-planar CVPixelBufferRefs.
}
function CVPixelBufferGetPlaneCount( pixelBuffer: CVPixelBufferRef ): size_t;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetWidthOfPlane
    @abstract   Returns the width of the plane at planeIndex in the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @param      planeIndex  Identifying the plane.
    @result     Width in pixels, or 0 for non-planar CVPixelBufferRefs.
}
function CVPixelBufferGetWidthOfPlane( pixelBuffer: CVPixelBufferRef; planeIndex: size_t ): size_t;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetHeightOfPlane
    @abstract   Returns the height of the plane at planeIndex in the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @param      planeIndex  Identifying the plane.
    @result     Height in pixels, or 0 for non-planar CVPixelBufferRefs.
}
function CVPixelBufferGetHeightOfPlane( pixelBuffer: CVPixelBufferRef; planeIndex: size_t ): size_t;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetBaseAddressOfPlane
    @abstract   Returns the base address of the plane at planeIndex in the PixelBuffer.
    @discussion Retrieving the base address for a PixelBuffer requires that the buffer base address be locked
                via a successful call to CVPixelBufferLockBaseAddress.
    @param      pixelBuffer Target PixelBuffer.
    @param      planeIndex  Identifying the plane.
    @result     Base address of the plane, or NULL for non-planar CVPixelBufferRefs.
}
function CVPixelBufferGetBaseAddressOfPlane( pixelBuffer: CVPixelBufferRef; planeIndex: size_t ): UnivPtr;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetBytesPerRowOfPlane
    @abstract   Returns the row bytes of the plane at planeIndex in the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @param      planeIndex  Identifying the plane.
    @result     Row bytes of the plane, or NULL for non-planar CVPixelBufferRefs.
}
function CVPixelBufferGetBytesPerRowOfPlane( pixelBuffer: CVPixelBufferRef; planeIndex: size_t ): size_t;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferGetExtendedPixels
    @abstract   Returns the size of extended pixels of the PixelBuffer.
    @param      pixelBuffer Target PixelBuffer.
    @param      extraColumnsOnLeft Returns the pixel row padding to the left.  May be NULL.
    @param      extraRowsOnTop Returns the pixel row padding to the top.  May be NULL. 
    @param      extraColumnsOnRight Returns the pixel row padding to the right. May be NULL.
    @param      extraRowsOnBottom Returns the pixel row padding to the bottom. May be NULL.
}
procedure CVPixelBufferGetExtendedPixels( pixelBuffer: CVPixelBufferRef; var extraColumnsOnLeft: size_t; var extraColumnsOnRight: size_t; var extraRowsOnTop: size_t; var extraRowsOnBottom: size_t );
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);

{!
    @function   CVPixelBufferFillExtendedPixels
    @abstract   Fills the extended pixels of the PixelBuffer.   This function replicates edge pixels to fill the entire extended region of the image.
    @param      pixelBuffer Target PixelBuffer.
}
function CVPixelBufferFillExtendedPixels( pixelBuffer: CVPixelBufferRef ): CVReturn;
__OSX_AVAILABLE_STARTING(__MAC_10_4,__IPHONE_4_0);


end.
