{ CoreGraphics - CGDirectPalette.h
   Copyright (c) 2000-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGDirectPalette;
interface
uses MacTypes,CGDirectDisplay;
{$ALIGN POWER}


{ These types are deprecated; don't use them. }
{ A value in the interval [0, 1]. }
type
	CGPaletteBlendFraction = Float32;

{ A color in a display palette. Values should be in the interval [0, 1]
   where 0 is no color and 1 is full intensity. }

type
	CGDeviceColorPtr = ^CGDeviceColor;
	CGDeviceColor = record
		red: Float32;
		green: Float32;
		blue: Float32;
	end;

{ A color in a display palette, using 8-bit integer components. Values
   should be in the interval [0, 255] where 0 is no color and 255 is full
   intensity. }

	CGDeviceByteColorPtr = ^CGDeviceByteColor;
	CGDeviceByteColor = record
		red: UInt8;
		green: UInt8;
		blue: UInt8;
	end;

{$ifc TARGET_OS_MAC}

{
 * Create a new palette object representing the default 8 bit color palette.
 * Release the palette using CGPaletteRelease().
 }
function CGPaletteCreateDefaultColorPalette: CGDirectPaletteRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Create a copy of the display's current palette, if any.
 * Returns NULL if the current display mode does not support a palette.
 * Release the palette using CGPaletteRelease().
 }
function CGPaletteCreateWithDisplay( display: CGDirectDisplayID ): CGDirectPaletteRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Create a new palette with a capacity as specified.  Entries are initialized from
 * the default color palette.  Release the palette using CGPaletteRelease().
 }
function CGPaletteCreateWithCapacity( capacity: CGTableCount ): CGDirectPaletteRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Create a new palette with a capacity and contents as specified.
 * Release the palette using CGPaletteRelease().
 }
function CGPaletteCreateWithSamples( var sampleTable: CGDeviceColor; sampleCount: CGTableCount ): CGDirectPaletteRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Convenience function:
 * Create a new palette with a capacity and contents as specified.
 * Release the palette using CGPaletteRelease().
 }
function CGPaletteCreateWithByteSamples( var sampleTable: CGDeviceByteColor; sampleCount: CGTableCount ): CGDirectPaletteRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Release a palette
 }
procedure CGPaletteRelease( palette: CGDirectPaletteRef );
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Get the color value at the specified index
 }
function CGPaletteGetColorAtIndex( palette: CGDirectPaletteRef; index: CGTableCount ): CGDeviceColor;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Get the index for the specified color value
 * The index returned is for a palette color with the
 * lowest RMS error to the specified color.
 }
function CGPaletteGetIndexForColor( palette: CGDirectPaletteRef; color: CGDeviceColor ): CGTableCount;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Get the number of samples in the palette
 }
function CGPaletteGetNumberOfSamples( palette: CGDirectPaletteRef ): CGTableCount;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);


{
 * Set the color value at the specified index
 }
procedure CGPaletteSetColorAtIndex( palette: CGDirectPaletteRef; color: CGDeviceColor; index: CGTableCount );
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Copy a palette
 }
function CGPaletteCreateCopy( palette: CGDirectPaletteRef ): CGDirectPaletteRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Compare two palettes
 }
function CGPaletteIsEqualToPalette( palette1: CGDirectPaletteRef; palette2: CGDirectPaletteRef ): Boolean;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{
 * Create a new palette blended with a fraction of a device color.
 * Free the resulting palette with CGPaletteRelease()
 }
function CGPaletteCreateFromPaletteBlendedWithColor( palette: CGDirectPaletteRef; fraction: CGPaletteBlendFraction; color: CGDeviceColor ): CGDirectPaletteRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);

{$endc}

end.
