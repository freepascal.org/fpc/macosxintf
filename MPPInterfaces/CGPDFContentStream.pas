{ CoreGraphics - CGPDFContentStream.h
   Copyright (c) 2004-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFContentStream;
interface
uses MacTypes,CFArray,CGPDFObject,CGBase;
{$ALIGN POWER}


// CGPDFContentStreamRef defined in CGBase


{ Create a content stream from `page'. }

function CGPDFContentStreamCreateWithPage( page: CGPDFPageRef ): CGPDFContentStreamRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Create a content stream from `stream'. }

function CGPDFContentStreamCreateWithStream( stream: CGPDFStreamRef; streamResources: CGPDFDictionaryRef; parent: CGPDFContentStreamRef ): CGPDFContentStreamRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Increment the retain count of `cs'. }

function CGPDFContentStreamRetain( cs: CGPDFContentStreamRef ): CGPDFContentStreamRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Decrement the retain count of `cs'. }

procedure CGPDFContentStreamRelease( cs: CGPDFContentStreamRef );
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the array of CGPDFStreamRefs comprising the entire content stream
   of `cs'. }

function CGPDFContentStreamGetStreams( cs: CGPDFContentStreamRef ): CFArrayRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the resource named `name' in category `category' of the resource
   dictionaries of `cs'. }

function CGPDFContentStreamGetResource( cs: CGPDFContentStreamRef; category: ConstCStringPtr; name: ConstCStringPtr ): CGPDFObjectRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);


end.
