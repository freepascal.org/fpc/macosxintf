{
 *  QLGenerator.h
 *  Quick Look
 *
 *  Copyright 2007-2010 Apple Inc.
 *  All rights reserved.
 *
 }
{ Pascal Translation: Gorazd Krosl <gorazd_1957@yahoo.ca>, November 2009 }
{ Pascal Translation updated: Jonas Maebe <jonas@freepascal.org>, October 2012 }

unit QLGenerator;
interface
uses MacTypes,CFBase,CFURL,CFDictionary,CFArray,CFData,CFBundle,CFUUID,CGGeometry,CGImage,CGContext,QLBase;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}



{ #pragma mark Quick Look Plug-in Info.plist Keys }

{
 * QLThumbnailMinimumSize: (ex: <real>17</real>)
 *      Minimum useful size (in points) of generated thumbnails.
 *      Generator Thumbnail callback won't be called for sizes less than
 *      the value for this key. 17 is a good minimum value.
 *
 * QLPreviewWidth, QLPreviewHeight: (ex: <real>800</real>)
 *      Preview size hint. These values are used if the generator takes
 *      too long to produce the preview.
 *
 * QLSupportsConcurrentRequests: (ex: <true/>)
 *      This tells Quick Look never to call generator callbacks twice at the same time.
 *
 * QLNeedsToBeRunInMainThread: (ex: <false/>)
 *      This tells Quick Look to call generator callbacks in a main thread.
 *
 }

{ #pragma mark Common return code for generator's callback }

const
	kQLReturnMask = $af00;
	kQLReturnNoError = noErr;
	kQLReturnHasMore = (kQLReturnMask or 10);

{ #pragma mark Thumbnail generator callback }

{!
 *      @typedef QLThumbnailRequestRef
 *      @abstract This is the type of a reference to Thumbnail requests.
 }
type
	QLThumbnailRequestRef = ^__QLThumbnailRequest; { an opaque type }
	__QLThumbnailRequest = record end;

{!
 *      @function QLThumbnailRequestGetTypeID
 *      @abstract Returns the CoreFoundation type ID for QLThumbnailRequests.
 }
function QLThumbnailRequestGetTypeID: CFTypeID;

{!
 *      @function QLThumbnailRequestCopyURL
 *      @abstract Returns the url of the file for the thumbnail request.
 *      @param thumbnail The thumbnail request.
 *      @result The url of the file for the thumbnail request.
 }
function QLThumbnailRequestCopyURL( thumbnail: QLThumbnailRequestRef ): CFURLRef;

{!
 *      @function QLThumbnailRequestCopyOptions
 *      @abstract Returns the desired options for the thumbnail request.
 *      @param thumbnail The thumbnail request.
 *      @result The desired options for the thumbnail request.
 }
function QLThumbnailRequestCopyOptions( thumbnail: QLThumbnailRequestRef ): CFDictionaryRef;

{!
 *      @function QLThumbnailRequestCopyContentUTI
 *      @abstract Returns the UTI for the thumbnail request.
 *      @param thumbnail The thumbnail request.
 *      @result The UTI of the content being thumbnailed, NULL if not available.
 }
function QLThumbnailRequestCopyContentUTI( thumbnail: QLThumbnailRequestRef ): CFStringRef;

{!
 *      @function QLThumbnailRequestGetMaximumSize
 *      @abstract Returns the maximum desired size (in points) for the thumbnail request.
 *      @param thumbnail The thumbnail request.
 *      @result The maximum desired size (in points) for the thumbnail request.
 }
function QLThumbnailRequestGetMaximumSize( thumbnail: QLThumbnailRequestRef ): CGSize;

{!
 *      @function QLThumbnailRequestGetGeneratorBundle
 *      @abstract Get the thumbnail request generator bundle.
 *      @param thumbnail The thumbnail request.
 }
function QLThumbnailRequestGetGeneratorBundle( thumbnail: QLThumbnailRequestRef ): CFBundleRef;

{!
 *      @function QLThumbnailRequestSetDocumentObject
 *      @abstract Store some object in thumbnail request.
 *      @param thumbnail The thumbnail request.
 *      @param object The object representing the document
 *      @param callbacks Callbacks to retain/release/etc. the object.
 *      @discussion You can only call this function once per request.
 }
procedure QLThumbnailRequestSetDocumentObject( thumbnail: QLThumbnailRequestRef; objct: {const} univ Ptr; const var callbacks: CFArrayCallBacks );
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @function QLThumbnailRequestGetDocumentObject
 *      @abstract Get the object previously stored with QLThumbnailRequestSetDocumentObject.
 *      @param thumbnail The thumbnail request.
 *      @result The object representing the document
 }
function QLThumbnailRequestGetDocumentObject( thumbnail: QLThumbnailRequestRef ): UnivPtr;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @function QLThumbnailRequestSetImage
 *      @abstract Sets the thumbnail request response to image.
 *      @param thumbnail The thumbnail request.
 *      @param image The thumbnail image response.
 *      @param properties See possible properties below.
 }
procedure QLThumbnailRequestSetImage( thumbnail: QLThumbnailRequestRef; image: CGImageRef; properties: CFDictionaryRef );

{!
 *      @function QLThumbnailRequestSetImageWithData
 *      @abstract Sets the thumbnail request response to image data.
 *      @param thumbnail The thumbnail request.
 *      @param data The thumbnail image response as data. The image format should be supported by ImageIO
 *      @param properties See possible properties below. Additional useful properties: kCGImageSourceTypeIdentifierHint (see ImageIO documentation).
 }
procedure QLThumbnailRequestSetImageWithData( thumbnail: QLThumbnailRequestRef; data: CFDataRef; properties: CFDictionaryRef );

{!
 *      @function QLThumbnailRequestCreateContext
 *      @abstract Creates a graphic context to draw the thumbnail response in.
 *      @param thumbnail The thumbnail request.
 *      @param size Size in points of the context for the thumbnail response.
 *      @param isBitmap True if thumbnail contents is based on bitmap. size will then be interpreted as pixels, not points.
 *      @param properties See possible properties below.
 *      @result A graphic context to draw to.
 *      @discussion Once the thumbnail is fully drawn, you should call QLThumbnailRequestFlushContext().
 }
function QLThumbnailRequestCreateContext( thumbnail: QLThumbnailRequestRef; size: CGSize; isBitmap: Boolean; properties: CFDictionaryRef ): CGContextRef;

{!
 *      @function QLThumbnailRequestFlushContext
 *      @abstract Flushes the graphic context and creates the thumbnail image response.
 *      @param thumbnail The thumbnail request.
 *      @param context The graphic context created by QLThumbnailRequestCreateContext().
 }
procedure QLThumbnailRequestFlushContext( thumbnail: QLThumbnailRequestRef; context: CGContextRef );

{!
 *      @function QLThumbnailRequestSetImageAtURL
 *      @abstract Sets the thumbnail request response to the image contained at url.
 *      @param thumbnail The thumbnail request.
 *      @param url The url to the thumbnail image response.
 *      @param properties Currently unused.
 }
procedure QLThumbnailRequestSetImageAtURL( thumbnail: QLThumbnailRequestRef; url: CFURLRef; properties: CFDictionaryRef );

{!
 *      @function QLThumbnailRequestSetThumbnailWithDataRepresentation
 *      @abstract Sets the thumbnail request response to the image produced by the equivalent preview representation.
 *      @param thumbnail The thumbnail request.
 *      @param data The content data.
 *      @param contentTypeUTI The contentTypeUTI for the preview representation.
 *      @param previewProperties Additional properties for the preview response.
 *      @param properties Currently unused.
 *      @discussion Currently supported UTIs are: none. This call only works if your gemeratpr is set to be run in the main thread
 }
procedure QLThumbnailRequestSetThumbnailWithDataRepresentation( thumbnail: QLThumbnailRequestRef; data: CFDataRef; contentTypeUTI: CFStringRef; previewProperties: CFDictionaryRef; properties: CFDictionaryRef );
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @function QLThumbnailRequestSetThumbnailWithURLRepresentation
 *      @abstract Sets the thumbnail request response to the image produced by the equivalent preview representation.
 *      @param thumbnail The thumbnail request.
 *      @param url The url to the preview response.
 *      @param contentTypeUTI The contentTypeUTI for the preview representation.
 *      @param properties Additional properties for the preview response.
 *      @discussion Currently supported UTIs are: none. This call only works if your gemeratpr is set to be run in the main thread
 }
procedure QLThumbnailRequestSetThumbnailWithURLRepresentation( thumbnail: QLThumbnailRequestRef; url: CFURLRef; contentTypeUTI: CFStringRef; previewProperties: CFDictionaryRef; properties: CFDictionaryRef );
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @function QLThumbnailRequestIsCancelled
 *      @abstract Returns wether the thumbnail request was cancelled or not.
 *      @param thumbnail The thumbnail request.
 *      @result true if the request was cancelled.
 }
function QLThumbnailRequestIsCancelled( thumbnail: QLThumbnailRequestRef ): Boolean;

{!
 *      @constant kQLThumbnailPropertyExtensionKey
 *      @abstract Value should be a CFString. The extension is used as a badge when producing an icon.
 }
const kQLThumbnailPropertyExtensionKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @constant kQLThumbnailPropertyExtensionKey
 *      @abstract Value should be a CGImage. The badge is used when producing an icon.
 }
const kQLThumbnailPropertyBadgeImageKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @constant kQLThumbnailPropertyExtensionKey
 *      @abstract Extends the security scope where Quick Look will accept to look at a file. Value is a path as CFString.
 *      @discussion Only useful when using QLThumbnailRequestSetImageAtURL() or QLThumbnailRequestSetThumbnailWithURLRepresentation().
 *                  By default, Quick Look will only accept files within the current document bundle.
 }
const kQLThumbnailPropertyBaseBundlePathKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{ #pragma mark Preview generator callback }

{!
 *      @typedef QLPreviewRequestRef
 *      This is the type of a reference to Preview requests.
 }
type
	QLPreviewRequestRef = ^__QLPreviewRequest; { an opaque type }
	__QLPreviewRequest = record end;

{!
 *      @function QLPreviewRequestGetTypeID
 *      @abstract Returns the CoreFoundation type ID for QLPreviewRequests.
 }
function QLPreviewRequestGetTypeID: CFTypeID;

{!
 *      @constant kQLPreviewPropertyDisplayNameKey
 *      @abstract Customizes Displayed name in the preview panel. This replaces the document's display name. Value is a CFString.
 }
const kQLPreviewPropertyDisplayNameKey: CFStringRef; // useful to customize the title of the QuickLook panel

{!
 *      @constant kQLPreviewPropertyWidthKey
 *      @abstract Gives the width (in points) of the preview. Value is a CFNumber.
 }
const kQLPreviewPropertyWidthKey: CFStringRef;

{!
 *      @constant kQLPreviewPropertyHeightKey
 *      @abstract Gives the height (in points) of the preview. Value is a CFNumber.
 }
const kQLPreviewPropertyHeightKey: CFStringRef;

{!
 *      @constant kQLPreviewPropertyBaseBundlePathKey
 *      @abstract Extends the security scope where Quick Look will accept to look at a file. Value is a path as CFString.
 *      @discussion Only useful when using QLPreviewRequestSetURLRepresentation().
 *                  By default, Quick Look will only accept files within the current document bundle.
 }
const kQLPreviewPropertyBaseBundlePathKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @constant kQLPreviewPropertyStringEncodingKey
 *      @abstract Gives the CFStringEncoding of the preview data if the preview type is plain text. Value is a CFNumber.
 }
const kQLPreviewPropertyStringEncodingKey: CFStringRef;

const
    kQLPreviewPDFStandardStyle						= 0;
    kQLPreviewPDFPagesWithThumbnailsOnRightStyle	= 3;
    kQLPreviewPDFPagesWithThumbnailsOnLeftStyle		= 4;
type
	QLPreviewPDFStyle = UInt32;

{!
 *      @constant kQLPreviewPropertyPDFStyleKey
 *      @abstract Specify the preferred way to display PDF content. Value is a CFNumber using QLPreviewPDFStyle values.
 }
const kQLPreviewPropertyPDFStyleKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @constant kQLPreviewOptionCursorKey
 *      @abstract Value is the same CFNumber passed by potential previous calls to generator's preview callback for the same document with kQLPreviewPropertyCursorKey.
 *      @discussion Use this value to provide more of the preview content.
 }
const kQLPreviewOptionCursorKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @constant kQLPreviewPropertyCursorKey
 *      @abstract Value should be a CFNumber. This value will be used to get more of the document's preview if necessary
 *                (and if the preview genererator returns kQLReturnHasMore)
 }
const kQLPreviewPropertyCursorKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @function QLPreviewRequestCopyURL
 *      @abstract Returns the url of the file for the preview request.
 *      @param preview The preview request.
 *      @result The url of the file for the preview request.
 }
function QLPreviewRequestCopyURL( preview: QLPreviewRequestRef ): CFURLRef;

{!
 *      @function QLPreviewRequestCopyOptions
 *      @abstract Returns the desired options for the preview request.
 *      @param preview The preview request.
 *      @result The desired options for the preview request.
 }
function QLPreviewRequestCopyOptions( preview: QLPreviewRequestRef ): CFDictionaryRef;

{!
 *      @function QLPreviewRequestCopyContentUTI
 *      @abstract Returns the UTI for the preview request.
 *      @param preview The preview request.
 *      @result The UTI of the content being previewed, NULL if not available.
 }
function QLPreviewRequestCopyContentUTI( preview: QLPreviewRequestRef ): CFStringRef;

{!
 *      @function QLPreviewRequestGetGeneratorBundle
 *      @abstract Gets the preview request generator bundle.
 *      @param preview The preview request.
 }
function QLPreviewRequestGetGeneratorBundle( preview: QLPreviewRequestRef ): CFBundleRef;

{!
 *      @function QLPreviewRequestSetDocumentObject
 *      @abstract Store some object in preview request.
 *      @param thumbnail The preview request.
 *      @param object The object representing the document
 *      @param callbacks Callbacks to retain/release/etc. the object.
 *      @discussion You can only call this function once per request.
 }
procedure QLPreviewRequestSetDocumentObject( preview: QLPreviewRequestRef; objct: {const} univ Ptr; const var callbacks: CFArrayCallBacks );
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @function QLPreviewRequestGetDocumentObject
 *      @abstract Get the object previously stored with QLPreviewRequestSetDocumentObject.
 *      @param preview The preview request.
 *      @result The object representing the document
 }
function QLPreviewRequestGetDocumentObject( preview: QLPreviewRequestRef ): UnivPtr;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

{!
 *      @function QLPreviewRequestIsCancelled
 *      @abstract Returns wether the preview request was cancelled or not.
 *      @param preview The preview request.
 *      @result true if the request was cancelled.
 }
function QLPreviewRequestIsCancelled( preview: QLPreviewRequestRef ): Boolean;

{ #pragma mark Set preview to some native type preview data }

{!
 * @function QLPreviewRequestSetDataRepresentation
 * @abstract Sets the preview response with the provided data.
 * @param preview The preview request.
 * @param data The content data.
 * @param contentTypeUTI The contentTypeUTI for the preview representation.
 * @param properties Additional properties for the preview response.
 * @discussion Currently supported UTIs are: kUTTypeImage, kUTTypePDF, kUTTypeHTML,
 *             kUTTypeXML, kUTTypePlainText, kUTTypeRTF, kUTTypeMovie, kUTTypeAudio
 }
procedure QLPreviewRequestSetDataRepresentation( preview: QLPreviewRequestRef; data: CFDataRef; contentTypeUTI: CFStringRef; properties: CFDictionaryRef );

{!
 *      @function QLPreviewRequestSetURLRepresentation
 *      @abstract Sets the preview request response with contents at url.
 *      @param preview The preview request.
 *      @param url The url to the preview response.
 *      @param contentTypeUTI The contentTypeUTI for the preview representation.
 *      @param properties Additional properties for the preview response.
 *      @discussion Currently supported UTIs are: kUTTypeImage, kUTTypePDF, kUTTypeHTML, kUTTypeXML, kUTTypePlainText, kUTTypeRTF, kUTTypeRTFD, kUTTypeMovie, kUTTypeAudio
 }
procedure QLPreviewRequestSetURLRepresentation( preview: QLPreviewRequestRef; url: CFURLRef; contentTypeUTI: CFStringRef; properties: CFDictionaryRef );

{ #pragma mark Draw preview in a context }

{!
 *      @function QLPreviewRequestCreateContext
 *      @abstract Creates a context to draw the preview in. Context should be flushed with QLPreviewRequestFlushContext()
 *      @param preview The preview request.
 *      @param size The size of the context.
 *      @param isBitmap true if preview is bitmap-based.
 *      @param properties Additional properties for the preview response.
 }
function QLPreviewRequestCreateContext( preview: QLPreviewRequestRef; size: CGSize; isBitmap: Boolean; properties: CFDictionaryRef ): CGContextRef;

{!
 *      @function QLPreviewRequestCreatePDFContext
 *      @abstract Creates a PDF context to draw the preview in, likely to be multi-pages. Context should be flushed with QLPreviewRequestFlushContext()
 *      @param preview The preview request.
 *      @param mediaBox The media box of the context. see CGPDFContextCreate().
 *      @param auxiliaryInfo The PDF auxiliary info. see CGPDFContextCreate().
 *      @param properties Additional properties for the preview response.
 }
function QLPreviewRequestCreatePDFContext( preview: QLPreviewRequestRef; const var mediaBox: CGRect; auxiliaryInfo: CFDictionaryRef; properties: CFDictionaryRef ): CGContextRef;


{!
 *      @function QLPreviewRequestFlushContext
 *      @abstract Flush the context and sets the preview response.
 *      @param preview The preview request.
 *      @param context context previously created by QLPreviewRequestCreateContext() or QLPreviewRequestCreatePDFContext().
 }

procedure QLPreviewRequestFlushContext( preview: QLPreviewRequestRef; context: CGContextRef );

{ #pragma mark Provide preview as Web content using QLPreviewRequestSetDataRepresentation }

{!
 *      @constant kQLPreviewPropertyMIMETypeKey
 *      @abstract Gives the web content or attachment mime type. For the main data, default is text/html. Value is a CFString.
 }
const kQLPreviewPropertyMIMETypeKey: CFStringRef;

{!
 *      @constant kQLPreviewPropertyTextEncodingNameKey
 *      @abstract Gives the web content or attachment text encoding. Use IANA encodings like UTF-8. Value is a CFString.
 }
const kQLPreviewPropertyTextEncodingNameKey: CFStringRef;

{!
 *      @constant kQLPreviewPropertyAttachmentDataKey
 *      @abstract Gives the attachment data. Value is a CFData.
 }
const kQLPreviewPropertyAttachmentDataKey: CFStringRef;

{!
 *      @constant kQLPreviewPropertyAttachmentsKey
 *      @abstract Gives the list of attachments (or sub-resources). Value is a CFDictionary.
 *      @discussion Keys are the attachment ids (CFStringRef) that can be referenced with "cid:id" URL and
 *                  Values are dictionaries using kQLPreviewPropertyAttachmentDataKey,
 *                  kQLPreviewPropertyMIMETypeKey and kQLPreviewPropertyTextEncodingNameKey keys.
 }
const kQLPreviewPropertyAttachmentsKey: CFStringRef;

{!
 *      @constant kQLPreviewContentIDScheme
 *      @abstract Is the "cid" URL scheme.
 }
const kQLPreviewContentIDScheme: CFStringRef;


{
 * Definition of the Quick Look Generator interface.
 *
 }
{FPC-ONLY-START}

{ 5E2D9680-5022-40FA-B806-43349622E5B9 }
implemented function kQLGeneratorTypeID : CFUUIDRef; inline;
{ 865AF5E0-6D30-4345-951B-D37105754F2D }
implemented function kQLGeneratorCallbacksInterfaceID: CFUUIDRef; inline; 

{FPC-ONLY-ELSE}

{$mwgpcdefinec kQLGeneratorTypeID CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault, $5E, $2D, $96, $80, $50, $22, $40, $FA, $B8, $06, $43, $34, $96, $22, $E5, $B9)}
{ 865AF5E0-6D30-4345-951B-D37105754F2D }
{$mwgpcdefinec kQLGeneratorCallbacksInterfaceID CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault, $86, $5A, $F5, $E0, $6D, $30, $43, $45, $95, $1B, $D3, $71, $05, $75, $4F, $2D)}

{FPC-ONLY-FINISH}

{$endif} {TARGET_OS_MAC}

implementation

{$ifc TARGET_OS_MAC}

{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

{FPC-ONLY-START}
function kQLGeneratorTypeID : CFUUIDRef; inline;
begin
	kQLGeneratorTypeID := CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault, $5E, $2D, $96, $80, $50, $22, $40, $FA, $B8, $06, $43, $34, $96, $22, $E5, $B9)
end;

function kQLGeneratorCallbacksInterfaceID: CFUUIDRef; inline; 
begin
	kQLGeneratorCallbacksInterfaceID := CFUUIDGetConstantUUIDWithBytes(kCFAllocatorDefault, $86, $5A, $F5, $E0, $6D, $30, $43, $45, $95, $1B, $D3, $71, $05, $75, $4F, $2D)
end;
{FPC-ONLY-FINISH}

{$endc} {TARGET_OS_MAC}

end.
