{
 *	CTGlyphInfo.h
 *	CoreText
 *
 *	Copyright (c) 2006-2012 Apple Inc. All rights reserved.
 *
 }
{  Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CTGlyphInfo;
interface
uses MacTypes,CTFont,CFBase,CGFont;

{$ALIGN POWER}


{!
    @header

    Thread Safety Information

    All functions in this header are thread safe unless otherwise specified.
}


{ --------------------------------------------------------------------------- }
{ Glyph Info Types }
{ --------------------------------------------------------------------------- }

type
	CTGlyphInfoRef = ^__CTGlyphInfo; { an opaque type }
	__CTGlyphInfo = record end;
	CTGlyphInfoRefPtr = ^CTGlyphInfoRef;

{!
	@function	CTGlyphInfoGetTypeID
	@abstract	Returns the CFType of the glyph info object
}

function CTGlyphInfoGetTypeID: CFTypeID;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);


{ --------------------------------------------------------------------------- }
{ Glyph Info Values }
{ --------------------------------------------------------------------------- }

{!
	@enum		CTCharacterCollection
	@abstract	These constants specify character collections.

	@constant	kCTCharacterCollectionIdentityMapping
				Indicates that the character identifier is equal to the CGGlyph
				glyph index.

	@constant	kCTCharacterCollectionAdobeCNS1
				Indicates the Adobe-CNS1 mapping.

	@constant	kCTCharacterCollectionAdobeGB1
				Indicates the Adobe-GB1 mapping.

	@constant	kCTCharacterCollectionAdobeJapan1
				Indicates the Adobe-Japan1 mapping.

	@constant	kCTCharacterCollectionAdobeJapan2
				Indicates the Adobe-Japan2 mapping.

	@constant	kCTCharacterCollectionAdobeKorea1
				Indicates the Adobe-Korea1 mapping.
}

const
	kCTCharacterCollectionIdentityMapping   = 0;
	kCTCharacterCollectionAdobeCNS1         = 1;
	kCTCharacterCollectionAdobeGB1          = 2;
	kCTCharacterCollectionAdobeJapan1       = 3;
	kCTCharacterCollectionAdobeJapan2       = 4;
	kCTCharacterCollectionAdobeKorea1       = 5;

	kCTIdentityMappingCharacterCollection = kCTCharacterCollectionIdentityMapping;
	kCTAdobeCNS1CharacterCollection = kCTCharacterCollectionAdobeCNS1;
	kCTAdobeGB1CharacterCollection = kCTCharacterCollectionAdobeGB1;
	kCTAdobeJapan1CharacterCollection = kCTCharacterCollectionAdobeJapan1;
	kCTAdobeJapan2CharacterCollection = kCTCharacterCollectionAdobeJapan2;
	kCTAdobeKorea1CharacterCollection = kCTCharacterCollectionAdobeKorea1;
type
	CTCharacterCollection = UInt16;


{ --------------------------------------------------------------------------- }
{ Glyph Info Creation }
{ --------------------------------------------------------------------------- }

{!
	@function	CTGlyphInfoCreateWithGlyphName
	@abstract	Creates an immutable glyph info object.

	@discussion This function creates an immutable glyph info object for a glyph
				name such as "copyright" and a specified font.

	@param		glyphName
				The name of the glyph.

	@param		font
				The font to be associated with the returned CTGlyphInfo object.

	@param		baseString
				The part of the string the returned object is intended
				to override.

	@result		If glyph info creation was successful, this function will return
				a valid reference to an immutable CTGlyphInfo object. Otherwise,
				this function will return NULL.
}

function CTGlyphInfoCreateWithGlyphName( glyphName: CFStringRef; font: CTFontRef; baseString: CFStringRef ): CTGlyphInfoRef;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);


{!
	@function	CTGlyphInfoCreateWithGlyph
	@abstract	Creates an immutable glyph info object.

	@discussion This function creates an immutable glyph info object for a glyph
				index and a specified font.

	@param		glyph
				The glyph identifier.

	@param		font
				The font to be associated with the returned CTGlyphInfo object.

	@param		baseString
				The part of the string the returned object is intended
				to override.

	@result		If glyph info creation was successful, this function will return
				a valid reference to an immutable CTGlyphInfo object. Otherwise,
				this function will return NULL.
}

function CTGlyphInfoCreateWithGlyph( glyph: CGGlyph; font: CTFontRef; baseString: CFStringRef ): CTGlyphInfoRef;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);


{!
	@function	CTGlyphInfoCreateWithCharacterIdentifier
	@abstract	Creates an immutable glyph info object.

	@discussion This function creates an immutable glyph info object for a
				character identifier and a character collection.

	@param		cid
				A character identifier.

	@param		collection
				A character collection identifier.

	@param		baseString
				The part of the string the returned object is intended
				to override.

	@result		If glyph info creation was successful, this function will return
				a valid reference to an immutable CTGlyphInfo object. Otherwise,
				this function will return NULL.
}

function CTGlyphInfoCreateWithCharacterIdentifier( cid: CGFontIndex; collection: CTCharacterCollection; baseString: CFStringRef ): CTGlyphInfoRef;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);


{ --------------------------------------------------------------------------- }
{ Glyph Info Access }
{ --------------------------------------------------------------------------- }

{!
	@function	CTGlyphInfoGetGlyphName
	@abstract	Gets the glyph name for a glyph info, if applicable.

	@discussion This function will return the glyph name.

	@param		glyphInfo
				The glyph info for which you would like the glyph name. This
				parameter may not be set to NULL.

	@result		If the glyph info object was created with a glyph name, it will
				be returned. Otherwise, this function will return NULL.
}

function CTGlyphInfoGetGlyphName( glyphInfo: CTGlyphInfoRef ): CFStringRef;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);


{!
	@function	CTGlyphInfoGetCharacterIdentifier
	@abstract	Gets the character identifier for a glyph info.

	@discussion This function will return the character identifier.

	@param		glyphInfo
				The glyph info for which you would like the character identifier.
				This parameter may not be set to NULL.

	@result		If the glyph info object was created with a character identifier,
				it will be returned. Otherwise, this function will return 0.
}

function CTGlyphInfoGetCharacterIdentifier( glyphInfo: CTGlyphInfoRef ): CGFontIndex;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);


{!
	@function	CTGlyphInfoGetCharacterCollection
	@abstract	Gets the character collection for a glyph info.

	@discussion This function will return the character collection. If the glyph
				info object was created with a glyph name or a glyph index, its
				character collection will be
				kCTIdentityMappingCharacterCollection.

	@param		glyphInfo
				The glyph info for which you would like the character collection.
				This parameter may not be set to NULL.

	@result		This function will return the character collection of the given
				glyph info.
}

function CTGlyphInfoGetCharacterCollection( glyphInfo: CTGlyphInfoRef ): CTCharacterCollection;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);


end.
