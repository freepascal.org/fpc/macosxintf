{
 *  CVPixelBufferIOSurface.h
 *  CoreVideo
 *
 *  Copyright (c) 2010 Apple Computer, Inc. All rights reserved.
 *
 }
{  Initial Pascal Translation: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }
unit CVPixelBufferIOSurface;
interface
uses MacTypes,CFBase,CFDictionary,IOSurfaceAPI,CVPixelBuffer,CVReturns;
{$ALIGN POWER}

 
  {! @header CVPixelBufferIOSurface.h
	@copyright 2010 Apple Computer, Inc. All rights reserved.
	@availability Mac OS X 10.4 or later
    @discussion routines for accessing and manipulating IOSurface backings for CVPixelBuffers
		   
}

{$ifc (TARGET_OS_IPHONE and TARGET_OS_EMBEDDED) or TARGET_OS_MAC}
{$definec COREVIDEO_SUPPORTS_IOSURFACE TRUE}
{$elsec}
{$definec COREVIDEO_SUPPORTS_IOSURFACE FALSE}
{$endc}

{$ifc TARGET_OS_MAC}
// Ensures that CGLTexImageIOSurface2D() will succeed in creating a valid texture object from the CVPixelBuffer's IOSurface.
const kCVPixelBufferIOSurfaceOpenGLTextureCompatibilityKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;	// CFBoolean
// Ensures that CGLTexImageIOSurface2D() will succeed in creating a valid texture object from the CVPixelBuffer's IOSurface AND that the resulting texture may be used as a color buffer attachment to a OpenGL frame buffer object.
const kCVPixelBufferIOSurfaceOpenGLFBOCompatibilityKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;	// CFBoolean
// Ensures that the CVPixelBuffer's IOSurfaceRef can be displayed in an CoreAnimation CALayer.
const kCVPixelBufferIOSurfaceCoreAnimationCompatibilityKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;	// CFBoolean
{$endc} {TARGET_OS_MAC}

{$ifc TARGET_OS_IPHONE}
// Ensures that OpenGLES can create a valid texture object from IOSurface-backed CVPixelBuffers.
const kCVPixelBufferIOSurfaceOpenGLESTextureCompatibilityKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_5_0);	// CFBoolean
// Ensures that OpenGLES can create a valid texture object from IOSurface-backed CVPixelBuffers AND that the resulting texture may be used as a color buffer attachment to a OpenGLES frame buffer object.
const kCVPixelBufferIOSurfaceOpenGLESFBOCompatibilityKey: CFStringRef;
__OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_5_0);	// CFBoolean
{$endc}

{$ifc COREVIDEO_SUPPORTS_IOSURFACE}
{!
	@function   CVPixelBufferGetIOSurface
	@abstract   Returns the IOSurface backing the pixel buffer, or NULL if it is not backed by an IOSurface.
	@param      pixelBuffer Target PixelBuffer.
}
function CVPixelBufferGetIOSurface( pixelBuffer: CVPixelBufferRef ): IOSurfaceRef;
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0);

{!
    @function   CVPixelBufferCreateWithIOSurface
    @abstract   Call to create a single CVPixelBuffer for a passed-in IOSurface.
    @discussion The CVPixelBuffer will retain the IOSurface.
    	IMPORTANT NOTE: If you are using IOSurface to share CVPixelBuffers between processes
    	and those CVPixelBuffers are allocated via a CVPixelBufferPool, it is important
    	that the CVPixelBufferPool does not reuse CVPixelBuffers whose IOSurfaces are still
    	in use in other processes.  
    	CoreVideo and IOSurface will take care of this for if you use IOSurfaceCreateMachPort 
    	and IOSurfaceLookupFromMachPort, but NOT if you pass IOSurfaceIDs.
    @param      surface		            The IOSurface to wrap.
    @param      pixelBufferAttributes   A dictionary with additional attributes for a a pixel buffer. This parameter is optional. See PixelBufferAttributes for more details.
    @param      pixelBufferOut          The new pixel buffer will be returned here
    @result     returns kCVReturnSuccess on success.
}
function CVPixelBufferCreateWithIOSurface( allocator: CFAllocatorRef; surface: IOSurfaceRef; pixelBufferAttributes: CFDictionaryRef; var pixelBufferOut: CVPixelBufferRef ): CVReturn;
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0);

{$endc} {COREVIDEO_SUPPORTS_IOSURFACE}


end.
