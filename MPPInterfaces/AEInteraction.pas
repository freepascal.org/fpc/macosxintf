{
     File:       HIToolbox/AEInteraction.h
 
     Contains:   AppleEvent functions that deal with Events and interacting with user
 
     Version:    HIToolbox-624~3
 
     Copyright:  � 2000-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit AEInteraction;
interface
uses MacTypes,QuickdrawTypes,AEDataModel,Notification,Events,CarbonEventsCore;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}


{*************************************************************************
  AppleEvent callbacks. 
*************************************************************************}
type
	AEIdleProcPtr = function( var theEvent: EventRecord; var sleepTime: SInt32; var mouseRgn: RgnHandle ): Boolean;
	AEFilterProcPtr = function( var theEvent: EventRecord; returnID: SInt32; transactionID: AETransactionID; const var sender: AEAddressDesc ): Boolean;
{GPC-ONLY-START}
	AEIdleUPP = UniversalProcPtr; // should be AEIdleProcPtr
{GPC-ONLY-ELSE}
	AEIdleUPP = AEIdleProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	AEFilterUPP = UniversalProcPtr; // should be AEFilterProcPtr
{GPC-ONLY-ELSE}
	AEFilterUPP = AEFilterProcPtr;
{GPC-ONLY-FINISH}

{*************************************************************************
  The next couple of calls are basic routines used to create, send,
  and process AppleEvents. 
*************************************************************************}
{
 *  AESend()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AESend( const var theAppleEvent: AppleEvent; var reply: AppleEvent; sendMode: AESendMode; sendPriority: AESendPriority; timeOutInTicks: SInt32; idleProc: AEIdleUPP { can be NULL }; filterProc: AEFilterUPP { can be NULL } ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  AEProcessAppleEvent()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEProcessAppleEvent( const var theEventRecord: EventRecord ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  AEProcessEvent()
 *  
 *  Summary:
 *    Dispatches a Carbon event of type kEventAppleEvent to the
 *    appropriate AppleEvent handlers.
 *  
 *  Discussion:
 *    This API is similar to AEProcessAppleEvent, but does not require
 *    the Carbon event to be converted to an EventRecord. Also, unlike
 *    AEProcessAppleEvent, this API does not require that an event be
 *    removed from its event queue before processing; the AppleEvent
 *    will be correctly dispatched even if the Carbon event is still in
 *    its event queue. Of course, you should still remove the Carbon
 *    event from its event queue later once you're done handling it,
 *    and it is also acceptable to remove it from the event queue
 *    before calling this API.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    inEvent:
 *      A Carbon event of class kEventClassAppleEvent and kind
 *      kEventAppleEvent.
 *  
 *  Result:
 *    The operating system result code returned by the AppleEvent
 *    handler, or paramErr if the event passed to this API is not of
 *    the correct class and kind.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in Carbon.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function AEProcessEvent( inEvent: EventRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;


{ 
 Note: during event processing, an event handler may realize that it is likely
 to exceed the client's timeout limit. Passing the reply to this
 routine causes a wait event to be generated that asks the client
 for more time. 
}
{
 *  AEResetTimer()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEResetTimer( const var reply: AppleEvent ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{*************************************************************************
  The following three calls are used to allow applications to behave
  courteously when a user interaction such as a dialog box is needed. 
*************************************************************************}

type
	AEInteractAllowed = SInt8;
const
	kAEInteractWithSelf = 0;
	kAEInteractWithLocal = 1;
	kAEInteractWithAll = 2;

{
 *  AEGetInteractionAllowed()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEGetInteractionAllowed( var level: AEInteractAllowed ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  AESetInteractionAllowed()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AESetInteractionAllowed( level: AEInteractAllowed ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  AEInteractWithUser()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEInteractWithUser( timeOutInTicks: SInt32; nmReqPtr: NMRecPtr; idleProc: AEIdleUPP ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{*************************************************************************
 The following four calls are available for applications which need more
 sophisticated control over when and how events are processed. Applications
 which implement multi-session servers or which implement their own
 internal event queueing will probably be the major clients of these
 routines. They can be called from within a handler to prevent the AEM from
 disposing of the AppleEvent when the handler returns. They can be used to
 asynchronously process the event (as MacApp does).
*************************************************************************}
{
 *  AESuspendTheCurrentEvent()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AESuspendTheCurrentEvent( const var theAppleEvent: AppleEvent ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ 
 Note: The following routine tells the AppleEvent manager that processing
 is either about to resume or has been completed on a previously suspended
 event. The procPtr passed in as the dispatcher parameter will be called to
 attempt to redispatch the event. Several constants for the dispatcher
 parameter allow special behavior. They are:
    - kAEUseStandardDispatch means redispatch as if the event was just
      received, using the standard AppleEvent dispatch mechanism.
    - kAENoDispatch means ignore the parameter.
      Use this in the case where the event has been handled and no
      redispatch is needed.
    - non nil means call the routine which the dispatcher points to.
}
{ Constants for Refcon in AEResumeTheCurrentEvent with kAEUseStandardDispatch }
const
	kAEDoNotIgnoreHandler = $00000000;
	kAEIgnoreAppPhacHandler = $00000001; { available only in vers 1.0.1 and greater }
	kAEIgnoreAppEventHandler = $00000002; { available only in vers 1.0.1 and greater }
	kAEIgnoreSysPhacHandler = $00000004; { available only in vers 1.0.1 and greater }
	kAEIgnoreSysEventHandler = $00000008; { available only in vers 1.0.1 and greater }
	kAEIngoreBuiltInEventHandler = $00000010; { available only in vers 1.0.1 and greater }
	kAEDontDisposeOnResume = $80000000; { available only in vers 1.0.1 and greater }

{ Constants for AEResumeTheCurrentEvent }
const
	kAENoDispatch = 0;    { dispatch parameter to AEResumeTheCurrentEvent takes a pointer to a dispatch }
	kAEUseStandardDispatch = $FFFFFFFF; { table, or one of these two constants }

{
 *  AEResumeTheCurrentEvent()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEResumeTheCurrentEvent( const var theAppleEvent: AppleEvent; const var reply: AppleEvent; dispatcher: AEEventHandlerUPP { can be NULL }; handlerRefcon: SRefCon ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  AEGetTheCurrentEvent()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AEGetTheCurrentEvent( var theAppleEvent: AppleEvent ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  AESetTheCurrentEvent()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function AESetTheCurrentEvent( const var theAppleEvent: AppleEvent ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{*************************************************************************
  AppleEvent callbacks. 
*************************************************************************}
{
 *  NewAEIdleUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewAEIdleUPP( userRoutine: AEIdleProcPtr ): AEIdleUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewAEFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewAEFilterUPP( userRoutine: AEFilterProcPtr ): AEFilterUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeAEIdleUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeAEIdleUPP( userUPP: AEIdleUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeAEFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeAEFilterUPP( userUPP: AEFilterUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeAEIdleUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeAEIdleUPP( var theEvent: EventRecord; var sleepTime: SInt32; var mouseRgn: RgnHandle; userUPP: AEIdleUPP ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeAEFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeAEFilterUPP( var theEvent: EventRecord; returnID: SInt32; transactionID: AETransactionID; const var sender: AEAddressDesc; userUPP: AEFilterUPP ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{$endc} {TARGET_OS_MAC}

end.
