{
 *  AXValueConstants.h
 *  HIServices
 *
 *  Created by John Louch on Wed Feb 25 2004.
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }

{	 Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 (no changes in 10.6) }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }

unit AXValueConstants;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


// orientations (see kAXOrientationAttribute in AXAttributeConstants.h)
const kAXHorizontalOrientationValue = CFSTR( 'AXHorizontalOrientation' );
const kAXVerticalOrientationValue = CFSTR( 'AXVerticalOrientation' );
const kAXUnknownOrientationValue = CFSTR( 'AXUnknownOrientation' );

// sort directions (see kAXSortDirectionAttribute in AXAttributeConstants.h)
const kAXAscendingSortDirectionValue = CFSTR( 'AXAscendingSortDirection' );
const kAXDescendingSortDirectionValue = CFSTR( 'AXDescendingSortDirection' );
const kAXUnknownSortDirectionValue = CFSTR( 'AXUnknownSortDirection' );

{$endc} {TARGET_OS_MAC}

end.
