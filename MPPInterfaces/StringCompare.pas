{
     File:       CarbonCore/StringCompare.h
 
     Contains:   Public interfaces for String Comparison and related operations
                 The contents of this header file are deprecated.
 
     Copyright:  � 1985-2011 by Apple Inc., all rights reserved.
}
unit StringCompare;
interface
uses MacTypes,MixedMode,TextCommon,Script;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{

    Here are the current System 7 routine names and the translations to the older forms.
    Please use the newer forms in all new code and migrate the older names out of existing
    code as maintenance permits.
    
    NEW NAME                    OLD NAME                    OBSOLETE FORM (no handle)
    
    CompareString (Str255)      IUCompPString (hp only)     IUCompString (hp only)
    CompareText (ptr/len)       IUMagPString                IUMagString
    IdenticalString (Str255)    IUEqualPString (hp only)    IUEqualString  (hp only)
    IdenticalText (ptr/len)     IUMagIDPString              IUMagIDString
    LanguageOrder               IULangOrder
    ScriptOrder                 IUScriptOrder
    StringOrder (Str255)        IUStringOrder (hp only)
    TextOrder (ptr/len)         IUTextOrder

    RelString
    CmpString (a only)                  
    EqualString (hp only)
    
    ReplaceText

    Carbon only supports the new names.  The old names are undefined for Carbon targets.

    InterfaceLib always has exported the old names.  For C macros have been defined to allow
    the use of the new names.  For Pascal and Assembly using the new names will result
    in link errors. 
    
}

const
{ Special language code values for Language Order}
	systemCurLang = -2;   { current (itlbLang) lang for system script}
	systemDefLang = -3;   { default (table) lang for system script}
	currentCurLang = -4;   { current (itlbLang) lang for current script}
	currentDefLang = -5;   { default lang for current script}
	scriptCurLang = -6;   { current (itlbLang) lang for specified script}
	scriptDefLang = -7;    { default language for a specified script}

{ obsolete names}
const
	iuSystemCurLang = systemCurLang;
	iuSystemDefLang = systemDefLang;
	iuCurrentCurLang = currentCurLang;
	iuCurrentDefLang = currentDefLang;
	iuScriptCurLang = scriptCurLang;
	iuScriptDefLang = scriptDefLang;


{
 *  These routines are available in Carbon with the new names.
 }
{$ifc not TARGET_CPU_64}
{
 *  [Mac]ReplaceText()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringReplace instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringReplace instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function MacReplaceText( baseText: Handle; substitutionText: Handle; key: Str15 ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  ScriptOrder()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare or UCCompareText instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare or UCCompareText instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function ScriptOrder( script1: ScriptCode; script2: ScriptCode ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  [Mac]CompareString()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare or UCCompareText instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare or UCCompareText instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function MacCompareString( const var aStr: Str255; const var bStr: Str255; itl2Handle: Handle ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  IdenticalString()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function IdenticalString( const var aStr: Str255; const var bStr: Str255; itl2Handle: Handle ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  StringOrder()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare or UCCompareText instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare or UCCompareText instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function StringOrder( const var aStr: Str255; const var bStr: Str255; aScript: ScriptCode; bScript: ScriptCode; aLang: LangCode; bLang: LangCode ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  CompareText()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare or UCCompareText instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare or UCCompareText instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function CompareText( aPtr: {const} univ Ptr; bPtr: {const} univ Ptr; aLen: SInt16; bLen: SInt16; itl2Handle: Handle ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  IdenticalText()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function IdenticalText( aPtr: {const} univ Ptr; bPtr: {const} univ Ptr; aLen: SInt16; bLen: SInt16; itl2Handle: Handle ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  TextOrder()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare or UCCompareText instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare or UCCompareText instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function TextOrder( aPtr: {const} univ Ptr; bPtr: {const} univ Ptr; aLen: SInt16; bLen: SInt16; aScript: ScriptCode; bScript: ScriptCode; aLang: LangCode; bLang: LangCode ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  LanguageOrder()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare or UCCompareText instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare or UCCompareText instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function LanguageOrder( language1: LangCode; language2: LangCode ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  These routines are available in InterfaceLib with old names.
 *  Macros are provided for C to allow source code use to the new names.
 }
{$endc} {not TARGET_CPU_64}

{
 *  IUMagPString()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUMagIDPString()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUTextOrder()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IULangOrder()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUScriptOrder()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUMagString()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUMagIDString()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUCompPString()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUEqualPString()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUStringOrder()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUCompString()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  IUEqualString()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  iucomppstring()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  iuequalpstring()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  iustringorder()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  iucompstring()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{
 *  iuequalstring()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{$ifc not TARGET_CPU_64}
{
 *  RelString()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare or UCCompareText instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare or UCCompareText instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function RelString( const var str1: Str255; const var str2: Str255; caseSensitive: Boolean; diacSensitive: Boolean ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  EqualString()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function EqualString( const var str1: Str255; const var str2: Str255; caseSensitive: Boolean; diacSensitive: Boolean ): Boolean;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{$endc} {not TARGET_CPU_64}

{$ifc not TARGET_CPU_64}
{
 *  relstring()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    use CFStringCompare or UCCompareText instead.
 *  
 *  Discussion:
 *    This function is no longer recommended. Please use
 *    CFStringCompare or UCCompareText instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
(*
no function overloading

function relstring( str1: ConstCStringPtr; str2: ConstCStringPtr; caseSensitive: Boolean; diacSensitive: Boolean ): SInt16;
*)
// AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  equalstring()
 *  
 *  Availability:
 *    Mac OS X:         not available [32-bit only]
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }


{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
