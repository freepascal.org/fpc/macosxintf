{ CoreGraphics - CGGeometry.h
   Copyright (c) 1998-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2015 }
unit CGGeometry;
interface
uses MacTypes,CFBase,CFDictionary,CGBase;
{$ALIGN POWER}


{ Points. }

type
	CGPointPtr = ^CGPoint;
	CGPoint = record
		x: CGFloat;
		y: CGFloat;
	end;

{ Sizes. }

type
	CGSizePtr = ^CGSize;
	CGSize = record
		width: CGFloat;
		height: CGFloat;
	end;

{ Vectors. }

const
	CGVECTOR_DEFINED = 1;

type
  CGVectorPtr = ^CGVector;
	CGVector = record
		dx: CGFloat;
		dy: CGFloat;
	end;

{ Rectangles. }

type
	CGRectPtr = ^CGRect;
	CGRect = record
		origin: CGPoint;
		size: CGSize;
	end;

{ Rectangle edges. }

type
	CGRectEdge = SInt32;
const
	CGRectMinXEdge = 0;
	CGRectMinYEdge = 1;
	CGRectMaxXEdge = 2;
	CGRectMaxYEdge = 3;

{ The "zero" point -- equivalent to CGPointMake(0, 0). } 

const CGPointZero: CGPoint;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ The "zero" size -- equivalent to CGSizeMake(0, 0). } 

const CGSizeZero: CGSize;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ The "zero" rectangle -- equivalent to CGRectMake(0, 0, 0, 0). } 

const CGRectZero: CGRect;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ The "empty" rect. This is the rectangle returned when, for example, we
   intersect two disjoint rectangles. Note that the null rect is not the
   same as the zero rect. }

const CGRectNull: CGRect;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ The infinite rectangle. }

const CGRectInfinite: CGRect;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Return the leftmost x-value of `rect'. }

function CGRectGetMinX( rect: CGRect ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the midpoint x-value of `rect'. }

function CGRectGetMidX( rect: CGRect ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the rightmost x-value of `rect'. }

function CGRectGetMaxX( rect: CGRect ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the bottommost y-value of `rect'. }

function CGRectGetMinY( rect: CGRect ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the midpoint y-value of `rect'. }

function CGRectGetMidY( rect: CGRect ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the topmost y-value of `rect'. }

function CGRectGetMaxY( rect: CGRect ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the width of `rect'. }

function CGRectGetWidth( rect: CGRect ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the height of `rect'. }

function CGRectGetHeight( rect: CGRect ): CGFloat;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{GPC-MW-ONLY-START}
{ Return true if `point1' and `point2' are the same, false otherwise. }

// inline definition available for FPC
function CGPointEqualToPoint( point1: CGPoint; point2: CGPoint ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `size1' and `size2' are the same, false otherwise. }

// inline definition available for FPC
function CGSizeEqualToSize( size1: CGSize; size2: CGSize ): CBool;
// CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);
{GPC-MW-ONLY-FINISH}


{ Return true if `rect1' and `rect2' are the same, false otherwise. }

function CGRectEqualToRect( rect1: CGRect; rect2: CGRect ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Standardize `rect' -- i.e., convert it to an equivalent rect which has
   positive width and height. }

function CGRectStandardize( rect: CGRect ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `rect' is empty (that is, if it has zero width or height),
   false otherwise. A null rect is defined to be empty. }

function CGRectIsEmpty( rect: CGRect ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `rect' is the null rectangle, false otherwise. }

function CGRectIsNull( rect: CGRect ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `rect' is the infinite rectangle, false otherwise. }

function CGRectIsInfinite( rect: CGRect ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Inset `rect' by `(dx, dy)' -- i.e., offset its origin by `(dx, dy)', and
   decrease its size by `(2*dx, 2*dy)'. }

function CGRectInset( rect: CGRect; dx: CGFloat; dy: CGFloat ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Expand `rect' to the smallest rect containing it with integral origin and
   size. }

function CGRectIntegral( rect: CGRect ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the union of `r1' and `r2'. }

function CGRectUnion( r1: CGRect; r2: CGRect ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the intersection of `r1' and `r2'. This may return a null rect. }

function CGRectIntersection( r1: CGRect; r2: CGRect ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Offset `rect' by `(dx, dy)'. }

function CGRectOffset( rect: CGRect; dx: CGFloat; dy: CGFloat ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Make two new rectangles, `slice' and `remainder', by dividing `rect' with
   a line that's parallel to one of its sides, specified by `edge' -- either
   `CGRectMinXEdge', `CGRectMinYEdge', `CGRectMaxXEdge', or
   `CGRectMaxYEdge'. The size of `slice' is determined by `amount', which
   measures the distance from the specified edge. }

procedure CGRectDivide( rect: CGRect; var slice: CGRect; var remainder: CGRect; amount: CGFloat; edge: CGRectEdge );
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `point' is contained in `rect', false otherwise. }

function CGRectContainsPoint( rect: CGRect; point: CGPoint ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `rect2' is contained in `rect1', false otherwise. `rect2'
   is contained in `rect1' if the union of `rect1' and `rect2' is equal to
   `rect1'. }

function CGRectContainsRect( rect1: CGRect; rect2: CGRect ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `rect1' intersects `rect2', false otherwise. `rect1'
   intersects `rect2' if the intersection of `rect1' and `rect2' is not the
   null rect. }

function CGRectIntersectsRect( rect1: CGRect; rect2: CGRect ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{** Persistent representations. **}

{ Return a dictionary representation of `point'. }

function CGPointCreateDictionaryRepresentation( point: CGPoint ): CFDictionaryRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Make a CGPoint from the contents of `dict' (presumably returned earlier
   from `CGPointCreateDictionaryRepresentation') and store the value in
   `point'. Returns true on success; false otherwise. }

function CGPointMakeWithDictionaryRepresentation( dict: CFDictionaryRef; var point: { out } CGPoint ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return a dictionary representation of `size'. }

function CGSizeCreateDictionaryRepresentation( size: CGSize ): CFDictionaryRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Make a CGSize from the contents of `dict' (presumably returned earlier
   from `CGSizeCreateDictionaryRepresentation') and store the value in
   `size'. Returns true on success; false otherwise. }

function CGSizeMakeWithDictionaryRepresentation( dict: CFDictionaryRef; var size: { out } CGSize ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return a dictionary representation of `rect'. }

function CGRectCreateDictionaryRepresentation( rect: CGRect ): CFDictionaryRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Make a CGRect from the contents of `dict' (presumably returned earlier
   from `CGRectCreateDictionaryRepresentation') and store the value in
   `rect'. Returns true on success; false otherwise. }

function CGRectMakeWithDictionaryRepresentation( dict: CFDictionaryRef; var rect: { out } CGRect ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);


{** Definitions of inline functions. **}
// CG_INLINE CGPoint
{FPC-ONLY-START}
implemented function CGPointMake(x: CGFloat; y: CGFloat): CGPoint; inline;
{FPC-ONLY-ELSE}
implemented function CGPointMake(x: CGFloat; y: CGFloat): CGPoint;
{FPC-ONLY-FINISH}

{
  CGPoint p; p.x = x; p.y = y; return p;
}

// CG_INLINE CGSize
{FPC-ONLY-START}
implemented function CGSizeMake(width: CGFloat; height: CGFloat): CGSize; inline;
{FPC-ONLY-ELSE}
implemented function CGSizeMake(width: CGFloat; height: CGFloat): CGSize;
{FPC-ONLY-FINISH}
{
  CGSize size; size.width = width; size.height = height; return size;
}
// CG_INLINE CGVector
{FPC-ONLY-START}
implemented function CGVectorMake(dx: CGFloat; dy: CGFloat): CGVector; inline;
{FPC-ONLY-ELSE}
implemented function CGVectorMake(dx: CGFloat; dy: CGFloat): CGVector;
{FPC-ONLY-FINISH}


// CG_INLINE CGRect
// seems not useful to inline to me, is fairly big (unless you can reschedule
// all the stores among the rest of the code, but still will probably increase
// code size in all cases)
implemented function CGRectMake(x: CGFloat; y: CGFloat; width: CGFloat; height: CGFloat): CGRect;
{
  CGRect rect;
  rect.origin.x = x; rect.origin.y = y;
  rect.size.width = width; rect.size.height = height;
  return rect;
}

// CG_INLINE bool
{FPC-ONLY-START}
implemented function CGPointEqualToPoint(const point1: CGPoint; const point2: CGPoint): boolean; inline;
{
  return point1.x == point2.x && point1.y == point2.y;
}

// CG_INLINE bool
implemented function CGSizeEqualToSize(size1: CGSize; size2: CGSize): boolean; inline;
{FPC-ONLY-FINISH}
{
  return size1.width == size2.width && size1.height == size2.height;
}

implementation

{FPC-ONLY-START}
function CGPointMake(x: CGFloat; y: CGFloat): CGPoint; inline;
begin
  CGPointMake.x := x;
  CGPointMake.y := y;
end;
{FPC-ONLY-ELSE}
function CGPointMake(x: CGFloat; y: CGFloat): CGPoint;
var
  res: CGPoint;
begin
  res.x := x;
  res.y := y;
  CGPointMake := res;
end;
{FPC-ONLY-FINISH}


{FPC-ONLY-START}
function CGSizeMake(width: CGFloat; height: CGFloat): CGSize; inline;
begin
  CGSizeMake.width := width;
  CGSizeMake.height := height;
end;
{FPC-ONLY-ELSE}
function CGSizeMake(width: CGFloat; height: CGFloat): CGSize;
var
  res: CGSize;
begin
  res.width := width;
  res.height := height;
  CGSizeMake := res;
end;
{FPC-ONLY-FINISH}


{FPC-ONLY-START}
function CGVectorMake(dx: CGFloat; dy: CGFloat): CGVector; inline;
begin
  CGVectorMake.dx := dx;
  CGVectorMake.dy := dy;
end;
{FPC-ONLY-ELSE}
function CGVectorMake(dx: CGFloat; dy: CGFloat): CGVector;
var
  res: CGVectorMake;
begin
  res.dx := dx;
  res.dy := dy;
  CGSizeMake := res;
end;
{FPC-ONLY-FINISH}


function CGRectMake(x: CGFloat; y: CGFloat; width: CGFloat; height: CGFloat): CGRect;
{FPC-ONLY-START}
begin
  CGRectMake.origin.x := x;
  CGRectMake.origin.y := y;
  CGRectMake.size.width := width;
  CGRectMake.size.height := height;
end;
{FPC-ONLY-ELSE}
var
  res: CGRect;
begin
  res.origin.x := x;
  res.origin.y := y;
  res.size.width := width;
  res.size.height := height;
  CGRectMake := res;
end;
{FPC-ONLY-FINISH}


{FPC-ONLY-START}
function CGPointEqualToPoint(const point1: CGPoint; const point2: CGPoint): boolean; inline;
begin
  CGPointEqualToPoint:=
    (point1.x = point2.x) and
    (point1.y = point2.y);
end;


function CGSizeEqualToSize(size1: CGSize; size2: CGSize): boolean; inline;
begin
  CGSizeEqualToSize:=
    (size1.width = size2.width) and
    (size1.height = size2.height);
end;
{FPC-ONLY-FINISH}


end.
