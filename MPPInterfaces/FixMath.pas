{
     File:       CarbonCore/FixMath.h
 
     Contains:   Fixed Math Interfaces.
                 The contents of this header file are deprecated.
 
     Copyright:  � 1985-2011 by Apple Inc. All rights reserved.
}
unit FixMath;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}


const
	fixed1 = $00010000;
	fract1 = $40000000;
	positiveInfinity = $7FFFFFFF;
	negativeInfinity = $80000000;

{
    FixRatio, FixMul, and FixRound were previously in ToolUtils.h
}
{
 *  FixRatio()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FixRatio( numer: SInt16; denom: SInt16 ): Fixed;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FixMul()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FixMul( a: Fixed; b: Fixed ): Fixed;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FixRound()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FixRound( x: Fixed ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  Fix2Frac()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function Fix2Frac( x: Fixed ): Fract;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  Fix2Long()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function Fix2Long( x: Fixed ): SInt32;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  Long2Fix()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function Long2Fix( x: SInt32 ): Fixed;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  Frac2Fix()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function Frac2Fix( x: Fract ): Fixed;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FracMul()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FracMul( x: Fract; y: Fract ): Fract;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FixDiv()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FixDiv( x: Fixed; y: Fixed ): Fixed;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FracDiv()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FracDiv( x: Fract; y: Fract ): Fract;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FracSqrt()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FracSqrt( x: Fract ): Fract;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FracSin()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FracSin( x: Fixed ): Fract;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FracCos()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FracCos( x: Fixed ): Fract;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FixATan2()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FixATan2( x: SInt32; y: SInt32 ): Fixed;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
    Frac2X, Fix2X, X2Fix, and X2Frac translate to and from
    the floating point type "extended" (that's what the X is for).
    On the original Mac this was 80-bits and the functions could be
    accessed via A-Traps.  When the 68881 co-processor was added,
    it used 96-bit floating point types, so the A-Traps could not 
    be used.  When PowerPC was added, it used 64-bit floating point
    types, so yet another prototype was added.
}
{
 *  Frac2X()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function Frac2X( x: Fract ): Float64;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  Fix2X()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function Fix2X( x: Fixed ): Float64;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  X2Fix()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function X2Fix( x: Float64 ): Fixed;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  X2Frac()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function X2Frac( x: Float64 ): Fract;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideCompare()
 *  
 *  Parameters:
 *    
 *    target:
 *      a pointer to the first wide to compare
 *    
 *    source:
 *      a pointer to the second wide to compare
 *  
 *  Result:
 *    return 0 if the value in target == the value in source ; a value
 *    < 0 if *target < *source and a value > 0 if *target > *source
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideCompare( const var target: wide; const var source: wide ): SInt16;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideAdd()
 *  
 *  Discussion:
 *    Adds the value in source to target and returns target.  Note that
 *    target is updated to the new value.
 *  
 *  Parameters:
 *    
 *    target:
 *      a pointer to the value to have source added to
 *    
 *    source:
 *      a pointer to the value to be added to target
 *  
 *  Result:
 *    returns the value target
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideAdd( var target: wide; const var source: wide ): widePtr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideSubtract()
 *  
 *  Discussion:
 *    Subtracts the value in source from target and returns target. 
 *    Note that target is updated to the new value.
 *  
 *  Parameters:
 *    
 *    target:
 *      a pointer to the value to have source subtracted from
 *    
 *    source:
 *      a pointer to the value to be substracted from target
 *  
 *  Result:
 *    returns the value target
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideSubtract( var target: wide; const var source: wide ): widePtr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideNegate()
 *  
 *  Discussion:
 *    Negates the value ( twos complement ) in target and returns
 *    target.  Note that target is updated to the new value.
 *  
 *  Parameters:
 *    
 *    target:
 *  
 *  Result:
 *    returns the value target
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideNegate( var target: wide ): widePtr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideShift()
 *  
 *  Discussion:
 *    Shift the value in target by shift bits with upwards rounding of
 *    the remainder.    Note that target is updated to the new value.
 *  
 *  Parameters:
 *    
 *    target:
 *      the value to be shifted
 *    
 *    shift:
 *      the count of bits to shift, positive values shift right and
 *      negative values shift left
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideShift( var target: wide; shift: SInt32 ): widePtr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideSquareRoot()
 *  
 *  Discussion:
 *    Return the closest integer value to the square root for the given
 *    number.
 *  
 *  Parameters:
 *    
 *    source:
 *      the value to calculate the root for
 *  
 *  Result:
 *    the closest integer value to the square root of source
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideSquareRoot( const var source: wide ): UInt32;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideMultiply()
 *  
 *  Discussion:
 *    Returns the wide result of multipling two SInt32 values
 *  
 *  Parameters:
 *    
 *    multiplicand:
 *    
 *    multiplier:
 *    
 *    target:
 *      a pointer to where to put the result  of multiplying
 *      multiplicand and multiplier, must not be NULL
 *  
 *  Result:
 *    the value target
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideMultiply( multiplicand: SInt32; multiplier: SInt32; var target: wide ): widePtr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideDivide()
 *  
 *  Discussion:
 *    Returns the integer and remainder results after dividing a wide
 *    value by an SInt32. Will overflow to positiveInfinity or
 *    negativeInfinity if the result won't fit into an SInt32.  If
 *    remainder is (SInt32) -1 then any overflow rounds to
 *    negativeInfinity.
 *  
 *  Parameters:
 *    
 *    dividend:
 *      the value to be divided
 *    
 *    divisor:
 *      the value to divide by
 *    
 *    remainder:
 *      a pointer to where to put the remainder result, between 0 and
 *      divisor, after dividing divident by divisor. If NULL, no
 *      remainder is returned.  If (SInt32*) -1, then any overflow
 *      result will round to negativeInfinity.
 *  
 *  Result:
 *    the integer signed result of dividend / divisor
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideDivide( const var dividend: wide; divisor: SInt32; var remainder: SInt32 ): SInt32;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideWideDivide()
 *  
 *  Discussion:
 *    Returns the wide integer and remainder results after dividing a
 *    wide value by an SInt32. Note that dividend is updated with the
 *    result.
 *  
 *  Parameters:
 *    
 *    dividend:
 *      the value to be divided
 *    
 *    divisor:
 *      the value to divide by
 *    
 *    remainder:
 *      a pointer to where to put the remainder result, between 0 and
 *      divisor, after dividing divident by divisor
 *  
 *  Result:
 *    the wide result of dividend / divisor
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideWideDivide( var dividend: wide; divisor: SInt32; var remainder: SInt32 ): widePtr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  WideBitShift()
 *  
 *  Discussion:
 *    Shift the value in target by shift bits.  Note that target is
 *    updated with the shifted result.
 *  
 *  Parameters:
 *    
 *    target:
 *      the value to be shifted
 *    
 *    shift:
 *      the count of bits to shift, positive values shift right and
 *      negative values shift left
 *  
 *  Result:
 *    return the value target
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function WideBitShift( var target: wide; shift: SInt32 ): widePtr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  UnsignedFixedMulDiv()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function UnsignedFixedMulDiv( value: UnsignedFixed; multiplier: UnsignedFixed; divisor: UnsignedFixed ): UnsignedFixed;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{$endc} {TARGET_OS_MAC}

end.
