{
     File:       PMPrintSettingsKeys.h
 
     Contains:   Mac OS X Printing Manager Print Settings Keys.
 
     Version:    Technology: Mac OS X
                 Release:    1.0
 
     Copyright  (c) 2008 by Apple Inc. All Rights Reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit PMPrintSettingsKeys;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


const
	kPMCopiesStr = 'com.apple.print.PrintSettings.PMCopies';
const kPMCopiesKey = CFSTR( 'com.apple.print.PrintSettings.PMCopies' );                       { CFNumber, kCFNumberSInt32Type, number of copies to print. }

const
	kPMCopyCollateStr = 'com.apple.print.PrintSettings.PMCopyCollate';
const kPMCopyCollateKey = CFSTR( 'com.apple.print.PrintSettings.PMCopyCollate' );                  { CFBoolean, Turns on collating }

const
	kPMOutputOrderStr = 'OutputOrder';
const kPMOutputOrderKey = CFSTR( 'OutputOrder' );			{ CFString, Reverse or Normal. default is Printer Specific }

const
	kPMPageSetStr = 'page-set';
const kPMPageSetKey = CFSTR( 'page-set' );	{ CFString, even, odd, or all. default is all }

const
	kPMMirrorStr = 'mirror';
const kPMMirrorKey = CFSTR( 'mirror' );		{ CFString, true or false. default is false }


const
	kPMPrintSelectionOnlyStr = 'com.apple.print.PrintSettings.PMPrintSelectionOnly';
const kPMPrintSelectionOnlyKey = CFSTR( 'com.apple.print.PrintSettings.PMPrintSelectionOnly' );            { CFBoolean - True if only current selection should be printed. }

const
	kPMBorderStr = 'com.apple.print.PrintSettings.PMBorder';
const kPMBorderKey = CFSTR( 'com.apple.print.PrintSettings.PMBorder' );                       { CFBoolean - If true, we do borders. }

const
	kPMBorderTypeStr = 'com.apple.print.PrintSettings.PMBorderType';
const kPMBorderTypeKey = CFSTR( 'com.apple.print.PrintSettings.PMBorderType' );                   { CFNumber - kCFNumberSInt32Type, Enum (PMBorderType) }

const
	kPMLayoutNUpStr = 'com.apple.print.PrintSettings.PMLayoutNUp';
const kPMLayoutNUpKey = CFSTR( 'com.apple.print.PrintSettings.PMLayoutNUp' );                    { CFBoolean, Turns on N-Up layout. }

const
	kPMLayoutRowsStr = 'com.apple.print.PrintSettings.PMLayoutRows';
const kPMLayoutRowsKey = CFSTR( 'com.apple.print.PrintSettings.PMLayoutRows' );                   { CFNumber - kCFNumberSInt32Type, indicates number of layout rows. }

const
	kPMLayoutColumnsStr = 'com.apple.print.PrintSettings.PMLayoutColumns';
const kPMLayoutColumnsKey = CFSTR( 'com.apple.print.PrintSettings.PMLayoutColumns' );                { CFNumber - kCFNumberSInt32Type, indicates number of layout columns. }

const
	kPMLayoutDirectionStr = 'com.apple.print.PrintSettings.PMLayoutDirection';
const kPMLayoutDirectionKey = CFSTR( 'com.apple.print.PrintSettings.PMLayoutDirection' );              { CFNumber - kCFNumberSInt32Type, Enum (PMLayoutDirection) }

const
	kPMLayoutTileOrientationStr = 'com.apple.print.PrintSettings.PMLayoutTileOrientation';
const kPMLayoutTileOrientationKey = CFSTR( 'com.apple.print.PrintSettings.PMLayoutTileOrientation' );        { CFNumber - kCFNumberSInt32Type, PMOrientation, 1 = portrait, 2 = landscape, etc. }

const
	kPMJobStateStr = 'com.apple.print.PrintSettings.PMJobState';
const kPMJobStateKey = CFSTR( 'com.apple.print.PrintSettings.PMJobState' );                     { CFNumber - kCFNumberSInt32Type, Enum, active = 0, pending, hold until, hold indefinitely, aborted, finished }

const
	kPMJobHoldUntilTimeStr = 'com.apple.print.PrintSettings.PMJobHoldUntilTime';
const kPMJobHoldUntilTimeKey = CFSTR( 'com.apple.print.PrintSettings.PMJobHoldUntilTime' );             { CFDate - Time we expect to print the job. }

const
	kPMJobPriorityStr = 'com.apple.print.PrintSettings.PMJobPriority';
const kPMJobPriorityKey = CFSTR( 'com.apple.print.PrintSettings.PMJobPriority' );                  { CFNumber - kCFNumberSInt32Type, Enum, Low = 0, normal, urgent }

const
	kPMDuplexingStr = 'com.apple.print.PrintSettings.PMDuplexing';
const kPMDuplexingKey = CFSTR( 'com.apple.print.PrintSettings.PMDuplexing' );                    { CFNumber - kCFNumberSInt32Type, Enum, kPMDuplexNone,  kPMDuplexNoTumble, kPMDuplexTumble, kPMSimplexTumble }

const
	kPMColorSyncProfileIDStr = 'com.apple.print.PrintSettings.PMColorSyncProfileID';
const kPMColorSyncProfileIDKey = CFSTR( 'com.apple.print.PrintSettings.PMColorSyncProfileID' );           { CFNumber - kCFNumberSInt32Type, ID of profile to use. }

const
	kPMPrimaryPaperFeedStr = 'com.apple.print.PrintSettings.PMPrimaryPaperFeed';
const kPMPrimaryPaperFeedKey = CFSTR( 'com.apple.print.PrintSettings.PMPrimaryPaperFeed' );				{ CFArray - main & option PPD key for input paper feed }

const
	kPMSecondaryPaperFeedStr = 'com.apple.print.PrintSettings.PMSecondaryPaperFeed';
const kPMSecondaryPaperFeedKey = CFSTR( 'com.apple.print.PrintSettings.PMSecondaryPaperFeed' );			{ CFArray - main & option PPD key for input paper feed }

const
	kPMPSErrorHandlerStr = 'com.apple.print.PrintSettings.PMPSErrorHandler';
const kPMPSErrorHandlerKey = CFSTR( 'com.apple.print.PrintSettings.PMPSErrorHandler' );				{ CFNumber - kCFNumberSInt32Type  }

const
	kPMPSTraySwitchStr = 'com.apple.print.PrintSettings.PMPSTraySwitch';
const kPMPSTraySwitchKey = CFSTR( 'com.apple.print.PrintSettings.PMPSTraySwitch' );					{ CFArray - main & option PPD key for tray switching }

const
	kPMTotalBeginPagesStr = 'com.apple.print.PrintSettings.PMTotalBeginPages';
const kPMTotalBeginPagesKey = CFSTR( 'com.apple.print.PrintSettings.PMTotalBeginPages' );			{ CFNumber the total number of times beginpage was called }

const
	kPMTotalSidesImagedStr = 'com.apple.print.PrintSettings.PMTotalSidesImaged';
const kPMTotalSidesImagedKey = CFSTR( 'com.apple.print.PrintSettings.PMTotalSidesImaged' );			{ CFNumber the total number of sides that will printed. Does not take into account duplex and collation }


{ Fax Related }
const
	kPMFaxNumberStr = 'phone';
const kPMFaxNumberKey = CFSTR( 'phone' );			{ CFString - fax number to dial }

const
	kPMFaxToStr = 'faxTo';
const kPMFaxToKey = CFSTR( 'faxTo' );				{ CFString - entire fax to line }

const
	kPMFaxPrefixStr = 'faxPrefix';
const kPMFaxPrefixKey = CFSTR( 'faxPrefix' );			{ CFString - fax prefix to dial }

const
	kPMFaxSubjectStr = 'faxSubject';
const kPMFaxSubjectKey = CFSTR( 'faxSubject' );			{ CFString - fax subject linee}

const
	kPMFaxCoverSheetStr = 'faxCoverSheet';
const kPMFaxCoverSheetKey = CFSTR( 'faxCoverSheet' );		{ CFString - fax cover sheet }

const
	kPMFaxCoverSheetMessageStr = 'faxCoverSheetMessage';
const kPMFaxCoverSheetMessageKey = CFSTR( 'faxCoverSheetMessage' );	{ CFString - fax cover sheet message}

const
	kPMFaxToneDialingStr = 'faxToneDialing';
const kPMFaxToneDialingKey = CFSTR( 'faxToneDialing' );		{ CFString - fax use tone dialing }

const
	kPMFaxUseSoundStr = 'faxUseSound';
const kPMFaxUseSoundKey = CFSTR( 'faxUseSound' );			{ CFString - fax use sound }

const
	kPMFaxWaitForDialToneStr = 'faxWaitForDialTone';
const kPMFaxWaitForDialToneKey = CFSTR( 'faxWaitForDialTone' );	{ CFString - fax wait for dial tone }

const
	kPMFaxToLabelStr = 'faxToLabel';
const kPMFaxToLabelKey = CFSTR( 'faxToLabel' );			{ CFString - To: label }

const
	kPMFaxFromLabelStr = 'faxFromLabel';
const kPMFaxFromLabelKey = CFSTR( 'faxFromLabel' );			{ CFString - From: label }

const
	kPMFaxDateLabelStr = 'faxDateLabel';
const kPMFaxDateLabelKey = CFSTR( 'faxDateLabel' );			{ CFString - Date: label }

const
	kPMFaxSubjectLabelStr = 'faxSubjectLabel';
const kPMFaxSubjectLabelKey = CFSTR( 'faxSubjectLabel' );		{ CFString - Subject: label }

const
	kPMFaxSheetsLabelStr = 'faxSheetsLabel';
const kPMFaxSheetsLabelKey = CFSTR( 'faxSheetsLabel' );		{ CFString - Sheets to Follow: label }


{ Coverpage Related }
const
	kPMCoverPageStr = 'com.apple.print.PrintSettings.PMCoverPage';
const kPMCoverPageKey = CFSTR( 'com.apple.print.PrintSettings.PMCoverPage' );                    { CFNumber - kCFNumberSInt32Type, Enum, kPMCoverPageNone,  kPMCoverPageBefore, kPMCoverPageAfter }

{ The values for kPMCoverPageKey }

const
	kPMCoverPageNone = 1;	
	// Print a cover page before printing the document.
	kPMCoverPageBefore = 2;
	// Print a cover page after printing the document.
	kPMCoverPageAfter = 3;
{ If the kPMDuplexingKey is not in a print settings then kPMDuplexDefault should be assumed.
 }
	kPMCoverPageDefault	= kPMCoverPageNone;


const
	kPMCoverPageSourceStr = 'com.apple.print.PrintSettings.PMCoverPageSource';
const kPMCoverPageSourceKey = CFSTR( 'com.apple.print.PrintSettings.PMCoverPageSource' );				{ CFArray - main & option PPD key for cover page paper source }


const
	kPMDestinationPrinterIDStr = 'DestinationPrinterID';
const kPMDestinationPrinterIDKey = CFSTR( 'DestinationPrinterID' );	{ CFStringRef - the printer ID corresponding to the destination printer }

const
	kPMInlineWorkflowStr = 'inlineWorkflow';
const kPMInlineWorkflowKey = CFSTR( 'inlineWorkflow' );	{ CFStringRef - the URL for the inline workflow item that will process this job }

const
	kPMPageToPaperMappingTypeStr = 'com.apple.print.PageToPaperMappingType';
const kPMPageToPaperMappingTypeKey = CFSTR( 'com.apple.print.PageToPaperMappingType' ); { a CFNumber - values from PMPageToPaperMappingType }

const
	kPMPageToPaperMediaNameStr = 'com.apple.print.PageToPaperMappingMediaName';
const kPMPageToPaperMediaNameKey = CFSTR( 'com.apple.print.PageToPaperMappingMediaName' ); { a CFString - the untranslated media name for the destination sheet }

const
	kPMPageToPaperMappingAllowScalingUpStr = 'com.apple.print.PageToPaperMappingAllowScalingUp';
const kPMPageToPaperMappingAllowScalingUpKey = CFSTR( 'com.apple.print.PageToPaperMappingAllowScalingUp' ); { a CFBoolean - if true, allow scaling up to fit
												    destination sheet, otherwise do not scale
												    up if destination sheet is larger than formatting
												    sheet. Default value: false. }

{
    The kPMCustomProfilePathKey key stores a CFString that corresponds to a custom profile setting for a given printer.
}
const
	kPMCustomProfilePathStr = 'PMCustomProfilePath';
const kPMCustomProfilePathKey = CFSTR( 'PMCustomProfilePath' );

{ Page to Paper Mapping Types }
const
	kPMPageToPaperMappingNone = 1;
	kPMPageToPaperMappingScaleToFit = 2;
type
	PMPageToPaperMappingType = SInt32;


{ Possible values for the kPMColorMatchingModeKey}
const
	kPMVendorColorMatchingStr = 'AP_VendorColorMatching';
const kPMVendorColorMatching = CFSTR( 'AP_VendorColorMatching' );
const
	kPMApplicationColorMatchingStr = 'AP_ApplicationColorMatching';
const kPMApplicationColorMatching = CFSTR( 'AP_ApplicationColorMatching' );

const
	kPMColorMatchingModeStr = 'AP_ColorMatchingMode';
const kPMColorMatchingModeKey = CFSTR( 'AP_ColorMatchingMode' );   { Value is CFStringRef - one of kPMColorSyncMatching (deprecated), 
										kPMVendorColorMatching, kPMApplicationColorMatching }


{ Begin: Use of these keys is discouraged. Use PMSessionSetDestination, PMSessionGetDestinationType, PMSessionCopyDestinationFormat, and PMSessionCopyDestinationLocation instead }
const
	kPMDestinationTypeStr = 'com.apple.print.PrintSettings.PMDestinationType';
const kPMDestinationTypeKey = CFSTR( 'com.apple.print.PrintSettings.PMDestinationType' );              { CFNumber, kCFNumberSInt32Type kPMDestinationPrinter kPMDestinationFile kPMDestinationFax, etc. }
const
	kPMOutputFilenameStr = 'com.apple.print.PrintSettings.PMOutputFilename';
const kPMOutputFilenameKey = CFSTR( 'com.apple.print.PrintSettings.PMOutputFilename' );               { CFString - URL for the output filename. }
{ End: Use of these keys is discouraged. Use PMSessionSetDestination, PMSessionGetDestinationType, PMSessionCopyDestinationFormat, and PMSessionCopyDestinationLocation instead }

{$endc} {TARGET_OS_MAC}

end.
