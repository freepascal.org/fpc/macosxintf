{
	Copyright (c) 2000-2003, Apple, Inc. All rights reserved.
}
{	  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, 2004 }


unit MacOSXPosix;
interface
uses MacTypes;
{$ALIGN MAC68K}

type
	{ sys/types }
	u_quad_t = UInt64;	{ quads }
	quad_t = SInt64;
	qaddr_t = ^quad_t;
	
	caddr_t = Ptr;	{ core address }
	daddr_t = SInt32;	{ disk address }
	dev_t = UInt32;		{ device number }
	fixpt_t = UInt32;	{ fixed point number }
	gid_t = UInt32;		{ group id }
	gid_t_ptr = ^gid_t;
	in_addr_t = UInt32;	{ base type for internet address }
	in_port_t = UInt16;
{$ifc TARGET_CPU_ARM}
	ino_t = UInt64;		{ inode number }
{$elsec}
	ino_t = UInt32;		{ inode number }
{$endc}
	key_t = SInt32;		{ IPC key (for Sys V IPC) }
	mode_t = UInt16;		{ permissions }
	nlink_t = UInt16;	{ link count }
	off_t = quad_t;		{ file offset }
	pid_t = SInt32;		{ process id }
	rlim_t = quad_t;		{ resource limit }
	segsz_t = SInt32;	{ segment size }
	swblk_t = SInt32;	{ swap offset }
	uid_t = UInt32;		{ user id }
	uid_t_ptr = ^uid_t;
	id_t = UInt32; { can hold pid_t, gid_t, or uid_t }
	id_t_ptr = ^id_t;
	useconds_t = UInt32;	{ microseconds (unsigned) }

	mach_port_name_t = UInt32; { represents a name of a port right }
	mach_port_t = UInt32; { reference added or deleted to a port right }
	mach_msg_bits_t = UInt32;
	natural_t = UInt32; {�also on 64 bit systems! }
	integer_t = SInt32; { also on 64 bit systems! }
	mach_msg_size_t = natural_t;
	mach_msg_id_t = integer_t;
	mach_msg_header_t = record
	  msgh_bits: mach_msg_bits_t;
	  msgh_size: mach_msg_size_t;
	  msgh_remote_port: mach_port_t;
	  msgh_local_port: mach_port_t;
	  msgh_reserved: mach_msg_size_t;
	  msgh_id: mach_msg_id_t;
  end;
	  

	type
		sockaddr = packed record
			sin_len: UInt8;
			sin_family: UInt8;
			sa_data: packed array[0..13] of UInt8;
		end;
		sockaddr_ptr = ^sockaddr;
		sockaddr_in = packed record
			sin_len: UInt8;
			sin_family: UInt8;
			sin_port: UInt16;
			sin_addr: UInt32;
			sin_zero: packed array[0..7] of UInt8;
		end;
		sockaddr_in_ptr = ^sockaddr_in;
		socklen_t = UInt32;

  dispatch_queue_s = record
  end;
  dispatch_queue_t = ^dispatch_queue_s;

end.
