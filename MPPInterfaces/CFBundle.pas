{	CFBundle.h
	Copyright (c) 1999-2013, Apple Inc.  All rights reserved.
}
unit CFBundle;
interface
uses MacTypes,CFBase,CFArray,CFDictionary,CFError,CFString,CFURL;
{$ALIGN POWER}


type
	CFBundleRef = ^__CFBundle; { an opaque type }
	__CFBundle = record end;
	CFBundleRefPtr = ^CFBundleRef;
	CFPlugInRef = ^__CFBundle; { an opaque type }
	CFPlugInRefPtr = ^CFPlugInRef;

{ ===================== Standard Info.plist keys ===================== }
const kCFBundleInfoDictionaryVersionKey: CFStringRef;
    { The version of the Info.plist format }
const kCFBundleExecutableKey: CFStringRef;
    { The name of the executable in this bundle, if any }
const kCFBundleIdentifierKey: CFStringRef;
    { The bundle identifier (for CFBundleGetBundleWithIdentifier()) }
const kCFBundleVersionKey: CFStringRef;
    { The version number of the bundle.  For Mac OS 9 style version numbers (for example "2.5.3d5"), }
    { clients can use CFBundleGetVersionNumber() instead of accessing this key directly since that }
    { function will properly convert the version string into its compact integer representation. }
const kCFBundleDevelopmentRegionKey: CFStringRef;
    { The name of the development language of the bundle. }
const kCFBundleNameKey: CFStringRef;
    { The human-readable name of the bundle.  This key is often found in the InfoPlist.strings since it is usually localized. }
{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
const kCFBundleLocalizationsKey: CFStringRef;
    { Allows an unbundled application that handles localization itself to specify which localizations it has available. }
{#endif}

{ ===================== Finding Bundles ===================== }

function CFBundleGetMainBundle: CFBundleRef;

function CFBundleGetBundleWithIdentifier( bundleID: CFStringRef ): CFBundleRef;
    { A bundle can name itself by providing a key in the info dictionary. }
    { This facility is meant to allow bundle-writers to get hold of their }
    { bundle from their code without having to know where it was on the disk. }
    { This is meant to be a replacement mechanism for +bundleForClass: users. }
    { Note that this does not search for bundles on the disk; it will locate }
    { only bundles already loaded or otherwise known to the current process. }

function CFBundleGetAllBundles: CFArrayRef;
    { This is potentially expensive, and not thread-safe.  Use with care. }
    { Best used for debuggging or other diagnostic purposes. }

{ ===================== Creating Bundles ===================== }

function CFBundleGetTypeID: CFTypeID;

function CFBundleCreate( allocator: CFAllocatorRef; bundleURL: CFURLRef ): CFBundleRef;
    { Might return an existing instance with the ref-count bumped. }

function CFBundleCreateBundlesFromDirectory( allocator: CFAllocatorRef; directoryURL: CFURLRef; bundleType: CFStringRef ): CFArrayRef;
    { Create instances for all bundles in the given directory matching the given type }
    { (or all of them if bundleType is NULL).  Instances are created using CFBundleCreate() and are not released. }

{ ==================== Basic Bundle Info ==================== }

function CFBundleCopyBundleURL( bundle: CFBundleRef ): CFURLRef;

function CFBundleGetValueForInfoDictionaryKey( bundle: CFBundleRef; key: CFStringRef ): CFTypeRef;
    { Returns a localized value if available, otherwise the global value. }
    { This is the recommended function for examining the info dictionary. }

function CFBundleGetInfoDictionary( bundle: CFBundleRef ): CFDictionaryRef;
    { This is the global info dictionary.  Note that CFBundle may add }
    { extra keys to the dictionary for its own use. }

function CFBundleGetLocalInfoDictionary( bundle: CFBundleRef ): CFDictionaryRef;
    { This is the localized info dictionary. }

procedure CFBundleGetPackageInfo( bundle: CFBundleRef; var packageType: OSType; var packageCreator: OSType );

function CFBundleGetIdentifier( bundle: CFBundleRef ): CFStringRef;

function CFBundleGetVersionNumber( bundle: CFBundleRef ): UInt32;

function CFBundleGetDevelopmentRegion( bundle: CFBundleRef ): CFStringRef;

function CFBundleCopySupportFilesDirectoryURL( bundle: CFBundleRef ): CFURLRef;

function CFBundleCopyResourcesDirectoryURL( bundle: CFBundleRef ): CFURLRef;

function CFBundleCopyPrivateFrameworksURL( bundle: CFBundleRef ): CFURLRef;

function CFBundleCopySharedFrameworksURL( bundle: CFBundleRef ): CFURLRef;

function CFBundleCopySharedSupportURL( bundle: CFBundleRef ): CFURLRef;

function CFBundleCopyBuiltInPlugInsURL( bundle: CFBundleRef ): CFURLRef;

{ ------------- Basic Bundle Info without a CFBundle instance ------------- }
{ This API is provided to enable developers to retrieve basic information }
{ about a bundle without having to create an instance of CFBundle. }
{ Because of caching behavior when a CFBundle instance exists, it will be faster }
{ to actually create a CFBundle if you need to retrieve multiple pieces of info. }
function CFBundleCopyInfoDictionaryInDirectory( bundleURL: CFURLRef ): CFDictionaryRef;

function CFBundleGetPackageInfoInDirectory( url: CFURLRef; var packageType: UInt32; var packageCreator: UInt32 ): Boolean;

{ ==================== Resource Handling API ==================== }

function CFBundleCopyResourceURL( bundle: CFBundleRef; resourceName: CFStringRef; resourceType: CFStringRef; subDirName: CFStringRef ): CFURLRef;

function CFBundleCopyResourceURLsOfType( bundle: CFBundleRef; resourceType: CFStringRef; subDirName: CFStringRef ): CFArrayRef;

function CFBundleCopyLocalizedString( bundle: CFBundleRef; key: CFStringRef; value: CFStringRef; tableName: CFStringRef ): CFStringRef;

{MW-GPC-ONLY-START}
{$mwgpcdefinec CFCopyLocalizedString(key, comment) CFBundleCopyLocalizedString(CFBundleGetMainBundle, key, key, nil)}
{$mwgpcdefinec CFCopyLocalizedStringFromTable(key, tbl, comment) CFBundleCopyLocalizedString(CFBundleGetMainBundle, key, key, tbl)}
{$mwgpcdefinec CFCopyLocalizedStringFromTableInBundle(key, tbl, bundle, comment) CFBundleCopyLocalizedString(bundle, key, key, tbl)}
{$mwgpcdefinec CFCopyLocalizedStringWithDefaultValue(key, tbl, bundle, value, comment) CFBundleCopyLocalizedString(bundle, key, value, tbl)}
{MW-GPC-ONLY-ELSE}
implemented function CFCopyLocalizedString( key: CFStringRef; comment: PChar ): CFStringRef; inline;
implemented function CFCopyLocalizedStringFromTable( key: CFStringRef; tableName: CFStringRef; comment: PChar ): CFStringRef; inline;
implemented function CFCopyLocalizedStringFromTableInBundle( key: CFStringRef; tableName: CFStringRef; bundle: CFBundleRef; comment: PChar ): CFStringRef; inline;
implemented function CFCopyLocalizedStringWithDefaultValue( key: CFStringRef; tableName: CFStringRef; bundle: CFBundleRef; value: CFStringRef; comment: PChar ): CFStringRef; inline;
{MW-GPC-ONLY-FINISH}

{ ------------- Resource Handling without a CFBundle instance ------------- }
{ This API is provided to enable developers to use the CFBundle resource }
{ searching policy without having to create an instance of CFBundle. }
{ Because of caching behavior when a CFBundle instance exists, it will be faster }
{ to actually create a CFBundle if you need to access several resources. }

function CFBundleCopyResourceURLInDirectory( bundleURL: CFURLRef; resourceName: CFStringRef; resourceType: CFStringRef; subDirName: CFStringRef ): CFURLRef;

function CFBundleCopyResourceURLsOfTypeInDirectory( bundleURL: CFURLRef; resourceType: CFStringRef; subDirName: CFStringRef ): CFArrayRef;

{ =========== Localization-specific Resource Handling API =========== }
{ This API allows finer-grained control over specific localizations,  }
{ as distinguished from the above API, which always uses the user's   }
{ preferred localizations for the bundle in the current app context.  }

function CFBundleCopyBundleLocalizations( bundle: CFBundleRef ): CFArrayRef;
    { Lists the localizations that a bundle contains.  }

function CFBundleCopyPreferredLocalizationsFromArray( locArray: CFArrayRef ): CFArrayRef;
    { Given an array of possible localizations, returns the one or more }
    { of them that CFBundle would use in the current application context. }
    { To determine the localizations that would be used for a particular }
    { bundle in the current application context, apply this function to the }
    { result of CFBundleCopyBundleLocalizations().  }

{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
function CFBundleCopyLocalizationsForPreferences( locArray: CFArrayRef; prefArray: CFArrayRef ): CFArrayRef;
    { Given an array of possible localizations, returns the one or more of }
    { them that CFBundle would use, without reference to the current application }
    { context, if the user's preferred localizations were given by prefArray. }
    { If prefArray is NULL, the current user's actual preferred localizations will }
    { be used. This is not the same as CFBundleCopyPreferredLocalizationsFromArray(), }
    { because that function takes the current application context into account. }
    { To determine the localizations that another application would use, apply }
    { this function to the result of CFBundleCopyBundleLocalizations().  }
{#endif}

function CFBundleCopyResourceURLForLocalization( bundle: CFBundleRef; resourceName: CFStringRef; resourceType: CFStringRef; subDirName: CFStringRef; localizationName: CFStringRef ): CFURLRef;

function CFBundleCopyResourceURLsOfTypeForLocalization( bundle: CFBundleRef; resourceType: CFStringRef; subDirName: CFStringRef; localizationName: CFStringRef ): CFArrayRef;
    { The localizationName argument to CFBundleCopyResourceURLForLocalization() or }
    { CFBundleCopyResourceURLsOfTypeForLocalization() must be identical to one of the }
    { localizations in the bundle, as returned by CFBundleCopyBundleLocalizations(). }
    { It is recommended that either CFBundleCopyPreferredLocalizationsFromArray() or }
    { CFBundleCopyLocalizationsForPreferences() be used to select the localization. }

{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}
{ =================== Unbundled application info ===================== }
{ This API is provided to enable developers to retrieve bundle-related }
{ information about an application that may be bundled or unbundled.   }
function CFBundleCopyInfoDictionaryForURL( url: CFURLRef ): CFDictionaryRef;
    { For a directory URL, this is equivalent to CFBundleCopyInfoDictionaryInDirectory(). }
    { For a plain file URL representing an unbundled executable, this will attempt to read }
    { an info dictionary from the (__TEXT, __info_plist) section, if it is a Mach-o file, }
    { or from a 'plst' resource.  }

function CFBundleCopyLocalizationsForURL( url: CFURLRef ): CFArrayRef;
    { For a directory URL, this is equivalent to calling CFBundleCopyBundleLocalizations() }
    { on the corresponding bundle.  For a plain file URL representing an unbundled executable, }
    { this will attempt to determine its localizations using the CFBundleLocalizations and }
    { CFBundleDevelopmentRegion keys in the dictionary returned by CFBundleCopyInfoDictionaryForURL,}
    { or from a 'vers' resource if those are not present.  }
{#endif}

function CFBundleCopyExecutableArchitecturesForURL( url: CFURLRef ): CFArrayRef;
CF_AVAILABLE_STARTING(10_5, 2_0);
    { For a directory URL, this is equivalent to calling CFBundleCopyExecutableArchitectures() }
    { on the corresponding bundle.  For a plain file URL representing an unbundled executable, }
    { this will return the architectures it provides, if it is a Mach-o file, or NULL otherwise. }

{ ==================== Primitive Code Loading API ==================== }
{ This API abstracts the various different executable formats supported on }
{ various platforms.  It can load DYLD, CFM, or DLL shared libraries (on their }
{ appropriate platforms) and gives a uniform API for looking up functions. }
{ Note that Cocoa-based bundles containing Objective-C or Java code must }
{ be loaded with NSBundle, not CFBundle.  Once they are loaded, however, }
{ either CFBundle or NSBundle can be used. }

function CFBundleCopyExecutableURL( bundle: CFBundleRef ): CFURLRef;

{#if MAC_OS_X_VERSION_10_5 <= MAC_OS_X_VERSION_MAX_ALLOWED}
const
	kCFBundleExecutableArchitectureI386 = $00000007;
	kCFBundleExecutableArchitecturePPC = $00000012;
	kCFBundleExecutableArchitectureX86_64 = $01000007;
	kCFBundleExecutableArchitecturePPC64 = $01000012;
{#endif} { MAC_OS_X_VERSION_10_5 <= MAC_OS_X_VERSION_MAX_ALLOWED }

function CFBundleCopyExecutableArchitectures( bundle: CFBundleRef ): CFArrayRef;
CF_AVAILABLE_STARTING(10_5, 2_0);
    { If the bundle's executable exists and is a Mach-o file, this function will return an array }
    { of CFNumbers whose values are integers representing the architectures the file provides. }
    { The values currently in use are those listed in the enum above, but others may be added }
    { in the future.  If the executable is not a Mach-o file, this function returns NULL. }

function CFBundlePreflightExecutable( bundle: CFBundleRef; var error: CFErrorRef ): Boolean;
CF_AVAILABLE_STARTING(10_5, 2_0);
    { This function will return true if the bundle is loaded, or if the bundle appears to be }
    { loadable upon inspection.  This does not mean that the bundle is definitively loadable, }
    { since it may fail to load due to link errors or other problems not readily detectable. }
    { If this function detects problems, it will return false, and return a CFError by reference. }
    { It is the responsibility of the caller to release the CFError. }

function CFBundleLoadExecutableAndReturnError( bundle: CFBundleRef; var error: CFErrorRef ): Boolean;
CF_AVAILABLE_STARTING(10_5, 2_0);
    { If the bundle is already loaded, this function will return true.  Otherwise, it will attempt }
    { to load the bundle, and it will return true if that attempt succeeds.  If the bundle fails }
    { to load, this function will return false, and it will return a CFError by reference.  }
    { It is the responsibility of the caller to release the CFError. }

function CFBundleLoadExecutable( bundle: CFBundleRef ): Boolean;

function CFBundleIsExecutableLoaded( bundle: CFBundleRef ): Boolean;

procedure CFBundleUnloadExecutable( bundle: CFBundleRef );

function CFBundleGetFunctionPointerForName( bundle: CFBundleRef; functionName: CFStringRef ): UnivPtr;

procedure CFBundleGetFunctionPointersForNames( bundle: CFBundleRef; functionNames: CFArrayRef; ftbl: {variable-size-array} UnivPtrPtr );

function CFBundleGetDataPointerForName( bundle: CFBundleRef; symbolName: CFStringRef ): UnivPtr;

procedure CFBundleGetDataPointersForNames( bundle: CFBundleRef; symbolNames: CFArrayRef; stbl: {variable-size-array} UnivPtrPtr );

function CFBundleCopyAuxiliaryExecutableURL( bundle: CFBundleRef; executableName: CFStringRef ): CFURLRef;
    { This function can be used to find executables other than your main }
    { executable.  This is useful, for instance, for applications that have }
    { some command line tool that is packaged with and used by the application. }
    { The tool can be packaged in the various platform executable directories }
    { in the bundle and can be located with this function.  This allows an }
    { app to ship versions of the tool for each platform as it does for the }
    { main app executable. }

{ ==================== Getting a bundle's plugIn ==================== }

function CFBundleGetPlugIn( bundle: CFBundleRef ): CFPlugInRef;

{ ==================== Resource Manager-Related API ==================== }

{$ifc TARGET_CPU_64}
type
	CFBundleRefNum = SInt32;
{$elsec}
type
	CFBundleRefNum = SInt16;
{$endc}

function CFBundleOpenBundleResourceMap( bundle: CFBundleRef ): CFBundleRefNum;
   { This function opens the non-localized and the localized resource files }
   { (if any) for the bundle, creates and makes current a single read-only }
   { resource map combining both, and returns a reference number for it. }
   { If it is called multiple times, it opens the files multiple times, }
   { and returns distinct reference numbers.  }

function CFBundleOpenBundleResourceFiles( bundle: CFBundleRef; var refNum: CFBundleRefNum; var localizedRefNum: CFBundleRefNum ): SInt32;
   { Similar to CFBundleOpenBundleResourceMap(), except that it creates two }
   { separate resource maps and returns reference numbers for both. }

procedure CFBundleCloseBundleResourceMap( bundle: CFBundleRef; refNum: CFBundleRefNum );


implementation

{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

{FPC-ONLY-START}
{$R-}

function CFCopyLocalizedString( key: CFStringRef; comment: PChar ): CFStringRef; inline;
begin
	CFCopyLocalizedString := CFBundleCopyLocalizedString( CFBundleGetMainBundle, key, key, nil );
end;

function CFCopyLocalizedStringFromTable( key: CFStringRef; tableName: CFStringRef; comment: PChar ): CFStringRef; inline;
begin
	CFCopyLocalizedStringFromTable := CFBundleCopyLocalizedString( CFBundleGetMainBundle, key, key, tableName );
end;

function CFCopyLocalizedStringFromTableInBundle( key: CFStringRef; tableName: CFStringRef; bundle: CFBundleRef; comment: PChar ): CFStringRef; inline;
begin
	CFCopyLocalizedStringFromTableInBundle := CFBundleCopyLocalizedString( bundle, key, key, tableName );
end;

function CFCopyLocalizedStringWithDefaultValue( key: CFStringRef; tableName: CFStringRef; bundle: CFBundleRef; value: CFStringRef; comment: PChar ): CFStringRef; inline;
begin
	CFCopyLocalizedStringWithDefaultValue := CFBundleCopyLocalizedString( bundle, key, value, tableName );
end;

{FPC-ONLY-FINISH}

end.
