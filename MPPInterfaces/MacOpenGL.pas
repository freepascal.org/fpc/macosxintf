{
	Copyright:	(c) 1999-2008 Apple Inc. All rights reserved.
}
{       Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }

unit MacOpenGL;
interface
uses MacTypes, CGLTypes, CGLCurrent, macgl;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
** CGL API version.
}
{$definec CGL_VERSION_1_0 TRUE}
{$definec CGL_VERSION_1_1 TRUE}
{$definec CGL_VERSION_1_2 TRUE}


{
** Pixel format functions
}
function CGLChoosePixelFormat( const attribs: PCGLPixelFormatAttribute; pix: PCGLPixelFormatObj; npix: PGLint ): CGLError;
function CGLDestroyPixelFormat( pix: CGLPixelFormatObj ): CGLError;
function CGLDescribePixelFormat( pix: CGLPixelFormatObj; pix_num: GLint; attrib: CGLPixelFormatAttribute; value: PGLint ): CGLError;
procedure CGLReleasePixelFormat( pix: CGLPixelFormatObj );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
function CGLRetainPixelFormat( pix: CGLPixelFormatObj ): CGLPixelFormatObj;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
function CGLGetPixelFormatRetainCount( pix: CGLPixelFormatObj ): GLuint;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;

{
** Renderer information functions
}
function CGLQueryRendererInfo( display_mask: GLuint; rend: PCGLRendererInfoObj; nrend: PGLint ): CGLError;
function CGLDestroyRendererInfo( rend: CGLRendererInfoObj ): CGLError;
function CGLDescribeRenderer( rend: CGLRendererInfoObj; rend_num: GLint; prop: CGLRendererProperty; value: PGLint ): CGLError;

{
** Context functions
}
function CGLCreateContext( pix: CGLPixelFormatObj; share: CGLContextObj; ctx: PCGLContextObj ): CGLError;
function CGLDestroyContext( ctx: CGLContextObj ): CGLError;
function CGLCopyContext( src: CGLContextObj; dst: CGLContextObj; mask: GLbitfield ): CGLError;
function CGLRetainContext( ctx: CGLContextObj ): CGLContextObj;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
procedure CGLReleaseContext( ctx: CGLContextObj );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
function CGLGetContextRetainCount( ctx: CGLContextObj ): GLuint;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
function CGLGetPixelFormat( ctx: CGLContextObj ): CGLPixelFormatObj;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;

{
** PBuffer functions
}
function CGLCreatePBuffer( width: GLsizei; height: GLsizei; target: GLenum; internalFormat: GLenum; max_level: GLint; pbuffer: PCGLPBufferObj ): CGLError;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function CGLDestroyPBuffer( pbuffer: CGLPBufferObj ): CGLError;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function CGLDescribePBuffer( obj: CGLPBufferObj; width: PGLsizei; height: PGLsizei; target: PGLenum; internalFormat: PGLenum; mipmap: PGLint ): CGLError;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function CGLTexImagePBuffer( ctx: CGLContextObj; pbuffer: CGLPBufferObj; source: GLenum ): CGLError;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function CGLRetainPBuffer( pbuffer: CGLPBufferObj ): CGLPBufferObj;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
procedure CGLReleasePBuffer( pbuffer: CGLPBufferObj );
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;
function CGLGetPBufferRetainCount( pbuffer: CGLPBufferObj ): GLuint;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;

{
** Drawable Functions
}
function CGLSetOffScreen( ctx: CGLContextObj; width: GLsizei; height: GLsizei; rowbytes: GLint; baseaddr: univ Ptr ): CGLError;
function CGLGetOffScreen( ctx: CGLContextObj; width: PGLsizei; height: PGLsizei; rowbytes: PGLint; baseaddr: UnivPtrPtr ): CGLError;
{ #ifdef DEPRECATED_IN_MAC_OS_X_VERSION_10_6_AND_LATER }
{ function CGLSetFullScreen( ctx: CGLContextObj ): CGLError; }
{ DEPRECATED_IN_MAC_OS_X_VERSION_10_6_AND_LATER; }
{ #else }
function CGLSetFullScreen( ctx: CGLContextObj ): CGLError;
{ #endif }
function CGLSetFullScreenOnDisplay( ctx: CGLContextObj; display_mask: GLuint ): CGLError;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;

function CGLSetPBuffer( ctx: CGLContextObj; pbuffer: CGLPBufferObj; face: GLenum; level: GLint; screen: GLint ): CGLError;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;
function CGLGetPBuffer( ctx: CGLContextObj; pbuffer: PCGLPBufferObj; face: PGLenum; level: PGLint; screen: PGLint ): CGLError;
AVAILABLE_MAC_OS_X_VERSION_10_3_AND_LATER;

function CGLClearDrawable( ctx: CGLContextObj ): CGLError;
function CGLFlushDrawable( ctx: CGLContextObj ): CGLError;

{
** Per context enables and parameters
}
function CGLEnable( ctx: CGLContextObj; pname: CGLContextEnable ): CGLError;
function CGLDisable( ctx: CGLContextObj; pname: CGLContextEnable ): CGLError;
function CGLIsEnabled( ctx: CGLContextObj; pname: CGLContextEnable; enable: PGLint ): CGLError;
function CGLSetParameter( ctx: CGLContextObj; pname: CGLContextParameter; const params: PGLint ): CGLError;
function CGLGetParameter( ctx: CGLContextObj; pname: CGLContextParameter; params: PGLint ): CGLError;

{
** Virtual screen functions
}
function CGLSetVirtualScreen( ctx: CGLContextObj; screen: GLint ): CGLError;
function CGLGetVirtualScreen( ctx: CGLContextObj; screen: PGLint ): CGLError;

{
** Global library options
}
{ #ifdef AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER }
{ function CGLSetGlobalOption( pname: CGLGlobalOption; const params: PGLint ): CGLError; }
(* AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER *)
{ function CGLGetGlobalOption( pname: CGLGlobalOption; params: PGLint ): CGLError }
(* AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER *)
{ #else }
function CGLSetGlobalOption( pname: CGLGlobalOption; const params: PGLint ): CGLError;
function CGLGetGlobalOption( pname: CGLGlobalOption; params: PGLint ): CGLError;
{ #endif }

function CGLSetOption( pname: CGLGlobalOption; param: GLint ): CGLError; { Use CGLSetGlobalOption }
function CGLGetOption( pname: CGLGlobalOption; param: PGLint ): CGLError; { Use CGLGetGlobalOption }

{
** Locking functions
}
function CGLLockContext( ctx: CGLContextObj ): CGLError;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CGLUnlockContext( ctx: CGLContextObj ): CGLError;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{
** Version numbers
}
procedure CGLGetVersion( majorvers: PGLint; minorvers: PGLint );

{
** Convert an error code to a string
}
function CGLErrorString( error: CGLError ): PChar;

{$endc} {TARGET_OS_MAC}

end.
