{
 *  CoreText.h
 *  CoreText
 *
 *  Copyright 2006-2012 Apple Inc. All rights reserved.
 *
 }
{  Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
{$propagate-units}
unit CoreText;
interface
uses MacTypes,CTFont,CTFontCollection,CTFontDescriptor,CTFontManager,CTFontTraits,CTFrame,CTFramesetter,CTGlyphInfo,CTLine,CTParagraphStyle,CTRun,CTStringAttributes,CTTextTab,CTTypesetter;

{$ALIGN POWER}


{!
    @header

    Thread Safety Information

    All functions in this header are thread safe unless otherwise specified.
}


{!
    @function   CTGetCoreTextVersion
    @abstract   Returns the version of the CoreText framework.

    @discussion This function returns a number indicating the version of the
                CoreText framework. Note that framework version is not always
                an accurate indicator of feature availability. The recommended
                way to use this function is first to check that the function
                pointer is non-NULL, followed by calling it and comparing its
                result to a defined constant (or constants). For example, to
                determine whether the CoreText API is available:
                    if (&CTGetCoreTextVersion != NULL && CTGetCoreTextVersion() >= kCTVersionNumber10_5) (
                        // CoreText API is available
                    )

    @result     The version number. This value is for comparison with the
                constants beginning with kCTVersionNumber.
}

function CTGetCoreTextVersion: UInt32;
CT_AVAILABLE_STARTING( __MAC_10_5, __IPHONE_3_2);

const kCTVersionNumber10_5   = $00020000;
const kCTVersionNumber10_5_2 = $00020001;
const kCTVersionNumber10_5_3 = $00020002;
const kCTVersionNumber10_5_5 = $00020003;
const kCTVersionNumber10_6   = $00030000;
const kCTVersionNumber10_7   = $00040000;
const kCTVersionNumber10_8   = $00050000;
const kCTVersionNumber10_9   = $00060000;


end.
