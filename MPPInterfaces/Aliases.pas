{
     File:       CarbonCore/Aliases.h
 
     Contains:   Alias Manager Interfaces.
                 The contents of this header file are deprecated.
                 Use Foundation or CoreFoundation URL Bookmarks instead.
 
     Copyright:  � 1989-2011 by Apple Inc. All rights reserved.
}
unit Aliases;
interface
uses MacTypes,Files,UTCUtils,CFBase;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}


type
	FSAliasInfoBitmap = UInt32;
	FSAliasInfoBitmapPtr = ^FSAliasInfoBitmap;
const
	kFSAliasInfoNone = $00000000; { no valid info}
	kFSAliasInfoVolumeCreateDate = $00000001; { volume creation date is valid}
	kFSAliasInfoTargetCreateDate = $00000002; { target creation date is valid}
	kFSAliasInfoFinderInfo = $00000004; { file type and creator are valid}
	kFSAliasInfoIsDirectory = $00000008; { isDirectory boolean is valid}
	kFSAliasInfoIDs = $00000010; { parentDirID and nodeID are valid}
	kFSAliasInfoFSInfo = $00000020; { filesystemID and signature are valid}
	kFSAliasInfoVolumeFlags = $00000040; { volumeIsBootVolume, volumeIsAutomounted, volumeIsEjectable and volumeHasPersistentFileIDs are valid}

const
	rAliasType = FOUR_CHAR_CODE('alis'); { Aliases are stored as resources of this type }

const
{ define alias resolution action rules mask }
	kARMMountVol = $00000001; { mount the volume automatically }
	kARMNoUI = $00000002; { no user interface allowed during resolution }
	kARMMultVols = $00000008; { search on multiple volumes }
	kARMSearch = $00000100; { search quickly }
	kARMSearchMore = $00000200; { search further }
	kARMSearchRelFirst = $00000400; { search target on a relative path first }
	kARMTryFileIDFirst = $00000800; { search by file id before path }

const
{ define alias record information types }
	asiZoneName = -3;   { get zone name }
	asiServerName = -2;   { get server name }
	asiVolumeName = -1;   { get volume name }
	asiAliasName = 0;    { get aliased file/folder/volume name }
	asiParentName = 1;     { get parent folder name }

{ ResolveAliasFileWithMountFlags options }
const
	kResolveAliasFileNoUI = $00000001; { no user interaction during resolution }
	kResolveAliasTryFileIDFirst = $00000002; { search by file id before path }

	{	 define the alias record that will be the blackbox for the caller 	}

type
	AliasRecordPtr = ^AliasRecord;
  { Opaque as of Mac OS X 10.4 ... }
	AliasRecord = record
		userType: OSType;               { appl stored type like creator type }
		aliasSize: UInt16;              { alias record size in bytes, for appl usage }
	end;

type
	AliasPtr = AliasRecordPtr;
	AliasHandle = ^AliasPtr;
{ info block to pass to FSCopyAliasInfo }
type
	FSAliasInfo = record
		volumeCreateDate: UTCDateTime;
		targetCreateDate: UTCDateTime;
		fileType: OSType;
		fileCreator: OSType;
		parentDirID: UInt32;
		nodeID: UInt32;
		filesystemID: UInt16;
		signature: UInt16;
		volumeIsBootVolume: Boolean;
		volumeIsAutomounted: Boolean;
		volumeIsEjectable: Boolean;
		volumeHasPersistentFileIDs: Boolean;
		isDirectory: Boolean;
	end;
	FSAliasInfoPtr = ^FSAliasInfo;
{ alias record information type }
type
	AliasInfoType = SInt16;
{$ifc not TARGET_CPU_64}
type
	AliasFilterProcPtr = function( cpbPtr: CInfoPBPtr; var quitFlag: Boolean; myDataPtr: Ptr ): Boolean;
{GPC-ONLY-START}
  AliasFilterUPP = UniversalProcPtr; // should be AliasFilterProcPtr
{GPC-ONLY-ELSE}
	AliasFilterUPP = AliasFilterProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewAliasFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
{ old name was NewAliasFilterProc }
function NewAliasFilterUPP( userRoutine: AliasFilterProcPtr ): AliasFilterUPP;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);
{
 *  DisposeAliasFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeAliasFilterUPP( userUPP: AliasFilterUPP );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{
 *  InvokeAliasFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeAliasFilterUPP( cpbPtr: CInfoPBPtr; var quitFlag: Boolean; myDataPtr: Ptr; userUPP: AliasFilterUPP ): Boolean;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{#if __MACH__
    #define NewAliasFilterUPP(userRoutine)                      ((AliasFilterUPP)userRoutine)
    #define DisposeAliasFilterUPP(userUPP)
    #define InvokeAliasFilterUPP(cpbPtr, quitFlag, myDataPtr, userUPP) (*userUPP)(cpbPtr, quitFlag, myDataPtr)
#endif}

{$endc} {not TARGET_CPU_64}

type
	FSAliasFilterProcPtr = function( const var ref: FSRef; var quitFlag: Boolean; myDataPtr: Ptr ): Boolean;
{GPC-ONLY-START}
  DSAliasFilterUPP = UniversalProcPtr; // should be FSAliasFilterProcPtr
{GPC-ONLY-ELSE}
	FSAliasFilterUPP = FSAliasFilterProcPtr;
{GPC-ONLY-FINISH}

{
 *  FSNewAlias()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSNewAlias( {const} fromFile: FSRefPtr { can be NULL }; const var target: FSRef; var inAlias: AliasHandle ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSNewAliasMinimal()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSNewAliasMinimal( const var target: FSRef; var inAlias: AliasHandle ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSIsAliasFile()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSIsAliasFile( const var fileRef: FSRef; var aliasFileFlag: Boolean; var folderFlag: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSResolveAliasWithMountFlags()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSResolveAliasWithMountFlags( {const} fromFile: FSRefPtr { can be NULL }; inAlias: AliasHandle; var target: FSRef; var wasChanged: Boolean; mountFlags: UNSIGNEDLONG ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSResolveAlias()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSResolveAlias( {const} fromFile: FSRefPtr { can be NULL }; alias: AliasHandle; var target: FSRef; var wasChanged: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSResolveAliasFileWithMountFlags()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSResolveAliasFileWithMountFlags( var theRef: FSRef; resolveAliasChains: Boolean; var targetIsFolder: Boolean; var wasAliased: Boolean; mountFlags: UNSIGNEDLONG ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSResolveAliasFile()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSResolveAliasFile( var theRef: FSRef; resolveAliasChains: Boolean; var targetIsFolder: Boolean; var wasAliased: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSFollowFinderAlias()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSFollowFinderAlias( fromFile: FSRefPtr { can be NULL }; alias: AliasHandle; logon: Boolean; var target: FSRef; var wasChanged: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSUpdateAlias()
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   in InterfaceLib 9.1 and later
 }
function FSUpdateAlias( {const} fromFile: FSRefPtr { can be NULL }; const var target: FSRef; alias: AliasHandle; var wasChanged: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSNewAliasUnicode()
 *  
 *  Summary:
 *    Creates an alias given a ref to the target's parent directory and
 *    the target's unicode name.  If the target does not exist fnfErr
 *    will be returned but the alias will still be created.  This
 *    allows the creation of aliases to targets that do not exist.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    fromFile:
 *      The starting point for a relative search.
 *    
 *    targetParentRef:
 *      An FSRef to the parent directory of the target.
 *    
 *    targetNameLength:
 *      Number of Unicode characters in the target's name.
 *    
 *    targetName:
 *      A pointer to the Unicode name.
 *    
 *    inAlias:
 *      A Handle to the newly created alias record.
 *    
 *    isDirectory:
 *      On input, if target does not exist, a flag to indicate whether
 *      or not the target is a directory.  On output, if the target did
 *      exist, a flag indicating if the target is a directory.  Pass
 *      NULL in the non-existant case if unsure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
function FSNewAliasUnicode( {const} fromFile: FSRefPtr { can be NULL }; const var targetParentRef: FSRef; targetNameLength: UniCharCount; targetName: ConstUniCharPtr; var inAlias: AliasHandle; isDirectory: BooleanPtr { can be NULL } ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSNewAliasMinimalUnicode()
 *  
 *  Summary:
 *    Creates a minimal alias given a ref to the target's parent
 *    directory and the target's unicode name.  If the target does not
 *    exist fnfErr will be returned but the alias will still be created.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    targetParentRef:
 *      An FSRef to the parent directory of the target.
 *    
 *    targetNameLength:
 *      Number of Unicode characters in the target's name.
 *    
 *    targetName:
 *      A pointer to the Unicode name.
 *    
 *    inAlias:
 *      A Handle to the newly created alias record.
 *    
 *    isDirectory:
 *      On input, if target does not exist, a flag to indicate whether
 *      or not the target is a directory.  On output, if the target did
 *      exist, a flag indicating if the target is a directory.  Pass
 *      NULL in the non-existant case if unsure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
function FSNewAliasMinimalUnicode( const var targetParentRef: FSRef; targetNameLength: UniCharCount; targetName: ConstUniCharPtr; var inAlias: AliasHandle; isDirectory: BooleanPtr { can be NULL } ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSNewAliasFromPath()
 *  
 *  Summary:
 *    Creates an alias given a POSIX style utf-8 path to the target. 
 *    If the target file does not exist but the path up to the leaf
 *    does then fnfErr will be returned but the alias will still be
 *    created.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Parameters:
 *    
 *    fromFilePath:
 *      The starting point for a relative search.
 *    
 *    targetPath:
 *      POSIX style UTF-8 path to target.
 *    
 *    flags:
 *      Options for future use.  Pass in 0.
 *    
 *    inAlias:
 *      A Handle to the newly created alias record.
 *    
 *    isDirectory:
 *      On input, if target does not exist, a flag to indicate whether
 *      or not the target is a directory.  On output, if the target did
 *      exist, a flag indicating if the target is a directory.  Pass
 *      NULL in the non-existant case if unsure.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.5 and later
 *    Non-Carbon CFM:   not available
 }
function FSNewAliasFromPath( fromFilePath: ConstCStringPtr { can be NULL }; targetPath: ConstCStringPtr; flags: OptionBits; var inAlias: AliasHandle; isDirectory: BooleanPtr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_5, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSMatchAliasBulk()
 *  
 *  Summary:
 *    Given an alias handle and fromFile, match the alias and return
 *    FSRefs to the aliased file(s) and needsUpdate flag
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.5
 *  
 *  Availability:
 *    Mac OS X:         in version 10.5 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function FSMatchAliasBulk( {const} fromFile: FSRefPtr { can be NULL }; rulesMask: UNSIGNEDLONG; inAlias: AliasHandle; var aliasCount: SInt16; var aliasList: FSRef; var needsUpdate: Boolean; aliasFilter: FSAliasFilterProcPtr { can be NULL }; yourDataPtr: univ Ptr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_5, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  FSCopyAliasInfo()
 *  
 *  Discussion:
 *    This routine will return the requested information from the
 *    passed in aliasHandle.  The information is gathered only from the
 *    alias record so it may not match what is on disk (no disk i/o is
 *    performed).  The whichInfo paramter is an output parameter that
 *    signifies which fields in the info record contain valid data.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.2
 *  
 *  Parameters:
 *    
 *    inAlias:
 *      A handle to the alias record to get the information from.
 *    
 *    targetName:
 *      The name of the target item.
 *    
 *    volumeName:
 *      The name of the volume the target resides on.
 *    
 *    pathString:
 *      POSIX path to target.
 *    
 *    whichInfo:
 *      An indication of which fields in the info block contain valid
 *      data.
 *    
 *    info:
 *      Returned information about the alias.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
function FSCopyAliasInfo( inAlias: AliasHandle; targetName: HFSUniStr255Ptr { can be NULL }; volumeName: HFSUniStr255Ptr { can be NULL }; pathString: CFStringRefPtr { can be NULL }; whichInfo: FSAliasInfoBitmapPtr { can be NULL }; info: FSAliasInfoPtr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  GetAliasSize()
 *  
 *  Discussion:
 *    This routine will return the size of the alias record referenced
 *    by the AliasHandle alias.  This will be smaller than the size
 *    returned by GetHandleSize if any custom data has been added (IM
 *    Files 4-13).
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    alias:
 *      A handle to the alias record to get the information from.
 *  
 *  Result:
 *    The size of the private section of the alias record.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function GetAliasSize( alias: AliasHandle ): Size;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  GetAliasUserType()
 *  
 *  Discussion:
 *    This routine will return the usertype associated with the alias
 *    record referenced by the AliasHandle alias.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    alias:
 *      A handle to the alias record to get the userType from.
 *  
 *  Result:
 *    The userType associated with the alias as an OSType
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
function GetAliasUserType( alias: AliasHandle ): OSType;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  SetAliasUserType()
 *  
 *  Discussion:
 *    This routine will set the userType associated with an alias
 *    record.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    alias:
 *      A handle to the alias record to set the userType for.
 *    
 *    userType:
 *      The OSType to set the userType to.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.4 and later
 *    Non-Carbon CFM:   not available
 }
procedure SetAliasUserType( alias: AliasHandle; userType: OSType );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  GetAliasSizeFromPtr()
 *  
 *  Discussion:
 *    This routine will return the size of the alias record referenced
 *    by a pointer to the AliasRecord.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    alias:
 *      A pointer to the alias record to get the information from.
 *  
 *  Result:
 *    The size of the private section of the alias record.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function GetAliasSizeFromPtr( const var alias: AliasRecord ): Size;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  GetAliasUserTypeFromPtr()
 *  
 *  Discussion:
 *    This routine will return the usertype associated withthe alias
 *    record pointed to by alias.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    alias:
 *      A pointer to the alias record to get the userType from.
 *  
 *  Result:
 *    The userType associated with the alias as an OSType
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function GetAliasUserTypeFromPtr( const var alias: AliasRecord ): OSType;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  SetAliasUserTypeWithPtr()
 *  
 *  Discussion:
 *    This routine will set the userType associated with an alias
 *    record.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.4
 *  
 *  Parameters:
 *    
 *    alias:
 *      A pointer to the alias record to set the userType for.
 *    
 *    userType:
 *      The OSType to set the userType to.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
procedure SetAliasUserTypeWithPtr( alias: AliasPtr; userType: OSType );
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{ Functions beyond this point are deprecated}

{$ifc not TARGET_CPU_64}
{
 *  FSMatchAlias()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSMatchAliasBulk instead
 *  
 *  Summary:
 *    Given an alias handle and fromFile, match the alias and return
 *    FSRefs to the aliased file(s) and needsUpdate flag
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
function FSMatchAlias( {const} fromFile: FSRefPtr { can be NULL }; rulesMask: UNSIGNEDLONG; inAlias: AliasHandle; var aliasCount: SInt16; var aliasList: FSRef; var needsUpdate: Boolean; aliasFilter: AliasFilterUPP; yourDataPtr: univ Ptr ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);


{
 *  FSMatchAliasNoUI()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSMatchAliasBulk with the kARMNoUI flag instead
 *  
 *  Summary:
 *    variation on FSMatchAlias that does not prompt user with a dialog
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.2 and later
 *    Non-Carbon CFM:   not available
 }
function FSMatchAliasNoUI( {const} fromFile: FSRefPtr { can be NULL }; rulesMask: UNSIGNEDLONG; inAlias: AliasHandle; var aliasCount: SInt16; var aliasList: FSRef; var needsUpdate: Boolean; aliasFilter: AliasFilterUPP; yourDataPtr: univ Ptr ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);


{
 *  NewAlias()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSNewAlias
 *  
 *  Summary:
 *    create a new alias between fromFile and target, returns alias
 *    record handle
 *  
 *  Discussion:
 *    Create an alias betwen fromFile and target, and return it in an
 *    AliasHandle. This function is deprecated in Mac OS X 10.4;
 *    instead, you should use FSNewAliasUnicode() because NewAlias()
 *    has problems creating aliases to certain files, including those
 *    which are impossible to represent in an FSSpec.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function NewAlias( {const} fromFile: FSSpecPtr { can be NULL }; const var target: FSSpec; var alias: AliasHandle ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  NewAliasMinimal()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSNewAliasMinimalUnicode
 *  
 *  Summary:
 *    create a minimal new alias for a target and return alias record
 *    handle
 *  
 *  Discussion:
 *    Create a minimal alias for a target, and return it in an
 *    AliasHandle. This function is deprecated in Mac OS X 10.4;
 *    instead, you should use FSNewAliasMinimalUnicode() because
 *    NewAliasMinimalAlias() has problems creating aliases to certain
 *    files, including those which are impossible to represent in an
 *    FSSpec.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function NewAliasMinimal( const var target: FSSpec; var alias: AliasHandle ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  NewAliasMinimalFromFullPath()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSNewAliasMinimalUnicode
 *  
 *  Summary:
 *    create a minimal new alias from a target fullpath (optional zone
 *    and server name) and return alias record handle
 *  
 *  Discussion:
 *    Create a minimal alias for a target fullpath, and return it in an
 *    AliasHandle. This function is deprecated in Mac OS X 10.4;
 *    instead, you should use FSNewAliasMinimalUnicode() because
 *    NewAliasMinimalFromFullPath() has problems creating aliases to
 *    certain files, including those which are impossible to represent
 *    in an FSSpec.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function NewAliasMinimalFromFullPath( fullPathLength: SInt16; fullPath: {const} univ Ptr; const var zoneName: Str32; const var serverName: Str31; var alias: AliasHandle ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  ResolveAlias()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSResolveAlias() or FSResolveAliasWithMountFlags() instead.
 *  
 *  Summary:
 *    given an alias handle and fromFile, resolve the alias, update the
 *    alias record and return aliased filename and wasChanged flag.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function ResolveAlias( {const} fromFile: FSSpecPtr { can be NULL }; alias: AliasHandle; var target: FSSpec; var wasChanged: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  GetAliasInfo()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSCopyAliasInfo instead.
 *  
 *  Summary:
 *    This call does not work on all aliases. Given an alias handle and
 *    an index specifying requested alias information type, return the
 *    information from alias record as a string. An empty string is
 *    returned when the index is greater than the number of levels
 *    between the target and root.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.0
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.3
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetAliasInfo( alias: AliasHandle; itemIndex: AliasInfoType; var theString: Str63 ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_3, __IPHONE_NA, __IPHONE_NA);


{
 *  IsAliasFile()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSIsAliasFile() instead.
 *  
 *  Summary:
 *    Return true if the file pointed to by fileFSSpec is an alias file.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.0
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.5 and later
 }
function IsAliasFile( const var fileFSSpec: FSSpec; var aliasFileFlag: Boolean; var folderFlag: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  ResolveAliasWithMountFlags()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSResolveAliasWithMountFlags() instead.
 *  
 *  Summary:
 *    Given an AliasHandle, return target file spec. It resolves the
 *    entire alias chain or one step of the chain.  It returns info
 *    about whether the target is a folder or file; and whether the
 *    input file spec was an alias or not.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.5 and later
 }
function ResolveAliasWithMountFlags( {const} fromFile: FSSpecPtr { can be NULL }; alias: AliasHandle; var target: FSSpec; var wasChanged: Boolean; mountFlags: UNSIGNEDLONG ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  ResolveAliasFile()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSResolveAliasFile
 *  
 *  Summary:
 *    Given a file spec, return target file spec if input file spec is
 *    an alias. It resolves the entire alias chain or one step of the
 *    chain.  It returns info about whether the target is a folder or
 *    file; and whether the input file spec was an alias or not.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function ResolveAliasFile( var theSpec: FSSpec; resolveAliasChains: Boolean; var targetIsFolder: Boolean; var wasAliased: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{ Deprecated: Use FSResolveAliasFileWithMountFlags instead}
{
 *  ResolveAliasFileWithMountFlags()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.5 and later
 }
function ResolveAliasFileWithMountFlags( var theSpec: FSSpec; resolveAliasChains: Boolean; var targetIsFolder: Boolean; var wasAliased: Boolean; mountFlags: UNSIGNEDLONG ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);


{ Deprecated:  Use FSFollowFinderAlias instead}
{
 *  FollowFinderAlias()   *** DEPRECATED ***
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 8.5 and later
 }
function FollowFinderAlias( {const} fromFile: FSSpecPtr { can be NULL }; alias: AliasHandle; logon: Boolean; var target: FSSpec; var wasChanged: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);


{ 
   Low Level Routines 
}
{
 *  UpdateAlias()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    UseFSUpdateAlias
 *  
 *  Summary:
 *    given a fromFile-target pair and an alias handle, update the
 *    alias record pointed to by alias handle to represent target as
 *    the new alias.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function UpdateAlias( {const} fromFile: FSSpecPtr { can be NULL }; const var target: FSSpec; alias: AliasHandle; var wasChanged: Boolean ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  MatchAlias()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSMatchAliasBulk instead
 *  
 *  Summary:
 *    Given an alias handle and fromFile, match the alias and return
 *    FSSpecs to the aliased file(s) and needsUpdate flag
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function MatchAlias( {const} fromFile: FSSpecPtr { can be NULL }; rulesMask: UNSIGNEDLONG; alias: AliasHandle; var aliasCount: SInt16; aliasList: FSSpecArrayPtr; var needsUpdate: Boolean; aliasFilter: AliasFilterUPP; yourDataPtr: univ Ptr ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  ResolveAliasFileWithMountFlagsNoUI()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSResolveAliasFileWithMountFlags passing in the
 *    kResolveAliasFileNoUI flag
 *  
 *  Summary:
 *    variation on ResolveAliasFile that does not prompt user with a
 *    dialog
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function ResolveAliasFileWithMountFlagsNoUI( var theSpec: FSSpec; resolveAliasChains: Boolean; var targetIsFolder: Boolean; var wasAliased: Boolean; mountFlags: UNSIGNEDLONG ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_4, __IPHONE_NA, __IPHONE_NA);


{
 *  MatchAliasNoUI()   *** DEPRECATED ***
 *  
 *  Deprecated:
 *    Use FSMatchAliasBulk with the kARMNoUI flag instead
 *  
 *  Summary:
 *    variation on MatchAlias that does not prompt user with a dialog
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in CoreServices.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function MatchAliasNoUI( {const} fromFile: FSSpecPtr { can be NULL }; rulesMask: UNSIGNEDLONG; alias: AliasHandle; var aliasCount: SInt16; aliasList: FSSpecArrayPtr; var needsUpdate: Boolean; aliasFilter: AliasFilterUPP; yourDataPtr: univ Ptr ): OSErr;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_0, __MAC_10_5, __IPHONE_NA, __IPHONE_NA);



{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}


end.
