{
     File:       HIToolbox/Dialogs.h
 
     Contains:   Dialog Manager interfaces.
 
     Version:    HIToolbox-624~3
 
     Copyright:  © 1985-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit Dialogs;
interface
uses MacTypes,CFBase,CarbonEventsCore,QuickdrawTypes,Events,IconsCore,TextEdit,HIObject;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

const
{ new, more standard names for dialog item types}
	kControlDialogItem = 4;
	kButtonDialogItem = kControlDialogItem or 0;
	kCheckBoxDialogItem = kControlDialogItem or 1;
	kRadioButtonDialogItem = kControlDialogItem or 2;
	kResourceControlDialogItem = kControlDialogItem or 3;
	kStaticTextDialogItem = 8;
	kEditTextDialogItem = 16;
	kIconDialogItem = 32;
	kPictureDialogItem = 64;
	kUserDialogItem = 0;
	kHelpDialogItem = 1;
	kItemDisableBit = 128;

const
{ old names for dialog item types}
	ctrlItem = 4;
	btnCtrl = 0;
	chkCtrl = 1;
	radCtrl = 2;
	resCtrl = 3;
	statText = 8;
	editText = 16;
	iconItem = 32;
	picItem = 64;
	userItem = 0;
	itemDisable = 128;

const
{ standard dialog item numbers}
	kStdOkItemIndex = 1;
	kStdCancelItemIndex = 2;    { old names}
	ok = kStdOkItemIndex;
	cancel = kStdCancelItemIndex;

const
{ standard icon resource id's    }
	kStopIcon = 0;
	kNoteIcon = 1;
	kCautionIcon = 2;    { old names}
	stopIcon = kStopIcon;
	noteIcon = kNoteIcon;
	cautionIcon = kCautionIcon;


{  Dialog Item List Manipulation Constants }
type
	DITLMethod = SInt16;
const
	overlayDITL = 0;
	appendDITLRight = 1;
	appendDITLBottom = 2;

type
	StageList = SInt16;
{ DialogPtr is obsolete. Use DialogRef instead.}
type
	DialogRef = DialogPtr;

type
	DialogTemplatePtr = ^DialogTemplate;
	DialogTemplate = record
		boundsRect: Rect;
		procID: SInt16;
		visible: Boolean;
		filler1: Boolean;
		goAwayFlag: Boolean;
		filler2: Boolean;
		refCon: SInt32;
		itemsID: SInt16;
		title: Str255;
	end;
type
	DialogTPtr = DialogTemplatePtr;
type
	DialogTHndl = ^DialogTPtr;
type
	AlertTemplatePtr = ^AlertTemplate;
	AlertTemplate = record
		boundsRect: Rect;
		itemsID: SInt16;
		stages: StageList;
	end;
type
	AlertTPtr = AlertTemplatePtr;
type
	AlertTHndl = ^AlertTPtr;
{ new type abstractions for the dialog manager }
type
	DialogItemIndexZeroBased = SInt16;
	DialogItemIndex = SInt16;
	DialogItemIndexPtr = ^DialogItemIndex;
	DialogItemType = SInt16;
	DialogItemTypePtr = ^DialogItemType;
{ dialog manager callbacks }
type
	SoundProcPtr = procedure( soundNumber: SInt16 );
	ModalFilterProcPtr = function( theDialog: DialogRef; var theEvent: EventRecord; var itemHit: DialogItemIndex ): Boolean;
{ ModalFilterYDProcPtr was previously in StandardFile.h }
type
	ModalFilterYDProcPtr = function( theDialog: DialogRef; var theEvent: EventRecord; var itemHit: SInt16; yourDataPtr: univ Ptr ): Boolean;
	UserItemProcPtr = procedure( theDialog: DialogRef; itemNo: DialogItemIndex );
{GPC-ONLY-START}
	SoundUPP = UniversalProcPtr; // should be SoundProcPtr
{GPC-ONLY-ELSE}
	SoundUPP = SoundProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	ModalFilterUPP = UniversalProcPtr; // should be ModalFilterProcPtr
{GPC-ONLY-ELSE}
	ModalFilterUPP = ModalFilterProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	ModalFilterYDUPP = UniversalProcPtr; // should be ModalFilterYDProcPtr
{GPC-ONLY-ELSE}
	ModalFilterYDUPP = ModalFilterYDProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	UserItemUPP = UniversalProcPtr; // should be UserItemProcPtr
{GPC-ONLY-ELSE}
	UserItemUPP = UserItemProcPtr;
{GPC-ONLY-FINISH}
{
 *  NewSoundUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
 *  NewModalFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewModalFilterUPP( userRoutine: ModalFilterProcPtr ): ModalFilterUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewModalFilterYDUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewModalFilterYDUPP( userRoutine: ModalFilterYDProcPtr ): ModalFilterYDUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  NewUserItemUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function NewUserItemUPP( userRoutine: UserItemProcPtr ): UserItemUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeSoundUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
 *  DisposeModalFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeModalFilterUPP( userUPP: ModalFilterUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeModalFilterYDUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeModalFilterYDUPP( userUPP: ModalFilterYDUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  DisposeUserItemUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure DisposeUserItemUPP( userUPP: UserItemUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeSoundUPP()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   available as macro/inline
 }

{
 *  InvokeModalFilterUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeModalFilterUPP( theDialog: DialogRef; var theEvent: EventRecord; var itemHit: DialogItemIndex; userUPP: ModalFilterUPP ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeModalFilterYDUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0.2 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
function InvokeModalFilterYDUPP( theDialog: DialogRef; var theEvent: EventRecord; var itemHit: SInt16; yourDataPtr: univ Ptr; userUPP: ModalFilterYDUPP ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{
 *  InvokeUserItemUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   available as macro/inline
 }
procedure InvokeUserItemUPP( theDialog: DialogRef; itemNo: DialogItemIndex; userUPP: UserItemUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
    ₯ Following types are valid with Appearance 1.0 and later
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
}
const
{ Alert types to pass into StandardAlert }
	kAlertStopAlert = 0;
	kAlertNoteAlert = 1;
	kAlertCautionAlert = 2;
	kAlertPlainAlert = 3;

type
	AlertType = SInt16;
const
	kAlertDefaultOKText = -1;   { "OK"}
	kAlertDefaultCancelText = -1;   { "Cancel"}
	kAlertDefaultOtherText = -1;    { "Don't Save"}

{ StandardAlert alert button numbers }
const
	kAlertStdAlertOKButton = 1;
	kAlertStdAlertCancelButton = 2;
	kAlertStdAlertOtherButton = 3;
	kAlertStdAlertHelpButton = 4;


{
 *  Dialog feature flags
 *  
 *  Summary:
 *    These feature flags can be used in a 'dlgx' resource or in the
 *    inFlags parameter to NewFeaturesDialog.
 }
const
{
   * Requests the Dialog Manager to set the dialog boxΥs background
   * color or pattern, and to use Appearance Manager APIs to draw the
   * dialog frame.
   }
	kDialogFlagsUseThemeBackground = 1 shl 0;

  {
   * Requests the Dialog Manager to create a root control in the dialog
   * window and establish a control embedding hierarchy. Any dialog
   * items become controls once the embedding hierarchy is established.
   }
	kDialogFlagsUseControlHierarchy = 1 shl 1;

  {
   * Requests the Dialog Manager to automatically handle movable modal
   * behavior such as dragging a dialog window by its title bar.
   }
	kDialogFlagsHandleMovableModal = 1 shl 2;

  {
   * On Mac OS 8.x and 9.x, requests the Dialog Manager to create
   * Appearance-compliant controls in the dialog window. Otherwise,
   * push buttons, checkboxes, and radio buttons are displayed in their
   * pre-Appearance forms when systemwide Appearance is off. On Mac OS
   * X, this flag is ignored; dialogs always use controls to implement
   * standard dialog items.
   }
	kDialogFlagsUseThemeControls = 1 shl 3;

  {
   * Requests the Dialog Manager to create a compositing window to
   * contain the dialog items. The window will also use the standard
   * window event handler and will have live resize enabled. Available
   * in Mac OS X 10.5 and later.
   }
	kDialogFlagsUseCompositing = 1 shl 8;


{
 *  Alert feature flags
 *  
 *  Summary:
 *    These feature flags can be used in a 'alrx' resource.
 }
const
{ Alert Flags for use in alrx resource }

  {
   * Requests the Dialog Manager to set the alertΥs background color or
   * pattern, and to use Appearance Manager APIs to draw the alert
   * frame.
   }
	kAlertFlagsUseThemeBackground = 1 shl 0;

  {
   * Requests the Dialog Manager to create a root control in the alert
   * window and establish a control embedding hierarchy. Any alert
   * items become controls once the embedding hierarchy is established.
   }
	kAlertFlagsUseControlHierarchy = 1 shl 1;

  {
   * Requests the Dialog Manager to make the alert window movable, and
   * to automatically handle dragging the alert by its title bar.
   }
	kAlertFlagsAlertIsMovable = 1 shl 2;

  {
   * On Mac OS 8.x and 9.x, requests the Dialog Manager to create
   * Appearance-compliant controls in the alert window. Otherwise, push
   * buttons, checkboxes, and radio buttons are displayed in their
   * pre-Appearance forms when systemwide Appearance is off. On Mac OS
   * X, this flag is ignored; alerts always use controls to implement
   * standard alert items.
   }
	kAlertFlagsUseThemeControls = 1 shl 3;

  {
   * Requests the Dialog Manager to create a compositing window to
   * contain the alert items. The window will also use the standard
   * window event handler. Available in Mac OS X 10.5 and later.
   }
	kAlertFlagsUseCompositing = 1 shl 8;

{ For dftb resource }
const
	kDialogFontNoFontStyle = 0;
	kDialogFontUseFontMask = $0001;
	kDialogFontUseFaceMask = $0002;
	kDialogFontUseSizeMask = $0004;
	kDialogFontUseForeColorMask = $0008;
	kDialogFontUseBackColorMask = $0010;
	kDialogFontUseModeMask = $0020;
	kDialogFontUseJustMask = $0040;
	kDialogFontUseAllMask = $00FF;
	kDialogFontAddFontSizeMask = $0100;
	kDialogFontUseFontNameMask = $0200;
	kDialogFontAddToMetaFontMask = $0400;

{ Also for dftb resource. This one is available in Mac OS X or later. }
{ It corresponds directly to kControlUseThemeFontIDMask from Controls.h. }
const
	kDialogFontUseThemeFontIDMask = $0080;


{
 *  AlertStdAlertParamRec
 *  
 *  Summary:
 *    AlertStdCFStringAlertParamRec is preferred.
 *  
 *  Discussion:
 *    As of Mac OS X 10.5, defaultButton can be the same as the
 *    cancelButton. Prior to Mac OS X 10.5, having cancelButton and
 *    defaultButton the same will cause a paramErr.
 }
type
	AlertStdAlertParamRec = record
{
   * Is the alert movable? This parameter is ignored on Mac OS X; all
   * standard alerts are movable.
   }
		movable: Boolean;

  {
   * Is there a help button?
   }
		helpButton: Boolean;

  {
   * The event filter to be used.
   }
		filterProc: ModalFilterUPP;

  {
   * Text for the button in the OK position. kAlertDefaultOKText can be
   * used for the system standard text.
   }
		defaultText: ConstStringPtr;

  {
   * Text for the button in the Cancel position.
   * kAlertDefaultCancelText can be used for the system standard text.
   }
		cancelText: ConstStringPtr;

  {
   * Text for the button in the leftmost position.
   * kAlertDefaultOtherText can be used for the system standard text.
   }
		otherText: ConstStringPtr;

  {
   * Which button is default, i.e. activated by pressing Return. Prior
   * to Mac OS X 10.5, this field must be set to a non-zero value in
   * the range kAlertStdAlertOKButton..kAlertStdAlertOtherButton, and
   * your alert will always have a default button. In Mac OS X 10.5 and
   * later, you may set this field to zero to request no default button.
   }
		defaultButton: SInt16;

  {
   * Which button is default for cancelling, i.e. activated by pressing
   * Escape. May be zero to request no cancel button; otherwise, must
   * be in the range kAlertStdAlertOKButton..kAlertStdAlertOtherButton.
   }
		cancelButton: SInt16;

  {
   * Where to position the alert. kWindowDefaultPosition is the default
   * and is equal to kWindowAlertPositionParentWindowScreen.
   }
		position: UInt16;
	end;
type
	AlertStdAlertParamRecPtr = ^AlertStdAlertParamRec;
const
	kHICommandOther = FOUR_CHAR_CODE('othr'); { sent by standard sheet dialogs when the "other" button is pressed }


const
{
   * AlertStdCFStringAlertParamRec version prior to Mac OS X 10.5.
   }
	kStdCFStringAlertVersionOne = 1;

  {
   * AlertStdCFStringAlertParamRec version for Mac OS X 10.5 and later.
   * When using this version, you must set the icon field of the
   * structure to NULL or a valid IconRef.
   }
	kStdCFStringAlertVersionTwo = 2;


{
 *  Summary:
 *    Flags to CreateStandardAlert that are specified in the
 *    AlertStdCFStringAlertParamRec.flags field.
 }
const
{
   * Applies to StandardSheet only. Do not dispose of the sheet window
   * after closing it; allows the sheet to be re-used again in a later
   * call to ShowSheetWindow.
   }
	kStdAlertDoNotDisposeSheet = 1 shl 0;

  {
   * Applies to StandardSheet only. Causes the sheet window to be
   * hidden immediately without animation effects when the default
   * button is chosen by the user.
   }
	kStdAlertDoNotAnimateOnDefault = 1 shl 1;

  {
   * Applies to StandardSheet only. Causes the sheet window to be
   * hidden immediately without animation effects when the cancel
   * button is chosen by the user.
   }
	kStdAlertDoNotAnimateOnCancel = 1 shl 2;

  {
   * Applies to StandardSheet only. Causes the sheet window to be
   * hidden immediately without animation effects when the other button
   * is chosen by the user.
   }
	kStdAlertDoNotAnimateOnOther = 1 shl 3;

  {
   * Allows dialog to stay up even after clicking the Help button.
   * Normally, it would close immediately. It is not necessary to set
   * this option for sheets, as they merely send the HICommandHelp
   * command to the target provided. RunStandardAlert will return with
   * the help button item in the itemHit parameter, but the window will
   * remain up. You can then perform whatever help function you wish
   * and then call RunStandardAlert again. This option is available in
   * Mac OS X 10.4 or later.
   }
	kStdAlertDoNotCloseOnHelp = 1 shl 4;


{
 *  AlertStdCFStringAlertParamRec
 *  
 *  Summary:
 *    A CFString variant of AlertStdAlertParamRec. CFStrings are
 *    preferred.
 *  
 *  Discussion:
 *    As of Mac OS X 10.5, defaultButton can be the same as the
 *    cancelButton. Prior to Mac OS X 10.5, having cancelButton and
 *    defaultButton the same will cause a paramErr.
 }
type
	AlertStdCFStringAlertParamRec = record
{
   * The version of the structure. Prior to Mac OS X 10.5, you must use
   * kStdCFStringAlertVersionOne. In Mac OS X 10.5 or later, you may
   * use either VersionOne or VersionTwo. If you use VersionTwo, you
   * must set the icon field to NULL or a valid IconRef.
   }
		version: UInt32;

  {
   * Is the alert movable? This parameter is ignored on Mac OS X; all
   * standard alerts are movable.
   }
		movable: Boolean;

  {
   * Is there a help button?
   }
		helpButton: Boolean;

  {
   * Text for the button in the OK position.
   }
		defaultText: CFStringRef;

  {
   * Text for the button in the Cancel position.
   }
		cancelText: CFStringRef;

  {
   * Text for the button in the leftmost/help position.
   }
		otherText: CFStringRef;

  {
   * Which button is default, i.e. activated by pressing Return. Prior
   * to Mac OS X 10.5, this field must be set to a non-zero value in
   * the range kAlertStdAlertOKButton..kAlertStdAlertOtherButton, and
   * your alert will always have a default button. In Mac OS X 10.5 and
   * later, you may set this field to zero to request no default button.
   }
		defaultButton: SInt16;

  {
   * Which button is default for cancelling, i.e. activated by pressing
   * Escape. May be zero to request no cancel button; otherwise, must
   * be in the range kAlertStdAlertOKButton..kAlertStdAlertOtherButton.
   }
		cancelButton: SInt16;

  {
   * Where to position the alert. kWindowDefaultPosition is the default
   * and is equal to kWindowAlertPositionParentWindowScreen.
   }
		position: UInt16;

  {
   * Options for the behavior of the alert or sheet.
   }
		flags: OptionBits;

  {
   * Custom icon for display in the alert. Available on Mac OS X 10.5
   * and later when the version field is set to
   * kStdCFStringAlertVersionTwo.
   }
		icon: IconRef;
	end;
type
	AlertStdCFStringAlertParamPtr = ^AlertStdCFStringAlertParamRec;
{ ΡΡΡ end Appearance 1.0 or later stuff}


{$ifc not TARGET_CPU_64}
{
 *  NewDialog()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function NewDialog( dStorage: univ Ptr; const var boundsRect: Rect; const var title: Str255; visible: Boolean; procID: SInt16; behind: WindowRef; goAwayFlag: Boolean; refCon: SRefCon; items: Handle ): DialogRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetNewDialog()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetNewDialog( dialogID: SInt16; dStorage: univ Ptr; behind: WindowRef ): DialogRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  NewColorDialog()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function NewColorDialog( dStorage: univ Ptr; const var boundsRect: Rect; const var title: Str255; visible: Boolean; procID: SInt16; behind: WindowRef; goAwayFlag: Boolean; refCon: SRefCon; items: Handle ): DialogRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DisposeDialog()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure DisposeDialog( theDialog: DialogRef );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ModalDialog()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure ModalDialog( modalFilter: ModalFilterUPP; var itemHit: DialogItemIndex );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  IsDialogEvent()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function IsDialogEvent( const var theEvent: EventRecord ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DialogSelect()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function DialogSelect( const var theEvent: EventRecord; var theDialog: DialogRef; var itemHit: DialogItemIndex ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DrawDialog()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure DrawDialog( theDialog: DialogRef );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  UpdateDialog()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure UpdateDialog( theDialog: DialogRef; updateRgn: RgnHandle );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  HideDialogItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure HideDialogItem( theDialog: DialogRef; itemNo: DialogItemIndex );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ShowDialogItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure ShowDialogItem( theDialog: DialogRef; itemNo: DialogItemIndex );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  FindDialogItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function FindDialogItem( theDialog: DialogRef; thePt: Point ): DialogItemIndexZeroBased;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{$ifc not TARGET_CPU_64}
{
 *  DialogCut()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure DialogCut( theDialog: DialogRef );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DialogPaste()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure DialogPaste( theDialog: DialogRef );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DialogCopy()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure DialogCopy( theDialog: DialogRef );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  DialogDelete()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure DialogDelete( theDialog: DialogRef );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{$ifc not TARGET_CPU_64}
{
 *  Alert()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function Alert( alertID: SInt16; modalFilter: ModalFilterUPP ): DialogItemIndex;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  StopAlert()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function StopAlert( alertID: SInt16; modalFilter: ModalFilterUPP ): DialogItemIndex;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  NoteAlert()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function NoteAlert( alertID: SInt16; modalFilter: ModalFilterUPP ): DialogItemIndex;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CautionAlert()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function CautionAlert( alertID: SInt16; modalFilter: ModalFilterUPP ): DialogItemIndex;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure GetDialogItem( theDialog: DialogRef; itemNo: DialogItemIndex; var itemType: DialogItemType; var item: Handle; var box: Rect );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetDialogItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure SetDialogItem( theDialog: DialogRef; itemNo: DialogItemIndex; itemType: DialogItemType; item: Handle; const var box: Rect );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ParamText()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure ParamText( const var param0: Str255; const var param1: Str255; const var param2: Str255; const var param3: Str255 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SelectDialogItemText()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure SelectDialogItemText( theDialog: DialogRef; itemNo: DialogItemIndex; strtSel: SInt16; endSel: SInt16 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogItemText()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure GetDialogItemText( item: Handle; var text: Str255 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetDialogItemText()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure SetDialogItemText( item: Handle; const var text: Str255 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetAlertStage()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetAlertStage: SInt16;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetDialogFont()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure SetDialogFont( fontNum: SInt16 );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ResetAlertStage()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure ResetAlertStage;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ APIs in Carbon}
{
 *  GetParamText()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0 and later
 }
procedure GetParamText( param0: StringPtr; param1: StringPtr; param2: StringPtr; param3: StringPtr );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  AppendDITL()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure AppendDITL( theDialog: DialogRef; theHandle: Handle; method: DITLMethod );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CountDITL()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function CountDITL( theDialog: DialogRef ): DialogItemIndex;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  ShortenDITL()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
procedure ShortenDITL( theDialog: DialogRef; numberItems: DialogItemIndex );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  InsertDialogItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function InsertDialogItem( theDialog: DialogRef; afterItem: DialogItemIndex; itemType: DialogItemType; itemHandle: Handle; const var box: Rect ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RemoveDialogItems()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function RemoveDialogItems( theDialog: DialogRef; itemNo: DialogItemIndex; amountToRemove: DialogItemIndex; disposeItemData: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{$ifc not TARGET_CPU_64}
{
 *  StdFilterProc()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function StdFilterProc( theDialog: DialogRef; var event: EventRecord; var itemHit: DialogItemIndex ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

{$ifc not TARGET_CPU_64}
{
 *  GetStdFilterProc()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function GetStdFilterProc( var theProc: ModalFilterUPP ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetDialogDefaultItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SetDialogDefaultItem( theDialog: DialogRef; newItem: DialogItemIndex ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetDialogCancelItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SetDialogCancelItem( theDialog: DialogRef; newItem: DialogItemIndex ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetDialogTracksCursor()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in InterfaceLib 7.1 and later
 }
function SetDialogTracksCursor( theDialog: DialogRef; tracks: Boolean ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
    ₯ Appearance Dialog Routines (available only with Appearance 1.0 and later)
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
}

{
 *  NewFeaturesDialog()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppearanceLib 1.0 and later
 }
function NewFeaturesDialog( inStorage: univ Ptr; const var inBoundsRect: Rect; const var inTitle: Str255; inIsVisible: Boolean; inProcID: SInt16; inBehind: WindowRef; inGoAwayFlag: Boolean; inRefCon: SRefCon; inItemListHandle: Handle; inFlags: UInt32 ): DialogRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  AutoSizeDialog()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppearanceLib 1.0 and later
 }
function AutoSizeDialog( inDialog: DialogRef ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
    Regarding StandardAlert and constness:
    Even though the inAlertParam parameter is marked const here, there was
    a chance Dialog Manager would modify it on versions of Mac OS prior to 9.
}
{
 *  StandardAlert()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppearanceLib 1.0 and later
 }
function StandardAlert( inAlertType: AlertType; const var inError: Str255; const var inExplanation: Str255; {const} inAlertParam: AlertStdAlertParamRecPtr { can be NULL }; var outItemHit: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ CFString-based StandardAlert and StandardSheet APIs are only available on Mac OS X and later}

{
 *  GetStandardAlertDefaultParams()
 *  
 *  Summary:
 *    Fills out an AlertStdCFStringAlertParamRec with default
 *    values:
 *    -   movable
 *    -   no help button
 *    -   default button with title kAlertDefaultOKText
 *    -   no cancel or other buttons
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    param:
 *      The parameter block to initialize.
 *    
 *    version:
 *      The parameter block version; pass kStdCFStringAlertVersionOne.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function GetStandardAlertDefaultParams( param: AlertStdCFStringAlertParamPtr; version: UInt32 ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CreateStandardAlert()
 *  
 *  Summary:
 *    Creates an alert containing standard elements and using standard
 *    formatting rules.
 *  
 *  Discussion:
 *    CreateStandardAlert should be used in conjunction with
 *    RunStandardAlert. After CreateStandardAlert returns, the alert is
 *    still invisible. RunStandardAlert will show the alert and run a
 *    modal dialog loop to process events in the alert. 
 *    
 *    The strings passed to this API in the error, explanation, and
 *    AlertStdCFStringAlertParamRec button title parameters will all be
 *    retained during the creation of the alert, and released when the
 *    alert is disposed by RunStandardAlert. There is no net change to
 *    the refcount of these strings across CreateStandardAlert and
 *    RunStandardAlert.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    alertType:
 *      The type of alert to create.
 *    
 *    error:
 *      The error string to display. CreateStandardAlert increments the
 *      refcount on this string, so you may release it after
 *      CreateStandardAlert returns if you don't need it later.
 *    
 *    explanation:
 *      The explanation string to display. May be NULL or empty to
 *      display no explanation. CreateStandardAlert increments the
 *      refcount on this string, so you may release it after
 *      CreateStandardAlert returns if you don't need it later.
 *    
 *    param:
 *      The parameter block describing how to create the alert. May be
 *      NULL. CreateStandardAlert increments the refcount on the button
 *      title strings in the parameter block, so you may release them
 *      after CreateStandardAlert returns if you don't need them later.
 *    
 *    outAlert:
 *      On exit, contains the new alert.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function CreateStandardAlert( alertType_: AlertType; error: CFStringRef; explanation: CFStringRef { can be NULL }; {const} param: AlertStdCFStringAlertParamPtr { can be NULL }; var outAlert: DialogRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  RunStandardAlert()
 *  
 *  Summary:
 *    Shows, runs, and destroys a standard alert using a modal dialog
 *    loop.
 *  
 *  Discussion:
 *    RunStandardAlert displays and runs an alert created by
 *    CreateStandardAlert. It handles all user interaction with the
 *    alert. After the user has dismissed the alert, RunStandardAlert
 *    destroys the alert dialog; the DialogRef will be invalid after
 *    RunStandardAlert returns. DO NOT call DisposeDialog. 
 *    
 *    NOTE: DO NOT call this function for a dialog that was not created
 *    with CreateStandardAlert! You will sorely regret it, I promise
 *    you.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    inAlert:
 *      The alert to display.
 *    
 *    filterProc:
 *      An event filter function for handling events that do not apply
 *      to the alert. May be NULL.
 *    
 *    outItemHit:
 *      On exit, contains the item index of the button that was pressed
 *      to close the alert.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function RunStandardAlert( inAlert: DialogRef; filterProc: ModalFilterUPP { can be NULL }; var outItemHit: DialogItemIndex ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CreateStandardSheet()
 *  
 *  Summary:
 *    Creates an alert containing standard elements and using standard
 *    formatting rules, and prepares it to be displayed as a sheet.
 *  
 *  Discussion:
 *    CreateStandardSheet should be used in conjunction with
 *    ShowSheetWindow. After CreateStandardSheet returns, the alert is
 *    still invisible. ShowSheetWindow will show the alert and then
 *    return. Events in the sheet are handled asynchronously; the
 *    application should be prepared for the sheet window to be part of
 *    its windowlist while running its own event loop. When a button in
 *    the sheet is pressed, the EventTargetRef passed to
 *    CreateStandardSheet will receive a command event with one of the
 *    command IDs kHICommandOK, kHICommandCancel, or kHICommandOther.
 *    The sheet is hidden and the sheet dialog destroyed before the
 *    command is sent; the caller does not have to call HideSheetWindow
 *    or DisposeDialog. 
 *    
 *    If the caller needs to destroy the sheet before showing it, then
 *    it is sufficient to call DisposeDialog on the sheet. This is the
 *    only case in which the caller would need to destroy the sheet
 *    explicitly. 
 *    
 *    The strings passed to this API in the error, explanation, and
 *    AlertStdCFStringAlertParamRec button title parameters will all be
 *    retained during the creation of the sheet, and released when the
 *    sheet is disposed. There is no net change to the refcount of
 *    these strings across CreateStandardSheet and sheet destruction.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    alertType:
 *      The type of alert to create.
 *    
 *    error:
 *      The error string to display. CreateStandardSheet increments the
 *      refcount on this string, so you may release it after
 *      CreateStandardSheet returns if you don't need it later.
 *    
 *    explanation:
 *      The explanation string to display. May be NULL or empty to
 *      display no explanation. CreateStandardSheet increments the
 *      refcount on this string, so you may release it after
 *      CreateStandardSheet returns if you don't need it later.
 *    
 *    param:
 *      The parameter block describing how to create the alert. May be
 *      NULL. CreateStandardSheet increments the refcount on the button
 *      title strings in the parameter block, so you may release them
 *      after CreateStandardSheet returns if you don't need them later.
 *    
 *    notifyTarget:
 *      The event target to be notified when the sheet is closed. The
 *      caller should install an event handler on this target for the
 *      [kEventClassCommand, kEventProcessCommand] event. May be NULL
 *      if the caller does not need the command event to be sent to any
 *      target. 
 *      
 *      Typically, this will be the event target for the parent window
 *      of the sheet; a standard practice is to install a handler on
 *      the parent window just before showing the sheet window, and to
 *      remove the handler from the parent window after the sheet has
 *      been closed. It is also possible to install a handler on the
 *      sheet window itself, in which case you would pass NULL for this
 *      parameter, since the command event is automatically sent to the
 *      sheet window already. If you install a handler on the sheet
 *      itself, make sure to return eventNotHandledErr from your
 *      handler, because CreateStandardSheet installs its own handler
 *      on the sheet and that handler must be allowed to run to close
 *      the sheet window and release the DialogRef.
 *    
 *    outSheet:
 *      On exit, contains the new alert.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function CreateStandardSheet( alertType_: AlertType; error: CFStringRef; explanation: CFStringRef { can be NULL }; {const} param: AlertStdCFStringAlertParamPtr { can be NULL }; notifyTarget: EventTargetRef { can be NULL }; var outSheet: DialogRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  CloseStandardSheet()
 *  
 *  Summary:
 *    Closes a standard sheet dialog and releases the dialog data
 *    structures.
 *  
 *  Discussion:
 *    CloseStandardSheet is meant to be used when you need to remove a
 *    sheet because of a higher-priority request to close the sheet's
 *    document window. For example, you might have a Save Changes sheet
 *    open on a document window. Meanwhile, the user drags the document
 *    into the trash. When your application sees that the document has
 *    been moved to the trash, it knows that it should close the
 *    document window, but first it needs to close the sheet. 
 *    
 *    CloseStandardSheet should not be used by your Carbon event
 *    handler in response to a click in one of the sheet buttons; the
 *    Dialog Manager will close the sheet automatically in that case.
 *    
 *    
 *    If kStdAlertDoNotDisposeSheet was specified when the sheet was
 *    created, the sheet dialog will be hidden but not released, and
 *    you can reuse the sheet later.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    inSheet:
 *      The sheet to close.
 *    
 *    inResultCommand:
 *      This command, if not zero, will be sent to the EventTarget
 *      specified when the sheet was created.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        not available in CarbonLib 1.x, is available on Mac OS X version 10.0 and later
 *    Non-Carbon CFM:   not available
 }
function CloseStandardSheet( inSheet: DialogRef; inResultCommand: UInt32 ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogItemAsControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppearanceLib 1.0 and later
 }
function GetDialogItemAsControl( inDialog: DialogRef; inItemNo: DialogItemIndex; var outControl: ControlRef ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  MoveDialogItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppearanceLib 1.0 and later
 }
function MoveDialogItem( inDialog: DialogRef; inItemNo: DialogItemIndex; inHoriz: SInt16; inVert: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SizeDialogItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppearanceLib 1.0 and later
 }
function SizeDialogItem( inDialog: DialogRef; inItemNo: DialogItemIndex; inWidth: SInt16; inHeight: SInt16 ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  AppendDialogItemList()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DialogsLib 8.5 and later
 }
function AppendDialogItemList( dialog: DialogRef; ditlID: SInt16; method: DITLMethod ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
    ₯ Dialog Routines available only with Appearance 1.1 (Mac OS 8.5) and later
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
}

{
 *  SetDialogTimeout()
 *  
 *  Summary:
 *    Sets the timeout for a modal dialog.
 *  
 *  Discussion:
 *    SetDialogTimeout sets the delay after which a dialog will be
 *    automatically dismissed. When SetDialogTimeout is called, the
 *    Dialog Manager takes the current time, adds the timeout to it,
 *    and stores the result as the time to dismiss the dialog. If the
 *    dismissal time is reached, the dialog is automatically closed and
 *    the specified dialog item index is returned from ModalDialog in
 *    the itemHit parameter. If the user moves the mouse or presses a
 *    key, the dismissal time is reset by adding the original timeout
 *    to the time of the event. Only the ModalDialog API observes the
 *    timeout value; if you are handling events in a modeless dialog or
 *    sheet using IsDialogEvent and DialogSelect, the timeout will be
 *    ignored.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    inDialog:
 *      The dialog on which to set a timeout.
 *    
 *    inButtonToPress:
 *      The dialog item index that should be returned from ModalDialog
 *      when the timeout expires.
 *    
 *    inSecondsToWait:
 *      The time to wait before dismissing the dialog, in seconds.
 *  
 *  Result:
 *    An operating system result code.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DialogsLib 8.5 and later
 }
function SetDialogTimeout( inDialog: DialogRef; inButtonToPress: DialogItemIndex; inSecondsToWait: UInt32 ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogTimeout()
 *  
 *  Summary:
 *    Retrieves the timeout for a modal dialog.
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Parameters:
 *    
 *    inDialog:
 *      The dialog from which to retrieve the timeout.
 *    
 *    outButtonToPress:
 *      On exit, contains the dialog item index that should be returned
 *      from ModalDialog when the dialog is dismissed. May be NULL if
 *      you do not need this information.
 *    
 *    outSecondsToWait:
 *      On exit, contains the time to wait before dismissing the
 *      dialog, in seconds. May be NULL if you do not need this
 *      information.
 *    
 *    outSecondsRemaining:
 *      On exit, contains the time until the dialog is dismissed, in
 *      seconds. May be NULL if you do not need this information.
 *  
 *  Result:
 *    An operating system result code. Returns dialogNoTimeoutErr if no
 *    timeout has been set for this dialog.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DialogsLib 8.5 and later
 }
function GetDialogTimeout( inDialog: DialogRef; outButtonToPress: DialogItemIndexPtr { can be NULL }; outSecondsToWait: UInt32Ptr { can be NULL }; outSecondsRemaining: UInt32Ptr { can be NULL } ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetModalDialogEventMask()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DialogsLib 8.5 and later
 }
function SetModalDialogEventMask( inDialog: DialogRef; inMask: EventMask ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetModalDialogEventMask()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in DialogsLib 8.5 and later
 }
function GetModalDialogEventMask( inDialog: DialogRef; var outMask: EventMask ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
    ₯ Accessor functions
  ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
}


{
 *  GetDialogWindow()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0 and later or as macro/inline
 }
function GetDialogWindow( dialog: DialogRef ): WindowRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogTextEditHandle()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0 and later or as macro/inline
 }
function GetDialogTextEditHandle( dialog: DialogRef ): TEHandle;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogDefaultItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0 and later or as macro/inline
 }
function GetDialogDefaultItem( dialog: DialogRef ): SInt16;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogCancelItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0 and later or as macro/inline
 }
function GetDialogCancelItem( dialog: DialogRef ): SInt16;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogKeyboardFocusItem()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0 and later or as macro/inline
 }
function GetDialogKeyboardFocusItem( dialog: DialogRef ): SInt16;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  SetPortDialogPort()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0 and later or as macro/inline
 }
procedure SetPortDialogPort( dialog: DialogRef );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogPort()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0 and later or as macro/inline
 }
function GetDialogPort( dialog: DialogRef ): CGrafPtr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  GetDialogFromWindow()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in CarbonAccessors.o 1.0 and later or as macro/inline
 }
function GetDialogFromWindow( window: WindowRef ): DialogRef;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{$endc} {not TARGET_CPU_64}

(*
#if OLDROUTINENAMES && !__LP64__
#define DisposDialog(theDialog) DisposeDialog(theDialog)
#define UpdtDialog(theDialog, updateRgn) UpdateDialog(theDialog, updateRgn)
#define GetDItem(theDialog, itemNo, itemType, item, box) GetDialogItem(theDialog, itemNo, itemType, item, box)
#define SetDItem(theDialog, itemNo, itemType, item, box) SetDialogItem(theDialog, itemNo, itemType, item, box)
#define HideDItem(theDialog, itemNo) HideDialogItem(theDialog, itemNo)
#define ShowDItem(theDialog, itemNo) ShowDialogItem(theDialog, itemNo)
#define SelIText(theDialog, itemNo, strtSel, endSel) SelectDialogItemText(theDialog, itemNo, strtSel, endSel)
#define GetIText(item, text) GetDialogItemText(item, text)
#define SetIText(item, text) SetDialogItemText(item, text)
#define FindDItem(theDialog, thePt) FindDialogItem(theDialog, thePt)
#define NewCDialog(dStorage, boundsRect, title, visible, procID, behind, goAwayFlag, refCon, items) \
NewColorDialog(dStorage, boundsRect, title, visible, procID, behind, goAwayFlag, refCon, items)
#define GetAlrtStage() GetAlertStage()
#define ResetAlrtStage() ResetAlertStage()
#define DlgCut(theDialog) DialogCut(theDialog)
#define DlgPaste(theDialog) DialogPaste(theDialog)
#define DlgCopy(theDialog) DialogCopy(theDialog)
#define DlgDelete(theDialog) DialogDelete(theDialog)
#define SetDAFont(fontNum) SetDialogFont(fontNum)
#define SetGrafPortOfDialog(dialog) SetPortDialogPort(dialog)
#endif  { OLDROUTINENAMES }
*)

{$endc} {TARGET_OS_MAC}

end.
