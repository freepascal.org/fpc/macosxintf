{
	Copyright:  (c) 1999-2008 Apple Inc. All rights reserved.
}
{       Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }

unit gliContexts;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
** GL context data type
}
type
	__GLIContextRec = record end;
	GLIContext = ^__GLIContextRec;
	
	__GLISharedRec = record end;
	GLIShared = ^__GLISharedRec;

{$endc} {TARGET_OS_MAC}

end.
