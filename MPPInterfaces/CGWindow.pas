{ CoreGraphics - CGWindow.h
   Copyright (c) 2006-2008 Apple Inc.
   All rights reserved. }
{       Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGWindow;
interface
uses MacTypes,CGBase,CGGeometry,CGImage,CFBase,CFDictionary,CFArray,CFNumber;
{$ALIGN POWER}


{ The CGWindowID contains a unique value within the user session
   representing a window }

type
	CGWindowID = UInt32;

{ Options for use with these APIs. }
type
	CGWindowListOption = UInt32;
	CGWindowImageOption = UInt32;

{ Values used for `CGWindowSharingType'. }
const
	kCGWindowSharingNone = 0;
	kCGWindowSharingReadOnly = 1;
	kCGWindowSharingReadWrite = 2;
type
	CGWindowSharingType = UInt32;

{ Values used for `CGWindowBackingType'. }
const
	kCGBackingStoreRetained = 0;
	kCGBackingStoreNonretained = 1;
	kCGBackingStoreBuffered = 2;
type
	CGWindowBackingType = UInt32;


{ A value which is never the window ID of any window. }

const
  kCGNullWindowID = CGWindowID(0);

{$ifc TARGET_OS_MAC}
{ Keys for window dictionaries. }

{ The window ID, a unique value within the user session representing the
   window. The value of this key is a CFNumber 32-bit signed integer
   value. }

const kCGWindowNumber: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ The backing store type of the window, one of `kCGBackingStoreRetained',
   `kCGBackingStoreNonretained', or `kCGBackingStoreBuffered'. The value of
   this key is a CFNumber 32-bit signed integer value. }

const kCGWindowStoreType: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ The window layer number of the window. The value of this key is a
   CFNumber 32-bit signed integer value. }

const kCGWindowLayer: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ The bounds of the window in screen space, with the origin at the
   upper-left corner of the main display. The value of this key is a
   CFDictionary; use `CGRectMakeWithDictionaryRepresentation' to obtain the
   bounds as a CGRect value. }

const kCGWindowBounds: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ The sharing state of the window, one of `kCGWindowSharingNone',
   `kCGWindowSharingReadOnly', or `kCGWindowSharingReadWrite'. The value of
   this key is a CFNumber 32-bit signed integer value. }

const kCGWindowSharingState: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ The alpha fade of the window. The value of this key is a CFNumber
   floating-point value. The value 1.0 is normal (opaque); the value 0.0 is
   fully transparent (invisible). }

const kCGWindowAlpha: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ The process ID of the process that owns the window. The value of this key
   is a CFNumber 32-bit signed integer value. }

const kCGWindowOwnerPID: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ An estimate of the memory in bytes currently used by the window and its
   supporting data structures. The value of this key is a CFNumber 64-bit
   signed integer value. }

const kCGWindowMemoryUsage: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ Optional keys for window dictionaries. }

{ If present, the workspace ID of the workspace associated with the window.
   The value of this key is a CFNumber 32-bit signed integer value. }

const kCGWindowWorkspace: CFStringRef;
CG_AVAILABLE_BUT_DEPRECATED(__MAC_10_5, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{ If present, the name of the application process which owns the window.
   The value of this key is a CFString. }

const kCGWindowOwnerName: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ If present, the name of the window. The value of this key is a
   CFString. }

const kCGWindowName: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ If present, true if the window is ordered on screen, false otherwise. If
   the key is not present, then the window is not ordered on screen. The
   value of this key is a CFBoolean. }

const kCGWindowIsOnscreen: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ If present, true if the window backing store is in video memory, false
   otherwise. If the key is not present, then the window backing store is in
   main memory. The value of this key is a CFBoolean. }

const kCGWindowBackingLocationVideoMemory: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{$endc} {TARGET_OS_MAC}

{ Flags for CGWindowListOption values.  These may be ORed together. }

const
{ List all windows in this user session, including both on- and
     off-screen windows. The parameter `relativeToWindow' should be
     `kCGNullWindowID'. }
	kCGWindowListOptionAll = 0;

  { List all on-screen windows in this user session, ordered from front to
     back. The parameter `relativeToWindow' should be `kCGNullWindowID'. }
	kCGWindowListOptionOnScreenOnly = 1 shl 0;

  { List all on-screen windows above the window specified by
     `relativeToWindow', ordered from front to back. }
	kCGWindowListOptionOnScreenAboveWindow = 1 shl 1;

  { List all on-screen windows below the window specified by
     `relativeToWindow', ordered from front to back. }
	kCGWindowListOptionOnScreenBelowWindow = 1 shl 2;

  { Include the window specified by `relativeToWindow' in any list,
     effectively creating `at-or-above' or `at-or-below' lists. }
	kCGWindowListOptionIncludingWindow = 1 shl 3;
    
  { Exclude any windows from the list that are elements of the desktop. }
	kCGWindowListExcludeDesktopElements = 1 shl 4;

{$ifc TARGET_OS_MAC}
{ Return an array of window dictionaries for windows within the user
   session.

   This function returns NULL if the caller is not running within a Quartz
   GUI session or the window server is disabled. You should release the
   array when you are finished using it. }

function CGWindowListCopyWindowInfo( option: CGWindowListOption; relativeToWindow: CGWindowID ): CFArrayRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ Return an array of CGWindowID values for windows within the user session.

   This function returns NULL if the caller is not running within a Quartz
   GUI session or the window server is disabled. You should release the
   array when you are finished using it. }

function CGWindowListCreate( option: CGWindowListOption; relativeToWindow: CGWindowID ): CFArrayRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ Return an array of window dictionaries, each corresponding to a window ID
   specified in `windowArray'.

   This function returns NULL if the caller is not running within a Quartz
   GUI session or the window server is disabled. You should release the
   array when you are finished using it. }

function CGWindowListCreateDescriptionFromArray( windowArray: CFArrayRef ): CFArrayRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{$endc} {TARGET_OS_MAC}

{ Flags for CGWindowImageOption values.  These may be ORed together. }

const
{ If `CGRectNull' is passed as the screen bounds, then then bounds
     computation includes window frame ornamentation, such as a shadow. }
	kCGWindowImageDefault = 0;

  { If `CGRectNull' is passed as the screen bounds, then then bounds
     computation excludes window frame ornamentation, such as a shadow. }
	kCGWindowImageBoundsIgnoreFraming = 1 shl 0;

  { Force the created image to be opaque.  Empty areas are white }
	kCGWindowImageShouldBeOpaque = 1 shl 1;

  { Only draw the windows' shadows, not the windows themselves. }
	kCGWindowImageOnlyShadows = 1 shl 2;

  { Return the best image resolution. The screen size may be
     different than the returned image size. }
	kCGWindowImageBestResolution = 1 shl 3;

  { Return the nominal image resolution. The screen size
     equals the returned image size. }
	kCGWindowImageNominalResolution = 1 shl 4;

{$ifc TARGET_OS_MAC}

{ Create an image containing a composite of the specified set of windows
   contained within a rectangular area. The set of windows is specified
   using options from `CGWindowListOption', along with an optional
   additional window ID.

   The windows list options are:

   --- kCGWindowListOptionAll, kCGWindowListOptionOnScreenOnly: Use all
   on-screen windows in this user session to construct the image. The
   parameter `windowID' should be `kCGNullWindowID'.

   --- kCGWindowListOptionOnScreenAboveWindow: Use all on-screen windows in
   this user session above the window specified by `windowID', ordered from
   front to back, to construct the image. To include the window specified by
   `windowID', add the flag `kCGWindowListOptionIncludingWindow'.

   --- kCGWindowListOptionOnScreenBelowWindow: Use all on-screen windows in
   this user session below the window specified by `windowID', ordered from
   front to back, to construct the image. To include the window specified by
   `windowID', add the flag `kCGWindowListOptionIncludingWindow'.

   --- kCGWindowListOptionIncludingWindow: Use only the window specified by
   `windowID' to construct the image.

   The parameter `screenBounds' specifies the rectangle in screen space
   (origin at the upper-left; y-value increasing downward). Setting
   `screenBounds' to `CGRectInfinite' will include all the windows on the
   entire desktop. Setting `screenBounds' to `CGRectNull' will use the
   bounding box of the specified windows as the screen space rectangle.

   The parameter `imageOptions' allows you to specify whether the window
   frame ornamentation, such as a shadow or similar effect, should be
   included or excluded in the bounds calculation when `CGRectNull' is
   specified for the window bounds.

   If no windows meet the specified criteria, or the windows can't be read,
   then a transparent black image will be returned.

   Any on-screen window with sharing type `kCGWindowSharingNone' will not
   be included in the image.

   This function returns NULL if the caller is not running within a Quartz
   GUI session or the window server is disabled. }

function CGWindowListCreateImage( screenBounds: CGRect; listOption: CGWindowListOption; windowID: CGWindowID; imageOption: CGWindowImageOption ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ Create an image containing a composite of the specified set of windows
   contained within a rectangular area à la `CGWindowListCreateImage'. The
   set of windows is specified by `windowArray', an array of window IDs. }

function CGWindowListCreateImageFromArray( screenBounds: CGRect; windowArray: CFArrayRef; imageOption: CGWindowImageOption ): CGImageRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{$endc} {TARGET_OS_MAC}

{ A CFNumberRef encoding appropriate for use with a CGWindowID. }
const
  kCGWindowIDCFNumberType = kCFNumberSInt32Type;

{ CFNumberRef encoding appropriate for use with CGWindowSharingType }
const
  kCGWindowSharingCFNumberType = kCFNumberSInt32Type;

{ CFNumberRef encoding appropriate for use with CGWindowBackingType }
const
  kCGWindowBackingCFNumberType = kCFNumberSInt32Type;


end.
