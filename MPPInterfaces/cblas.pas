{
   =================================================================================================
   Definitions of the Basic Linear Algebra Subprograms (BLAS) as provided Apple Computer.
   A few additional functions, unique to Mac OS, have also been provided.  
   These are clearly documented as Apple extensions.

   Documentation on the BLAS standard, including reference implementations, can be found on the web
   starting from the BLAS FAQ page at these URLs (verified live as of April 2002):
        http://www.netlib.org/blas/faq.html
        http://www.netlib.org/blas/blast-forum/blast-forum.html
   =================================================================================================
}
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit cblas;
interface
uses MacTypes;

{$ALIGN POWER}


{
   =================================================================================================
   Matrix shape and storage
   ========================
   Keeping the various matrix shape and storage parameters straight can be difficult.  The BLAS
   documentation generally makes a distinction between the concpetual "matrix" and the physical
   "array".  However there are a number of places where this becomes fuzzy because of the overall
   bias towards FORTRAN's column major storage.  The confusion is made worse by style differences
   between the level 2 and level 3 functions.  It is amplified further by the explicit choice of row
   or column major storage in the C interface.
   The storage order does not affect the actual computation that is performed.  That is, it does not
   affect the results other than where they appear in memory.  It does affect the values passed
   for so-called "leading dimension" parameters, such as lda in sgemv.  These are always the major
   stride in storage, allowing operations on rectangular subsets of larger matrices.  For row major
   storage this is the number of columns in the parent matrix, and for column major storage this is
   the number of rows in the parent matrix.
   For the level 2 functions, which deal with only a single matrix, the matrix shape parameters are
   always M and N.  These are the logical shape of the matrix, M rows by N columns.  The transpose
   parameter, such as transA in sgemv, defines whether the regular matrix or its transpose is used
   in the operation.  This affects the implicit length of the input and output vectors.  For example,
   if the regular matrix A is used in sgemv, the input vector X has length N, the number of columns
   of A, and the output vector Y has length M, the number of rows of A.  The length of the input and
   output vectors is not affected by the storage order of the matrix.
   The level 3 functions deal with 2 input matrices and one output matrix, the matrix shape parameters
   are M, N, and K.  The logical shape of the output matrix is always M by N, while K is the common
   dimension of the input matrices.  Like level 2, the transpose parameters, such as transA and transB
   in sgemm, define whether the regular input or its transpose is used in the operation.  However
   unlike level 2, in level 3 the transpose parameters affect the implicit shape of the input matrix.
   Consider sgemm, which computes "C = (alpha * A * B) + (beta * C)", where A and B might be regular
   or transposed.  The logical shape of C is always M rows by N columns.  The physical shape depends
   on the storage order parameter.  Using column major storage the declaration of C (the array) in C
   (the language) would be something like "float C[N][M]".  The logical shape of A without transposition
   is M by K, and B is K by N.  The one storage order parameter affects all three matrices.
   For those readers still wondering about the style differences between level 2 and level 3, they
   involve whether the input or output shapes are explicit.  For level 2, the input matrix shape is
   always M by N.  The input and output vector lengths are implicit and vary according to the
   transpose parameter.  For level 3, the output matrix shape is always M by N.  The input matrix
   shapes are implicit and vary according to the transpose parameters.
   =================================================================================================
}

type
	CBLAS_ORDER = SInt32;
const
	CblasRowMajor = 101;
	CblasColMajor = 102;
type
	CBLAS_TRANSPOSE = SInt32;
const
	CblasNoTrans = 111;
	CblasTrans = 112;
	CblasConjTrans = 113;
	AtlasConj = 114;
type
	CBLAS_UPLO = SInt32;
const
	CblasUpper = 121;
	CblasLower = 122;
type
	CBLAS_DIAG = SInt32;
const
	CblasNonUnit = 131;
	CblasUnit = 132;
type
	CBLAS_SIDE = SInt32;
const
	CblasLeft = 141;
	CblasRight = 142;

type
  CBLAS_INDEX = SInt32;

function cblas_errprn( ierr: SInt32; info: SInt32; var form: char; ... ): SInt32;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_xerbla( p: SInt32; var rout: char; var form: char; ... );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

{
 * ===========================================================================
 * Prototypes for level 1 BLAS functions (complex are recast as routines)
 * ===========================================================================
 }
function cblas_sdsdot( {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32; Y: Float32Ptr; {const} incY: SInt32 ): Float32;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_dsdot( {const} N: SInt32; X: Float32Ptr; {const} incX: SInt32; Y: Float32Ptr; {const} incY: SInt32 ): Float64;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_sdot( {const} N: SInt32; X: Float32Ptr; {const} incX: SInt32; Y: Float32Ptr; {const} incY: SInt32 ): Float32;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_ddot( {const} N: SInt32; X: Float64Ptr; {const} incX: SInt32; Y: Float64Ptr; {const} incY: SInt32 ): Float64;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
{
 * Functions having prefixes Z and C only
 }
procedure cblas_cdotu_sub( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; dotu: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_cdotc_sub( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; dotc: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_zdotu_sub( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; dotu: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zdotc_sub( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; dotc: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);


{
 * Functions having prefixes S D SC DZ
 }
function cblas_snrm2( {const} N: SInt32; X: Float32Ptr; {const} incX: SInt32 ): Float32;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_sasum( {const} N: SInt32; X: Float32Ptr; {const} incX: SInt32 ): Float32;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

function cblas_dnrm2( {const} N: SInt32; X: Float64Ptr; {const} incX: SInt32 ): Float64;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_dasum( {const} N: SInt32; X: Float64Ptr; {const} incX: SInt32 ): Float64;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

function cblas_scnrm2( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32 ): Float32;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_scasum( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32 ): Float32;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

function cblas_dznrm2( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32 ): Float64;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_dzasum( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32 ): Float64;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);


{
 * Functions having standard 4 prefixes (S D C Z)
 }
function cblas_isamax( {const} N: SInt32; X: Float32Ptr; {const} incX: SInt32 ): CBLAS_INDEX;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_idamax( {const} N: SInt32; X: Float64Ptr; {const} incX: SInt32 ): CBLAS_INDEX;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_icamax( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32 ): CBLAS_INDEX;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
function cblas_izamax( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32 ): CBLAS_INDEX;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

{
 * ===========================================================================
 * Prototypes for level 1 BLAS routines
 * ===========================================================================
 }

{
 * Routines with standard 4 prefixes (s, d, c, z)
 }
procedure cblas_sswap( {const} N: SInt32; X: Float32Ptr; {const} incX: SInt32; Y: Float32Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_scopy( {const} N: SInt32; X: Float32Ptr; {const} incX: SInt32; Y: Float32Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_saxpy( {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32; Y: Float32Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure catlas_saxpby( {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32; {const} beta: Float32; Y: Float32Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure catlas_sset( {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_dswap( {const} N: SInt32; X: Float64Ptr; {const} incX: SInt32; Y: Float64Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dcopy( {const} N: SInt32; X: Float64Ptr; {const} incX: SInt32; Y: Float64Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_daxpy( {const} N: SInt32; {const} alpha: Float64; X: Float64Ptr; {const} incX: SInt32; Y: Float64Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure catlas_daxpby( {const} N: SInt32; {const} alpha: Float64; X: Float64Ptr; {const} incX: SInt32; {const} beta: Float64; Y: Float64Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure catlas_dset( {const} N: SInt32; {const} alpha: Float64; X: Float64Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_cswap( {const} N: SInt32; X: univ Ptr; {const} incX: SInt32; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ccopy( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_caxpy( {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure catlas_caxpby( {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure catlas_cset( {const} N: SInt32; alpha: {const} univ Ptr; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_zswap( {const} N: SInt32; X: univ Ptr; {const} incX: SInt32; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zcopy( {const} N: SInt32; X: {const} univ Ptr; {const} incX: SInt32; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zaxpy( {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure catlas_zaxpby( {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure catlas_zset( {const} N: SInt32; alpha: {const} univ Ptr; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);


{
 * Routines with S and D prefix only
 }
procedure cblas_srotg( var a: Float32; var b: Float32; var c: Float32; var s: Float32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_srotmg( var d1: Float32; var d2: Float32; var b1: Float32; {const} b2: Float32; var P: Float32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_srot( {const} N: SInt32; var X: Float32; {const} incX: SInt32; var Y: Float32; {const} incY: SInt32; {const} c: Float32; {const} s: Float32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_srotm( {const} N: SInt32; var X: Float32; {const} incX: SInt32; var Y: Float32; {const} incY: SInt32; P: Float32Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_drotg( var a: Float64; var b: Float64; var c: Float64; var s: Float64 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_drotmg( var d1: Float64; var d2: Float64; var b1: Float64; {const} b2: Float64; var P: Float64 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_drot( {const} N: SInt32; var X: Float64; {const} incX: SInt32; var Y: Float64; {const} incY: SInt32; {const} c: Float64; {const} s: Float64 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_drotm( {const} N: SInt32; var X: Float64; {const} incX: SInt32; var Y: Float64; {const} incY: SInt32; P: Float64Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);


{
 * Routines with S D C Z CS and ZD prefixes
 }
procedure cblas_sscal( {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dscal( {const} N: SInt32; {const} alpha: Float64; X: Float64Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_cscal( {const} N: SInt32; alpha: {const} univ Ptr; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zscal( {const} N: SInt32; alpha: {const} univ Ptr; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_csscal( {const} N: SInt32; {const} alpha: Float32; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zdscal( {const} N: SInt32; {const} alpha: Float64; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

{
 * Extra reference routines provided by ATLAS, but not mandated by the standard
 }
procedure cblas_crotg( a: univ Ptr; b: univ Ptr; c: univ Ptr; s: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zrotg( a: univ Ptr; b: univ Ptr; c: univ Ptr; s: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_csrot( {const} N: SInt32; X: univ Ptr; {const} incX: SInt32; Y: univ Ptr; {const} incY: SInt32; {const} c: Float32; {const} s: Float32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zdrot( {const} N: SInt32; X: univ Ptr; {const} incX: SInt32; Y: univ Ptr; {const} incY: SInt32; {const} c: Float64; {const} s: Float64 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

{
 * ===========================================================================
 * Prototypes for level 2 BLAS
 * ===========================================================================
 }

{
 * Routines with standard 4 prefixes (S, D, C, Z)
 }
procedure cblas_sgemv( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; X: Float32Ptr; {const} incX: SInt32; {const} beta: Float32; Y: Float32Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_sgbmv( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} KL: SInt32; {const} KU: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; X: Float32Ptr; {const} incX: SInt32; {const} beta: Float32; Y: Float32Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_strmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; A: Float32Ptr; {const} lda: SInt32; X: Float32Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_stbmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} K: SInt32; A: Float32Ptr; {const} lda: SInt32; X: Float32Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_stpmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} Ap: Float32Ptr; X: Float32Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_strsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; A: Float32Ptr; {const} lda: SInt32; X: Float32Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_stbsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} K: SInt32; A: Float32Ptr; {const} lda: SInt32; X: Float32Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_stpsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} Ap: Float32Ptr; X: Float32Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_dgemv( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; X: Float64Ptr; {const} incX: SInt32; {const} beta: Float64; Y: Float64Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dgbmv( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} KL: SInt32; {const} KU: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; X: Float64Ptr; {const} incX: SInt32; {const} beta: Float64; Y: Float64Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dtrmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; A: Float64Ptr; {const} lda: SInt32; X: Float64Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dtbmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} K: SInt32; A: Float64Ptr; {const} lda: SInt32; X: Float64Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dtpmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} Ap: Float64Ptr; X: Float64Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dtrsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; A: Float64Ptr; {const} lda: SInt32; X: Float64Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dtbsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} K: SInt32; A: Float64Ptr; {const} lda: SInt32; X: Float64Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dtpsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} Ap: Float64Ptr; X: Float64Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_cgemv( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_cgbmv( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} KL: SInt32; {const} KU: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ctrmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; A: {const} univ Ptr; {const} lda: SInt32; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ctbmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} K: SInt32; A: {const} univ Ptr; {const} lda: SInt32; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ctpmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; Ap: {const} univ Ptr; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ctrsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; A: {const} univ Ptr; {const} lda: SInt32; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ctbsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} K: SInt32; A: {const} univ Ptr; {const} lda: SInt32; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ctpsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; Ap: {const} univ Ptr; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_zgemv( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zgbmv( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} KL: SInt32; {const} KU: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ztrmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; A: {const} univ Ptr; {const} lda: SInt32; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ztbmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} K: SInt32; A: {const} univ Ptr; {const} lda: SInt32; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ztpmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; Ap: {const} univ Ptr; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ztrsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; A: {const} univ Ptr; {const} lda: SInt32; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ztbsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; {const} K: SInt32; A: {const} univ Ptr; {const} lda: SInt32; X: univ Ptr; {const} incX: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ztpsv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} N: SInt32; Ap: {const} univ Ptr; X: univ Ptr; {const} incX: SInt32 );


{
 * Routines with S and D prefixes only
 }
procedure cblas_ssymv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; X: Float32Ptr; {const} incX: SInt32; {const} beta: Float32; Y: Float32Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ssbmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; X: Float32Ptr; {const} incX: SInt32; {const} beta: Float32; Y: Float32Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_sspmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float32; {const} Ap: Float32Ptr; {const} X: Float32Ptr; {const} incX: SInt32; {const} beta: Float32; Y: Float32Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_sger( {const} Order: CBLAS_ORDER; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32; Y: Float32Ptr; {const} incY: SInt32; A: Float32Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ssyr( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32; A: Float32Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_sspr( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32; Ap: Float32Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ssyr2( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32; Y: Float32Ptr; {const} incY: SInt32; A: Float32Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_sspr2( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float32; X: Float32Ptr; {const} incX: SInt32; Y: Float32Ptr; {const} incY: SInt32; A: Float32Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_dsymv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; X: Float64Ptr; {const} incX: SInt32; {const} beta: Float64; Y: Float64Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dsbmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; X: Float64Ptr; {const} incX: SInt32; {const} beta: Float64; Y: Float64Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dspmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float64; {const} Ap: Float64Ptr; {const} X: Float64Ptr; {const} incX: SInt32; {const} beta: Float64; Y: Float64Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dger( {const} Order: CBLAS_ORDER; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float64; X: Float64Ptr; {const} incX: SInt32; Y: Float64Ptr; {const} incY: SInt32; A: Float64Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dsyr( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float64; X: Float64Ptr; {const} incX: SInt32; A: Float64Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dspr( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float64; X: Float64Ptr; {const} incX: SInt32; Ap: Float64Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dsyr2( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float64; X: Float64Ptr; {const} incX: SInt32; Y: Float64Ptr; {const} incY: SInt32; A: Float64Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dspr2( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float64; X: Float64Ptr; {const} incX: SInt32; Y: Float64Ptr; {const} incY: SInt32; A: Float64Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);


{
 * Routines with C and Z prefixes only
 }
procedure cblas_chemv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_chbmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_chpmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; alpha: {const} univ Ptr; Ap: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_cgeru( {const} Order: CBLAS_ORDER; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; A: univ Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_cgerc( {const} Order: CBLAS_ORDER; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; A: univ Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_cher( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float32; X: {const} univ Ptr; {const} incX: SInt32; A: univ Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_chpr( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float32; X: {const} univ Ptr; {const} incX: SInt32; A: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_cher2( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; A: univ Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_chpr2( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; Ap: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_zhemv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zhbmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zhpmv( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; alpha: {const} univ Ptr; Ap: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; beta: {const} univ Ptr; Y: univ Ptr; {const} incY: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zgeru( {const} Order: CBLAS_ORDER; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; A: univ Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zgerc( {const} Order: CBLAS_ORDER; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; A: univ Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zher( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float64; X: {const} univ Ptr; {const} incX: SInt32; A: univ Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zhpr( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; {const} alpha: Float64; X: {const} univ Ptr; {const} incX: SInt32; A: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zher2( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; A: univ Ptr; {const} lda: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zhpr2( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} N: SInt32; alpha: {const} univ Ptr; X: {const} univ Ptr; {const} incX: SInt32; Y: {const} univ Ptr; {const} incY: SInt32; Ap: univ Ptr );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

{
 * ===========================================================================
 * Prototypes for level 3 BLAS
 * ===========================================================================
 }

{
 * Routines with standard 4 prefixes (S, D, C, Z)
 }
procedure cblas_sgemm( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} TransB: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; B: Float32Ptr; {const} ldb: SInt32; {const} beta: Float32; C: Float32Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ssymm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; B: Float32Ptr; {const} ldb: SInt32; {const} beta: Float32; C: Float32Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ssyrk( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; {const} beta: Float32; C: Float32Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ssyr2k( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; B: Float32Ptr; {const} ldb: SInt32; {const} beta: Float32; C: Float32Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_strmm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; B: Float32Ptr; {const} ldb: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_strsm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float32; A: Float32Ptr; {const} lda: SInt32; B: Float32Ptr; {const} ldb: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_dgemm( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} TransB: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; B: Float64Ptr; {const} ldb: SInt32; {const} beta: Float64; C: Float64Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dsymm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; B: Float64Ptr; {const} ldb: SInt32; {const} beta: Float64; C: Float64Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dsyrk( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; {const} beta: Float64; C: Float64Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dsyr2k( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; B: Float64Ptr; {const} ldb: SInt32; {const} beta: Float64; C: Float64Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dtrmm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; B: Float64Ptr; {const} ldb: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_dtrsm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} M: SInt32; {const} N: SInt32; {const} alpha: Float64; A: Float64Ptr; {const} lda: SInt32; B: Float64Ptr; {const} ldb: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_cgemm( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} TransB: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_csymm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_csyrk( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_csyr2k( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ctrmm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: univ Ptr; {const} ldb: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ctrsm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: univ Ptr; {const} ldb: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

procedure cblas_zgemm( {const} Order: CBLAS_ORDER; {const} TransA: CBLAS_TRANSPOSE; {const} TransB: CBLAS_TRANSPOSE; {const} M: SInt32; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zsymm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zsyrk( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zsyr2k( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ztrmm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: univ Ptr; {const} ldb: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_ztrsm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} TransA: CBLAS_TRANSPOSE; {const} Diag: CBLAS_DIAG; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: univ Ptr; {const} ldb: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);


{
 * Routines with prefixes C and Z only
 }
procedure cblas_chemm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_cherk( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float32; A: {const} univ Ptr; {const} lda: SInt32; {const} beta: Float32; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_cher2k( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; {const} beta: Float32; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zhemm( {const} Order: CBLAS_ORDER; {const} Side: CBLAS_SIDE; {const} Uplo: CBLAS_UPLO; {const} M: SInt32; {const} N: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; beta: {const} univ Ptr; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zherk( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; {const} alpha: Float64; A: {const} univ Ptr; {const} lda: SInt32; {const} beta: Float64; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);
procedure cblas_zher2k( {const} Order: CBLAS_ORDER; {const} Uplo: CBLAS_UPLO; {const} Trans: CBLAS_TRANSPOSE; {const} N: SInt32; {const} K: SInt32; alpha: {const} univ Ptr; A: {const} univ Ptr; {const} lda: SInt32; B: {const} univ Ptr; {const} ldb: SInt32; {const} beta: Float64; C: univ Ptr; {const} ldc: SInt32 );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

{
 Apple extensions follow.
 }

{
 The level 3 BLAS may allocate a large (approximately 4Mb) buffer to hold intermediate operands
 and results. These intermediate quantities are organized in memory for efficient access and
 so contribute to optimal performance. By default, this buffer is retained across calls to the
 BLAS. This strategy has substantial advantages when the BLAS are executed repeatedly. Clients
 who wish to free this buffer and return the memory allocation to the malloc heap can call the
 following routine at any time. Note that subsequent calls to the level 3 BLAS may allocate this
 buffer again.
 }

procedure ATLU_DestroyThreadMemory;
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

{
 -------------------------------------------------------------------------------------------------
 The BLAS standard requires that parameter errors be reported and cause the program to terminate.
 The default behavior for the Mac OS implementation of the BLAS is to print a message in English
 to stdout using printf and call exit with EXIT_FAILURE as the status.  If this is adequate, then
 you need do nothing more or worry about error handling.
 The BLAS standard also mentions a function, cblas_xerbla, suggesting that a program provide its
 own implementation to override the default error handling.  This will not work in the shared
 library environment of Mac OS.  Instead the Mac OS implementation provides a means to install
 an error handler.  There can only be one active error handler, installing a new one causes any
 previous handler to be forgotten.  Passing a null function pointer installs the default handler.
 The default handler is automatically installed at startup and implements the default behavior
 defined above.
 An error handler may return, it need not abort the program.  If the error handler returns, the
 BLAS routine also returns immediately without performing any processing.  Level 1 functions that
 return a numeric value return zero if the error handler returns.
 -------------------------------------------------------------------------------------------------
 }

type
	BLASParamErrorProc = procedure( funcName: ConstCStringPtr; paramName: ConstCStringPtr; const var paramPos: SInt32; const var paramValue: SInt32 );
procedure SetBLASParamErrorProc( ErrorProc: BLASParamErrorProc );
__OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);

{$ifc defined TARGET_CPU_PPC or defined TARGET_CPU_PPC64}

{$elsec}
  {$ifc defined TARGET_CPU_X86 or defined TARGET_CPU_X86_64}

  {$elsec}
{MW-ONLY-START}
	   {$errorc Unknown architecture}
{MW-ONLY-ELSE}
	   {$error Unknown architecture}
{MW-ONLY-FINISH}
  {$endc}
{$endc}

{$ifc defined __VEC__ or defined __SSE__}
(*
{
   -------------------------------------------------------------------------------------------------
   These routines provide optimized, SIMD-only support for common small matrix multiplications.
   They do not check for the availability of SIMD instructions or parameter errors.  They just do
   the multiplication as fast as possible.  Matrices are presumed to use row major storage.  Because
   these are all square, column major matrices can be multiplied by simply reversing the parameters.
   -------------------------------------------------------------------------------------------------
}


procedure vMultVecMat_4x4( ConstVectorFloat X[1]; ConstVectorFloat A[4][1]; VectorFloat Y[1] );
procedure vMultMatVec_4x4( ConstVectorFloat A[4][1]; ConstVectorFloat X[1]; VectorFloat Y[1] );
procedure vMultMatMat_4x4( ConstVectorFloat A[4][1]; ConstVectorFloat B[4][1]; VectorFloat C[4][1] );
procedure vMultVecMat_8x8( ConstVectorFloat X[2]; ConstVectorFloat A[8][2]; VectorFloat Y[2] );
procedure vMultMatVec_8x8( ConstVectorFloat A[8][2]; ConstVectorFloat X[2]; VectorFloat Y[2] );
procedure vMultMatMat_8x8( ConstVectorFloat A[8][2]; ConstVectorFloat B[8][2]; VectorFloat C[8][2] );
procedure vMultVecMat_16x16( ConstVectorFloat X[4]; ConstVectorFloat A[16][4]; VectorFloat Y[4] );
procedure vMultMatVec_16x16( ConstVectorFloat A[16][4]; ConstVectorFloat X[4]; VectorFloat Y[4] );
procedure vMultMatMat_16x16( ConstVectorFloat A[16][4]; ConstVectorFloat B[16][4]; VectorFloat C[16][4] );
procedure vMultVecMat_32x32( ConstVectorFloat X[8]; ConstVectorFloat A[32][8]; VectorFloat Y[8] );
procedure vMultMatVec_32x32( ConstVectorFloat A[32][8]; ConstVectorFloat X[8]; VectorFloat Y[8] );
procedure vMultMatMat_32x32( ConstVectorFloat A[32][8]; ConstVectorFloat B[32][8]; VectorFloat C[32][8] );
    
    {
     -------------------------------------------------------------------------------------------------
     These routines provide optimized support for common small matrix multiplications. They use
     the scalar floating point unit and have no dependancy on SIMD instructions. They are intended
     as complements to the AltiVec-only routines above. They do not check for parameter errors.  They just do
     the multiplication as fast as possible.  Matrices are presumed to use row major storage.  Because
     these are all square, column major matrices can be multiplied by simply reversing the parameters.
     -------------------------------------------------------------------------------------------------
     }
    
procedure sMultVecMat_4x4( ConstVectorFloat X[1]; ConstVectorFloat A[4][1]; VectorFloat Y[1] );
procedure sMultMatVec_4x4( ConstVectorFloat A[4][1]; ConstVectorFloat X[1]; VectorFloat Y[1] );
procedure sMultMatMat_4x4( ConstVectorFloat A[4][1]; ConstVectorFloat B[4][1]; VectorFloat C[4][1] );
procedure sMultVecMat_8x8( ConstVectorFloat X[2]; ConstVectorFloat A[8][2]; VectorFloat Y[2] );
procedure sMultMatVec_8x8( ConstVectorFloat A[8][2]; ConstVectorFloat X[2]; VectorFloat Y[2] );
procedure sMultMatMat_8x8( ConstVectorFloat A[8][2]; ConstVectorFloat B[8][2]; VectorFloat C[8][2] );
procedure sMultVecMat_16x16( ConstVectorFloat X[4]; ConstVectorFloat A[16][4]; VectorFloat Y[4] );
procedure sMultMatVec_16x16( ConstVectorFloat A[16][4]; ConstVectorFloat X[4]; VectorFloat Y[4] );
procedure sMultMatMat_16x16( ConstVectorFloat A[16][4]; ConstVectorFloat B[16][4]; VectorFloat C[16][4] );
procedure sMultVecMat_32x32( ConstVectorFloat X[8]; ConstVectorFloat A[32][8]; VectorFloat Y[8] );
procedure sMultMatVec_32x32( ConstVectorFloat A[32][8]; ConstVectorFloat X[8]; VectorFloat Y[8] );
procedure sMultMatMat_32x32( ConstVectorFloat A[32][8]; ConstVectorFloat B[32][8]; VectorFloat C[32][8] );
    
procedure dMultVecMat_4x4( const double X[4]; const double A[4][4]; double Y[4] );
procedure dMultMatVec_4x4( const double A[4][4]; const double X[4]; double Y[4] );
procedure dMultMatMat_4x4( const double A[4][4]; const double B[4][4]; double C[4][4] );
procedure dMultVecMat_8x8( const double X[8]; const double A[8][8]; double Y[8] );
procedure dMultMatVec_8x8( const double A[8][8]; const double X[8]; double Y[8] );
procedure dMultMatMat_8x8( const double A[8][8]; const double B[8][8]; double C[8][8] );
procedure dMultVecMat_16x16( const double X[16]; const double A[16][16]; double Y[16] );
procedure dMultMatVec_16x16( const double A[16][16]; const double X[16]; double Y[16] );
procedure dMultMatMat_16x16( const double A[16][16]; const double B[16][16]; double C[16][16] );
procedure dMultVecMat_32x32( const double X[32]; const double A[32][32]; double Y[32] );
procedure dMultMatVec_32x32( const double A[32][32]; const double X[32]; double Y[32] );
procedure dMultMatMat_32x32( const double A[32][32]; const double B[32][32]; double C[32][32] );
*)

{$endc} { defined(__VEC__) || defined(__SSE__)}

end.
