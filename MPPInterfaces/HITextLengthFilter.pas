{
     File:       HIToolbox/HITextLengthFilter.h
 
     Contains:   Header file for HITextLengthFilter object.
 
     Version:    HIToolbox-624~3
 
     Copyright:  � 1984-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HITextLengthFilter;
interface
uses MacTypes,CFString;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

{
    HITextLengthFilter
    
    This object enforces a maximum character length on a text field. It is typically attached as a
    delegate to an editable text view, such as an EditUnicodeText, HIComboBox, HISearchField, or
    HITextView object.
    
    This object handles the following Carbon events:
        
        kEventClassTextField
            kEventTextShouldChangeInRange
            
    This object does not send or post any Carbon events.
    
    This object has no dependencies on other AppObjects.
    
    This object is customizable to specify the maximum length for the text field.
    Your application customizes the maximum length for the text field by specifying
    a value for the kEventParamTextLength param of the kEventHIObjectInitialize event
    used when creating an instance of this object.
}


{
 *  kHITextLengthFilterClassID
 *  
 *  Discussion:
 *    HIObject class ID for the HITextLengthFilter object.
 }
const kHITextLengthFilterClassID = CFSTR( 'com.apple.appobjects.HITextLengthFilter' );

{
 *  Summary:
 *    Initialization event parameters for HITextLengthFilter objects.
 }
const
{
   * [typeUInt32]  The maximum text length in characters that is
   * allowed by this object.
   }
	kEventParamTextLength = FOUR_CHAR_CODE('TLEN');

{$endc} {TARGET_OS_MAC}

end.
