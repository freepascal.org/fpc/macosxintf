{ CoreGraphics - CGPDFObject.h
   Copyright (c) 2002-2011 Apple Inc.
   All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFObject;
interface
uses MacTypes,CGBase;
{$ALIGN POWER}


{ A type for boolean values. }

type
	CGPDFBoolean = UInt8;

{ A type for integer values. }

type
	CGPDFInteger = SIGNEDLONG;

{ A type for real values. }

type
	CGPDFReal = CGFloat;

{ A type to hold any object. }

type
	CGPDFObjectRef = ^OpaqueCGPDFObjectRef; { an opaque type }
	OpaqueCGPDFObjectRef = record end;

{ An identifier to describe an object's type. }

type
	CGPDFObjectType = SInt32;
const
	kCGPDFObjectTypeNull = 1;
	kCGPDFObjectTypeBoolean = 2;
	kCGPDFObjectTypeInteger = 3;
	kCGPDFObjectTypeReal = 4;
	kCGPDFObjectTypeName = 5;
	kCGPDFObjectTypeString = 6;
	kCGPDFObjectTypeArray = 7;
	kCGPDFObjectTypeDictionary = 8;
	kCGPDFObjectTypeStream = 9;

{ Return the type of `object'. }

function CGPDFObjectGetType( objct: CGPDFObjectRef ): CGPDFObjectType;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Get the value of `object'. If the type of `object' is equal to `type',
   then copy the value of `object' to `value' (if it's non-NULL) and return
   true. Otherwise, if the type of `object' is `kCGPDFObjectTypeInteger' and
   `type' is equal to `kCGPDFObjectTypeReal', then convert the value of
   `object' to floating point and copy the result to `value' (if it's
   non-NULL) and return true. Otherwise, return false. }

function CGPDFObjectGetValue( objct: CGPDFObjectRef; typ: CGPDFObjectType; value: univ Ptr ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);


end.
