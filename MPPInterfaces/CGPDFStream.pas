{ CoreGraphics - CGPDFStream.h
 * Copyright (c) 2002-2008 Apple Inc.
 * All rights reserved. }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGPDFStream;
interface
uses MacTypes,CGBase,CFData;
{$ALIGN POWER}


// CGPDFStreamRef defined in CGBase

type
	CGPDFDataFormat = SInt32;
const
	CGPDFDataFormatRaw = 0;
	CGPDFDataFormatJPEGEncoded = 1;
	CGPDFDataFormatJPEG2000 = 2;


{ Return the dictionary of `stream'. }

function CGPDFStreamGetDictionary( stream: CGPDFStreamRef ): CGPDFDictionaryRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);

{ Return the data of `stream'. }

function CGPDFStreamCopyData( stream: CGPDFStreamRef; var format: CGPDFDataFormat ): CFDataRef;
CG_AVAILABLE_STARTING(__MAC_10_3, __IPHONE_2_0);


end.
