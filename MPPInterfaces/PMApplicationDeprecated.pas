{
     File:       Print/PMApplicationDeprecated.h
 
     Contains:   Deprecated Carbon Printing Manager Interfaces.
 
	 Copyright  (c) 1998-2006, 2008, 2011 Apple Inc. All Rights Reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit PMApplicationDeprecated;
interface
uses MacTypes,Dialogs,PMDefinitions,PMDefinitionsDeprecated;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

{$ifc not TARGET_CPU_64}

{****************************************}
{ The following callbacks are deprecated }
{****************************************}
type
	PMItemProcPtr = procedure( theDialog: DialogRef; item: SInt16 );
	PMPrintDialogInitProcPtr = procedure( printSettings: PMPrintSettings; var theDialog: PMDialog );
	PMPageSetupDialogInitProcPtr = procedure( pageFormat: PMPageFormat; var theDialog: PMDialog );
{GPC-ONLY-START}
	PMItemUPP = UniversalProcPtr; // should be PMItemProcPtr
{GPC-ONLY-ELSE}
	PMItemUPP = PMItemProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	PMPrintDialogInitUPP = UniversalProcPtr; // should be PMPrintDialogInitProcPtr
{GPC-ONLY-ELSE}
	PMPrintDialogInitUPP = PMPrintDialogInitProcPtr;
{GPC-ONLY-FINISH}
{GPC-ONLY-START}
	PMPageSetupDialogInitUPP = UniversalProcPtr; // should be PMPageSetupDialogInitProcPtr
{GPC-ONLY-ELSE}
	PMPageSetupDialogInitUPP = PMPageSetupDialogInitProcPtr;
{GPC-ONLY-FINISH}

{
 *  NewPMItemUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function NewPMItemUPP( userRoutine: PMItemProcPtr ): PMItemUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewPMPrintDialogInitUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function NewPMPrintDialogInitUPP( userRoutine: PMPrintDialogInitProcPtr ): PMPrintDialogInitUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  NewPMPageSetupDialogInitUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function NewPMPageSetupDialogInitUPP( userRoutine: PMPageSetupDialogInitProcPtr ): PMPageSetupDialogInitUPP;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposePMItemUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure DisposePMItemUPP( userUPP: PMItemUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposePMPrintDialogInitUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure DisposePMPrintDialogInitUPP( userUPP: PMPrintDialogInitUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  DisposePMPageSetupDialogInitUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure DisposePMPageSetupDialogInitUPP( userUPP: PMPageSetupDialogInitUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokePMItemUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure InvokePMItemUPP( theDialog: DialogRef; item: SInt16; userUPP: PMItemUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokePMPrintDialogInitUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure InvokePMPrintDialogInitUPP( printSettings: PMPrintSettings; var theDialog: PMDialog; userUPP: PMPrintDialogInitUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{
 *  InvokePMPageSetupDialogInitUPP()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
procedure InvokePMPageSetupDialogInitUPP( pageFormat: PMPageFormat; var theDialog: PMDialog; userUPP: PMPageSetupDialogInitUPP );
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$ifc PM_USE_SESSION_APIS}

{
 *  PMSessionBeginDocument()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Instead use PMSessionBeginCGDocument.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.5
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionBeginDocument( printSession: PMPrintSession; printSettings: PMPrintSettings; pageFormat: PMPageFormat ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_5;

{
 *  PMSessionPageSetupDialogInit()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionPageSetupDialogInit( printSession: PMPrintSession; pageFormat: PMPageFormat; var newDialog: PMDialog ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionPrintDialogInit()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionPrintDialogInit( printSession: PMPrintSession; printSettings: PMPrintSettings; constPageFormat: PMPageFormat; var newDialog: PMDialog ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionPrintDialogMain()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionPrintDialogMain( printSession: PMPrintSession; printSettings: PMPrintSettings; constPageFormat: PMPageFormat; var accepted: Boolean; myInitProc: PMPrintDialogInitUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSessionPageSetupDialogMain()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMSessionPageSetupDialogMain( printSession: PMPrintSession; pageFormat: PMPageFormat; var accepted: Boolean; myInitProc: PMPageSetupDialogInitUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$elsec} {PM_USE_SESSION_APIS}

{
 *  PMBeginDocument()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionBeginDocument instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMBeginDocument( printSettings: PMPrintSettings; pageFormat: PMPageFormat; var printContext: PMPrintContext ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMEndDocument()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionEndDocument instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMEndDocument( printContext: PMPrintContext ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMBeginPage()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionBeginPage instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMBeginPage( printContext: PMPrintContext; const var pageFrame: PMRect ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMEndPage()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionEndPage instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMEndPage( printContext: PMPrintContext ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPageSetupDialog()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionPageSetupDialog instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPageSetupDialog( pageFormat: PMPageFormat; var accepted: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPrintDialog()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Use PMSessionPrintDialog instead.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPrintDialog( printSettings: PMPrintSettings; constPageFormat: PMPageFormat; var accepted: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPageSetupDialogInit()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPageSetupDialogInit( pageFormat: PMPageFormat; var newDialog: PMDialog ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPrintDialogInit()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPrintDialogInit( printSettings: PMPrintSettings; var newDialog: PMDialog ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPrintDialogInitWithPageFormat()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function PMPrintDialogInitWithPageFormat( printSettings: PMPrintSettings; constPageFormat: PMPageFormat; var newDialog: PMDialog ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPrintDialogMain()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPrintDialogMain( printSettings: PMPrintSettings; constPageFormat: PMPageFormat; var accepted: Boolean; myInitProc: PMPrintDialogInitUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMPageSetupDialogMain()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMPageSetupDialogMain( pageFormat: PMPageFormat; var accepted: Boolean; myInitProc: PMPageSetupDialogInitUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{$endc} {PM_USE_SESSION_APIS}

{
 *  PMGetDialogPtr()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetDialogPtr( pmDialog_: PMDialog; var theDialog: DialogRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{GPC-FPC-ONLY-START}
function PMGetDialogRef__NAMED__PMGetDialogPtr( pmDialog_: PMDialog; var theDialog: DialogRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;
{GPC-FPC-ONLY-FINISH}
{
 *  PMGetModalFilterProc()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetModalFilterProc( pmDialog_: PMDialog; var filterProc: ModalFilterUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSetModalFilterProc()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetModalFilterProc( pmDialog_: PMDialog; filterProc: ModalFilterUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetItemProc()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetItemProc( pmDialog_: PMDialog; var itemProc: PMItemUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSetItemProc()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetItemProc( pmDialog_: PMDialog; itemProc: PMItemUPP ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetDialogAccepted()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetDialogAccepted( pmDialog_: PMDialog; var process: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSetDialogAccepted()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetDialogAccepted( pmDialog_: PMDialog; process: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMGetDialogDone()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMGetDialogDone( pmDialog_: PMDialog; var done: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;


{
 *  PMSetDialogDone()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    You should create a PDE for your application instead of relying
 *    on this function.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only] but deprecated in 10.4
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   not available
 }
function PMSetDialogDone( pmDialog_: PMDialog; done: Boolean ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_4;

{$endc} {not TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}

end.
