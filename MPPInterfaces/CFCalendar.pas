{	CFCalendar.h
	Copyright (c) 2004-2013, Apple Inc. All rights reserved.
}
unit CFCalendar;
interface
uses MacTypes,CFBase,CFLocale,CFDate,CFTimeZone;
{$ALIGN POWER}


{#if MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_4}


type
	CFCalendarRef = ^__CFCalendar; { an opaque type }
	__CFCalendar = record end;

function CFCalendarGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarCopyCurrent: CFCalendarRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarCreateWithIdentifier( allocator: CFAllocatorRef; identifier: CFStringRef ): CFCalendarRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Create a calendar.  The identifiers are the kCF*Calendar
	// constants in CFLocale.h.

function CFCalendarGetIdentifier( calendar: CFCalendarRef ): CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
	// Returns the calendar's identifier.

function CFCalendarCopyLocale( calendar: CFCalendarRef ): CFLocaleRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

procedure CFCalendarSetLocale( calendar: CFCalendarRef; locale: CFLocaleRef );
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarCopyTimeZone( calendar: CFCalendarRef ): CFTimeZoneRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

procedure CFCalendarSetTimeZone( calendar: CFCalendarRef; tz: CFTimeZoneRef );
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarGetFirstWeekday( calendar: CFCalendarRef ): CFIndex;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

procedure CFCalendarSetFirstWeekday( calendar: CFCalendarRef; wkdy: CFIndex );
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarGetMinimumDaysInFirstWeek( calendar: CFCalendarRef ): CFIndex;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

procedure CFCalendarSetMinimumDaysInFirstWeek( calendar: CFCalendarRef; mwd: CFIndex );
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


type
	CFCalendarUnit = UNSIGNEDLONG;
const
	kCFCalendarUnitEra = 1 shl 1;
	kCFCalendarUnitYear = 1 shl 2;
	kCFCalendarUnitMonth = 1 shl 3;
	kCFCalendarUnitDay = 1 shl 4;
	kCFCalendarUnitHour = 1 shl 5;
	kCFCalendarUnitMinute = 1 shl 6;
	kCFCalendarUnitSecond = 1 shl 7;
	kCFCalendarUnitWeek = 1 shl 8;
	kCFCalendarUnitWeekday = 1 shl 9;
	kCFCalendarUnitWeekdayOrdinal = 1 shl 10;
	kCFCalendarUnitQuarter = 1 shl 11; { CF_AVAILABLE_STARTING(10_6, 4_0); }
	kCFCalendarUnitWeekOfMonth = 1 shl 12; { CF_AVAILABLE_STARTING(10_7, 5_0) }
	kCFCalendarUnitWeekOfYear = 1 shl 13; { CF_AVAILABLE_STARTING(10_7, 5_0) }
	kCFCalendarUnitYearForWeekOfYear = 1 shl 14; { CF_AVAILABLE_STARTING(10_7, 5_0) }

function CFCalendarGetMinimumRangeOfUnit( calendar: CFCalendarRef; unt: CFCalendarUnit ): CFRange;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarGetMaximumRangeOfUnit( calendar: CFCalendarRef; unt: CFCalendarUnit ): CFRange;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarGetRangeOfUnit( calendar: CFCalendarRef; smallerUnit: CFCalendarUnit; biggerUnit: CFCalendarUnit; at: CFAbsoluteTime ): CFRange;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarGetOrdinalityOfUnit( calendar: CFCalendarRef; smallerUnit: CFCalendarUnit; biggerUnit: CFCalendarUnit; at: CFAbsoluteTime ): CFIndex;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarGetTimeRangeOfUnit( calendar: CFCalendarRef; unt: CFCalendarUnit; at: CFAbsoluteTime; var startp: CFAbsoluteTime; var tip: CFTimeInterval ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_5_AND_LATER;

function CFCalendarComposeAbsoluteTime( calendar: CFCalendarRef; var at: { out } CFAbsoluteTime; componentDesc: ConstCStringPtr; ... ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarDecomposeAbsoluteTime( calendar: CFCalendarRef; at: CFAbsoluteTime; componentDesc: ConstCStringPtr; ... ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


const
	kCFCalendarComponentsWrap = 1 shl 0;  // option for adding

function CFCalendarAddComponents( calendar: CFCalendarRef; var at: { inout } CFAbsoluteTime; options: CFOptionFlags; componentDesc: ConstCStringPtr; ... ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

function CFCalendarGetComponentDifference( calendar: CFCalendarRef; startingAT: CFAbsoluteTime; resultAT: CFAbsoluteTime; options: CFOptionFlags; componentDesc: ConstCStringPtr; ... ): Boolean;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;



{#endif}


end.
