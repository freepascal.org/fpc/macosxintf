{
     File:       OSServices/WSTypes.h
 
     Contains:   *** DEPRECATED *** WebServicesCore Method Invocation API
 
     Copyright:  (c) 2002-2011 Apple Inc. All rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
unit WSTypes;
interface
uses MacTypes,CFBase;

{$ifc TARGET_OS_MAC}



{
    WSTypes
 }
{
    WebServicesCore error codes
 }


{$ALIGN MAC68K}

const
	errWSInternalError = -65793; { An internal framework error }
	errWSTransportError = -65794; { A network error occured }
	errWSParseError = -65795; { The server response wasn't valid XML }
	errWSTimeoutError = -65796; { The invocation timed out }


{
 *  WSTypeID
 *  
 *  Discussion:
 *    Internally, WebServicesCore uses the following enumeration when
 *    serializing between CoreFoundation and XML types. Because CFTypes
 *    are defined at runtime, it isn't always possible to produce a
 *    static mapping to a particular CFTypeRef.  This enum and
 *    associated API allows for static determination of the expected
 *    serialization.
 }
type
	WSTypeID = SInt32;
const
{
   * No mapping is known for this type
   }
	eWSUnknownType = 0;

  {
   * CFNullRef
   }
	eWSNullType = 1;

  {
   * CFBooleanRef
   }
	eWSBooleanType = 2;

  {
   * CFNumberRef for 8, 16, 32 bit integers
   }
	eWSIntegerType = 3;

  {
   * CFNumberRef for long double real numbers
   }
	eWSDoubleType = 4;

  {
   * CFStringRef
   }
	eWSStringType = 5;

  {
   * CFDateRef
   }
	eWSDateType = 6;

  {
   * CFDataRef
   }
	eWSDataType = 7;

  {
   * CFArrayRef
   }
	eWSArrayType = 8;

  {
   * CFDictionaryRef
   }
	eWSDictionaryType = 9;

type
	WSClientContextRetainCallBackProcPtr = function( info: univ Ptr ): UnivPtr;
	WSClientContextReleaseCallBackProcPtr = procedure( info: univ Ptr );
	WSClientContextCopyDescriptionCallBackProcPtr = function( info: univ Ptr ): CFStringRef;

{
 *  WSClientContext
 *  
 *  Discussion:
 *    Several calls in WebServicesCore take a callback with an optional
 *    context pointer.  The context is copied and the info pointer
 *    retained.  When the callback is made, the info pointer is passed
 *    to the callback.
 }
type
	WSClientContext = record
{
   * set to zero (0)
   }
		version: CFIndex;

  {
   * info pointer to be passed to the callback
   }
		info: UnivPtr;

  {
   * callback made on the info pointer. This field may be NULL.
   }
		retain: WSClientContextRetainCallBackProcPtr;

  {
   * callback made on the info pointer. This field may be NULL.
   }
		release: WSClientContextReleaseCallBackProcPtr;

  {
   * callback made on the info pointer. This field may be NULL.
   }
		copyDescription: WSClientContextCopyDescriptionCallBackProcPtr;
	end;
{
    Web Service protocol types.  These constant strings specify the type
    of web service method invocation created.  These are passed to
    WSMethodInvocationCreate.

    For information on these service types, see:

    XML-RPC:    <http://www.xml-rpc.com/spec/>
    SOAP 1.1:   <http://www.w3.org/TR/SOAP/>
    SOAP 1.2:   <http://www.w3.org/2002/ws/>
}
const kWSXMLRPCProtocol: CFStringRef;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2,__MAC_10_8,__IPHONE_NA,__IPHONE_NA);
const kWSSOAP1999Protocol: CFStringRef;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2,__MAC_10_8,__IPHONE_NA,__IPHONE_NA);
const kWSSOAP2001Protocol: CFStringRef;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2,__MAC_10_8,__IPHONE_NA,__IPHONE_NA);


{
 *  WSGetWSTypeIDFromCFType()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Returns the WSTypeID associated with CFTypeRef.  There is not a
 *    one to one mapping between CFTypeID and WSTypesID therefore an
 *    actual instance of a CFType must be passed.
 *  
 *  Mac OS X threading:
 *    Thread safe
 *  
 *  Parameters:
 *    
 *    ref:
 *      a CFTypeRef object
 *  
 *  Result:
 *    the WSTypeID used in serializing the object.  If no WSTypeID
 *    matches, eWSUnknownType is returned.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later but deprecated in 10.8
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function WSGetWSTypeIDFromCFType( ref: CFTypeRef ): WSTypeID;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2,__MAC_10_8,__IPHONE_NA,__IPHONE_NA);


{
 *  WSGetCFTypeIDFromWSTypeID()   *** DEPRECATED ***
 *  
 *  Discussion:
 *    Returns the CFTypeID that is associated with a given WSTypeID. 
 *    CFTypeIDs are only valid during a particular instance of a
 *    process and should not be used as static values.
 *  
 *  Mac OS X threading:
 *    Thread safe
 *  
 *  Parameters:
 *    
 *    typeID:
 *      a WSTypeID constant
 *  
 *  Result:
 *    a CFTypeID, or 0 if not found
 *  
 *  Availability:
 *    Mac OS X:         in version 10.2 and later but deprecated in 10.8
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function WSGetCFTypeIDFromWSTypeID( typeID: WSTypeID ): CFTypeID;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_2,__MAC_10_8,__IPHONE_NA,__IPHONE_NA);



{$endc} {TARGET_OS_MAC}


end.
