{	CFNumber.h
	Copyright (c) 1999-2013, Apple Inc. All rights reserved.
}
unit CFNumber;
interface
uses MacTypes,CFBase;
{$ALIGN POWER}


type
	CFBooleanRef = ^__CFBoolean; { an opaque type }
	__CFBoolean = record end;
	CFBooleanRefPtr = ^CFBooleanRef;

const kCFBooleanTrue: CFBooleanRef;
const kCFBooleanFalse: CFBooleanRef;

function CFBooleanGetTypeID: CFTypeID;

function CFBooleanGetValue( value: CFBooleanRef ): Boolean;

type
	CFNumberType = CFIndex;
const
																{  Types from MacTypes.h  }
	kCFNumberSInt8Type = 1;
	kCFNumberSInt16Type = 2;
	kCFNumberSInt32Type = 3;
	kCFNumberSInt64Type = 4;
	kCFNumberFloat32Type = 5;
	kCFNumberFloat64Type = 6;							{  64-bit IEEE 754  }
																{  Basic C types  }
	kCFNumberCharType = 7;
	kCFNumberShortType = 8;
	kCFNumberIntType = 9;
	kCFNumberLongType = 10;
	kCFNumberLongLongType = 11;
	kCFNumberFloatType = 12;
	kCFNumberDoubleType = 13;							{  Other  }
	kCFNumberCFIndexType = 14;
  kCFNumberMaxType_MAC_OS_X_VERSION_PRE_10_5 = 14;
{#if MAC_OS_X_VERSION_10_5 <= MAC_OS_X_VERSION_MAX_ALLOWED}
  kCFNumberNSIntegerType = 15; CF_AVAILABLE_STARTING(10_5, 2_0);
  kCFNumberCGFloatType = 16; CF_AVAILABLE_STARTING(10_5, 2_0);
  kCFNumberMaxType = 16;
{#else
 kCFNumberMaxType = 14
#endif}

type
	CFNumberRef = ^__CFNumber; { an opaque type }
	__CFNumber = record end;
	CFNumberRefPtr = ^CFNumberRef;

const kCFNumberPositiveInfinity: CFNumberRef;
const kCFNumberNegativeInfinity: CFNumberRef;
const kCFNumberNaN: CFNumberRef;

function CFNumberGetTypeID: CFTypeID;

{
	Creates a CFNumber with the given value. The type of number pointed
	to by the valuePtr is specified by type. If type is a floating point
	type and the value represents one of the infinities or NaN, the
	well-defined CFNumber for that value is returned. If either of
	valuePtr or type is an invalid value, the result is undefined.
}
function CFNumberCreate( allocator: CFAllocatorRef; theType: CFNumberType; valuePtr: {const} univ Ptr ): CFNumberRef;

{
	Returns the storage format of the CFNumber's value.  Note that
	this is not necessarily the type provided in CFNumberCreate().
}
function CFNumberGetType( number: CFNumberRef ): CFNumberType;

{
	Returns the size in bytes of the type of the number.
}
function CFNumberGetByteSize( number: CFNumberRef ): CFIndex;

{
	Returns true if the type of the CFNumber's value is one of
	the defined floating point types.
}
function CFNumberIsFloatType( number: CFNumberRef ): Boolean;

{
	Copies the CFNumber's value into the space pointed to by
	valuePtr, as the specified type. If conversion needs to take
	place, the conversion rules follow human expectation and not
	C's promotion and truncation rules. If the conversion is
	lossy, or the value is out of range, false is returned. Best
	attempt at conversion will still be in *valuePtr.
}
function CFNumberGetValue( number: CFNumberRef; theType: CFNumberType; valuePtr: univ Ptr ): Boolean;

{
	Compares the two CFNumber instances. If conversion of the
	types of the values is needed, the conversion and comparison
	follow human expectations and not C's promotion and comparison
	rules. Negative zero compares less than positive zero.
	Positive infinity compares greater than everything except
	itself, to which it compares equal. Negative infinity compares
	less than everything except itself, to which it compares equal.
	Unlike standard practice, if both numbers are NaN, then they
	compare equal; if only one of the numbers is NaN, then the NaN
	compares greater than the other number if it is negative, and
	smaller than the other number if it is positive. (Note that in
	CFEqual() with two CFNumbers, if either or both of the numbers
	is NaN, true is returned.)
}
function CFNumberCompare( number: CFNumberRef; otherNumber: CFNumberRef; context: univ Ptr ): CFComparisonResult;


end.
