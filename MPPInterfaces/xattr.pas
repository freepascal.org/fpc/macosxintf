{
 * Copyright (c) 2004-2010 Apple Inc. All rights reserved.
 *
 * @APPLE_OSREFERENCE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. The rights granted to you under the License
 * may not be used to create, or enable the creation or redistribution of,
 * unlawful or unlicensed copies of an Apple operating system, or to
 * circumvent, violate, or enable the circumvention or violation of, any
 * terms of an Apple operating system software license agreement.
 * 
 * Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_OSREFERENCE_LICENSE_HEADER_END@
 }
{  Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit xattr;
interface
uses MacTypes;
{$ALIGN POWER}

const
// Options for pathname based xattr calls
	XATTR_NOFOLLOW = $0001;     // Don't follow symbolic links

// Options for setxattr calls
	XATTR_CREATE = $0002;     // set the value, fail if attr already exists
	XATTR_REPLACE = $0004;     // set the value, fail if attr does not exist

// Set this to bypass authorization checking (eg. if doing auth-related work)
	XATTR_NOSECURITY = $0008;

{ Set this to bypass the default extended attribute file (dot-underscore file) }
const
	XATTR_NODEFAULT = $0010;

{ option for f/getxattr() and f/listxattr() to expose the HFS Compression extended attributes }
const
	XATTR_SHOWCOMPRESSION = $0020;

const
	XATTR_MAXNAMELEN = 127;

{ See the ATTR_CMN_FNDRINFO section of getattrlist(2) for details on FinderInfo }
const
	XATTR_FINDERINFO_NAME = 'com.apple.FinderInfo';

const
	XATTR_RESOURCEFORK_NAME = 'com.apple.ResourceFork';


function getxattr( path: ConstCStringPtr; name: ConstCStringPtr; value: UnivPtr; siz: size_t; position: UInt32; options: SInt32 ): ssize_t;

function fgetxattr( fd: SInt32; name: ConstCStringPtr; value: UnivPtr; siz: size_t; position: UInt32; options: SInt32 ): ssize_t;

function setxattr( path: ConstCStringPtr; name: ConstCStringPtr; value: {const} UnivPtr; siz: size_t; position: UInt32; options: SInt32 ): SInt32;

function fsetxattr( fd: SInt32; name: ConstCStringPtr; value: {const} UnivPtr; siz: size_t; position: UInt32; options: SInt32 ): SInt32;

function removexattr( path: ConstCStringPtr; name: ConstCStringPtr; options: SInt32 ): SInt32;

function fremovexattr( fd: SInt32; name: ConstCStringPtr; options: SInt32 ): SInt32;

function listxattr( path: ConstCStringPtr; namebuff: UnivPtr; siz: size_t; options: SInt32 ): ssize_t;
 
function flistxattr( fd: SInt32; namebuff: UnivPtr; siz: size_t; options: SInt32 ): ssize_t;

end.
