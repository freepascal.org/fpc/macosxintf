{
     File:       OpenScripting/OSAComp.h
 
     Contains:   AppleScript Component Implementor's Interfaces.
 
     Version:    OSA-148~28
 
     Copyright:  � 1992-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}

{  Pascal Translation Updated: Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit OSAComp;
interface
uses MacTypes,AEDataModel;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{*************************************************************************
    Types and Constants
*************************************************************************}
{*************************************************************************
    Routines for Associating a Storage Type with a Script Data Handle 
*************************************************************************}
{
 *  OSAGetStorageType()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetStorageType( scriptData: AEDataStorage; var dscType: DescType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  OSAAddStorageType()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAAddStorageType( scriptData: AEDataStorage; dscType: DescType ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  OSARemoveStorageType()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSARemoveStorageType( scriptData: AEDataStorage ): OSErr;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{$endc} {TARGET_OS_MAC}

end.
