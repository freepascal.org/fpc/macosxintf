{
 *  CVOpenGLBuffer.h
 *  CoreVideo
 *
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 } 
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }
unit CVOpenGLBuffer;
interface
uses MacTypes,CFBase,CFString,CFDictionary,CVReturns,CVImageBuffer,CGLTypes,macgl;

{$ifc TARGET_OS_MAC}
 
{$ALIGN POWER}

 {! @header CVOpenGLBuffer.h
	@copyright 2004 Apple Computer, Inc. All rights reserved.
	@availability Mac OS X 10.4 or later
    @discussion A CoreVideo buffer derives from a generic buffer and can be an ImageBuffer or PixelBuffer. 
		   
}


const kCVOpenGLBufferWidth: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCVOpenGLBufferHeight: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCVOpenGLBufferTarget: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCVOpenGLBufferInternalFormat: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCVOpenGLBufferMaximumMipmapLevel: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

type
	CVOpenGLBufferRef = CVImageBufferRef;

function CVOpenGLBufferGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLBufferRetain
    @abstract   Retains a CVOpenGLBuffer object
    @discussion Equivalent to CFRetain, but NULL safe
    @param      buffer A CVOpenGLBuffer object that you want to retain.
    @result     A CVOpenGLBuffer object that is the same as the passed in buffer.
}
function CVOpenGLBufferRetain( buffer: CVOpenGLBufferRef ): CVOpenGLBufferRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLBufferRelease
    @abstract   Releases a CVOpenGLBuffer object
    @discussion Equivalent to CFRelease, but NULL safe
    @param      buffer A CVOpenGLBuffer object that you want to release.
}
procedure CVOpenGLBufferRelease( buffer: CVOpenGLBufferRef );
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLBufferCreate
    @abstract   Create a new CVOpenGLBuffer that may be used for OpenGL rendering purposes
    @param      width The width of the buffer in pixels
    @param      height The height of the buffer in pixels
    @param      attributes A CFDictionaryRef containing other desired attributes of the buffer (texture target, internal format, max mipmap level, etc.).
		May be NULL.  Defaults are GL_TEXTURE_RECTANGLE_EXT, GL_RGBA, and 0 for kCVOpenGLBufferTarget, kCVOpenGLBufferInternalFormat and kCVOpenGLBufferMaximumMipmapLevel,
		respectively.
    @param      bufferOut    The newly created buffer will be placed here.
    @result     kCVReturnSuccess if the attachment succeeded
}
function CVOpenGLBufferCreate( allocator: CFAllocatorRef; width: size_t; height: size_t; attributes: CFDictionaryRef; var bufferOut: CVOpenGLBufferRef ): CVReturn;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLBufferGetAttributes
    @param      openGLBuffer Target OpenGL Buffer.
    @result     CVOpenGLBuffer attributes dictionary, NULL if not set.
}
function CVOpenGLBufferGetAttributes( openGLBuffer: CVOpenGLBufferRef ): CFDictionaryRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLBufferAttach
    @param      openGLBuffer The buffer you wish to attach a GL context to
    @param      cglContext   The CGLContextObj you wish to attach
    @param      face	     The target GL face enum (0 for non cube maps)
    @param      level        The mipmap level index you wish to attach to
    @param      screen       The virtual screen number you want to use
    @result     kCVReturnSuccess if the attachment succeeded
}
function CVOpenGLBufferAttach( openGLBuffer: CVOpenGLBufferRef; cglContext: CGLContextObj; face: GLenum; level: GLint; screen: GLint ): CVReturn;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{$endc}	// TARGET_OS_MAC

end.
