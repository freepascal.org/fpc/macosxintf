{
     File:       CarbonCore/Endian.h
 
     Contains:   Endian swapping utilties
                 The contents of this header file are deprecated.
                 Use CFByteOrder API instead.
 
     Copyright:  � 1997-2011 by Apple Inc. All rights reserved.
}
unit Endian;
interface
uses MacTypes;


{$ALIGN MAC68K}

{
    This file provides Endian Flipping routines for dealing with converting data
    between Big-Endian and Little-Endian machines.  These routines are useful
    when writing code to compile for both Big and Little Endian machines and  
    which must handle other endian number formats, such as reading or writing 
    to a file or network packet.
    
    These routines are named as follows:
    
        Endian<U><W>_<S>to<D>

    where
        <U> is whether the integer is signed ('S') or unsigned ('U')
        <W> is integer bit width: 16, 32, or 64 
        <S> is the source endian format: 'B' for big, 'L' for little, or 'N' for native
        <D> is the destination endian format: 'B' for big, 'L' for little, or 'N' for native
    
    For example, to convert a Big Endian 32-bit unsigned integer to the current native format use:
        
        long i = EndianU32_BtoN(data);
        
    This file is set up so that the function macro to nothing when the target runtime already
    is the desired format (e.g. on Big Endian machines, EndianU32_BtoN() macros away).
            
    If long long's are not supported, you cannot get 64-bit quantities as a single value.
    The macros are not defined in that case.

    For gcc, the macros build on top of the inline byte swapping
    routines from <libkern/OSByteOrder.h>, which may have better performance.
    
    
                                <<< W A R N I N G >>>
    
    It is very important not to put any autoincrements inside the macros.  This 
    will produce erroneous results because each time the address is accessed in the macro, 
    the increment occurs.
    
 }
 // Macros might be better solutions
implemented function Endian16_Swap( arg: UInt16 ): UInt16; inline;
implemented function Endian32_Swap( arg: UInt32 ): UInt32; inline;
implemented function Endian64_Swap_Pascal( arg: UInt64 ): UInt64; inline;
implemented function EndianS16_Swap( arg: SInt16 ): SInt16; inline;
implemented function EndianS32_Swap( arg: SInt32 ): SInt32; inline;
implemented function EndianS64_Swap( arg: SInt64 ): SInt64; inline;

{GPC-ONLY-START}
function Endian64_Swap__NAMED__Endian64_Swap_Pascal( arg: UInt64 ): UInt64;
{GPC-ONLY-FINISH}

{FPC-ONLY-START}
implemented function Endian64_Swap( arg: UInt64 ): UInt64; inline;
{FPC-ONLY-FINISH}
//  Macro away no-op functions

{$ifc TARGET_RT_BIG_ENDIAN}

{FPC-ONLY-START}
implemented function EndianS16_BtoN( arg: SInt16 ): SInt16; inline;
implemented function EndianS16_NtoB( arg: SInt16 ): SInt16; inline;
implemented function EndianU16_BtoN( arg: UInt16 ): UInt16; inline;
implemented function EndianU16_NtoB( arg: UInt16 ): UInt16; inline;
implemented function EndianS32_BtoN( arg: SInt32 ): SInt32; inline;
implemented function EndianS32_NtoB( arg: SInt32 ): SInt32; inline;
implemented function EndianU32_BtoN( arg: UInt32 ): UInt32; inline;
implemented function EndianU32_NtoB( arg: UInt32 ): UInt32; inline;
implemented function EndianS64_BtoN( arg: SInt64 ): SInt64; inline;
implemented function EndianS64_NtoB( arg: SInt64 ): SInt64; inline;
implemented function EndianU64_BtoN( arg: UInt64 ): UInt64; inline;
implemented function EndianU64_NtoB( arg: UInt64 ): UInt64; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS16_BtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS16_NtoB( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU16_BtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU16_NtoB( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS32_BtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS32_NtoB( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU32_BtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU32_NtoB( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS64_BtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS64_NtoB( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU64_BtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU64_NtoB( arg ) (arg)}
{FPC-ONLY-FINISH}

{$elsec}

{FPC-ONLY-START}
implemented function EndianS16_LtoN( arg: SInt16 ): SInt16; inline;
implemented function EndianS16_NtoL( arg: SInt16 ): SInt16; inline;
implemented function EndianU16_LtoN( arg: UInt16 ): UInt16; inline;
implemented function EndianU16_NtoL( arg: UInt16 ): UInt16; inline;
implemented function EndianS32_LtoN( arg: SInt32 ): SInt32; inline;
implemented function EndianS32_NtoL( arg: SInt32 ): SInt32; inline;
implemented function EndianU32_LtoN( arg: UInt32 ): UInt32; inline;
implemented function EndianU32_NtoL( arg: UInt32 ): UInt32; inline;
implemented function EndianS64_LtoN( arg: SInt64 ): SInt64; inline;
implemented function EndianS64_NtoL( arg: SInt64 ): SInt64; inline;
implemented function EndianU64_LtoN( arg: UInt64 ): UInt64; inline;
implemented function EndianU64_NtoL( arg: UInt64 ): UInt64; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS16_LtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS16_NtoL( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU16_LtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU16_NtoL( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS32_LtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS32_NtoL( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU32_LtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU32_NtoL( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS64_LtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS64_NtoL( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU64_LtoN( arg ) (arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU64_NtoL( arg ) (arg)}
{FPC-ONLY-FINISH}

{$endc}

//  Map native to actual

{$ifc TARGET_RT_BIG_ENDIAN}

{FPC-ONLY-START}
implemented function EndianS16_LtoN( arg: SInt16 ): SInt16; inline;
implemented function EndianS16_NtoL( arg: SInt16 ): SInt16; inline;
implemented function EndianU16_LtoN( arg: UInt16 ): UInt16; inline;
implemented function EndianU16_NtoL( arg: UInt16 ): UInt16; inline;
implemented function EndianS32_LtoN( arg: SInt32 ): SInt32; inline;
implemented function EndianS32_NtoL( arg: SInt32 ): SInt32; inline;
implemented function EndianU32_LtoN( arg: UInt32 ): UInt32; inline;
implemented function EndianU32_NtoL( arg: UInt32 ): UInt32; inline;
implemented function EndianS64_LtoN( arg: SInt64 ): SInt64; inline;
implemented function EndianS64_NtoL( arg: SInt64 ): SInt64; inline;
implemented function EndianU64_LtoN( arg: UInt64 ): UInt64; inline;
implemented function EndianU64_NtoL( arg: UInt64 ): UInt64; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS16_LtoN( arg ) EndianS16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS16_NtoL( arg ) EndianS16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU16_LtoN( arg ) Endian16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU16_NtoL( arg ) Endian16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS32_LtoN( arg ) EndianS32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS32_NtoL( arg ) EndianS32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU32_LtoN( arg ) Endian32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU32_NtoL( arg ) Endian32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS64_LtoN( arg ) EndianS64_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS64_NtoL( arg ) EndianS64_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU64_LtoN( arg ) Endian64_Swap_Pascal(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU64_NtoL( arg ) Endian64_Swap_Pascal(arg)}
{FPC-ONLY-FINISH}

{$elsec}

{FPC-ONLY-START}
implemented function EndianS16_BtoN( arg: SInt16 ): SInt16; inline;
implemented function EndianS16_NtoB( arg: SInt16 ): SInt16; inline;
implemented function EndianU16_BtoN( arg: UInt16 ): UInt16; inline;
implemented function EndianU16_NtoB( arg: UInt16 ): UInt16; inline;
implemented function EndianS32_BtoN( arg: SInt32 ): SInt32; inline;
implemented function EndianS32_NtoB( arg: SInt32 ): SInt32; inline;
implemented function EndianU32_BtoN( arg: UInt32 ): UInt32; inline;
implemented function EndianU32_NtoB( arg: UInt32 ): UInt32; inline;
implemented function EndianS64_BtoN( arg: SInt64 ): SInt64; inline;
implemented function EndianS64_NtoB( arg: SInt64 ): SInt64; inline;
implemented function EndianU64_BtoN( arg: UInt64 ): UInt64; inline;
implemented function EndianU64_NtoB( arg: UInt64 ): UInt64; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS16_BtoN( arg ) EndianS16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS16_NtoB( arg ) EndianS16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU16_BtoN( arg ) Endian16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU16_NtoB( arg ) Endian16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS32_BtoN( arg ) EndianS32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS32_NtoB( arg ) EndianS32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU32_BtoN( arg ) Endian32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU32_NtoB( arg ) Endian32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS64_BtoN( arg ) EndianS64_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianS64_NtoB( arg ) EndianS64_Swap(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU64_BtoN( arg ) Endian64_Swap_Pascal(arg)}
{$mwgpcdefinec TARGET_RT_LITTLE_ENDIAN:EndianU64_NtoB( arg ) Endian64_Swap_Pascal(arg)}
{FPC-ONLY-FINISH}

{$endc}

//     Implement *LtoB and *BtoL

{FPC-ONLY-START}
implemented function EndianS16_LtoB( arg: SInt16 ): SInt16; inline;
implemented function EndianS16_BtoL( arg: SInt16 ): SInt16; inline;
implemented function EndianU16_LtoB( arg: UInt16 ): UInt16; inline;
implemented function EndianU16_BtoL( arg: UInt16 ): UInt16; inline;
implemented function EndianS32_LtoB( arg: SInt32 ): SInt32; inline;
implemented function EndianS32_BtoL( arg: SInt32 ): SInt32; inline;
implemented function EndianU32_LtoB( arg: UInt32 ): UInt32; inline;
implemented function EndianU32_BtoL( arg: UInt32 ): UInt32; inline;
implemented function EndianS64_LtoB( arg: SInt64 ): SInt64; inline;
implemented function EndianS64_BtoL( arg: SInt64 ): SInt64; inline;
implemented function EndianU64_LtoB( arg: UInt64 ): UInt64; inline;
implemented function EndianU64_BtoL( arg: UInt64 ): UInt64; inline;
{FPC-ONLY-ELSE}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS16_LtoB( arg ) EndianS16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS16_BtoL( arg ) EndianS16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU16_LtoB( arg ) Endian16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU16_BtoL( arg ) Endian16_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS32_LtoB( arg ) EndianS32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS32_BtoL( arg ) EndianS32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU32_LtoB( arg ) Endian32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU32_BtoL( arg ) Endian32_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS64_LtoB( arg ) EndianS64_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianS64_BtoL( arg ) EndianS64_Swap(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU64_LtoB( arg ) Endian64_Swap_Pascal(arg)}
{$mwgpcdefinec TARGET_RT_BIG_ENDIAN:EndianU64_BtoL( arg ) Endian64_Swap_Pascal(arg)}
{FPC-ONLY-FINISH}

{
   These types are used for structures that contain data that is
   always in BigEndian format.  This extra typing prevents little
   endian code from directly changing the data, thus saving much
   time in the debugger.
}

{$ifc TARGET_RT_LITTLE_ENDIAN}

type
	BigEndianUInt32 = record
		bigEndianValue: UInt32;
	end;
type
	BigEndianLong = record
		bigEndianValue: SIGNEDLONG;
	end;
type
	BigEndianUnsignedLong = record
		bigEndianValue: UNSIGNEDLONG;
	end;
type
	BigEndianShort = record
		bigEndianValue: SInt16;
	end;
type
	BigEndianUnsignedShort = record
		bigEndianValue: UInt16;
	end;
type
	BigEndianFixed = record
		bigEndianValue: Fixed;
	end;
type
	BigEndianUnsignedFixed = record
		bigEndianValue: UnsignedFixed;
	end;
type
	BigEndianOSType = record
		bigEndianValue: OSType;
	end;

{$elsec}

type
	BigEndianUInt32 = UInt32;
	BigEndianLong = SIGNEDLONG;
	BigEndianUnsignedLong = UNSIGNEDLONG;
	BigEndianShort = SInt16;
	BigEndianUnsignedShort = UInt16;
	BigEndianFixed = Fixed;
	BigEndianUnsignedFixed = UnsignedFixed;
	BigEndianOSType = OSType;
{$endc}  {TARGET_RT_LITTLE_ENDIAN}

type
	BigEndianUInt32Ptr = ^BigEndianUInt32;
	BigEndianLongPtr = ^BigEndianLong;
	BigEndianUnsignedLongPtr = ^BigEndianUnsignedLong;
	BigEndianShortPtr = ^BigEndianShort;
	BigEndianUnsignedShortPtr = ^BigEndianUnsignedShort;
	BigEndianFixedPtr = ^BigEndianFixed;
	BigEndianUnsignedFixedPtr = ^BigEndianUnsignedFixed;
	BigEndianOSTypePtr = ^BigEndianOSType;

{$ifc TARGET_API_MAC_OSX}
{
        CoreEndian flipping API.

        This API is used to generically massage data buffers, in
        place, from one endian architecture to another.  In effect,
        the API supports registering a set of callbacks that can
        effect this translation.  

        The data types have specific meanings within their domain,
        although some data types can be registered with the same
        callback in several domains.  There is no wildcard domain.

        A set of pre-defined flippers are implemented by the Carbon
        frameworks for most common resource manager and AppleEvent data
        types.
  }
const
	kCoreEndianResourceManagerDomain = FOUR_CHAR_CODE('rsrc');
	kCoreEndianAppleEventManagerDomain = FOUR_CHAR_CODE('aevt');


{
 *  CoreEndianFlipProc
 *  
 *  Discussion:
 *    Callback use to flip endian-ness of typed data
 *  
 *  Parameters:
 *    
 *    dataDomain:
 *      Domain of the data type
 *    
 *    dataType:
 *      Type of data being flipped
 *    
 *    id:
 *      resource id (if being flipped on behalf of the resource
 *      manager, otherwise will be zero)
 *    
 *    dataPtr:
 *      Pointer to the data
 *    
 *    dataSize:
 *      Length of the data
 *    
 *    currentlyNative:
 *      Boolean indicating which direction to flip: false means flip
 *      from disk big endian to native (from disk), true means flip
 *      from native to disk big endian (to disk)
 *    
 *    refcon:
 *      An optional user reference supplied when the flipper is
 *      installed
 *  
 *  Result:
 *    Error code indicating whether the data was flipped.  noErr would
 *    indicate that the data was flipped as appropriate; any other
 *    error will be propagated back to the caller.
 }
type
	CoreEndianFlipProc = function( dataDomain: OSType; dataType: OSType; id: SInt16; dataPtr: univ Ptr; dataSize: ByteCount; currentlyNative: Boolean; refcon: univ Ptr ): OSStatus;
{
 * Install a flipper for this application
 }
{
 *  CoreEndianInstallFlipper()
 *  
 *  Summary:
 *    Installs a flipper proc for the given data type.  If the flipper
 *    is already registered, this flipper will take replace it.
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    dataDomain:
 *      Domain of the data type
 *    
 *    dataType:
 *      Type of data for which this flipper should be installed
 *    
 *    proc:
 *      Flipper callback to be called for data of this type
 *    
 *    refcon:
 *      Optional user reference for the flipper
 *  
 *  Result:
 *    Error code indicating whether or not the flipper could be
 *    installed
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CoreEndianInstallFlipper( dataDomain: OSType; dataType: OSType; proc: CoreEndianFlipProc; refcon: univ Ptr { can be NULL } ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_3, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  CoreEndianGetFlipper()
 *  
 *  Summary:
 *    Gets an existing data flipper proc for the given data type
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    dataDomain:
 *      Domain of the data type
 *    
 *    dataType:
 *      Type of the data for which this flipper should be installed
 *    
 *    proc:
 *      Pointer to a flipper callback
 *    
 *    refcon:
 *      Pointer to the callback refcon
 *  
 *  Result:
 *    noErr if the given flipper could be found; otherwise
 *    handlerNotFoundErr will be returned.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CoreEndianGetFlipper( dataDomain: OSType; dataType: OSType; var proc: CoreEndianFlipProc; refcon: UnivPtrPtr ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_3, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);


{
 *  CoreEndianFlipData()
 *  
 *  Summary:
 *    Calls the flipper for the given data type with the associated data
 *  
 *  Mac OS X threading:
 *    Thread safe since version 10.3
 *  
 *  Parameters:
 *    
 *    dataDomain:
 *      Domain of the data type
 *    
 *    dataType:
 *      type of the data
 *    
 *    id:
 *      resource id (if not a resource, pass zero)
 *    
 *    data:
 *      a pointer to the data to be flipped (in place)
 *    
 *    dataLen:
 *      length of the data to flip
 *    
 *    currentlyNative:
 *      a boolean indicating the direction to flip (whether the data is
 *      currently native endian or big-endian)
 *  
 *  Result:
 *    Error code indicating whether the data was flipped.  If
 *    handlerNotFound is returned, then no flipping took place (which
 *    is not necessarily an error condtion)
 *  
 *  Availability:
 *    Mac OS X:         in version 10.3 and later in CoreServices.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function CoreEndianFlipData( dataDomain: OSType; dataType: OSType; id: SInt16; data: univ Ptr; dataLen: ByteCount; currentlyNative: Boolean ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_3, __MAC_10_8, __IPHONE_NA, __IPHONE_NA);

{$endc} {TARGET_API_MAC_OSX}

implementation

{$R-}
{MW-ONLY-START}
{$pragmac warn_undefroutine off}
{MW-ONLY-FINISH}

function Endian16_Swap( arg: UInt16 ): UInt16; inline;
begin
	Endian16_Swap := (( arg shl 8) and $0FF00) or (( arg shr 8) and $00FF);
end;

function Endian32_Swap( arg: UInt32 ): UInt32; inline;
begin
    Endian32_Swap := ((arg and $FF) shl 24) or ((arg and $0FF00) shl 8) or ((arg shr 8) and $0FF00) or ((arg shr 24) and $FF);
end;

{GPC-FPC-ONLY-START}
function Endian64_Swap_Pascal( arg: UInt64 ): UInt64; inline;
begin
	Endian64_Swap_Pascal := (Endian32_Swap( arg and $FFFFFFFF ) shl 32) or Endian32_Swap( (arg shr 32) and $FFFFFFFF );
end;
{GPC-FPC-ONLY-ELSE}
function Endian64_Swap_Pascal( arg: UInt64 ): UInt64; inline;
	var
		x: UInt64;
begin
	x.lo := Endian32_Swap( arg.hi );
	x.hi := Endian32_Swap( arg.lo );
	Endian64_Swap_Pascal := x;
end;
{GPC-FPC-ONLY-FINISH}

{FPC-ONLY-START}
function Endian64_Swap( arg: UInt64 ): UInt64; inline;
begin
	Endian64_Swap := Endian64_Swap_Pascal(arg);
end;
{FPC-ONLY-FINISH}

function EndianS16_Swap( arg: SInt16 ): SInt16; inline;
begin
	EndianS16_Swap := (( arg shl 8) and $0FF00) or (( arg shr 8) and $00FF);
end;

function EndianS32_Swap( arg: SInt32 ): SInt32; inline;
begin
    EndianS32_Swap := ((arg and $FF) shl 24) or ((arg and $0FF00) shl 8) or ((arg shr 8) and $0FF00) or ((arg shr 24) and $FF);
end;

{GPC-FPC-ONLY-START}
function EndianS64_Swap( arg: SInt64 ): SInt64; inline;
begin
	EndianS64_Swap := (SInt64( Endian32_Swap( arg and $FFFFFFFF ) ) shl 32) or Endian32_Swap( (arg shr 32) and $FFFFFFFF );
end;
{GPC-FPC-ONLY-ELSE}
function EndianS64_Swap( arg: SInt64 ): SInt64; inline;
	var
		x: SInt64;
begin
	x.lo := Endian32_Swap( arg.hi );
	x.hi := Endian32_Swap( arg.lo );
	EndianS64_Swap := x;
end;
{GPC-FPC-ONLY-FINISH}

{FPC-ONLY-START}
{$ifc TARGET_RT_BIG_ENDIAN}
function EndianS16_BtoN( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_BtoN := arg;
end;

function EndianS16_NtoB( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_NtoB := arg;
end;

function EndianU16_BtoN( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_BtoN := arg;
end;

function EndianU16_NtoB( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_NtoB := arg;
end;

function EndianS32_BtoN( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_BtoN := arg;
end;

function EndianS32_NtoB( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_NtoB := arg;
end;

function EndianU32_BtoN( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_BtoN := arg;
end;

function EndianU32_NtoB( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_NtoB := arg;
end;

function EndianS64_BtoN( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_BtoN := arg;
end;

function EndianS64_NtoB( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_NtoB := arg;
end;

function EndianU64_BtoN( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_BtoN := arg;
end;

function EndianU64_NtoB( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_NtoB := arg;
end;

function EndianS16_LtoN( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_LtoN := EndianS16_Swap(arg);
end;

function EndianS16_NtoL( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_NtoL := EndianS16_Swap(arg);
end;

function EndianU16_LtoN( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_LtoN := Endian16_Swap(arg);
end;

function EndianU16_NtoL( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_NtoL := Endian16_Swap(arg);
end;

function EndianS32_LtoN( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_LtoN := EndianS32_Swap(arg);
end;

function EndianS32_NtoL( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_NtoL := EndianS32_Swap(arg);
end;

function EndianU32_LtoN( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_LtoN := Endian32_Swap(arg);
end;

function EndianU32_NtoL( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_NtoL := Endian32_Swap(arg);
end;


function EndianS64_LtoN( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_LtoN := EndianS64_Swap(arg);
end;

function EndianS64_NtoL( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_NtoL := EndianS64_Swap(arg);
end;

function EndianU64_LtoN( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_LtoN := Endian64_Swap(arg);
end;

function EndianU64_NtoL( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_NtoL := Endian64_Swap(arg);
end;

{$elsec}
function EndianS16_BtoN( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_BtoN := EndianS16_Swap(arg);
end;

function EndianS16_NtoB( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_NtoB := EndianS16_Swap(arg);
end;

function EndianU16_BtoN( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_BtoN := Endian16_Swap(arg);
end;

function EndianU16_NtoB( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_NtoB := Endian16_Swap(arg);
end;

function EndianS32_BtoN( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_BtoN := EndianS32_Swap(arg);
end;

function EndianS32_NtoB( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_NtoB := EndianS32_Swap(arg);
end;

function EndianU32_BtoN( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_BtoN := Endian32_Swap(arg);
end;

function EndianU32_NtoB( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_NtoB := Endian32_Swap(arg);
end;


function EndianS64_BtoN( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_BtoN := EndianS64_Swap(arg);
end;

function EndianS64_NtoB( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_NtoB := EndianS64_Swap(arg);
end;

function EndianU64_BtoN( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_BtoN := Endian64_Swap(arg);
end;

function EndianU64_NtoB( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_NtoB := Endian64_Swap(arg);
end;

function EndianS16_LtoN( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_LtoN := arg;
end;

function EndianS16_NtoL( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_NtoL := arg;
end;

function EndianU16_LtoN( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_LtoN := arg;
end;

function EndianU16_NtoL( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_NtoL := arg;
end;

function EndianS32_LtoN( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_LtoN := arg;
end;

function EndianS32_NtoL( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_NtoL := arg;
end;

function EndianU32_LtoN( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_LtoN := arg;
end;

function EndianU32_NtoL( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_NtoL := arg;
end;

function EndianS64_LtoN( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_LtoN := arg;
end;

function EndianS64_NtoL( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_NtoL := arg;
end;

function EndianU64_LtoN( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_LtoN := arg;
end;

function EndianU64_NtoL( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_NtoL := arg;
end;

{$endc}

function EndianS16_LtoB( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_LtoB:=EndianS16_Swap(arg);
end;

function EndianS16_BtoL( arg: SInt16 ): SInt16; inline;
begin
  EndianS16_BtoL:=EndianS16_Swap(arg);
end;

function EndianU16_LtoB( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_LtoB:=Endian16_Swap(arg);
end;

function EndianU16_BtoL( arg: UInt16 ): UInt16; inline;
begin
  EndianU16_BtoL:=Endian16_Swap(arg);
end;

function EndianS32_LtoB( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_LtoB:=EndianS32_Swap(arg);
end;

function EndianS32_BtoL( arg: SInt32 ): SInt32; inline;
begin
  EndianS32_BtoL:=EndianS32_Swap(arg);
end;

function EndianU32_LtoB( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_LtoB:=Endian32_Swap(arg);
end;

function EndianU32_BtoL( arg: UInt32 ): UInt32; inline;
begin
  EndianU32_BtoL:=Endian32_Swap(arg);
end;

function EndianS64_LtoB( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_LtoB:=EndianS64_Swap(arg);
end;

function EndianS64_BtoL( arg: SInt64 ): SInt64; inline;
begin
  EndianS64_BtoL:=EndianS64_Swap(arg);
end;

function EndianU64_LtoB( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_LtoB:=Endian64_Swap_Pascal(arg);
end;

function EndianU64_BtoL( arg: UInt64 ): UInt64; inline;
begin
  EndianU64_BtoL:=Endian64_Swap_Pascal(arg);
end;


{FPC-ONLY-FINISH}

end.
