{
     File:       QuickTime/HIMovieView.h
 
     Contains:   HIView-based movie playback
 
     Version:    QuickTime 7.7.1
 
     Copyright:  � 2004-2012 by Apple Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HIMovieView;
interface
uses MacTypes,HIGeometry,HIObject,HIView,Movies;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{ QuickTime is not available to 64-bit clients }

{$ifc not TARGET_CPU_64}


{
 *  kHIMovieViewClassID
 *  
 *  Summary:
 *    Class ID for HIMovieView
 }
const kHIMovieViewClassID = CFSTR( 'com.apple.quicktime.HIMovieView' );

{
 *  Summary:
 *    HIMovieView Event class
 }
const
{
   * Events related to movie views.
   }
	kEventClassMovieView = FOUR_CHAR_CODE('moov');


{
 *  kEventClassMovieView / kEventMovieViewOptimalBoundsChanged
 *  
 *  Summary:
 *    Sent when the movie size changes.
 *  
 *  Parameters:
 *    
 *    --> kEventParamDirectObject (in, typeControlRef)
 *          The movie view whose size is changing.
 *    
 *    --> kEventParamControlOptimalBounds (in, typeHIRect)
 *          The new optimal bounds.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 }
const
	kEventMovieViewOptimalBoundsChanged = 1;


{
 *  Summary:
 *    HIMovieView attributes
 }
const
{
   * No attributes
   }
	kHIMovieViewNoAttributes = 0;

  {
   * Movie controller bar is visible below visual content
   }
	kHIMovieViewControllerVisibleAttribute = 1 shl 0;

  {
   * Automatically call MCIdle() at appropriate times
   }
	kHIMovieViewAutoIdlingAttribute = 1 shl 1;

  {
   * Accepts keyboard focus
   }
	kHIMovieViewAcceptsFocusAttribute = 1 shl 2;

  {
   * Movie editing enabled
   }
	kHIMovieViewEditableAttribute = 1 shl 3;

  {
   * Handles editing HI commands such as cut, copy and paste
   }
	kHIMovieViewHandleEditingHIAttribute = 1 shl 4;

  {
   * Combination of kHIMovieViewControllerVisibleAttribute,
   * kHIMovieViewAutoIdlingAttribute, and
   * kHIMovieViewAcceptsFocusAttribute
   }
	kHIMovieViewStandardAttributes = kHIMovieViewControllerVisibleAttribute or kHIMovieViewAutoIdlingAttribute or kHIMovieViewAcceptsFocusAttribute;

{
 *  HIMovieViewCreate()
 *  
 *  Summary:
 *    Creates an HIMovieView object
 *  
 *  Discussion:
 *    If successful, the created view will have a single retain count.
 *  
 *  Parameters:
 *    
 *    inMovie:
 *      [in]  Initial movie to view, may be NULL
 *    
 *    inAttributes:
 *      [in]  Initial HIMovieView attributes
 *    
 *    outMovieView:
 *      [out] Points to variable to receive new HIMovieView
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function HIMovieViewCreate( inMovie: Movie; inAttributes: OptionBits; var outMovieView: HIViewRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  HIMovieViewGetMovie()
 *  
 *  Summary:
 *    Returns the view's current movie.
 *  
 *  Parameters:
 *    
 *    inView:
 *      [in]  The HIMovieView
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function HIMovieViewGetMovie( inView: HIViewRef ): Movie;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  HIMovieViewSetMovie()
 *  
 *  Summary:
 *    Sets the view's current movie.
 *  
 *  Parameters:
 *    
 *    inView:
 *      [in]  The HIMovieView
 *    
 *    inMovie:
 *      [in]  The new movie to display
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function HIMovieViewSetMovie( inView: HIViewRef; inMovie: Movie ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  HIMovieViewGetAttributes()
 *  
 *  Summary:
 *    Returns the view's current attributes.
 *  
 *  Parameters:
 *    
 *    inView:
 *      [in]  The HIMovieView
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function HIMovieViewGetAttributes( inView: HIViewRef ): OptionBits;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  HIMovieViewChangeAttributes()
 *  
 *  Summary:
 *    Changes the views attributes.
 *  
 *  Discussion:
 *    Setting an attribute takes precedence over clearing the attribute.
 *  
 *  Parameters:
 *    
 *    inView:
 *      [in]  The HIMovieView
 *    
 *    inAttributesToSet:
 *      [in]  Attributes to set
 *    
 *    inAttributesToClear:
 *      [in]  Attributes to clear
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function HIMovieViewChangeAttributes( inView: HIViewRef; inAttributesToSet: OptionBits; inAttributesToClear: OptionBits ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  HIMovieViewGetMovieController()
 *  
 *  Summary:
 *    Returns the view's current movie controller.
 *  
 *  Parameters:
 *    
 *    inView:
 *      [in]  The HIMovieView
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function HIMovieViewGetMovieController( inView: HIViewRef ): MovieController;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  HIMovieViewGetControllerBarSize()
 *  
 *  Summary:
 *    Returns the size of the visible movie controller bar.
 *  
 *  Parameters:
 *    
 *    inView:
 *      [in]  The HIMovieView
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function HIMovieViewGetControllerBarSize( inView: HIViewRef ): HISize;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  HIMovieViewPlay()
 *  
 *  Summary:
 *    Convenience routine to play the view's current movie.
 *  
 *  Discussion:
 *    If the movie is already playing, this function does nothing.
 *  
 *  Parameters:
 *    
 *    movieView:
 *      [in]  The movie view.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function HIMovieViewPlay( movieView: HIViewRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{
 *  HIMovieViewPause()
 *  
 *  Summary:
 *    Convenience routine to pause the view's current movie.
 *  
 *  Discussion:
 *    If the movie is already paused, this function does nothing.
 *  
 *  Parameters:
 *    
 *    movieView:
 *      [in]  The movie view.
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 (or QuickTime 7.0) and later in QuickTime.framework
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   not available
 }
function HIMovieViewPause( movieView: HIViewRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;


{$endc} {TARGET_CPU_64}

{$endc} {TARGET_OS_MAC}


end.
