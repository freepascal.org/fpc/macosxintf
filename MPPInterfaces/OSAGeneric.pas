{
     File:       OpenScripting/OSAGeneric.h
 
     Contains:   AppleScript Generic Component Interfaces.
 
     Version:    OSA-148~28
 
     Copyright:  � 1992-2008 by Apple Computer, Inc., all rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}

{  Pascal Translation Updated: Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit OSAGeneric;
interface
uses MacTypes,AEDataModel,Components,MacErrors,AppleEvents,OSA;
{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{    NOTE:   This interface defines a "generic scripting component."
            The Generic Scripting Component allows automatic dispatch to a
            specific scripting component that conforms to the OSA interface.
            This component supports OSA, by calling AppleScript or some other 
            scripting component.  Additionally it provides access to the default
            and the user-prefered scripting component.
}


const
{ Component version this header file describes }
	kGenericComponentVersion = $0100;

const
	kGSSSelectGetDefaultScriptingComponent = $1001;
	kGSSSelectSetDefaultScriptingComponent = $1002;
	kGSSSelectGetScriptingComponent = $1003;
	kGSSSelectGetScriptingComponentFromStored = $1004;
	kGSSSelectGenericToRealID = $1005;
	kGSSSelectRealToGenericID = $1006;
	kGSSSelectOutOfRange = $1007;

type
	ScriptingComponentSelector = OSType;
	GenericID = OSAID;
{ get and set the default scripting component }
{
 *  OSAGetDefaultScriptingComponent()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetDefaultScriptingComponent( genericScriptingComponent: ComponentInstance; var scriptingSubType: ScriptingComponentSelector ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{
 *  OSASetDefaultScriptingComponent()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSASetDefaultScriptingComponent( genericScriptingComponent: ComponentInstance; scriptingSubType: ScriptingComponentSelector ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ get a scripting component instance from its subtype code }
{
 *  OSAGetScriptingComponent()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetScriptingComponent( genericScriptingComponent: ComponentInstance; scriptingSubType: ScriptingComponentSelector; var scriptingInstance: ComponentInstance ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ get a scripting component selector (subType) from a stored script }
{
 *  OSAGetScriptingComponentFromStored()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGetScriptingComponentFromStored( genericScriptingComponent: ComponentInstance; const var scriptData: AEDesc; var scriptingSubType: ScriptingComponentSelector ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ get a real component instance and script id from a generic id }
{
 *  OSAGenericToRealID()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSAGenericToRealID( genericScriptingComponent: ComponentInstance; var theScriptID: OSAID; var theExactComponent: ComponentInstance ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ get a generic id from a real component instance and script id }
{
 *  OSARealToGenericID()
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework
 *    CarbonLib:        in CarbonLib 1.0 and later
 *    Non-Carbon CFM:   in AppleScriptLib 1.1 and later
 }
function OSARealToGenericID( genericScriptingComponent: ComponentInstance; var theScriptID: OSAID; theExactComponent: ComponentInstance ): OSAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;

{$endc} {TARGET_OS_MAC}

end.
