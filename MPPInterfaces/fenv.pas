{
 * Copyright (c) 2002 Apple Computer, Inc. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 * 
 * The contents of this file constitute Original Code as defined in and
 * are subject to the Apple Public Source License Version 1.1 (the
 * "License").  You may not use this file except in compliance with the
 * License.  Please obtain a copy of the License at
 * http://www.apple.com/publicsource and read it before using this file.
 * 
 * This Original Code and all software distributed under the License are
 * distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT.  Please see the
 * License for the specific language governing rights and limitations
 * under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 }
{   Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, November 2005 }
unit fenv;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

 
{******************************************************************************
*                                                                              *
*     File:  fenv.h	                                                       *
*                                                                              *
*     Contains: typedefs and prototypes for C99 floating point environment.    *
*                                                                              *
******************************************************************************}


{
    A collection of functions designed to provide access to the floating
    point environment for numerical programming. It is modeled after
    the floating-point requirements in C9X.
    
    The file <fenv.h> declares many functions in support of numerical
    programming. Programs that test flags or run under
    non-default modes must do so under the effect of an enabling
    "fenv_access" pragma.
}

{*******************************************************************************
*                                                                               *
*    fenv_t         is a type for representing the entire floating-point        *
*                   environment in a single object.                             *
*                                                                               *
*    fexcept_t      is a type for representing the floating-point               *
*                   exception flag state collectively.                          *
*                                                                               *
*******************************************************************************}
{$ifc TARGET_CPU_PPC}
type
	fenv_t = UInt32;
	fexcept_t = UInt32;
{$elsec}
type
	fenv_t = record
    __control: UInt16;
    __status: UInt16;
	end;
	fexcept_t = UInt16;
{$endc}

{    Definitions of floating-point exception macros                          }
{$ifc TARGET_CPU_PPC}
const
	_FE_INEXACT = $02000000;
	_FE_DIVBYZERO = $04000000;
	_FE_UNDERFLOW = $08000000;
	_FE_OVERFLOW = $10000000;
	_FE_INVALID = $20000000;
	_FE_ALL_EXCEPT = $3E000000; // FE_INEXACT | FE_DIVBYZERO | FE_UNDERFLOW | FE_OVERFLOW | FE_INVALID
{$elsec}
const
  _FE_INEXACT = $0020;
  _FE_DIVBYZERO = $0004;
  _FE_UNDERFLOW = $0010;
  _FE_OVERFLOW = $0008;
  _FE_INVALID = $0001;
  _FE_ALL_EXCEPT = $003D; // FE_INEXACT | FE_DIVBYZERO | FE_UNDERFLOW | FE_OVERFLOW | FE_INVALID
{$endc}
const
	FE_INEXACT = _FE_INEXACT;
	FE_DIVBYZERO = _FE_DIVBYZERO;
	FE_UNDERFLOW = _FE_UNDERFLOW;
	FE_OVERFLOW = _FE_OVERFLOW;
	FE_INVALID = _FE_INVALID;
	FE_ALL_EXCEPT = _FE_ALL_EXCEPT;

{    Definitions of rounding direction macros                                }
{$ifc TARGET_CPU_PPC}
const
	_FE_TONEAREST = $00000000;
	_FE_TOWARDZERO = $00000001;
	_FE_UPWARD = $00000002;
	_FE_DOWNWARD = $00000003;
{$elsec}
const
  _FE_TONEAREST = $0000;
  _FE_TOWARDZERO = $0C00;
  _FE_UPWARD = $0800;
  _FE_DOWNWARD = $0400;
{$endc}
const
	FE_TONEAREST = _FE_TONEAREST;
	FE_TOWARDZERO = _FE_TOWARDZERO;
	FE_UPWARD = _FE_UPWARD;
	FE_DOWNWARD = _FE_DOWNWARD;

{ default environment object        }
const _FE_DFL_ENV: fenv_t;
// #define FE_DFL_ENV &_FE_DFL_ENV          { pointer to default environment    }

{******************************************************************************
*     The following functions provide access to the exception flags.  The      *
*     "int" input argument can be constructed by bitwise ORs of the exception  *
*     macros: for example: FE_OVERFLOW | FE_INEXACT.                           *
******************************************************************************}

{******************************************************************************
*     The function "feclearexcept" clears the supported exceptions represented *
*     by its argument.                                                         *
******************************************************************************}

function feclearexcept( excepts: SInt32 ): SInt32;


{******************************************************************************
*    The function "fegetexceptflag" stores a representation of the exception   *
*     flags indicated by its integer argument through the fexcept_t pointer    *
*     argument.                                                                *
******************************************************************************}

function fegetexceptflag( var flagp: fexcept_t; excepts: SInt32 ): SInt32;


{******************************************************************************
*     The function "feraiseexcept" raises the supported exceptions             *
*     represented by its argument.                                             *
******************************************************************************}

function feraiseexcept( excepts: SInt32 ): SInt32;


{******************************************************************************
*     The function "fesetexceptflag" sets or clears the exception flags indicated  *
*     by the its integer argument according to the representation in the       *
*     object pointed to by the fexcept_t pointer argument.  The value of the   *
*     object must have been set by a previous call to "fegetexceptflag".       *
*     This function does not raise exceptions; it just sets the state of       *
*     the flags.                                                               *
******************************************************************************}

function fesetexceptflag( const var flagp: fexcept_t; excepts: SInt32 ): SInt32;


{******************************************************************************
*     The function "fetestexcept" determines which of the specified subset of  *
*     the exception flags are currently set.  The integer argument specifies   *
*     the exception flags to be queried as a bitwise OR of the exception       *
*     macros.  This function returns the bitwise OR of the exception macros    *
*     corresponding to the currently set exceptions included in "excepts".     *
******************************************************************************}

function fetestexcept( excepts: SInt32 ): SInt32;


{******************************************************************************
*     The following functions provide control of rounding direction modes.     *
******************************************************************************}

{******************************************************************************
*     The function "fegetround" returns the value of the rounding direction    *
*     macro which represents the current rounding direction.                   *
******************************************************************************}

function fegetround: SInt32;


{******************************************************************************
*     The function "fesetround" establishes the rounding direction represented *
*     by its argument.  It returns zero if and only if the argument matches    *
*     a rounding direction macro.  If not, the rounding direction is not       *
*     changed.                                                                 *
******************************************************************************}

function fesetround( round: SInt32 ): SInt32;


{******************************************************************************
*    The following functions manage the floating-point environment, exception  *
*    flags and dynamic modes, as one entity.                                   *
******************************************************************************}

function fegetenv( var envp: fenv_t ): SInt32;
function feholdexcept( var envp: fenv_t ): SInt32;
function fesetenv( const var envp: fenv_t ): SInt32;
function feupdateenv( const var envp: fenv_t ): SInt32;

{$endc} {TARGET_OS_MAC}

end.
