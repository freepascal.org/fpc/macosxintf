{
 *  CFFileSecurity.h
 *  NSFileSecurity and CFFileSecurity are toll-free bridged.
 *
 *  Copyright (c) 2010-2013, Apple Inc. All rights reserved.
 }
unit CFFileSecurity;
interface
uses MacTypes,MacOSXPosix,CFBase,acl,CFUUID;
{$ALIGN POWER}


{ #if (TARGET_OS_MAC || TARGET_OS_EMBEDDED || TARGET_OS_IPHONE) || CF_BUILDING_CF || NSBUILDINGFOUNDATION }


{
 *	A CFFileSecurity object encapulates a file system object's security information
 *	in a CF object.
 }
type
	CFFileSecurityRef = ^__CFFileSecurity; { an opaque type }
	__CFFileSecurity = record end;

{
 *	Returns the type identifier for the CFFileSecurity opaque type.
 *
 *	Return Value
 *		The type identifier for the CFFileSecurity opaque type.
 }
function CFFileSecurityGetTypeID: CFTypeID;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	Creates an CFFileSecurity object.
 *
 *	Parameters
 *		allocator
 *			The allocator to use to allocate memory for the new object. Pass
 *			NULL or kCFAllocatorDefault to use the current default allocator.
 *	Return Value
 *		A new CFFileSecurity object, or NULL if there was a problem creating the
 *		object. Ownership follows the Create Rule.
 }
function CFFileSecurityCreate( allocator: CFAllocatorRef ): CFFileSecurityRef;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *  Creates a copy of a CFFileSecurity object.
 *
 *  Parameters
 *		allocator
 *			The allocator to use to allocate memory for the new object. Pass
 *			NULL or kCFAllocatorDefault to use the current default allocator.
 *		fileSec
 *			The CFFileSecurity object to copy.
 *	Return Value
 *		A copy of fileSec, or NULL if there was a problem creating the object.
 *		Ownership follows the Create Rule.
 }
function CFFileSecurityCreateCopy( allocator: CFAllocatorRef; fileSec: CFFileSecurityRef ): CFFileSecurityRef;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	This routine copies the owner UUID associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		ownerUUID
 *			A pointer to storage for the owner UUID.
 *	Return Value
 *		true if ownerUUID is successfully returned; false if there is no owner
 *		UUID property associated with an CFFileSecurity object.
 }
function CFFileSecurityCopyOwnerUUID( fileSec: CFFileSecurityRef; var ownerUUID: CFUUIDRef ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);

{
 *	This routine sets the owner UUID associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		ownerUUID
 *			The owner UUID.
 *	Return Value
 *		true if the owner UUID was successfully set; otherwise, false.
 }
function CFFileSecuritySetOwnerUUID( fileSec: CFFileSecurityRef; ownerUUID: CFUUIDRef ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);

{
 *	This routine copies the group UUID associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		groupUUID
 *			A pointer to storage for the group UUID.
 *	Return Value
 *		true if groupUUID is successfully returned; false if there is no group
 *		UUID property associated with an CFFileSecurity object.
 }
function CFFileSecurityCopyGroupUUID( fileSec: CFFileSecurityRef; var groupUUID: CFUUIDRef ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	This routine sets the group UUID associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		groupUUID
 *			The group UUID.
 *	Return Value
 *		true if the group UUID was successfully set; otherwise, false.
 }
function CFFileSecuritySetGroupUUID( fileSec: CFFileSecurityRef; groupUUID: CFUUIDRef ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	This routine copies the access control list (acl_t) associated with an
 *	CFFileSecurity object. The acl_t returned by this routine is a copy and must
 *	be released using acl_free(3). The acl_t is meant to be manipulated using
 *	the acl calls defined in <sys/acl.h>.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		accessControlList
 *			A pointer to storage for an acl_t. The acl_t be released using
 *			acl_free(3)
 *	Return Value
 *		true if the access control list is successfully copied; false if there is
 *		no access control list property associated with the CFFileSecurity object.
 }
function CFFileSecurityCopyAccessControlList( fileSec: CFFileSecurityRef; var accessControlList: acl_t ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);

const kCFFileSecurityRemoveACL = acl_t(1);

{
 *	This routine will set the access control list (acl_t) associated with an
 *	CFFileSecurityRef. To request removal of an access control list from a
 *	filesystem object, pass in kCFFileSecurityRemoveACL as the acl_t and set
 *	the fileSec on the target object using CFURLSetResourcePropertyForKey and
 *	the kCFURLFileSecurityKey. Setting the accessControlList to NULL will result
 *	in the property being unset.
 *
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		accessControlList
 *			The acl_t to set, or kCFFileSecurityRemoveACL to remove the access
 *			control list, or NULL to unset the accessControlList.
 *	Return Value
 *		true if the access control list is successfully set; otherwise, false.
 }
function CFFileSecuritySetAccessControlList( fileSec: CFFileSecurityRef; accessControlList: acl_t ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	This routine gets the owner uid_t associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		owner
 *			A pointer to where the owner uid_t will be put.
 *	Return Value
 *		true if owner uid_t is successfully obtained; false if there is no owner
 *		property associated with an CFFileSecurity object.
 }
function CFFileSecurityGetOwner( fileSec: CFFileSecurityRef; var owner: uid_t ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	This routine sets the owner uid_t associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		owner
 *			The owner uid_t.
 *	Return Value
 *		true if the owner uid_t was successfully set; otherwise, false.
 }
function CFFileSecuritySetOwner( fileSec: CFFileSecurityRef; owner: uid_t ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	This routine gets the group gid_t associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		owner
 *			A pointer to where the group gid_t will be put.
 *	Return Value
 *		true if group gid_t is successfully obtained; false if there is no group
 *		property associated with an CFFileSecurity object.
 }
function CFFileSecurityGetGroup( fileSec: CFFileSecurityRef; var group: gid_t ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	This routine sets the group gid_t associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		owner
 *			The group gid_t.
 *	Return Value
 *		true if the group gid_t was successfully set; otherwise, false.
 }
function CFFileSecuritySetGroup( fileSec: CFFileSecurityRef; group: gid_t ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	This routine gets the mode_t associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		owner
 *			A pointer to where the mode_t will be put.
 *	Return Value
 *		true if mode_t is successfully obtained; false if there is no mode
 *		property associated with an CFFileSecurity object.
 }
function CFFileSecurityGetMode( fileSec: CFFileSecurityRef; var mode: mode_t ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);


{
 *	This routine sets the mode_t associated with an CFFileSecurity object.
 *  
 *	Parameters
 *		fileSec
 *			The CFFileSecurity object.
 *		owner
 *			The mode_t.
 *	Return Value
 *		true if the mode_t was successfully set; otherwise, false.
 }
function CFFileSecuritySetMode( fileSec: CFFileSecurityRef; mode: mode_t ): Boolean;
CF_AVAILABLE_STARTING(10_7, 5_0);


{ values to pass in the clearPropertyMask to CFFileSecurityClearProperties }
type
  CFFileSecurityClearOptions = CFOptionFlags;
const
    kCFFileSecurityClearOwner               = UNSIGNEDLONG(1) shl 0;
    kCFFileSecurityClearGroup               = UNSIGNEDLONG(1) shl 1;
    kCFFileSecurityClearMode                = UNSIGNEDLONG(1) shl 2;
    kCFFileSecurityClearOwnerUUID           = UNSIGNEDLONG(1) shl 3;
    kCFFileSecurityClearGroupUUID           = UNSIGNEDLONG(1) shl 4;
    kCFFileSecurityClearAccessControlList   = UNSIGNEDLONG(1) shl 5;

{
 *	This routine clears file security properties in the CFFileSecurity object.
 *  
 *	Parameters
 *		clearPropertyMask
 *			The file security properties to clear.
 *	Return Value
 *		true if the file security properties were successfully cleared; otherwise, false.
 }
function CFFileSecurityClearProperties( fileSec: CFFileSecurityRef; clearPropertyMask: CFFileSecurityClearOptions ): Boolean;
CF_AVAILABLE_STARTING(10_8, 6_0);



end.
