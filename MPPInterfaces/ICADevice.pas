{------------------------------------------------------------------------------------------------------------------------------
 *
 *  ImageCapture/ICADevice.h
 *
 *  Copyright (c) 2000-2006 Apple Computer, Inc. All rights reserved.
 *
 *  For bug reports, consult the following page onthe World Wide Web:
 *  http://developer.apple.com/bugreporter/
 *
 *----------------------------------------------------------------------------------------------------------------------------}
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit ICADevice;
interface
uses MacTypes,ICAApplication;

{$ifc TARGET_OS_MAC}

{$ALIGN MAC68K}

//------------------------------------------------------------------------------------------------------------------------------
{!
    @header ICADevice.h
    @discussion
        ICADevice.h defines structures and functions that are used by native Image Capture device modules. 
}

//-------------------------------------------------------------------------------------------------------------------- ICDHeader
{!
    @struct ICDHeader
    @discussion
        This is the first field in all parameter blocks used by APIs defined in ICADevices.h.
        Type of parameter passed to a callback function used by APIs defined in ICADevices.h.
        The parameter for the completion proc should to be casted to an appropriate type such as ICD_NewObjectPB* for it to be useful.
    @field err
        Error returned by an API. -->
    @field refcon
        An arbitrary refcon value passed to the callback. <--
}
type
	ICDHeaderPtr = ^ICDHeader;
	ICDHeader = record
		err: ICAError;
		refcon: UNSIGNEDLONG;
	end;

//--------------------------------------------------------------------------------------------------------------- Callback procs

{!
    @typedef ICDCompletion
    @discussion
        Type of callback function used by APIs defined in ICADevices.h.
    @param pb
        The parameter pb is a pointer to the parameter block passed to the API.
}

type
	ICDCompletion = procedure( var pb: ICDHeader );

//----------------------------------------------------------------------------------------------------------------- ICDNewObject
{!
    @struct ICD_NewObjectPB
    @discussion
        Parameter block passed to function <code>ICDNewObject</code>.
    @field header
        The function returns error code in the <code>err</code> field of this structure. 
        The <code>refcon</code> field of this structure is used to pass a pointer to the callback function if <code>ICDNewObject</code> is called asynchronously.
    @field  parentObject
        Parent object of the new object.
    @field  objectInfo
        <code>ICAObjectInfo</code> struct filled with information about the new object.
    @field  object
        New object.
}
type
	ICD_NewObjectPBPtr = ^ICD_NewObjectPB;
	ICD_NewObjectPB = record
		header: ICDHeader;
		parentObject: ICAObject;
		objectInfo: ICAObjectInfo;
		objct: ICAObject;
	end;

{!
    @function ICDNewObject
    @abstract
        A function to create a new object.
    @discussion
        Call this function to create a new object.
    @param pb
        An <code>ICD_NewObjectPB</code> structure.
    @param completion
        A pointer to a callback function that conforms to the interface of <code>ICDCompletion</code>. Pass <code>NULL</code> to make a synchronous call. 
    @result
        Returns an error code. If the function is called asynchronously, it returns <code>0</code> if the the call is accepted for asynchronous
        processing and returns an error code in the header passed to the callback function.
}
function ICDNewObject( var pb: ICD_NewObjectPB; completion: ICDCompletion ): ICAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_7;

//------------------------------------------------------------------------------------------------------------- ICDDisposeObject
{!
    @struct ICD_DisposeObjectPB
    @discussion
        Parameter block passed to function <code>ICDDisposeObject</code>.
    @field header
        The function returns error code in the <code>err</code> field of this structure. 
        The <code>refcon</code> field of this structure is used to pass a pointer to the callback function if <code>ICDDisposeObject</code> is called asynchronously.
    @field object
        Object to be disposed.
}
type
	ICD_DisposeObjectPBPtr = ^ICD_DisposeObjectPB;
	ICD_DisposeObjectPB = record
		header: ICDHeader;
		objct: ICAObject;
	end;

{!
    @function ICDDisposeObject
    @abstract
        A function to dispose an object.
    @discussion
        Call this function to dispose an object.
    @param pb
        An <code>ICD_DisposeObjectPB</code> structure.
    @param completion
        A pointer to a callback function that conforms to the interface of <code>ICDCompletion</code>. Pass <code>NULL</code> to make a synchronous call. 
    @result
        Returns an error code. If the function is called asynchronously, it returns <code>0</code> if the the call is accepted for asynchronous 
        processing and returns an error code in the header passed to the callback function.
}
function ICDDisposeObject( var pb: ICD_DisposeObjectPB; completion: ICDCompletion ): ICAError;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER_BUT_DEPRECATED_IN_MAC_OS_X_VERSION_10_7;

//------------------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------------------

{$endc} {TARGET_OS_MAC}

end.
