{
     File:       OpenScripting/DigitalHubRegistry.h
 
     Contains:   Digital Hub AppleEvents
 
     Version:    OSA-148~28
 
     Copyright:  � 2002-2008 by Apple Computer, Inc.  All rights reserved
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}

{  Pascal Translation: Gorazd Krosl <gorazd_1957@yahoo.ca>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit DigitalHubRegistry;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{ class }
const
	kDigiHubEventClass = FOUR_CHAR_CODE('dhub');


{ events}
const
	kDigiHubMusicCD = FOUR_CHAR_CODE('aucd'); { 1635083108 0x61756364}
	kDigiHubPictureCD = FOUR_CHAR_CODE('picd'); { 1885954916 0x70696364}
	kDigiHubVideoDVD = FOUR_CHAR_CODE('vdvd'); { 1986295396 0x76647664}
	kDigiHubBlankCD = FOUR_CHAR_CODE('bcd '); { 1650680864 0x62636420}
	kDigiHubBlankDVD = FOUR_CHAR_CODE('bdvd'); { 1650751076 0x62647664}

{
    Parameters for Digital Hub AppleEvents:
                    
        kDigiHubMusicCD
        Required parameters:
        -->     keyDirectObject         typeFSRef

        kDigiHubPictureCD
        Required parameters:
        -->     keyDirectObject         typeFSRef
                
        kDigiHubVideoDVD
        Required parameters:
        -->     keyDirectObject         typeFSRef
                
        kDigiHubBlankCD
        Required parameters:
        -->     keyDirectObject         typeUTF8Text

        kDigiHubBlankDVD
        Required parameters:
        -->     keyDirectObject         typeUTF8Text

}

{$endc} {TARGET_OS_MAC}

end.
