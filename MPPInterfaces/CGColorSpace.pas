{ CoreGraphics - CGColorSpace.h
   Copyright (c) 1999-2009 Apple Inc.
   All rights reserved. }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }

unit CGColorSpace;
interface
uses MacTypes,CFBase,CFData,CFString,CGBase,CGDataProvider;
{$ALIGN POWER}


type
	CGColorSpaceRef = ^OpaqueCGColorSpaceRef; { an opaque type }
	OpaqueCGColorSpaceRef = record end;

type
	TristimulusValue = array[0..2] of CGFloat;
	RedGreenBlueValue = array[0..2] of CGFloat;
	Single4 = array[0..3] of CGFloat;
	Single9 = array[0..8] of CGFloat;

{ Color rendering intents. }

type
	CGColorRenderingIntent = SInt32;
const
	kCGRenderingIntentDefault = 0;
	kCGRenderingIntentAbsoluteColorimetric = 1;
	kCGRenderingIntentRelativeColorimetric = 2;
	kCGRenderingIntentPerceptual = 3;
	kCGRenderingIntentSaturation = 4;

{ The model of a color space. }

type
	CGColorSpaceModel = SInt32;
const
	kCGColorSpaceModelUnknown = -1;
	kCGColorSpaceModelMonochrome = -1 + 1;
	kCGColorSpaceModelRGB = -1 + 2;
	kCGColorSpaceModelCMYK = -1 + 3;
	kCGColorSpaceModelLab = -1 + 4;
	kCGColorSpaceModelDeviceN = -1 + 5;
	kCGColorSpaceModelIndexed = -1 + 6;
	kCGColorSpaceModelPattern = -1 + 7;

{$ifc TARGET_OS_MAC}
{ The name of the "Generic" gray color space. }

const kCGColorSpaceGenericGray: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_NA);

{ The name of the "Generic" RGB color space. }

const kCGColorSpaceGenericRGB: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_NA);

{ The name of the "Generic" CMYK color space. }

const kCGColorSpaceGenericCMYK: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_NA);

{ The name of the "Generic" linear RGB color space. This is the same as
   `kCGColorSpaceGenericRGB' but with a 1.0 gamma. }

const kCGColorSpaceGenericRGBLinear: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ The name of the Adobe RGB (1998) color space. For more information, see
  "Adobe RGB (1998) Color Image Encoding", Version 2005-05, Adobe Systems
  Inc. (http://www.adobe.com). }

const kCGColorSpaceAdobeRGB1998: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ The name of the sRGB color space. The capitalization in the name, while
   strictly inaccurate, avoids interpretational ambiguity. For more
   information, see IEC 61966-2-1 (1999-10): "Multimedia systems and
   equipment - Colour measurement and management - Part 2-1: Colour
   management - Default RGB colour space - sRGB". }

const kCGColorSpaceSRGB: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);

{ The "Generic" gray color space with gamma = 2.2. }

const kCGColorSpaceGenericGrayGamma2_2: CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);
{$endc}

{* Device-dependent color spaces.  *}

{ Create a DeviceGray color space. }

function CGColorSpaceCreateDeviceGray: CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create a DeviceRGB color space. }

function CGColorSpaceCreateDeviceRGB: CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create a DeviceCMYK color space. }

function CGColorSpaceCreateDeviceCMYK: CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{* Device-independent color spaces. *}

{ Create a calibrated gray color space. `whitePoint' is an array of 3
   numbers specifying the tristimulus value, in the CIE 1931 XYZ-space, of
   the diffuse white point. `blackPoint' is an array of 3 numbers specifying
   the tristimulus value, in CIE 1931 XYZ-space, of the diffuse black point.
   `gamma' defines the gamma for the gray component. }

function CGColorSpaceCreateCalibratedGray( const var whitePoint: TristimulusValue; const var blackPoint: TristimulusValue; gamma: CGFloat ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create a calibrated RGB color space. `whitePoint' is an array of 3
   numbers specifying the tristimulus value, in the CIE 1931 XYZ-space, of
   the diffuse white point. `blackPoint' is an array of 3 numbers specifying
   the tristimulus value, in CIE 1931 XYZ-space, of the diffuse black point.
   `gamma' is an array of 3 numbers specifying the gamma for the red, green,
   and blue components of the color space. `matrix' is an array of 9 numbers
   specifying the linear interpretation of the gamma-modified RGB values of
   the color space with respect to the final XYZ representation. }

function CGColorSpaceCreateCalibratedRGB( const var whitePoint: TristimulusValue; const var blackPoint: TristimulusValue; const var gamma: RedGreenBlueValue; const var matrix: Single9 ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);
    
{ Create an L*a*b* color space. `whitePoint' is an array of 3 numbers
   specifying the tristimulus value, in the CIE 1931 XYZ-space, of the
   diffuse white point. `blackPoint' is an array of 3 numbers specifying the
   tristimulus value, in CIE 1931 XYZ-space, of the diffuse black point.
   `range' is an array of four numbers specifying the range of valid values
   for the a* and b* components of the color space. }

function CGColorSpaceCreateLab(const var whitePoint: TristimulusValue; const var blackPoint: TristimulusValue; const var range: Single4): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create an ICC-based color space using the ICC profile specified by
   `data'. }

function CGColorSpaceCreateWithICCProfile( data: CFDataRef ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Create an ICC-based color space. `nComponents' specifies the number of
   color components in the color space defined by the ICC profile data. This
   must match the number of components actually in the ICC profile, and must
   be 1, 3, or 4. `range' is an array of 2*nComponents numbers specifying
   the minimum and maximum valid values of the corresponding color
   components, so that for color component k, range[2*k] <= c[k] <=
   range[2*k+1], where c[k] is the k'th color component. `profile' is a data
   provider specifying the ICC profile. `alternate' specifies an alternate
   color space to be used in case the ICC profile is not supported. It must
   have `nComponents' color components. If `alternate' is NULL, then the
   color space used will be DeviceGray, DeviceRGB, or DeviceCMYK, depending
   on whether `nComponents' is 1, 3, or 4, respectively. }

function CGColorSpaceCreateICCBased( nComponents: size_t; {const} range: {variable-size-array} CGFloatPtr; profile: CGDataProviderRef; alternate: CGColorSpaceRef ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{* Special colorspaces. *}

{ Create an indexed color space. A sample value in an indexed color space
   is treated as an index into the color table of the color space. `base'
   specifies the base color space in which the values in the color table are
   to be interpreted. `lastIndex' is an integer which specifies the maximum
   valid index value; it must be less than or equal to 255. `colorTable' is
   an array of m * (lastIndex + 1) bytes, where m is the number of color
   components in the base color space. Each byte is an unsigned integer in
   the range 0 to 255 that is scaled to the range of the corresponding color
   component in the base color space. }

function CGColorSpaceCreateIndexed( baseSpace: CGColorSpaceRef; lastIndex: size_t; colorTable: UInt8Ptr ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Create a pattern color space. `baseSpace' is the underlying color space
   of the pattern color space. For colored patterns, `baseSpace' should be
   NULL; for uncolored patterns, `baseSpace' specifies the color space of
   colors which will be painted through the pattern. }

function CGColorSpaceCreatePattern( baseSpace: CGColorSpaceRef ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{$ifc TARGET_OS_MAC}
{ Create a color space using `ref', a platform-specific color space
   reference. For MacOS X, `ref' should be a CMProfileRef. }

function CGColorSpaceCreateWithPlatformColorSpace( ref: {const} univ Ptr ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_NA);
{$endc}

{ Create a color space using `name' as the identifier for the color
   space. }

function CGColorSpaceCreateWithName( name: CFStringRef ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Equivalent to `CFRetain(space)', except it doesn't crash (as CFRetain
   does) if `space' is NULL. }

function CGColorSpaceRetain( space: CGColorSpaceRef ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Equivalent to `CFRelease(space)', except it doesn't crash (as CFRelease
   does) if `space' is NULL. }

procedure CGColorSpaceRelease( space: CGColorSpaceRef );
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{$ifc TARGET_OS_MAC}
{ Return the name used to create the color space `space', or NULL if the
   color space was not created using `CGColorSpaceCreateWithName'. }

function CGColorSpaceCopyName( space: CGColorSpaceRef ): CFStringRef;
CG_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);
{$endc}

{* Colorspace information. *}

{ Return the CFTypeID for CGColorSpaces. }

function CGColorSpaceGetTypeID: CFTypeID;
CG_AVAILABLE_STARTING(__MAC_10_2, __IPHONE_2_0);

{ Return the number of color components in the color space `space'. }

function CGColorSpaceGetNumberOfComponents( space: CGColorSpaceRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the color space model of `space'. }

function CGColorSpaceGetModel( space: CGColorSpaceRef ): CGColorSpaceModel;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the base color space of `space' if `space' is a pattern or indexed
   color space; otherwise, return NULL. To determine whether a color space
   is an indexed or pattern color space, use `CGColorSpaceGetModel'. }

function CGColorSpaceGetBaseColorSpace( space: CGColorSpaceRef ): CGColorSpaceRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Return the number of entries in the color table of `space' if `space' is
   an indexed color space; otherwise, return 0. To determine whether a color
   space is an indexed color space, use `CGColorSpaceGetModel'. }

function CGColorSpaceGetColorTableCount( space: CGColorSpaceRef ): size_t;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{ Copy the entries in the color table of `space' to `table' if `space' is
   an indexed color space; otherwise, do nothing. The array pointed to by
   `table' should be at least as large as the number of entries in the color
   table; the returned data is in the same format as that passed to
   `CGColorSpaceCreateIndexed'. To determine whether a color space is an
   indexed color space, use `CGColorSpaceGetModel'. }

procedure CGColorSpaceGetColorTable( space: CGColorSpaceRef; table: UInt8Ptr );
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_2_0);

{$ifc TARGET_OS_MAC}
{ Return a copy of the ICC profile of `space', or NULL if the color space
   doesn't have an ICC profile. }

function CGColorSpaceCopyICCProfile( space: CGColorSpaceRef ): CFDataRef;
CG_AVAILABLE_STARTING(__MAC_10_5, __IPHONE_NA);
{$endc}

{* Deprecated APIs. *}

{ Use "kCGColorSpaceGenericGray" instead. }
const kCGColorSpaceUserGray = CFSTR( 'kCGColorSpaceUserGray' );

{ Use "kCGColorSpaceGenericRGB" instead. }
const kCGColorSpaceUserRGB = CFSTR( 'kCGColorSpaceUserRGB' );

{ Use "kCGColorSpaceGenericCMYK" instead. }
const kCGColorSpaceUserCMYK = CFSTR( 'kCGColorSpaceUserCMYK' );


end.
