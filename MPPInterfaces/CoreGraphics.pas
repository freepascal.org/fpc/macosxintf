{ CoreGraphics - CoreGraphics.h
   Copyright (c) 2000-2011 Apple Computer, Inc.
   All rights reserved.
 }
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Gale R Paeper, <gpaeper@empirenet.com>, 2008 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }

{$propagate-units}
unit CoreGraphics;
interface
uses MacTypes,CGBase,CGAffineTransforms,CGBitmapContext,CGColor,CGColorSpace,CGContext,CGDataConsumer,CGDataProvider,CGErrors,CGFont,CGFunction,CGGeometry,CGGradient,CGImage,CGLayer,CGPDFArray,CGPDFContentStream,CGPDFContext,CGPDFDictionary,CGPDFDocument,CGPDFObject,CGPDFOperatorTable,CGPDFPage,CGPDFScanner,CGPDFStream,CGPDFString,CGPath,CGPattern,CGShading,CGDirectDisplay,CGDirectPalette,CGDisplayConfiguration,CGDisplayFades,CGEvent,CGEventSource,CGEventTypes,CGGLContext,CGPSConverter,CGRemoteOperation,CGSession,CGWindow,CGWindowLevels;
{$ALIGN POWER}


end.
