{
 *  CVOpenGLTextureCache.h
 *  CoreVideo
 *
 *  Copyright 2004 Apple Computer, Inc. All rights reserved.
 *
 }
{  Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, 2009 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, August 2015 }

unit CVOpenGLTextureCache;
interface
uses MacTypes,CFBase,CFString,CFDictionary,CVBase,CVReturns,CVBuffer,CVImageBuffer,CVOpenGLTexture,CGLTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}

{!
    @typedef	CVOpenGLTextureCacheRef
    @abstract   CoreVideo OpenGL Texture Cache

}
type
	CVOpenGLTextureCacheRef = ^__CVOpenGLTextureCache; { an opaque type }
	__CVOpenGLTextureCache = record end;

{ Dictionary keys and values for use with the 'cacheAttributes' parameter of CVOpenGLTextureCacheCreate }

{ CVOpenGLTextureCache can (in some cases) do higher quality chroma upsampling on GPUs that support ARB_fragment_program.  By
   default it will be enabled automatically if the texture cache determines that the GPU has the needed support and the image
   size is something reasonable for the GPU being used.   The automatic behaviour can be overridden below.  Note that setting
   kCVOpenGLTextureCacheChromaSamplingModeHighQuality is only a request.   GPUs that don't support ARB_fragment_program will still
   resort back to the native hardware support for YCbCr textures. }
const kCVOpenGLTextureCacheChromaSamplingModeKey: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;
const kCVOpenGLTextureCacheChromaSamplingModeAutomatic: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;	    // Defaut if the key is not present
const kCVOpenGLTextureCacheChromaSamplingModeHighestQuality: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;    // Force highest quality regardless of performance impact
const kCVOpenGLTextureCacheChromaSamplingModeBestPerformance: CFStringRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;   // Do it the quickest way possible

function CVOpenGLTextureCacheGetTypeID: CFTypeID;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLTextureCacheRetain
    @abstract   Retains a CVOpenGLTextureCache object
    @discussion Equivalent to CFRetain, but NULL safe
    @param      buffer A CVOpenGLTextureCache object that you want to retain.
    @result     A CVOpenGLTextureCache object that is the same as the passed in buffer.
}
function CVOpenGLTextureCacheRetain( textureCache: CVOpenGLTextureCacheRef ): CVOpenGLTextureCacheRef;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER; // NULL-safe

{!
    @function   CVOpenGLTextureCacheRelease
    @abstract   Releases a CVOpenGLTextureCache object
    @discussion Equivalent to CFRelease, but NULL safe
    @param      buffer A CVOpenGLTextureCache object that you want to release.
}
procedure CVOpenGLTextureCacheRelease( textureCache: CVOpenGLTextureCacheRef );
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER; // NULL-safe

{!
    @function   CVOpenGLTextureCacheCreate
    @abstract   Creates a new Texture Cache.
    @param      allocator The CFAllocatorRef to use for allocating the cache.  May be NULL.
    @param      cacheAttributes A CFDictionaryRef containing the attributes of the cache itself.   May be NULL.
    @param      cglContext The OpenGL context into which the texture objects will be created
    @param      cglPixelFormat The OpenGL pixel format object used to create the passed in OpenGL context
    @param      textureAttributes A CFDictionaryRef containing the attributes to be used for creating the CVOpenGLTexture objects.  May be NULL.
    @param      cacheOut   The newly created texture cache will be placed here
    @result     Returns kCVReturnSuccess on success
}
function CVOpenGLTextureCacheCreate( allocator: CFAllocatorRef; cacheAttributes: CFDictionaryRef; cglContext: CGLContextObj; cglPixelFormat: CGLPixelFormatObj; textureAttributes: CFDictionaryRef; var cacheOut: CVOpenGLTextureCacheRef ): CVReturn;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLTextureCacheCreateTextureFromImage
    @abstract   Creates a CVOpenGLTexture object from an existing CVImageBuffer
    @param      allocator The CFAllocatorRef to use for allocating the CVOpenGLTexture object.  May be NULL.
    @param      textureCache The texture cache object that will manage the texture
    @param      sourceImage The CVImageBuffer that you want to create a CVOpenGLTexture from.
    @param      attributes For Future use only! - The desired buffer attributes for the CVOpenGLTexture.
    @param      textureOut The newly created texture object will be placed here.
    @result     Returns kCVReturnSuccess on success
}
function CVOpenGLTextureCacheCreateTextureFromImage( allocator: CFAllocatorRef; textureCache: CVOpenGLTextureCacheRef; sourceImage: CVImageBufferRef; attributes: CFDictionaryRef; var textureOut: CVOpenGLTextureRef ): CVReturn;
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{!
    @function   CVOpenGLTextureCacheFlush
    @abstract   Performs internal housekeeping/recycling operations
    @discussion This call must be made periodically to give the texture cache a chance to make OpenGL calls
                on the OpenGL context used to create it in order to do housekeeping operations.
    @param      textureCache The texture cache object to flush
    @param      options Currently unused, set to 0.
    @result     Returns kCVReturnSuccess on success
}
procedure CVOpenGLTextureCacheFlush( textureCache: CVOpenGLTextureCacheRef; options: CVOptionFlags );
AVAILABLE_MAC_OS_X_VERSION_10_4_AND_LATER;

{$endc}	// TARGET_OS_MAC

end.
