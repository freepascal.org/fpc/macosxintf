{	CFXMLNode.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFXMLNode;
interface
uses MacTypes,CFBase,CFArray,CFDictionary,CFString,CFTree,CFURL;
{$ALIGN POWER}


{  CFXMLParser (and thus CFXMLNode) are deprecated as of Mac OS X 10.8 and iOS 6.0. The suggested replacements are the Foundation classes NSXMLParser and NSXMLDocument, or the libxml2 library. }


const
	kCFXMLNodeCurrentVersion = 1;

type
	CFXMLNodeRef = ^__CFXMLNode; { an opaque type }
	__CFXMLNode = record end;
	CFXMLNodeRefPtr = ^CFXMLNodeRef;
	CFXMLTreeRef = CFTreeRef;

{  An CFXMLNode describes an individual XML construct - like a tag, or a comment, or a string
    of character data.  Each CFXMLNode contains 3 main pieces of information - the node's type,
    the data string, and a pointer to an additional data structure.  The node's type ID is an enum
    value of type CFXMLNodeTypeID.  The data string is always a CFStringRef; the meaning of the
    string is dependent on the node's type ID. The format of the additional data is also dependent
    on the node's type; in general, there is a custom structure for each type that requires
    additional data.  See below for the mapping from type ID to meaning of the data string and
    structure of the additional data.  Note that these structures are versioned, and may change
    as the parser changes.  The current version can always be identified by kCFXMLNodeCurrentVersion;
    earlier versions can be identified and used by passing earlier values for the version number
    (although the older structures would have been removed from the header).

    An CFXMLTree is simply a CFTree whose context data is known to be an CFXMLNodeRef.  As
    such, an CFXMLTree can be used to represent an entire XML document; the CFTree
    provides the tree structure of the document, while the CFXMLNodes identify and describe
    the nodes of the tree.  An XML document can be parsed to a CFXMLTree, and a CFXMLTree
    can generate the data for the equivalent XML document - see CFXMLParser.h for more
    information on parsing XML.
    }

{ Type codes for the different possible XML nodes; this list may grow.}
type
	CFXMLNodeTypeCode = CFIndex;
const
	kCFXMLNodeTypeDocument = 1;
	kCFXMLNodeTypeElement = 2;
	kCFXMLNodeTypeAttribute = 3;
	kCFXMLNodeTypeProcessingInstruction = 4;
	kCFXMLNodeTypeComment = 5;
	kCFXMLNodeTypeText = 6;
	kCFXMLNodeTypeCDATASection = 7;
	kCFXMLNodeTypeDocumentFragment = 8;
	kCFXMLNodeTypeEntity = 9;
	kCFXMLNodeTypeEntityReference = 10;
	kCFXMLNodeTypeDocumentType = 11;
	kCFXMLNodeTypeWhitespace = 12;
	kCFXMLNodeTypeNotation = 13;
	kCFXMLNodeTypeElementTypeDeclaration = 14;
	kCFXMLNodeTypeAttributeListDeclaration = 15;

type
	CFXMLElementInfoPtr = ^CFXMLElementInfo;
	CFXMLElementInfo = record
		attributes: CFDictionaryRef;
		attributeOrder: CFArrayRef;
		isEmpty: Boolean;
		_reserved: array [0..3-1] of SInt8;
	end;

type
	CFXMLProcessingInstructionInfoPtr = ^CFXMLProcessingInstructionInfo;
	CFXMLProcessingInstructionInfo = record
		dataString: CFStringRef;
	end;

type
	CFXMLDocumentInfoPtr = ^CFXMLDocumentInfo;
	CFXMLDocumentInfo = record
		sourceURL: CFURLRef;
		encoding: CFStringEncoding;
	end;

type
	CFXMLExternalIDPtr = ^CFXMLExternalID;
	CFXMLExternalID = record
		systemID: CFURLRef;
		publicID: CFStringRef;
	end;

type
	CFXMLDocumentTypeInfoPtr = ^CFXMLDocumentTypeInfo;
	CFXMLDocumentTypeInfo = record
		externalID: CFXMLExternalID;
	end;

type
	CFXMLNotationInfoPtr = ^CFXMLNotationInfo;
	CFXMLNotationInfo = record
		externalID: CFXMLExternalID;
	end;

type
	CFXMLElementTypeDeclarationInfoPtr = ^CFXMLElementTypeDeclarationInfo;
	CFXMLElementTypeDeclarationInfo = record
{ This is expected to change in future versions }
		contentDescription: CFStringRef;
	end;

type
	CFXMLAttributeDeclarationInfoPtr = ^CFXMLAttributeDeclarationInfo;
	CFXMLAttributeDeclarationInfo = record
{ This is expected to change in future versions }
		attributeName: CFStringRef;
		typeString: CFStringRef;
		defaultString: CFStringRef;
	end;

type
	CFXMLAttributeListDeclarationInfoPtr = ^CFXMLAttributeListDeclarationInfo;
	CFXMLAttributeListDeclarationInfo = record
		numberOfAttributes: CFIndex;
		attributes: CFXMLAttributeDeclarationInfoPtr;
	end;

type
	CFXMLEntityTypeCode = CFIndex;
const
	kCFXMLEntityTypeParameter = 0;							{  Implies parsed, internal  }
	kCFXMLEntityTypeParsedInternal = 1;
	kCFXMLEntityTypeParsedExternal = 2;
	kCFXMLEntityTypeUnparsed = 3;
	kCFXMLEntityTypeCharacter = 4;

type
	CFXMLEntityInfoPtr = ^CFXMLEntityInfo;
	CFXMLEntityInfo = record
		entityType: CFXMLEntityTypeCode;
		replacementText: CFStringRef;     { NULL if entityType is external or unparsed }
		entityID: CFXMLExternalID;          { entityID.systemID will be NULL if entityType is internal }
		notationName: CFStringRef;        { NULL if entityType is parsed }
	end;

type
	CFXMLEntityReferenceInfoPtr = ^CFXMLEntityReferenceInfo;
	CFXMLEntityReferenceInfo = record
		entityType: CFXMLEntityTypeCode;
	end;

{
 dataTypeCode                       meaning of dataString                format of infoPtr
 ===========                        =====================                =================
 kCFXMLNodeTypeDocument             <currently unused>                   CFXMLDocumentInfo *
 kCFXMLNodeTypeElement              tag name                             CFXMLElementInfo *
 kCFXMLNodeTypeAttribute            <currently unused>                   <currently unused>
 kCFXMLNodeTypeProcessingInstruction   name of the target                   CFXMLProcessingInstructionInfo *
 kCFXMLNodeTypeComment              text of the comment                  NULL
 kCFXMLNodeTypeText                 the text's contents                  NULL
 kCFXMLNodeTypeCDATASection         text of the CDATA                    NULL
 kCFXMLNodeTypeDocumentFragment     <currently unused>                   <currently unused>
 kCFXMLNodeTypeEntity               name of the entity                   CFXMLEntityInfo *
 kCFXMLNodeTypeEntityReference      name of the referenced entity        CFXMLEntityReferenceInfo *
 kCFXMLNodeTypeDocumentType         name given as top-level element      CFXMLDocumentTypeInfo *
 kCFXMLNodeTypeWhitespace           text of the whitespace               NULL
 kCFXMLNodeTypeNotation             notation name                        CFXMLNotationInfo *
 kCFXMLNodeTypeElementTypeDeclaration     tag name                       CFXMLElementTypeDeclarationInfo *
 kCFXMLNodeTypeAttributeListDeclaration   tag name                       CFXMLAttributeListDeclarationInfo *
}

function CFXMLNodeGetTypeID: CFTypeID;
CF_DEPRECATED(10_0, 10_8, 2_0, 6_0);

{ Creates a new node based on xmlType, dataString, and additionalInfoPtr.  version (together with xmlType) determines the expected structure of additionalInfoPtr }
function CFXMLNodeCreate( alloc: CFAllocatorRef; xmlType: CFXMLNodeTypeCode; dataString: CFStringRef; additionalInfoPtr: {const} univ Ptr; version: CFIndex ): CFXMLNodeRef;
CF_DEPRECATED(10_0, 10_8, 2_0, 6_0);

{ Creates a copy of origNode (which may not be NULL). }
function CFXMLNodeCreateCopy( alloc: CFAllocatorRef; origNode: CFXMLNodeRef ): CFXMLNodeRef;
CF_DEPRECATED(10_0, 10_8, 2_0, 6_0);

function CFXMLNodeGetTypeCode( node: CFXMLNodeRef ): CFXMLNodeTypeCode;
CF_DEPRECATED(10_0, 10_8, 2_0, 6_0);

function CFXMLNodeGetString( node: CFXMLNodeRef ): CFStringRef;
CF_DEPRECATED(10_0, 10_8, 2_0, 6_0);

function CFXMLNodeGetInfoPtr( node: CFXMLNodeRef ): UnivPtr;
CF_DEPRECATED(10_0, 10_8, 2_0, 6_0);

function CFXMLNodeGetVersion( node: CFXMLNodeRef ): CFIndex;
CF_DEPRECATED(10_0, 10_8, 2_0, 6_0);

{ CFXMLTreeRef }

{ Creates a childless, parentless tree from node }
function CFXMLTreeCreateWithNode( allocator: CFAllocatorRef; node: CFXMLNodeRef ): CFXMLTreeRef;
CF_DEPRECATED(10_0, 10_8, 2_0, 6_0);

{ Extracts and returns the node stored in xmlTree }
function CFXMLTreeGetNode( xmlTree: CFXMLTreeRef ): CFXMLNodeRef;
CF_DEPRECATED(10_0, 10_8, 2_0, 6_0);


end.
