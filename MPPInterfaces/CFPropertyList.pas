{	CFPropertyList.h
	Copyright (c) 1998-2013, Apple Inc. All rights reserved.
}
unit CFPropertyList;
interface
uses MacTypes,CFBase,CFData,CFError,CFString,CFStream;
{$ALIGN POWER}


type
	CFPropertyListMutabilityOptions = CFOptionFlags;
const
	kCFPropertyListImmutable = 0;
	kCFPropertyListMutableContainers = 1;
	kCFPropertyListMutableContainersAndLeaves = 2;
    
{
	Creates a property list object from its XML description; xmlData should
	be the raw bytes of that description, possibly the contents of an XML
	file. Returns NULL if the data cannot be parsed; if the parse fails
	and errorString is non-NULL, a human-readable description of the failure
	is returned in errorString. It is the caller's responsibility to release
	either the returned object or the error string, whichever is applicable.
 
        This function is obsolete and will be deprecated soon. See CFPropertyListCreateWithData() for a replacement.
}
function CFPropertyListCreateFromXMLData( allocator: CFAllocatorRef; xmlData: CFDataRef; mutabilityOption: CFOptionFlags; errorString: CFStringRefPtr ): CFPropertyListRef;

{
	Returns the XML description of the given object; propertyList must
	be one of the supported property list types, and (for composite types
	like CFArray and CFDictionary) must not contain any elements that
	are not themselves of a property list type. If a non-property list
	type is encountered, NULL is returned. The returned data is
	appropriate for writing out to an XML file. Note that a data, not a
	string, is returned because the bytes contain in them a description
	of the string encoding used.
 
        This function is obsolete and will be deprecated soon. See CFPropertyListCreateData() for a replacement.
}
function CFPropertyListCreateXMLData( allocator: CFAllocatorRef; propertyList: CFPropertyListRef ): CFDataRef;

{
	Recursively creates a copy of the given property list (so nested arrays
	and dictionaries are copied as well as the top-most container). The
	resulting property list has the mutability characteristics determined
	by mutabilityOption.
}
function CFPropertyListCreateDeepCopy( allocator: CFAllocatorRef; propertyList: CFPropertyListRef; mutabilityOption: CFOptionFlags ): CFPropertyListRef;

{#if MAC_OS_X_VERSION_10_2 <= MAC_OS_X_VERSION_MAX_ALLOWED}

type
	CFPropertyListFormat = CFIndex;
	CFPropertyListFormatPtr = ^CFPropertyListFormat;

const
    kCFPropertyListOpenStepFormat = 1;
    kCFPropertyListXMLFormat_v1_0 = 100;
    kCFPropertyListBinaryFormat_v1_0 = 200;

{ Returns true if the object graph rooted at plist is a valid property list
 * graph -- that is, no cycles, containing only plist objects, and dictionary
 * keys are strings. The debugging library version spits out some messages
 * to be helpful. The plist structure which is to be allowed is given by
 * the format parameter. }
function CFPropertyListIsValid( plist: CFPropertyListRef; format: CFPropertyListFormat ): Boolean;

{ Writes the bytes of a plist serialization out to the stream.  The
 * stream must be opened and configured -- the function simply writes
 * a bunch of bytes to the stream. The output plist format can be chosen.
 * Leaves the stream open, but note that reading a plist expects the
 * reading stream to end wherever the writing ended, so that the
 * end of the plist data can be identified. Returns the number of bytes
 * written, or 0 on error. Error messages are not currently localized, but
 * may be in the future, so they are not suitable for comparison. 
 *
 * This function is obsolete and will be deprecated soon. See CFPropertyListWrite() for a replacement. }
function CFPropertyListWriteToStream( propertyList: CFPropertyListRef; stream: CFWriteStreamRef; format: CFPropertyListFormat; var errorString: CFStringRef ): CFIndex;


{ Same as current function CFPropertyListCreateFromXMLData()
 * but takes a stream instead of data, and works on any plist file format.
 * CFPropertyListCreateFromXMLData() also works on any plist file format.
 * The stream must be open and configured -- the function simply reads a bunch
 * of bytes from it starting at the current location in the stream, to the END
 * of the stream, which is expected to be the end of the plist, or up to the
 * number of bytes given by the length parameter if it is not 0. Error messages
 * are not currently localized, but may be in the future, so they are not
 * suitable for comparison. 
 *
 * This function is obsolete and will be deprecated soon. See CFPropertyListCreateWithStream() for a replacement. }
function CFPropertyListCreateFromStream( allocator: CFAllocatorRef; stream: CFReadStreamRef; streamLength: CFIndex; mutabilityOption: CFOptionFlags; var format: CFPropertyListFormat; var errorString: CFStringRef ): CFPropertyListRef;

{#endif}

{#if MAC_OS_X_VERSION_10_6 <= MAC_OS_X_VERSION_MAX_ALLOWED}
const
	kCFPropertyListReadCorruptError = 3840; CF_ENUM_AVAILABLE(10_6, 4_0);             // Error parsing a property list
	kCFPropertyListReadUnknownVersionError = 3841; CF_ENUM_AVAILABLE(10_6, 4_0);      // The version number in the property list is unknown
	kCFPropertyListReadStreamError = 3842; CF_ENUM_AVAILABLE(10_6, 4_0);              // Stream error reading a property list
	kCFPropertyListWriteStreamError = 3851; CF_ENUM_AVAILABLE(10_6, 4_0);             // Stream error writing a property list
{#endif}

{ Create a property list with a CFData input. If the format parameter is non-NULL, it will be set to the format of the data after parsing is complete. The options parameter is used to specify CFPropertyListMutabilityOptions. If an error occurs while parsing the data, the return value will be NULL. Additionally, if an error occurs and the error parameter is non-NULL, the error parameter will be set to a CFError describing the problem, which the caller must release. If the parse succeeds, the returned value is a reference to the new property list. It is the responsibility of the caller to release this value.
 }
function CFPropertyListCreateWithData( allocator: CFAllocatorRef; data: CFDataRef; options: CFOptionFlags; format: CFPropertyListFormatPtr { can be NULL };  error: CFErrorRefPtr { can be NULL } ): CFPropertyListRef;
CF_AVAILABLE_STARTING(10_6, 4_0);

{ Apple sets TARGET_OS_MAC to true for TARGET_IPHONE_SIMULATOR, but since we use unified
  headers for Mac OS X and iPhoneSimulator, that would result in many problems -> specify
  it explicitly }
{$ifc TARGET_OS_MAC or TARGET_OS_WIN32 or TARGET_OS_EMBEDDED or TARGET_IPHONE_SIMULATOR}
{ Create and return a property list with a CFReadStream input. TIf the format parameter is non-NULL, it will be set to the format of the data after parsing is complete. The options parameter is used to specify CFPropertyListMutabilityOptions. The streamLength parameter specifies the number of bytes to read from the stream. Set streamLength to 0 to read until the end of the stream is detected. If an error occurs while parsing the data, the return value will be NULL. Additionally, if an error occurs and the error parameter is non-NULL, the error parameter will be set to a CFError describing the problem, which the caller must release. If the parse succeeds, the returned value is a reference to the new property list. It is the responsibility of the caller to release this value.
 }
function CFPropertyListCreateWithStream( allocator: CFAllocatorRef; stream: CFReadStreamRef; streamLength: CFIndex; options: CFOptionFlags;  format: CFPropertyListFormatPtr { can be NULL }; error: CFErrorRefPtr { can be NULL } ): CFPropertyListRef;
CF_AVAILABLE_STARTING(10_6, 4_0);

{ Write the bytes of a serialized property list out to a stream. The stream must be opened and configured. The format of the property list can be chosen with the format parameter. The options parameter is currently unused and should be set to 0. The return value is the number of bytes written or 0 in the case of an error. If an error occurs and the error parameter is non-NULL, the error parameter will be set to a CFError describing the problem, which the caller must release.
 }
function CFPropertyListWrite( propertyList: CFPropertyListRef; stream: CFWriteStreamRef; format: CFPropertyListFormat; options: CFOptionFlags; error: CFErrorRefPtr { can be NULL } ): CFIndex;
CF_AVAILABLE_STARTING(10_6, 4_0);

{$endc} {TARGET_OS_MAC or TARGET_OS_WIN32 or TARGET_OS_EMBEDDED or TARGET_IPHONE_SIMULATOR}

{ Create a CFData with the bytes of a serialized property list. The format of the property list can be chosen with the format parameter. The options parameter is currently unused and should be set to 0. If an error occurs while parsing the data, the return value will be NULL. Additionally, if an error occurs and the error parameter is non-NULL, the error parameter will be set to a CFError describing the problem, which the caller must release. If the conversion succeeds, the returned value is a reference to the created data. It is the responsibility of the caller to release this value.
 }
function CFPropertyListCreateData( allocator: CFAllocatorRef; propertyList: CFPropertyListRef; format: CFPropertyListFormat; options: CFOptionFlags; error: CFErrorRefPtr { can be NULL } ): CFDataRef;
CF_AVAILABLE_STARTING(10_6, 4_0);



end.
