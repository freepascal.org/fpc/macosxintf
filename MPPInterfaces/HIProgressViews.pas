{
     File:       HIToolbox/HIProgressViews.h
 
     Contains:   Definition of the progress-display views provided by HIToolbox.
 
     Version:    HIToolbox-624~3
 
     Copyright:  © 2006-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Initial Pascal Translation:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
unit HIProgressViews;
interface
uses MacTypes,Appearance,CarbonEvents,Controls,QuickdrawTypes,HIObject;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
 *  HIProgressViews.h
 *  
 *  Discussion:
 *    API definitions for the views that display progress: the progress
 *    bar and the chasing arrows.
 }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ PROGRESS INDICATOR (CDEF 5)                                                       }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  This CDEF implements both determinate and indeterminate progress bars. To switch,   }
{  just use SetControlData to set the indeterminate flag to make it indeterminate. Any }
{  animation will automatically be handled by the toolbox on an event timer.           }
{ Progress Bar proc IDs }
const
	kControlProgressBarProc = 80;
	kControlRelevanceBarProc = 81;

{ Control Kind Tag }
const
	kControlKindProgressBar = FOUR_CHAR_CODE('prgb');
	kControlKindRelevanceBar = FOUR_CHAR_CODE('relb');

{ The HIObject class ID for the HIProgressBar class. }
const kHIProgressBarClassID = CFSTR( 'com.apple.HIProgressBar' );
{ Creation API: Carbon only }
{$ifc not TARGET_CPU_64}
{
 *  CreateProgressBarControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateProgressBarControl( window: WindowRef; const var boundsRect: Rect; value: SInt32; minimum: SInt32; maximum: SInt32; indeterminate: Boolean; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Tagged data supported by progress bars }
{$endc} {not TARGET_CPU_64}

const
	kControlProgressBarIndeterminateTag = FOUR_CHAR_CODE('inde'); { Boolean}
	kControlProgressBarAnimatingTag = FOUR_CHAR_CODE('anim'); { Boolean}

{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  ₯ CHASING ARROWS (CDEF 7)                                                           }
{ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ}
{  This control will automatically animate via an event loop timer.                    }
{ Chasing Arrows proc IDs }
const
	kControlChasingArrowsProc = 112;

{ Control Kind Tag }
const
	kControlKindChasingArrows = FOUR_CHAR_CODE('carr');

{ The HIObject class ID for the HIChasingArrows class. }
const kHIChasingArrowsClassID = CFSTR( 'com.apple.HIChasingArrows' );
{ Creation API: Carbon only }
{$ifc not TARGET_CPU_64}
{
 *  CreateChasingArrowsControl()
 *  
 *  Mac OS X threading:
 *    Not thread safe
 *  
 *  Availability:
 *    Mac OS X:         in version 10.0 and later in Carbon.framework [32-bit only]
 *    CarbonLib:        in CarbonLib 1.1 and later
 *    Non-Carbon CFM:   not available
 }
function CreateChasingArrowsControl( window: WindowRef; const var boundsRect: Rect; var outControl: ControlRef ): OSStatus;
AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;


{ Tagged data supported by the Chasing Arrows control }
{$endc} {not TARGET_CPU_64}

const
	kControlChasingArrowsAnimatingTag = FOUR_CHAR_CODE('anim'); { Boolean}

{$endc} {TARGET_OS_MAC}

end.
