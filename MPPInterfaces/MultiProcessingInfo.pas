{
     File:       CarbonCore/MultiprocessingInfo.h
 
     Contains:   Multiprocessing Information interfaces
                 The contents of this header file are deprecated.
 
     Copyright:  � 1995-2011 DayStar Digital, Inc.
}
unit MultiProcessingInfo;
interface
uses MacTypes,Multiprocessing;

{$ifc TARGET_OS_MAC}

{********************************************************************************************
 
 The Multiprocessing Utilites are deprecated.  Callers should use blocks, libDispatch, or pthreads.
  
********************************************************************************************}
{
   ==========================================================================================================================
   *** WARNING: You must properly check the availability of MP services before calling them!
   See the section titled "Checking API Availability".
   ==========================================================================================================================
}


{$ALIGN POWER}

{
   ======================================= NOTICE ============================================
   As of Mac OS X v10.6, the APIs in this header file are discouraged. These APIs are slated
   for deprecation in the next major release of OS X. The new dispatch APIs (see dispatch(3))
   replace the Multiprocessing APIs and the pthread threading APIs.
   ===========================================================================================
}

{
   ==========================================================================================================================
   This is the header file for version 2.3 of the Mac OS multiprocessing information support. 
   ==========================================================================================================================
}


{
   ==========================================================================================================================
   The following services are new in version 2.1:
    MPGetNextTaskID
    MPGetNextCpuID
   ==========================================================================================================================
}

{
   ==========================================================================================================================
   The following services are new in version 2.2:
    MPGetPageSizeClasses
    MPGetPageSize
    MPGetNextAreaID
   ==========================================================================================================================
}

{
   ==========================================================================================================================
   The following services are new in version 2.3:
    MPGetNextCoherenceID
    MPGetNextProcessID
    MPGetNextAddressSpaceID
    MPGetNextQueueID
    MPGetNextSemaphoreID
    MPGetNextCriticalRegionID
    MPGetNextTimerID
    MPGetNextEventID
    MPGetNextNotificationID
    MPGetNextConsoleID
   ==========================================================================================================================
}


{
   �
   ==========================================================================================================================
   Page size Services
   ==================
}

{
 *  MPGetPageSizeClasses()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{ The number of page size classes, 1 to n.}
{ -------------------------------------------------------------------------------------------}
{
 *  MPGetPageSize()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{ The page size in bytes.}

{
   �
   ==========================================================================================================================
   ID Iterator Services
   ==========================
}

{
 *  MPGetNextCoherenceID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextCpuID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework but deprecated in 10.7
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }
function MPGetNextCpuID( owningCoherenceID: MPCoherenceID; var cpuID: MPCpuID ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);


{
 *  MPGetNextProcessID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextAddressSpaceID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextTaskID()   *** DEPRECATED ***
 *  
 *  Availability:
 *    Mac OS X:         in version 10.4 and later in CoreServices.framework but deprecated in 10.7
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }
function MPGetNextTaskID( owningProcessID: MPProcessID; var taskID: MPTaskID ): OSStatus;
__OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_4, __MAC_10_7, __IPHONE_NA, __IPHONE_NA);


{
 *  MPGetNextQueueID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextSemaphoreID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextCriticalRegionID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextTimerID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextEventID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextNotificationID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextAreaID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNextConsoleID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{ -------------------------------------------------------------------------------------------}


{
 *  MPGetNextID()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
   �
   ==========================================================================================================================
   Object Information Services
   ===========================
}


{
   ----------------------------------------------------------------------------------------------
   ! The implementation of MPGetObjectInfo assumes that all info records are in 4 byte multiples.
}


const
{ The version of the MPAreaInfo structure requested.}
	kMPQueueInfoVersion = 1 or (kOpaqueQueueID shl 16);
	kMPSemaphoreInfoVersion = 1 or (kOpaqueSemaphoreID shl 16);
	kMPEventInfoVersion = 1 or (kOpaqueEventID shl 16);
	kMPCriticalRegionInfoVersion = 1 or (kOpaqueCriticalRegionID shl 16);
	kMPNotificationInfoVersion = 1 or (kOpaqueNotificationID shl 16);
	kMPAddressSpaceInfoVersion = 1 or (kOpaqueAddressSpaceID shl 16);


type
	MPQueueInfoPtr = ^MPQueueInfo;
	MPQueueInfo = record
		version: PBVersion;                { Version of the data structure requested}

		processID: MPProcessID;              { Owning process ID}
		queueName: OSType;              { Queue name}

		nWaiting: ItemCount;
		waitingTaskID: MPTaskID;          { First waiting task.}

		nMessages: ItemCount;
		nReserved: ItemCount;

		p1: UnivPtr;                     { First message parameters...}
		p2: UnivPtr;
		p3: UnivPtr;
	end;
type
	MPSemaphoreInfoPtr = ^MPSemaphoreInfo;
	MPSemaphoreInfo = record
		version: PBVersion;                { Version of the data structure requested}

		processID: MPProcessID;              { Owning process ID}
		semaphoreName: OSType;          { Semaphore name}

		nWaiting: ItemCount;
		waitingTaskID: MPTaskID;          { First waiting task.}

		maximum: ItemCount;
		count: ItemCount;
	end;
type
	MPEventInfoPtr = ^MPEventInfo;
	MPEventInfo = record
		version: PBVersion;                { Version of the data structure requested}

		processID: MPProcessID;              { Owning process ID}
		eventName: OSType;              { Event name}

		nWaiting: ItemCount;
		waitingTaskID: MPTaskID;          { First waiting task.}

		events: MPEventFlags;
	end;
type
	MPCriticalRegionInfoPtr = ^MPCriticalRegionInfo;
	MPCriticalRegionInfo = record
		version: PBVersion;                { Version of the data structure requested}

		processID: MPProcessID;              { Owning process ID}
		regionName: OSType;             { Critical region name}

		nWaiting: ItemCount;
		waitingTaskID: MPTaskID;          { First waiting task.}

		owningTask: MPTaskID;
		count: ItemCount;
	end;
type
	MPNotificationInfoPtr = ^MPNotificationInfo;
	MPNotificationInfo = record
		version: PBVersion;                { Version of the data structure requested}

		processID: MPProcessID;              { Owning process ID}
		notificationName: OSType;       { Notification name}

		queueID: MPQueueID;                { Queue to notify.}
		p1: UnivPtr;
		p2: UnivPtr;
		p3: UnivPtr;

		eventID: MPEventID;                { Event to set.}
		events: MPEventFlags;

		semaphoreID: MPSemaphoreID;            { Sempahore to signal.   }
	end;
type
	MPAddressSpaceInfoPtr = ^MPAddressSpaceInfo;
	MPAddressSpaceInfo = record
		version: PBVersion;                { Version of the data structure requested}

		processID: MPProcessID;              { Owning process ID}
		groupID: MPCoherenceID;                { Related coherence group.}
		nTasks: ItemCount;                 { Number of tasks in this space.}
    vsid: array [0..15] of UInt32;               { Segment register VSIDs.}
	end;
{ *** We should put the task info call here instead of in MPExtractTaskState.}


{
 *  MPGetQueueInfo()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetSemaphoreInfo()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetEventInfo()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetCriticalRegionInfo()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetNotificationInfo()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{
 *  MPGetAddressSpaceInfo()
 *  
 *  Availability:
 *    Mac OS X:         not available
 *    CarbonLib:        not available
 *    Non-Carbon CFM:   in MPDiagnostics 2.3 and later
 }


{ ==========================================================================================================================}


{$endc} {TARGET_OS_MAC}


end.
