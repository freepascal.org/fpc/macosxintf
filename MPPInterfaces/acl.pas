{
 * Copyright (c) 2004, 2010 Apple Inc. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 * 
 * The contents of this file constitute Original Code as defined in and
 * are subject to the Apple Public Source License Version 1.1 (the
 * "License").  You may not use this file except in compliance with the
 * License.  Please obtain a copy of the License at
 * http://www.apple.com/publicsource and read it before using this file.
 * 
 * This Original Code and all software distributed under the License are
 * distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT.  Please see the
 * License for the specific language governing rights and limitations
 * under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 }
unit acl;
interface
uses MacTypes,MacOSXPosix;
{$ALIGN POWER}


const __DARWIN_ACL_READ_DATA = 1 shl 1;
const __DARWIN_ACL_LIST_DIRECTORY = __DARWIN_ACL_READ_DATA;
const __DARWIN_ACL_WRITE_DATA = 1 shl 2;
const __DARWIN_ACL_ADD_FILE = __DARWIN_ACL_WRITE_DATA;
const __DARWIN_ACL_EXECUTE = 1 shl 3;
const __DARWIN_ACL_SEARCH = __DARWIN_ACL_EXECUTE;
const __DARWIN_ACL_DELETE = 1 shl 4;
const __DARWIN_ACL_APPEND_DATA = 1 shl 5;
const __DARWIN_ACL_ADD_SUBDIRECTORY = __DARWIN_ACL_APPEND_DATA;
const __DARWIN_ACL_DELETE_CHILD = 1 shl 6;
const __DARWIN_ACL_READ_ATTRIBUTES = 1 shl 7;
const __DARWIN_ACL_WRITE_ATTRIBUTES = 1 shl 8;
const __DARWIN_ACL_READ_EXTATTRIBUTES = 1 shl 9;
const __DARWIN_ACL_WRITE_EXTATTRIBUTES = 1 shl 10;
const __DARWIN_ACL_READ_SECURITY = 1 shl 11;
const __DARWIN_ACL_WRITE_SECURITY = 1 shl 12;
const __DARWIN_ACL_CHANGE_OWNER = 1 shl 13;

const
	__DARWIN_ACL_EXTENDED_ALLOW = 1;
const
	__DARWIN_ACL_EXTENDED_DENY = 2;

const __DARWIN_ACL_ENTRY_INHERITED = 1 shl 4;
const __DARWIN_ACL_ENTRY_FILE_INHERIT = 1 shl 5;
const __DARWIN_ACL_ENTRY_DIRECTORY_INHERIT = 1 shl 6;
const __DARWIN_ACL_ENTRY_LIMIT_INHERIT = 1 shl 7;
const __DARWIN_ACL_ENTRY_ONLY_INHERIT = 1 shl 8;
const __DARWIN_ACL_FLAG_NO_INHERIT = 1 shl 17;

{
 * Implementation constants.
 *
 * The ACL_TYPE_EXTENDED binary format permits 169 entries plus
 * the ACL header in a page.  Give ourselves some room to grow;
 * this limit is arbitrary.
 }
const
	ACL_MAX_ENTRIES = 128;

{ 23.2.2 Individual object access permissions - nonstandard }
type
  acl_perm_t = SInt32;
const
	ACL_READ_DATA		= __DARWIN_ACL_READ_DATA;
	ACL_LIST_DIRECTORY	= __DARWIN_ACL_LIST_DIRECTORY;
	ACL_WRITE_DATA		= __DARWIN_ACL_WRITE_DATA;
	ACL_ADD_FILE		= __DARWIN_ACL_ADD_FILE;
	ACL_EXECUTE		= __DARWIN_ACL_EXECUTE;
	ACL_SEARCH		= __DARWIN_ACL_SEARCH;
	ACL_DELETE		= __DARWIN_ACL_DELETE;
	ACL_APPEND_DATA		= __DARWIN_ACL_APPEND_DATA;
	ACL_ADD_SUBDIRECTORY	= __DARWIN_ACL_ADD_SUBDIRECTORY;
	ACL_DELETE_CHILD	= __DARWIN_ACL_DELETE_CHILD;
	ACL_READ_ATTRIBUTES	= __DARWIN_ACL_READ_ATTRIBUTES;
	ACL_WRITE_ATTRIBUTES	= __DARWIN_ACL_WRITE_ATTRIBUTES;
	ACL_READ_EXTATTRIBUTES	= __DARWIN_ACL_READ_EXTATTRIBUTES;
	ACL_WRITE_EXTATTRIBUTES	= __DARWIN_ACL_WRITE_EXTATTRIBUTES;
	ACL_READ_SECURITY	= __DARWIN_ACL_READ_SECURITY;
	ACL_WRITE_SECURITY	= __DARWIN_ACL_WRITE_SECURITY;
	ACL_CHANGE_OWNER	= __DARWIN_ACL_CHANGE_OWNER;


{ 23.2.5 ACL entry tag type bits - nonstandard }
type
  acl_tag_t = SInt32;
const
	ACL_UNDEFINED_TAG	= 0;
	ACL_EXTENDED_ALLOW	= __DARWIN_ACL_EXTENDED_ALLOW;
	ACL_EXTENDED_DENY	= __DARWIN_ACL_EXTENDED_DENY;

{ 23.2.6 Individual ACL types }
type
  acl_type_t = SInt32;
const
	ACL_TYPE_EXTENDED	= $00000100;
{ Posix 1003.1e types - not supported }
	ACL_TYPE_ACCESS         = $00000000;
	ACL_TYPE_DEFAULT        = $00000001;
{ The following types are defined on FreeBSD/Linux - not supported }
	ACL_TYPE_AFS            = $00000002;
	ACL_TYPE_CODA           = $00000003;
	ACL_TYPE_NTFS           = $00000004;
	ACL_TYPE_NWFS           = $00000005;

{ 23.2.7 ACL qualifier constants }

const ACL_UNDEFINED_ID = nil;	{ XXX ? }

{ 23.2.8 ACL Entry Constants }
type
  acl_entry_id_t = SInt32;
const
	ACL_FIRST_ENTRY		= 0;
	ACL_NEXT_ENTRY		= -1;
	ACL_LAST_ENTRY		= -2;

{ nonstandard ACL / entry flags }
type
  acl_flag_t = SInt32;
const
	ACL_FLAG_DEFER_INHERIT		= (1  shl  0);	{ tentative }
	ACL_FLAG_NO_INHERIT		= __DARWIN_ACL_FLAG_NO_INHERIT;
	ACL_ENTRY_INHERITED		= __DARWIN_ACL_ENTRY_INHERITED;
	ACL_ENTRY_FILE_INHERIT		= __DARWIN_ACL_ENTRY_FILE_INHERIT;
	ACL_ENTRY_DIRECTORY_INHERIT	= __DARWIN_ACL_ENTRY_DIRECTORY_INHERIT;
	ACL_ENTRY_LIMIT_INHERIT		= __DARWIN_ACL_ENTRY_LIMIT_INHERIT;
	ACL_ENTRY_ONLY_INHERIT		= __DARWIN_ACL_ENTRY_ONLY_INHERIT;

{ "External" ACL types }
type
	acl_t = ^_acl; { an opaque type }
	_acl = record end;
	acl_entry_t = ^_acl_entry; { an opaque type }
	_acl_entry = record end;
	acl_permset_t = ^_acl_permset; { an opaque type }
	_acl_permset = record end;
	acl_flagset_t = ^_acl_flagset; { an opaque type }
	_acl_flagset = record end;

type
	acl_permset_mask_t = UInt64;

{ 23.1.6.1 ACL Storage Management }
function acl_dup( acl: acl_t ): acl_t;
function acl_free( obj_p: univ Ptr ): SInt32;
function acl_init( count: SInt32 ): acl_t;

{ 23.1.6.2 (1) ACL Entry manipulation }
function acl_copy_entry( dest_d: acl_entry_t; src_d: acl_entry_t ): SInt32;
function acl_create_entry( var acl_p: acl_t; var entry_p: acl_entry_t ): SInt32;
function acl_create_entry_np( var acl_p: acl_t; var entry_p: acl_entry_t; entry_index: SInt32 ): SInt32;
function acl_delete_entry( acl: acl_t; entry_d: acl_entry_t ): SInt32;
function acl_get_entry( acl: acl_t; entry_id: SInt32; var entry_p: acl_entry_t ): SInt32;
function acl_valid( acl: acl_t ): SInt32;
function acl_valid_fd_np( fd: SInt32; typ: acl_type_t; acl: acl_t ): SInt32;
function acl_valid_file_np( path: ConstCStringPtr; typ: acl_type_t; acl: acl_t ): SInt32;
function acl_valid_link_np( path: ConstCStringPtr; typ: acl_type_t; acl: acl_t ): SInt32;

{ 23.1.6.2 (2) Manipulate permissions within an ACL entry }
function acl_add_perm( permset_d: acl_permset_t; perm: acl_perm_t ): SInt32;
function acl_calc_mask( var acl_p: acl_t ): SInt32;	{ not supported }
function acl_clear_perms( permset_d: acl_permset_t ): SInt32;
function acl_delete_perm( permset_d: acl_permset_t; perm: acl_perm_t ): SInt32;
function acl_get_perm_np( permset_d: acl_permset_t; perm: acl_perm_t ): SInt32;
function acl_get_permset( entry_d: acl_entry_t; var permset_p: acl_permset_t ): SInt32;
function acl_set_permset( entry_d: acl_entry_t; permset_d: acl_permset_t ): SInt32;

{ nonstandard - manipulate permissions within an ACL entry using bitmasks }
function acl_maximal_permset_mask_np( var mask_p: acl_permset_mask_t ): SInt32;
__OSX_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_4_3);
function acl_get_permset_mask_np( entry_d: acl_entry_t; var mask_p: acl_permset_mask_t ): SInt32;
__OSX_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_4_3);
function acl_set_permset_mask_np( entry_d: acl_entry_t; mask: acl_permset_mask_t ): SInt32;
__OSX_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_4_3);

{ nonstandard - manipulate flags on ACLs and entries }
function acl_add_flag_np( flagset_d: acl_flagset_t; flag: acl_flag_t ): SInt32;
function acl_clear_flags_np( flagset_d: acl_flagset_t ): SInt32;
function acl_delete_flag_np( flagset_d: acl_flagset_t; flag: acl_flag_t ): SInt32;
function acl_get_flag_np( flagset_d: acl_flagset_t; flag: acl_flag_t ): SInt32;
function acl_get_flagset_np( obj_p: univ Ptr; var flagset_p: acl_flagset_t ): SInt32;
function acl_set_flagset_np( obj_p: univ Ptr; flagset_d: acl_flagset_t ): SInt32;

{ 23.1.6.2 (3) Manipulate ACL entry tag type and qualifier }
function acl_get_qualifier( entry_d: acl_entry_t ): UnivPtr;
function acl_get_tag_type( entry_d: acl_entry_t; var tag_type_p: acl_tag_t ): SInt32;
function acl_set_qualifier( entry_d: acl_entry_t; tag_qualifier_p: {const} univ Ptr ): SInt32;
function acl_set_tag_type( entry_d: acl_entry_t; tag_type: acl_tag_t ): SInt32;

{ 23.1.6.3 ACL manipulation on an Object }
function acl_delete_def_file( path_p: ConstCStringPtr ): SInt32; { not supported }
function acl_get_fd( fd: SInt32 ): acl_t;
function acl_get_fd_np( fd: SInt32; typ: acl_type_t ): acl_t;
function acl_get_file( path_p: ConstCStringPtr; typ: acl_type_t ): acl_t;
function acl_get_link_np( path_p: ConstCStringPtr; typ: acl_type_t ): acl_t;
function acl_set_fd( fd: SInt32; acl: acl_t ): SInt32;
function acl_set_fd_np( fd: SInt32; acl: acl_t; acl_type: acl_type_t ): SInt32;
function acl_set_file( path_p: ConstCStringPtr; typ: acl_type_t; acl: acl_t ): SInt32;
function acl_set_link_np( path_p: ConstCStringPtr; typ: acl_type_t; acl: acl_t ): SInt32;

{ 23.1.6.4 ACL Format translation }
function acl_copy_ext( buf_p: univ Ptr; acl: acl_t; size: ssize_t ): ssize_t;
function acl_copy_ext_native( buf_p: univ Ptr; acl: acl_t; size: ssize_t ): ssize_t;
function acl_copy_int( buf_p: {const} univ Ptr ): acl_t;
function acl_copy_int_native( buf_p: {const} univ Ptr ): acl_t;
function acl_from_text( buf_p: ConstCStringPtr ): acl_t;
function acl_size( acl: acl_t ): ssize_t;
function acl_to_text( acl: acl_t; var len_p: ssize_t ): CStringPtr;

end.
