{ CoreGraphics - CGAffineTransform.h
   Copyright (c) 1998-2011 Apple Inc.
 * All rights reserved.
 }
{       Pascal Translation Updated:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, August 2015 }
unit CGAffineTransforms;
interface
uses MacTypes,CGBase,CGGeometry;
{$ALIGN POWER}


type
	CGAffineTransformPtr = ^CGAffineTransform;
	CGAffineTransform = record
		a, b, c, d: CGFloat;
		tx, ty: CGFloat;
	end;

{ The identity transform: [ 1 0 0 1 0 0 ]. }

const CGAffineTransformIdentity: CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return the transform [ a b c d tx ty ]. }

function CGAffineTransformMake( a: CGFloat; b: CGFloat; c: CGFloat; d: CGFloat; tx: CGFloat; ty: CGFloat ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return a transform which translates by `(tx, ty)':
     t' = [ 1 0 0 1 tx ty ] }

function CGAffineTransformMakeTranslation( tx: CGFloat; ty: CGFloat ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return a transform which scales by `(sx, sy)':
     t' = [ sx 0 0 sy 0 0 ] }

function CGAffineTransformMakeScale( sx: CGFloat; sy: CGFloat ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return a transform which rotates by `angle' radians:
     t' = [ cos(angle) sin(angle) -sin(angle) cos(angle) 0 0 ] }

function CGAffineTransformMakeRotation( angle: CGFloat ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `t' is the identity transform, false otherwise. }

function CGAffineTransformIsIdentity( t: CGAffineTransform ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Translate `t' by `(tx, ty)' and return the result:
     t' = [ 1 0 0 1 tx ty ] * t }

function CGAffineTransformTranslate( t: CGAffineTransform; tx: CGFloat; ty: CGFloat ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Scale `t' by `(sx, sy)' and return the result:
     t' = [ sx 0 0 sy 0 0 ] * t }

function CGAffineTransformScale( t: CGAffineTransform; sx: CGFloat; sy: CGFloat ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Rotate `t' by `angle' radians and return the result:
     t' =  [ cos(angle) sin(angle) -sin(angle) cos(angle) 0 0 ] * t }

function CGAffineTransformRotate( t: CGAffineTransform; angle: CGFloat ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Invert `t' and return the result. If `t' has zero determinant, then `t'
   is returned unchanged. }

function CGAffineTransformInvert( t: CGAffineTransform ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Concatenate `t2' to `t1' and return the result:
     t' = t1 * t2 }

function CGAffineTransformConcat( t1: CGAffineTransform; t2: CGAffineTransform ): CGAffineTransform;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Return true if `t1' and `t2' are equal, false otherwise. }

function CGAffineTransformEqualToTransform( t1: CGAffineTransform; t2: CGAffineTransform ): CBool;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

{ Transform `point' by `t' and return the result:
     p' = p * t
   where p = [ x y 1 ]. }

function CGPointApplyAffineTransform( point: CGPoint; t: CGAffineTransform ): CGPoint;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Transform `size' by `t' and return the result:
     s' = s * t
   where s = [ width height 0 ]. }

function CGSizeApplyAffineTransform( size: CGSize; t: CGAffineTransform ): CGSize;
CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

{ Transform `rect' by `t' and return the result. Since affine transforms do
   not preserve rectangles in general, this function returns the smallest
   rectangle which contains the transformed corner points of `rect'. If `t'
   consists solely of scales, flips and translations, then the returned
   rectangle coincides with the rectangle constructed from the four
   transformed corners. }

function CGRectApplyAffineTransform( rect: CGRect; t: CGAffineTransform ): CGRect;
CG_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_2_0);

(*
{** Definitions of inline functions. **}

CG_INLINE CGAffineTransform
__CGAffineTransformMake(float a, float b, float c, float d, float tx, float ty)
{
    CGAffineTransform t;

    t.a = a; t.b = b; t.c = c; t.d = d; t.tx = tx; t.ty = ty;
    return t;
}

#define CGAffineTransformMake __CGAffineTransformMake

CG_INLINE CGPoint
__CGPointApplyAffineTransform(CGPoint point, CGAffineTransform t)
{
    CGPoint p;

    p.x = t.a * point.x + t.c * point.y + t.tx;
    p.y = t.b * point.x + t.d * point.y + t.ty;
    return p;
}

#define CGPointApplyAffineTransform __CGPointApplyAffineTransform

CG_INLINE CGSize
__CGSizeApplyAffineTransform(CGSize size, CGAffineTransform t)
{
    CGSize s;

    s.width = t.a * size.width + t.c * size.height;
    s.height = t.b * size.width + t.d * size.height;
    return s;
}

#define CGSizeApplyAffineTransform __CGSizeApplyAffineTransform
*)

end.
