{
	Copyright:	(c) 1999-2008 Apple Inc. All rights reserved.
}
{       Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }

unit CGLCurrent;
interface
uses MacTypes,CGLTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
** Current context functions
}
function CGLSetCurrentContext( ctx: CGLContextObj ): CGLError;
function CGLGetCurrentContext: CGLContextObj;

{$endc} {TARGET_OS_MAC}

end.
