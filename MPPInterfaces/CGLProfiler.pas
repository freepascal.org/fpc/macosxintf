{
	Copyright:	(c) 2002 by Apple Computer, Inc., all rights reserved.
}
{       Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }

unit CGLProfiler;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{ Profiler extensions for CGLContextParameter }

{ Use these parameter names as the argument to CGLSetParameter. }
const
{ #define kCGLCPComment ((CGLContextParameter)1232) }
        { param is a pointer to a NULL-terminated C-style string. }
        { Inserts a context-specific comment into the function trace stream. }
	{ Availability: set only, get is ignored. }
	kCGLCPComment = 1232;
	
{ #define kCGLCPDumpState ((CGLContextParameter)1233) }
        { param ignored.  Dumps all the gl state. }
	{ Availability: set only, get is ignored. }
	kCGLCPDumpState = 1233;
	
{ #define kCGLCPEnableForceFlush ((CGLContextParameter)1234) }
        { param is GL_TRUE to enable "force flush" mode or GL_FALSE to disable. }
	{ Availability: set and get. }
	kCGLCPEnableForceFlush = 1234;
	
{ Profiler extensions for CGLGlobalOption }

{ Use these as the parameter name for CGLSetOption to control global profiling
** features from within your application.  Using CGLSetOption()
** overrides the settings you have in OpenGL Profiler.  Example:
**    CGLSetOption(kCGLGOEnableFunctionTrace, GL_TRUE);
**    CGLSetOption(kCGLGOComment, "Start call trace");
}
const
{ 	kCGLGOComment  = 1506; }
        { param is a pointer to a NULL-terminated C-style string. }
	{ Inserts a comment in the trace steam that applies to all contexts. }
	{ Availability: set only, get is ignored. }
	kCGLGOComment = 1506;
	
{ 	kCGLGOEnableFunctionTrace  = 1507; }
        { param is GL_TRUE or GL_FALSE }
        { Turns GL function call tracing on and off }
	{ Availability: set and get }
	kCGLGOEnableFunctionTrace = 1507;
	
{ 	kCGLGOEnableFunctionStatistics  = 1508; }
        { param is GL_TRUE or GL_FALSE }
        { Turns GL function counter on and off }
	{ Availability: set and get }
	kCGLGOEnableFunctionStatistics = 1508;

{ 	kCGLGOResetFunctionTrace  = 1509; }
        { param is ignored }
        { Erases current function trace and starts a new one }
	{ Availability: set only, get is ignored. }
	kCGLGOResetFunctionTrace = 1509;

{ 	kCGLGOPageBreak  = 1510; }
        { param is ignored }
        { Inserts a page break into the function trace }
	{ Availability: set only, get is ignored. }
	kCGLGOPageBreak = 1510;

{ 	kCGLGOResetFunctionStatistics  = 1511; }
        { param is ignored }
        { Erases current function statistics, resets all counters to 0 and starts a new set }
	{ Availability: set only, get is ignored. }
	kCGLGOResetFunctionStatistics = 1511;
	
{ 	kCGLGOEnableDebugAttach  = 1512; }
        { param is GL_TRUE or GL_FALSE }
        { Turns proflier attach feature on and off.  Off by default, you can
	   override this with the environment variable GL_ENABLE_DEBUG_ATTACH }
	{ Availability: set only, get is ignored. }
	kCGLGOEnableDebugAttach = 1512;
	
{ 	kCGLGOHideObjects  = 1513; }
        { param is GL_TRUE to hide all resources from Profiler or GL_FALSE to expose resources to
		   Profiler.  Default is GL_FALSE (expose resources to Profiler). }
	{ Availability: set and get. }
	kCGLGOHideObjects = 1513;

const
	kCGLProfBreakBefore = $0001;
	kCGLProfBreakAfter  = $0002;

{ 	kCGLGOEnableBreakpoint  = 1514; }
        { param is an array of 3 GLints:
				param[0] is function ID (see CGLProfilerFunctionEnum.h)
				param[1] is the logical OR of kCGLProfBreakBefore or kCGLProfBreakAfter, indicating how
				         you want the breakpoint to stop: before entering OpenGL, on return from OpenGL, or both.
				param[2] is a boolean which turns the breakpoint on or off.
		}
	{ Availability: set and get. }
	kCGLGOEnableBreakpoint = 1514;

{ Hardware accelerator controls
**
** Use these as the parameter name for CGLSetOption to control hardware
** accelerator debugging features from within your application.
**
** Sets debugging feature of hardware accelerator.  Unless otherwise noted, a
** non-0 param means turn the feature on, while a 0 param means turn it off.
**
** Availability: set and get
}
const
{ Rendering Options }
	kCGLGOForceSlowRenderingPath          = 1609;
	kCGLGODisableImmediateRenderPath      = 1610;
	kCGLGODisableCVARenderPath            = 1611;
	kCGLGODisableVARRenderPath            = 1612;
	kCGLGOForceWireframeRendering         = 1613;
	kCGLGOSubmitOnImmediateRenderCommand  = 1614;
	kCGLGOSubmitOnCVARenderCommand        = 1615;
	kCGLGOSubmitOnVAORenderCommand        = 1616;
	kCGLGOSubmitOnClearCommand            = 1617;

{ HW Transform & Lighting Engine Options }
	kCGLGOForceSoftwareTransformLighting  = 1618;
	kCGLGOForceSoftwareTexgen             = 1619;
	kCGLGOForceSoftwareTRUFORM_ATI        = 1620;

{ Vertex & Fragment Shader Options }
	kCGLGOForceSoftwareVertexShaders      = 1621;
	kCGLGODisableFragmentShaders_ATI      = 1622;

{ Texturing Options }
	kCGLGODisableTexturing                = 1623;
	kCGLGOOutlineTexture                  = 1624;
	kCGLGOOutlineTextureColor             = 1625;

{ glBitmap Options }
	kCGLGOForceSlowBitmapPath             = 1626;
	kCGLGODisableBitmap                   = 1627;

{ glReadPixels Options }
	kCGLGOForceSlowReadPixelsPath         = 1630;
	kCGLGODisableReadPixels               = 1631;
	kCGLGOOutlineReadPixelsBuffer         = 1632;
	kCGLGOOutlineReadPixelsBufferColor    = 1633;

{ glDrawPixels Options }
	kCGLGOForceSlowDrawPixelsPath         = 1634;
	kCGLGODisableDrawPixels               = 1635;
	kCGLGOOutlineDrawPixelsBuffer         = 1636;
	kCGLGOOutlineDrawPixelsBufferColor    = 1637;

{ glCopyPixels Options }
	kCGLGOForceSlowCopyPixelsPath         = 1638;
	kCGLGODisableCopyPixels               = 1639;
	kCGLGOOutlineCopyPixelsBuffer         = 1640;
	kCGLGOOutlineCopyPixelsBufferColor    = 1641;

{ GL Object & State Management Options }
	kCGLGOMakeAllGLObjectsRequireUpdate   = 1642;
	kCGLGOMakeAllGLStateRequireUpdate     = 1643;

{$endc} {TARGET_OS_MAC}


end.
