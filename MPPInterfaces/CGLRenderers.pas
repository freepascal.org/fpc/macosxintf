{
	Copyright:	(c) 1999-2008 Apple Inc. All rights reserved.
}
{       Pascal Translation:  Gorazd Krosl, <gorazd_1957@yahoo.ca>, October 2009 }

unit CGLRenderers;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


{
** Renderer ID numbers
}
const kCGLRendererGenericID        = $00020200;
const kCGLRendererGenericFloatID   = $00020400;
const kCGLRendererAppleSWID        = $00020600;
const kCGLRendererATIRage128ID     = $00021000;
const kCGLRendererATIRadeonID      = $00021200;
const kCGLRendererATIRageProID     = $00021400;
const kCGLRendererATIRadeon8500ID  = $00021600;
const kCGLRendererATIRadeon9700ID  = $00021800;
const kCGLRendererATIRadeonX1000ID = $00021900;
const kCGLRendererATIRadeonX2000ID = $00021A00;
const kCGLRendererGeForce2MXID     = $00022000; { also for GeForce 4MX  }
const kCGLRendererGeForce3ID       = $00022200; { also for GeForce 4 Ti }
const kCGLRendererGeForceFXID      = $00022400; { also for GeForce 6xxx, 7xxx }
const kCGLRendererGeForce8xxxID    = $00022600; { also for GeForce 9xxx }
const kCGLRendererVTBladeXP2ID     = $00023000;
const kCGLRendererIntel900ID       = $00024000;
const kCGLRendererIntelX3100ID     = $00024200;
const kCGLRendererMesa3DFXID       = $00040000;

{
** kCGLRendererIDMatchingMask gives the bits that are useful for matching a
** renderer ID (as returned by CGLDescribePixelFormat or CGLDescribeRenderer)
** with the assigned values above.  There should be no bits set in the assigned
** renderer ID's that are not set in this mask.
}
const kCGLRendererIDMatchingMask   = $00FE7F00;

{$endc} {TARGET_OS_MAC}

end.
