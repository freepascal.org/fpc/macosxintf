{
 *  AXNotificationConstants.h
 *  HIServices
 *
 *  Created by John Louch on Wed Feb 25 2004.
 *  Copyright (c) 2004 Apple Computer, Inc. All rights reserved.
 *
 }

{  Pascal Translation:  Gale R Paeper, <gpaeper@empirenet.com>, 2006 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }

unit AXNotificationConstants;
interface
uses MacTypes;

{$ifc TARGET_OS_MAC}

{$ALIGN POWER}


// focus notifications
const kAXMainWindowChangedNotification = CFSTR( 'AXMainWindowChanged' );
const kAXFocusedWindowChangedNotification = CFSTR( 'AXFocusedWindowChanged' );
const kAXFocusedUIElementChangedNotification = CFSTR( 'AXFocusedUIElementChanged' );

// application notifications
const kAXApplicationActivatedNotification = CFSTR( 'AXApplicationActivated' );
const kAXApplicationDeactivatedNotification = CFSTR( 'AXApplicationDeactivated' );
const kAXApplicationHiddenNotification = CFSTR( 'AXApplicationHidden' );
const kAXApplicationShownNotification = CFSTR( 'AXApplicationShown' );

// window notifications
const kAXWindowCreatedNotification = CFSTR( 'AXWindowCreated' );
const kAXWindowMovedNotification = CFSTR( 'AXWindowMoved' );
const kAXWindowResizedNotification = CFSTR( 'AXWindowResized' );
const kAXWindowMiniaturizedNotification = CFSTR( 'AXWindowMiniaturized' );
const kAXWindowDeminiaturizedNotification = CFSTR( 'AXWindowDeminiaturized' );

// new drawer, sheet, and help tag notifications
const kAXDrawerCreatedNotification = CFSTR( 'AXDrawerCreated' );
const kAXSheetCreatedNotification = CFSTR( 'AXSheetCreated' );
const kAXHelpTagCreatedNotification = CFSTR( 'AXHelpTagCreated' );

// element notifications
const kAXValueChangedNotification = CFSTR( 'AXValueChanged' );
const kAXUIElementDestroyedNotification = CFSTR( 'AXUIElementDestroyed' );

// menu notifications
const kAXMenuOpenedNotification = CFSTR( 'AXMenuOpened' );
const kAXMenuClosedNotification = CFSTR( 'AXMenuClosed' );
const kAXMenuItemSelectedNotification = CFSTR( 'AXMenuItemSelected' );

// table/outline notifications
const kAXRowCountChangedNotification = CFSTR( 'AXRowCountChanged' );

// outline notifications
const kAXRowExpandedNotification = CFSTR( 'AXRowExpanded' );
const kAXRowCollapsedNotification = CFSTR( 'AXRowCollapsed' );

// cell-based table notifications
const kAXSelectedCellsChangedNotification = CFSTR( 'AXSelectedCellsChanged' );

// layout area notifications
const kAXUnitsChangedNotification = CFSTR( 'AXUnitsChanged' );  
const kAXSelectedChildrenMovedNotification = CFSTR( 'AXSelectedChildrenMoved' );

// other notifications
const kAXSelectedChildrenChangedNotification = CFSTR( 'AXSelectedChildrenChanged' );
const kAXResizedNotification = CFSTR( 'AXResized' );
const kAXMovedNotification = CFSTR( 'AXMoved' );
const kAXCreatedNotification = CFSTR( 'AXCreated' );
const kAXSelectedRowsChangedNotification = CFSTR( 'AXSelectedRowsChanged' );
const kAXSelectedColumnsChangedNotification = CFSTR( 'AXSelectedColumnsChanged' );
const kAXSelectedTextChangedNotification = CFSTR( 'AXSelectedTextChanged' );
const kAXTitleChangedNotification = CFSTR( 'AXTitleChanged' );

{$endc} {TARGET_OS_MAC}

end.
