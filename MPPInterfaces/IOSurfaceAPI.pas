{
 *  IOSurface.h
 *  IOSurface
 *
 *  Copyright 2006-2008 Apple Computer, Inc. All rights reserved.
 *
 }
{  Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, September 2010 }
{  Pascal Translation Update: Jonas Maebe <jonas@freepascal.org>, October 2012 }
unit IOSurfaceAPI;
interface
uses MacTypes,MacOSXPosix,CFBase,CFDictionary,IOKitReturn;

type
	IOSurfaceRef = ^__IOSurface; { an opaque type }
	__IOSurface = record end;

type
	IOSurfaceID = UInt32;

{$ifc TARGET_OS_MAC}
{ The following list of properties are used with the CFDictionary passed to IOSurfaceCreate(). }

{ kIOSurfaceAllocSize    - CFNumber of the total allocation size of the buffer including all planes.    
				    Defaults to BufferHeight * BytesPerRow if not specified.   Must be specified for
				    dimensionless buffers. }
const kIOSurfaceAllocSize: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfaceWidth  - CFNumber for the width of the IOSurface buffer in pixels.  Required for planar IOSurfaces. }
const kIOSurfaceWidth: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfaceHeight - CFNumber for the height of the IOSurface buffer in pixels. Required for planar IOSurfaces. }
const kIOSurfaceHeight: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfaceBytesPerRow - CFNumber for the bytes per row of the buffer.   If not specified, IOSurface will first calculate
                                   the number full elements required on each row (by rounding up), multiplied by the bytes per element
				   for this buffer.   That value will then be appropriately aligned. }
const kIOSurfaceBytesPerRow: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Optional properties for non-planar two dimensional images }
 
{ kIOSurfaceBitsPerElement - CFNumber for the total number of bytes in an element.  Default to 1. }
const kIOSurfaceBytesPerElement: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfaceElementWidth   - CFNumber for how many pixels wide each element is.   Defaults to 1. } 
const kIOSurfaceElementWidth: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfaceElementHeight  - CFNumber for how many pixels high each element is.   Defaults to 1. } 
const kIOSurfaceElementHeight: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfaceOffset - CFNumber for the starting offset into the buffer.  Defaults to 0. }
const kIOSurfaceOffset: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Properties for planar surface buffers }

{ kIOSurfacePlaneInfo    - CFArray describing each image plane in the buffer as a CFDictionary.   The CFArray must have at least one entry. }
const kIOSurfacePlaneInfo: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfacePlaneWidth  - CFNumber for the width of this plane in pixels.  Required for image planes. }
const kIOSurfacePlaneWidth: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfacePlaneHeight  - CFNumber for the height of this plane in pixels.  Required for image planes. }
const kIOSurfacePlaneHeight: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfacePlaneBytesPerRow    - CFNumber for the bytes per row of this plane.  If not specified, IOSurface will first calculate
                                   the number full elements required on each row (by rounding up), multiplied by the bytes per element
				   for this plane.   That value will then be appropriately aligned. }
const kIOSurfacePlaneBytesPerRow: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfacePlaneOffset  - CFNumber for the offset into the buffer for this plane.  If not specified then IOSurface
   will lay out each plane sequentially based on the previous plane's allocation size. }
const kIOSurfacePlaneOffset: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfacePlaneSize    - CFNumber for the total data size of this plane.  Defaults to plane height * plane bytes per row if not specified. }
const kIOSurfacePlaneSize: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Optional properties for planar surface buffers }

{ kIOSurfacePlaneBase    - CFNumber for the base offset into the buffer for this plane. Optional, defaults to the plane offset }
const kIOSurfacePlaneBase: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfacePlaneBytesPerElement    - CFNumber for the bytes per element of this plane.  Optional, default is 1. }
const kIOSurfacePlaneBytesPerElement: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfacePlaneElementWidth    - CFNumber for the element width of this plane.  Optional, default is 1. }
const kIOSurfacePlaneElementWidth: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfacePlaneElementHeight   - CFNumber for the element height of this plane.  Optional, default is 1. }
const kIOSurfacePlaneElementHeight: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Optional properties global to the entire IOSurface }

{ kIOSurfaceCacheMode		- CFNumber for the CPU cache mode to be used for the allocation.  Default is kIOMapDefaultCache. }
const kIOSurfaceCacheMode: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfaceIsGlobal - CFBoolean     If true, the IOSurface may be looked up by any task in the system by its ID. }
const kIOSurfaceIsGlobal: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ kIOSurfacePixelFormat - CFNumber	A 32-bit unsigned integer that stores the traditional Mac OS X buffer format  }
const kIOSurfacePixelFormat: CFStringRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetTypeID: CFTypeID;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Create a brand new IOSurface object }
function IOSurfaceCreate( properties: CFDictionaryRef ): IOSurfaceRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Perform an atomic lookup and retain of a IOSurface by its IOSurfaceID.
   Note: Performing multiple lookups of the same IOSurface will *NOT* return
   the same IOSurfaceRef.   If you need to compare two IOSurface objects
   for equality, you must either do so by comparing their IOSurfaceIDs, or by 
   using CFEqual(). }
function IOSurfaceLookup( csid: IOSurfaceID ): IOSurfaceRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Retrieve the unique IOSurfaceID value for a IOSurface }
function IOSurfaceGetID( buffer: IOSurfaceRef ): IOSurfaceID;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);
	
const
// If you are not going to modify the data while you hold the lock, you should set this flag to avoid invalidating
	// any existing caches of the buffer contents.  This flag should be passed both to the lock and unlock functions.
	// Non-symmentrical usage of this flag will result in undefined behavior.
	kIOSurfaceLockReadOnly = $00000001;
	
	// If you want to detect/avoid a potentially expensive paging operation (such as readback from a GPU to system memory)
	// when you lock the buffer, you may include this flag.   If locking the buffer requires a readback, the lock will
	// fail with an error return of kIOReturnCannotLock.
	kIOSurfaceLockAvoidSync = $00000002;
			
{ "Lock" or "Unlock" a IOSurface for reading or writing.

    The term "lock" is used loosely in this context, and is simply used along with the
    "unlock" information to put a bound on CPU access to the raw IOSurface data.
    
    If the seed parameter is non-NULL, IOSurfaceLock() will store the buffer's
    internal modification seed value at the time you made the lock call.   You can compare
    this value to a value returned previously to determine of the contents of the buffer
    has been changed since the last lock.
    
    In the case of IOSurfaceUnlock(), the seed value returned will be the internal
    seed value at the time of the unlock.  If you locked the buffer for writing, this value
    will be incremented as the unlock is performed and the new value will be returned.
    
    See the kIOSurfaceLock enums for more information.
    
    Note: Locking and unlocking a IOSurface is not a particularly cheap operation,
    so care should be taken to avoid the calls whenever possible.   The seed values are 
    particularly useful for keeping a cache of the buffer contents.
}
function IOSurfaceLock( buffer: IOSurfaceRef; options: UInt32; var seed: UInt32 ): IOReturn;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);	
function IOSurfaceUnlock( buffer: IOSurfaceRef; options: UInt32; var seed: UInt32 ): IOReturn;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ These routines are all fairly self explanatory.  0 is returned if buffer is invalid or NULL }
function IOSurfaceGetAllocSize( buffer: IOSurfaceRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetWidth( buffer: IOSurfaceRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);
	
function IOSurfaceGetHeight( buffer: IOSurfaceRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetBytesPerElement( buffer: IOSurfaceRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetBytesPerRow( buffer: IOSurfaceRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetBaseAddress( buffer: IOSurfaceRef ): UnivPtr;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetElementWidth( buffer: IOSurfaceRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetElementHeight( buffer: IOSurfaceRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetPixelFormat( buffer: IOSurfaceRef ): OSType;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ This will return the current seed value of the buffer and is a cheap call to make to see
   if the contents of the buffer have changed since the last lock/unlock. }
function IOSurfaceGetSeed( buffer: IOSurfaceRef ): UInt32;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ Return the number of planes in this buffer.  May be 0.   Returns 0 for an invalid or NULL buffer pointer. }
function IOSurfaceGetPlaneCount( buffer: IOSurfaceRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ These routines return information about a particular plane of a IOSurface.   

   If the planeIndex is greater than or equal to the plane count of the IOSurface, zero
   is returned.... with one exception.   If this IOSurface has zero planes and a planeIndex
   of zero is passed in, the routines function just like the non-planar APIs.  This is to allow
   higher level code to treat planar and non-planar buffers is a more uniform fashion. }

function IOSurfaceGetWidthOfPlane( buffer: IOSurfaceRef; planeIndex: size_t ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetHeightOfPlane( buffer: IOSurfaceRef; planeIndex: size_t ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetBytesPerElementOfPlane( buffer: IOSurfaceRef; planeIndex: size_t ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetBytesPerRowOfPlane( buffer: IOSurfaceRef; planeIndex: size_t ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetBaseAddressOfPlane( buffer: IOSurfaceRef; planeIndex: size_t ): UnivPtr;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetElementWidthOfPlane( buffer: IOSurfaceRef; planeIndex: size_t ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceGetElementHeightOfPlane( buffer: IOSurfaceRef; planeIndex: size_t ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ These calls let you attach CF property list types to a IOSurface buffer.  These calls are 
   expensive (they essentially must serialize the data into the kernel) and thus should be avoided whenever
   possible.   Note:  These functions can not be used to change the underlying surface properties. }
procedure IOSurfaceSetValue( buffer: IOSurfaceRef; key: CFStringRef; value: CFTypeRef );
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

function IOSurfaceCopyValue( buffer: IOSurfaceRef; key: CFStringRef ): CFTypeRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

procedure IOSurfaceRemoveValue( buffer: IOSurfaceRef; key: CFStringRef );
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ This call lets you get a mach_port_t that holds a reference to the IOSurface. This is useful 
   if you need to atomically or securely pass an IOSurface to another task without making the surface global to
   the entire system.  The returned port must be deallocated with mach_port_deallocate or the equivalent.  
   Note: Any live mach ports created from an IOSurfaceRef implicity increase the IOSurface's global use
   count by one until the port is deleted. }
function IOSurfaceCreateMachPort( buffer: IOSurfaceRef ): mach_port_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ This call lets you take a mach_port_t created via IOSurfaceCreatePort() and recreate an IOSurfaceRef from it.
   Note: This call does NOT destroy the port. }
function IOSurfaceLookupFromMachPort( port: mach_port_t ): IOSurfaceRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{$ifdef XPC_TRANSLATED}
{ This call lets you get an xpc_object_t that holds a reference to the IOSurface.
   Note: Any live XPC objects created from an IOSurfaceRef implicity increase the IOSurface's global use
   count by one until the object is destroyed. }
function IOSurfaceCreateXPCObject( aSurface: IOSurfaceRef ): xpc_object_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_NA);

{ This call lets you take an xpc_object_t created via IOSurfaceCreatePort() and recreate an IOSurfaceRef from it. }
function IOSurfaceLookupFromXPCObject( xobj: xpc_object_t ): IOSurfaceRef;
IOSFC_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_NA);
{$endif} {XPC_TRANSLATED}

{ 
   IOSurfaceGetPropertyMaximum() will return the maximum of a given property that is guaranteed to be 
   compatible with all of the current devices (GPUs, etc.) in the system.   The most important ones being:
   
   kIOSurfaceBytesPerRow
   kIOSurfaceWidth
   kIOSurfaceHeight
   kIOSurfacePlaneBytesPerRow
   kIOSurfacePlaneWidth
   kIOSurfacePlaneHeight
   
   For the width and height properties, the maximum values are the largest that are guaranteed to work
   for both reading and writing.   In OpenGL terms this translates into the largest size that will work
   for both textures and render targets.
   
   This function returns 0 for properties that have no predefined limit or where the concept of a limit
   would be considered invalid (such as kIOSurfacePixelFormat).
      
}   
function IOSurfaceGetPropertyMaximum( property: CFStringRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ 
   If a property has a particular alignment requirement, then IOSurfaceGetPropertyAlignment() will return it.  
   If the property has no alignment requirement then 1 will be returned.   The following properties 
   should always be aligned if you choose calculate them yourself:
   
   kIOSurfaceBytesPerRow
   kIOSurfaceOffset
   
   kIOSurfacePlaneBase
   kIOSurfacePlaneOffset
   kIOSurfacePlaneBytesPerRow
   
}   
function IOSurfaceGetPropertyAlignment( property: CFStringRef ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{ This is a convenience function to automatically align property values.  For properties with no alignment
   requirements, the original value will be returned. }
function IOSurfaceAlignProperty( property: CFStringRef; value: size_t ): size_t;
IOSFC_AVAILABLE_STARTING(__MAC_10_6, __IPHONE_NA);

{$endc} {TARGET_OS_MAC}

end.
