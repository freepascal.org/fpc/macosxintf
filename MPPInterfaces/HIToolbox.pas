{
     File:       HIToolbox/HIToolbox.h
 
     Contains:   Master include for HIToolbox private framework
 
     Version:    HIToolbox-624~3
 
     Copyright:  � 1999-2008 by Apple Computer, Inc., all rights reserved.
 
     Bugs?:      For bug reports, consult the following page on
                 the World Wide Web:
 
                     http://developer.apple.com/bugreporter/
 
}
{       Pascal Translation:  Peter N Lewis, <peter@stairways.com.au>, August 2005 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2009 }
{       Pascal Translation Updated:  Jonas Maebe, <jonas@freepascal.org>, October 2012 }
{$propagate-units}
unit HIToolbox;
interface
uses MacTypes,HIObject,HIArchive,HIGeometry,HIToolbar,HIView,HITextUtils,HIAccessibility,Events,Notification,Drag,Controls,Appearance,HITheme,MacWindows,Menus,Dialogs,CarbonEventsCore,CarbonEvents,TextServices,Scrap,MacTextEditor,MacHelp,HIButtonViews,HIClockView,HIComboBox,HIContainerViews,HIDataBrowser,HIDisclosureViews,HIImageViews,HILittleArrows,HIMenuView,HIPopupButton,HIProgressViews,HIRelevanceBar,HIScrollView,HISearchField,HISegmentedView,HISeparator,HISlider,HITabbedView,HITextViews,HIWindowViews,HITextLengthFilter,ControlDefinitions,TranslationExtensions,Translation,AEInteraction,TypeSelect,MacApplication,Keyboards,IBCarbonRuntime,TextInputSources,HIToolboxDebugging,Lists,TextEdit,TSMTE;
{$ALIGN POWER}


{  #include <Displays.i>  -- moved to QD.i }


end.
